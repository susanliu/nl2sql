OUT_PATH="../model_output/generated_data/saved_models/"
IN_PATH="./newdata_augment/"

python preprocess_train_dev_data.py $1 full ${IN_PATH} ${OUT_PATH}
