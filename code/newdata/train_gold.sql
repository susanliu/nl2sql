SELECT count(*) FROM head WHERE age > 56	department_management
SELECT name , born_state , age FROM head ORDER BY age	department_management
SELECT creation , name , budget_in_billions FROM department	department_management
SELECT max(budget_in_billions) , min(budget_in_billions) FROM department	department_management
SELECT avg(num_employees) FROM department WHERE ranking BETWEEN 10 AND 15	department_management
SELECT name FROM head WHERE born_state != 'California'	department_management
SELECT DISTINCT T1.creation FROM department AS T1 JOIN management AS T2 ON T1.department_id = T2.department_id JOIN head AS T3 ON T2.head_id = T3.head_id WHERE T3.born_state = 'Alabama'	department_management
SELECT born_state FROM head GROUP BY born_state HAVING count(*) >= 3	department_management
SELECT creation FROM department GROUP BY creation ORDER BY count(*) DESC LIMIT 1	department_management
SELECT T1.name , T1.num_employees FROM department AS T1 JOIN management AS T2 ON T1.department_id = T2.department_id WHERE T2.temporary_acting = 'Yes'	department_management
SELECT count(DISTINCT temporary_acting) FROM management	department_management
SELECT count(*) FROM department WHERE department_id NOT IN (SELECT department_id FROM management);	department_management
SELECT DISTINCT T1.age FROM management AS T2 JOIN head AS T1 ON T1.head_id = T2.head_id WHERE T2.temporary_acting = 'Yes'	department_management
SELECT T3.born_state FROM department AS T1 JOIN management AS T2 ON T1.department_id = T2.department_id JOIN head AS T3 ON T2.head_id = T3.head_id WHERE T1.name = 'Treasury' INTERSECT SELECT T3.born_state FROM department AS T1 JOIN management AS T2 ON T1.department_id = T2.department_id JOIN head AS T3 ON T2.head_id = T3.head_id WHERE T1.name = 'Homeland Security'	department_management
SELECT T1.department_id , T1.name , count(*) FROM management AS T2 JOIN department AS T1 ON T1.department_id = T2.department_id GROUP BY T1.department_id HAVING count(*) > 1	department_management
SELECT head_id , name FROM head WHERE name LIKE '%Ha%'	department_management
SELECT count(*) FROM farm	farm
SELECT count(*) FROM farm	farm
SELECT Total_Horses FROM farm ORDER BY Total_Horses ASC	farm
SELECT Total_Horses FROM farm ORDER BY Total_Horses ASC	farm
SELECT Hosts FROM farm_competition WHERE Theme != 'Aliens'	farm
SELECT Hosts FROM farm_competition WHERE Theme != 'Aliens'	farm
SELECT Theme FROM farm_competition ORDER BY YEAR ASC	farm
SELECT Theme FROM farm_competition ORDER BY YEAR ASC	farm
SELECT avg(Working_Horses) FROM farm WHERE Total_Horses > 5000	farm
SELECT avg(Working_Horses) FROM farm WHERE Total_Horses > 5000	farm
SELECT max(Cows) , min(Cows) FROM farm	farm
SELECT max(Cows) , min(Cows) FROM farm	farm
SELECT count(DISTINCT Status) FROM city	farm
SELECT count(DISTINCT Status) FROM city	farm
SELECT Official_Name FROM city ORDER BY Population DESC	farm
SELECT Official_Name FROM city ORDER BY Population DESC	farm
SELECT Official_Name , Status FROM city ORDER BY Population DESC LIMIT 1	farm
SELECT Official_Name , Status FROM city ORDER BY Population DESC LIMIT 1	farm
SELECT T2.Year , T1.Official_Name FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID	farm
SELECT T2.Year , T1.Official_Name FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID	farm
SELECT T1.Official_Name FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID GROUP BY T2.Host_city_ID HAVING COUNT(*) > 1	farm
SELECT T1.Official_Name FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID GROUP BY T2.Host_city_ID HAVING COUNT(*) > 1	farm
SELECT T1.Status FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID GROUP BY T2.Host_city_ID ORDER BY COUNT(*) DESC LIMIT 1	farm
SELECT T1.Status FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID GROUP BY T2.Host_city_ID ORDER BY COUNT(*) DESC LIMIT 1	farm
SELECT T2.Theme FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID WHERE T1.Population > 1000	farm
SELECT T2.Theme FROM city AS T1 JOIN farm_competition AS T2 ON T1.City_ID = T2.Host_city_ID WHERE T1.Population > 1000	farm
SELECT Status , avg(Population) FROM city GROUP BY Status	farm
SELECT Status , avg(Population) FROM city GROUP BY Status	farm
SELECT Status FROM city GROUP BY Status ORDER BY COUNT(*) ASC	farm
SELECT Status FROM city GROUP BY Status ORDER BY COUNT(*) ASC	farm
SELECT Status FROM city GROUP BY Status ORDER BY COUNT(*) DESC LIMIT 1	farm
SELECT Status FROM city GROUP BY Status ORDER BY COUNT(*) DESC LIMIT 1	farm
SELECT Official_Name FROM city WHERE City_ID NOT IN (SELECT Host_city_ID FROM farm_competition)	farm
SELECT Official_Name FROM city WHERE City_ID NOT IN (SELECT Host_city_ID FROM farm_competition)	farm
SELECT Status FROM city WHERE Population > 1500 INTERSECT SELECT Status FROM city WHERE Population < 500	farm
SELECT Status FROM city WHERE Population > 1500 INTERSECT SELECT Status FROM city WHERE Population < 500	farm
SELECT Official_Name FROM city WHERE Population > 1500 OR Population < 500	farm
SELECT Official_Name FROM city WHERE Population > 1500 OR Population < 500	farm
SELECT Census_Ranking FROM city WHERE Status != "Village"	farm
SELECT Census_Ranking FROM city WHERE Status != "Village"	farm
SELECT T1.course_name FROM courses AS T1 JOIN student_course_registrations AS T2 ON T1.course_id = T2.course_Id GROUP BY T1.course_id ORDER BY count(*) DESC LIMIT 1	student_assessment
SELECT T1.course_name FROM courses AS T1 JOIN student_course_registrations AS T2 ON T1.course_id = T2.course_Id GROUP BY T1.course_id ORDER BY count(*) DESC LIMIT 1	student_assessment
SELECT student_id FROM student_course_registrations GROUP BY student_id ORDER BY count(*) LIMIT 1	student_assessment
SELECT student_id FROM student_course_registrations GROUP BY student_id ORDER BY count(*) LIMIT 1	student_assessment
SELECT T2.first_name , T2.last_name FROM candidates AS T1 JOIN people AS T2 ON T1.candidate_id = T2.person_id	student_assessment
SELECT T2.first_name , T2.last_name FROM candidates AS T1 JOIN people AS T2 ON T1.candidate_id = T2.person_id	student_assessment
SELECT student_id FROM students WHERE student_id NOT IN (SELECT student_id FROM student_course_attendance)	student_assessment
SELECT student_id FROM students WHERE student_id NOT IN (SELECT student_id FROM student_course_attendance)	student_assessment
SELECT student_id FROM student_course_attendance	student_assessment
SELECT student_id FROM student_course_attendance	student_assessment
SELECT T1.student_id , T2.course_name FROM student_course_registrations AS T1 JOIN courses AS T2 ON T1.course_id = T2.course_id	student_assessment
SELECT T2.student_details FROM student_course_registrations AS T1 JOIN students AS T2 ON T1.student_id = T2.student_id ORDER BY T1.registration_date DESC LIMIT 1	student_assessment
SELECT T2.student_details FROM student_course_registrations AS T1 JOIN students AS T2 ON T1.student_id = T2.student_id ORDER BY T1.registration_date DESC LIMIT 1	student_assessment
SELECT count(*) FROM courses AS T1 JOIN student_course_attendance AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "English"	student_assessment
SELECT count(*) FROM courses AS T1 JOIN student_course_attendance AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "English"	student_assessment
SELECT count(*) FROM courses AS T1 JOIN student_course_attendance AS T2 ON T1.course_id = T2.course_id WHERE T2.student_id = 171	student_assessment
SELECT count(*) FROM courses AS T1 JOIN student_course_attendance AS T2 ON T1.course_id = T2.course_id WHERE T2.student_id = 171	student_assessment
SELECT T2.candidate_id FROM people AS T1 JOIN candidates AS T2 ON T1.person_id = T2.candidate_id WHERE T1.email_address = "stanley.monahan@example.org"	student_assessment
SELECT T2.candidate_id FROM people AS T1 JOIN candidates AS T2 ON T1.person_id = T2.candidate_id WHERE T1.email_address = "stanley.monahan@example.org"	student_assessment
SELECT candidate_id FROM candidate_assessments ORDER BY assessment_date DESC LIMIT 1	student_assessment
SELECT candidate_id FROM candidate_assessments ORDER BY assessment_date DESC LIMIT 1	student_assessment
SELECT T1.student_details FROM students AS T1 JOIN student_course_registrations AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1	student_assessment
SELECT T1.student_details FROM students AS T1 JOIN student_course_registrations AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1	student_assessment
SELECT T1.student_id , count(*) FROM students AS T1 JOIN student_course_registrations AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id	student_assessment
SELECT T1.student_id , count(*) FROM students AS T1 JOIN student_course_registrations AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id	student_assessment
SELECT T3.course_name , count(*) FROM students AS T1 JOIN student_course_registrations AS T2 ON T1.student_id = T2.student_id JOIN courses AS T3 ON T2.course_id = T3.course_id GROUP BY T2.course_id	student_assessment
SELECT T3.course_name , count(*) FROM students AS T1 JOIN student_course_registrations AS T2 ON T1.student_id = T2.student_id JOIN courses AS T3 ON T2.course_id = T3.course_id GROUP BY T2.course_id	student_assessment
SELECT candidate_id FROM candidate_assessments WHERE asessment_outcome_code = "Pass"	student_assessment
SELECT candidate_id FROM candidate_assessments WHERE asessment_outcome_code = "Pass"	student_assessment
SELECT T3.cell_mobile_number FROM candidates AS T1 JOIN candidate_assessments AS T2 ON T1.candidate_id = T2.candidate_id JOIN people AS T3 ON T1.candidate_id = T3.person_id WHERE T2.asessment_outcome_code = "Fail"	student_assessment
SELECT T3.cell_mobile_number FROM candidates AS T1 JOIN candidate_assessments AS T2 ON T1.candidate_id = T2.candidate_id JOIN people AS T3 ON T1.candidate_id = T3.person_id WHERE T2.asessment_outcome_code = "Fail"	student_assessment
SELECT student_id FROM student_course_attendance WHERE course_id = 301	student_assessment
SELECT student_id FROM student_course_attendance WHERE course_id = 301	student_assessment
SELECT student_id FROM student_course_attendance WHERE course_id = 301 ORDER BY date_of_attendance DESC LIMIT 1	student_assessment
SELECT student_id FROM student_course_attendance WHERE course_id = 301 ORDER BY date_of_attendance DESC LIMIT 1	student_assessment
SELECT DISTINCT T1.city FROM addresses AS T1 JOIN people_addresses AS T2 ON T1.address_id = T2.address_id	student_assessment
SELECT DISTINCT T1.city FROM addresses AS T1 JOIN people_addresses AS T2 ON T1.address_id = T2.address_id	student_assessment
SELECT DISTINCT T1.city FROM addresses AS T1 JOIN people_addresses AS T2 ON T1.address_id = T2.address_id JOIN students AS T3 ON T2.person_id = T3.student_id	student_assessment
SELECT DISTINCT T1.city FROM addresses AS T1 JOIN people_addresses AS T2 ON T1.address_id = T2.address_id JOIN students AS T3 ON T2.person_id = T3.student_id	student_assessment
SELECT course_name FROM courses ORDER BY course_name	student_assessment
SELECT course_name FROM courses ORDER BY course_name	student_assessment
SELECT first_name FROM people ORDER BY first_name	student_assessment
SELECT first_name FROM people ORDER BY first_name	student_assessment
SELECT student_id FROM student_course_registrations UNION SELECT student_id FROM student_course_attendance	student_assessment
SELECT student_id FROM student_course_registrations UNION SELECT student_id FROM student_course_attendance	student_assessment
SELECT course_id FROM student_course_registrations WHERE student_id = 121 UNION SELECT course_id FROM student_course_attendance WHERE student_id = 121	student_assessment
SELECT course_id FROM student_course_registrations WHERE student_id = 121 UNION SELECT course_id FROM student_course_attendance WHERE student_id = 121	student_assessment
SELECT * FROM student_course_registrations WHERE student_id NOT IN (SELECT student_id FROM student_course_attendance)	student_assessment
SELECT * FROM student_course_registrations WHERE student_id NOT IN (SELECT student_id FROM student_course_attendance)	student_assessment
SELECT T2.student_id FROM courses AS T1 JOIN student_course_registrations AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "statistics" ORDER BY T2.registration_date	student_assessment
SELECT T2.student_id FROM courses AS T1 JOIN student_course_registrations AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "statistics" ORDER BY T2.registration_date	student_assessment
SELECT T2.student_id FROM courses AS T1 JOIN student_course_attendance AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "statistics" ORDER BY T2.date_of_attendance	student_assessment
SELECT T2.student_id FROM courses AS T1 JOIN student_course_attendance AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "statistics" ORDER BY T2.date_of_attendance	student_assessment
SELECT date FROM weather WHERE max_temperature_f > 85	bike_1
SELECT date FROM weather WHERE max_temperature_f > 85	bike_1
SELECT name FROM station WHERE lat < 37.5	bike_1
SELECT name FROM station WHERE lat < 37.5	bike_1
SELECT city , max(lat) FROM station GROUP BY city	bike_1
SELECT city , max(lat) FROM station GROUP BY city	bike_1
SELECT start_station_name , end_station_name FROM trip ORDER BY id LIMIT 3	bike_1
SELECT start_station_name , end_station_name FROM trip ORDER BY id LIMIT 3	bike_1
SELECT avg(lat) , avg(long) FROM station WHERE city = "San Jose"	bike_1
SELECT avg(lat) , avg(long) FROM station WHERE city = "San Jose"	bike_1
SELECT id FROM trip ORDER BY duration LIMIT 1	bike_1
SELECT id FROM trip ORDER BY duration LIMIT 1	bike_1
SELECT sum(duration) , max(duration) FROM trip WHERE bike_id = 636	bike_1
SELECT sum(duration) , max(duration) FROM trip WHERE bike_id = 636	bike_1
SELECT zip_code , avg(mean_temperature_f) FROM weather WHERE date LIKE "8/%" GROUP BY zip_code	bike_1
SELECT zip_code , avg(mean_temperature_f) FROM weather WHERE date LIKE "8/%" GROUP BY zip_code	bike_1
SELECT count(DISTINCT bike_id) FROM trip	bike_1
SELECT count(DISTINCT bike_id) FROM trip	bike_1
SELECT count(DISTINCT city) FROM station	bike_1
SELECT count(DISTINCT city) FROM station	bike_1
SELECT COUNT(*) FROM station WHERE city = "Mountain View"	bike_1
SELECT COUNT(*) FROM station WHERE city = "Mountain View"	bike_1
SELECT DISTINCT T1.name FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id WHERE T2.bikes_available = 7	bike_1
SELECT DISTINCT T1.name FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id WHERE T2.bikes_available = 7	bike_1
SELECT start_station_name , start_station_id FROM trip WHERE start_date LIKE "8/%" GROUP BY start_station_name ORDER BY COUNT(*) DESC LIMIT 1	bike_1
SELECT start_station_name , start_station_id FROM trip WHERE start_date LIKE "8/%" GROUP BY start_station_name ORDER BY COUNT(*) DESC LIMIT 1	bike_1
SELECT bike_id FROM trip WHERE zip_code = 94002 GROUP BY bike_id ORDER BY COUNT(*) DESC LIMIT 1	bike_1
SELECT bike_id FROM trip WHERE zip_code = 94002 GROUP BY bike_id ORDER BY COUNT(*) DESC LIMIT 1	bike_1
SELECT COUNT(*) FROM weather WHERE mean_humidity > 50 AND mean_visibility_miles > 8	bike_1
SELECT COUNT(*) FROM weather WHERE mean_humidity > 50 AND mean_visibility_miles > 8	bike_1
SELECT T1.lat , T1.long , T1.city FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.start_station_id ORDER BY T2.duration LIMIT 1	bike_1
SELECT T1.lat , T1.long , T1.city FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.start_station_id ORDER BY T2.duration LIMIT 1	bike_1
SELECT id FROM station WHERE city = "San Francisco" INTERSECT SELECT station_id FROM status GROUP BY station_id HAVING avg(bikes_available) > 10	bike_1
SELECT id FROM station WHERE city = "San Francisco" INTERSECT SELECT station_id FROM status GROUP BY station_id HAVING avg(bikes_available) > 10	bike_1
SELECT T1.name , T1.id FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id GROUP BY T2.station_id HAVING avg(T2.bikes_available) > 14 UNION SELECT name , id FROM station WHERE installation_date LIKE "12/%"	bike_1
SELECT T1.name , T1.id FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id GROUP BY T2.station_id HAVING avg(T2.bikes_available) > 14 UNION SELECT name , id FROM station WHERE installation_date LIKE "12/%"	bike_1
SELECT cloud_cover FROM weather WHERE zip_code = 94107 GROUP BY cloud_cover ORDER BY COUNT (*) DESC LIMIT 3	bike_1
SELECT cloud_cover FROM weather WHERE zip_code = 94107 GROUP BY cloud_cover ORDER BY COUNT (*) DESC LIMIT 3	bike_1
SELECT zip_code FROM weather GROUP BY zip_code ORDER BY avg(mean_sea_level_pressure_inches) LIMIT 1	bike_1
SELECT zip_code FROM weather GROUP BY zip_code ORDER BY avg(mean_sea_level_pressure_inches) LIMIT 1	bike_1
SELECT avg(bikes_available) FROM status WHERE station_id NOT IN (SELECT id FROM station WHERE city = "Palo Alto")	bike_1
SELECT avg(bikes_available) FROM status WHERE station_id NOT IN (SELECT id FROM station WHERE city = "Palo Alto")	bike_1
SELECT avg(long) FROM station WHERE id NOT IN (SELECT station_id FROM status GROUP BY station_id HAVING max(bikes_available) > 10)	bike_1
SELECT avg(long) FROM station WHERE id NOT IN (SELECT station_id FROM status GROUP BY station_id HAVING max(bikes_available) > 10)	bike_1
SELECT date , zip_code FROM weather WHERE max_temperature_f >= 80	bike_1
SELECT date , zip_code FROM weather WHERE max_temperature_f >= 80	bike_1
SELECT T1.id FROM trip AS T1 JOIN weather AS T2 ON T1.zip_code = T2.zip_code GROUP BY T2.zip_code HAVING avg(T2.mean_temperature_f) > 60	bike_1
SELECT T1.id FROM trip AS T1 JOIN weather AS T2 ON T1.zip_code = T2.zip_code GROUP BY T2.zip_code HAVING avg(T2.mean_temperature_f) > 60	bike_1
SELECT zip_code , count(*) FROM weather WHERE max_wind_Speed_mph >= 25 GROUP BY zip_code	bike_1
SELECT zip_code , count(*) FROM weather WHERE max_wind_Speed_mph >= 25 GROUP BY zip_code	bike_1
SELECT date , zip_code FROM weather WHERE min_dew_point_f < (SELECT min(min_dew_point_f) FROM weather WHERE zip_code = 94107)	bike_1
SELECT date , zip_code FROM weather WHERE min_dew_point_f < (SELECT min(min_dew_point_f) FROM weather WHERE zip_code = 94107)	bike_1
SELECT T1.id , T2.installation_date FROM trip AS T1 JOIN station AS T2 ON T1.end_station_id = T2.id	bike_1
SELECT T1.id , T2.installation_date FROM trip AS T1 JOIN station AS T2 ON T1.end_station_id = T2.id	bike_1
SELECT T1.id FROM trip AS T1 JOIN station AS T2 ON T1.start_station_id = T2.id ORDER BY T2.dock_count DESC LIMIT 1	bike_1
SELECT T1.id FROM trip AS T1 JOIN station AS T2 ON T1.start_station_id = T2.id ORDER BY T2.dock_count DESC LIMIT 1	bike_1
SELECT count(*) FROM trip AS T1 JOIN station AS T2 ON T1.end_station_id = T2.id WHERE T2.city != "San Francisco"	bike_1
SELECT count(*) FROM trip AS T1 JOIN station AS T2 ON T1.end_station_id = T2.id WHERE T2.city != "San Francisco"	bike_1
SELECT date FROM weather WHERE zip_code = 94107 AND EVENTS != "Fog" AND EVENTS != "Rain"	bike_1
SELECT date FROM weather WHERE zip_code = 94107 AND EVENTS != "Fog" AND EVENTS != "Rain"	bike_1
SELECT id FROM station WHERE lat > 37.4 EXCEPT SELECT station_id FROM status GROUP BY station_id HAVING min(bikes_available) < 7	bike_1
SELECT id FROM station WHERE lat > 37.4 EXCEPT SELECT station_id FROM status GROUP BY station_id HAVING min(bikes_available) < 7	bike_1
SELECT T1.name FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id GROUP BY T2.station_id HAVING avg(bikes_available) > 10 EXCEPT SELECT name FROM station WHERE city = "San Jose"	bike_1
SELECT T1.name FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id GROUP BY T2.station_id HAVING avg(bikes_available) > 10 EXCEPT SELECT name FROM station WHERE city = "San Jose"	bike_1
SELECT name , lat , city FROM station ORDER BY lat LIMIT 1	bike_1
SELECT name , lat , city FROM station ORDER BY lat LIMIT 1	bike_1
SELECT date , mean_temperature_f , mean_humidity FROM weather ORDER BY max_gust_speed_mph DESC LIMIT 3	bike_1
SELECT date , mean_temperature_f , mean_humidity FROM weather ORDER BY max_gust_speed_mph DESC LIMIT 3	bike_1
SELECT city , COUNT(*) FROM station GROUP BY city HAVING COUNT(*) >= 15	bike_1
SELECT city , COUNT(*) FROM station GROUP BY city HAVING COUNT(*) >= 15	bike_1
SELECT start_station_id , start_station_name FROM trip GROUP BY start_station_name HAVING COUNT(*) >= 200	bike_1
SELECT start_station_id , start_station_name FROM trip GROUP BY start_station_name HAVING COUNT(*) >= 200	bike_1
SELECT zip_code FROM weather GROUP BY zip_code HAVING avg(mean_visibility_miles) < 10	bike_1
SELECT zip_code FROM weather GROUP BY zip_code HAVING avg(mean_visibility_miles) < 10	bike_1
SELECT city FROM station GROUP BY city ORDER BY max(lat) DESC	bike_1
SELECT city FROM station GROUP BY city ORDER BY max(lat) DESC	bike_1
SELECT date , cloud_cover FROM weather ORDER BY cloud_cover DESC LIMIT 5	bike_1
SELECT date , cloud_cover FROM weather ORDER BY cloud_cover DESC LIMIT 5	bike_1
SELECT id , duration FROM trip ORDER BY duration DESC LIMIT 3	bike_1
SELECT id , duration FROM trip ORDER BY duration DESC LIMIT 3	bike_1
SELECT T1.name , T1.long , avg(T2.duration) FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.start_station_id GROUP BY T2.start_station_id	bike_1
SELECT T1.name , T1.long , avg(T2.duration) FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.start_station_id GROUP BY T2.start_station_id	bike_1
SELECT T1.name , T1.lat , min(T2.duration) FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.end_station_id GROUP BY T2.end_station_id	bike_1
SELECT T1.name , T1.lat , min(T2.duration) FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.end_station_id GROUP BY T2.end_station_id	bike_1
SELECT DISTINCT start_station_name FROM trip WHERE duration < 100	bike_1
SELECT DISTINCT start_station_name FROM trip WHERE duration < 100	bike_1
SELECT DISTINCT zip_code FROM weather EXCEPT SELECT DISTINCT zip_code FROM weather WHERE max_dew_point_f >= 70	bike_1
SELECT DISTINCT zip_code FROM weather EXCEPT SELECT DISTINCT zip_code FROM weather WHERE max_dew_point_f >= 70	bike_1
SELECT id FROM trip WHERE duration >= (SELECT avg(duration) FROM trip WHERE zip_code = 94103)	bike_1
SELECT id FROM trip WHERE duration >= (SELECT avg(duration) FROM trip WHERE zip_code = 94103)	bike_1
SELECT date FROM weather WHERE mean_sea_level_pressure_inches BETWEEN 30.3 AND 31	bike_1
SELECT date FROM weather WHERE mean_sea_level_pressure_inches BETWEEN 30.3 AND 31	bike_1
SELECT date , max_temperature_f - min_temperature_f FROM weather ORDER BY max_temperature_f - min_temperature_f LIMIT 1	bike_1
SELECT date , max_temperature_f - min_temperature_f FROM weather ORDER BY max_temperature_f - min_temperature_f LIMIT 1	bike_1
SELECT DISTINCT T1.id , T1.name FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id WHERE T2.bikes_available > 12	bike_1
SELECT DISTINCT T1.id , T1.name FROM station AS T1 JOIN status AS T2 ON T1.id = T2.station_id WHERE T2.bikes_available > 12	bike_1
SELECT zip_code FROM weather GROUP BY zip_code HAVING avg(mean_humidity) < 70 INTERSECT SELECT zip_code FROM trip GROUP BY zip_code HAVING count(*) >= 100	bike_1
SELECT zip_code FROM weather GROUP BY zip_code HAVING avg(mean_humidity) < 70 INTERSECT SELECT zip_code FROM trip GROUP BY zip_code HAVING count(*) >= 100	bike_1
SELECT name FROM station WHERE city = "Palo Alto" EXCEPT SELECT end_station_name FROM trip GROUP BY end_station_name HAVING count(*) > 100	bike_1
SELECT name FROM station WHERE city = "Palo Alto" EXCEPT SELECT end_station_name FROM trip GROUP BY end_station_name HAVING count(*) > 100	bike_1
SELECT count(*) FROM station AS T1 JOIN trip AS T2 JOIN station AS T3 JOIN trip AS T4 ON T1.id = T2.start_station_id AND T2.id = T4.id AND T3.id = T4.end_station_id WHERE T1.city = "Mountain View" AND T3.city = "Palo Alto"	bike_1
SELECT count(*) FROM station AS T1 JOIN trip AS T2 JOIN station AS T3 JOIN trip AS T4 ON T1.id = T2.start_station_id AND T2.id = T4.id AND T3.id = T4.end_station_id WHERE T1.city = "Mountain View" AND T3.city = "Palo Alto"	bike_1
SELECT avg(T1.lat) , avg(T1.long) FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.start_station_id	bike_1
SELECT avg(T1.lat) , avg(T1.long) FROM station AS T1 JOIN trip AS T2 ON T1.id = T2.start_station_id	bike_1
SELECT count(*) FROM book	book_2
SELECT Writer FROM book ORDER BY Writer ASC	book_2
SELECT Title FROM book ORDER BY Issues ASC	book_2
SELECT Title FROM book WHERE Writer != "Elaine Lee"	book_2
SELECT Title , Issues FROM book	book_2
SELECT Publication_Date FROM publication ORDER BY Price DESC	book_2
SELECT DISTINCT Publisher FROM publication WHERE Price > 5000000	book_2
SELECT Publisher FROM publication ORDER BY Price DESC LIMIT 1	book_2
SELECT Publication_Date FROM publication ORDER BY Price ASC LIMIT 3	book_2
SELECT T1.Title , T2.Publication_Date FROM book AS T1 JOIN publication AS T2 ON T1.Book_ID = T2.Book_ID	book_2
SELECT T1.Writer FROM book AS T1 JOIN publication AS T2 ON T1.Book_ID = T2.Book_ID WHERE T2.Price > 4000000	book_2
SELECT T1.Title FROM book AS T1 JOIN publication AS T2 ON T1.Book_ID = T2.Book_ID ORDER BY T2.Price DESC	book_2
SELECT Publisher FROM publication GROUP BY Publisher HAVING COUNT(*) > 1	book_2
SELECT Publisher , COUNT(*) FROM publication GROUP BY Publisher	book_2
SELECT Publication_Date FROM publication GROUP BY Publication_Date ORDER BY COUNT(*) DESC LIMIT 1	book_2
SELECT Writer FROM book GROUP BY Writer HAVING COUNT(*) > 1	book_2
SELECT Title FROM book WHERE Book_ID NOT IN (SELECT Book_ID FROM publication)	book_2
SELECT Publisher FROM publication WHERE Price > 10000000 INTERSECT SELECT Publisher FROM publication WHERE Price < 5000000	book_2
SELECT COUNT (DISTINCT Publication_Date) FROM publication	book_2
SELECT COUNT (DISTINCT Publication_Date) FROM publication	book_2
SELECT Price FROM publication WHERE Publisher = "Person" OR Publisher = "Wiley"	book_2
SELECT count(*) FROM actor	musical
SELECT count(*) FROM actor	musical
SELECT Name FROM actor ORDER BY Name ASC	musical
SELECT Name FROM actor ORDER BY Name ASC	musical
SELECT Character , Duration FROM actor	musical
SELECT Character , Duration FROM actor	musical
SELECT Name FROM actor WHERE Age != 20	musical
SELECT Name FROM actor WHERE Age != 20	musical
SELECT Character FROM actor ORDER BY age DESC	musical
SELECT Character FROM actor ORDER BY age DESC	musical
SELECT Duration FROM actor ORDER BY Age DESC LIMIT 1	musical
SELECT Duration FROM actor ORDER BY Age DESC LIMIT 1	musical
SELECT Name FROM musical WHERE Nominee = "Bob Fosse"	musical
SELECT Name FROM musical WHERE Nominee = "Bob Fosse"	musical
SELECT DISTINCT Nominee FROM musical WHERE Award != "Tony Award"	musical
SELECT DISTINCT Nominee FROM musical WHERE Award != "Tony Award"	musical
SELECT T1.Name , T2.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID	musical
SELECT T1.Name , T2.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID	musical
SELECT T1.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID WHERE T2.Name = "The Phantom of the Opera"	musical
SELECT T1.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID WHERE T2.Name = "The Phantom of the Opera"	musical
SELECT T1.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID ORDER BY T2.Year DESC	musical
SELECT T1.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID ORDER BY T2.Year DESC	musical
SELECT T2.Name , COUNT(*) FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID GROUP BY T1.Musical_ID	musical
SELECT T2.Name , COUNT(*) FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID GROUP BY T1.Musical_ID	musical
SELECT T2.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID GROUP BY T1.Musical_ID HAVING COUNT(*) >= 3	musical
SELECT T2.Name FROM actor AS T1 JOIN musical AS T2 ON T1.Musical_ID = T2.Musical_ID GROUP BY T1.Musical_ID HAVING COUNT(*) >= 3	musical
SELECT Nominee , COUNT(*) FROM musical GROUP BY Nominee	musical
SELECT Nominee , COUNT(*) FROM musical GROUP BY Nominee	musical
SELECT Nominee FROM musical GROUP BY Nominee ORDER BY COUNT(*) DESC LIMIT 1	musical
SELECT Nominee FROM musical GROUP BY Nominee ORDER BY COUNT(*) DESC LIMIT 1	musical
SELECT RESULT FROM musical GROUP BY RESULT ORDER BY COUNT(*) DESC LIMIT 1	musical
SELECT RESULT FROM musical GROUP BY RESULT ORDER BY COUNT(*) DESC LIMIT 1	musical
SELECT Nominee FROM musical GROUP BY Nominee HAVING COUNT(*) > 2	musical
SELECT Nominee FROM musical GROUP BY Nominee HAVING COUNT(*) > 2	musical
SELECT Name FROM musical WHERE Musical_ID NOT IN (SELECT Musical_ID FROM actor)	musical
SELECT Name FROM musical WHERE Musical_ID NOT IN (SELECT Musical_ID FROM actor)	musical
SELECT Nominee FROM musical WHERE Award = "Tony Award" INTERSECT SELECT Nominee FROM musical WHERE Award = "Drama Desk Award"	musical
SELECT Nominee FROM musical WHERE Award = "Tony Award" INTERSECT SELECT Nominee FROM musical WHERE Award = "Drama Desk Award"	musical
SELECT Nominee FROM musical WHERE Award = "Tony Award" OR Award = "Cleavant Derricks"	musical
SELECT Nominee FROM musical WHERE Award = "Tony Award" OR Award = "Cleavant Derricks"	musical
SELECT email FROM user_profiles WHERE name = 'Mary'	twitter_1
SELECT partitionid FROM user_profiles WHERE name = 'Iron Man'	twitter_1
SELECT count(*) FROM user_profiles	twitter_1
SELECT count(*) FROM follows	twitter_1
SELECT count(*) FROM follows GROUP BY f1	twitter_1
SELECT count(*) FROM tweets	twitter_1
SELECT count(DISTINCT UID) FROM tweets	twitter_1
SELECT name , email FROM user_profiles WHERE name LIKE '%Swift%'	twitter_1
SELECT name FROM user_profiles WHERE email LIKE '%superstar%' OR email LIKE '%edu%'	twitter_1
SELECT text FROM tweets WHERE text LIKE '%intern%'	twitter_1
SELECT name , email FROM user_profiles WHERE followers > 1000	twitter_1
SELECT T1.name FROM user_profiles AS T1 JOIN follows AS T2 ON T1.uid = T2.f1 GROUP BY T2.f1 HAVING count(*) > (SELECT count(*) FROM user_profiles AS T1 JOIN follows AS T2 ON T1.uid = T2.f1 WHERE T1.name = 'Tyler Swift')	twitter_1
SELECT T1.name , T1.email FROM user_profiles AS T1 JOIN follows AS T2 ON T1.uid = T2.f1 GROUP BY T2.f1 HAVING count(*) > 1	twitter_1
SELECT T1.name FROM user_profiles AS T1 JOIN tweets AS T2 ON T1.uid = T2.uid GROUP BY T2.uid HAVING count(*) > 1	twitter_1
SELECT T2.f1 FROM user_profiles AS T1 JOIN follows AS T2 ON T1.uid = T2.f2 WHERE T1.name = "Mary" INTERSECT SELECT T2.f1 FROM user_profiles AS T1 JOIN follows AS T2 ON T1.uid = T2.f2 WHERE T1.name = "Susan"	twitter_1
SELECT T2.f1 FROM user_profiles AS T1 JOIN follows AS T2 ON T1.uid = T2.f2 WHERE T1.name = "Mary" OR T1.name = "Susan"	twitter_1
SELECT name FROM user_profiles ORDER BY followers DESC LIMIT 1	twitter_1
SELECT name , email FROM user_profiles ORDER BY followers LIMIT 1	twitter_1
SELECT name , followers FROM user_profiles ORDER BY followers DESC	twitter_1
SELECT name FROM user_profiles ORDER BY followers DESC LIMIT 5	twitter_1
SELECT text FROM tweets ORDER BY createdate	twitter_1
SELECT T1.name , count(*) FROM user_profiles AS T1 JOIN tweets AS T2 ON T1.uid = T2.uid GROUP BY T2.uid	twitter_1
SELECT T1.name , T1.partitionid FROM user_profiles AS T1 JOIN tweets AS T2 ON T1.uid = T2.uid GROUP BY T2.uid HAVING count(*) < 2	twitter_1
SELECT T1.name , count(*) FROM user_profiles AS T1 JOIN tweets AS T2 ON T1.uid = T2.uid GROUP BY T2.uid HAVING count(*) > 1	twitter_1
SELECT avg(followers) FROM user_profiles WHERE UID NOT IN (SELECT UID FROM tweets)	twitter_1
SELECT avg(followers) FROM user_profiles WHERE UID IN (SELECT UID FROM tweets)	twitter_1
SELECT max(followers) , sum(followers) FROM user_profiles	twitter_1
SELECT distinct(catalog_entry_name) FROM catalog_contents	product_catalog
SELECT distinct(catalog_entry_name) FROM catalog_contents	product_catalog
SELECT attribute_data_type FROM Attribute_Definitions GROUP BY attribute_data_type HAVING count(*) > 3	product_catalog
SELECT attribute_data_type FROM Attribute_Definitions GROUP BY attribute_data_type HAVING count(*) > 3	product_catalog
SELECT attribute_data_type FROM Attribute_Definitions WHERE attribute_name = "Green"	product_catalog
SELECT attribute_data_type FROM Attribute_Definitions WHERE attribute_name = "Green"	product_catalog
SELECT catalog_level_name , catalog_level_number FROM Catalog_Structure WHERE catalog_level_number BETWEEN 5 AND 10	product_catalog
SELECT catalog_level_name , catalog_level_number FROM Catalog_Structure WHERE catalog_level_number BETWEEN 5 AND 10	product_catalog
SELECT distinct(catalog_publisher) FROM catalogs WHERE catalog_publisher LIKE "%Murray%"	product_catalog
SELECT distinct(catalog_publisher) FROM catalogs WHERE catalog_publisher LIKE "%Murray%"	product_catalog
SELECT catalog_publisher FROM catalogs GROUP BY catalog_publisher ORDER BY count(*) DESC LIMIT 1	product_catalog
SELECT catalog_publisher FROM catalogs GROUP BY catalog_publisher ORDER BY count(*) DESC LIMIT 1	product_catalog
SELECT t1.catalog_name , t1.date_of_publication FROM catalogs AS t1 JOIN catalog_structure AS t2 ON t1.catalog_id = t2.catalog_id WHERE catalog_level_number > 5	product_catalog
SELECT t1.catalog_name , t1.date_of_publication FROM catalogs AS t1 JOIN catalog_structure AS t2 ON t1.catalog_id = t2.catalog_id WHERE catalog_level_number > 5	product_catalog
SELECT t1.catalog_entry_name FROM Catalog_Contents AS t1 JOIN Catalog_Contents_Additional_Attributes AS t2 ON t1.catalog_entry_id = t2.catalog_entry_id WHERE t2.attribute_value = (SELECT attribute_value FROM Catalog_Contents_Additional_Attributes GROUP BY attribute_value ORDER BY count(*) DESC LIMIT 1)	product_catalog
SELECT t1.catalog_entry_name FROM Catalog_Contents AS t1 JOIN Catalog_Contents_Additional_Attributes AS t2 ON t1.catalog_entry_id = t2.catalog_entry_id WHERE t2.attribute_value = (SELECT attribute_value FROM Catalog_Contents_Additional_Attributes GROUP BY attribute_value ORDER BY count(*) DESC LIMIT 1)	product_catalog
SELECT catalog_entry_name FROM catalog_contents ORDER BY price_in_dollars DESC LIMIT 1	product_catalog
SELECT catalog_entry_name FROM catalog_contents ORDER BY price_in_dollars DESC LIMIT 1	product_catalog
SELECT t2.catalog_level_name FROM catalog_contents AS t1 JOIN catalog_structure AS t2 ON t1.catalog_level_number = t2.catalog_level_number ORDER BY t1.price_in_dollars LIMIT 1	product_catalog
SELECT t2.catalog_level_name FROM catalog_contents AS t1 JOIN catalog_structure AS t2 ON t1.catalog_level_number = t2.catalog_level_number ORDER BY t1.price_in_dollars LIMIT 1	product_catalog
SELECT avg(price_in_euros) , min(price_in_euros) FROM catalog_contents	product_catalog
SELECT avg(price_in_euros) , min(price_in_euros) FROM catalog_contents	product_catalog
SELECT catalog_entry_name FROM catalog_contents ORDER BY height DESC LIMIT 1	product_catalog
SELECT catalog_entry_name FROM catalog_contents ORDER BY height DESC LIMIT 1	product_catalog
SELECT catalog_entry_name FROM catalog_contents ORDER BY capacity ASC LIMIT 1	product_catalog
SELECT catalog_entry_name FROM catalog_contents ORDER BY capacity ASC LIMIT 1	product_catalog
SELECT catalog_entry_name FROM catalog_contents WHERE product_stock_number LIKE "2%"	product_catalog
SELECT catalog_entry_name FROM catalog_contents WHERE product_stock_number LIKE "2%"	product_catalog
SELECT t1.catalog_entry_name FROM Catalog_Contents AS t1 JOIN Catalog_Contents_Additional_Attributes AS t2 ON t1.catalog_entry_id = t2.catalog_entry_id WHERE t2.catalog_level_number = "8"	product_catalog
SELECT t1.catalog_entry_name FROM Catalog_Contents AS t1 JOIN Catalog_Contents_Additional_Attributes AS t2 ON t1.catalog_entry_id = t2.catalog_entry_id WHERE t2.catalog_level_number = "8"	product_catalog
SELECT catalog_entry_name FROM catalog_contents WHERE LENGTH < 3 OR width > 5	product_catalog
SELECT catalog_entry_name FROM catalog_contents WHERE LENGTH < 3 OR width > 5	product_catalog
SELECT t1.attribute_name , t1.attribute_id FROM Attribute_Definitions AS t1 JOIN Catalog_Contents_Additional_Attributes AS t2 ON t1.attribute_id = t2.attribute_id WHERE t2.attribute_value = 0	product_catalog
SELECT t1.attribute_name , t1.attribute_id FROM Attribute_Definitions AS t1 JOIN Catalog_Contents_Additional_Attributes AS t2 ON t1.attribute_id = t2.attribute_id WHERE t2.attribute_value = 0	product_catalog
SELECT catalog_entry_name , capacity FROM Catalog_Contents WHERE price_in_dollars > 700	product_catalog
SELECT catalog_entry_name , capacity FROM Catalog_Contents WHERE price_in_dollars > 700	product_catalog
SELECT date_of_latest_revision FROM Catalogs GROUP BY date_of_latest_revision HAVING count(*) > 1	product_catalog
SELECT date_of_latest_revision FROM Catalogs GROUP BY date_of_latest_revision HAVING count(*) > 1	product_catalog
SELECT count(*) FROM catalog_contents	product_catalog
SELECT count(*) FROM catalog_contents	product_catalog
SELECT catalog_entry_name FROM catalog_contents WHERE next_entry_id > 8	product_catalog
SELECT catalog_entry_name FROM catalog_contents WHERE next_entry_id > 8	product_catalog
SELECT count(*) FROM Aircraft	flight_1
SELECT count(*) FROM Aircraft	flight_1
SELECT name , distance FROM Aircraft	flight_1
SELECT name , distance FROM Aircraft	flight_1
SELECT aid FROM Aircraft WHERE distance > 1000	flight_1
SELECT aid FROM Aircraft WHERE distance > 1000	flight_1
SELECT count(*) FROM Aircraft WHERE distance BETWEEN 1000 AND 5000	flight_1
SELECT count(*) FROM Aircraft WHERE distance BETWEEN 1000 AND 5000	flight_1
SELECT name , distance FROM Aircraft WHERE aid = 12	flight_1
SELECT name , distance FROM Aircraft WHERE aid = 12	flight_1
SELECT min(distance) , avg(distance) , max(distance) FROM Aircraft	flight_1
SELECT min(distance) , avg(distance) , max(distance) FROM Aircraft	flight_1
SELECT aid , name FROM Aircraft ORDER BY distance DESC LIMIT 1	flight_1
SELECT aid , name FROM Aircraft ORDER BY distance DESC LIMIT 1	flight_1
SELECT name FROM Aircraft ORDER BY distance LIMIT 3	flight_1
SELECT name FROM Aircraft ORDER BY distance LIMIT 3	flight_1
SELECT name FROM Aircraft WHERE distance > (SELECT avg(distance) FROM Aircraft)	flight_1
SELECT name FROM Aircraft WHERE distance > (SELECT avg(distance) FROM Aircraft)	flight_1
SELECT count(*) FROM Employee	flight_1
SELECT count(*) FROM Employee	flight_1
SELECT name , salary FROM Employee ORDER BY salary	flight_1
SELECT name , salary FROM Employee ORDER BY salary	flight_1
SELECT eid FROM Employee WHERE salary > 100000	flight_1
SELECT eid FROM Employee WHERE salary > 100000	flight_1
SELECT count(*) FROM Employee WHERE salary BETWEEN 100000 AND 200000	flight_1
SELECT count(*) FROM Employee WHERE salary BETWEEN 100000 AND 200000	flight_1
SELECT name , salary FROM Employee WHERE eid = 242518965	flight_1
SELECT name , salary FROM Employee WHERE eid = 242518965	flight_1
SELECT avg(salary) , max(salary) FROM Employee	flight_1
SELECT avg(salary) , max(salary) FROM Employee	flight_1
SELECT eid , name FROM Employee ORDER BY salary DESC LIMIT 1	flight_1
SELECT eid , name FROM Employee ORDER BY salary DESC LIMIT 1	flight_1
SELECT name FROM Employee ORDER BY salary ASC LIMIT 3	flight_1
SELECT name FROM Employee ORDER BY salary ASC LIMIT 3	flight_1
SELECT name FROM Employee WHERE salary > (SELECT avg(salary) FROM Employee)	flight_1
SELECT name FROM Employee WHERE salary > (SELECT avg(salary) FROM Employee)	flight_1
SELECT eid , salary FROM Employee WHERE name = 'Mark Young'	flight_1
SELECT eid , salary FROM Employee WHERE name = 'Mark Young'	flight_1
SELECT count(*) FROM Flight	flight_1
SELECT count(*) FROM Flight	flight_1
SELECT flno , origin , destination FROM Flight ORDER BY origin	flight_1
SELECT flno , origin , destination FROM Flight ORDER BY origin	flight_1
SELECT flno FROM Flight WHERE origin = "Los Angeles"	flight_1
SELECT flno FROM Flight WHERE origin = "Los Angeles"	flight_1
SELECT origin FROM Flight WHERE destination = "Honolulu"	flight_1
SELECT origin FROM Flight WHERE destination = "Honolulu"	flight_1
SELECT departure_date , arrival_date FROM Flight WHERE origin = "Los Angeles" AND destination = "Honolulu"	flight_1
SELECT departure_date , arrival_date FROM Flight WHERE origin = "Los Angeles" AND destination = "Honolulu"	flight_1
SELECT flno FROM Flight WHERE distance > 2000	flight_1
SELECT flno FROM Flight WHERE distance > 2000	flight_1
SELECT avg(price) FROM Flight WHERE origin = "Los Angeles" AND destination = "Honolulu"	flight_1
SELECT avg(price) FROM Flight WHERE origin = "Los Angeles" AND destination = "Honolulu"	flight_1
SELECT origin , destination FROM Flight WHERE price > 300	flight_1
SELECT origin , destination FROM Flight WHERE price > 300	flight_1
SELECT flno , distance FROM Flight ORDER BY price DESC LIMIT 1	flight_1
SELECT flno , distance FROM Flight ORDER BY price DESC LIMIT 1	flight_1
SELECT flno FROM Flight ORDER BY distance ASC LIMIT 3	flight_1
SELECT flno FROM Flight ORDER BY distance ASC LIMIT 3	flight_1
SELECT avg(distance) , avg(price) FROM Flight WHERE origin = "Los Angeles"	flight_1
SELECT avg(distance) , avg(price) FROM Flight WHERE origin = "Los Angeles"	flight_1
SELECT origin , count(*) FROM Flight GROUP BY origin	flight_1
SELECT origin , count(*) FROM Flight GROUP BY origin	flight_1
SELECT destination , count(*) FROM Flight GROUP BY destination	flight_1
SELECT destination , count(*) FROM Flight GROUP BY destination	flight_1
SELECT origin FROM Flight GROUP BY origin ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT origin FROM Flight GROUP BY origin ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT destination FROM Flight GROUP BY destination ORDER BY count(*) LIMIT 1	flight_1
SELECT destination FROM Flight GROUP BY destination ORDER BY count(*) LIMIT 1	flight_1
SELECT T2.name FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid WHERE T1.flno = 99	flight_1
SELECT T2.name FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid WHERE T1.flno = 99	flight_1
SELECT T1.flno FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid WHERE T2.name = "Airbus A340-300"	flight_1
SELECT T1.flno FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid WHERE T2.name = "Airbus A340-300"	flight_1
SELECT T2.name , count(*) FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid GROUP BY T1.aid	flight_1
SELECT T2.name , count(*) FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid GROUP BY T1.aid	flight_1
SELECT T2.name FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid GROUP BY T1.aid HAVING count(*) >= 2	flight_1
SELECT T2.name FROM Flight AS T1 JOIN Aircraft AS T2 ON T1.aid = T2.aid GROUP BY T1.aid HAVING count(*) >= 2	flight_1
SELECT count(DISTINCT eid) FROM Certificate	flight_1
SELECT count(DISTINCT eid) FROM Certificate	flight_1
SELECT eid FROM Employee EXCEPT SELECT eid FROM Certificate	flight_1
SELECT eid FROM Employee EXCEPT SELECT eid FROM Certificate	flight_1
SELECT T3.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T1.name = "John Williams"	flight_1
SELECT T3.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T1.name = "John Williams"	flight_1
SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Boeing 737-800"	flight_1
SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Boeing 737-800"	flight_1
SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Boeing 737-800" INTERSECT SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Airbus A340-300"	flight_1
SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Boeing 737-800" INTERSECT SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Airbus A340-300"	flight_1
SELECT name FROM Employee EXCEPT SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Boeing 737-800"	flight_1
SELECT name FROM Employee EXCEPT SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.name = "Boeing 737-800"	flight_1
SELECT T2.name FROM Certificate AS T1 JOIN Aircraft AS T2 ON T2.aid = T1.aid GROUP BY T1.aid ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT T2.name FROM Certificate AS T1 JOIN Aircraft AS T2 ON T2.aid = T1.aid GROUP BY T1.aid ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT T2.name FROM Certificate AS T1 JOIN Aircraft AS T2 ON T2.aid = T1.aid WHERE T2.distance > 5000 GROUP BY T1.aid ORDER BY count(*) >= 5	flight_1
SELECT T2.name FROM Certificate AS T1 JOIN Aircraft AS T2 ON T2.aid = T1.aid WHERE T2.distance > 5000 GROUP BY T1.aid ORDER BY count(*) >= 5	flight_1
SELECT T1.name , T1.salary FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid GROUP BY T1.eid ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT T1.name , T1.salary FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid GROUP BY T1.eid ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.distance > 5000 GROUP BY T1.eid ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT T1.name FROM Employee AS T1 JOIN Certificate AS T2 ON T1.eid = T2.eid JOIN Aircraft AS T3 ON T3.aid = T2.aid WHERE T3.distance > 5000 GROUP BY T1.eid ORDER BY count(*) DESC LIMIT 1	flight_1
SELECT billing_country , COUNT(*) FROM invoices GROUP BY billing_country ORDER BY count(*) DESC LIMIT 5;	store_1
SELECT billing_country , COUNT(*) FROM invoices GROUP BY billing_country ORDER BY count(*) DESC LIMIT 5;	store_1
SELECT billing_country , SUM(total) FROM invoices GROUP BY billing_country ORDER BY SUM(total) DESC LIMIT 8;	store_1
SELECT billing_country , SUM(total) FROM invoices GROUP BY billing_country ORDER BY SUM(total) DESC LIMIT 8;	store_1
SELECT billing_country , AVG(total) FROM invoices GROUP BY billing_country ORDER BY AVG(total) DESC LIMIT 10;	store_1
SELECT billing_country , AVG(total) FROM invoices GROUP BY billing_country ORDER BY AVG(total) DESC LIMIT 10;	store_1
SELECT T1.first_name , T1.last_name FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id ORDER BY T2.invoice_date DESC LIMIT 5;	store_1
SELECT T1.first_name , T1.last_name FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id ORDER BY T2.invoice_date DESC LIMIT 5;	store_1
SELECT T1.first_name , T1.last_name , COUNT(*) FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id GROUP BY T1.id ORDER BY COUNT(*) DESC LIMIT 10;	store_1
SELECT T1.first_name , T1.last_name , COUNT(*) FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id GROUP BY T1.id ORDER BY COUNT(*) DESC LIMIT 10;	store_1
SELECT T1.first_name , T1.last_name , SUM(T2.total) FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id GROUP BY T1.id ORDER BY SUM(T2.total) DESC LIMIT 10;	store_1
SELECT T1.first_name , T1.last_name , SUM(T2.total) FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id GROUP BY T1.id ORDER BY SUM(T2.total) DESC LIMIT 10;	store_1
SELECT T1.name , COUNT(*) FROM genres AS T1 JOIN tracks AS T2 ON T2.genre_id = T1.id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 5;	store_1
SELECT T1.name , COUNT(*) FROM genres AS T1 JOIN tracks AS T2 ON T2.genre_id = T1.id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 5;	store_1
SELECT title FROM albums;	store_1
SELECT title FROM albums;	store_1
SELECT title FROM albums ORDER BY title;	store_1
SELECT title FROM albums ORDER BY title;	store_1
SELECT title FROM albums WHERE title LIKE 'A%' ORDER BY title;	store_1
SELECT title FROM albums WHERE title LIKE 'A%' ORDER BY title;	store_1
SELECT T1.first_name , T1.last_name FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id ORDER BY total LIMIT 10;	store_1
SELECT T1.first_name , T1.last_name FROM customers AS T1 JOIN invoices AS T2 ON T2.customer_id = T1.id ORDER BY total LIMIT 10;	store_1
SELECT sum(total) FROM invoices WHERE billing_city = "Chicago" AND billing_state = "IL";	store_1
SELECT sum(total) FROM invoices WHERE billing_city = "Chicago" AND billing_state = "IL";	store_1
SELECT COUNT(*) FROM invoices WHERE billing_city = "Chicago" AND billing_state = "IL";	store_1
SELECT COUNT(*) FROM invoices WHERE billing_city = "Chicago" AND billing_state = "IL";	store_1
SELECT billing_state , COUNT(*) FROM invoices WHERE billing_country = "USA" GROUP BY billing_state;	store_1
SELECT billing_state , COUNT(*) FROM invoices WHERE billing_country = "USA" GROUP BY billing_state;	store_1
SELECT billing_state , COUNT(*) FROM invoices WHERE billing_country = "USA" GROUP BY billing_state ORDER BY COUNT(*) DESC LIMIT 1;	store_1
SELECT billing_state , COUNT(*) FROM invoices WHERE billing_country = "USA" GROUP BY billing_state ORDER BY COUNT(*) DESC LIMIT 1;	store_1
SELECT billing_state , COUNT(*) , SUM(total) FROM invoices WHERE billing_state = "CA";	store_1
SELECT billing_state , COUNT(*) , SUM(total) FROM invoices WHERE billing_state = "CA";	store_1
SELECT T1.title FROM albums AS T1 JOIN artists AS T2 ON T1.artist_id = T2.id WHERE T2.name = "Aerosmith";	store_1
SELECT T1.title FROM albums AS T1 JOIN artists AS T2 ON T1.artist_id = T2.id WHERE T2.name = "Aerosmith";	store_1
SELECT count(*) FROM albums AS T1 JOIN artists AS T2 ON T1.artist_id = T2.id WHERE T2.name = "Billy Cobham";	store_1
SELECT count(*) FROM albums AS T1 JOIN artists AS T2 ON T1.artist_id = T2.id WHERE T2.name = "Billy Cobham";	store_1
SELECT company FROM customers WHERE first_name = "Eduardo" AND last_name = "Martins";	store_1
SELECT company FROM customers WHERE first_name = "Eduardo" AND last_name = "Martins";	store_1
SELECT email , phone FROM customers WHERE first_name = "Astrid" AND last_name = "Gruber";	store_1
SELECT email , phone FROM customers WHERE first_name = "Astrid" AND last_name = "Gruber";	store_1
SELECT count(*) FROM customers WHERE city = "Prague";	store_1
SELECT count(*) FROM customers WHERE city = "Prague";	store_1
SELECT count(*) FROM customers WHERE state = "CA";	store_1
SELECT count(*) FROM customers WHERE state = "CA";	store_1
SELECT country FROM customers WHERE first_name = "Roberto" AND last_name = "Almeida";	store_1
SELECT country FROM customers WHERE first_name = "Roberto" AND last_name = "Almeida";	store_1
SELECT T2.title FROM artists AS T1 JOIN albums AS T2 ON T1.id = T2.artist_id WHERE T1.name LIKE '%Led%'	store_1
SELECT T2.title FROM artists AS T1 JOIN albums AS T2 ON T1.id = T2.artist_id WHERE T1.name LIKE '%Led%'	store_1
SELECT count(*) FROM employees AS T1 JOIN customers AS T2 ON T2.support_rep_id = T1.id WHERE T1.first_name = "Steve" AND T1.last_name = "Johnson";	store_1
SELECT count(*) FROM employees AS T1 JOIN customers AS T2 ON T2.support_rep_id = T1.id WHERE T1.first_name = "Steve" AND T1.last_name = "Johnson";	store_1
SELECT title , phone , hire_date FROM employees WHERE first_name = "Nancy" AND last_name = "Edwards";	store_1
SELECT title , phone , hire_date FROM employees WHERE first_name = "Nancy" AND last_name = "Edwards";	store_1
SELECT T2.first_name , T2.last_name FROM employees AS T1 JOIN employees AS T2 ON T1.id = T2.reports_to WHERE T1.first_name = "Nancy" AND T1.last_name = "Edwards";	store_1
SELECT T2.first_name , T2.last_name FROM employees AS T1 JOIN employees AS T2 ON T1.id = T2.reports_to WHERE T1.first_name = "Nancy" AND T1.last_name = "Edwards";	store_1
SELECT address FROM employees WHERE first_name = "Nancy" AND last_name = "Edwards";	store_1
SELECT address FROM employees WHERE first_name = "Nancy" AND last_name = "Edwards";	store_1
SELECT T1.first_name , T1.last_name FROM employees AS T1 JOIN customers AS T2 ON T1.id = T2.support_rep_id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1	store_1
SELECT T1.first_name , T1.last_name FROM employees AS T1 JOIN customers AS T2 ON T1.id = T2.support_rep_id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1	store_1
SELECT count(*) FROM employees WHERE country = "Canada";	store_1
SELECT count(*) FROM employees WHERE country = "Canada";	store_1
SELECT phone FROM employees WHERE first_name = "Nancy" AND last_name = "Edwards";	store_1
SELECT phone FROM employees WHERE first_name = "Nancy" AND last_name = "Edwards";	store_1
SELECT first_name , last_name FROM employees ORDER BY birth_date DESC LIMIT 1;	store_1
SELECT first_name , last_name FROM employees ORDER BY birth_date DESC LIMIT 1;	store_1
SELECT first_name , last_name FROM employees ORDER BY hire_date ASC LIMIT 10;	store_1
SELECT first_name , last_name FROM employees ORDER BY hire_date ASC LIMIT 10;	store_1
SELECT count(*) , city FROM employees WHERE title = 'IT Staff' GROUP BY city	store_1
SELECT count(*) , city FROM employees WHERE title = 'IT Staff' GROUP BY city	store_1
SELECT T2.first_name , T2.last_name , count(T1.reports_to) FROM employees AS T1 JOIN employees AS T2 ON T1.reports_to = T2.id GROUP BY T1.reports_to ORDER BY count(T1.reports_to) DESC LIMIT 1;	store_1
SELECT T2.first_name , T2.last_name , count(T1.reports_to) FROM employees AS T1 JOIN employees AS T2 ON T1.reports_to = T2.id GROUP BY T1.reports_to ORDER BY count(T1.reports_to) DESC LIMIT 1;	store_1
SELECT count(*) FROM customers AS T1 JOIN invoices AS T2 ON T1.id = T2.customer_id WHERE T1.first_name = "Lucas" AND T1.last_name = "Mancini";	store_1
SELECT count(*) FROM customers AS T1 JOIN invoices AS T2 ON T1.id = T2.customer_id WHERE T1.first_name = "Lucas" AND T1.last_name = "Mancini";	store_1
SELECT sum(T2.total) FROM customers AS T1 JOIN invoices AS T2 ON T1.id = T2.customer_id WHERE T1.first_name = "Lucas" AND T1.last_name = "Mancini";	store_1
SELECT sum(T2.total) FROM customers AS T1 JOIN invoices AS T2 ON T1.id = T2.customer_id WHERE T1.first_name = "Lucas" AND T1.last_name = "Mancini";	store_1
SELECT name FROM media_types;	store_1
SELECT name FROM media_types;	store_1
SELECT DISTINCT name FROM genres;	store_1
SELECT DISTINCT name FROM genres;	store_1
SELECT name FROM playlists;	store_1
SELECT name FROM playlists;	store_1
SELECT composer FROM tracks WHERE name = "Fast As a Shark";	store_1
SELECT composer FROM tracks WHERE name = "Fast As a Shark";	store_1
SELECT milliseconds FROM tracks WHERE name = "Fast As a Shark";	store_1
SELECT milliseconds FROM tracks WHERE name = "Fast As a Shark";	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T1.name = "Rock";	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T1.name = "Rock";	store_1
SELECT T1.title FROM albums AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T2.name = "Balls to the Wall";	store_1
SELECT T1.title FROM albums AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T2.name = "Balls to the Wall";	store_1
SELECT T2.name FROM albums AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T1.title = "Balls to the Wall";	store_1
SELECT T2.name FROM albums AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T1.title = "Balls to the Wall";	store_1
SELECT T1.title FROM albums AS T1 JOIN tracks AS T2 ON T1.id = T2.album_id GROUP BY T1.id HAVING count(T1.id) > 10;	store_1
SELECT T1.title FROM albums AS T1 JOIN tracks AS T2 ON T1.id = T2.album_id GROUP BY T1.id HAVING count(T1.id) > 10;	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id JOIN media_types AS T3 ON T3.id = T2.media_type_id WHERE T1.name = "Rock" AND T3.name = "MPEG audio file";	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id JOIN media_types AS T3 ON T3.id = T2.media_type_id WHERE T1.name = "Rock" AND T3.name = "MPEG audio file";	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id JOIN media_types AS T3 ON T3.id = T2.media_type_id WHERE T1.name = "Rock" OR T3.name = "MPEG audio file";	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id JOIN media_types AS T3 ON T3.id = T2.media_type_id WHERE T1.name = "Rock" OR T3.name = "MPEG audio file";	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T1.name = "Rock" OR T1.name = "Jazz"	store_1
SELECT T2.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id WHERE T1.name = "Rock" OR T1.name = "Jazz"	store_1
SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T3.id = T2.playlist_id WHERE T3.name = "Movies";	store_1
SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T3.id = T2.playlist_id WHERE T3.name = "Movies";	store_1
SELECT T2.name FROM playlist_tracks AS T1 JOIN playlists AS T2 ON T2.id = T1.playlist_id GROUP BY T1.playlist_id HAVING count(T1.track_id) > 100;	store_1
SELECT T2.name FROM playlist_tracks AS T1 JOIN playlists AS T2 ON T2.id = T1.playlist_id GROUP BY T1.playlist_id HAVING count(T1.track_id) > 100;	store_1
SELECT T1.name FROM tracks AS T1 JOIN invoice_lines AS T2 ON T1.id = T2.track_id JOIN invoices AS T3 ON T3.id = T2.invoice_id JOIN customers AS T4 ON T4.id = T3.customer_id WHERE T4.first_name = "Daan" AND T4.last_name = "Peeters";	store_1
SELECT T1.name FROM tracks AS T1 JOIN invoice_lines AS T2 ON T1.id = T2.track_id JOIN invoices AS T3 ON T3.id = T2.invoice_id JOIN customers AS T4 ON T4.id = T3.customer_id WHERE T4.first_name = "Daan" AND T4.last_name = "Peeters";	store_1
SELECT unit_price FROM tracks WHERE name = "Fast As a Shark";	store_1
SELECT unit_price FROM tracks WHERE name = "Fast As a Shark";	store_1
SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Movies' EXCEPT SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Music'	store_1
SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Movies' EXCEPT SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Music'	store_1
SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Movies' INTERSECT SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Music'	store_1
SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Movies' INTERSECT SELECT T1.name FROM tracks AS T1 JOIN playlist_tracks AS T2 ON T1.id = T2.track_id JOIN playlists AS T3 ON T2.playlist_id = T3.id WHERE T3.name = 'Music'	store_1
SELECT count(*) , T1.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id GROUP BY T1.name;	store_1
SELECT count(*) , T1.name FROM genres AS T1 JOIN tracks AS T2 ON T1.id = T2.genre_id GROUP BY T1.name;	store_1
SELECT count(*) FROM editor	journal_committee
SELECT Name FROM editor ORDER BY Age ASC	journal_committee
SELECT Name , Age FROM editor	journal_committee
SELECT Name FROM editor WHERE Age > 25	journal_committee
SELECT Name FROM editor WHERE Age = 24 OR Age = 25	journal_committee
SELECT Name FROM editor ORDER BY Age ASC LIMIT 1	journal_committee
SELECT Age , COUNT(*) FROM editor GROUP BY Age	journal_committee
SELECT Age FROM editor GROUP BY Age ORDER BY COUNT(*) DESC LIMIT 1	journal_committee
SELECT DISTINCT Theme FROM journal	journal_committee
SELECT T2.Name , T3.Theme FROM journal_committee AS T1 JOIN editor AS T2 ON T1.Editor_ID = T2.Editor_ID JOIN journal AS T3 ON T1.Journal_ID = T3.Journal_ID	journal_committee
SELECT T2.Name , T3.Theme FROM journal_committee AS T1 JOIN editor AS T2 ON T1.Editor_ID = T2.Editor_ID JOIN journal AS T3 ON T1.Journal_ID = T3.Journal_ID	journal_committee
SELECT T2.Name , T2.age , T3.Theme FROM journal_committee AS T1 JOIN editor AS T2 ON T1.Editor_ID = T2.Editor_ID JOIN journal AS T3 ON T1.Journal_ID = T3.Journal_ID ORDER BY T3.Theme ASC	journal_committee
SELECT T2.Name FROM journal_committee AS T1 JOIN editor AS T2 ON T1.Editor_ID = T2.Editor_ID JOIN journal AS T3 ON T1.Journal_ID = T3.Journal_ID WHERE T3.Sales > 3000	journal_committee
SELECT T1.editor_id , T1.Name , COUNT(*) FROM editor AS T1 JOIN journal_committee AS T2 ON T1.Editor_ID = T2.Editor_ID GROUP BY T1.editor_id	journal_committee
SELECT T1.Name FROM editor AS T1 JOIN journal_committee AS T2 ON T1.Editor_ID = T2.Editor_ID GROUP BY T1.Name HAVING COUNT(*) >= 2	journal_committee
SELECT Name FROM editor WHERE editor_id NOT IN (SELECT editor_id FROM journal_committee)	journal_committee
SELECT date , theme , sales FROM journal EXCEPT SELECT T1.date , T1.theme , T1.sales FROM journal AS T1 JOIN journal_committee AS T2 ON T1.journal_ID = T2.journal_ID	journal_committee
SELECT avg(T1.sales) FROM journal AS T1 JOIN journal_committee AS T2 ON T1.journal_ID = T2.journal_ID WHERE T2.work_type = 'Photo'	journal_committee
SELECT count(*) FROM Accounts	customers_card_transactions
SELECT count(*) FROM Accounts	customers_card_transactions
SELECT account_id , customer_id , account_name FROM Accounts	customers_card_transactions
SELECT account_id , customer_id , account_name FROM Accounts	customers_card_transactions
SELECT other_account_details FROM Accounts WHERE account_name = "338"	customers_card_transactions
SELECT other_account_details FROM Accounts WHERE account_name = "338"	customers_card_transactions
SELECT T2.customer_first_name , T2.customer_last_name , T2.customer_phone FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.account_name = "162"	customers_card_transactions
SELECT T2.customer_first_name , T2.customer_last_name , T2.customer_phone FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.account_name = "162"	customers_card_transactions
SELECT count(*) FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Art" AND T2.customer_last_name = "Turcotte"	customers_card_transactions
SELECT count(*) FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Art" AND T2.customer_last_name = "Turcotte"	customers_card_transactions
SELECT customer_id , count(*) FROM Accounts GROUP BY customer_id	customers_card_transactions
SELECT customer_id , count(*) FROM Accounts GROUP BY customer_id	customers_card_transactions
SELECT customer_id , count(*) FROM Accounts GROUP BY customer_id ORDER BY count(*) DESC LIMIT 1	customers_card_transactions
SELECT customer_id , count(*) FROM Accounts GROUP BY customer_id ORDER BY count(*) DESC LIMIT 1	customers_card_transactions
SELECT T2.customer_first_name , T2.customer_last_name , T1.customer_id FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) ASC LIMIT 1	customers_card_transactions
SELECT T2.customer_first_name , T2.customer_last_name , T1.customer_id FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) ASC LIMIT 1	customers_card_transactions
SELECT count(*) FROM Customers WHERE customer_id NOT IN (SELECT customer_id FROM Accounts)	customers_card_transactions
SELECT count(*) FROM Customers WHERE customer_id NOT IN (SELECT customer_id FROM Accounts)	customers_card_transactions
SELECT customer_first_name , customer_last_name FROM Customers EXCEPT SELECT T1.customer_first_name , T1.customer_last_name FROM Customers AS T1 JOIN Accounts AS T2 ON T1.customer_id = T2.customer_id	customers_card_transactions
SELECT customer_first_name , customer_last_name FROM Customers EXCEPT SELECT T1.customer_first_name , T1.customer_last_name FROM Customers AS T1 JOIN Accounts AS T2 ON T1.customer_id = T2.customer_id	customers_card_transactions
SELECT DISTINCT T1.customer_first_name , T1.customer_last_name FROM Customers AS T1 JOIN Accounts AS T2 ON T1.customer_id = T2.customer_id	customers_card_transactions
SELECT DISTINCT T1.customer_first_name , T1.customer_last_name FROM Customers AS T1 JOIN Accounts AS T2 ON T1.customer_id = T2.customer_id	customers_card_transactions
SELECT count(DISTINCT customer_id) FROM Accounts	customers_card_transactions
SELECT count(DISTINCT customer_id) FROM Accounts	customers_card_transactions
SELECT count(*) FROM Customers	customers_card_transactions
SELECT count(*) FROM Customers	customers_card_transactions
SELECT customer_id , customer_first_name , customer_last_name , customer_phone FROM Customers	customers_card_transactions
SELECT customer_id , customer_first_name , customer_last_name , customer_phone FROM Customers	customers_card_transactions
SELECT customer_phone , customer_email FROM Customers WHERE customer_first_name = "Aniyah" AND customer_last_name = "Feest"	customers_card_transactions
SELECT customer_phone , customer_email FROM Customers WHERE customer_first_name = "Aniyah" AND customer_last_name = "Feest"	customers_card_transactions
SELECT count(*) FROM Customers_cards	customers_card_transactions
SELECT count(*) FROM Customers_cards	customers_card_transactions
SELECT card_id , customer_id , card_type_code , card_number FROM Customers_cards	customers_card_transactions
SELECT card_id , customer_id , card_type_code , card_number FROM Customers_cards	customers_card_transactions
SELECT date_valid_from , date_valid_to FROM Customers_cards WHERE card_number = "4560596484842"	customers_card_transactions
SELECT date_valid_from , date_valid_to FROM Customers_cards WHERE card_number = "4560596484842"	customers_card_transactions
SELECT T2.customer_first_name , T2.customer_last_name , T2.customer_phone FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.card_number = "4560596484842"	customers_card_transactions
SELECT T2.customer_first_name , T2.customer_last_name , T2.customer_phone FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.card_number = "4560596484842"	customers_card_transactions
SELECT count(*) FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Art" AND T2.customer_last_name = "Turcotte"	customers_card_transactions
SELECT count(*) FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Art" AND T2.customer_last_name = "Turcotte"	customers_card_transactions
SELECT count(*) FROM Customers_cards WHERE card_type_code = "Debit"	customers_card_transactions
SELECT count(*) FROM Customers_cards WHERE card_type_code = "Debit"	customers_card_transactions
SELECT count(*) FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Blanche" AND T2.customer_last_name = "Huels" AND T1.card_type_code = "Credit"	customers_card_transactions
SELECT count(*) FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Blanche" AND T2.customer_last_name = "Huels" AND T1.card_type_code = "Credit"	customers_card_transactions
SELECT customer_id , count(*) FROM Customers_cards GROUP BY customer_id	customers_card_transactions
SELECT customer_id , count(*) FROM Customers_cards GROUP BY customer_id	customers_card_transactions
SELECT customer_id , count(*) FROM Customers_cards GROUP BY customer_id ORDER BY count(*) DESC LIMIT 1	customers_card_transactions
SELECT customer_id , count(*) FROM Customers_cards GROUP BY customer_id ORDER BY count(*) DESC LIMIT 1	customers_card_transactions
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 2	customers_card_transactions
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 2	customers_card_transactions
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) ASC LIMIT 1	customers_card_transactions
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) ASC LIMIT 1	customers_card_transactions
SELECT card_type_code , count(*) FROM Customers_cards GROUP BY card_type_code	customers_card_transactions
SELECT card_type_code , count(*) FROM Customers_cards GROUP BY card_type_code	customers_card_transactions
SELECT card_type_code FROM Customers_cards GROUP BY card_type_code ORDER BY count(*) DESC LIMIT 1	customers_card_transactions
SELECT card_type_code FROM Customers_cards GROUP BY card_type_code ORDER BY count(*) DESC LIMIT 1	customers_card_transactions
SELECT card_type_code FROM Customers_cards GROUP BY card_type_code HAVING count(*) >= 5	customers_card_transactions
SELECT card_type_code FROM Customers_cards GROUP BY card_type_code HAVING count(*) >= 5	customers_card_transactions
SELECT card_type_code , count(DISTINCT customer_id) FROM Customers_cards GROUP BY card_type_code	customers_card_transactions
SELECT card_type_code , count(DISTINCT customer_id) FROM Customers_cards GROUP BY card_type_code	customers_card_transactions
SELECT customer_id , customer_first_name FROM Customers EXCEPT SELECT T1.customer_id , T2.customer_first_name FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE card_type_code = "Credit"	customers_card_transactions
SELECT customer_id , customer_first_name FROM Customers EXCEPT SELECT T1.customer_id , T2.customer_first_name FROM Customers_cards AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE card_type_code = "Credit"	customers_card_transactions
SELECT DISTINCT card_type_code FROM Customers_Cards	customers_card_transactions
SELECT DISTINCT card_type_code FROM Customers_Cards	customers_card_transactions
SELECT count(DISTINCT card_type_code) FROM Customers_Cards	customers_card_transactions
SELECT count(DISTINCT card_type_code) FROM Customers_Cards	customers_card_transactions
SELECT DISTINCT transaction_type FROM Financial_Transactions	customers_card_transactions
SELECT DISTINCT transaction_type FROM Financial_Transactions	customers_card_transactions
SELECT count(DISTINCT transaction_type) FROM Financial_Transactions	customers_card_transactions
SELECT count(DISTINCT transaction_type) FROM Financial_Transactions	customers_card_transactions
SELECT avg(transaction_amount) , sum(transaction_amount) FROM Financial_transactions	customers_card_transactions
SELECT avg(transaction_amount) , sum(transaction_amount) FROM Financial_transactions	customers_card_transactions
SELECT T2.card_type_code , count(*) FROM Financial_transactions AS T1 JOIN Customers_cards AS T2 ON T1.card_id = T2.card_id GROUP BY T2.card_type_code	customers_card_transactions
SELECT T2.card_type_code , count(*) FROM Financial_transactions AS T1 JOIN Customers_cards AS T2 ON T1.card_id = T2.card_id GROUP BY T2.card_type_code	customers_card_transactions
SELECT transaction_type , count(*) FROM Financial_transactions GROUP BY transaction_type	customers_card_transactions
SELECT transaction_type , count(*) FROM Financial_transactions GROUP BY transaction_type	customers_card_transactions
SELECT transaction_type FROM Financial_transactions GROUP BY transaction_type ORDER BY sum(transaction_amount) DESC LIMIT 1	customers_card_transactions
SELECT transaction_type FROM Financial_transactions GROUP BY transaction_type ORDER BY sum(transaction_amount) DESC LIMIT 1	customers_card_transactions
SELECT account_id , count(*) FROM Financial_transactions GROUP BY account_id	customers_card_transactions
SELECT account_id , count(*) FROM Financial_transactions GROUP BY account_id	customers_card_transactions
SELECT count(*) FROM track	race_track
SELECT count(*) FROM track	race_track
SELECT name , LOCATION FROM track	race_track
SELECT name , LOCATION FROM track	race_track
SELECT name , seating FROM track WHERE year_opened > 2000 ORDER BY seating	race_track
SELECT name , seating FROM track WHERE year_opened > 2000 ORDER BY seating	race_track
SELECT name , LOCATION , seating FROM track ORDER BY year_opened DESC LIMIT 1	race_track
SELECT name , LOCATION , seating FROM track ORDER BY year_opened DESC LIMIT 1	race_track
SELECT min(seating) , max(seating) , avg(seating) FROM track	race_track
SELECT min(seating) , max(seating) , avg(seating) FROM track	race_track
SELECT name , LOCATION , year_opened FROM track WHERE seating > (SELECT avg(seating) FROM track)	race_track
SELECT name , LOCATION , year_opened FROM track WHERE seating > (SELECT avg(seating) FROM track)	race_track
SELECT DISTINCT LOCATION FROM track	race_track
SELECT DISTINCT LOCATION FROM track	race_track
SELECT count(*) FROM race	race_track
SELECT count(*) FROM race	race_track
SELECT DISTINCT CLASS FROM race	race_track
SELECT DISTINCT CLASS FROM race	race_track
SELECT name , CLASS , date FROM race	race_track
SELECT name , CLASS , date FROM race	race_track
SELECT CLASS , count(*) FROM race GROUP BY CLASS	race_track
SELECT CLASS , count(*) FROM race GROUP BY CLASS	race_track
SELECT CLASS FROM race GROUP BY CLASS ORDER BY count(*) DESC LIMIT 1	race_track
SELECT CLASS FROM race GROUP BY CLASS ORDER BY count(*) DESC LIMIT 1	race_track
SELECT CLASS FROM race GROUP BY CLASS HAVING count(*) >= 2	race_track
SELECT CLASS FROM race GROUP BY CLASS HAVING count(*) >= 2	race_track
SELECT name FROM track EXCEPT SELECT T2.name FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id WHERE T1.class = 'GT'	race_track
SELECT name FROM track EXCEPT SELECT T2.name FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id WHERE T1.class = 'GT'	race_track
SELECT name FROM track WHERE track_id NOT IN (SELECT track_id FROM race)	race_track
SELECT name FROM track WHERE track_id NOT IN (SELECT track_id FROM race)	race_track
SELECT year_opened FROM track WHERE seating BETWEEN 4000 AND 5000	race_track
SELECT year_opened FROM track WHERE seating BETWEEN 4000 AND 5000	race_track
SELECT T2.name , count(*) FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id GROUP BY T1.track_id	race_track
SELECT T2.name , count(*) FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id GROUP BY T1.track_id	race_track
SELECT T2.name FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id GROUP BY T1.track_id ORDER BY count(*) DESC LIMIT 1	race_track
SELECT T2.name FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id GROUP BY T1.track_id ORDER BY count(*) DESC LIMIT 1	race_track
SELECT T1.name , T1.date , T2.name FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id	race_track
SELECT T1.name , T1.date , T2.name FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id	race_track
SELECT T2.name , T2.location FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id GROUP BY T1.track_id HAVING count(*) = 1	race_track
SELECT T2.name , T2.location FROM race AS T1 JOIN track AS T2 ON T1.track_id = T2.track_id GROUP BY T1.track_id HAVING count(*) = 1	race_track
SELECT LOCATION FROM track WHERE seating > 90000 INTERSECT SELECT LOCATION FROM track WHERE seating < 70000	race_track
SELECT LOCATION FROM track WHERE seating > 90000 INTERSECT SELECT LOCATION FROM track WHERE seating < 70000	race_track
SELECT customer_phone FROM available_policies	insurance_fnol
SELECT customer_phone FROM available_policies	insurance_fnol
SELECT customer_phone FROM available_policies WHERE policy_type_code = "Life Insurance"	insurance_fnol
SELECT customer_phone FROM available_policies WHERE policy_type_code = "Life Insurance"	insurance_fnol
SELECT policy_type_code FROM available_policies GROUP BY policy_type_code ORDER BY count(*) DESC LIMIT 1	insurance_fnol
SELECT policy_type_code FROM available_policies GROUP BY policy_type_code ORDER BY count(*) DESC LIMIT 1	insurance_fnol
SELECT customer_phone FROM available_policies WHERE policy_type_code = (SELECT policy_type_code FROM available_policies GROUP BY policy_type_code ORDER BY count(*) DESC LIMIT 1)	insurance_fnol
SELECT customer_phone FROM available_policies WHERE policy_type_code = (SELECT policy_type_code FROM available_policies GROUP BY policy_type_code ORDER BY count(*) DESC LIMIT 1)	insurance_fnol
SELECT policy_type_code FROM available_policies GROUP BY policy_type_code HAVING count(*) > 4	insurance_fnol
SELECT policy_type_code FROM available_policies GROUP BY policy_type_code HAVING count(*) > 4	insurance_fnol
SELECT sum(settlement_amount) , avg(settlement_amount) FROM settlements	insurance_fnol
SELECT sum(settlement_amount) , avg(settlement_amount) FROM settlements	insurance_fnol
SELECT t2.service_name FROM first_notification_of_loss AS t1 JOIN services AS t2 ON t1.service_id = t2.service_id GROUP BY t1.service_id HAVING count(*) > 2	insurance_fnol
SELECT t2.service_name FROM first_notification_of_loss AS t1 JOIN services AS t2 ON t1.service_id = t2.service_id GROUP BY t1.service_id HAVING count(*) > 2	insurance_fnol
SELECT t1.Effective_Date FROM claims AS t1 JOIN settlements AS t2 ON t1.claim_id = t2.claim_id GROUP BY t1.claim_id ORDER BY sum(t2.settlement_amount) DESC LIMIT 1	insurance_fnol
SELECT t1.Effective_Date FROM claims AS t1 JOIN settlements AS t2 ON t1.claim_id = t2.claim_id GROUP BY t1.claim_id ORDER BY sum(t2.settlement_amount) DESC LIMIT 1	insurance_fnol
SELECT count(*) FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name = "Dayana Robel"	insurance_fnol
SELECT count(*) FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name = "Dayana Robel"	insurance_fnol
SELECT t1.customer_name FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id GROUP BY t1.customer_name ORDER BY count(*) DESC LIMIT 1	insurance_fnol
SELECT t1.customer_name FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id GROUP BY t1.customer_name ORDER BY count(*) DESC LIMIT 1	insurance_fnol
SELECT DISTINCT t3.policy_type_code FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id JOIN available_policies AS t3 ON t2.policy_id = t3.policy_id WHERE t1.customer_name = "Dayana Robel"	insurance_fnol
SELECT DISTINCT t3.policy_type_code FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id JOIN available_policies AS t3 ON t2.policy_id = t3.policy_id WHERE t1.customer_name = "Dayana Robel"	insurance_fnol
SELECT DISTINCT t3.policy_type_code FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id JOIN available_policies AS t3 ON t2.policy_id = t3.policy_id WHERE t1.customer_name = (SELECT t1.customer_name FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id GROUP BY t1.customer_name ORDER BY count(*) DESC LIMIT 1)	insurance_fnol
SELECT DISTINCT t3.policy_type_code FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id JOIN available_policies AS t3 ON t2.policy_id = t3.policy_id WHERE t1.customer_name = (SELECT t1.customer_name FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id GROUP BY t1.customer_name ORDER BY count(*) DESC LIMIT 1)	insurance_fnol
SELECT service_name FROM services ORDER BY service_name	insurance_fnol
SELECT service_name FROM services ORDER BY service_name	insurance_fnol
SELECT count(*) FROM services	insurance_fnol
SELECT count(*) FROM services	insurance_fnol
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id	insurance_fnol
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id	insurance_fnol
SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id JOIN services AS t3 ON t2.service_id = t3.service_id WHERE t3.service_name = "Close a policy" OR t3.service_name = "Upgrade a policy"	insurance_fnol
SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id JOIN services AS t3 ON t2.service_id = t3.service_id WHERE t3.service_name = "Close a policy" OR t3.service_name = "Upgrade a policy"	insurance_fnol
SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id JOIN services AS t3 ON t2.service_id = t3.service_id WHERE t3.service_name = "Close a policy" INTERSECT SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id JOIN services AS t3 ON t2.service_id = t3.service_id WHERE t3.service_name = "New policy application"	insurance_fnol
SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id JOIN services AS t3 ON t2.service_id = t3.service_id WHERE t3.service_name = "Close a policy" INTERSECT SELECT t1.customer_name FROM customers AS t1 JOIN first_notification_of_loss AS t2 ON t1.customer_id = t2.customer_id JOIN services AS t3 ON t2.service_id = t3.service_id WHERE t3.service_name = "New policy application"	insurance_fnol
SELECT customer_id FROM customers WHERE customer_name LIKE "%Diana%"	insurance_fnol
SELECT customer_id FROM customers WHERE customer_name LIKE "%Diana%"	insurance_fnol
SELECT max(settlement_amount) , min(settlement_amount) FROM settlements	insurance_fnol
SELECT max(settlement_amount) , min(settlement_amount) FROM settlements	insurance_fnol
SELECT customer_id , customer_name FROM customers ORDER BY customer_id ASC	insurance_fnol
SELECT customer_id , customer_name FROM customers ORDER BY customer_id ASC	insurance_fnol
SELECT t2.date_opened , t2.date_closed FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name LIKE "%Diana%"	insurance_fnol
SELECT t2.date_opened , t2.date_closed FROM customers AS t1 JOIN customers_policies AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name LIKE "%Diana%"	insurance_fnol
SELECT count(*) FROM enzyme	medicine_enzyme_interaction
SELECT count(*) FROM enzyme	medicine_enzyme_interaction
SELECT name FROM enzyme ORDER BY name DESC	medicine_enzyme_interaction
SELECT name FROM enzyme ORDER BY name DESC	medicine_enzyme_interaction
SELECT name , LOCATION FROM enzyme	medicine_enzyme_interaction
SELECT name , LOCATION FROM enzyme	medicine_enzyme_interaction
SELECT max(OMIM) FROM enzyme	medicine_enzyme_interaction
SELECT max(OMIM) FROM enzyme	medicine_enzyme_interaction
SELECT product , chromosome , porphyria FROM enzyme WHERE LOCATION = 'Cytosol'	medicine_enzyme_interaction
SELECT product , chromosome , porphyria FROM enzyme WHERE LOCATION = 'Cytosol'	medicine_enzyme_interaction
SELECT name FROM enzyme WHERE product != 'Heme'	medicine_enzyme_interaction
SELECT name FROM enzyme WHERE product != 'Heme'	medicine_enzyme_interaction
SELECT name , trade_name FROM medicine WHERE FDA_approved = 'Yes'	medicine_enzyme_interaction
SELECT name , trade_name FROM medicine WHERE FDA_approved = 'Yes'	medicine_enzyme_interaction
SELECT T1.name FROM enzyme AS T1 JOIN medicine_enzyme_interaction AS T2 ON T1.id = T2.enzyme_id JOIN medicine AS T3 ON T2.medicine_id = T3.id WHERE T3.name = 'Amisulpride' AND T2.interaction_type = 'inhibitor'	medicine_enzyme_interaction
SELECT T1.name FROM enzyme AS T1 JOIN medicine_enzyme_interaction AS T2 ON T1.id = T2.enzyme_id JOIN medicine AS T3 ON T2.medicine_id = T3.id WHERE T3.name = 'Amisulpride' AND T2.interaction_type = 'inhibitor'	medicine_enzyme_interaction
SELECT T1.id , T1.Name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id GROUP BY T1.id HAVING count(*) >= 2	medicine_enzyme_interaction
SELECT T1.id , T1.Name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id GROUP BY T1.id HAVING count(*) >= 2	medicine_enzyme_interaction
SELECT T1.id , T1.Name , T1.FDA_approved FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id GROUP BY T1.id ORDER BY count(*) DESC	medicine_enzyme_interaction
SELECT T1.id , T1.Name , T1.FDA_approved FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id GROUP BY T1.id ORDER BY count(*) DESC	medicine_enzyme_interaction
SELECT T1.id , T1.name FROM enzyme AS T1 JOIN medicine_enzyme_interaction AS T2 ON T1.id = T2.enzyme_id WHERE T2.interaction_type = 'activitor' GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1	medicine_enzyme_interaction
SELECT T1.id , T1.name FROM enzyme AS T1 JOIN medicine_enzyme_interaction AS T2 ON T1.id = T2.enzyme_id WHERE T2.interaction_type = 'activitor' GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1	medicine_enzyme_interaction
SELECT T1.interaction_type FROM medicine_enzyme_interaction AS T1 JOIN medicine AS T2 ON T1.medicine_id = T2.id JOIN enzyme AS T3 ON T1.enzyme_id = T3.id WHERE T3.name = 'ALA synthase' AND T2.name = 'Aripiprazole'	medicine_enzyme_interaction
SELECT T1.interaction_type FROM medicine_enzyme_interaction AS T1 JOIN medicine AS T2 ON T1.medicine_id = T2.id JOIN enzyme AS T3 ON T1.enzyme_id = T3.id WHERE T3.name = 'ALA synthase' AND T2.name = 'Aripiprazole'	medicine_enzyme_interaction
SELECT interaction_type , count(*) FROM medicine_enzyme_interaction GROUP BY interaction_type ORDER BY count(*) DESC LIMIT 1	medicine_enzyme_interaction
SELECT interaction_type , count(*) FROM medicine_enzyme_interaction GROUP BY interaction_type ORDER BY count(*) DESC LIMIT 1	medicine_enzyme_interaction
SELECT count(*) FROM medicine WHERE FDA_approved = 'No'	medicine_enzyme_interaction
SELECT count(*) FROM medicine WHERE FDA_approved = 'No'	medicine_enzyme_interaction
SELECT count(*) FROM enzyme WHERE id NOT IN ( SELECT enzyme_id FROM medicine_enzyme_interaction );	medicine_enzyme_interaction
SELECT count(*) FROM enzyme WHERE id NOT IN ( SELECT enzyme_id FROM medicine_enzyme_interaction );	medicine_enzyme_interaction
SELECT T1.id , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id GROUP BY T1.id HAVING COUNT(*) >= 3	medicine_enzyme_interaction
SELECT T1.id , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id GROUP BY T1.id HAVING COUNT(*) >= 3	medicine_enzyme_interaction
SELECT DISTINCT T1.name , T1.location , T1.product FROM enzyme AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.enzyme_id = T1.id WHERE T2.interaction_type = 'inhibitor'	medicine_enzyme_interaction
SELECT DISTINCT T1.name , T1.location , T1.product FROM enzyme AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.enzyme_id = T1.id WHERE T2.interaction_type = 'inhibitor'	medicine_enzyme_interaction
SELECT T1.name , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id WHERE interaction_type = 'inhibitor' INTERSECT SELECT T1.name , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id WHERE interaction_type = 'activitor'	medicine_enzyme_interaction
SELECT T1.name , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id WHERE interaction_type = 'inhibitor' INTERSECT SELECT T1.name , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id WHERE interaction_type = 'activitor'	medicine_enzyme_interaction
SELECT name , trade_name FROM medicine EXCEPT SELECT T1.name , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id JOIN enzyme AS T3 ON T3.id = T2.enzyme_id WHERE T3.product = 'Protoporphyrinogen IX'	medicine_enzyme_interaction
SELECT name , trade_name FROM medicine EXCEPT SELECT T1.name , T1.trade_name FROM medicine AS T1 JOIN medicine_enzyme_interaction AS T2 ON T2.medicine_id = T1.id JOIN enzyme AS T3 ON T3.id = T2.enzyme_id WHERE T3.product = 'Protoporphyrinogen IX'	medicine_enzyme_interaction
SELECT count(DISTINCT FDA_approved) FROM medicine	medicine_enzyme_interaction
SELECT count(DISTINCT FDA_approved) FROM medicine	medicine_enzyme_interaction
SELECT name FROM enzyme WHERE name LIKE "%ALA%"	medicine_enzyme_interaction
SELECT name FROM enzyme WHERE name LIKE "%ALA%"	medicine_enzyme_interaction
SELECT trade_name , count(*) FROM medicine GROUP BY trade_name	medicine_enzyme_interaction
SELECT trade_name , count(*) FROM medicine GROUP BY trade_name	medicine_enzyme_interaction
SELECT school , nickname FROM university ORDER BY founded	university_basketball
SELECT school , nickname FROM university ORDER BY founded	university_basketball
SELECT school , LOCATION FROM university WHERE affiliation = 'Public'	university_basketball
SELECT school , LOCATION FROM university WHERE affiliation = 'Public'	university_basketball
SELECT founded FROM university ORDER BY enrollment DESC LIMIT 1	university_basketball
SELECT founded FROM university ORDER BY enrollment DESC LIMIT 1	university_basketball
SELECT founded FROM university WHERE affiliation != 'Public' ORDER BY founded DESC LIMIT 1	university_basketball
SELECT founded FROM university WHERE affiliation != 'Public' ORDER BY founded DESC LIMIT 1	university_basketball
SELECT count(DISTINCT school_id) FROM basketball_match	university_basketball
SELECT count(DISTINCT school_id) FROM basketball_match	university_basketball
SELECT acc_percent FROM basketball_match ORDER BY acc_percent DESC LIMIT 1	university_basketball
SELECT acc_percent FROM basketball_match ORDER BY acc_percent DESC LIMIT 1	university_basketball
SELECT t1.Primary_conference FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id ORDER BY t2.acc_percent LIMIT 1	university_basketball
SELECT t1.Primary_conference FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id ORDER BY t2.acc_percent LIMIT 1	university_basketball
SELECT t2.team_name , t2.ACC_Regular_Season FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id ORDER BY t1.founded LIMIT 1	university_basketball
SELECT t2.team_name , t2.ACC_Regular_Season FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id ORDER BY t1.founded LIMIT 1	university_basketball
SELECT t2.All_Games , t1.location FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id WHERE team_name = 'Clemson'	university_basketball
SELECT t2.All_Games , t1.location FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id WHERE team_name = 'Clemson'	university_basketball
SELECT avg(enrollment) FROM university WHERE founded < 1850	university_basketball
SELECT avg(enrollment) FROM university WHERE founded < 1850	university_basketball
SELECT enrollment , primary_conference FROM university ORDER BY founded LIMIT 1	university_basketball
SELECT enrollment , primary_conference FROM university ORDER BY founded LIMIT 1	university_basketball
SELECT sum(enrollment) , min(enrollment) FROM university	university_basketball
SELECT sum(enrollment) , min(enrollment) FROM university	university_basketball
SELECT sum(enrollment) , affiliation FROM university GROUP BY affiliation	university_basketball
SELECT sum(enrollment) , affiliation FROM university GROUP BY affiliation	university_basketball
SELECT count(*) FROM university WHERE school_id NOT IN (SELECT school_id FROM basketball_match)	university_basketball
SELECT count(*) FROM university WHERE school_id NOT IN (SELECT school_id FROM basketball_match)	university_basketball
SELECT school FROM university WHERE founded > 1850 OR affiliation = 'Public'	university_basketball
SELECT school FROM university WHERE founded > 1850 OR affiliation = 'Public'	university_basketball
SELECT count(DISTINCT affiliation) FROM university	university_basketball
SELECT count(DISTINCT affiliation) FROM university	university_basketball
SELECT count(*) FROM university WHERE LOCATION LIKE "%NY%"	university_basketball
SELECT count(*) FROM university WHERE LOCATION LIKE "%NY%"	university_basketball
SELECT t2.team_name FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id WHERE enrollment < (SELECT avg(enrollment) FROM university)	university_basketball
SELECT t2.team_name FROM university AS t1 JOIN basketball_match AS t2 ON t1.school_id = t2.school_id WHERE enrollment < (SELECT avg(enrollment) FROM university)	university_basketball
SELECT count(*) , affiliation FROM university WHERE enrollment > 20000 GROUP BY affiliation	university_basketball
SELECT count(*) , affiliation FROM university WHERE enrollment > 20000 GROUP BY affiliation	university_basketball
SELECT sum(Enrollment) , affiliation FROM university WHERE founded > 1850 GROUP BY affiliation	university_basketball
SELECT sum(Enrollment) , affiliation FROM university WHERE founded > 1850 GROUP BY affiliation	university_basketball
SELECT max(Enrollment) FROM university	university_basketball
SELECT max(Enrollment) FROM university	university_basketball
SELECT * FROM basketball_match	university_basketball
SELECT * FROM basketball_match	university_basketball
SELECT team_name FROM basketball_match ORDER BY All_Home DESC	university_basketball
SELECT team_name FROM basketball_match ORDER BY All_Home DESC	university_basketball
SELECT count(*) FROM country	match_season
SELECT count(*) FROM country	match_season
SELECT Country_name , Capital FROM country	match_season
SELECT Country_name , Capital FROM country	match_season
SELECT Official_native_language FROM country WHERE Official_native_language LIKE "%English%"	match_season
SELECT Official_native_language FROM country WHERE Official_native_language LIKE "%English%"	match_season
SELECT DISTINCT POSITION FROM match_season	match_season
SELECT DISTINCT POSITION FROM match_season	match_season
SELECT Player FROM match_season WHERE College = "UCLA"	match_season
SELECT Player FROM match_season WHERE College = "UCLA"	match_season
SELECT DISTINCT POSITION FROM match_season WHERE College = "UCLA" OR College = "Duke"	match_season
SELECT DISTINCT POSITION FROM match_season WHERE College = "UCLA" OR College = "Duke"	match_season
SELECT Draft_Pick_Number , Draft_Class FROM match_season WHERE POSITION = "Defender"	match_season
SELECT Draft_Pick_Number , Draft_Class FROM match_season WHERE POSITION = "Defender"	match_season
SELECT count(DISTINCT Team) FROM match_season	match_season
SELECT count(DISTINCT Team) FROM match_season	match_season
SELECT Player , Years_Played FROM player	match_season
SELECT Player , Years_Played FROM player	match_season
SELECT Name FROM Team	match_season
SELECT Name FROM Team	match_season
SELECT T2.Season , T2.Player , T1.Country_name FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country	match_season
SELECT T2.Season , T2.Player , T1.Country_name FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country	match_season
SELECT T2.Player FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T1.Country_name = "Indonesia"	match_season
SELECT T2.Player FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T1.Country_name = "Indonesia"	match_season
SELECT DISTINCT T2.Position FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T1.Capital = "Dublin"	match_season
SELECT DISTINCT T2.Position FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T1.Capital = "Dublin"	match_season
SELECT T1.Official_native_language FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.College = "Maryland" OR T2.College = "Duke"	match_season
SELECT T1.Official_native_language FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.College = "Maryland" OR T2.College = "Duke"	match_season
SELECT count(DISTINCT T1.Official_native_language) FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.Position = "Defender"	match_season
SELECT count(DISTINCT T1.Official_native_language) FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.Position = "Defender"	match_season
SELECT T1.Season , T1.Player , T2.Name FROM match_season AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id	match_season
SELECT T1.Season , T1.Player , T2.Name FROM match_season AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id	match_season
SELECT T1.Position FROM match_season AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id WHERE T2.Name = "Ryley Goldner"	match_season
SELECT T1.Position FROM match_season AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id WHERE T2.Name = "Ryley Goldner"	match_season
SELECT count(DISTINCT T1.College) FROM match_season AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id WHERE T2.Name = "Columbus Crew"	match_season
SELECT count(DISTINCT T1.College) FROM match_season AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id WHERE T2.Name = "Columbus Crew"	match_season
SELECT T1.Player , T1.Years_Played FROM player AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id WHERE T2.Name = "Columbus Crew"	match_season
SELECT T1.Player , T1.Years_Played FROM player AS T1 JOIN team AS T2 ON T1.Team = T2.Team_id WHERE T2.Name = "Columbus Crew"	match_season
SELECT POSITION , COUNT(*) FROM match_season GROUP BY POSITION	match_season
SELECT POSITION , COUNT(*) FROM match_season GROUP BY POSITION	match_season
SELECT Country_name , COUNT(*) FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country GROUP BY T1.Country_name	match_season
SELECT Country_name , COUNT(*) FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country GROUP BY T1.Country_name	match_season
SELECT player FROM match_season ORDER BY College ASC	match_season
SELECT player FROM match_season ORDER BY College ASC	match_season
SELECT POSITION FROM match_season GROUP BY POSITION ORDER BY count(*) DESC LIMIT 1	match_season
SELECT POSITION FROM match_season GROUP BY POSITION ORDER BY count(*) DESC LIMIT 1	match_season
SELECT College FROM match_season GROUP BY College ORDER BY count(*) DESC LIMIT 3	match_season
SELECT College FROM match_season GROUP BY College ORDER BY count(*) DESC LIMIT 3	match_season
SELECT College FROM match_season GROUP BY College HAVING count(*) >= 2	match_season
SELECT College FROM match_season GROUP BY College HAVING count(*) >= 2	match_season
SELECT College FROM match_season GROUP BY College HAVING count(*) >= 2 ORDER BY College DESC	match_season
SELECT College FROM match_season GROUP BY College HAVING count(*) >= 2 ORDER BY College DESC	match_season
SELECT Name FROM team WHERE Team_id NOT IN (SELECT Team FROM match_season)	match_season
SELECT Name FROM team WHERE Team_id NOT IN (SELECT Team FROM match_season)	match_season
SELECT T1.Country_name FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.Position = "Forward" INTERSECT SELECT T1.Country_name FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.Position = "Defender"	match_season
SELECT T1.Country_name FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.Position = "Forward" INTERSECT SELECT T1.Country_name FROM country AS T1 JOIN match_season AS T2 ON T1.Country_id = T2.Country WHERE T2.Position = "Defender"	match_season
SELECT College FROM match_season WHERE POSITION = "Midfielder" INTERSECT SELECT College FROM match_season WHERE POSITION = "Defender"	match_season
SELECT College FROM match_season WHERE POSITION = "Midfielder" INTERSECT SELECT College FROM match_season WHERE POSITION = "Defender"	match_season
SELECT count(*) FROM climber	climbing
SELECT count(*) FROM climber	climbing
SELECT Name FROM climber ORDER BY Points DESC	climbing
SELECT Name FROM climber ORDER BY Points DESC	climbing
SELECT Name FROM climber WHERE Country != "Switzerland"	climbing
SELECT Name FROM climber WHERE Country != "Switzerland"	climbing
SELECT max(Points) FROM climber WHERE Country = "United Kingdom"	climbing
SELECT max(Points) FROM climber WHERE Country = "United Kingdom"	climbing
SELECT COUNT(DISTINCT Country) FROM climber	climbing
SELECT COUNT(DISTINCT Country) FROM climber	climbing
SELECT Name FROM mountain ORDER BY Name ASC	climbing
SELECT Name FROM mountain ORDER BY Name ASC	climbing
SELECT Country FROM mountain WHERE Height > 5000	climbing
SELECT Country FROM mountain WHERE Height > 5000	climbing
SELECT Name FROM mountain ORDER BY Height DESC LIMIT 1	climbing
SELECT Name FROM mountain ORDER BY Height DESC LIMIT 1	climbing
SELECT DISTINCT Range FROM mountain ORDER BY Prominence DESC LIMIT 3	climbing
SELECT DISTINCT Range FROM mountain ORDER BY Prominence DESC LIMIT 3	climbing
SELECT T1.Name , T2.Name FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID	climbing
SELECT T1.Name , T2.Name FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID	climbing
SELECT T1.Name , T2.Height FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID	climbing
SELECT T1.Name , T2.Height FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID	climbing
SELECT T2.Height FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID ORDER BY T1.Points DESC LIMIT 1	climbing
SELECT T2.Height FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID ORDER BY T1.Points DESC LIMIT 1	climbing
SELECT DISTINCT T2.Name FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID WHERE T1.Country = "West Germany"	climbing
SELECT DISTINCT T2.Name FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID WHERE T1.Country = "West Germany"	climbing
SELECT T1.Time FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID WHERE T2.Country = "Uganda"	climbing
SELECT T1.Time FROM climber AS T1 JOIN mountain AS T2 ON T1.Mountain_ID = T2.Mountain_ID WHERE T2.Country = "Uganda"	climbing
SELECT Country , COUNT(*) FROM climber GROUP BY Country	climbing
SELECT Country , COUNT(*) FROM climber GROUP BY Country	climbing
SELECT Country FROM mountain GROUP BY Country HAVING COUNT(*) > 1	climbing
SELECT Country FROM mountain GROUP BY Country HAVING COUNT(*) > 1	climbing
SELECT Name FROM mountain WHERE Mountain_ID NOT IN (SELECT Mountain_ID FROM climber)	climbing
SELECT Name FROM mountain WHERE Mountain_ID NOT IN (SELECT Mountain_ID FROM climber)	climbing
SELECT Country FROM mountain WHERE Height > 5600 INTERSECT SELECT Country FROM mountain WHERE Height < 5200	climbing
SELECT Country FROM mountain WHERE Height > 5600 INTERSECT SELECT Country FROM mountain WHERE Height < 5200	climbing
SELECT Range FROM mountain GROUP BY Range ORDER BY COUNT(*) DESC LIMIT 1	climbing
SELECT Range FROM mountain GROUP BY Range ORDER BY COUNT(*) DESC LIMIT 1	climbing
SELECT Name FROM mountain WHERE Height > 5000 OR Prominence > 1000	climbing
SELECT Name FROM mountain WHERE Height > 5000 OR Prominence > 1000	climbing
SELECT count(*) FROM body_builder	body_builder
SELECT Total FROM body_builder ORDER BY Total ASC	body_builder
SELECT Snatch , Clean_Jerk FROM body_builder ORDER BY Snatch ASC	body_builder
SELECT avg(Snatch) FROM body_builder	body_builder
SELECT Clean_Jerk FROM body_builder ORDER BY Total DESC LIMIT 1	body_builder
SELECT Birth_Date FROM People ORDER BY Height ASC	body_builder
SELECT T2.Name FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID	body_builder
SELECT T2.Name FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Total > 300	body_builder
SELECT T2.Name FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Weight DESC LIMIT 1	body_builder
SELECT T2.Birth_Date , T2.Birth_Place FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Total DESC LIMIT 1	body_builder
SELECT T2.Height FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Total < 315	body_builder
SELECT avg(T1.Total) FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Height > 200	body_builder
SELECT T2.Name FROM body_builder AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Total DESC	body_builder
SELECT Birth_Place , COUNT(*) FROM people GROUP BY Birth_Place	body_builder
SELECT Birth_Place FROM people GROUP BY Birth_Place ORDER BY COUNT(*) DESC LIMIT 1	body_builder
SELECT Birth_Place FROM people GROUP BY Birth_Place HAVING COUNT(*) >= 2	body_builder
SELECT Height , Weight FROM people ORDER BY Height DESC	body_builder
SELECT * FROM body_builder	body_builder
SELECT Name , birth_place FROM people EXCEPT SELECT T1.Name , T1.birth_place FROM people AS T1 JOIN body_builder AS T2 ON T1.people_id = T2.people_id	body_builder
SELECT count(DISTINCT Birth_Place) FROM people	body_builder
SELECT count(*) FROM people WHERE people_id NOT IN (SELECT People_ID FROM body_builder)	body_builder
SELECT T2.weight FROM body_builder AS T1 JOIN people AS T2 ON T1.people_id = T2.people_id WHERE T1.snatch > 140 OR T2.height > 200;	body_builder
SELECT T1.total FROM body_builder AS T1 JOIN people AS T2 ON T1.people_id = T2.people_id WHERE T2.Birth_Date LIKE "%January%";	body_builder
SELECT min(snatch) FROM body_builder	body_builder
SELECT count(*) FROM election	election_representative
SELECT Votes FROM election ORDER BY Votes DESC	election_representative
SELECT Date , Vote_Percent FROM election	election_representative
SELECT min(Vote_Percent) , max(Vote_Percent) FROM election	election_representative
SELECT Name , Party FROM representative	election_representative
SELECT Name FROM Representative WHERE Party != "Republican"	election_representative
SELECT Lifespan FROM representative WHERE State = "New York" OR State = "Indiana"	election_representative
SELECT T2.Name , T1.Date FROM election AS T1 JOIN representative AS T2 ON T1.Representative_ID = T2.Representative_ID	election_representative
SELECT T2.Name FROM election AS T1 JOIN representative AS T2 ON T1.Representative_ID = T2.Representative_ID WHERE Votes > 10000	election_representative
SELECT T2.Name FROM election AS T1 JOIN representative AS T2 ON T1.Representative_ID = T2.Representative_ID ORDER BY votes DESC	election_representative
SELECT T2.Party FROM election AS T1 JOIN representative AS T2 ON T1.Representative_ID = T2.Representative_ID ORDER BY votes ASC LIMIT 1	election_representative
SELECT T2.Lifespan FROM election AS T1 JOIN representative AS T2 ON T1.Representative_ID = T2.Representative_ID ORDER BY Vote_Percent DESC	election_representative
SELECT avg(T1.Votes) FROM election AS T1 JOIN representative AS T2 ON T1.Representative_ID = T2.Representative_ID WHERE T2.Party = "Republican"	election_representative
SELECT Party , COUNT(*) FROM representative GROUP BY Party	election_representative
SELECT Party , COUNT(*) FROM representative GROUP BY Party ORDER BY COUNT(*) DESC LIMIT 1	election_representative
SELECT Party FROM representative GROUP BY Party HAVING COUNT(*) >= 3	election_representative
SELECT State FROM representative GROUP BY State HAVING COUNT(*) >= 2	election_representative
SELECT Name FROM representative WHERE Representative_ID NOT IN (SELECT Representative_ID FROM election)	election_representative
SELECT Party FROM representative WHERE State = "New York" INTERSECT SELECT Party FROM representative WHERE State = "Pennsylvania"	election_representative
SELECT count(DISTINCT Party) FROM representative	election_representative
SELECT count(*) FROM Apartment_Bookings	apartment_rentals
SELECT count(*) FROM Apartment_Bookings	apartment_rentals
SELECT booking_start_date , booking_end_date FROM Apartment_Bookings	apartment_rentals
SELECT booking_start_date , booking_end_date FROM Apartment_Bookings	apartment_rentals
SELECT DISTINCT building_description FROM Apartment_Buildings	apartment_rentals
SELECT DISTINCT building_description FROM Apartment_Buildings	apartment_rentals
SELECT building_short_name FROM Apartment_Buildings WHERE building_manager = "Emma"	apartment_rentals
SELECT building_short_name FROM Apartment_Buildings WHERE building_manager = "Emma"	apartment_rentals
SELECT building_address , building_phone FROM Apartment_Buildings WHERE building_manager = "Brenden"	apartment_rentals
SELECT building_address , building_phone FROM Apartment_Buildings WHERE building_manager = "Brenden"	apartment_rentals
SELECT building_full_name FROM Apartment_Buildings WHERE building_full_name LIKE "%court%"	apartment_rentals
SELECT building_full_name FROM Apartment_Buildings WHERE building_full_name LIKE "%court%"	apartment_rentals
SELECT min(bathroom_count) , max(bathroom_count) FROM Apartments	apartment_rentals
SELECT min(bathroom_count) , max(bathroom_count) FROM Apartments	apartment_rentals
SELECT avg(bedroom_count) FROM Apartments	apartment_rentals
SELECT avg(bedroom_count) FROM Apartments	apartment_rentals
SELECT apt_number , room_count FROM Apartments	apartment_rentals
SELECT apt_number , room_count FROM Apartments	apartment_rentals
SELECT avg(room_count) FROM Apartments WHERE apt_type_code = "Studio"	apartment_rentals
SELECT avg(room_count) FROM Apartments WHERE apt_type_code = "Studio"	apartment_rentals
SELECT apt_number FROM Apartments WHERE apt_type_code = "Flat"	apartment_rentals
SELECT apt_number FROM Apartments WHERE apt_type_code = "Flat"	apartment_rentals
SELECT guest_first_name , guest_last_name FROM Guests	apartment_rentals
SELECT guest_first_name , guest_last_name FROM Guests	apartment_rentals
SELECT date_of_birth FROM Guests WHERE gender_code = "Male"	apartment_rentals
SELECT date_of_birth FROM Guests WHERE gender_code = "Male"	apartment_rentals
SELECT T2.apt_number , T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id	apartment_rentals
SELECT T2.apt_number , T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id	apartment_rentals
SELECT T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.apt_type_code = "Duplex"	apartment_rentals
SELECT T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.apt_type_code = "Duplex"	apartment_rentals
SELECT T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.bedroom_count > 2	apartment_rentals
SELECT T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.bedroom_count > 2	apartment_rentals
SELECT T1.booking_status_code FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.apt_number = "Suite 634"	apartment_rentals
SELECT T1.booking_status_code FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.apt_number = "Suite 634"	apartment_rentals
SELECT DISTINCT T2.apt_number FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Confirmed"	apartment_rentals
SELECT DISTINCT T2.apt_number FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Confirmed"	apartment_rentals
SELECT avg(room_count) FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Provisional"	apartment_rentals
SELECT avg(room_count) FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Provisional"	apartment_rentals
SELECT T2.guest_first_name , T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Guests AS T2 ON T1.guest_id = T2.guest_id	apartment_rentals
SELECT T2.guest_first_name , T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Guests AS T2 ON T1.guest_id = T2.guest_id	apartment_rentals
SELECT T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Guests AS T2 ON T1.guest_id = T2.guest_id WHERE T2.gender_code = "Female"	apartment_rentals
SELECT T1.booking_start_date , T1.booking_start_date FROM Apartment_Bookings AS T1 JOIN Guests AS T2 ON T1.guest_id = T2.guest_id WHERE T2.gender_code = "Female"	apartment_rentals
SELECT T2.guest_first_name , T2.guest_last_name FROM Apartment_Bookings AS T1 JOIN Guests AS T2 ON T1.guest_id = T2.guest_id WHERE T1.booking_status_code = "Confirmed"	apartment_rentals
SELECT T2.guest_first_name , T2.guest_last_name FROM Apartment_Bookings AS T1 JOIN Guests AS T2 ON T1.guest_id = T2.guest_id WHERE T1.booking_status_code = "Confirmed"	apartment_rentals
SELECT T1.facility_code FROM Apartment_Facilities AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.bedroom_count > 4	apartment_rentals
SELECT T1.facility_code FROM Apartment_Facilities AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T2.bedroom_count > 4	apartment_rentals
SELECT sum(T2.room_count) FROM Apartment_Facilities AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.facility_code = "Gym"	apartment_rentals
SELECT sum(T2.room_count) FROM Apartment_Facilities AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.facility_code = "Gym"	apartment_rentals
SELECT sum(T2.room_count) FROM Apartment_Buildings AS T1 JOIN Apartments AS T2 ON T1.building_id = T2.building_id WHERE T1.building_short_name = "Columbus Square"	apartment_rentals
SELECT sum(T2.room_count) FROM Apartment_Buildings AS T1 JOIN Apartments AS T2 ON T1.building_id = T2.building_id WHERE T1.building_short_name = "Columbus Square"	apartment_rentals
SELECT T1.building_address FROM Apartment_Buildings AS T1 JOIN Apartments AS T2 ON T1.building_id = T2.building_id WHERE T2.bathroom_count > 2	apartment_rentals
SELECT T1.building_address FROM Apartment_Buildings AS T1 JOIN Apartments AS T2 ON T1.building_id = T2.building_id WHERE T2.bathroom_count > 2	apartment_rentals
SELECT T2.apt_type_code , T2.apt_number FROM Apartment_Buildings AS T1 JOIN Apartments AS T2 ON T1.building_id = T2.building_id WHERE T1.building_manager = "Kyle"	apartment_rentals
SELECT T2.apt_type_code , T2.apt_number FROM Apartment_Buildings AS T1 JOIN Apartments AS T2 ON T1.building_id = T2.building_id WHERE T1.building_manager = "Kyle"	apartment_rentals
SELECT booking_status_code , COUNT(*) FROM Apartment_Bookings GROUP BY booking_status_code	apartment_rentals
SELECT booking_status_code , COUNT(*) FROM Apartment_Bookings GROUP BY booking_status_code	apartment_rentals
SELECT apt_number FROM Apartments ORDER BY room_count ASC	apartment_rentals
SELECT apt_number FROM Apartments ORDER BY room_count ASC	apartment_rentals
SELECT apt_number FROM Apartments ORDER BY bedroom_count DESC LIMIT 1	apartment_rentals
SELECT apt_number FROM Apartments ORDER BY bedroom_count DESC LIMIT 1	apartment_rentals
SELECT apt_type_code , COUNT(*) FROM Apartments GROUP BY apt_type_code ORDER BY COUNT(*) ASC	apartment_rentals
SELECT apt_type_code , COUNT(*) FROM Apartments GROUP BY apt_type_code ORDER BY COUNT(*) ASC	apartment_rentals
SELECT apt_type_code FROM Apartments GROUP BY apt_type_code ORDER BY avg(room_count) DESC LIMIT 3	apartment_rentals
SELECT apt_type_code FROM Apartments GROUP BY apt_type_code ORDER BY avg(room_count) DESC LIMIT 3	apartment_rentals
SELECT apt_type_code , bathroom_count , bedroom_count FROM Apartments GROUP BY apt_type_code ORDER BY sum(room_count) DESC LIMIT 1	apartment_rentals
SELECT apt_type_code , bathroom_count , bedroom_count FROM Apartments GROUP BY apt_type_code ORDER BY sum(room_count) DESC LIMIT 1	apartment_rentals
SELECT apt_type_code FROM Apartments GROUP BY apt_type_code ORDER BY count(*) DESC LIMIT 1	apartment_rentals
SELECT apt_type_code FROM Apartments GROUP BY apt_type_code ORDER BY count(*) DESC LIMIT 1	apartment_rentals
SELECT apt_type_code FROM Apartments WHERE bathroom_count > 1 GROUP BY apt_type_code ORDER BY count(*) DESC LIMIT 1	apartment_rentals
SELECT apt_type_code FROM Apartments WHERE bathroom_count > 1 GROUP BY apt_type_code ORDER BY count(*) DESC LIMIT 1	apartment_rentals
SELECT apt_type_code , max(room_count) , min(room_count) FROM Apartments GROUP BY apt_type_code	apartment_rentals
SELECT apt_type_code , max(room_count) , min(room_count) FROM Apartments GROUP BY apt_type_code	apartment_rentals
SELECT gender_code , COUNT(*) FROM Guests GROUP BY gender_code ORDER BY COUNT(*) DESC	apartment_rentals
SELECT gender_code , COUNT(*) FROM Guests GROUP BY gender_code ORDER BY COUNT(*) DESC	apartment_rentals
SELECT count(*) FROM Apartments WHERE apt_id NOT IN (SELECT apt_id FROM Apartment_Facilities)	apartment_rentals
SELECT count(*) FROM Apartments WHERE apt_id NOT IN (SELECT apt_id FROM Apartment_Facilities)	apartment_rentals
SELECT T2.apt_number FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Confirmed" INTERSECT SELECT T2.apt_number FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Provisional"	apartment_rentals
SELECT T2.apt_number FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Confirmed" INTERSECT SELECT T2.apt_number FROM Apartment_Bookings AS T1 JOIN Apartments AS T2 ON T1.apt_id = T2.apt_id WHERE T1.booking_status_code = "Provisional"	apartment_rentals
SELECT T1.apt_number FROM Apartments AS T1 JOIN View_Unit_Status AS T2 ON T1.apt_id = T2.apt_id WHERE T2.available_yn = 0 INTERSECT SELECT T1.apt_number FROM Apartments AS T1 JOIN View_Unit_Status AS T2 ON T1.apt_id = T2.apt_id WHERE T2.available_yn = 1	apartment_rentals
SELECT T1.apt_number FROM Apartments AS T1 JOIN View_Unit_Status AS T2 ON T1.apt_id = T2.apt_id WHERE T2.available_yn = 0 INTERSECT SELECT T1.apt_number FROM Apartments AS T1 JOIN View_Unit_Status AS T2 ON T1.apt_id = T2.apt_id WHERE T2.available_yn = 1	apartment_rentals
SELECT T1.name , T2.name FROM Country AS T1 JOIN League AS T2 ON T1.id = T2.country_id	soccer_1
SELECT count(*) FROM Country AS T1 JOIN League AS T2 ON T1.id = T2.country_id WHERE T1.name = "England"	soccer_1
SELECT avg(weight) FROM Player	soccer_1
SELECT max(weight) , min(weight) FROM Player	soccer_1
SELECT DISTINCT T1.player_name FROM Player AS T1 JOIN Player_Attributes AS T2 ON T1.player_api_id = T2.player_api_id WHERE T2.overall_rating > ( SELECT avg(overall_rating) FROM Player_Attributes )	soccer_1
SELECT DISTINCT T1.player_name FROM Player AS T1 JOIN Player_Attributes AS T2 ON T1.player_api_id = T2.player_api_id WHERE T2.dribbling = ( SELECT max(overall_rating) FROM Player_Attributes)	soccer_1
SELECT DISTINCT T1.player_name FROM Player AS T1 JOIN Player_Attributes AS T2 ON T1.player_api_id = T2.player_api_id WHERE T2.crossing > 90 AND T2.preferred_foot = "right"	soccer_1
SELECT DISTINCT T1.player_name FROM Player AS T1 JOIN Player_Attributes AS T2 ON T1.player_api_id = T2.player_api_id WHERE T2.preferred_foot = "left" AND T2.overall_rating >= 85 AND T2.overall_rating <= 90	soccer_1
SELECT preferred_foot , avg(overall_rating) FROM Player_Attributes GROUP BY preferred_foot	soccer_1
SELECT preferred_foot , count(*) FROM Player_Attributes WHERE overall_rating > 80 GROUP BY preferred_foot	soccer_1
SELECT player_api_id FROM Player WHERE height >= 180 INTERSECT SELECT player_api_id FROM Player_Attributes WHERE overall_rating > 85	soccer_1
SELECT player_api_id FROM Player WHERE height >= 180 AND height <= 190 INTERSECT SELECT player_api_id FROM Player_Attributes WHERE preferred_foot = "left"	soccer_1
SELECT DISTINCT T1.player_name FROM Player AS T1 JOIN Player_Attributes AS T2 ON T1.player_api_id = T2.player_api_id ORDER BY overall_rating DESC LIMIT 3	soccer_1
SELECT DISTINCT T1.player_name , T1.birthday FROM Player AS T1 JOIN Player_Attributes AS T2 ON T1.player_api_id = T2.player_api_id ORDER BY potential DESC LIMIT 5	soccer_1
SELECT count(*) FROM performance	performance_attendance
SELECT HOST FROM performance ORDER BY Attendance ASC	performance_attendance
SELECT Date , LOCATION FROM performance	performance_attendance
SELECT Attendance FROM performance WHERE LOCATION = "TD Garden" OR LOCATION = "Bell Centre"	performance_attendance
SELECT avg(Attendance) FROM performance	performance_attendance
SELECT Date FROM performance ORDER BY Attendance DESC LIMIT 1	performance_attendance
SELECT LOCATION , COUNT(*) FROM performance GROUP BY LOCATION	performance_attendance
SELECT LOCATION FROM performance GROUP BY LOCATION ORDER BY COUNT(*) DESC LIMIT 1	performance_attendance
SELECT LOCATION FROM performance GROUP BY LOCATION HAVING COUNT(*) >= 2	performance_attendance
SELECT LOCATION FROM performance WHERE Attendance > 2000 INTERSECT SELECT LOCATION FROM performance WHERE Attendance < 1000	performance_attendance
SELECT T2.Name , T3.Location FROM member_attendance AS T1 JOIN member AS T2 ON T1.Member_ID = T2.Member_ID JOIN performance AS T3 ON T1.Performance_ID = T3.Performance_ID	performance_attendance
SELECT T2.Name , T3.Location FROM member_attendance AS T1 JOIN member AS T2 ON T1.Member_ID = T2.Member_ID JOIN performance AS T3 ON T1.Performance_ID = T3.Performance_ID ORDER BY T2.Name ASC	performance_attendance
SELECT T3.Date FROM member_attendance AS T1 JOIN member AS T2 ON T1.Member_ID = T2.Member_ID JOIN performance AS T3 ON T1.Performance_ID = T3.Performance_ID WHERE T2.Role = "Violin"	performance_attendance
SELECT T2.Name , T3.Date FROM member_attendance AS T1 JOIN member AS T2 ON T1.Member_ID = T2.Member_ID JOIN performance AS T3 ON T1.Performance_ID = T3.Performance_ID ORDER BY T3.Attendance DESC	performance_attendance
SELECT Name FROM member WHERE Member_ID NOT IN (SELECT Member_ID FROM member_attendance)	performance_attendance
SELECT count(*) FROM debate	debate
SELECT Venue FROM debate ORDER BY Num_of_Audience ASC	debate
SELECT Date , Venue FROM debate	debate
SELECT Date FROM debate WHERE Num_of_Audience > 150	debate
SELECT Name FROM people WHERE Age = 35 OR Age = 36	debate
SELECT Party FROM people ORDER BY Age ASC LIMIT 1	debate
SELECT Party , COUNT(*) FROM people GROUP BY Party	debate
SELECT Party FROM people GROUP BY Party ORDER BY COUNT(*) DESC LIMIT 1	debate
SELECT DISTINCT Venue FROM debate	debate
SELECT T3.Name , T2.Date , T2.Venue FROM debate_people AS T1 JOIN debate AS T2 ON T1.Debate_ID = T2.Debate_ID JOIN people AS T3 ON T1.Affirmative = T3.People_ID	debate
SELECT T3.Name , T2.Date , T2.Venue FROM debate_people AS T1 JOIN debate AS T2 ON T1.Debate_ID = T2.Debate_ID JOIN people AS T3 ON T1.Negative = T3.People_ID ORDER BY T3.Name ASC	debate
SELECT T3.Name FROM debate_people AS T1 JOIN debate AS T2 ON T1.Debate_ID = T2.Debate_ID JOIN people AS T3 ON T1.Affirmative = T3.People_ID WHERE T2.Num_of_Audience > 200	debate
SELECT T2.Name , COUNT(*) FROM debate_people AS T1 JOIN people AS T2 ON T1.Affirmative = T2.People_ID GROUP BY T2.Name	debate
SELECT T2.Name FROM debate_people AS T1 JOIN people AS T2 ON T1.Negative = T2.People_ID GROUP BY T2.Name HAVING COUNT(*) >= 2	debate
SELECT Name FROM people WHERE People_id NOT IN (SELECT Affirmative FROM debate_people)	debate
SELECT customer_details FROM customers ORDER BY customer_details	insurance_and_eClaims
SELECT customer_details FROM customers ORDER BY customer_details	insurance_and_eClaims
SELECT policy_type_code FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t2.customer_details = "Dayana Robel"	insurance_and_eClaims
SELECT policy_type_code FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t2.customer_details = "Dayana Robel"	insurance_and_eClaims
SELECT policy_type_code FROM policies GROUP BY policy_type_code ORDER BY count(*) DESC LIMIT 1	insurance_and_eClaims
SELECT policy_type_code FROM policies GROUP BY policy_type_code ORDER BY count(*) DESC LIMIT 1	insurance_and_eClaims
SELECT policy_type_code FROM policies GROUP BY policy_type_code HAVING count(*) > 2	insurance_and_eClaims
SELECT policy_type_code FROM policies GROUP BY policy_type_code HAVING count(*) > 2	insurance_and_eClaims
SELECT sum(amount_piad) , avg(amount_piad) FROM claim_headers	insurance_and_eClaims
SELECT sum(amount_piad) , avg(amount_piad) FROM claim_headers	insurance_and_eClaims
SELECT sum(t1.amount_claimed) FROM claim_headers AS t1 JOIN claims_documents AS t2 ON t1.claim_header_id = t2.claim_id WHERE t2.created_date = (SELECT created_date FROM claims_documents ORDER BY created_date LIMIT 1)	insurance_and_eClaims
SELECT sum(t1.amount_claimed) FROM claim_headers AS t1 JOIN claims_documents AS t2 ON t1.claim_header_id = t2.claim_id WHERE t2.created_date = (SELECT created_date FROM claims_documents ORDER BY created_date LIMIT 1)	insurance_and_eClaims
SELECT t3.customer_details FROM claim_headers AS t1 JOIN policies AS t2 ON t1.policy_id = t2.policy_id JOIN customers AS t3 ON t2.customer_id = t3.customer_id WHERE t1.amount_claimed = (SELECT max(amount_claimed) FROM claim_headers)	insurance_and_eClaims
SELECT t3.customer_details FROM claim_headers AS t1 JOIN policies AS t2 ON t1.policy_id = t2.policy_id JOIN customers AS t3 ON t2.customer_id = t3.customer_id WHERE t1.amount_claimed = (SELECT max(amount_claimed) FROM claim_headers)	insurance_and_eClaims
SELECT t3.customer_details FROM claim_headers AS t1 JOIN policies AS t2 ON t1.policy_id = t2.policy_id JOIN customers AS t3 ON t2.customer_id = t3.customer_id WHERE t1.amount_piad = (SELECT min(amount_piad) FROM claim_headers)	insurance_and_eClaims
SELECT t3.customer_details FROM claim_headers AS t1 JOIN policies AS t2 ON t1.policy_id = t2.policy_id JOIN customers AS t3 ON t2.customer_id = t3.customer_id WHERE t1.amount_piad = (SELECT min(amount_piad) FROM claim_headers)	insurance_and_eClaims
SELECT customer_details FROM customers EXCEPT SELECT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id	insurance_and_eClaims
SELECT customer_details FROM customers EXCEPT SELECT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id	insurance_and_eClaims
SELECT count(*) FROM claims_processing_stages	insurance_and_eClaims
SELECT count(*) FROM claims_processing_stages	insurance_and_eClaims
SELECT t2.claim_status_name FROM claims_processing AS t1 JOIN claims_processing_stages AS t2 ON t1.claim_stage_id = t2.claim_stage_id GROUP BY t1.claim_stage_id ORDER BY count(*) DESC LIMIT 1	insurance_and_eClaims
SELECT t2.claim_status_name FROM claims_processing AS t1 JOIN claims_processing_stages AS t2 ON t1.claim_stage_id = t2.claim_stage_id GROUP BY t1.claim_stage_id ORDER BY count(*) DESC LIMIT 1	insurance_and_eClaims
SELECT customer_details FROM customers WHERE customer_details LIKE "%Diana%"	insurance_and_eClaims
SELECT customer_details FROM customers WHERE customer_details LIKE "%Diana%"	insurance_and_eClaims
SELECT DISTINCT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t1.policy_type_code = "Deputy"	insurance_and_eClaims
SELECT DISTINCT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t1.policy_type_code = "Deputy"	insurance_and_eClaims
SELECT DISTINCT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t1.policy_type_code = "Deputy" OR t1.policy_type_code = "Uniform"	insurance_and_eClaims
SELECT DISTINCT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t1.policy_type_code = "Deputy" OR t1.policy_type_code = "Uniform"	insurance_and_eClaims
SELECT customer_details FROM customers UNION SELECT staff_details FROM staff	insurance_and_eClaims
SELECT customer_details FROM customers UNION SELECT staff_details FROM staff	insurance_and_eClaims
SELECT policy_type_code , count(*) FROM policies GROUP BY policy_type_code	insurance_and_eClaims
SELECT policy_type_code , count(*) FROM policies GROUP BY policy_type_code	insurance_and_eClaims
SELECT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id GROUP BY t2.customer_details ORDER BY count(*) DESC LIMIT 1	insurance_and_eClaims
SELECT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id GROUP BY t2.customer_details ORDER BY count(*) DESC LIMIT 1	insurance_and_eClaims
SELECT claim_status_description FROM claims_processing_stages WHERE claim_status_name = "Open"	insurance_and_eClaims
SELECT claim_status_description FROM claims_processing_stages WHERE claim_status_name = "Open"	insurance_and_eClaims
SELECT count(DISTINCT claim_outcome_code) FROM claims_processing	insurance_and_eClaims
SELECT count(DISTINCT claim_outcome_code) FROM claims_processing	insurance_and_eClaims
SELECT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t1.start_date = (SELECT max(start_date) FROM policies)	insurance_and_eClaims
SELECT t2.customer_details FROM policies AS t1 JOIN customers AS t2 ON t1.customer_id = t2.customer_id WHERE t1.start_date = (SELECT max(start_date) FROM policies)	insurance_and_eClaims
SELECT count(*) FROM Church WHERE Open_Date < 1850	wedding
SELECT name , open_date , organized_by FROM Church	wedding
SELECT name FROM church ORDER BY open_date DESC	wedding
SELECT open_date FROM church GROUP BY open_date HAVING count(*) >= 2	wedding
SELECT organized_by , name FROM church WHERE open_date BETWEEN 1830 AND 1840	wedding
SELECT open_date , count(*) FROM church GROUP BY open_date	wedding
SELECT name , open_date FROM church ORDER BY open_date DESC LIMIT 3	wedding
SELECT count(*) FROM people WHERE is_male = 'F' AND age > 30	wedding
SELECT country FROM people WHERE age < 25 INTERSECT SELECT country FROM people WHERE age > 30	wedding
SELECT min(age) , max(age) , avg(age) FROM people	wedding
SELECT name , country FROM people WHERE age < (SELECT avg(age) FROM people)	wedding
SELECT T2.name , T3.name FROM wedding AS T1 JOIN people AS T2 ON T1.male_id = T2.people_id JOIN people AS T3 ON T1.female_id = T3.people_id WHERE T1.year > 2014	wedding
SELECT name , age FROM people WHERE is_male = 'T' AND people_id NOT IN (SELECT male_id FROM wedding)	wedding
SELECT name FROM church EXCEPT SELECT T1.name FROM church AS T1 JOIN wedding AS T2 ON T1.church_id = T2.church_id WHERE T2.year = 2015	wedding
SELECT T1.name FROM church AS T1 JOIN wedding AS T2 ON T1.church_id = T2.church_id GROUP BY T1.church_id HAVING count(*) >= 2	wedding
SELECT T2.name FROM wedding AS T1 JOIN people AS T2 ON T1.female_id = T2.people_id WHERE T1.year = 2016 AND T2.is_male = 'F' AND T2.country = 'Canada'	wedding
SELECT count(*) FROM wedding WHERE YEAR = 2016	wedding
SELECT T4.name FROM wedding AS T1 JOIN people AS T2 ON T1.male_id = T2.people_id JOIN people AS T3 ON T1.female_id = T3.people_id JOIN church AS T4 ON T4.church_id = T1.church_id WHERE T2.age > 30 OR T3.age > 30	wedding
SELECT country , count(*) FROM people GROUP BY country	wedding
SELECT COUNT (DISTINCT church_id) FROM wedding WHERE YEAR = 2016	wedding
SELECT i_id FROM item WHERE title = "orange"	epinions_1
SELECT * FROM item	epinions_1
SELECT count(*) FROM review	epinions_1
SELECT count(*) FROM useracct	epinions_1
SELECT avg(rating) , max(rating) FROM review	epinions_1
SELECT min(rank) FROM review	epinions_1
SELECT count(DISTINCT u_id) FROM review	epinions_1
SELECT count(DISTINCT i_id) FROM review	epinions_1
SELECT count(*) FROM item WHERE i_id NOT IN (SELECT i_id FROM review)	epinions_1
SELECT name FROM useracct WHERE u_id NOT IN (SELECT u_id FROM review)	epinions_1
SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id WHERE T2.rating = 10	epinions_1
SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id WHERE T2.rating > (SELECT avg(rating) FROM review)	epinions_1
SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id WHERE T2.rating < 5	epinions_1
SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id WHERE T2.rating > 8 INTERSECT SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id WHERE T2.rating < 5	epinions_1
SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id WHERE T2.rank > 3 INTERSECT SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id GROUP BY T2.i_id HAVING avg(T2.rating) > 5	epinions_1
SELECT T1.title FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id GROUP BY T2.i_id ORDER BY avg(T2.rating) LIMIT 1	epinions_1
SELECT title FROM item ORDER BY title	epinions_1
SELECT T1.name FROM useracct AS T1 JOIN review AS T2 ON T1.u_id = T2.u_id GROUP BY T2.u_id ORDER BY count(*) DESC LIMIT 1	epinions_1
SELECT T1.title , T1.i_id FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id GROUP BY T2.i_id ORDER BY avg(T2.rating) DESC LIMIT 1	epinions_1
SELECT T1.title , T1.i_id FROM item AS T1 JOIN review AS T2 ON T1.i_id = T2.i_id GROUP BY T2.i_id ORDER BY avg(T2.rank) DESC LIMIT 1	epinions_1
SELECT T1.name , avg(T2.rating) FROM useracct AS T1 JOIN review AS T2 ON T1.u_id = T2.u_id GROUP BY T2.u_id	epinions_1
SELECT T1.name , count(*) FROM useracct AS T1 JOIN review AS T2 ON T1.u_id = T2.u_id GROUP BY T2.u_id	epinions_1
SELECT T1.name FROM useracct AS T1 JOIN review AS T2 ON T1.u_id = T2.u_id ORDER BY T2.rating DESC LIMIT 1	epinions_1
SELECT T1.name FROM useracct AS T1 JOIN trust AS T2 ON T1.u_id = T2.source_u_id GROUP BY T2.source_u_id ORDER BY avg(trust) DESC LIMIT 1	epinions_1
SELECT T1.name , avg(trust) FROM useracct AS T1 JOIN trust AS T2 ON T1.u_id = T2.target_u_id GROUP BY T2.target_u_id	epinions_1
SELECT T1.name FROM useracct AS T1 JOIN trust AS T2 ON T1.u_id = T2.target_u_id ORDER BY trust LIMIT 1	epinions_1
SELECT title FROM item WHERE i_id NOT IN (SELECT i_id FROM review)	epinions_1
SELECT name FROM useracct WHERE u_id NOT IN (SELECT u_id FROM review)	epinions_1
SELECT count(*) FROM useracct WHERE u_id NOT IN (SELECT u_id FROM review)	epinions_1
SELECT count(*) FROM item WHERE i_id NOT IN (SELECT i_id FROM review)	epinions_1
SELECT count(*) FROM gymnast	gymnast
SELECT count(*) FROM gymnast	gymnast
SELECT Total_Points FROM gymnast ORDER BY Total_Points DESC	gymnast
SELECT Total_Points FROM gymnast ORDER BY Total_Points DESC	gymnast
SELECT Total_Points FROM gymnast ORDER BY Floor_Exercise_Points DESC	gymnast
SELECT Total_Points FROM gymnast ORDER BY Floor_Exercise_Points DESC	gymnast
SELECT avg(Horizontal_Bar_Points) FROM gymnast	gymnast
SELECT avg(Horizontal_Bar_Points) FROM gymnast	gymnast
SELECT Name FROM People ORDER BY Name ASC	gymnast
SELECT Name FROM People ORDER BY Name ASC	gymnast
SELECT T2.Name FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID	gymnast
SELECT T2.Name FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID	gymnast
SELECT T2.Name FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID WHERE T2.Hometown != "Santo Domingo"	gymnast
SELECT T2.Name FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID WHERE T2.Hometown != "Santo Domingo"	gymnast
SELECT Age FROM people ORDER BY Height DESC LIMIT 1	gymnast
SELECT Age FROM people ORDER BY Height DESC LIMIT 1	gymnast
SELECT Name FROM People ORDER BY Age DESC LIMIT 5	gymnast
SELECT Name FROM People ORDER BY Age DESC LIMIT 5	gymnast
SELECT T1.Total_Points FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID ORDER BY T2.Age ASC LIMIT 1	gymnast
SELECT T1.Total_Points FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID ORDER BY T2.Age ASC LIMIT 1	gymnast
SELECT avg(T2.Age) FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID	gymnast
SELECT avg(T2.Age) FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID	gymnast
SELECT DISTINCT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID WHERE T1.Total_Points > 57.5	gymnast
SELECT DISTINCT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID WHERE T1.Total_Points > 57.5	gymnast
SELECT T2.Hometown , COUNT(*) FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID GROUP BY T2.Hometown	gymnast
SELECT T2.Hometown , COUNT(*) FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID GROUP BY T2.Hometown	gymnast
SELECT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID GROUP BY T2.Hometown ORDER BY COUNT(*) DESC LIMIT 1	gymnast
SELECT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID GROUP BY T2.Hometown ORDER BY COUNT(*) DESC LIMIT 1	gymnast
SELECT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID GROUP BY T2.Hometown HAVING COUNT(*) >= 2	gymnast
SELECT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID GROUP BY T2.Hometown HAVING COUNT(*) >= 2	gymnast
SELECT T2.Name FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID ORDER BY T2.Height ASC	gymnast
SELECT T2.Name FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID ORDER BY T2.Height ASC	gymnast
SELECT DISTINCT Hometown FROM people EXCEPT SELECT DISTINCT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID	gymnast
SELECT DISTINCT Hometown FROM people EXCEPT SELECT DISTINCT T2.Hometown FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID	gymnast
SELECT Hometown FROM people WHERE Age > 23 INTERSECT SELECT Hometown FROM people WHERE Age < 20	gymnast
SELECT Hometown FROM people WHERE Age > 23 INTERSECT SELECT Hometown FROM people WHERE Age < 20	gymnast
SELECT count(DISTINCT Hometown) FROM people	gymnast
SELECT count(DISTINCT Hometown) FROM people	gymnast
SELECT T2.Age FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID ORDER BY T1.Total_Points DESC	gymnast
SELECT T2.Age FROM gymnast AS T1 JOIN people AS T2 ON T1.Gymnast_ID = T2.People_ID ORDER BY T1.Total_Points DESC	gymnast
SELECT sum(T2.balance) FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid WHERE T1.name != 'Brown'	small_bank_1
SELECT sum(T2.balance) FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid WHERE T1.name != 'Brown'	small_bank_1
SELECT count(*) FROM accounts	small_bank_1
SELECT count(*) FROM accounts	small_bank_1
SELECT sum(balance) FROM checking	small_bank_1
SELECT sum(balance) FROM checking	small_bank_1
SELECT avg(balance) FROM checking	small_bank_1
SELECT avg(balance) FROM checking	small_bank_1
SELECT count(*) FROM savings WHERE balance > (SELECT avg(balance) FROM savings)	small_bank_1
SELECT count(*) FROM savings WHERE balance > (SELECT avg(balance) FROM savings)	small_bank_1
SELECT T1.custid , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T2.balance < (SELECT max(balance) FROM checking)	small_bank_1
SELECT T1.custid , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T2.balance < (SELECT max(balance) FROM checking)	small_bank_1
SELECT T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T1.name LIKE '%ee%'	small_bank_1
SELECT T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T1.name LIKE '%ee%'	small_bank_1
SELECT T2.balance , T3.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T1.name = 'Brown'	small_bank_1
SELECT T2.balance , T3.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T1.name = 'Brown'	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T2.balance > (SELECT avg(balance) FROM checking) INTERSECT SELECT T1.name FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid WHERE T2.balance < (SELECT avg(balance) FROM savings)	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T2.balance > (SELECT avg(balance) FROM checking) INTERSECT SELECT T1.name FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid WHERE T2.balance < (SELECT avg(balance) FROM savings)	small_bank_1
SELECT T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T1.name IN (SELECT T1.name FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid WHERE T2.balance > (SELECT avg(balance) FROM savings))	small_bank_1
SELECT T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T1.name IN (SELECT T1.name FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid WHERE T2.balance > (SELECT avg(balance) FROM savings))	small_bank_1
SELECT name FROM accounts ORDER BY name	small_bank_1
SELECT name FROM accounts ORDER BY name	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T2.balance + T3.balance LIMIT 1	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T2.balance + T3.balance LIMIT 1	small_bank_1
SELECT T1.name , T2.balance + T3.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T3.balance > (SELECT avg(balance) FROM savings)	small_bank_1
SELECT T1.name , T2.balance + T3.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T3.balance > (SELECT avg(balance) FROM savings)	small_bank_1
SELECT T1.name , T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T3.balance LIMIT 1	small_bank_1
SELECT T1.name , T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T3.balance LIMIT 1	small_bank_1
SELECT count(*) , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid GROUP BY T1.name	small_bank_1
SELECT count(*) , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid GROUP BY T1.name	small_bank_1
SELECT sum(T2.balance) , T1.name FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid GROUP BY T1.name	small_bank_1
SELECT sum(T2.balance) , T1.name FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid GROUP BY T1.name	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T2.balance < (SELECT avg(balance) FROM checking)	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid WHERE T2.balance < (SELECT avg(balance) FROM checking)	small_bank_1
SELECT T3.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T2.balance DESC LIMIT 1	small_bank_1
SELECT T3.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T2.balance DESC LIMIT 1	small_bank_1
SELECT T1.balance + T2.balance FROM checking AS T1 JOIN savings AS T2 ON T1.custid = T2.custid ORDER BY T1.balance + T2.balance	small_bank_1
SELECT T1.balance + T2.balance FROM checking AS T1 JOIN savings AS T2 ON T1.custid = T2.custid ORDER BY T1.balance + T2.balance	small_bank_1
SELECT T2.balance , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T3.balance LIMIT 1	small_bank_1
SELECT T2.balance , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T3.balance LIMIT 1	small_bank_1
SELECT T2.balance , T3.balance , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid	small_bank_1
SELECT T2.balance , T3.balance , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid	small_bank_1
SELECT T2.balance , T3.balance , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T2.balance + T3.balance DESC	small_bank_1
SELECT T2.balance , T3.balance , T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid ORDER BY T2.balance + T3.balance DESC	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T2.balance > T3.balance	small_bank_1
SELECT T1.name FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T2.balance > T3.balance	small_bank_1
SELECT T1.name , T3.balance + T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T3.balance < T2.balance	small_bank_1
SELECT T1.name , T3.balance + T2.balance FROM accounts AS T1 JOIN checking AS T2 ON T1.custid = T2.custid JOIN savings AS T3 ON T1.custid = T3.custid WHERE T3.balance < T2.balance	small_bank_1
SELECT T1.name , T2.balance FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid ORDER BY T2.balance DESC LIMIT 3	small_bank_1
SELECT T1.name , T2.balance FROM accounts AS T1 JOIN savings AS T2 ON T1.custid = T2.custid ORDER BY T2.balance DESC LIMIT 3	small_bank_1
SELECT count(*) FROM browser WHERE market_share >= 5	browser_web
SELECT name FROM browser ORDER BY market_share DESC	browser_web
SELECT id , name , market_share FROM browser	browser_web
SELECT max(market_share) , min(market_share) , avg(market_share) FROM browser	browser_web
SELECT id , market_share FROM browser WHERE name = 'Safari'	browser_web
SELECT name , operating_system FROM web_client_accelerator WHERE CONNECTION != 'Broadband'	browser_web
SELECT T1.name FROM browser AS T1 JOIN accelerator_compatible_browser AS T2 ON T1.id = T2.browser_id JOIN web_client_accelerator AS T3 ON T2.accelerator_id = T3.id WHERE T3.name = 'CProxy' AND T2.compatible_since_year > 1998	browser_web
SELECT T1.id , T1.Name FROM web_client_accelerator AS T1 JOIN accelerator_compatible_browser AS T2 ON T2.accelerator_id = T1.id GROUP BY T1.id HAVING count(*) >= 2	browser_web
SELECT T1.id , T1.name FROM browser AS T1 JOIN accelerator_compatible_browser AS T2 ON T1.id = T2.browser_id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1	browser_web
SELECT T1.compatible_since_year FROM accelerator_compatible_browser AS T1 JOIN browser AS T2 ON T1.browser_id = T2.id JOIN web_client_accelerator AS T3 ON T1.accelerator_id = T3.id WHERE T3.name = 'CACHEbox' AND T2.name = 'Internet Explorer'	browser_web
SELECT count(DISTINCT client) FROM web_client_accelerator	browser_web
SELECT count(*) FROM web_client_accelerator WHERE id NOT IN ( SELECT accelerator_id FROM accelerator_compatible_browser );	browser_web
SELECT DISTINCT T1.name FROM web_client_accelerator AS T1 JOIN accelerator_compatible_browser AS T2 ON T2.accelerator_id = T1.id JOIN browser AS T3 ON T2.browser_id = T3.id WHERE T3.market_share > 15;	browser_web
SELECT T3.name FROM web_client_accelerator AS T1 JOIN accelerator_compatible_browser AS T2 ON T2.accelerator_id = T1.id JOIN browser AS T3 ON T2.browser_id = T3.id WHERE T1.name = 'CACHEbox' INTERSECT SELECT T3.name FROM web_client_accelerator AS T1 JOIN accelerator_compatible_browser AS T2 ON T2.accelerator_id = T1.id JOIN browser AS T3 ON T2.browser_id = T3.id WHERE T1.name = 'Fasterfox'	browser_web
SELECT name , operating_system FROM web_client_accelerator EXCEPT SELECT T1.name , T1.operating_system FROM web_client_accelerator AS T1 JOIN accelerator_compatible_browser AS T2 ON T2.accelerator_id = T1.id JOIN browser AS T3 ON T2.browser_id = T3.id WHERE T3.name = 'Opera'	browser_web
SELECT name FROM web_client_accelerator WHERE name LIKE "%Opera%"	browser_web
SELECT Operating_system , count(*) FROM web_client_accelerator GROUP BY Operating_system	browser_web
SELECT T2.name , T3.name FROM accelerator_compatible_browser AS T1 JOIN browser AS T2 ON T1.browser_id = T2.id JOIN web_client_accelerator AS T3 ON T1.accelerator_id = T3.id ORDER BY T1.compatible_since_year DESC	browser_web
SELECT count(*) FROM school	school_finance
SELECT count(*) FROM school	school_finance
SELECT school_name FROM school ORDER BY school_name	school_finance
SELECT school_name , LOCATION , mascot FROM school	school_finance
SELECT sum(enrollment) , avg(enrollment) FROM school	school_finance
SELECT mascot FROM school WHERE enrollment > (SELECT avg(enrollment) FROM school)	school_finance
SELECT school_name FROM school ORDER BY enrollment LIMIT 1	school_finance
SELECT avg(enrollment) , max(enrollment) , min(enrollment) FROM school	school_finance
SELECT county , count(*) , sum(enrollment) FROM school GROUP BY county	school_finance
SELECT count(DISTINCT T1.donator_name) FROM endowment AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id WHERE T2.school_name = "Glenn"	school_finance
SELECT donator_name , sum(amount) FROM endowment GROUP BY donator_name ORDER BY sum(amount) DESC	school_finance
SELECT school_name FROM school WHERE school_id NOT IN (SELECT school_id FROM endowment)	school_finance
SELECT T2.school_name FROM endowment AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id GROUP BY T1.school_id HAVING sum(T1.amount) <= 10	school_finance
SELECT T1.donator_name FROM endowment AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id WHERE T2.school_name = 'Glenn' INTERSECT SELECT T1.donator_name FROM endowment AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id WHERE T2.school_name = 'Triton'	school_finance
SELECT donator_name FROM endowment EXCEPT SELECT donator_name FROM endowment WHERE amount < 9	school_finance
SELECT amount , donator_name FROM endowment ORDER BY amount DESC LIMIT 1	school_finance
SELECT count(*) FROM budget WHERE budgeted > 3000 AND YEAR <= 2001	school_finance
SELECT count(*) FROM budget WHERE budgeted > 3000 AND YEAR <= 2001	school_finance
SELECT T2.school_name , T1.budgeted , T1.invested FROM budget AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id WHERE T1.year >= 2002	school_finance
SELECT DISTINCT donator_name FROM endowment	school_finance
SELECT count(*) FROM budget WHERE budgeted < invested	school_finance
SELECT sum(T1.budgeted) FROM budget AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id WHERE T2.school_name = 'Glenn'	school_finance
SELECT T2.school_name FROM budget AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id JOIN endowment AS T3 ON T2.school_id = T3.school_id GROUP BY T2.school_name HAVING sum(T1.budgeted) > 100 OR sum(T3.amount) > 10	school_finance
SELECT T2.School_name FROM endowment AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id WHERE T1.amount > 8.5 GROUP BY T1.school_id HAVING count(*) > 1	school_finance
SELECT count(*) FROM (SELECT * FROM endowment WHERE amount > 8.5 GROUP BY school_id HAVING count(*) > 1)	school_finance
SELECT T1.School_name , T1.Mascot , T1.IHSAA_Football_Class FROM school AS T1 JOIN budget AS T2 ON T1.school_id = T2.school_id WHERE Budgeted > 6000 OR YEAR < 2003 ORDER BY T2.total_budget_percent_invested , T2.total_budget_percent_budgeted	school_finance
SELECT count(*) FROM building	protein_institute
SELECT name , street_address , floors FROM building ORDER BY floors	protein_institute
SELECT name FROM building ORDER BY height_feet DESC LIMIT 1	protein_institute
SELECT avg(floors) , max(floors) , min(floors) FROM building	protein_institute
SELECT count(*) FROM building WHERE height_feet > (SELECT avg(height_feet) FROM building) OR floors > (SELECT avg(floors) FROM building)	protein_institute
SELECT name FROM building WHERE height_feet >= 200 AND floors >= 20	protein_institute
SELECT institution , LOCATION FROM institution WHERE founded > 1990 AND TYPE = 'Private'	protein_institute
SELECT TYPE , count(*) , sum(enrollment) FROM institution GROUP BY TYPE	protein_institute
SELECT TYPE FROM institution GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	protein_institute
SELECT TYPE FROM institution WHERE founded > 1990 AND enrollment >= 1000	protein_institute
SELECT name FROM building WHERE building_id NOT IN (SELECT building_id FROM institution)	protein_institute
SELECT name FROM building EXCEPT SELECT T1.name FROM building AS T1 JOIN institution AS T2 ON T1.building_id = T2.building_id WHERE T2.founded = 2003	protein_institute
SELECT T1.name , count(*) FROM building AS T1 JOIN institution AS T2 ON T1.building_id = T2.building_id GROUP BY T1.building_id	protein_institute
SELECT T1.name , T1.height_feet FROM building AS T1 JOIN institution AS T2 ON T1.building_id = T2.building_id WHERE T2.founded > 1880 GROUP BY T1.building_id HAVING count(*) >= 2	protein_institute
SELECT DISTINCT TYPE FROM institution	protein_institute
SELECT T1.institution , count(*) FROM institution AS T1 JOIN protein AS T2 ON T1.institution_id = T2.institution_id GROUP BY T1.institution_id	protein_institute
SELECT count(*) FROM institution AS T1 JOIN protein AS T2 ON T1.institution_id = T2.institution_id WHERE T1.founded > 1880 OR T1.type = 'Private'	protein_institute
SELECT T2.protein_name , T1.institution FROM institution AS T1 JOIN protein AS T2 ON T1.institution_id = T2.institution_id	protein_institute
SELECT count(*) FROM institution AS T1 JOIN protein AS T2 ON T1.institution_id = T2.institution_id JOIN building AS T3 ON T3.building_id = T1.building_id WHERE T3.floors >= 20	protein_institute
SELECT count(*) FROM institution WHERE institution_id NOT IN (SELECT institution_id FROM protein)	protein_institute
SELECT LOCATION FROM cinema EXCEPT SELECT LOCATION FROM cinema WHERE capacity > 800	cinema
SELECT LOCATION FROM cinema WHERE openning_year = 2010 INTERSECT SELECT LOCATION FROM cinema WHERE openning_year = 2011	cinema
SELECT count(*) FROM cinema	cinema
SELECT count(*) FROM cinema	cinema
SELECT name , openning_year , capacity FROM cinema	cinema
SELECT name , LOCATION FROM cinema WHERE capacity > (SELECT avg(capacity) FROM cinema)	cinema
SELECT DISTINCT LOCATION FROM cinema	cinema
SELECT DISTINCT LOCATION FROM cinema	cinema
SELECT name , openning_year FROM cinema ORDER BY openning_year DESC	cinema
SELECT name , LOCATION FROM cinema ORDER BY capacity DESC LIMIT 1	cinema
SELECT avg(capacity) , min(capacity) , max(capacity) FROM cinema WHERE openning_year >= 2011	cinema
SELECT LOCATION , count(*) FROM cinema GROUP BY LOCATION	cinema
SELECT LOCATION FROM cinema WHERE openning_year >= 2010 GROUP BY LOCATION ORDER BY count(*) DESC LIMIT 1	cinema
SELECT LOCATION FROM cinema WHERE capacity > 300 GROUP BY LOCATION HAVING count(*) >= 2	cinema
SELECT LOCATION FROM cinema WHERE capacity > 300 GROUP BY LOCATION HAVING count(*) >= 2	cinema
SELECT title , directed_by FROM film	cinema
SELECT title , directed_by FROM film	cinema
SELECT DISTINCT directed_by FROM film	cinema
SELECT DISTINCT directed_by FROM film	cinema
SELECT directed_by , count(*) FROM film GROUP BY directed_by	cinema
SELECT T2.name , sum(T1.show_times_per_day) FROM schedule AS T1 JOIN cinema AS T2 ON T1.cinema_id = T2.cinema_id GROUP BY T1.cinema_id	cinema
SELECT T2.title , max(T1.price) FROM schedule AS T1 JOIN film AS T2 ON T1.film_id = T2.film_id GROUP BY T1.film_id	cinema
SELECT T2.title , max(T1.price) FROM schedule AS T1 JOIN film AS T2 ON T1.film_id = T2.film_id GROUP BY T1.film_id	cinema
SELECT T3.name , T2.title , T1.date , T1.price FROM schedule AS T1 JOIN film AS T2 ON T1.film_id = T2.film_id JOIN cinema AS T3 ON T1.cinema_id = T3.cinema_id	cinema
SELECT title , directed_by FROM film WHERE film_id NOT IN (SELECT film_id FROM schedule)	cinema
SELECT T2.directed_by FROM schedule AS T1 JOIN film AS T2 ON T1.film_id = T2.film_id GROUP BY T2.directed_by ORDER BY sum(T1.show_times_per_day) DESC LIMIT 1	cinema
SELECT LOCATION FROM cinema WHERE capacity > 300 GROUP BY LOCATION HAVING count(*) > 1	cinema
SELECT LOCATION FROM cinema WHERE capacity > 300 GROUP BY LOCATION HAVING count(*) > 1	cinema
SELECT count(*) FROM film WHERE title LIKE "%Dummy%"	cinema
SELECT count(*) FROM film WHERE title LIKE "%Dummy%"	cinema
SELECT T1.good_or_bad_customer FROM customers AS T1 JOIN discount_coupons AS T2 ON T1.coupon_id = T2.coupon_id WHERE T2.coupon_amount = 500	products_for_hire
SELECT T1.customer_id , T1.first_name , count(*) FROM Customers AS T1 JOIN bookings AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id	products_for_hire
SELECT customer_id , sum(amount_paid) FROM Payments GROUP BY customer_id ORDER BY sum(amount_paid) DESC LIMIT 1	products_for_hire
SELECT T1.booking_id , T1.amount_of_refund FROM Bookings AS T1 JOIN Payments AS T2 ON T1.booking_id = T2.booking_id GROUP BY T1.booking_id ORDER BY count(*) DESC LIMIT 1	products_for_hire
SELECT product_id FROM products_booked GROUP BY product_id HAVING count(*) = 3	products_for_hire
SELECT T2.product_description FROM products_booked AS T1 JOIN products_for_hire AS T2 ON T1.product_id = T2.product_id WHERE T1.booked_amount = 102.76	products_for_hire
SELECT T3.booking_start_date , T3.booking_end_date FROM Products_for_hire AS T1 JOIN products_booked AS T2 ON T1.product_id = T2.product_id JOIN bookings AS T3 ON T2.booking_id = T3.booking_id WHERE T1.product_name = 'Book collection A'	products_for_hire
SELECT T2.product_name FROM view_product_availability AS T1 JOIN products_for_hire AS T2 ON T1.product_id = T2.product_id WHERE T1.available_yn = 1	products_for_hire
SELECT count(DISTINCT product_type_code) FROM products_for_hire	products_for_hire
SELECT first_name , last_name , gender_mf FROM customers WHERE good_or_bad_customer = 'good' ORDER BY last_name	products_for_hire
SELECT avg(amount_due) FROM payments	products_for_hire
SELECT max(booked_count) , min(booked_count) , avg(booked_count) FROM products_booked	products_for_hire
SELECT DISTINCT payment_type_code FROM payments	products_for_hire
SELECT daily_hire_cost FROM Products_for_hire WHERE product_name LIKE '%Book%'	products_for_hire
SELECT count(*) FROM Products_for_hire WHERE product_id NOT IN ( SELECT product_id FROM products_booked WHERE booked_amount > 200 )	products_for_hire
SELECT T1.coupon_amount FROM Discount_Coupons AS T1 JOIN customers AS T2 ON T1.coupon_id = T2.coupon_id WHERE T2.good_or_bad_customer = 'good' INTERSECT SELECT T1.coupon_amount FROM Discount_Coupons AS T1 JOIN customers AS T2 ON T1.coupon_id = T2.coupon_id WHERE T2.good_or_bad_customer = 'bad'	products_for_hire
SELECT payment_date FROM payments WHERE amount_paid > 300 OR payment_type_code = 'Check'	products_for_hire
SELECT product_name , product_description FROM products_for_hire WHERE product_type_code = 'Cutlery' AND daily_hire_cost < 20	products_for_hire
SELECT count(*) FROM phone	phone_market
SELECT Name FROM phone ORDER BY Price ASC	phone_market
SELECT Memory_in_G , Carrier FROM phone	phone_market
SELECT DISTINCT Carrier FROM phone WHERE Memory_in_G > 32	phone_market
SELECT Name FROM phone WHERE Carrier = "Sprint" OR Carrier = "TMobile"	phone_market
SELECT Carrier FROM phone ORDER BY Price DESC LIMIT 1	phone_market
SELECT Carrier , COUNT(*) FROM phone GROUP BY Carrier	phone_market
SELECT Carrier FROM phone GROUP BY Carrier ORDER BY COUNT(*) DESC LIMIT 1	phone_market
SELECT Carrier FROM phone WHERE Memory_in_G < 32 INTERSECT SELECT Carrier FROM phone WHERE Memory_in_G > 64	phone_market
SELECT T3.Name , T2.District FROM phone_market AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID JOIN phone AS T3 ON T1.Phone_ID = T3.Phone_ID	phone_market
SELECT T3.Name , T2.District FROM phone_market AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID JOIN phone AS T3 ON T1.Phone_ID = T3.Phone_ID ORDER BY T2.Ranking	phone_market
SELECT T3.Name FROM phone_market AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID JOIN phone AS T3 ON T1.Phone_ID = T3.Phone_ID WHERE T2.Num_of_shops > 50	phone_market
SELECT T2.Name , sum(T1.Num_of_stock) FROM phone_market AS T1 JOIN phone AS T2 ON T1.Phone_ID = T2.Phone_ID GROUP BY T2.Name	phone_market
SELECT T2.Name FROM phone_market AS T1 JOIN phone AS T2 ON T1.Phone_ID = T2.Phone_ID GROUP BY T2.Name HAVING sum(T1.Num_of_stock) >= 2000 ORDER BY sum(T1.Num_of_stock) DESC	phone_market
SELECT Name FROM phone WHERE Phone_id NOT IN (SELECT Phone_ID FROM phone_market)	phone_market
SELECT count(*) FROM region	party_people
SELECT count(*) FROM region	party_people
SELECT DISTINCT region_name FROM region ORDER BY Label	party_people
SELECT DISTINCT region_name FROM region ORDER BY Label	party_people
SELECT count(DISTINCT party_name) FROM party	party_people
SELECT count(DISTINCT party_name) FROM party	party_people
SELECT minister , took_office , left_office FROM party ORDER BY left_office	party_people
SELECT minister , took_office , left_office FROM party ORDER BY left_office	party_people
SELECT minister FROM party WHERE took_office > 1961 OR took_office < 1959	party_people
SELECT minister FROM party WHERE took_office > 1961 OR took_office < 1959	party_people
SELECT minister FROM party WHERE party_name != 'Progress Party'	party_people
SELECT minister FROM party WHERE party_name != 'Progress Party'	party_people
SELECT minister , party_name FROM party ORDER BY took_office DESC	party_people
SELECT minister , party_name FROM party ORDER BY took_office DESC	party_people
SELECT minister FROM party ORDER BY left_office DESC LIMIT 1	party_people
SELECT minister FROM party ORDER BY left_office DESC LIMIT 1	party_people
SELECT T1.member_name , T2.party_name FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id	party_people
SELECT T1.member_name , T2.party_name FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id	party_people
SELECT T2.party_name , count(*) FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id	party_people
SELECT T2.party_name , count(*) FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id	party_people
SELECT T2.party_name FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id ORDER BY count(*) DESC LIMIT 1	party_people
SELECT T2.party_name FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id ORDER BY count(*) DESC LIMIT 1	party_people
SELECT T1.party_name , T2.region_name FROM party AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id	party_people
SELECT T1.party_name , T2.region_name FROM party AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id	party_people
SELECT party_name FROM party WHERE party_id NOT IN (SELECT party_id FROM Member)	party_people
SELECT party_name FROM party WHERE party_id NOT IN (SELECT party_id FROM Member)	party_people
SELECT member_name FROM member WHERE party_id = 3 INTERSECT SELECT member_name FROM member WHERE party_id = 1	party_people
SELECT member_name FROM member WHERE party_id = 3 INTERSECT SELECT member_name FROM member WHERE party_id = 1	party_people
SELECT T1.member_name FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id WHERE T2.Party_name != "Progress Party"	party_people
SELECT T1.member_name FROM Member AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id WHERE T2.Party_name != "Progress Party"	party_people
SELECT count(*) FROM party_events	party_people
SELECT count(*) FROM party_events	party_people
SELECT T2.party_name , count(*) FROM party_events AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id	party_people
SELECT T2.party_name , count(*) FROM party_events AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id	party_people
SELECT member_name FROM member EXCEPT SELECT T1.member_name FROM member AS T1 JOIN party_events AS T2 ON T1.member_id = T2.member_in_charge_id	party_people
SELECT member_name FROM member EXCEPT SELECT T1.member_name FROM member AS T1 JOIN party_events AS T2 ON T1.member_id = T2.member_in_charge_id	party_people
SELECT T2.party_name FROM party_events AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id HAVING count(*) >= 2	party_people
SELECT T2.party_name FROM party_events AS T1 JOIN party AS T2 ON T1.party_id = T2.party_id GROUP BY T1.party_id HAVING count(*) >= 2	party_people
SELECT T1.member_name FROM member AS T1 JOIN party_events AS T2 ON T1.member_id = T2.member_in_charge_id GROUP BY T2.member_in_charge_id ORDER BY count(*) DESC LIMIT 1	party_people
SELECT T1.member_name FROM member AS T1 JOIN party_events AS T2 ON T1.member_id = T2.member_in_charge_id GROUP BY T2.member_in_charge_id ORDER BY count(*) DESC LIMIT 1	party_people
SELECT event_name FROM party_events GROUP BY event_name HAVING count(*) > 2	party_people
SELECT event_name FROM party_events GROUP BY event_name HAVING count(*) > 2	party_people
SELECT count(*) FROM region AS t1 JOIN party AS t2 ON t1.region_id = t2.region_id JOIN party_events AS t3 ON t2.party_id = t3.party_id WHERE t1.region_name = "United Kingdom" AND t3.Event_Name = "Annaual Meeting"	party_people
SELECT count(*) FROM region AS t1 JOIN party AS t2 ON t1.region_id = t2.region_id JOIN party_events AS t3 ON t2.party_id = t3.party_id WHERE t1.region_name = "United Kingdom" AND t3.Event_Name = "Annaual Meeting"	party_people
SELECT count(*) FROM pilot	pilot_record
SELECT Pilot_name FROM pilot ORDER BY Rank ASC	pilot_record
SELECT POSITION , Team FROM pilot	pilot_record
SELECT DISTINCT POSITION FROM pilot WHERE Age > 30	pilot_record
SELECT Pilot_name FROM pilot WHERE Team = "Bradley" OR Team = "Fordham"	pilot_record
SELECT Join_Year FROM pilot ORDER BY Rank ASC LIMIT 1	pilot_record
SELECT Nationality , COUNT(*) FROM pilot GROUP BY Nationality	pilot_record
SELECT Nationality FROM pilot GROUP BY Nationality ORDER BY COUNT(*) DESC LIMIT 1	pilot_record
SELECT POSITION FROM pilot WHERE Join_Year < 2000 INTERSECT SELECT POSITION FROM pilot WHERE Join_Year > 2005	pilot_record
SELECT T3.Pilot_name , T2.Model FROM pilot_record AS T1 JOIN aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN pilot AS T3 ON T1.Pilot_ID = T3.Pilot_ID	pilot_record
SELECT T3.Pilot_name , T2.Fleet_Series FROM pilot_record AS T1 JOIN aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN pilot AS T3 ON T1.Pilot_ID = T3.Pilot_ID ORDER BY T3.Rank	pilot_record
SELECT T2.Fleet_Series FROM pilot_record AS T1 JOIN aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN pilot AS T3 ON T1.Pilot_ID = T3.Pilot_ID WHERE T3.Age < 34	pilot_record
SELECT T2.Pilot_name , COUNT(*) FROM pilot_record AS T1 JOIN pilot AS T2 ON T1.pilot_ID = T2.pilot_ID GROUP BY T2.Pilot_name	pilot_record
SELECT T2.Pilot_name , COUNT(*) FROM pilot_record AS T1 JOIN pilot AS T2 ON T1.pilot_ID = T2.pilot_ID GROUP BY T2.Pilot_name HAVING COUNT(*) > 1	pilot_record
SELECT Pilot_name FROM pilot WHERE Pilot_ID NOT IN (SELECT Pilot_ID FROM pilot_record)	pilot_record
SELECT document_status_code FROM Ref_Document_Status;	cre_Doc_Control_Systems
SELECT document_status_description FROM Ref_Document_Status WHERE document_status_code = "working";	cre_Doc_Control_Systems
SELECT document_type_code FROM Ref_Document_Types;	cre_Doc_Control_Systems
SELECT document_type_description FROM Ref_Document_Types WHERE document_type_code = "Paper";	cre_Doc_Control_Systems
SELECT shipping_agent_name FROM Ref_Shipping_Agents;	cre_Doc_Control_Systems
SELECT shipping_agent_code FROM Ref_Shipping_Agents WHERE shipping_agent_name = "UPS";	cre_Doc_Control_Systems
SELECT role_code FROM ROLES;	cre_Doc_Control_Systems
SELECT role_description FROM ROLES WHERE role_code = "ED";	cre_Doc_Control_Systems
SELECT count(*) FROM Employees;	cre_Doc_Control_Systems
SELECT T1.role_description FROM ROLES AS T1 JOIN Employees AS T2 ON T1.role_code = T2.role_code WHERE T2.employee_name = "Koby";	cre_Doc_Control_Systems
SELECT document_id , receipt_date FROM Documents;	cre_Doc_Control_Systems
SELECT T1.role_description , T2.role_code , count(*) FROM ROLES AS T1 JOIN Employees AS T2 ON T1.role_code = T2.role_code GROUP BY T2.role_code;	cre_Doc_Control_Systems
SELECT T1.role_description , count(T2.employee_id) FROM Roles AS T1 JOIN Employees AS T2 ON T2.role_code = T1.role_code GROUP BY T2.role_code HAVING count(T2.employee_id) > 1;	cre_Doc_Control_Systems
SELECT T1.document_status_description FROM Ref_Document_Status AS T1 JOIN Documents AS T2 ON T1.document_status_code = T2.document_status_code WHERE T2.document_id = 1;	cre_Doc_Control_Systems
SELECT count(*) FROM Documents WHERE document_status_code = "done";	cre_Doc_Control_Systems
SELECT document_type_code FROM Documents WHERE document_id = 2;	cre_Doc_Control_Systems
SELECT document_id FROM Documents WHERE document_status_code = "done" AND document_type_code = "Paper";	cre_Doc_Control_Systems
SELECT T1.shipping_agent_name FROM Ref_Shipping_Agents AS T1 JOIN Documents AS T2 ON T2.shipping_agent_code = T1.shipping_agent_code WHERE T2.document_id = 2;	cre_Doc_Control_Systems
SELECT count(*) FROM Ref_Shipping_Agents AS T1 JOIN Documents AS T2 ON T2.shipping_agent_code = T1.shipping_agent_code WHERE T1.shipping_agent_name = "USPS";	cre_Doc_Control_Systems
SELECT T1.shipping_agent_name , count(T2.document_id) FROM Ref_Shipping_Agents AS T1 JOIN Documents AS T2 ON T2.shipping_agent_code = T1.shipping_agent_code GROUP BY T1.shipping_agent_code ORDER BY count(T2.document_id) DESC LIMIT 1;	cre_Doc_Control_Systems
SELECT receipt_date FROM Documents WHERE document_id = 3;	cre_Doc_Control_Systems
SELECT T1.address_details FROM Addresses AS T1 JOIN Documents_Mailed AS T2 ON T2.mailed_to_address_id = T1.address_id WHERE document_id = 4;	cre_Doc_Control_Systems
SELECT mailing_date FROM Documents_Mailed WHERE document_id = 7;	cre_Doc_Control_Systems
SELECT document_id FROM Documents WHERE document_status_code = "done" AND document_type_code = "Paper" EXCEPT SELECT T1.document_id FROM Documents AS T1 JOIN Ref_Shipping_Agents AS T2 ON T1.shipping_agent_code = T2.shipping_agent_code WHERE T2.shipping_agent_name = "USPS";	cre_Doc_Control_Systems
SELECT document_id FROM Documents WHERE document_status_code = "done" AND document_type_code = "Paper" INTERSECT SELECT T1.document_id FROM Documents AS T1 JOIN Ref_Shipping_Agents AS T2 ON T1.shipping_agent_code = T2.shipping_agent_code WHERE T2.shipping_agent_name = "USPS";	cre_Doc_Control_Systems
SELECT draft_details FROM Document_Drafts WHERE document_id = 7;	cre_Doc_Control_Systems
SELECT count(*) FROM Draft_Copies WHERE document_id = 2;	cre_Doc_Control_Systems
SELECT document_id , count(copy_number) FROM Draft_Copies GROUP BY document_id ORDER BY count(copy_number) DESC LIMIT 1;	cre_Doc_Control_Systems
SELECT document_id , count(*) FROM Draft_Copies GROUP BY document_id HAVING count(*) > 1;	cre_Doc_Control_Systems
SELECT T1.employee_name FROM Employees AS T1 JOIN Circulation_History AS T2 ON T2.employee_id = T1.employee_id WHERE T2.document_id = 1;	cre_Doc_Control_Systems
SELECT employee_name FROM Employees EXCEPT SELECT T1.employee_name FROM Employees AS T1 JOIN Circulation_History AS T2 ON T2.employee_id = T1.employee_id	cre_Doc_Control_Systems
SELECT T1.employee_name , count(*) FROM Employees AS T1 JOIN Circulation_History AS T2 ON T2.employee_id = T1.employee_id GROUP BY T2.document_id , T2.draft_number , T2.copy_number ORDER BY count(*) DESC LIMIT 1;	cre_Doc_Control_Systems
SELECT document_id , count(DISTINCT employee_id) FROM Circulation_History GROUP BY document_id;	cre_Doc_Control_Systems
SELECT name FROM races ORDER BY date DESC LIMIT 1	formula_1
SELECT name FROM races ORDER BY date DESC LIMIT 1	formula_1
SELECT name , date FROM races ORDER BY date DESC LIMIT 1	formula_1
SELECT name , date FROM races ORDER BY date DESC LIMIT 1	formula_1
SELECT name FROM races WHERE YEAR = 2017	formula_1
SELECT name FROM races WHERE YEAR = 2017	formula_1
SELECT DISTINCT name FROM races WHERE YEAR BETWEEN 2014 AND 2017	formula_1
SELECT DISTINCT name FROM races WHERE YEAR BETWEEN 2014 AND 2017	formula_1
SELECT DISTINCT T1.forename , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid WHERE T2.milliseconds < 93000	formula_1
SELECT DISTINCT T1.forename , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid WHERE T2.milliseconds < 93000	formula_1
SELECT DISTINCT T1.driverid , T1.nationality FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid WHERE T2.milliseconds > 100000	formula_1
SELECT DISTINCT T1.driverid , T1.nationality FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid WHERE T2.milliseconds > 100000	formula_1
SELECT T1.forename , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid ORDER BY T2.milliseconds LIMIT 1	formula_1
SELECT T1.forename , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid ORDER BY T2.milliseconds LIMIT 1	formula_1
SELECT T1.driverid , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid ORDER BY T2.milliseconds DESC LIMIT 1	formula_1
SELECT T1.driverid , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid ORDER BY T2.milliseconds DESC LIMIT 1	formula_1
SELECT T1.driverid , T1.forename , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid WHERE POSITION = '1' GROUP BY T1.driverid HAVING count(*) >= 2	formula_1
SELECT T1.driverid , T1.forename , T1.surname FROM drivers AS T1 JOIN laptimes AS T2 ON T1.driverid = T2.driverid WHERE POSITION = '1' GROUP BY T1.driverid HAVING count(*) >= 2	formula_1
SELECT count(*) FROM results AS T1 JOIN races AS T2 ON T1.raceid = T2.raceid WHERE T2.name = "Australian Grand Prix" AND YEAR = 2009	formula_1
SELECT count(*) FROM results AS T1 JOIN races AS T2 ON T1.raceid = T2.raceid WHERE T2.name = "Australian Grand Prix" AND YEAR = 2009	formula_1
SELECT count(DISTINCT driverId) FROM results WHERE raceId NOT IN( SELECT raceId FROM races WHERE YEAR != 2009 )	formula_1
SELECT count(DISTINCT driverId) FROM results WHERE raceId NOT IN( SELECT raceId FROM races WHERE YEAR != 2009 )	formula_1
SELECT T2.name , T2.year FROM results AS T1 JOIN races AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T1.driverid = T3.driverid WHERE T3.forename = "Lewis"	formula_1
SELECT T2.name , T2.year FROM results AS T1 JOIN races AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T1.driverid = T3.driverid WHERE T3.forename = "Lewis"	formula_1
SELECT forename , surname FROM drivers WHERE nationality = "German"	formula_1
SELECT forename , surname FROM drivers WHERE nationality = "German"	formula_1
SELECT T2.driverid , T3.forename FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Australian Grand Prix" INTERSECT SELECT T2.driverid , T3.forename FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Chinese Grand Prix"	formula_1
SELECT T2.driverid , T3.forename FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Australian Grand Prix" INTERSECT SELECT T2.driverid , T3.forename FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Chinese Grand Prix"	formula_1
SELECT T3.forename , T3.surname FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Australian Grand Prix" EXCEPT SELECT T3.forename , T3.surname FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Chinese Grand Prix"	formula_1
SELECT T3.forename , T3.surname FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Australian Grand Prix" EXCEPT SELECT T3.forename , T3.surname FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid JOIN drivers AS T3 ON T2.driverid = T3.driverid WHERE T1.name = "Chinese Grand Prix"	formula_1
SELECT DISTINCT T1.forename FROM drivers AS T1 JOIN driverstandings AS T2 ON T1.driverid = T2.driverid WHERE T2.position = 1 AND T2.wins = 1	formula_1
SELECT DISTINCT T1.forename FROM drivers AS T1 JOIN driverstandings AS T2 ON T1.driverid = T2.driverid WHERE T2.position = 1 AND T2.wins = 1	formula_1
SELECT DISTINCT T1.forename FROM drivers AS T1 JOIN driverstandings AS T2 ON T1.driverid = T2.driverid WHERE T2.position = 1 AND T2.wins = 1 AND T2.points > 20	formula_1
SELECT DISTINCT T1.forename FROM drivers AS T1 JOIN driverstandings AS T2 ON T1.driverid = T2.driverid WHERE T2.position = 1 AND T2.wins = 1 AND T2.points > 20	formula_1
SELECT count(*) , nationality FROM constructors GROUP BY nationality	formula_1
SELECT count(*) , nationality FROM constructors GROUP BY nationality	formula_1
SELECT count(*) , constructorid FROM constructorStandings GROUP BY constructorid	formula_1
SELECT count(*) , constructorid FROM constructorStandings GROUP BY constructorid	formula_1
SELECT T1.name FROM races AS T1 JOIN circuits AS T2 ON T1.circuitid = T2.circuitid WHERE T2.country = "Spain" AND T1.year > 2017	formula_1
SELECT T1.name FROM races AS T1 JOIN circuits AS T2 ON T1.circuitid = T2.circuitid WHERE T2.country = "Spain" AND T1.year > 2017	formula_1
SELECT DISTINCT T1.name FROM races AS T1 JOIN circuits AS T2 ON T1.circuitid = T2.circuitid WHERE T2.country = "Spain" AND T1.year > 2000	formula_1
SELECT DISTINCT T1.name FROM races AS T1 JOIN circuits AS T2 ON T1.circuitid = T2.circuitid WHERE T2.country = "Spain" AND T1.year > 2000	formula_1
SELECT DISTINCT driverid , STOP FROM pitstops WHERE duration < (SELECT max(duration) FROM pitstops WHERE raceid = 841)	formula_1
SELECT DISTINCT driverid , STOP FROM pitstops WHERE duration < (SELECT max(duration) FROM pitstops WHERE raceid = 841)	formula_1
SELECT DISTINCT driverid , STOP FROM pitstops WHERE duration > (SELECT min(duration) FROM pitstops WHERE raceid = 841)	formula_1
SELECT DISTINCT driverid , STOP FROM pitstops WHERE duration > (SELECT min(duration) FROM pitstops WHERE raceid = 841)	formula_1
SELECT DISTINCT forename FROM drivers ORDER BY forename ASC	formula_1
SELECT DISTINCT forename FROM drivers ORDER BY forename ASC	formula_1
SELECT DISTINCT name FROM races ORDER BY name DESC	formula_1
SELECT DISTINCT name FROM races ORDER BY name DESC	formula_1
SELECT name FROM races WHERE YEAR BETWEEN 2009 AND 2011	formula_1
SELECT name FROM races WHERE YEAR BETWEEN 2009 AND 2011	formula_1
SELECT name FROM races WHERE TIME > "12:00:00" OR TIME < "09:00:00"	formula_1
SELECT name FROM races WHERE TIME > "12:00:00" OR TIME < "09:00:00"	formula_1
SELECT T1.forename , T1.surname , T1.driverid FROM drivers AS T1 JOIN pitstops AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) > 8 UNION SELECT T1.forename , T1.surname , T1.driverid FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) > 5	formula_1
SELECT T1.forename , T1.surname , T1.driverid FROM drivers AS T1 JOIN pitstops AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) > 8 UNION SELECT T1.forename , T1.surname , T1.driverid FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) > 5	formula_1
SELECT T1.surname , T1.driverid FROM drivers AS T1 JOIN pitstops AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) = 11 INTERSECT SELECT T1.surname , T1.driverid FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) > 5	formula_1
SELECT T1.surname , T1.driverid FROM drivers AS T1 JOIN pitstops AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) = 11 INTERSECT SELECT T1.surname , T1.driverid FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid GROUP BY T1.driverid HAVING count(*) > 5	formula_1
SELECT T1.driverid , T1.surname FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid WHERE T3.year > 2010 GROUP BY T1.driverid ORDER BY count(*) DESC LIMIT 1	formula_1
SELECT T1.driverid , T1.surname FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid WHERE T3.year > 2010 GROUP BY T1.driverid ORDER BY count(*) DESC LIMIT 1	formula_1
SELECT name FROM circuits WHERE country = "UK" OR country = "Malaysia"	formula_1
SELECT name FROM circuits WHERE country = "UK" OR country = "Malaysia"	formula_1
SELECT circuitid , LOCATION FROM circuits WHERE country = "France" OR country = "Belgium"	formula_1
SELECT circuitid , LOCATION FROM circuits WHERE country = "France" OR country = "Belgium"	formula_1
SELECT T1.name FROM constructors AS T1 JOIN constructorstandings AS T2 ON T1.constructorid = T2.constructorid WHERE T1.nationality = "Japanese" AND T2.points > 5	formula_1
SELECT T1.name FROM constructors AS T1 JOIN constructorstandings AS T2 ON T1.constructorid = T2.constructorid WHERE T1.nationality = "Japanese" AND T2.points > 5	formula_1
SELECT avg(T2.fastestlapspeed) FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year = 2008 AND T1.name = "Monaco Grand Prix"	formula_1
SELECT avg(T2.fastestlapspeed) FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year = 2008 AND T1.name = "Monaco Grand Prix"	formula_1
SELECT max(T2.fastestlapspeed) FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year = 2008 AND T1.name = "Monaco Grand Prix"	formula_1
SELECT max(T2.fastestlapspeed) FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year = 2008 AND T1.name = "Monaco Grand Prix"	formula_1
SELECT max(T2.fastestlapspeed) , T1.name , T1.year FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year > 2014 GROUP BY T1.name ORDER BY T1.year	formula_1
SELECT max(T2.fastestlapspeed) , T1.name , T1.year FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year > 2014 GROUP BY T1.name ORDER BY T1.year	formula_1
SELECT avg(T2.fastestlapspeed) , T1.name , T1.year FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year > 2014 GROUP BY T1.name ORDER BY T1.year	formula_1
SELECT avg(T2.fastestlapspeed) , T1.name , T1.year FROM races AS T1 JOIN results AS T2 ON T1.raceid = T2.raceid WHERE T1.year > 2014 GROUP BY T1.name ORDER BY T1.year	formula_1
SELECT T1.driverid , T1.forename , count(*) FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid GROUP BY T1.driverid HAVING count(*) >= 2	formula_1
SELECT T1.driverid , T1.forename , count(*) FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid GROUP BY T1.driverid HAVING count(*) >= 2	formula_1
SELECT T1.driverid , count(*) FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid GROUP BY T1.driverid HAVING count(*) <= 30	formula_1
SELECT T1.driverid , count(*) FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid GROUP BY T1.driverid HAVING count(*) <= 30	formula_1
SELECT T1.driverid , T1.surname FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid GROUP BY T1.driverid ORDER BY count(*) DESC LIMIT 1	formula_1
SELECT T1.driverid , T1.surname FROM drivers AS T1 JOIN results AS T2 ON T1.driverid = T2.driverid JOIN races AS T3 ON T2.raceid = T3.raceid GROUP BY T1.driverid ORDER BY count(*) DESC LIMIT 1	formula_1
SELECT count(*) FROM technician	machine_repair
SELECT count(*) FROM technician	machine_repair
SELECT Name FROM technician ORDER BY Age ASC	machine_repair
SELECT Name FROM technician ORDER BY Age ASC	machine_repair
SELECT Team , Starting_Year FROM technician	machine_repair
SELECT Team , Starting_Year FROM technician	machine_repair
SELECT Name FROM technician WHERE Team != "NYY"	machine_repair
SELECT Name FROM technician WHERE Team != "NYY"	machine_repair
SELECT Name FROM technician WHERE Age = 36 OR Age = 37	machine_repair
SELECT Name FROM technician WHERE Age = 36 OR Age = 37	machine_repair
SELECT Starting_Year FROM technician ORDER BY Age DESC LIMIT 1	machine_repair
SELECT Starting_Year FROM technician ORDER BY Age DESC LIMIT 1	machine_repair
SELECT Team , COUNT(*) FROM technician GROUP BY Team	machine_repair
SELECT Team , COUNT(*) FROM technician GROUP BY Team	machine_repair
SELECT Team FROM technician GROUP BY Team ORDER BY COUNT(*) DESC LIMIT 1	machine_repair
SELECT Team FROM technician GROUP BY Team ORDER BY COUNT(*) DESC LIMIT 1	machine_repair
SELECT Team FROM technician GROUP BY Team HAVING COUNT(*) >= 2	machine_repair
SELECT Team FROM technician GROUP BY Team HAVING COUNT(*) >= 2	machine_repair
SELECT T3.Name , T2.Machine_series FROM repair_assignment AS T1 JOIN machine AS T2 ON T1.machine_id = T2.machine_id JOIN technician AS T3 ON T1.technician_ID = T3.technician_ID	machine_repair
SELECT T3.Name , T2.Machine_series FROM repair_assignment AS T1 JOIN machine AS T2 ON T1.machine_id = T2.machine_id JOIN technician AS T3 ON T1.technician_ID = T3.technician_ID	machine_repair
SELECT T3.Name FROM repair_assignment AS T1 JOIN machine AS T2 ON T1.machine_id = T2.machine_id JOIN technician AS T3 ON T1.technician_ID = T3.technician_ID ORDER BY T2.quality_rank	machine_repair
SELECT T3.Name FROM repair_assignment AS T1 JOIN machine AS T2 ON T1.machine_id = T2.machine_id JOIN technician AS T3 ON T1.technician_ID = T3.technician_ID ORDER BY T2.quality_rank	machine_repair
SELECT T3.Name FROM repair_assignment AS T1 JOIN machine AS T2 ON T1.machine_id = T2.machine_id JOIN technician AS T3 ON T1.technician_ID = T3.technician_ID WHERE T2.value_points > 70	machine_repair
SELECT T3.Name FROM repair_assignment AS T1 JOIN machine AS T2 ON T1.machine_id = T2.machine_id JOIN technician AS T3 ON T1.technician_ID = T3.technician_ID WHERE T2.value_points > 70	machine_repair
SELECT T2.Name , COUNT(*) FROM repair_assignment AS T1 JOIN technician AS T2 ON T1.technician_ID = T2.technician_ID GROUP BY T2.Name	machine_repair
SELECT T2.Name , COUNT(*) FROM repair_assignment AS T1 JOIN technician AS T2 ON T1.technician_ID = T2.technician_ID GROUP BY T2.Name	machine_repair
SELECT Name FROM technician WHERE technician_id NOT IN (SELECT technician_id FROM repair_assignment)	machine_repair
SELECT Name FROM technician WHERE technician_id NOT IN (SELECT technician_id FROM repair_assignment)	machine_repair
SELECT Starting_Year FROM technician WHERE Team = "CLE" INTERSECT SELECT Starting_Year FROM technician WHERE Team = "CWS"	machine_repair
SELECT Starting_Year FROM technician WHERE Team = "CLE" INTERSECT SELECT Starting_Year FROM technician WHERE Team = "CWS"	machine_repair
SELECT count(*) FROM entrepreneur	entrepreneur
SELECT count(*) FROM entrepreneur	entrepreneur
SELECT Company FROM entrepreneur ORDER BY Money_Requested DESC	entrepreneur
SELECT Company FROM entrepreneur ORDER BY Money_Requested DESC	entrepreneur
SELECT Company , Investor FROM entrepreneur	entrepreneur
SELECT Company , Investor FROM entrepreneur	entrepreneur
SELECT avg(Money_Requested) FROM entrepreneur	entrepreneur
SELECT avg(Money_Requested) FROM entrepreneur	entrepreneur
SELECT Name FROM People ORDER BY Weight ASC	entrepreneur
SELECT Name FROM People ORDER BY Weight ASC	entrepreneur
SELECT T2.Name FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID	entrepreneur
SELECT T2.Name FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID	entrepreneur
SELECT T2.Name FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Investor != "Rachel Elnaugh"	entrepreneur
SELECT T2.Name FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Investor != "Rachel Elnaugh"	entrepreneur
SELECT Weight FROM people ORDER BY Height ASC LIMIT 1	entrepreneur
SELECT Weight FROM people ORDER BY Height ASC LIMIT 1	entrepreneur
SELECT T2.Name FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Weight DESC LIMIT 1	entrepreneur
SELECT T2.Name FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Weight DESC LIMIT 1	entrepreneur
SELECT sum(T1.Money_Requested) FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Height > 1.85	entrepreneur
SELECT sum(T1.Money_Requested) FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Height > 1.85	entrepreneur
SELECT T2.Date_of_Birth FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Investor = "Simon Woodroffe" OR T1.Investor = "Peter Jones"	entrepreneur
SELECT T2.Date_of_Birth FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Investor = "Simon Woodroffe" OR T1.Investor = "Peter Jones"	entrepreneur
SELECT T2.Weight FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Money_Requested DESC	entrepreneur
SELECT T2.Weight FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Money_Requested DESC	entrepreneur
SELECT Investor , COUNT(*) FROM entrepreneur GROUP BY Investor	entrepreneur
SELECT Investor , COUNT(*) FROM entrepreneur GROUP BY Investor	entrepreneur
SELECT Investor FROM entrepreneur GROUP BY Investor ORDER BY COUNT(*) DESC LIMIT 1	entrepreneur
SELECT Investor FROM entrepreneur GROUP BY Investor ORDER BY COUNT(*) DESC LIMIT 1	entrepreneur
SELECT Investor FROM entrepreneur GROUP BY Investor HAVING COUNT(*) >= 2	entrepreneur
SELECT Investor FROM entrepreneur GROUP BY Investor HAVING COUNT(*) >= 2	entrepreneur
SELECT T2.Name , T1.Company FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Money_Requested	entrepreneur
SELECT T2.Name , T1.Company FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Money_Requested	entrepreneur
SELECT Name FROM people WHERE People_ID NOT IN (SELECT People_ID FROM entrepreneur)	entrepreneur
SELECT Name FROM people WHERE People_ID NOT IN (SELECT People_ID FROM entrepreneur)	entrepreneur
SELECT Investor FROM entrepreneur WHERE Money_Requested > 140000 INTERSECT SELECT Investor FROM entrepreneur WHERE Money_Requested < 120000	entrepreneur
SELECT Investor FROM entrepreneur WHERE Money_Requested > 140000 INTERSECT SELECT Investor FROM entrepreneur WHERE Money_Requested < 120000	entrepreneur
SELECT count(DISTINCT Company) FROM entrepreneur	entrepreneur
SELECT count(DISTINCT Company) FROM entrepreneur	entrepreneur
SELECT T1.Company FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Height DESC LIMIT 1	entrepreneur
SELECT T1.Company FROM entrepreneur AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Height DESC LIMIT 1	entrepreneur
SELECT campus FROM campuses WHERE county = "Los Angeles"	csu_1
SELECT campus FROM campuses WHERE county = "Los Angeles"	csu_1
SELECT campus FROM campuses WHERE LOCATION = "Chico"	csu_1
SELECT campus FROM campuses WHERE LOCATION = "Chico"	csu_1
SELECT campus FROM campuses WHERE YEAR = 1958	csu_1
SELECT campus FROM campuses WHERE YEAR = 1958	csu_1
SELECT campus FROM campuses WHERE YEAR < 1800	csu_1
SELECT campus FROM campuses WHERE YEAR < 1800	csu_1
SELECT campus FROM campuses WHERE YEAR >= 1935 AND YEAR <= 1939	csu_1
SELECT campus FROM campuses WHERE YEAR >= 1935 AND YEAR <= 1939	csu_1
SELECT campus FROM campuses WHERE LOCATION = "Northridge" AND county = "Los Angeles" UNION SELECT campus FROM campuses WHERE LOCATION = "San Francisco" AND county = "San Francisco"	csu_1
SELECT campus FROM campuses WHERE LOCATION = "Northridge" AND county = "Los Angeles" UNION SELECT campus FROM campuses WHERE LOCATION = "San Francisco" AND county = "San Francisco"	csu_1
SELECT campusfee FROM campuses AS T1 JOIN csu_fees AS T2 ON T1.id = t2.campus WHERE t1.campus = "San Jose State University" AND T2.year = 1996	csu_1
SELECT campusfee FROM campuses AS T1 JOIN csu_fees AS T2 ON T1.id = t2.campus WHERE t1.campus = "San Jose State University" AND T2.year = 1996	csu_1
SELECT campusfee FROM campuses AS T1 JOIN csu_fees AS T2 ON T1.id = t2.campus WHERE t1.campus = "San Francisco State University" AND T2.year = 1996	csu_1
SELECT campusfee FROM campuses AS T1 JOIN csu_fees AS T2 ON T1.id = t2.campus WHERE t1.campus = "San Francisco State University" AND T2.year = 1996	csu_1
SELECT count(*) FROM csu_fees WHERE campusfee > (SELECT avg(campusfee) FROM csu_fees)	csu_1
SELECT count(*) FROM csu_fees WHERE campusfee > (SELECT avg(campusfee) FROM csu_fees)	csu_1
SELECT count(*) FROM csu_fees WHERE campusfee > (SELECT avg(campusfee) FROM csu_fees)	csu_1
SELECT count(*) FROM csu_fees WHERE campusfee > (SELECT avg(campusfee) FROM csu_fees)	csu_1
SELECT campus FROM campuses WHERE county = "Los Angeles" AND YEAR > 1950	csu_1
SELECT campus FROM campuses WHERE county = "Los Angeles" AND YEAR > 1950	csu_1
SELECT YEAR FROM degrees GROUP BY YEAR ORDER BY sum(degrees) DESC LIMIT 1	csu_1
SELECT YEAR FROM degrees GROUP BY YEAR ORDER BY sum(degrees) DESC LIMIT 1	csu_1
SELECT campus FROM degrees GROUP BY campus ORDER BY sum(degrees) DESC LIMIT 1	csu_1
SELECT campus FROM degrees GROUP BY campus ORDER BY sum(degrees) DESC LIMIT 1	csu_1
SELECT T1.campus FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = T2.campus WHERE T2.year = 2003 ORDER BY T2.faculty DESC LIMIT 1	csu_1
SELECT T1.campus FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = T2.campus WHERE T2.year = 2003 ORDER BY T2.faculty DESC LIMIT 1	csu_1
SELECT avg(campusfee) FROM csu_fees WHERE YEAR = 1996	csu_1
SELECT avg(campusfee) FROM csu_fees WHERE YEAR = 1996	csu_1
SELECT avg(campusfee) FROM csu_fees WHERE YEAR = 2005	csu_1
SELECT avg(campusfee) FROM csu_fees WHERE YEAR = 2005	csu_1
SELECT T1.campus , sum(T2.degrees) FROM campuses AS T1 JOIN degrees AS T2 ON T1.id = T2.campus WHERE T2.year >= 1998 AND T2.year <= 2002 GROUP BY T1.campus	csu_1
SELECT T1.campus , sum(T2.degrees) FROM campuses AS T1 JOIN degrees AS T2 ON T1.id = T2.campus WHERE T2.year >= 1998 AND T2.year <= 2002 GROUP BY T1.campus	csu_1
SELECT T1.campus , sum(T2.degrees) FROM campuses AS T1 JOIN degrees AS T2 ON T1.id = T2.campus WHERE T1.county = "Orange" AND T2.year >= 2000 GROUP BY T1.campus	csu_1
SELECT T1.campus , sum(T2.degrees) FROM campuses AS T1 JOIN degrees AS T2 ON T1.id = T2.campus WHERE T1.county = "Orange" AND T2.year >= 2000 GROUP BY T1.campus	csu_1
SELECT T1.campus FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = T2.campus WHERE T2.year = 2002 AND faculty > (SELECT max(faculty) FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = T2.campus WHERE T2.year = 2002 AND T1.county = "Orange")	csu_1
SELECT T1.campus FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = T2.campus WHERE T2.year = 2002 AND faculty > (SELECT max(faculty) FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = T2.campus WHERE T2.year = 2002 AND T1.county = "Orange")	csu_1
SELECT T1.campus FROM campuses AS t1 JOIN enrollments AS t2 ON t1.id = t2.campus WHERE t2.year = 1956 AND totalenrollment_ay > 400 AND FTE_AY > 200	csu_1
SELECT T1.campus FROM campuses AS t1 JOIN enrollments AS t2 ON t1.id = t2.campus WHERE t2.year = 1956 AND totalenrollment_ay > 400 AND FTE_AY > 200	csu_1
SELECT count(*) FROM campuses WHERE county = "Los Angeles"	csu_1
SELECT count(*) FROM campuses WHERE county = "Los Angeles"	csu_1
SELECT campus FROM campuses WHERE county = "Los Angeles"	csu_1
SELECT campus FROM campuses WHERE county = "Los Angeles"	csu_1
SELECT degrees FROM campuses AS T1 JOIN degrees AS T2 ON t1.id = t2.campus WHERE t1.campus = "San Jose State University" AND t2.year = 2000	csu_1
SELECT degrees FROM campuses AS T1 JOIN degrees AS T2 ON t1.id = t2.campus WHERE t1.campus = "San Jose State University" AND t2.year = 2000	csu_1
SELECT degrees FROM campuses AS T1 JOIN degrees AS T2 ON t1.id = t2.campus WHERE t1.campus = "San Francisco State University" AND t2.year = 2001	csu_1
SELECT degrees FROM campuses AS T1 JOIN degrees AS T2 ON t1.id = t2.campus WHERE t1.campus = "San Francisco State University" AND t2.year = 2001	csu_1
SELECT sum(faculty) FROM faculty WHERE YEAR = 2002	csu_1
SELECT sum(faculty) FROM faculty WHERE YEAR = 2002	csu_1
SELECT faculty FROM faculty AS T1 JOIN campuses AS T2 ON T1.campus = T2.id WHERE T1.year = 2002 AND T2.campus = "Long Beach State University"	csu_1
SELECT faculty FROM faculty AS T1 JOIN campuses AS T2 ON T1.campus = T2.id WHERE T1.year = 2002 AND T2.campus = "Long Beach State University"	csu_1
SELECT faculty FROM faculty AS T1 JOIN campuses AS T2 ON T1.campus = T2.id WHERE T1.year = 2004 AND T2.campus = "San Francisco State University"	csu_1
SELECT faculty FROM faculty AS T1 JOIN campuses AS T2 ON T1.campus = T2.id WHERE T1.year = 2004 AND T2.campus = "San Francisco State University"	csu_1
SELECT T1.campus FROM campuses AS t1 JOIN faculty AS t2 ON t1.id = t2.campus WHERE t2.faculty >= 600 AND t2.faculty <= 1000 AND T1.year = 2004	csu_1
SELECT T1.campus FROM campuses AS t1 JOIN faculty AS t2 ON t1.id = t2.campus WHERE t2.faculty >= 600 AND t2.faculty <= 1000 AND T1.year = 2004	csu_1
SELECT T2.faculty FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = t2.campus JOIN degrees AS T3 ON T1.id = t3.campus AND t2.year = t3.year WHERE t2.year = 2002 ORDER BY t3.degrees DESC LIMIT 1	csu_1
SELECT T2.faculty FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = t2.campus JOIN degrees AS T3 ON T1.id = t3.campus AND t2.year = t3.year WHERE t2.year = 2002 ORDER BY t3.degrees DESC LIMIT 1	csu_1
SELECT T2.faculty FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = t2.campus JOIN degrees AS T3 ON T1.id = t3.campus AND t2.year = t3.year WHERE t2.year = 2001 ORDER BY t3.degrees LIMIT 1	csu_1
SELECT T2.faculty FROM campuses AS T1 JOIN faculty AS T2 ON T1.id = t2.campus JOIN degrees AS T3 ON T1.id = t3.campus AND t2.year = t3.year WHERE t2.year = 2001 ORDER BY t3.degrees LIMIT 1	csu_1
SELECT sum(t1.undergraduate) FROM discipline_enrollments AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t1.year = 2004 AND t2.campus = "San Jose State University"	csu_1
SELECT sum(t1.undergraduate) FROM discipline_enrollments AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t1.year = 2004 AND t2.campus = "San Jose State University"	csu_1
SELECT sum(t1.graduate) FROM discipline_enrollments AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t1.year = 2004 AND t2.campus = "San Francisco State University"	csu_1
SELECT sum(t1.graduate) FROM discipline_enrollments AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t1.year = 2004 AND t2.campus = "San Francisco State University"	csu_1
SELECT t1.campusfee FROM csu_fees AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t2.campus = "San Francisco State University" AND t1.year = 2000	csu_1
SELECT t1.campusfee FROM csu_fees AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t2.campus = "San Francisco State University" AND t1.year = 2000	csu_1
SELECT t1.campusfee FROM csu_fees AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t2.campus = "San Jose State University" AND t1.year = 2000	csu_1
SELECT t1.campusfee FROM csu_fees AS t1 JOIN campuses AS t2 ON t1.campus = t2.id WHERE t2.campus = "San Jose State University" AND t1.year = 2000	csu_1
SELECT count(*) FROM campuses	csu_1
SELECT count(*) FROM campuses	csu_1
SELECT count(*) FROM candidate	candidate_poll
SELECT count(*) FROM candidate	candidate_poll
SELECT poll_source FROM candidate GROUP BY poll_source ORDER BY count(*) DESC LIMIT 1	candidate_poll
SELECT poll_source FROM candidate GROUP BY poll_source ORDER BY count(*) DESC LIMIT 1	candidate_poll
SELECT support_rate FROM candidate ORDER BY support_rate DESC LIMIT 3	candidate_poll
SELECT support_rate FROM candidate ORDER BY support_rate DESC LIMIT 3	candidate_poll
SELECT Candidate_ID FROM candidate ORDER BY oppose_rate LIMIT 1	candidate_poll
SELECT Candidate_ID FROM candidate ORDER BY oppose_rate LIMIT 1	candidate_poll
SELECT Support_rate , Consider_rate , Oppose_rate FROM candidate ORDER BY unsure_rate	candidate_poll
SELECT Support_rate , Consider_rate , Oppose_rate FROM candidate ORDER BY unsure_rate	candidate_poll
SELECT poll_source FROM candidate ORDER BY oppose_rate DESC LIMIT 1	candidate_poll
SELECT poll_source FROM candidate ORDER BY oppose_rate DESC LIMIT 1	candidate_poll
SELECT name FROM people ORDER BY date_of_birth	candidate_poll
SELECT name FROM people ORDER BY date_of_birth	candidate_poll
SELECT avg(height) , avg(weight) FROM people WHERE sex = 'M'	candidate_poll
SELECT avg(height) , avg(weight) FROM people WHERE sex = 'M'	candidate_poll
SELECT name FROM people WHERE height > 200 OR height < 190	candidate_poll
SELECT name FROM people WHERE height > 200 OR height < 190	candidate_poll
SELECT avg(weight) , min(weight) , sex FROM people GROUP BY sex	candidate_poll
SELECT avg(weight) , min(weight) , sex FROM people GROUP BY sex	candidate_poll
SELECT t1.name , t1.sex FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id ORDER BY t2.support_rate DESC LIMIT 1	candidate_poll
SELECT t1.name , t1.sex FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id ORDER BY t2.support_rate DESC LIMIT 1	candidate_poll
SELECT t1.name , t1.sex , min(oppose_rate) FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id GROUP BY t1.sex	candidate_poll
SELECT t1.name , t1.sex , min(oppose_rate) FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id GROUP BY t1.sex	candidate_poll
SELECT t1.sex FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id GROUP BY t1.sex ORDER BY avg(t2.unsure_rate) DESC LIMIT 1	candidate_poll
SELECT t1.sex FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id GROUP BY t1.sex ORDER BY avg(t2.unsure_rate) DESC LIMIT 1	candidate_poll
SELECT name FROM people WHERE people_id NOT IN (SELECT people_id FROM candidate)	candidate_poll
SELECT name FROM people WHERE people_id NOT IN (SELECT people_id FROM candidate)	candidate_poll
SELECT t1.name FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id WHERE t2.support_rate < t2.oppose_rate	candidate_poll
SELECT t1.name FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id WHERE t2.support_rate < t2.oppose_rate	candidate_poll
SELECT count(*) , sex FROM people WHERE weight > 85 GROUP BY sex	candidate_poll
SELECT count(*) , sex FROM people WHERE weight > 85 GROUP BY sex	candidate_poll
SELECT max(support_rate) , min(consider_rate) , min(oppose_rate) FROM candidate	candidate_poll
SELECT max(support_rate) , min(consider_rate) , min(oppose_rate) FROM candidate	candidate_poll
SELECT t1.name FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id WHERE t1.sex = 'F' ORDER BY t1.name	candidate_poll
SELECT t1.name FROM people AS t1 JOIN candidate AS t2 ON t1.people_id = t2.people_id WHERE t1.sex = 'F' ORDER BY t1.name	candidate_poll
SELECT name FROM people WHERE height < (SELECT avg(height) FROM people)	candidate_poll
SELECT name FROM people WHERE height < (SELECT avg(height) FROM people)	candidate_poll
SELECT * FROM people	candidate_poll
SELECT * FROM people	candidate_poll
SELECT title FROM Movie WHERE director = 'Steven Spielberg'	movie_1
SELECT title FROM Movie WHERE director = 'Steven Spielberg'	movie_1
SELECT title FROM Movie WHERE director = 'James Cameron' AND YEAR > 2000	movie_1
SELECT title FROM Movie WHERE director = 'James Cameron' AND YEAR > 2000	movie_1
SELECT count(*) FROM Movie WHERE YEAR < 2000	movie_1
SELECT count(*) FROM Movie WHERE YEAR < 2000	movie_1
SELECT director FROM Movie WHERE title = 'Avatar'	movie_1
SELECT director FROM Movie WHERE title = 'Avatar'	movie_1
SELECT count(*) FROM Reviewer	movie_1
SELECT count(*) FROM Reviewer	movie_1
SELECT rID FROM Reviewer WHERE name LIKE "%Mike%"	movie_1
SELECT rID FROM Reviewer WHERE name LIKE "%Mike%"	movie_1
SELECT rID FROM Reviewer WHERE name = "Daniel Lewis"	movie_1
SELECT rID FROM Reviewer WHERE name = "Daniel Lewis"	movie_1
SELECT count(*) FROM Rating WHERE stars > 3	movie_1
SELECT count(*) FROM Rating WHERE stars > 3	movie_1
SELECT max(stars) , min(stars) FROM Rating	movie_1
SELECT max(stars) , min(stars) FROM Rating	movie_1
SELECT DISTINCT YEAR FROM Movie AS T1 JOIN Rating AS T2 ON T1.mID = T2.mID WHERE T2.stars >= 4 ORDER BY T1.year	movie_1
SELECT DISTINCT YEAR FROM Movie AS T1 JOIN Rating AS T2 ON T1.mID = T2.mID WHERE T2.stars >= 4 ORDER BY T1.year	movie_1
SELECT T1.director , T1.title FROM Movie AS T1 JOIN Rating AS T2 ON T1.mID = T2.mID WHERE T2.stars = 5	movie_1
SELECT T1.director , T1.title FROM Movie AS T1 JOIN Rating AS T2 ON T1.mID = T2.mID WHERE T2.stars = 5	movie_1
SELECT T2.name , avg(T1.stars) FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID GROUP BY T2.name	movie_1
SELECT T2.name , avg(T1.stars) FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID GROUP BY T2.name	movie_1
SELECT title FROM Movie WHERE mID NOT IN (SELECT mID FROM Rating)	movie_1
SELECT title FROM Movie WHERE mID NOT IN (SELECT mID FROM Rating)	movie_1
SELECT DISTINCT name FROM Reviewer AS T1 JOIN Rating AS T2 ON T1.rID = T2.rID WHERE ratingDate = "null"	movie_1
SELECT DISTINCT name FROM Reviewer AS T1 JOIN Rating AS T2 ON T1.rID = T2.rID WHERE ratingDate = "null"	movie_1
SELECT avg(T1.stars) , T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T2.year = (SELECT min(YEAR) FROM Movie)	movie_1
SELECT avg(T1.stars) , T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T2.year = (SELECT min(YEAR) FROM Movie)	movie_1
SELECT title FROM Movie WHERE YEAR = (SELECT max(YEAR) FROM Movie)	movie_1
SELECT title FROM Movie WHERE YEAR = (SELECT max(YEAR) FROM Movie)	movie_1
SELECT max(T1.stars) , T2.year FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T2.year = (SELECT max(YEAR) FROM Movie)	movie_1
SELECT max(T1.stars) , T2.year FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T2.year = (SELECT max(YEAR) FROM Movie)	movie_1
SELECT title FROM Movie WHERE YEAR > (SELECT max(YEAR) FROM Movie WHERE director = "Steven Spielberg")	movie_1
SELECT title FROM Movie WHERE YEAR > (SELECT max(YEAR) FROM Movie WHERE director = "Steven Spielberg")	movie_1
SELECT T2.title , T2.director FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars > (SELECT avg(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T2.director = "James Cameron")	movie_1
SELECT T2.title , T2.director FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars > (SELECT avg(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T2.director = "James Cameron")	movie_1
SELECT T3.name , T2.title , T1.stars , T1.ratingDate FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID ORDER BY T3.name , T2.title , T1.stars	movie_1
SELECT T3.name , T2.title , T1.stars , T1.ratingDate FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID ORDER BY T3.name , T2.title , T1.stars	movie_1
SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID GROUP BY T1.rID HAVING COUNT(*) >= 3	movie_1
SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID GROUP BY T1.rID HAVING COUNT(*) >= 3	movie_1
SELECT DISTINCT T3.name FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T2.title = 'Gone with the Wind'	movie_1
SELECT DISTINCT T3.name FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T2.title = 'Gone with the Wind'	movie_1
SELECT DISTINCT T2.director FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T3.name = 'Sarah Martinez'	movie_1
SELECT DISTINCT T2.director FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T3.name = 'Sarah Martinez'	movie_1
SELECT DISTINCT T3.name , T2.title , T1.stars FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T2.director = T3.name	movie_1
SELECT DISTINCT T3.name , T2.title , T1.stars FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T2.director = T3.name	movie_1
SELECT name FROM Reviewer UNION SELECT title FROM Movie	movie_1
SELECT name FROM Reviewer UNION SELECT title FROM Movie	movie_1
SELECT DISTINCT title FROM Movie EXCEPT SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T3.name = 'Chris Jackson'	movie_1
SELECT DISTINCT title FROM Movie EXCEPT SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T3.name = 'Chris Jackson'	movie_1
SELECT T1.title , T1.director FROM Movie AS T1 JOIN Movie AS T2 ON T1.director = T2.director WHERE T1.title != T2.title ORDER BY T1.director , T1.title	movie_1
SELECT T1.title , T1.director FROM Movie AS T1 JOIN Movie AS T2 ON T1.director = T2.director WHERE T1.title != T2.title ORDER BY T1.director , T1.title	movie_1
SELECT T1.title , T1.year FROM Movie AS T1 JOIN Movie AS T2 ON T1.director = T2.director WHERE T1.title != T2.title	movie_1
SELECT T1.title , T1.year FROM Movie AS T1 JOIN Movie AS T2 ON T1.director = T2.director WHERE T1.title != T2.title	movie_1
SELECT director FROM Movie GROUP BY director HAVING count(*) = 1	movie_1
SELECT director FROM Movie GROUP BY director HAVING count(*) = 1	movie_1
SELECT director FROM Movie WHERE director != "null" GROUP BY director HAVING count(*) = 1	movie_1
SELECT director FROM Movie WHERE director != "null" GROUP BY director HAVING count(*) = 1	movie_1
SELECT count(*) , T1.director FROM Movie AS T1 JOIN Rating AS T2 ON T1.mID = T2.mID GROUP BY T1.director	movie_1
SELECT count(*) , T1.director FROM Movie AS T1 JOIN Rating AS T2 ON T1.mID = T2.mID GROUP BY T1.director	movie_1
SELECT T2.title , avg(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.mID ORDER BY avg(T1.stars) DESC LIMIT 1	movie_1
SELECT T2.title , avg(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.mID ORDER BY avg(T1.stars) DESC LIMIT 1	movie_1
SELECT T2.title , avg(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.mID ORDER BY avg(T1.stars) LIMIT 1	movie_1
SELECT T2.title , avg(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.mID ORDER BY avg(T1.stars) LIMIT 1	movie_1
SELECT T2.title , T2.year FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID ORDER BY T1.stars DESC LIMIT 3	movie_1
SELECT T2.title , T2.year FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID ORDER BY T1.stars DESC LIMIT 3	movie_1
SELECT T2.title , T1.stars , T2.director , max(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE director != "null" GROUP BY director	movie_1
SELECT T2.title , T1.stars , T2.director , max(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE director != "null" GROUP BY director	movie_1
SELECT T2.title , T1.rID , T1.stars , min(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.rID	movie_1
SELECT T2.title , T1.rID , T1.stars , min(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.rID	movie_1
SELECT T2.title , T1.stars , T2.director , min(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T2.director	movie_1
SELECT T2.title , T1.stars , T2.director , min(T1.stars) FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T2.director	movie_1
SELECT T2.title , T1.mID FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.mID ORDER BY count(*) DESC LIMIT 1	movie_1
SELECT T2.title , T1.mID FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID GROUP BY T1.mID ORDER BY count(*) DESC LIMIT 1	movie_1
SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars BETWEEN 3 AND 5	movie_1
SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars BETWEEN 3 AND 5	movie_1
SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T1.stars > 3	movie_1
SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T1.stars > 3	movie_1
SELECT mID , avg(stars) FROM Rating WHERE mID NOT IN (SELECT T1.mID FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T2.name = "Brittany Harris") GROUP BY mID	movie_1
SELECT mID , avg(stars) FROM Rating WHERE mID NOT IN (SELECT T1.mID FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T2.name = "Brittany Harris") GROUP BY mID	movie_1
SELECT mID FROM Rating EXCEPT SELECT T1.mID FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T2.name = "Brittany Harris"	movie_1
SELECT mID FROM Rating EXCEPT SELECT T1.mID FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T2.name = "Brittany Harris"	movie_1
SELECT mID , avg(stars) FROM Rating GROUP BY mID HAVING count(*) >= 2	movie_1
SELECT mID , avg(stars) FROM Rating GROUP BY mID HAVING count(*) >= 2	movie_1
SELECT rID FROM Rating EXCEPT SELECT rID FROM Rating WHERE stars = 4	movie_1
SELECT rID FROM Rating EXCEPT SELECT rID FROM Rating WHERE stars = 4	movie_1
SELECT rID FROM Rating WHERE stars != 4	movie_1
SELECT rID FROM Rating WHERE stars != 4	movie_1
SELECT DISTINCT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T3.name = 'Brittany Harris' OR T2.year > 2000	movie_1
SELECT DISTINCT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID JOIN Reviewer AS T3 ON T1.rID = T3.rID WHERE T3.name = 'Brittany Harris' OR T2.year > 2000	movie_1
SELECT title FROM Movie WHERE director = "James Cameron" OR YEAR < 1980	movie_1
SELECT title FROM Movie WHERE director = "James Cameron" OR YEAR < 1980	movie_1
SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T1.stars = 3 INTERSECT SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T1.stars = 4	movie_1
SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T1.stars = 3 INTERSECT SELECT T2.name FROM Rating AS T1 JOIN Reviewer AS T2 ON T1.rID = T2.rID WHERE T1.stars = 4	movie_1
SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars = 3 INTERSECT SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars = 4	movie_1
SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars = 3 INTERSECT SELECT T2.title FROM Rating AS T1 JOIN Movie AS T2 ON T1.mID = T2.mID WHERE T1.stars = 4	movie_1
SELECT count(*) FROM region	storm_record
SELECT count(*) FROM region	storm_record
SELECT region_code , region_name FROM region ORDER BY region_code	storm_record
SELECT region_code , region_name FROM region ORDER BY region_code	storm_record
SELECT region_name FROM region ORDER BY region_name	storm_record
SELECT region_name FROM region ORDER BY region_name	storm_record
SELECT region_name FROM region WHERE region_name != 'Denmark'	storm_record
SELECT region_name FROM region WHERE region_name != 'Denmark'	storm_record
SELECT count(*) FROM storm WHERE Number_Deaths > 0	storm_record
SELECT count(*) FROM storm WHERE Number_Deaths > 0	storm_record
SELECT name , dates_active , number_deaths FROM storm WHERE number_deaths >= 1	storm_record
SELECT name , dates_active , number_deaths FROM storm WHERE number_deaths >= 1	storm_record
SELECT avg(damage_millions_USD) , max(damage_millions_USD) FROM storm WHERE max_speed > 1000	storm_record
SELECT avg(damage_millions_USD) , max(damage_millions_USD) FROM storm WHERE max_speed > 1000	storm_record
SELECT sum(number_deaths) , sum(damage_millions_USD) FROM storm WHERE max_speed > (SELECT avg(max_speed) FROM storm)	storm_record
SELECT sum(number_deaths) , sum(damage_millions_USD) FROM storm WHERE max_speed > (SELECT avg(max_speed) FROM storm)	storm_record
SELECT name , damage_millions_USD FROM storm ORDER BY max_speed DESC	storm_record
SELECT name , damage_millions_USD FROM storm ORDER BY max_speed DESC	storm_record
SELECT count(DISTINCT region_id) FROM affected_region	storm_record
SELECT count(DISTINCT region_id) FROM affected_region	storm_record
SELECT region_name FROM region WHERE region_id NOT IN (SELECT region_id FROM affected_region)	storm_record
SELECT region_name FROM region WHERE region_id NOT IN (SELECT region_id FROM affected_region)	storm_record
SELECT T1.region_name , count(*) FROM region AS T1 JOIN affected_region AS T2 ON T1.region_id = T2.region_id GROUP BY T1.region_id	storm_record
SELECT T1.region_name , count(*) FROM region AS T1 JOIN affected_region AS T2 ON T1.region_id = T2.region_id GROUP BY T1.region_id	storm_record
SELECT T1.name , count(*) FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id	storm_record
SELECT T1.name , count(*) FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id	storm_record
SELECT T1.name , T1.max_speed FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id ORDER BY count(*) DESC LIMIT 1	storm_record
SELECT T1.name , T1.max_speed FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id ORDER BY count(*) DESC LIMIT 1	storm_record
SELECT name FROM storm WHERE storm_id NOT IN (SELECT storm_id FROM affected_region)	storm_record
SELECT name FROM storm WHERE storm_id NOT IN (SELECT storm_id FROM affected_region)	storm_record
SELECT T1.name FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id HAVING count(*) >= 2 INTERSECT SELECT T1.name FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id HAVING sum(T2.number_city_affected) >= 10	storm_record
SELECT T1.name FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id HAVING count(*) >= 2 INTERSECT SELECT T1.name FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id HAVING sum(T2.number_city_affected) >= 10	storm_record
SELECT name FROM storm EXCEPT SELECT T1.name FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id HAVING count(*) >= 2	storm_record
SELECT name FROM storm EXCEPT SELECT T1.name FROM storm AS T1 JOIN affected_region AS T2 ON T1.storm_id = T2.storm_id GROUP BY T1.storm_id HAVING count(*) >= 2	storm_record
SELECT T2.region_name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T3.number_deaths >= 10	storm_record
SELECT T2.region_name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T3.number_deaths >= 10	storm_record
SELECT T3.name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T2.region_name = 'Denmark'	storm_record
SELECT T3.name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T2.region_name = 'Denmark'	storm_record
SELECT T1.region_name FROM region AS T1 JOIN affected_region AS T2 ON T1.region_id = T2.region_id GROUP BY T1.region_id HAVING count(*) >= 2	storm_record
SELECT T1.region_name FROM region AS T1 JOIN affected_region AS T2 ON T1.region_id = T2.region_id GROUP BY T1.region_id HAVING count(*) >= 2	storm_record
SELECT T2.region_name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id ORDER BY T3.Number_Deaths DESC LIMIT 1	storm_record
SELECT T2.region_name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id ORDER BY T3.Number_Deaths DESC LIMIT 1	storm_record
SELECT T3.Name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T2.Region_name = 'Afghanistan' INTERSECT SELECT T3.Name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T2.Region_name = 'Albania'	storm_record
SELECT T3.Name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T2.Region_name = 'Afghanistan' INTERSECT SELECT T3.Name FROM affected_region AS T1 JOIN region AS T2 ON T1.region_id = T2.region_id JOIN storm AS T3 ON T1.storm_id = T3.storm_id WHERE T2.Region_name = 'Albania'	storm_record
SELECT count(*) FROM county	election
SELECT count(*) FROM county	election
SELECT County_name , Population FROM county	election
SELECT County_name , Population FROM county	election
SELECT avg(Population) FROM county	election
SELECT avg(Population) FROM county	election
SELECT max(Population) , min(Population) FROM county	election
SELECT max(Population) , min(Population) FROM county	election
SELECT DISTINCT District FROM election	election
SELECT DISTINCT District FROM election	election
SELECT Zip_code FROM county WHERE County_name = "Howard"	election
SELECT Zip_code FROM county WHERE County_name = "Howard"	election
SELECT Delegate FROM election WHERE District = 1	election
SELECT Delegate FROM election WHERE District = 1	election
SELECT Delegate , Committee FROM election	election
SELECT Delegate , Committee FROM election	election
SELECT count(DISTINCT Governor) FROM party	election
SELECT count(DISTINCT Governor) FROM party	election
SELECT Lieutenant_Governor , Comptroller FROM party WHERE Party = "Democratic"	election
SELECT Lieutenant_Governor , Comptroller FROM party WHERE Party = "Democratic"	election
SELECT DISTINCT YEAR FROM party WHERE Governor = "Eliot Spitzer"	election
SELECT DISTINCT YEAR FROM party WHERE Governor = "Eliot Spitzer"	election
SELECT * FROM election	election
SELECT * FROM election	election
SELECT T2.Delegate , T1.County_name FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District	election
SELECT T2.Delegate , T1.County_name FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District	election
SELECT T2.Delegate FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District WHERE T1.Population < 100000	election
SELECT T2.Delegate FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District WHERE T1.Population < 100000	election
SELECT count(DISTINCT T2.Delegate) FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District WHERE T1.Population > 50000	election
SELECT count(DISTINCT T2.Delegate) FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District WHERE T1.Population > 50000	election
SELECT T1.County_name FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District WHERE T2.Committee = "Appropriations"	election
SELECT T1.County_name FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District WHERE T2.Committee = "Appropriations"	election
SELECT T1.Delegate , T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID	election
SELECT T1.Delegate , T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID	election
SELECT T2.Governor FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.District = 1	election
SELECT T2.Governor FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.District = 1	election
SELECT T2.Comptroller FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.District = 1 OR T1.District = 2	election
SELECT T2.Comptroller FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.District = 1 OR T1.District = 2	election
SELECT T1.Committee FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T2.Party = "Democratic"	election
SELECT T1.Committee FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T2.Party = "Democratic"	election
SELECT T1.County_name , COUNT(*) FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District GROUP BY T1.County_id	election
SELECT T1.County_name , COUNT(*) FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District GROUP BY T1.County_id	election
SELECT T2.Party , COUNT(*) FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID GROUP BY T1.Party	election
SELECT T2.Party , COUNT(*) FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID GROUP BY T1.Party	election
SELECT County_name FROM county ORDER BY Population ASC	election
SELECT County_name FROM county ORDER BY Population ASC	election
SELECT County_name FROM county ORDER BY County_name DESC	election
SELECT County_name FROM county ORDER BY County_name DESC	election
SELECT County_name FROM county ORDER BY Population DESC LIMIT 1	election
SELECT County_name FROM county ORDER BY Population DESC LIMIT 1	election
SELECT County_name FROM county ORDER BY Population ASC LIMIT 3	election
SELECT County_name FROM county ORDER BY Population ASC LIMIT 3	election
SELECT T1.County_name FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District GROUP BY T1.County_id HAVING COUNT(*) >= 2	election
SELECT T1.County_name FROM county AS T1 JOIN election AS T2 ON T1.County_id = T2.District GROUP BY T1.County_id HAVING COUNT(*) >= 2	election
SELECT Party FROM party GROUP BY Party HAVING COUNT(*) >= 2	election
SELECT Party FROM party GROUP BY Party HAVING COUNT(*) >= 2	election
SELECT T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID GROUP BY T1.Party ORDER BY COUNT(*) DESC LIMIT 1	election
SELECT T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID GROUP BY T1.Party ORDER BY COUNT(*) DESC LIMIT 1	election
SELECT Governor FROM party GROUP BY Governor ORDER BY COUNT(*) DESC LIMIT 1	election
SELECT Governor FROM party GROUP BY Governor ORDER BY COUNT(*) DESC LIMIT 1	election
SELECT Comptroller , COUNT(*) FROM party GROUP BY Comptroller ORDER BY COUNT(*) DESC LIMIT 1	election
SELECT Comptroller , COUNT(*) FROM party GROUP BY Comptroller ORDER BY COUNT(*) DESC LIMIT 1	election
SELECT Party FROM party WHERE Party_ID NOT IN (SELECT Party FROM election)	election
SELECT Party FROM party WHERE Party_ID NOT IN (SELECT Party FROM election)	election
SELECT T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.Committee = "Appropriations" INTERSECT SELECT T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.Committee = "Economic Matters"	election
SELECT T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.Committee = "Appropriations" INTERSECT SELECT T2.Party FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T1.Committee = "Economic Matters"	election
SELECT T1.Committee FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T2.Party = "Democratic" INTERSECT SELECT T1.Committee FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T2.Party = "Liberal"	election
SELECT T1.Committee FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T2.Party = "Democratic" INTERSECT SELECT T1.Committee FROM election AS T1 JOIN party AS T2 ON T1.Party = T2.Party_ID WHERE T2.Party = "Liberal"	election
SELECT count(*) FROM journalist	news_report
SELECT Name FROM journalist ORDER BY Years_working ASC	news_report
SELECT Nationality , Age FROM journalist	news_report
SELECT Name FROM journalist WHERE Nationality = "England" OR Nationality = "Wales"	news_report
SELECT avg(Years_working) FROM journalist	news_report
SELECT Nationality FROM journalist ORDER BY Years_working DESC LIMIT 1	news_report
SELECT Nationality , COUNT(*) FROM journalist GROUP BY Nationality	news_report
SELECT Nationality FROM journalist GROUP BY Nationality ORDER BY COUNT(*) DESC LIMIT 1	news_report
SELECT Nationality FROM journalist WHERE Years_working > 10 INTERSECT SELECT Nationality FROM journalist WHERE Years_working < 3	news_report
SELECT Date , Name , venue FROM event ORDER BY Event_Attendance DESC	news_report
SELECT T3.Name , T2.Date FROM news_report AS T1 JOIN event AS T2 ON T1.Event_ID = T2.Event_ID JOIN journalist AS T3 ON T1.journalist_ID = T3.journalist_ID	news_report
SELECT T3.Name , T2.Name FROM news_report AS T1 JOIN event AS T2 ON T1.Event_ID = T2.Event_ID JOIN journalist AS T3 ON T1.journalist_ID = T3.journalist_ID ORDER BY T2.Event_Attendance ASC	news_report
SELECT T3.Name , COUNT(*) FROM news_report AS T1 JOIN event AS T2 ON T1.Event_ID = T2.Event_ID JOIN journalist AS T3 ON T1.journalist_ID = T3.journalist_ID GROUP BY T3.Name	news_report
SELECT T3.Name FROM news_report AS T1 JOIN event AS T2 ON T1.Event_ID = T2.Event_ID JOIN journalist AS T3 ON T1.journalist_ID = T3.journalist_ID GROUP BY T3.Name HAVING COUNT(*) > 1	news_report
SELECT Name FROM journalist WHERE journalist_ID NOT IN (SELECT journalist_ID FROM news_report)	news_report
SELECT avg(Event_Attendance) , max(Event_Attendance) FROM event	news_report
SELECT avg(t1.age) , avg(Years_working) , t2.work_type FROM journalist AS t1 JOIN news_report AS t2 ON t1.journalist_id = t2.journalist_id GROUP BY t2.work_type	news_report
SELECT venue , name FROM event ORDER BY Event_Attendance DESC LIMIT 2	news_report
SELECT actual_order_id FROM actual_orders WHERE order_status_code = 'Success'	customer_deliveries
SELECT t1.product_name , t1.product_price FROM products AS t1 JOIN regular_order_products AS t2 ON t1.product_id = t2.product_id GROUP BY t2.product_id ORDER BY count(*) DESC LIMIT 1	customer_deliveries
SELECT count(*) FROM customers	customer_deliveries
SELECT count(DISTINCT payment_method) FROM customers	customer_deliveries
SELECT truck_details FROM trucks ORDER BY truck_licence_number	customer_deliveries
SELECT product_name FROM products ORDER BY product_price DESC LIMIT 1	customer_deliveries
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t3.state_province_county = 'California'	customer_deliveries
SELECT customer_email , customer_name FROM customers WHERE payment_method = 'Visa'	customer_deliveries
SELECT t1.customer_name , t1.customer_phone FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t3.state_province_county = 'California'	customer_deliveries
SELECT state_province_county FROM addresses WHERE address_id NOT IN (SELECT employee_address_id FROM Employees)	customer_deliveries
SELECT customer_name , customer_phone , customer_email FROM Customers ORDER BY date_became_customer	customer_deliveries
SELECT customer_name FROM Customers ORDER BY date_became_customer LIMIT 5	customer_deliveries
SELECT payment_method FROM Customers GROUP BY payment_method ORDER BY count(*) DESC LIMIT 1	customer_deliveries
SELECT route_name FROM Delivery_Routes ORDER BY route_name	customer_deliveries
SELECT t1.route_name FROM Delivery_Routes AS t1 JOIN Delivery_Route_Locations AS t2 ON t1.route_id = t2.route_id GROUP BY t1.route_id ORDER BY count(*) DESC LIMIT 1	customer_deliveries
SELECT t2.state_province_county , count(*) FROM customer_addresses AS t1 JOIN addresses AS t2 ON t1.address_id = t2.address_id GROUP BY t2.state_province_county	customer_deliveries
SELECT count(*) FROM authors	icfp_1
SELECT count(*) FROM authors	icfp_1
SELECT count(*) FROM inst	icfp_1
SELECT count(*) FROM inst	icfp_1
SELECT count(*) FROM papers	icfp_1
SELECT count(*) FROM papers	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Jeremy" AND t1.lname = "Gibbons"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Jeremy" AND t1.lname = "Gibbons"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Aaron" AND t1.lname = "Turon"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Aaron" AND t1.lname = "Turon"	icfp_1
SELECT count(*) FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Atsushi" AND t1.lname = "Ohori"	icfp_1
SELECT count(*) FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Atsushi" AND t1.lname = "Ohori"	icfp_1
SELECT DISTINCT t3.name FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t1.fname = "Matthias" AND t1.lname = "Blume"	icfp_1
SELECT DISTINCT t3.name FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t1.fname = "Matthias" AND t1.lname = "Blume"	icfp_1
SELECT DISTINCT t3.name FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t1.fname = "Katsuhiro" AND t1.lname = "Ueno"	icfp_1
SELECT DISTINCT t3.name FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t1.fname = "Katsuhiro" AND t1.lname = "Ueno"	icfp_1
SELECT DISTINCT t1.fname , t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "University of Oxford"	icfp_1
SELECT DISTINCT t1.fname , t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "University of Oxford"	icfp_1
SELECT DISTINCT t1.fname , t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Google"	icfp_1
SELECT DISTINCT t1.fname , t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Google"	icfp_1
SELECT t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title = "Binders Unbound"	icfp_1
SELECT t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title = "Binders Unbound"	icfp_1
SELECT t1.fname , t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title = "Nameless , Painless"	icfp_1
SELECT t1.fname , t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title = "Nameless , Painless"	icfp_1
SELECT DISTINCT t1.title FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Indiana University"	icfp_1
SELECT DISTINCT t1.title FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Indiana University"	icfp_1
SELECT DISTINCT t1.title FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Google"	icfp_1
SELECT DISTINCT t1.title FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Google"	icfp_1
SELECT count(DISTINCT t1.title) FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Tokohu University"	icfp_1
SELECT count(DISTINCT t1.title) FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "Tokohu University"	icfp_1
SELECT count(DISTINCT t1.title) FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "University of Pennsylvania"	icfp_1
SELECT count(DISTINCT t1.title) FROM papers AS t1 JOIN authorship AS t2 ON t1.paperid = t2.paperid JOIN inst AS t3 ON t2.instid = t3.instid WHERE t3.name = "University of Pennsylvania"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Olin" AND t1.lname = "Shivers"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Olin" AND t1.lname = "Shivers"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Stephanie" AND t1.lname = "Weirich"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t1.fname = "Stephanie" AND t1.lname = "Weirich"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid JOIN inst AS t4 ON t2.instid = t4.instid WHERE t4.country = "USA" AND t2.authorder = 2 AND t1.lname = "Turon"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid JOIN inst AS t4 ON t2.instid = t4.instid WHERE t4.country = "USA" AND t2.authorder = 2 AND t1.lname = "Turon"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid JOIN inst AS t4 ON t2.instid = t4.instid WHERE t4.country = "Japan" AND t2.authorder = 1 AND t1.lname = "Ohori"	icfp_1
SELECT t3.title FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid JOIN inst AS t4 ON t2.instid = t4.instid WHERE t4.country = "Japan" AND t2.authorder = 1 AND t1.lname = "Ohori"	icfp_1
SELECT t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid GROUP BY t1.fname , t1.lname ORDER BY count(*) DESC LIMIT 1	icfp_1
SELECT t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid GROUP BY t1.fname , t1.lname ORDER BY count(*) DESC LIMIT 1	icfp_1
SELECT t1.country FROM inst AS t1 JOIN authorship AS t2 ON t1.instid = t2.instid JOIN papers AS t3 ON t2.paperid = t3.paperid GROUP BY t1.country ORDER BY count(*) DESC LIMIT 1	icfp_1
SELECT t1.country FROM inst AS t1 JOIN authorship AS t2 ON t1.instid = t2.instid JOIN papers AS t3 ON t2.paperid = t3.paperid GROUP BY t1.country ORDER BY count(*) DESC LIMIT 1	icfp_1
SELECT t1.name FROM inst AS t1 JOIN authorship AS t2 ON t1.instid = t2.instid JOIN papers AS t3 ON t2.paperid = t3.paperid GROUP BY t1.name ORDER BY count(*) DESC LIMIT 1	icfp_1
SELECT t1.name FROM inst AS t1 JOIN authorship AS t2 ON t1.instid = t2.instid JOIN papers AS t3 ON t2.paperid = t3.paperid GROUP BY t1.name ORDER BY count(*) DESC LIMIT 1	icfp_1
SELECT title FROM papers WHERE title LIKE "%ML%"	icfp_1
SELECT title FROM papers WHERE title LIKE "%ML%"	icfp_1
SELECT title FROM papers WHERE title LIKE "%Database%"	icfp_1
SELECT title FROM papers WHERE title LIKE "%Database%"	icfp_1
SELECT t1.fname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title LIKE "%Functional%"	icfp_1
SELECT t1.fname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title LIKE "%Functional%"	icfp_1
SELECT t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title LIKE "%Monadic%"	icfp_1
SELECT t1.lname FROM authors AS t1 JOIN authorship AS t2 ON t1.authid = t2.authid JOIN papers AS t3 ON t2.paperid = t3.paperid WHERE t3.title LIKE "%Monadic%"	icfp_1
SELECT t2.title FROM authorship AS t1 JOIN papers AS t2 ON t1.paperid = t2.paperid WHERE t1.authorder = (SELECT max(authorder) FROM authorship)	icfp_1
SELECT t2.title FROM authorship AS t1 JOIN papers AS t2 ON t1.paperid = t2.paperid WHERE t1.authorder = (SELECT max(authorder) FROM authorship)	icfp_1
SELECT fname FROM authors WHERE lname = "Ueno"	icfp_1
SELECT fname FROM authors WHERE lname = "Ueno"	icfp_1
SELECT lname FROM authors WHERE fname = "Amal"	icfp_1
SELECT lname FROM authors WHERE fname = "Amal"	icfp_1
SELECT fname FROM authors ORDER BY fname	icfp_1
SELECT fname FROM authors ORDER BY fname	icfp_1
SELECT lname FROM authors ORDER BY lname	icfp_1
SELECT lname FROM authors ORDER BY lname	icfp_1
SELECT fname , lname FROM authors ORDER BY lname	icfp_1
SELECT fname , lname FROM authors ORDER BY lname	icfp_1
SELECT count(DISTINCT last_name) FROM actor	sakila_1
SELECT count(DISTINCT last_name) FROM actor	sakila_1
SELECT first_name FROM actor GROUP BY first_name ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT first_name FROM actor GROUP BY first_name ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT first_name , last_name FROM actor GROUP BY first_name , last_name ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT first_name , last_name FROM actor GROUP BY first_name , last_name ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT district FROM address GROUP BY district HAVING count(*) >= 2	sakila_1
SELECT district FROM address GROUP BY district HAVING count(*) >= 2	sakila_1
SELECT phone , postal_code FROM address WHERE address = '1031 Daugavpils Parkway'	sakila_1
SELECT phone , postal_code FROM address WHERE address = '1031 Daugavpils Parkway'	sakila_1
SELECT T2.city , count(*) , T1.city_id FROM address AS T1 JOIN city AS T2 ON T1.city_id = T2.city_id GROUP BY T1.city_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T2.city , count(*) , T1.city_id FROM address AS T1 JOIN city AS T2 ON T1.city_id = T2.city_id GROUP BY T1.city_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT count(*) FROM address WHERE district = 'California'	sakila_1
SELECT count(*) FROM address WHERE district = 'California'	sakila_1
SELECT title , film_id FROM film WHERE rental_rate = 0.99 INTERSECT SELECT T1.title , T1.film_id FROM film AS T1 JOIN inventory AS T2 ON T1.film_id = T2.film_id GROUP BY T1.film_id HAVING count(*) < 3	sakila_1
SELECT title , film_id FROM film WHERE rental_rate = 0.99 INTERSECT SELECT T1.title , T1.film_id FROM film AS T1 JOIN inventory AS T2 ON T1.film_id = T2.film_id GROUP BY T1.film_id HAVING count(*) < 3	sakila_1
SELECT count(*) FROM city AS T1 JOIN country AS T2 ON T1.country_id = T2.country_id WHERE T2.country = 'Australia'	sakila_1
SELECT count(*) FROM city AS T1 JOIN country AS T2 ON T1.country_id = T2.country_id WHERE T2.country = 'Australia'	sakila_1
SELECT T2.country FROM city AS T1 JOIN country AS T2 ON T1.country_id = T2.country_id GROUP BY T2.country_id HAVING count(*) >= 3	sakila_1
SELECT T2.country FROM city AS T1 JOIN country AS T2 ON T1.country_id = T2.country_id GROUP BY T2.country_id HAVING count(*) >= 3	sakila_1
SELECT payment_date FROM payment WHERE amount > 10 UNION SELECT T1.payment_date FROM payment AS T1 JOIN staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = 'Elsa'	sakila_1
SELECT payment_date FROM payment WHERE amount > 10 UNION SELECT T1.payment_date FROM payment AS T1 JOIN staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = 'Elsa'	sakila_1
SELECT count(*) FROM customer WHERE active = '1'	sakila_1
SELECT count(*) FROM customer WHERE active = '1'	sakila_1
SELECT title , rental_rate FROM film ORDER BY rental_rate DESC LIMIT 1	sakila_1
SELECT title , rental_rate FROM film ORDER BY rental_rate DESC LIMIT 1	sakila_1
SELECT T2.title , T2.film_id , T2.description FROM film_actor AS T1 JOIN film AS T2 ON T1.film_id = T2.film_id GROUP BY T2.film_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T2.title , T2.film_id , T2.description FROM film_actor AS T1 JOIN film AS T2 ON T1.film_id = T2.film_id GROUP BY T2.film_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T2.first_name , T2.last_name , T2.actor_id FROM film_actor AS T1 JOIN actor AS T2 ON T1.actor_id = T2.actor_id GROUP BY T2.actor_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T2.first_name , T2.last_name , T2.actor_id FROM film_actor AS T1 JOIN actor AS T2 ON T1.actor_id = T2.actor_id GROUP BY T2.actor_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T2.first_name , T2.last_name FROM film_actor AS T1 JOIN actor AS T2 ON T1.actor_id = T2.actor_id GROUP BY T2.actor_id HAVING count(*) > 30	sakila_1
SELECT T2.first_name , T2.last_name FROM film_actor AS T1 JOIN actor AS T2 ON T1.actor_id = T2.actor_id GROUP BY T2.actor_id HAVING count(*) > 30	sakila_1
SELECT store_id FROM inventory GROUP BY store_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT store_id FROM inventory GROUP BY store_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT sum(amount) FROM payment	sakila_1
SELECT sum(amount) FROM payment	sakila_1
SELECT T1.first_name , T1.last_name , T1.customer_id FROM customer AS T1 JOIN payment AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY sum(amount) ASC LIMIT 1	sakila_1
SELECT T1.first_name , T1.last_name , T1.customer_id FROM customer AS T1 JOIN payment AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY sum(amount) ASC LIMIT 1	sakila_1
SELECT T1.name FROM category AS T1 JOIN film_category AS T2 ON T1.category_id = T2.category_id JOIN film AS T3 ON T2.film_id = T3.film_id WHERE T3.title = 'HUNGER ROOF'	sakila_1
SELECT T1.name FROM category AS T1 JOIN film_category AS T2 ON T1.category_id = T2.category_id JOIN film AS T3 ON T2.film_id = T3.film_id WHERE T3.title = 'HUNGER ROOF'	sakila_1
SELECT T2.name , T1.category_id , count(*) FROM film_category AS T1 JOIN category AS T2 ON T1.category_id = T2.category_id GROUP BY T1.category_id	sakila_1
SELECT T2.name , T1.category_id , count(*) FROM film_category AS T1 JOIN category AS T2 ON T1.category_id = T2.category_id GROUP BY T1.category_id	sakila_1
SELECT T1.title , T1.film_id FROM film AS T1 JOIN inventory AS T2 ON T1.film_id = T2.film_id GROUP BY T1.film_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T1.title , T1.film_id FROM film AS T1 JOIN inventory AS T2 ON T1.film_id = T2.film_id GROUP BY T1.film_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T1.title , T2.inventory_id FROM film AS T1 JOIN inventory AS T2 ON T1.film_id = T2.film_id JOIN rental AS T3 ON T2.inventory_id = T3.inventory_id GROUP BY T2.inventory_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT T1.title , T2.inventory_id FROM film AS T1 JOIN inventory AS T2 ON T1.film_id = T2.film_id JOIN rental AS T3 ON T2.inventory_id = T3.inventory_id GROUP BY T2.inventory_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT count(DISTINCT language_id) FROM film	sakila_1
SELECT count(DISTINCT language_id) FROM film	sakila_1
SELECT title FROM film WHERE rating = 'R'	sakila_1
SELECT title FROM film WHERE rating = 'R'	sakila_1
SELECT T2.address FROM store AS T1 JOIN address AS T2 ON T1.address_id = T2.address_id WHERE store_id = 1	sakila_1
SELECT T2.address FROM store AS T1 JOIN address AS T2 ON T1.address_id = T2.address_id WHERE store_id = 1	sakila_1
SELECT T1.first_name , T1.last_name , T1.staff_id FROM staff AS T1 JOIN payment AS T2 ON T1.staff_id = T2.staff_id GROUP BY T1.staff_id ORDER BY count(*) ASC LIMIT 1	sakila_1
SELECT T1.first_name , T1.last_name , T1.staff_id FROM staff AS T1 JOIN payment AS T2 ON T1.staff_id = T2.staff_id GROUP BY T1.staff_id ORDER BY count(*) ASC LIMIT 1	sakila_1
SELECT T2.name FROM film AS T1 JOIN LANGUAGE AS T2 ON T1.language_id = T2.language_id WHERE T1.title = 'AIRPORT POLLOCK'	sakila_1
SELECT T2.name FROM film AS T1 JOIN LANGUAGE AS T2 ON T1.language_id = T2.language_id WHERE T1.title = 'AIRPORT POLLOCK'	sakila_1
SELECT count(*) FROM store	sakila_1
SELECT count(*) FROM store	sakila_1
SELECT count(DISTINCT rating) FROM film	sakila_1
SELECT count(DISTINCT rating) FROM film	sakila_1
SELECT title FROM film WHERE special_features LIKE '%Deleted Scenes%'	sakila_1
SELECT title FROM film WHERE special_features LIKE '%Deleted Scenes%'	sakila_1
SELECT count(*) FROM inventory WHERE store_id = 1	sakila_1
SELECT count(*) FROM inventory WHERE store_id = 1	sakila_1
SELECT payment_date FROM payment ORDER BY payment_date ASC LIMIT 1	sakila_1
SELECT payment_date FROM payment ORDER BY payment_date ASC LIMIT 1	sakila_1
SELECT T2.address , T1.email FROM customer AS T1 JOIN address AS T2 ON T2.address_id = T1.address_id WHERE T1.first_name = 'LINDA'	sakila_1
SELECT T2.address , T1.email FROM customer AS T1 JOIN address AS T2 ON T2.address_id = T1.address_id WHERE T1.first_name = 'LINDA'	sakila_1
SELECT title FROM film WHERE LENGTH > 100 OR rating = 'PG' EXCEPT SELECT title FROM film WHERE replacement_cost > 200	sakila_1
SELECT title FROM film WHERE LENGTH > 100 OR rating = 'PG' EXCEPT SELECT title FROM film WHERE replacement_cost > 200	sakila_1
SELECT T1.first_name , T1.last_name FROM customer AS T1 JOIN rental AS T2 ON T1.customer_id = T2.customer_id ORDER BY T2.rental_date ASC LIMIT 1	sakila_1
SELECT T1.first_name , T1.last_name FROM customer AS T1 JOIN rental AS T2 ON T1.customer_id = T2.customer_id ORDER BY T2.rental_date ASC LIMIT 1	sakila_1
SELECT DISTINCT T1.first_name , T1.last_name FROM staff AS T1 JOIN rental AS T2 ON T1.staff_id = T2.staff_id JOIN customer AS T3 ON T2.customer_id = T3.customer_id WHERE T3.first_name = 'APRIL' AND T3.last_name = 'BURNS'	sakila_1
SELECT DISTINCT T1.first_name , T1.last_name FROM staff AS T1 JOIN rental AS T2 ON T1.staff_id = T2.staff_id JOIN customer AS T3 ON T2.customer_id = T3.customer_id WHERE T3.first_name = 'APRIL' AND T3.last_name = 'BURNS'	sakila_1
SELECT store_id FROM customer GROUP BY store_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT store_id FROM customer GROUP BY store_id ORDER BY count(*) DESC LIMIT 1	sakila_1
SELECT amount FROM payment ORDER BY amount DESC LIMIT 1	sakila_1
SELECT amount FROM payment ORDER BY amount DESC LIMIT 1	sakila_1
SELECT T2.address FROM staff AS T1 JOIN address AS T2 ON T1.address_id = T2.address_id WHERE T1.first_name = 'Elsa'	sakila_1
SELECT T2.address FROM staff AS T1 JOIN address AS T2 ON T1.address_id = T2.address_id WHERE T1.first_name = 'Elsa'	sakila_1
SELECT first_name FROM customer WHERE customer_id NOT IN( SELECT customer_id FROM rental WHERE rental_date > '2005-08-23 02:06:01' )	sakila_1
SELECT first_name FROM customer WHERE customer_id NOT IN( SELECT customer_id FROM rental WHERE rental_date > '2005-08-23 02:06:01' )	sakila_1
SELECT count(*) FROM ASSESSMENT_NOTES	behavior_monitoring
SELECT date_of_notes FROM Assessment_Notes	behavior_monitoring
SELECT count(*) FROM ADDRESSES WHERE zip_postcode = "197"	behavior_monitoring
SELECT count(DISTINCT incident_type_code) FROM Behavior_Incident	behavior_monitoring
SELECT DISTINCT detention_type_code FROM Detention	behavior_monitoring
SELECT date_incident_start , date_incident_end FROM Behavior_Incident WHERE incident_type_code = "NOISE"	behavior_monitoring
SELECT detention_summary FROM Detention	behavior_monitoring
SELECT cell_mobile_number , email_address FROM STUDENTS	behavior_monitoring
SELECT email_address FROM Students WHERE first_name = "Emma" AND last_name = "Rohan"	behavior_monitoring
SELECT count(DISTINCT student_id) FROM Students_in_Detention	behavior_monitoring
SELECT gender FROM TEACHERS WHERE last_name = "Medhurst"	behavior_monitoring
SELECT incident_type_description FROM Ref_Incident_Type WHERE incident_type_code = "VIOLENCE"	behavior_monitoring
SELECT max(monthly_rental) , min(monthly_rental) FROM Student_Addresses	behavior_monitoring
SELECT first_name FROM Teachers WHERE email_address LIKE '%man%'	behavior_monitoring
SELECT * FROM Assessment_Notes ORDER BY date_of_notes ASC	behavior_monitoring
SELECT city FROM Addresses ORDER BY city	behavior_monitoring
SELECT first_name , last_name FROM Teachers ORDER BY last_name	behavior_monitoring
SELECT * FROM Student_Addresses ORDER BY monthly_rental DESC	behavior_monitoring
SELECT T1.student_id , T2.first_name FROM Assessment_Notes AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1	behavior_monitoring
SELECT T1.teacher_id , T2.first_name FROM Assessment_Notes AS T1 JOIN Teachers AS T2 ON T1.teacher_id = T2.teacher_id GROUP BY T1.teacher_id ORDER BY count(*) DESC LIMIT 3	behavior_monitoring
SELECT T1.student_id , T2.last_name FROM Behavior_Incident AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1	behavior_monitoring
SELECT T1.teacher_id , T2.last_name FROM Detention AS T1 JOIN Teachers AS T2 ON T1.teacher_id = T2.teacher_id WHERE T1.detention_type_code = "AFTER" GROUP BY T1.teacher_id ORDER BY count(*) DESC LIMIT 1	behavior_monitoring
SELECT T1.student_id , T2.first_name FROM Student_Addresses AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY AVG(monthly_rental) DESC LIMIT 1	behavior_monitoring
SELECT T2.address_id , T1.city FROM Addresses AS T1 JOIN Student_Addresses AS T2 ON T1.address_id = T2.address_id GROUP BY T2.address_id ORDER BY AVG(monthly_rental) DESC LIMIT 1	behavior_monitoring
SELECT T1.incident_type_code , T2.incident_type_description FROM Behavior_Incident AS T1 JOIN Ref_Incident_Type AS T2 ON T1.incident_type_code = T2.incident_type_code GROUP BY T1.incident_type_code ORDER BY count(*) DESC LIMIT 1	behavior_monitoring
SELECT T1.detention_type_code , T2.detention_type_description FROM Detention AS T1 JOIN Ref_Detention_Type AS T2 ON T1.detention_type_code = T2.detention_type_code GROUP BY T1.detention_type_code ORDER BY count(*) ASC LIMIT 1	behavior_monitoring
SELECT T1.date_of_notes FROM Assessment_Notes AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id WHERE T2.first_name = "Fanny"	behavior_monitoring
SELECT T1.text_of_notes FROM Assessment_Notes AS T1 JOIN Teachers AS T2 ON T1.teacher_id = T2.teacher_id WHERE T2.last_name = "Schuster"	behavior_monitoring
SELECT T1.date_incident_start , date_incident_end FROM Behavior_Incident AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id WHERE T2.last_name = "Fahey"	behavior_monitoring
SELECT T1.datetime_detention_start , datetime_detention_end FROM Detention AS T1 JOIN Teachers AS T2 ON T1.teacher_id = T2.teacher_id WHERE T2.last_name = "Schultz"	behavior_monitoring
SELECT T2.address_id , T1.zip_postcode FROM Addresses AS T1 JOIN Student_Addresses AS T2 ON T1.address_id = T2.address_id ORDER BY monthly_rental DESC LIMIT 1	behavior_monitoring
SELECT T2.cell_mobile_number FROM Student_Addresses AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id ORDER BY T1.monthly_rental ASC LIMIT 1	behavior_monitoring
SELECT T2.monthly_rental FROM Addresses AS T1 JOIN Student_Addresses AS T2 ON T1.address_id = T2.address_id WHERE T1.state_province_county = "Texas"	behavior_monitoring
SELECT T2.first_name , T2.last_name FROM Addresses AS T1 JOIN Students AS T2 ON T1.address_id = T2.address_id WHERE T1.state_province_county = "Wisconsin"	behavior_monitoring
SELECT T1.line_1 , avg(T2.monthly_rental) FROM Addresses AS T1 JOIN Student_Addresses AS T2 ON T1.address_id = T2.address_id GROUP BY T2.address_id	behavior_monitoring
SELECT T1.zip_postcode FROM Addresses AS T1 JOIN Teachers AS T2 ON T1.address_id = T2.address_id WHERE T2.first_name = "Lyla"	behavior_monitoring
SELECT T2.email_address FROM Addresses AS T1 JOIN Teachers AS T2 ON T1.address_id = T2.address_id WHERE T1.zip_postcode = "918"	behavior_monitoring
SELECT count(*) FROM STUDENTS WHERE student_id NOT IN ( SELECT student_id FROM Behavior_Incident )	behavior_monitoring
SELECT last_name FROM Teachers EXCEPT SELECT T1.last_name FROM Teachers AS T1 JOIN Detention AS T2 ON T1.teacher_id = T2.teacher_id	behavior_monitoring
SELECT T1.line_1 FROM Addresses AS T1 JOIN Students AS T2 ON T1.address_id = T2.address_id INTERSECT SELECT T1.line_1 FROM Addresses AS T1 JOIN Teachers AS T2 ON T1.address_id = T2.address_id	behavior_monitoring
SELECT T1.asset_id , T1.asset_details FROM Assets AS T1 JOIN Asset_Parts AS T2 ON T1.asset_id = T2.asset_id GROUP BY T1.asset_id HAVING count(*) = 2 INTERSECT SELECT T1.asset_id , T1.asset_details FROM Assets AS T1 JOIN Fault_Log AS T2 ON T1.asset_id = T2.asset_id GROUP BY T1.asset_id HAVING count(*) < 2	assets_maintenance
SELECT count(*) , T1.maintenance_contract_id FROM Maintenance_Contracts AS T1 JOIN Assets AS T2 ON T1.maintenance_contract_id = T2.maintenance_contract_id GROUP BY T1.maintenance_contract_id	assets_maintenance
SELECT count(*) , T1.company_id FROM Third_Party_Companies AS T1 JOIN Assets AS T2 ON T1.company_id = T2.supplier_company_id GROUP BY T1.company_id	assets_maintenance
SELECT T1.company_id , T1.company_name FROM Third_Party_Companies AS T1 JOIN Maintenance_Engineers AS T2 ON T1.company_id = T2.company_id GROUP BY T1.company_id HAVING count(*) >= 2 UNION SELECT T3.company_id , T3.company_name FROM Third_Party_Companies AS T3 JOIN Maintenance_Contracts AS T4 ON T3.company_id = T4.maintenance_contract_company_id GROUP BY T3.company_id HAVING count(*) >= 2	assets_maintenance
SELECT T1.staff_name , T1.staff_id FROM Staff AS T1 JOIN Fault_Log AS T2 ON T1.staff_id = T2.recorded_by_staff_id EXCEPT SELECT T3.staff_name , T3.staff_id FROM Staff AS T3 JOIN Engineer_Visits AS T4 ON T3.staff_id = T4.contact_staff_id	assets_maintenance
SELECT T1.engineer_id , T1.first_name , T1.last_name FROM Maintenance_Engineers AS T1 JOIN Engineer_Visits AS T2 GROUP BY T1.engineer_id ORDER BY count(*) DESC LIMIT 1	assets_maintenance
SELECT T1.part_name , T1.part_id FROM Parts AS T1 JOIN Part_Faults AS T2 ON T1.part_id = T2.part_id GROUP BY T1.part_id HAVING count(*) > 2	assets_maintenance
SELECT T1.first_name , T1.last_name , T1.other_details , T3.skill_description FROM Maintenance_Engineers AS T1 JOIN Engineer_Skills AS T2 ON T1.engineer_id = T2.engineer_id JOIN Skills AS T3 ON T2.skill_id = T3.skill_id	assets_maintenance
SELECT T1.fault_short_name , T3.skill_description FROM Part_Faults AS T1 JOIN Skills_Required_To_Fix AS T2 ON T1.part_fault_id = T2.part_fault_id JOIN Skills AS T3 ON T2.skill_id = T3.skill_id	assets_maintenance
SELECT T1.part_name , count(*) FROM Parts AS T1 JOIN Asset_Parts AS T2 ON T1.part_id = T2.part_id GROUP BY T1.part_name	assets_maintenance
SELECT T1.fault_description , T2.fault_status FROM Fault_Log AS T1 JOIN Fault_Log_Parts AS T2 ON T1.fault_log_entry_id = T2.fault_log_entry_id	assets_maintenance
SELECT count(*) , T1.fault_log_entry_id FROM Fault_Log AS T1 JOIN Engineer_Visits AS T2 ON T1.fault_log_entry_id = T2.fault_log_entry_id GROUP BY T1.fault_log_entry_id ORDER BY count(*) DESC LIMIT 1	assets_maintenance
SELECT DISTINCT last_name FROM Maintenance_Engineers	assets_maintenance
SELECT DISTINCT fault_status FROM Fault_Log_Parts	assets_maintenance
SELECT first_name , last_name FROM Maintenance_Engineers WHERE engineer_id NOT IN (SELECT engineer_id FROM Engineer_Visits)	assets_maintenance
SELECT asset_id , asset_details , asset_make , asset_model FROM Assets	assets_maintenance
SELECT asset_acquired_date FROM Assets ORDER BY asset_acquired_date ASC LIMIT 1	assets_maintenance
SELECT T1.part_id , T1.part_name FROM Parts AS T1 JOIN Part_Faults AS T2 ON T1.part_id = T2.part_id JOIN Skills_Required_To_Fix AS T3 ON T2.part_fault_id = T3.part_fault_id GROUP BY T1.part_id ORDER BY count(*) DESC LIMIT 1	assets_maintenance
SELECT T1.part_name FROM Parts AS T1 JOIN Part_Faults AS T2 ON T1.part_id = T2.part_id GROUP BY T1.part_name ORDER BY count(*) ASC LIMIT 1	assets_maintenance
SELECT T1.engineer_id , T1.first_name , T1.last_name FROM Maintenance_Engineers AS T1 JOIN Engineer_Visits AS T2 ON T1.engineer_id = T2.engineer_id GROUP BY T1.engineer_id ORDER BY count(*) ASC LIMIT 1	assets_maintenance
SELECT T1.staff_name , T3.first_name , T3.last_name FROM Staff AS T1 JOIN Engineer_Visits AS T2 ON T1.staff_id = T2.contact_staff_id JOIN Maintenance_Engineers AS T3 ON T2.engineer_id = T3.engineer_id	assets_maintenance
SELECT T1.fault_log_entry_id , T1.fault_description , T1.fault_log_entry_datetime FROM Fault_Log AS T1 JOIN Fault_Log_Parts AS T2 ON T1.fault_log_entry_id = T2.fault_log_entry_id GROUP BY T1.fault_log_entry_id ORDER BY count(*) DESC LIMIT 1	assets_maintenance
SELECT T1.skill_id , T1.skill_description FROM Skills AS T1 JOIN Skills_Required_To_Fix AS T2 ON T1.skill_id = T2.skill_id GROUP BY T1.skill_id ORDER BY count(*) DESC LIMIT 1	assets_maintenance
SELECT DISTINCT asset_model FROM Assets	assets_maintenance
SELECT asset_make , asset_model , asset_details FROM Assets ORDER BY asset_disposed_date ASC	assets_maintenance
SELECT part_id , chargeable_amount FROM Parts ORDER BY chargeable_amount ASC LIMIT 1	assets_maintenance
SELECT T1.company_name FROM Third_Party_Companies AS T1 JOIN Maintenance_Contracts AS T2 ON T1.company_id = T2.maintenance_contract_company_id ORDER BY T2.contract_start_date ASC LIMIT 1	assets_maintenance
SELECT gender FROM staff GROUP BY gender ORDER BY count(*) DESC LIMIT 1	assets_maintenance
SELECT T1.staff_name , count(*) FROM Staff AS T1 JOIN Engineer_Visits AS T2 ON T1.staff_id = T2.contact_staff_id GROUP BY T1.staff_name	assets_maintenance
SELECT asset_model FROM Assets WHERE asset_id NOT IN (SELECT asset_id FROM Fault_Log)	assets_maintenance
SELECT local_authority , services FROM station	station_weather
SELECT train_number , name FROM train ORDER BY TIME	station_weather
SELECT TIME , train_number FROM train WHERE destination = 'Chennai' ORDER BY TIME	station_weather
SELECT count(*) FROM train WHERE name LIKE "%Express%"	station_weather
SELECT train_number , TIME FROM train WHERE origin = 'Chennai' AND destination = 'Guruvayur'	station_weather
SELECT origin , count(*) FROM train GROUP BY origin	station_weather
SELECT t1.name FROM train AS t1 JOIN route AS t2 ON t1.id = t2.train_id GROUP BY t2.train_id ORDER BY count(*) DESC LIMIT 1	station_weather
SELECT count(*) , t1.network_name , t1.services FROM station AS t1 JOIN route AS t2 ON t1.id = t2.station_id GROUP BY t2.station_id	station_weather
SELECT avg(high_temperature) , day_of_week FROM weekly_weather GROUP BY day_of_week	station_weather
SELECT max(t1.low_temperature) , avg(t1.precipitation) FROM weekly_weather AS t1 JOIN station AS t2 ON t1.station_id = t2.id WHERE t2.network_name = "Amersham"	station_weather
SELECT t3.name , t3.time FROM station AS t1 JOIN route AS t2 ON t1.id = t2.station_id JOIN train AS t3 ON t2.train_id = t3.id WHERE t1.local_authority = "Chiltern"	station_weather
SELECT count(DISTINCT services) FROM station	station_weather
SELECT t2.id , t2.local_authority FROM weekly_weather AS t1 JOIN station AS t2 ON t1.station_id = t2.id GROUP BY t1.station_id ORDER BY avg(high_temperature) DESC LIMIT 1	station_weather
SELECT t2.id , t2.local_authority FROM weekly_weather AS t1 JOIN station AS t2 ON t1.station_id = t2.id GROUP BY t1.station_id HAVING max(t1.precipitation) > 50	station_weather
SELECT min(low_temperature) , max(wind_speed_mph) FROM weekly_weather	station_weather
SELECT origin FROM train GROUP BY origin HAVING count(*) > 1	station_weather
SELECT count(*) FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE DEPT_NAME = "Accounting"	college_1
SELECT count(*) FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE DEPT_NAME = "Accounting"	college_1
SELECT count(DISTINCT PROF_NUM) FROM CLASS WHERE CRS_CODE = "ACCT-211"	college_1
SELECT count(DISTINCT PROF_NUM) FROM CLASS WHERE CRS_CODE = "ACCT-211"	college_1
SELECT T3.EMP_FNAME , T3.EMP_LNAME FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code JOIN employee AS T3 ON T1.EMP_NUM = T3.EMP_NUM WHERE DEPT_NAME = "Biology"	college_1
SELECT T3.EMP_FNAME , T3.EMP_LNAME FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code JOIN employee AS T3 ON T1.EMP_NUM = T3.EMP_NUM WHERE DEPT_NAME = "Biology"	college_1
SELECT DISTINCT T1.EMP_FNAME , T1.EMP_DOB FROM employee AS T1 JOIN CLASS AS T2 ON T1.EMP_NUM = T2.PROF_NUM WHERE CRS_CODE = "ACCT-211"	college_1
SELECT DISTINCT T1.EMP_FNAME , T1.EMP_DOB FROM employee AS T1 JOIN CLASS AS T2 ON T1.EMP_NUM = T2.PROF_NUM WHERE CRS_CODE = "ACCT-211"	college_1
SELECT count(*) FROM employee AS T1 JOIN CLASS AS T2 ON T1.EMP_NUM = T2.PROF_NUM WHERE T1.EMP_LNAME = 'Graztevski'	college_1
SELECT count(*) FROM employee AS T1 JOIN CLASS AS T2 ON T1.EMP_NUM = T2.PROF_NUM WHERE T1.EMP_LNAME = 'Graztevski'	college_1
SELECT school_code FROM department WHERE dept_name = "Accounting"	college_1
SELECT school_code FROM department WHERE dept_name = "Accounting"	college_1
SELECT crs_credit , crs_description FROM course WHERE crs_code = 'CIS-220'	college_1
SELECT crs_credit , crs_description FROM course WHERE crs_code = 'CIS-220'	college_1
SELECT dept_address FROM department WHERE dept_name = 'History'	college_1
SELECT dept_address FROM department WHERE dept_name = 'History'	college_1
SELECT count(DISTINCT dept_address) FROM department WHERE school_code = 'BUS'	college_1
SELECT count(DISTINCT dept_address) FROM department WHERE school_code = 'BUS'	college_1
SELECT count(DISTINCT dept_address) , school_code FROM department GROUP BY school_code	college_1
SELECT count(DISTINCT dept_address) , school_code FROM department GROUP BY school_code	college_1
SELECT crs_credit , crs_description FROM course WHERE crs_code = 'QM-261'	college_1
SELECT crs_credit , crs_description FROM course WHERE crs_code = 'QM-261'	college_1
SELECT count(DISTINCT dept_name) , school_code FROM department GROUP BY school_code	college_1
SELECT count(DISTINCT dept_name) , school_code FROM department GROUP BY school_code	college_1
SELECT count(DISTINCT dept_name) , school_code FROM department GROUP BY school_code HAVING count(DISTINCT dept_name) < 5	college_1
SELECT count(DISTINCT dept_name) , school_code FROM department GROUP BY school_code HAVING count(DISTINCT dept_name) < 5	college_1
SELECT count(*) , crs_code FROM CLASS GROUP BY crs_code	college_1
SELECT count(*) , crs_code FROM CLASS GROUP BY crs_code	college_1
SELECT sum(crs_credit) , dept_code FROM course GROUP BY dept_code	college_1
SELECT sum(crs_credit) , dept_code FROM course GROUP BY dept_code	college_1
SELECT count(*) , class_room FROM CLASS GROUP BY class_room HAVING count(*) >= 2	college_1
SELECT count(*) , class_room FROM CLASS GROUP BY class_room HAVING count(*) >= 2	college_1
SELECT count(*) , dept_code FROM CLASS AS T1 JOIN course AS T2 ON T1.crs_code = T2.crs_code GROUP BY dept_code	college_1
SELECT count(*) , dept_code FROM CLASS AS T1 JOIN course AS T2 ON T1.crs_code = T2.crs_code GROUP BY dept_code	college_1
SELECT count(*) , T3.school_code FROM CLASS AS T1 JOIN course AS T2 ON T1.crs_code = T2.crs_code JOIN department AS T3 ON T2.dept_code = T3.dept_code GROUP BY T3.school_code	college_1
SELECT count(*) , T3.school_code FROM CLASS AS T1 JOIN course AS T2 ON T1.crs_code = T2.crs_code JOIN department AS T3 ON T2.dept_code = T3.dept_code GROUP BY T3.school_code	college_1
SELECT count(*) , T1.school_code FROM department AS T1 JOIN professor AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.school_code	college_1
SELECT count(*) , T1.school_code FROM department AS T1 JOIN professor AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.school_code	college_1
SELECT emp_jobcode , count(*) FROM employee GROUP BY emp_jobcode ORDER BY count(*) DESC LIMIT 1	college_1
SELECT emp_jobcode , count(*) FROM employee GROUP BY emp_jobcode ORDER BY count(*) DESC LIMIT 1	college_1
SELECT T1.school_code FROM department AS T1 JOIN professor AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.school_code ORDER BY count(*) LIMIT 1	college_1
SELECT T1.school_code FROM department AS T1 JOIN professor AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.school_code ORDER BY count(*) LIMIT 1	college_1
SELECT count(*) , dept_code FROM professor WHERE prof_high_degree = 'Ph.D.' GROUP BY dept_code	college_1
SELECT count(*) , dept_code FROM professor WHERE prof_high_degree = 'Ph.D.' GROUP BY dept_code	college_1
SELECT count(*) , dept_code FROM student GROUP BY dept_code	college_1
SELECT count(*) , dept_code FROM student GROUP BY dept_code	college_1
SELECT sum(stu_hrs) , dept_code FROM student GROUP BY dept_code	college_1
SELECT sum(stu_hrs) , dept_code FROM student GROUP BY dept_code	college_1
SELECT max(stu_gpa) , avg(stu_gpa) , min(stu_gpa) , dept_code FROM student GROUP BY dept_code	college_1
SELECT max(stu_gpa) , avg(stu_gpa) , min(stu_gpa) , dept_code FROM student GROUP BY dept_code	college_1
SELECT T2.dept_name , avg(T1.stu_gpa) FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY avg(T1.stu_gpa) DESC LIMIT 1	college_1
SELECT T2.dept_name , avg(T1.stu_gpa) FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY avg(T1.stu_gpa) DESC LIMIT 1	college_1
SELECT count(DISTINCT school_code) FROM department	college_1
SELECT count(DISTINCT school_code) FROM department	college_1
SELECT count(DISTINCT class_code) FROM CLASS	college_1
SELECT count(DISTINCT class_code) FROM CLASS	college_1
SELECT count(DISTINCT crs_code) FROM CLASS	college_1
SELECT count(DISTINCT crs_code) FROM CLASS	college_1
SELECT count(DISTINCT dept_name) FROM department	college_1
SELECT count(DISTINCT dept_name) FROM department	college_1
SELECT count(*) FROM department AS T1 JOIN course AS T2 ON T1.dept_code = T2.dept_code WHERE dept_name = "Computer Info. Systems"	college_1
SELECT count(*) FROM department AS T1 JOIN course AS T2 ON T1.dept_code = T2.dept_code WHERE dept_name = "Computer Info. Systems"	college_1
SELECT count(DISTINCT class_section) FROM CLASS WHERE crs_code = 'ACCT-211'	college_1
SELECT count(DISTINCT class_section) FROM CLASS WHERE crs_code = 'ACCT-211'	college_1
SELECT sum(T1.crs_credit) , T1.dept_code FROM course AS T1 JOIN CLASS AS T2 ON T1.crs_code = T2.crs_code GROUP BY T1.dept_code	college_1
SELECT sum(T1.crs_credit) , T1.dept_code FROM course AS T1 JOIN CLASS AS T2 ON T1.crs_code = T2.crs_code GROUP BY T1.dept_code	college_1
SELECT T3.dept_name FROM course AS T1 JOIN CLASS AS T2 ON T1.crs_code = T2.crs_code JOIN department AS T3 ON T1.dept_code = T3.dept_code GROUP BY T1.dept_code ORDER BY sum(T1.crs_credit) DESC LIMIT 1	college_1
SELECT T3.dept_name FROM course AS T1 JOIN CLASS AS T2 ON T1.crs_code = T2.crs_code JOIN department AS T3 ON T1.dept_code = T3.dept_code GROUP BY T1.dept_code ORDER BY sum(T1.crs_credit) DESC LIMIT 1	college_1
SELECT count(*) FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code WHERE T1.crs_code = 'ACCT-211'	college_1
SELECT count(*) FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code WHERE T1.crs_code = 'ACCT-211'	college_1
SELECT T3.stu_fname FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T2.stu_num = T3.stu_num WHERE T1.crs_code = 'ACCT-211'	college_1
SELECT T3.stu_fname FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T2.stu_num = T3.stu_num WHERE T1.crs_code = 'ACCT-211'	college_1
SELECT T3.stu_fname FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T2.stu_num = T3.stu_num WHERE T1.crs_code = 'ACCT-211' AND T2.enroll_grade = 'C'	college_1
SELECT T3.stu_fname FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T2.stu_num = T3.stu_num WHERE T1.crs_code = 'ACCT-211' AND T2.enroll_grade = 'C'	college_1
SELECT count(*) FROM employee	college_1
SELECT count(*) FROM employee	college_1
SELECT count(*) FROM professor WHERE prof_high_degree = 'Ph.D.'	college_1
SELECT count(*) FROM professor WHERE prof_high_degree = 'Ph.D.'	college_1
SELECT count(*) FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN department AS T4 ON T3.dept_code = T4.dept_code WHERE T4.dept_name = 'Accounting'	college_1
SELECT count(*) FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN department AS T4 ON T3.dept_code = T4.dept_code WHERE T4.dept_name = 'Accounting'	college_1
SELECT T4.dept_name FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN department AS T4 ON T3.dept_code = T4.dept_code GROUP BY T3.dept_code ORDER BY count(*) DESC LIMIT 1	college_1
SELECT T4.dept_name FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN department AS T4 ON T3.dept_code = T4.dept_code GROUP BY T3.dept_code ORDER BY count(*) DESC LIMIT 1	college_1
SELECT dept_name FROM department ORDER BY dept_name	college_1
SELECT dept_name FROM department ORDER BY dept_name	college_1
SELECT class_code FROM CLASS WHERE class_room = 'KLR209'	college_1
SELECT class_code FROM CLASS WHERE class_room = 'KLR209'	college_1
SELECT emp_fname FROM employee WHERE emp_jobcode = 'PROF' ORDER BY emp_dob	college_1
SELECT emp_fname FROM employee WHERE emp_jobcode = 'PROF' ORDER BY emp_dob	college_1
SELECT T2.emp_fname , T1.prof_office FROM professor AS T1 JOIN employee AS T2 ON T1.emp_num = T2.emp_num ORDER BY T2.emp_fname	college_1
SELECT T2.emp_fname , T1.prof_office FROM professor AS T1 JOIN employee AS T2 ON T1.emp_num = T2.emp_num ORDER BY T2.emp_fname	college_1
SELECT emp_fname , emp_lname FROM employee ORDER BY emp_dob LIMIT 1	college_1
SELECT emp_fname , emp_lname FROM employee ORDER BY emp_dob LIMIT 1	college_1
SELECT stu_fname , stu_lname , stu_gpa FROM student WHERE stu_gpa > 3 ORDER BY stu_dob DESC LIMIT 1	college_1
SELECT stu_fname , stu_lname , stu_gpa FROM student WHERE stu_gpa > 3 ORDER BY stu_dob DESC LIMIT 1	college_1
SELECT DISTINCT stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num WHERE enroll_grade = 'C'	college_1
SELECT DISTINCT stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num WHERE enroll_grade = 'C'	college_1
SELECT T2.dept_name FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY count(*) LIMIT 1	college_1
SELECT T2.dept_name FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY count(*) LIMIT 1	college_1
SELECT T2.dept_name , T1.dept_code FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE T1.prof_high_degree = 'Ph.D.' GROUP BY T1.dept_code ORDER BY count(*) DESC LIMIT 1	college_1
SELECT T2.dept_name , T1.dept_code FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE T1.prof_high_degree = 'Ph.D.' GROUP BY T1.dept_code ORDER BY count(*) DESC LIMIT 1	college_1
SELECT emp_fname FROM employee WHERE emp_jobcode = 'PROF' EXCEPT SELECT T1.emp_fname FROM employee AS T1 JOIN CLASS AS T2 ON T1.emp_num = T2.prof_num	college_1
SELECT emp_fname FROM employee WHERE emp_jobcode = 'PROF' EXCEPT SELECT T1.emp_fname FROM employee AS T1 JOIN CLASS AS T2 ON T1.emp_num = T2.prof_num	college_1
SELECT T1.emp_fname FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T2.dept_code = T3.dept_code WHERE T3.dept_name = 'History' EXCEPT SELECT T4.emp_fname FROM employee AS T4 JOIN CLASS AS T5 ON T4.emp_num = T5.prof_num	college_1
SELECT T1.emp_fname FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T2.dept_code = T3.dept_code WHERE T3.dept_name = 'History' EXCEPT SELECT T4.emp_fname FROM employee AS T4 JOIN CLASS AS T5 ON T4.emp_num = T5.prof_num	college_1
SELECT T1.emp_lname , T2.prof_office FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T2.dept_code = T3.dept_code WHERE T3.dept_name = 'History'	college_1
SELECT T1.emp_lname , T2.prof_office FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T2.dept_code = T3.dept_code WHERE T3.dept_name = 'History'	college_1
SELECT T3.dept_name , T2.prof_office FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T2.dept_code = T3.dept_code WHERE T1.emp_lname = 'Heffington'	college_1
SELECT T3.dept_name , T2.prof_office FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T2.dept_code = T3.dept_code WHERE T1.emp_lname = 'Heffington'	college_1
SELECT T1.emp_lname , T1.emp_hiredate FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num WHERE T2.prof_office = 'DRE 102'	college_1
SELECT T1.emp_lname , T1.emp_hiredate FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num WHERE T2.prof_office = 'DRE 102'	college_1
SELECT T1.crs_code FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T3.stu_num = T2.stu_num WHERE T3.stu_lname = 'Smithson'	college_1
SELECT T1.crs_code FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T3.stu_num = T2.stu_num WHERE T3.stu_lname = 'Smithson'	college_1
SELECT T4.crs_description , T4.crs_credit FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T3.stu_num = T2.stu_num JOIN course AS T4 ON T4.crs_code = T1.crs_code WHERE T3.stu_lname = 'Smithson'	college_1
SELECT T4.crs_description , T4.crs_credit FROM CLASS AS T1 JOIN enroll AS T2 ON T1.class_code = T2.class_code JOIN student AS T3 ON T3.stu_num = T2.stu_num JOIN course AS T4 ON T4.crs_code = T1.crs_code WHERE T3.stu_lname = 'Smithson'	college_1
SELECT count(*) FROM professor WHERE prof_high_degree = 'Ph.D.' OR prof_high_degree = 'MA'	college_1
SELECT count(*) FROM professor WHERE prof_high_degree = 'Ph.D.' OR prof_high_degree = 'MA'	college_1
SELECT count(*) FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE T2.dept_name = 'Accounting' OR T2.dept_name = 'Biology'	college_1
SELECT count(*) FROM professor AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE T2.dept_name = 'Accounting' OR T2.dept_name = 'Biology'	college_1
SELECT T1.emp_fname FROM employee AS T1 JOIN CLASS AS T2 ON T1.emp_num = T2.prof_num WHERE crs_code = 'CIS-220' INTERSECT SELECT T1.emp_fname FROM employee AS T1 JOIN CLASS AS T2 ON T1.emp_num = T2.prof_num WHERE crs_code = 'QM-261'	college_1
SELECT T1.emp_fname FROM employee AS T1 JOIN CLASS AS T2 ON T1.emp_num = T2.prof_num WHERE crs_code = 'CIS-220' INTERSECT SELECT T1.emp_fname FROM employee AS T1 JOIN CLASS AS T2 ON T1.emp_num = T2.prof_num WHERE crs_code = 'QM-261'	college_1
SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code JOIN course AS T4 ON T3.crs_code = T4.crs_code JOIN department AS T5 ON T5.dept_code = T4.dept_code WHERE T5.dept_name = 'Accounting' INTERSECT SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code JOIN course AS T4 ON T3.crs_code = T4.crs_code JOIN department AS T5 ON T5.dept_code = T4.dept_code WHERE T5.dept_name = 'Computer Info. Systems'	college_1
SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code JOIN course AS T4 ON T3.crs_code = T4.crs_code JOIN department AS T5 ON T5.dept_code = T4.dept_code WHERE T5.dept_name = 'Accounting' INTERSECT SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code JOIN course AS T4 ON T3.crs_code = T4.crs_code JOIN department AS T5 ON T5.dept_code = T4.dept_code WHERE T5.dept_name = 'Computer Info. Systems'	college_1
SELECT avg(T2.stu_gpa) FROM enroll AS T1 JOIN student AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T1.class_code = T3.class_code WHERE T3.crs_code = 'ACCT-211'	college_1
SELECT avg(T2.stu_gpa) FROM enroll AS T1 JOIN student AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T1.class_code = T3.class_code WHERE T3.crs_code = 'ACCT-211'	college_1
SELECT stu_gpa , stu_phone , stu_fname FROM student ORDER BY stu_gpa DESC LIMIT 5	college_1
SELECT stu_gpa , stu_phone , stu_fname FROM student ORDER BY stu_gpa DESC LIMIT 5	college_1
SELECT T2.dept_name FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code ORDER BY stu_gpa LIMIT 1	college_1
SELECT T2.dept_name FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code ORDER BY stu_gpa LIMIT 1	college_1
SELECT stu_fname , stu_gpa FROM student WHERE stu_gpa < (SELECT avg(stu_gpa) FROM student)	college_1
SELECT stu_fname , stu_gpa FROM student WHERE stu_gpa < (SELECT avg(stu_gpa) FROM student)	college_1
SELECT T2.dept_name , T2.dept_address FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY count(*) DESC LIMIT 1	college_1
SELECT T2.dept_name , T2.dept_address FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY count(*) DESC LIMIT 1	college_1
SELECT T2.dept_name , T2.dept_address , count(*) FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY count(*) DESC LIMIT 3	college_1
SELECT T2.dept_name , T2.dept_address , count(*) FROM student AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code GROUP BY T1.dept_code ORDER BY count(*) DESC LIMIT 3	college_1
SELECT T1.emp_fname , T2.prof_office FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T3.dept_code = T2.dept_code WHERE T3.dept_name = 'History' AND T2.prof_high_degree = 'Ph.D.'	college_1
SELECT T1.emp_fname , T2.prof_office FROM employee AS T1 JOIN professor AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T3.dept_code = T2.dept_code WHERE T3.dept_name = 'History' AND T2.prof_high_degree = 'Ph.D.'	college_1
SELECT T2.emp_fname , T1.crs_code FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num	college_1
SELECT T2.emp_fname , T1.crs_code FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num	college_1
SELECT T2.emp_fname , T3.crs_description FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN course AS T3 ON T1.crs_code = T3.crs_code	college_1
SELECT T2.emp_fname , T3.crs_description FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN course AS T3 ON T1.crs_code = T3.crs_code	college_1
SELECT T2.emp_fname , T4.prof_office , T3.crs_description FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN professor AS T4 ON T2.emp_num = T4.emp_num	college_1
SELECT T2.emp_fname , T4.prof_office , T3.crs_description FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN professor AS T4 ON T2.emp_num = T4.emp_num	college_1
SELECT T2.emp_fname , T4.prof_office , T3.crs_description , T5.dept_name FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN professor AS T4 ON T2.emp_num = T4.emp_num JOIN department AS T5 ON T4.dept_code = T5.dept_code	college_1
SELECT T2.emp_fname , T4.prof_office , T3.crs_description , T5.dept_name FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN course AS T3 ON T1.crs_code = T3.crs_code JOIN professor AS T4 ON T2.emp_num = T4.emp_num JOIN department AS T5 ON T4.dept_code = T5.dept_code	college_1
SELECT T1.stu_fname , T1.stu_lname , T4.crs_description FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code JOIN course AS T4 ON T3.crs_code = T4.crs_code	college_1
SELECT T1.stu_fname , T1.stu_lname , T4.crs_description FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code JOIN course AS T4 ON T3.crs_code = T4.crs_code	college_1
SELECT T1.stu_fname , T1.stu_lname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num WHERE T2.enroll_grade = 'C' OR T2.enroll_grade = 'A'	college_1
SELECT T1.stu_fname , T1.stu_lname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num WHERE T2.enroll_grade = 'C' OR T2.enroll_grade = 'A'	college_1
SELECT T2.emp_fname , T1.class_room FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN professor AS T3 ON T2.emp_num = T3.emp_num JOIN department AS T4 ON T4.dept_code = T3.dept_code WHERE T4.dept_name = 'Accounting'	college_1
SELECT T2.emp_fname , T1.class_room FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN professor AS T3 ON T2.emp_num = T3.emp_num JOIN department AS T4 ON T4.dept_code = T3.dept_code WHERE T4.dept_name = 'Accounting'	college_1
SELECT DISTINCT T2.emp_fname , T3.prof_high_degree FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN professor AS T3 ON T2.emp_num = T3.emp_num JOIN department AS T4 ON T4.dept_code = T3.dept_code WHERE T4.dept_name = 'Computer Info. Systems'	college_1
SELECT DISTINCT T2.emp_fname , T3.prof_high_degree FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num JOIN professor AS T3 ON T2.emp_num = T3.emp_num JOIN department AS T4 ON T4.dept_code = T3.dept_code WHERE T4.dept_name = 'Computer Info. Systems'	college_1
SELECT T1.stu_lname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num WHERE T2.enroll_grade = 'A' AND T2.class_code = 10018	college_1
SELECT T1.stu_lname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num WHERE T2.enroll_grade = 'A' AND T2.class_code = 10018	college_1
SELECT T2.emp_fname , T1.prof_office FROM professor AS T1 JOIN employee AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T1.dept_code = T3.dept_code WHERE T3.dept_name = 'History' AND T1.prof_high_degree != 'Ph.D.'	college_1
SELECT T2.emp_fname , T1.prof_office FROM professor AS T1 JOIN employee AS T2 ON T1.emp_num = T2.emp_num JOIN department AS T3 ON T1.dept_code = T3.dept_code WHERE T3.dept_name = 'History' AND T1.prof_high_degree != 'Ph.D.'	college_1
SELECT T2.emp_fname FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num GROUP BY T1.prof_num HAVING count(*) > 1	college_1
SELECT T2.emp_fname FROM CLASS AS T1 JOIN employee AS T2 ON T1.prof_num = T2.emp_num GROUP BY T1.prof_num HAVING count(*) > 1	college_1
SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num GROUP BY T2.stu_num HAVING count(*) = 1	college_1
SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num GROUP BY T2.stu_num HAVING count(*) = 1	college_1
SELECT T2.dept_name FROM course AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE T1.crs_description LIKE '%Statistics%'	college_1
SELECT T2.dept_name FROM course AS T1 JOIN department AS T2 ON T1.dept_code = T2.dept_code WHERE T1.crs_description LIKE '%Statistics%'	college_1
SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code WHERE T3.crs_code = 'ACCT-211' AND T1.stu_lname LIKE 'S%'	college_1
SELECT T1.stu_fname FROM student AS T1 JOIN enroll AS T2 ON T1.stu_num = T2.stu_num JOIN CLASS AS T3 ON T2.class_code = T3.class_code WHERE T3.crs_code = 'ACCT-211' AND T1.stu_lname LIKE 'S%'	college_1
SELECT sum(num_of_component) FROM furniture	manufacturer
SELECT name , furniture_id FROM furniture ORDER BY market_rate DESC LIMIT 1	manufacturer
SELECT sum(market_rate) FROM furniture ORDER BY market_rate DESC LIMIT 2	manufacturer
SELECT Num_of_Component , name FROM furniture WHERE Num_of_Component > 10	manufacturer
SELECT name , Num_of_Component FROM furniture ORDER BY market_rate LIMIT 1	manufacturer
SELECT t1.name FROM furniture AS t1 JOIN furniture_manufacte AS t2 ON t1.Furniture_ID = t2.Furniture_ID WHERE t2.Price_in_Dollar < (SELECT max(Price_in_Dollar) FROM furniture_manufacte)	manufacturer
SELECT open_year , name FROM manufacturer ORDER BY num_of_shops DESC LIMIT 1	manufacturer
SELECT avg(Num_of_Factories) FROM manufacturer WHERE num_of_shops > 20	manufacturer
SELECT name , manufacturer_id FROM manufacturer ORDER BY open_year	manufacturer
SELECT name , open_year FROM manufacturer WHERE num_of_shops > 10 OR Num_of_Factories < 10	manufacturer
SELECT max(num_of_shops) , avg(Num_of_Factories) FROM manufacturer WHERE open_year < 1990	manufacturer
SELECT t1.manufacturer_id , t1.num_of_shops FROM manufacturer AS t1 JOIN furniture_manufacte AS t2 ON t1.manufacturer_id = t2.manufacturer_id ORDER BY t2.Price_in_Dollar DESC LIMIT 1	manufacturer
SELECT count(*) , t1.name FROM manufacturer AS t1 JOIN furniture_manufacte AS t2 ON t1.manufacturer_id = t2.manufacturer_id GROUP BY t1.manufacturer_id	manufacturer
SELECT t1.name , t2.price_in_dollar FROM furniture AS t1 JOIN furniture_manufacte AS t2 ON t1.Furniture_ID = t2.Furniture_ID	manufacturer
SELECT Market_Rate , name FROM furniture WHERE Furniture_ID NOT IN (SELECT Furniture_ID FROM furniture_manufacte)	manufacturer
SELECT t3.name FROM furniture AS t1 JOIN furniture_manufacte AS t2 ON t1.Furniture_ID = t2.Furniture_ID JOIN manufacturer AS t3 ON t2.manufacturer_id = t3.manufacturer_id WHERE t1.num_of_component < 6 INTERSECT SELECT t3.name FROM furniture AS t1 JOIN furniture_manufacte AS t2 ON t1.Furniture_ID = t2.Furniture_ID JOIN manufacturer AS t3 ON t2.manufacturer_id = t3.manufacturer_id WHERE t1.num_of_component > 10	manufacturer
SELECT T1.first_name , T2.department_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id	hr_1
SELECT T1.first_name , T2.department_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id	hr_1
SELECT first_name , last_name , salary FROM employees WHERE salary < 6000	hr_1
SELECT first_name , last_name , salary FROM employees WHERE salary < 6000	hr_1
SELECT first_name , department_id FROM employees WHERE last_name = 'McEwen'	hr_1
SELECT first_name , department_id FROM employees WHERE last_name = 'McEwen'	hr_1
SELECT * FROM employees WHERE department_id = "null"	hr_1
SELECT * FROM employees WHERE department_id = "null"	hr_1
SELECT * FROM departments WHERE department_name = 'Marketing'	hr_1
SELECT * FROM departments WHERE department_name = 'Marketing'	hr_1
SELECT hire_date FROM employees WHERE first_name NOT LIKE '%M%'	hr_1
SELECT hire_date FROM employees WHERE first_name NOT LIKE '%M%'	hr_1
SELECT first_name , last_name , hire_date , salary , department_id FROM employees WHERE first_name NOT LIKE '%M%'	hr_1
SELECT first_name , last_name , hire_date , salary , department_id FROM employees WHERE first_name NOT LIKE '%M%'	hr_1
SELECT first_name , last_name , hire_date , salary , department_id FROM employees WHERE first_name NOT LIKE '%M%' ORDER BY department_id	hr_1
SELECT first_name , last_name , hire_date , salary , department_id FROM employees WHERE first_name NOT LIKE '%M%' ORDER BY department_id	hr_1
SELECT phone_number FROM employees WHERE salary BETWEEN 8000 AND 12000	hr_1
SELECT phone_number FROM employees WHERE salary BETWEEN 8000 AND 12000	hr_1
SELECT * FROM employees WHERE salary BETWEEN 8000 AND 12000 AND commission_pct != "null" OR department_id != 40	hr_1
SELECT * FROM employees WHERE salary BETWEEN 8000 AND 12000 AND commission_pct != "null" OR department_id != 40	hr_1
SELECT first_name , last_name , salary FROM employees WHERE commission_pct = "null"	hr_1
SELECT first_name , last_name , salary FROM employees WHERE commission_pct = "null"	hr_1
SELECT first_name , last_name , salary FROM employees WHERE first_name LIKE '%m'	hr_1
SELECT first_name , last_name , salary FROM employees WHERE first_name LIKE '%m'	hr_1
SELECT job_id , hire_date FROM employees WHERE hire_date BETWEEN '2007-11-05' AND '2009-07-05'	hr_1
SELECT job_id , hire_date FROM employees WHERE hire_date BETWEEN '2007-11-05' AND '2009-07-05'	hr_1
SELECT first_name , last_name FROM employees WHERE department_id = 70 OR department_id = 90	hr_1
SELECT first_name , last_name FROM employees WHERE department_id = 70 OR department_id = 90	hr_1
SELECT salary , manager_id FROM employees WHERE manager_id != "null"	hr_1
SELECT salary , manager_id FROM employees WHERE manager_id != "null"	hr_1
SELECT * FROM employees WHERE hire_date < '2002-06-21'	hr_1
SELECT * FROM employees WHERE hire_date < '2002-06-21'	hr_1
SELECT * FROM employees WHERE first_name LIKE '%D%' OR first_name LIKE '%S%' ORDER BY salary DESC	hr_1
SELECT * FROM employees WHERE first_name LIKE '%D%' OR first_name LIKE '%S%' ORDER BY salary DESC	hr_1
SELECT * FROM employees WHERE hire_date > '1987-09-07'	hr_1
SELECT * FROM employees WHERE hire_date > '1987-09-07'	hr_1
SELECT job_title FROM jobs WHERE min_salary > 9000	hr_1
SELECT job_title FROM jobs WHERE min_salary > 9000	hr_1
SELECT job_title , max_salary - min_salary FROM jobs WHERE max_salary BETWEEN 12000 AND 18000	hr_1
SELECT job_title , max_salary - min_salary FROM jobs WHERE max_salary BETWEEN 12000 AND 18000	hr_1
SELECT email FROM employees WHERE commission_pct = "null" AND salary BETWEEN 7000 AND 12000 AND department_id = 50	hr_1
SELECT email FROM employees WHERE commission_pct = "null" AND salary BETWEEN 7000 AND 12000 AND department_id = 50	hr_1
SELECT employee_id , MAX(end_date) FROM job_history GROUP BY employee_id	hr_1
SELECT employee_id , MAX(end_date) FROM job_history GROUP BY employee_id	hr_1
SELECT department_id FROM employees GROUP BY department_id HAVING COUNT(commission_pct) > 10	hr_1
SELECT department_id FROM employees GROUP BY department_id HAVING COUNT(commission_pct) > 10	hr_1
SELECT DISTINCT department_id FROM employees GROUP BY department_id , manager_id HAVING COUNT(employee_id) >= 4	hr_1
SELECT DISTINCT department_id FROM employees GROUP BY department_id , manager_id HAVING COUNT(employee_id) >= 4	hr_1
SELECT department_id , AVG(salary) FROM employees WHERE commission_pct != "null" GROUP BY department_id	hr_1
SELECT department_id , AVG(salary) FROM employees WHERE commission_pct != "null" GROUP BY department_id	hr_1
SELECT country_id , COUNT(*) FROM locations GROUP BY country_id	hr_1
SELECT country_id , COUNT(*) FROM locations GROUP BY country_id	hr_1
SELECT job_id FROM job_history WHERE end_date - start_date > 300 GROUP BY job_id HAVING COUNT(*) >= 2	hr_1
SELECT job_id FROM job_history WHERE end_date - start_date > 300 GROUP BY job_id HAVING COUNT(*) >= 2	hr_1
SELECT employee_id FROM job_history GROUP BY employee_id HAVING COUNT(*) >= 2	hr_1
SELECT employee_id FROM job_history GROUP BY employee_id HAVING COUNT(*) >= 2	hr_1
SELECT T1.employee_id , T4.country_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id JOIN countries AS T4 ON T3.country_id = T4.country_id	hr_1
SELECT T1.employee_id , T4.country_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id JOIN countries AS T4 ON T3.country_id = T4.country_id	hr_1
SELECT T2.department_name , COUNT(*) FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id GROUP BY T2.department_name	hr_1
SELECT T2.department_name , COUNT(*) FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id GROUP BY T2.department_name	hr_1
SELECT * FROM job_history AS T1 JOIN employees AS T2 ON T1.employee_id = T2.employee_id WHERE T2.salary >= 12000	hr_1
SELECT * FROM job_history AS T1 JOIN employees AS T2 ON T1.employee_id = T2.employee_id WHERE T2.salary >= 12000	hr_1
SELECT job_title , AVG(salary) FROM employees AS T1 JOIN jobs AS T2 ON T1.job_id = T2.job_id GROUP BY T2.job_title	hr_1
SELECT job_title , AVG(salary) FROM employees AS T1 JOIN jobs AS T2 ON T1.job_id = T2.job_id GROUP BY T2.job_title	hr_1
SELECT first_name , last_name FROM employees WHERE salary > (SELECT salary FROM employees WHERE employee_id = 163 )	hr_1
SELECT first_name , last_name FROM employees WHERE salary > (SELECT salary FROM employees WHERE employee_id = 163 )	hr_1
SELECT MIN(salary) , department_id FROM employees GROUP BY department_id	hr_1
SELECT MIN(salary) , department_id FROM employees GROUP BY department_id	hr_1
SELECT first_name , last_name , department_id FROM employees WHERE salary IN (SELECT MIN(salary) FROM employees GROUP BY department_id)	hr_1
SELECT first_name , last_name , department_id FROM employees WHERE salary IN (SELECT MIN(salary) FROM employees GROUP BY department_id)	hr_1
SELECT employee_id FROM employees WHERE salary > (SELECT AVG(salary) FROM employees)	hr_1
SELECT employee_id FROM employees WHERE salary > (SELECT AVG(salary) FROM employees)	hr_1
SELECT employee_id , salary FROM employees WHERE manager_id = (SELECT employee_id FROM employees WHERE first_name = 'Payam' )	hr_1
SELECT employee_id , salary FROM employees WHERE manager_id = (SELECT employee_id FROM employees WHERE first_name = 'Payam' )	hr_1
SELECT DISTINCT T2.department_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id	hr_1
SELECT DISTINCT T2.department_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id	hr_1
SELECT DISTINCT * FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id WHERE T1.employee_id = T2.manager_id	hr_1
SELECT DISTINCT * FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id WHERE T1.employee_id = T2.manager_id	hr_1
SELECT * FROM departments WHERE department_name = 'Marketing'	hr_1
SELECT * FROM departments WHERE department_name = 'Marketing'	hr_1
SELECT employee_id FROM job_history GROUP BY employee_id HAVING COUNT(*) >= 2	hr_1
SELECT employee_id FROM job_history GROUP BY employee_id HAVING COUNT(*) >= 2	hr_1
SELECT DISTINCT department_id FROM employees GROUP BY department_id , manager_id HAVING COUNT(employee_id) >= 4	hr_1
SELECT DISTINCT department_id FROM employees GROUP BY department_id , manager_id HAVING COUNT(employee_id) >= 4	hr_1
SELECT job_id FROM employees GROUP BY job_id HAVING AVG(salary) > 8000	hr_1
SELECT job_id FROM employees GROUP BY job_id HAVING AVG(salary) > 8000	hr_1
SELECT T1.employee_id , T2.job_title FROM employees AS T1 JOIN jobs AS T2 ON T1.job_id = T2.job_id WHERE T1.department_id = 80	hr_1
SELECT T1.employee_id , T2.job_title FROM employees AS T1 JOIN jobs AS T2 ON T1.job_id = T2.job_id WHERE T1.department_id = 80	hr_1
SELECT T1.first_name , T1.job_id FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id WHERE T2.department_name = 'Finance'	hr_1
SELECT T1.first_name , T1.job_id FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id WHERE T2.department_name = 'Finance'	hr_1
SELECT * FROM employees WHERE salary BETWEEN (SELECT MIN(salary) FROM employees) AND 2500	hr_1
SELECT * FROM employees WHERE salary BETWEEN (SELECT MIN(salary) FROM employees) AND 2500	hr_1
SELECT * FROM employees WHERE department_id NOT IN (SELECT department_id FROM departments WHERE manager_id BETWEEN 100 AND 200)	hr_1
SELECT * FROM employees WHERE department_id NOT IN (SELECT department_id FROM departments WHERE manager_id BETWEEN 100 AND 200)	hr_1
SELECT first_name , last_name , hire_date FROM employees WHERE department_id = (SELECT department_id FROM employees WHERE first_name = "Clara")	hr_1
SELECT first_name , last_name , hire_date FROM employees WHERE department_id = (SELECT department_id FROM employees WHERE first_name = "Clara")	hr_1
SELECT first_name , last_name , hire_date FROM employees WHERE department_id = ( SELECT department_id FROM employees WHERE first_name = "Clara") AND first_name != "Clara"	hr_1
SELECT first_name , last_name , hire_date FROM employees WHERE department_id = ( SELECT department_id FROM employees WHERE first_name = "Clara") AND first_name != "Clara"	hr_1
SELECT employee_id , first_name , last_name FROM employees WHERE department_id IN ( SELECT department_id FROM employees WHERE first_name LIKE '%T%' )	hr_1
SELECT employee_id , first_name , last_name FROM employees WHERE department_id IN ( SELECT department_id FROM employees WHERE first_name LIKE '%T%' )	hr_1
SELECT employee_id , first_name , last_name , salary FROM employees WHERE salary > ( SELECT AVG (salary) FROM employees ) AND department_id IN ( SELECT department_id FROM employees WHERE first_name LIKE '%J%')	hr_1
SELECT employee_id , first_name , last_name , salary FROM employees WHERE salary > ( SELECT AVG (salary) FROM employees ) AND department_id IN ( SELECT department_id FROM employees WHERE first_name LIKE '%J%')	hr_1
SELECT employee_id , job_id FROM employees WHERE salary < ( SELECT min(salary) FROM employees WHERE job_id = 'MK_MAN' )	hr_1
SELECT employee_id , job_id FROM employees WHERE salary < ( SELECT min(salary) FROM employees WHERE job_id = 'MK_MAN' )	hr_1
SELECT employee_id , first_name , last_name , job_id FROM employees WHERE salary > ( SELECT max(salary) FROM employees WHERE job_id = 'PU_MAN' )	hr_1
SELECT employee_id , first_name , last_name , job_id FROM employees WHERE salary > ( SELECT max(salary) FROM employees WHERE job_id = 'PU_MAN' )	hr_1
SELECT department_id , SUM(salary) FROM employees GROUP BY department_id HAVING count(*) >= 2	hr_1
SELECT department_id , SUM(salary) FROM employees GROUP BY department_id HAVING count(*) >= 2	hr_1
SELECT * FROM employees WHERE employee_id NOT IN (SELECT employee_id FROM job_history)	hr_1
SELECT * FROM employees WHERE employee_id NOT IN (SELECT employee_id FROM job_history)	hr_1
SELECT first_name , last_name , salary , department_id , MAX(salary) FROM employees GROUP BY department_id	hr_1
SELECT first_name , last_name , salary , department_id , MAX(salary) FROM employees GROUP BY department_id	hr_1
SELECT T1.first_name , T1.last_name , T2.department_name , T3.city , T3.state_province FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id	hr_1
SELECT T1.first_name , T1.last_name , T2.department_name , T3.city , T3.state_province FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id	hr_1
SELECT T1.first_name , T1.last_name , T3.city FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id WHERE T1.first_name LIKE '%z%'	hr_1
SELECT T1.first_name , T1.last_name , T3.city FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id WHERE T1.first_name LIKE '%z%'	hr_1
SELECT T1.department_name , T2.city , T2.state_province FROM departments AS T1 JOIN locations AS T2 ON T2.location_id = T1.location_id	hr_1
SELECT T1.department_name , T2.city , T2.state_province FROM departments AS T1 JOIN locations AS T2 ON T2.location_id = T1.location_id	hr_1
SELECT T1.first_name , T1.last_name , T1.employee_id , T4.country_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id JOIN countries AS T4 ON T3.country_id = T4.country_id	hr_1
SELECT T1.first_name , T1.last_name , T1.employee_id , T4.country_name FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id JOIN countries AS T4 ON T3.country_id = T4.country_id	hr_1
SELECT department_name , COUNT(*) FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id GROUP BY department_name	hr_1
SELECT department_name , COUNT(*) FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id GROUP BY department_name	hr_1
SELECT first_name , last_name , salary FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id WHERE T3.city = 'London'	hr_1
SELECT first_name , last_name , salary FROM employees AS T1 JOIN departments AS T2 ON T1.department_id = T2.department_id JOIN locations AS T3 ON T2.location_id = T3.location_id WHERE T3.city = 'London'	hr_1
SELECT song_name , releasedate FROM song ORDER BY releasedate DESC LIMIT 1	music_1
SELECT song_name , releasedate FROM song ORDER BY releasedate DESC LIMIT 1	music_1
SELECT f_id FROM files ORDER BY duration DESC LIMIT 1	music_1
SELECT f_id FROM files ORDER BY duration DESC LIMIT 1	music_1
SELECT song_name FROM song WHERE languages = "english"	music_1
SELECT song_name FROM song WHERE languages = "english"	music_1
SELECT f_id FROM files WHERE formats = "mp3"	music_1
SELECT f_id FROM files WHERE formats = "mp3"	music_1
SELECT DISTINCT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.rating > 9	music_1
SELECT DISTINCT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.rating > 9	music_1
SELECT DISTINCT T1.file_size , T1.formats FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T2.resolution < 800	music_1
SELECT DISTINCT T1.file_size , T1.formats FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T2.resolution < 800	music_1
SELECT T1.artist_name FROM song AS T1 JOIN files AS T2 ON T1.f_id = T2.f_id ORDER BY T2.duration LIMIT 1	music_1
SELECT T1.artist_name FROM song AS T1 JOIN files AS T2 ON T1.f_id = T2.f_id ORDER BY T2.duration LIMIT 1	music_1
SELECT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name ORDER BY T2.rating DESC LIMIT 3	music_1
SELECT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name ORDER BY T2.rating DESC LIMIT 3	music_1
SELECT count(*) FROM files WHERE duration LIKE "4:%"	music_1
SELECT count(*) FROM files WHERE duration LIKE "4:%"	music_1
SELECT count(*) FROM artist WHERE country = "Bangladesh"	music_1
SELECT count(*) FROM artist WHERE country = "Bangladesh"	music_1
SELECT avg(T2.rating) FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T1.gender = "Female"	music_1
SELECT avg(T2.rating) FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T1.gender = "Female"	music_1
SELECT formats FROM files GROUP BY formats ORDER BY COUNT (*) DESC LIMIT 1	music_1
SELECT formats FROM files GROUP BY formats ORDER BY COUNT (*) DESC LIMIT 1	music_1
SELECT artist_name FROM artist WHERE country = "UK" INTERSECT SELECT artist_name FROM song WHERE languages = "english"	music_1
SELECT artist_name FROM artist WHERE country = "UK" INTERSECT SELECT artist_name FROM song WHERE languages = "english"	music_1
SELECT f_id FROM files WHERE formats = "mp4" INTERSECT SELECT f_id FROM song WHERE resolution < 1000	music_1
SELECT f_id FROM files WHERE formats = "mp4" INTERSECT SELECT f_id FROM song WHERE resolution < 1000	music_1
SELECT T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T1.gender = "Female" AND T2.languages = "bangla"	music_1
SELECT T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T1.gender = "Female" AND T2.languages = "bangla"	music_1
SELECT avg(T1.duration) FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.formats = "mp3" AND T2.resolution < 800	music_1
SELECT avg(T1.duration) FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.formats = "mp3" AND T2.resolution < 800	music_1
SELECT count(*) , gender FROM artist GROUP BY gender	music_1
SELECT count(*) , gender FROM artist GROUP BY gender	music_1
SELECT avg(rating) , languages FROM song GROUP BY languages	music_1
SELECT avg(rating) , languages FROM song GROUP BY languages	music_1
SELECT T1.gender , T1.artist_name FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name ORDER BY T2.resolution LIMIT 1	music_1
SELECT T1.gender , T1.artist_name FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name ORDER BY T2.resolution LIMIT 1	music_1
SELECT count(*) , formats FROM files GROUP BY formats	music_1
SELECT count(*) , formats FROM files GROUP BY formats	music_1
SELECT DISTINCT song_name FROM song WHERE resolution > (SELECT min(resolution) FROM song WHERE languages = "english")	music_1
SELECT DISTINCT song_name FROM song WHERE resolution > (SELECT min(resolution) FROM song WHERE languages = "english")	music_1
SELECT song_name FROM song WHERE rating < (SELECT max(rating) FROM song WHERE genre_is = "blues")	music_1
SELECT song_name FROM song WHERE rating < (SELECT max(rating) FROM song WHERE genre_is = "blues")	music_1
SELECT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.song_name LIKE "%love%"	music_1
SELECT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.song_name LIKE "%love%"	music_1
SELECT T1.artist_name , T1.gender FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.releasedate LIKE "%Mar%"	music_1
SELECT T1.artist_name , T1.gender FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.releasedate LIKE "%Mar%"	music_1
SELECT g_name , rating FROM genre ORDER BY g_name	music_1
SELECT g_name , rating FROM genre ORDER BY g_name	music_1
SELECT song_name FROM song ORDER BY resolution	music_1
SELECT song_name FROM song ORDER BY resolution	music_1
SELECT f_id FROM files WHERE formats = "mp4" UNION SELECT f_id FROM song WHERE resolution > 720	music_1
SELECT f_id FROM files WHERE formats = "mp4" UNION SELECT f_id FROM song WHERE resolution > 720	music_1
SELECT T2.song_name FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.duration LIKE "4:%" UNION SELECT song_name FROM song WHERE languages = "english"	music_1
SELECT T2.song_name FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.duration LIKE "4:%" UNION SELECT song_name FROM song WHERE languages = "english"	music_1
SELECT languages FROM song GROUP BY languages ORDER BY count(*) DESC LIMIT 1	music_1
SELECT languages FROM song GROUP BY languages ORDER BY count(*) DESC LIMIT 1	music_1
SELECT artist_name FROM song WHERE resolution > 500 GROUP BY languages ORDER BY count(*) DESC LIMIT 1	music_1
SELECT artist_name FROM song WHERE resolution > 500 GROUP BY languages ORDER BY count(*) DESC LIMIT 1	music_1
SELECT artist_name FROM artist WHERE country = "UK" AND gender = "Male"	music_1
SELECT artist_name FROM artist WHERE country = "UK" AND gender = "Male"	music_1
SELECT song_name FROM song WHERE genre_is = "modern" OR languages = "english"	music_1
SELECT song_name FROM song WHERE genre_is = "modern" OR languages = "english"	music_1
SELECT T2.song_name FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.formats = "mp3" INTERSECT SELECT song_name FROM song WHERE resolution < 1000	music_1
SELECT T2.song_name FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.formats = "mp3" INTERSECT SELECT song_name FROM song WHERE resolution < 1000	music_1
SELECT artist_name FROM artist WHERE country = "UK" INTERSECT SELECT T1.artist_name FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.languages = "english"	music_1
SELECT artist_name FROM artist WHERE country = "UK" INTERSECT SELECT T1.artist_name FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.languages = "english"	music_1
SELECT avg(rating) , avg(resolution) FROM song WHERE languages = "bangla"	music_1
SELECT avg(rating) , avg(resolution) FROM song WHERE languages = "bangla"	music_1
SELECT max(T2.resolution) , min(T2.resolution) FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.duration LIKE "3:%"	music_1
SELECT max(T2.resolution) , min(T2.resolution) FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T1.duration LIKE "3:%"	music_1
SELECT max(T1.duration) , max(T2.resolution) , T2.languages FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id GROUP BY T2.languages ORDER BY T2.languages	music_1
SELECT max(T1.duration) , max(T2.resolution) , T2.languages FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id GROUP BY T2.languages ORDER BY T2.languages	music_1
SELECT min(T1.duration) , min(T2.rating) , T2.genre_is FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id GROUP BY T2.genre_is ORDER BY T2.genre_is	music_1
SELECT min(T1.duration) , min(T2.rating) , T2.genre_is FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id GROUP BY T2.genre_is ORDER BY T2.genre_is	music_1
SELECT T1.artist_name , count(*) FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.languages = "english" GROUP BY T2.artist_name HAVING count(*) >= 1	music_1
SELECT T1.artist_name , count(*) FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.languages = "english" GROUP BY T2.artist_name HAVING count(*) >= 1	music_1
SELECT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.resolution > 900 GROUP BY T2.artist_name HAVING count(*) >= 1	music_1
SELECT T1.artist_name , T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.resolution > 900 GROUP BY T2.artist_name HAVING count(*) >= 1	music_1
SELECT T1.artist_name , count(*) FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name GROUP BY T2.artist_name ORDER BY count(*) DESC LIMIT 3	music_1
SELECT T1.artist_name , count(*) FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name GROUP BY T2.artist_name ORDER BY count(*) DESC LIMIT 3	music_1
SELECT T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name GROUP BY T2.artist_name ORDER BY count(*) LIMIT 1	music_1
SELECT T1.country FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name GROUP BY T2.artist_name ORDER BY count(*) LIMIT 1	music_1
SELECT song_name FROM song WHERE rating < (SELECT min(rating) FROM song WHERE languages = 'english')	music_1
SELECT song_name FROM song WHERE rating < (SELECT min(rating) FROM song WHERE languages = 'english')	music_1
SELECT f_id FROM song WHERE resolution > (SELECT max(resolution) FROM song WHERE rating < 8)	music_1
SELECT f_id FROM song WHERE resolution > (SELECT max(resolution) FROM song WHERE rating < 8)	music_1
SELECT f_id FROM song WHERE resolution > (SELECT avg(resolution) FROM song WHERE genre_is = "modern")	music_1
SELECT f_id FROM song WHERE resolution > (SELECT avg(resolution) FROM song WHERE genre_is = "modern")	music_1
SELECT T1.artist_name FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.languages = "bangla" GROUP BY T2.artist_name ORDER BY count(*) DESC LIMIT 3	music_1
SELECT T1.artist_name FROM artist AS T1 JOIN song AS T2 ON T1.artist_name = T2.artist_name WHERE T2.languages = "bangla" GROUP BY T2.artist_name ORDER BY count(*) DESC LIMIT 3	music_1
SELECT f_id , genre_is , artist_name FROM song WHERE languages = "english" ORDER BY rating	music_1
SELECT f_id , genre_is , artist_name FROM song WHERE languages = "english" ORDER BY rating	music_1
SELECT T1.duration , T1.file_size , T1.formats FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T2.genre_is = "pop" ORDER BY T2.song_name	music_1
SELECT T1.duration , T1.file_size , T1.formats FROM files AS T1 JOIN song AS T2 ON T1.f_id = T2.f_id WHERE T2.genre_is = "pop" ORDER BY T2.song_name	music_1
SELECT DISTINCT artist_name FROM song WHERE languages = "english" EXCEPT SELECT DISTINCT artist_name FROM song WHERE rating > 8	music_1
SELECT DISTINCT artist_name FROM song WHERE languages = "english" EXCEPT SELECT DISTINCT artist_name FROM song WHERE rating > 8	music_1
SELECT DISTINCT artist_name FROM artist WHERE country = "Bangladesh" EXCEPT SELECT DISTINCT artist_name FROM song WHERE rating > 7	music_1
SELECT DISTINCT artist_name FROM artist WHERE country = "Bangladesh" EXCEPT SELECT DISTINCT artist_name FROM song WHERE rating > 7	music_1
SELECT name FROM channel WHERE OWNER != 'CCTV'	program_share
SELECT name FROM channel WHERE OWNER != 'CCTV'	program_share
SELECT name FROM channel ORDER BY rating_in_percent DESC	program_share
SELECT name FROM channel ORDER BY rating_in_percent DESC	program_share
SELECT OWNER FROM channel ORDER BY rating_in_percent DESC LIMIT 1	program_share
SELECT OWNER FROM channel ORDER BY rating_in_percent DESC LIMIT 1	program_share
SELECT count(*) FROM program	program_share
SELECT count(*) FROM program	program_share
SELECT name FROM program ORDER BY launch	program_share
SELECT name FROM program ORDER BY launch	program_share
SELECT name , origin , OWNER FROM program	program_share
SELECT name , origin , OWNER FROM program	program_share
SELECT name FROM program ORDER BY launch DESC LIMIT 1	program_share
SELECT name FROM program ORDER BY launch DESC LIMIT 1	program_share
SELECT sum(Share_in_percent) FROM channel WHERE OWNER = 'CCTV'	program_share
SELECT sum(Share_in_percent) FROM channel WHERE OWNER = 'CCTV'	program_share
SELECT t1.name FROM channel AS t1 JOIN broadcast AS t2 ON t1.channel_id = t2.channel_id WHERE t2.time_of_day = 'Morning'	program_share
SELECT t1.name FROM channel AS t1 JOIN broadcast AS t2 ON t1.channel_id = t2.channel_id WHERE t2.time_of_day = 'Morning'	program_share
SELECT t1.name FROM channel AS t1 JOIN broadcast AS t2 ON t1.channel_id = t2.channel_id WHERE t2.time_of_day = 'Morning' INTERSECT SELECT t1.name FROM channel AS t1 JOIN broadcast AS t2 ON t1.channel_id = t2.channel_id WHERE t2.time_of_day = 'Night'	program_share
SELECT t1.name FROM channel AS t1 JOIN broadcast AS t2 ON t1.channel_id = t2.channel_id WHERE t2.time_of_day = 'Morning' INTERSECT SELECT t1.name FROM channel AS t1 JOIN broadcast AS t2 ON t1.channel_id = t2.channel_id WHERE t2.time_of_day = 'Night'	program_share
SELECT count(*) , time_of_day FROM broadcast GROUP BY time_of_day	program_share
SELECT count(*) , time_of_day FROM broadcast GROUP BY time_of_day	program_share
SELECT count(DISTINCT program_id) FROM broadcast WHERE time_of_day = 'Night'	program_share
SELECT count(DISTINCT program_id) FROM broadcast WHERE time_of_day = 'Night'	program_share
SELECT name FROM program EXCEPT SELECT t1.name FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id WHERE t2.Time_of_day = "Morning"	program_share
SELECT name FROM program EXCEPT SELECT t1.name FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id WHERE t2.Time_of_day = "Morning"	program_share
SELECT t1.owner FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id WHERE t2.Time_of_day = "Morning" INTERSECT SELECT t1.owner FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id WHERE t2.Time_of_day = "Night"	program_share
SELECT t1.owner FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id WHERE t2.Time_of_day = "Morning" INTERSECT SELECT t1.owner FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id WHERE t2.Time_of_day = "Night"	program_share
SELECT origin FROM program ORDER BY origin	program_share
SELECT origin FROM program ORDER BY origin	program_share
SELECT count(DISTINCT OWNER) FROM channel	program_share
SELECT count(DISTINCT OWNER) FROM channel	program_share
SELECT name FROM program WHERE origin != 'Beijing'	program_share
SELECT name FROM program WHERE origin != 'Beijing'	program_share
SELECT name FROM channel WHERE OWNER = 'CCTV' OR OWNER = 'HBS'	program_share
SELECT name FROM channel WHERE OWNER = 'CCTV' OR OWNER = 'HBS'	program_share
SELECT sum(Rating_in_percent) , OWNER FROM channel GROUP BY OWNER	program_share
SELECT sum(Rating_in_percent) , OWNER FROM channel GROUP BY OWNER	program_share
SELECT t1.name FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id GROUP BY t2.program_id ORDER BY count(*) DESC LIMIT 1	program_share
SELECT t1.name FROM program AS t1 JOIN broadcast AS t2 ON t1.program_id = t2.program_id GROUP BY t2.program_id ORDER BY count(*) DESC LIMIT 1	program_share
SELECT count(*) FROM COURSES	e_learning
SELECT count(*) FROM COURSES	e_learning
SELECT course_description FROM COURSES WHERE course_name = "database"	e_learning
SELECT course_description FROM COURSES WHERE course_name = "database"	e_learning
SELECT address_line_1 FROM Course_Authors_and_Tutors WHERE personal_name = "Cathrine"	e_learning
SELECT address_line_1 FROM Course_Authors_and_Tutors WHERE personal_name = "Cathrine"	e_learning
SELECT address_line_1 FROM Course_Authors_and_Tutors	e_learning
SELECT address_line_1 FROM Course_Authors_and_Tutors	e_learning
SELECT login_name , family_name FROM Course_Authors_and_Tutors	e_learning
SELECT login_name , family_name FROM Course_Authors_and_Tutors	e_learning
SELECT date_of_enrolment , date_of_completion FROM Student_Course_Enrolment	e_learning
SELECT date_of_enrolment , date_of_completion FROM Student_Course_Enrolment	e_learning
SELECT count(DISTINCT student_id) FROM Student_Course_Enrolment	e_learning
SELECT count(DISTINCT student_id) FROM Student_Course_Enrolment	e_learning
SELECT count(course_id) FROM Student_Course_Enrolment	e_learning
SELECT count(course_id) FROM Student_Course_Enrolment	e_learning
SELECT date_test_taken FROM Student_Tests_Taken WHERE test_result = "Pass"	e_learning
SELECT date_test_taken FROM Student_Tests_Taken WHERE test_result = "Pass"	e_learning
SELECT count(*) FROM Student_Tests_Taken WHERE test_result = "Fail"	e_learning
SELECT count(*) FROM Student_Tests_Taken WHERE test_result = "Fail"	e_learning
SELECT login_name FROM Students WHERE family_name = "Ward"	e_learning
SELECT login_name FROM Students WHERE family_name = "Ward"	e_learning
SELECT date_of_latest_logon FROM Students WHERE family_name = "Jaskolski" OR family_name = "Langosh"	e_learning
SELECT date_of_latest_logon FROM Students WHERE family_name = "Jaskolski" OR family_name = "Langosh"	e_learning
SELECT COUNT(*) FROM Students WHERE personal_name LIKE "%son%"	e_learning
SELECT COUNT(*) FROM Students WHERE personal_name LIKE "%son%"	e_learning
SELECT subject_name FROM SUBJECTS	e_learning
SELECT subject_name FROM SUBJECTS	e_learning
SELECT * FROM Course_Authors_and_Tutors ORDER BY personal_name	e_learning
SELECT * FROM Course_Authors_and_Tutors ORDER BY personal_name	e_learning
SELECT personal_name , family_name FROM Students ORDER BY family_name	e_learning
SELECT personal_name , family_name FROM Students ORDER BY family_name	e_learning
SELECT test_result , COUNT(*) FROM Student_Tests_Taken GROUP BY test_result ORDER BY COUNT(*) DESC	e_learning
SELECT test_result , COUNT(*) FROM Student_Tests_Taken GROUP BY test_result ORDER BY COUNT(*) DESC	e_learning
SELECT T1.login_name FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id WHERE T2.course_name = "advanced database"	e_learning
SELECT T1.login_name FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id WHERE T2.course_name = "advanced database"	e_learning
SELECT T1.address_line_1 FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id WHERE T2.course_name = "operating system" OR T2.course_name = "data structure"	e_learning
SELECT T1.address_line_1 FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id WHERE T2.course_name = "operating system" OR T2.course_name = "data structure"	e_learning
SELECT T1.personal_name , T1.family_name , T2.author_id FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id GROUP BY T2.author_id ORDER BY COUNT(*) DESC LIMIT 1	e_learning
SELECT T1.personal_name , T1.family_name , T2.author_id FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id GROUP BY T2.author_id ORDER BY COUNT(*) DESC LIMIT 1	e_learning
SELECT T1.address_line_1 , T2.author_id FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id GROUP BY T2.author_id HAVING Count(*) >= 2	e_learning
SELECT T1.address_line_1 , T2.author_id FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id GROUP BY T2.author_id HAVING Count(*) >= 2	e_learning
SELECT T2.course_name FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id WHERE T1.personal_name = "Julio"	e_learning
SELECT T2.course_name FROM Course_Authors_and_Tutors AS T1 JOIN Courses AS T2 ON T1.author_id = T2.author_id WHERE T1.personal_name = "Julio"	e_learning
SELECT T1.course_name , T1.course_description FROM Courses AS T1 JOIN Subjects AS T2 ON T1.subject_id = T2.subject_id WHERE T2.subject_name = "Computer Science"	e_learning
SELECT T1.course_name , T1.course_description FROM Courses AS T1 JOIN Subjects AS T2 ON T1.subject_id = T2.subject_id WHERE T2.subject_name = "Computer Science"	e_learning
SELECT T1.subject_id , T2.subject_name , COUNT(*) FROM Courses AS T1 JOIN Subjects AS T2 ON T1.subject_id = T2.subject_id GROUP BY T1.subject_id	e_learning
SELECT T1.subject_id , T2.subject_name , COUNT(*) FROM Courses AS T1 JOIN Subjects AS T2 ON T1.subject_id = T2.subject_id GROUP BY T1.subject_id	e_learning
SELECT T1.subject_id , T2.subject_name , COUNT(*) FROM Courses AS T1 JOIN Subjects AS T2 ON T1.subject_id = T2.subject_id GROUP BY T1.subject_id ORDER BY COUNT(*) ASC	e_learning
SELECT T1.subject_id , T2.subject_name , COUNT(*) FROM Courses AS T1 JOIN Subjects AS T2 ON T1.subject_id = T2.subject_id GROUP BY T1.subject_id ORDER BY COUNT(*) ASC	e_learning
SELECT T2.date_of_enrolment FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "Spanish"	e_learning
SELECT T2.date_of_enrolment FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id WHERE T1.course_name = "Spanish"	e_learning
SELECT T1.course_name FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name ORDER BY COUNT(*) DESC LIMIT 1	e_learning
SELECT T1.course_name FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name ORDER BY COUNT(*) DESC LIMIT 1	e_learning
SELECT T1.course_name FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name HAVING COUNT(*) = 1	e_learning
SELECT T1.course_name FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name HAVING COUNT(*) = 1	e_learning
SELECT T1.course_description , T1.course_name FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name HAVING COUNT(*) > 2	e_learning
SELECT T1.course_description , T1.course_name FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name HAVING COUNT(*) > 2	e_learning
SELECT T1.course_name , COUNT(*) FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name	e_learning
SELECT T1.course_name , COUNT(*) FROM Courses AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name	e_learning
SELECT T1.date_of_enrolment FROM Student_Course_Enrolment AS T1 JOIN Student_Tests_Taken AS T2 ON T1.registration_id = T2.registration_id WHERE T2.test_result = "Pass"	e_learning
SELECT T1.date_of_enrolment FROM Student_Course_Enrolment AS T1 JOIN Student_Tests_Taken AS T2 ON T1.registration_id = T2.registration_id WHERE T2.test_result = "Pass"	e_learning
SELECT T1.date_of_completion FROM Student_Course_Enrolment AS T1 JOIN Student_Tests_Taken AS T2 ON T1.registration_id = T2.registration_id WHERE T2.test_result = "Fail"	e_learning
SELECT T1.date_of_completion FROM Student_Course_Enrolment AS T1 JOIN Student_Tests_Taken AS T2 ON T1.registration_id = T2.registration_id WHERE T2.test_result = "Fail"	e_learning
SELECT T1.date_of_enrolment , T1.date_of_completion FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id WHERE T2.personal_name = "Karson"	e_learning
SELECT T1.date_of_enrolment , T1.date_of_completion FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id WHERE T2.personal_name = "Karson"	e_learning
SELECT T1.date_of_enrolment , T1.date_of_completion FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id WHERE T2.family_name = "Zieme" AND T2.personal_name = "Bernie"	e_learning
SELECT T1.date_of_enrolment , T1.date_of_completion FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id WHERE T2.family_name = "Zieme" AND T2.personal_name = "Bernie"	e_learning
SELECT T1.student_id , T2.login_name FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY COUNT(*) DESC LIMIT 1	e_learning
SELECT T1.student_id , T2.login_name FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY COUNT(*) DESC LIMIT 1	e_learning
SELECT T1.student_id , T2.personal_name FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id HAVING COUNT(*) >= 2	e_learning
SELECT T1.student_id , T2.personal_name FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id HAVING COUNT(*) >= 2	e_learning
SELECT T1.student_id , T2.middle_name FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id HAVING COUNT(*) <= 2	e_learning
SELECT T1.student_id , T2.middle_name FROM Student_Course_Enrolment AS T1 JOIN Students AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id HAVING COUNT(*) <= 2	e_learning
SELECT personal_name FROM Students EXCEPT SELECT T1.personal_name FROM Students AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.student_id = T2.student_id	e_learning
SELECT personal_name FROM Students EXCEPT SELECT T1.personal_name FROM Students AS T1 JOIN Student_Course_Enrolment AS T2 ON T1.student_id = T2.student_id	e_learning
SELECT count(*) FROM Students WHERE student_id NOT IN (SELECT student_id FROM Student_Course_Enrolment)	e_learning
SELECT count(*) FROM Students WHERE student_id NOT IN (SELECT student_id FROM Student_Course_Enrolment)	e_learning
SELECT login_name FROM Course_Authors_and_Tutors INTERSECT SELECT login_name FROM Students	e_learning
SELECT login_name FROM Course_Authors_and_Tutors INTERSECT SELECT login_name FROM Students	e_learning
SELECT personal_name FROM Course_Authors_and_Tutors INTERSECT SELECT personal_name FROM Students	e_learning
SELECT personal_name FROM Course_Authors_and_Tutors INTERSECT SELECT personal_name FROM Students	e_learning
SELECT T1.Date_Claim_Made , T1.Claim_id FROM Claims AS T1 JOIN Settlements AS T2 ON T1.Claim_id = T2.Claim_id GROUP BY T1.Claim_id HAVING count(*) > 2 UNION SELECT T1.Date_Claim_Made , T1.Claim_id FROM Claims AS T1 JOIN Settlements AS T2 ON T1.Claim_id = T2.Claim_id WHERE T1.Amount_Claimed = ( SELECT max(Amount_Claimed) FROM Claims )	insurance_policies
SELECT T1.Date_Claim_Made , T1.Claim_id FROM Claims AS T1 JOIN Settlements AS T2 ON T1.Claim_id = T2.Claim_id GROUP BY T1.Claim_id HAVING count(*) > 2 UNION SELECT T1.Date_Claim_Made , T1.Claim_id FROM Claims AS T1 JOIN Settlements AS T2 ON T1.Claim_id = T2.Claim_id WHERE T1.Amount_Claimed = ( SELECT max(Amount_Claimed) FROM Claims )	insurance_policies
SELECT T1.customer_details , T1.customer_id FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.Customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 2 EXCEPT SELECT T1.customer_details , T1.customer_id FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.Customer_id = T2.customer_id JOIN Claims AS T3 ON T2.policy_id = T3.policy_id	insurance_policies
SELECT T1.customer_details , T1.customer_id FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.Customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 2 EXCEPT SELECT T1.customer_details , T1.customer_id FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.Customer_id = T2.customer_id JOIN Claims AS T3 ON T2.policy_id = T3.policy_id	insurance_policies
SELECT Payment_Method_Code , Date_Payment_Made , Amount_Payment FROM Payments ORDER BY Date_Payment_Made ASC	insurance_policies
SELECT Payment_Method_Code , Date_Payment_Made , Amount_Payment FROM Payments ORDER BY Date_Payment_Made ASC	insurance_policies
SELECT Amount_Settled , Amount_Claimed FROM Claims ORDER BY Amount_Claimed DESC LIMIT 1	insurance_policies
SELECT Amount_Settled , Amount_Claimed FROM Claims ORDER BY Amount_Claimed DESC LIMIT 1	insurance_policies
SELECT Amount_Settled , Amount_Claimed FROM Claims ORDER BY Amount_Settled ASC LIMIT 1	insurance_policies
SELECT Amount_Settled , Amount_Claimed FROM Claims ORDER BY Amount_Settled ASC LIMIT 1	insurance_policies
SELECT Date_Claim_Made , Date_Claim_Settled FROM Claims WHERE Amount_Claimed > ( SELECT avg(Amount_Claimed) FROM Claims )	insurance_policies
SELECT Date_Claim_Made , Date_Claim_Settled FROM Claims WHERE Amount_Claimed > ( SELECT avg(Amount_Claimed) FROM Claims )	insurance_policies
SELECT Date_Claim_Made FROM Claims WHERE Amount_Settled <= ( SELECT avg(Amount_Settled) FROM Claims )	insurance_policies
SELECT Date_Claim_Made FROM Claims WHERE Amount_Settled <= ( SELECT avg(Amount_Settled) FROM Claims )	insurance_policies
SELECT T1.Claim_id , count(*) FROM Claims AS T1 JOIN Settlements AS T2 ON T1.claim_id = T2.claim_id GROUP BY T1.claim_id	insurance_policies
SELECT T1.Claim_id , count(*) FROM Claims AS T1 JOIN Settlements AS T2 ON T1.claim_id = T2.claim_id GROUP BY T1.claim_id	insurance_policies
SELECT T1.claim_id , T1.date_claim_made , count(*) FROM Claims AS T1 JOIN Settlements AS T2 ON T1.claim_id = T2.claim_id GROUP BY T1.claim_id ORDER BY count(*) DESC LIMIT 1	insurance_policies
SELECT T1.claim_id , T1.date_claim_made , count(*) FROM Claims AS T1 JOIN Settlements AS T2 ON T1.claim_id = T2.claim_id GROUP BY T1.claim_id ORDER BY count(*) DESC LIMIT 1	insurance_policies
SELECT count(*) , T1.claim_id FROM Claims AS T1 JOIN Settlements AS T2 ON T1.claim_id = T2.claim_id GROUP BY T1.claim_id ORDER BY T1.Date_Claim_Settled DESC LIMIT 1	insurance_policies
SELECT count(*) , T1.claim_id FROM Claims AS T1 JOIN Settlements AS T2 ON T1.claim_id = T2.claim_id GROUP BY T1.claim_id ORDER BY T1.Date_Claim_Settled DESC LIMIT 1	insurance_policies
SELECT Date_Claim_Made FROM Claims ORDER BY Date_Claim_Made ASC LIMIT 1	insurance_policies
SELECT Date_Claim_Made FROM Claims ORDER BY Date_Claim_Made ASC LIMIT 1	insurance_policies
SELECT sum(Amount_Settled) FROM Settlements	insurance_policies
SELECT sum(Amount_Settled) FROM Settlements	insurance_policies
SELECT T1.customer_details , T1.customer_id FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.Customer_id = T2.Customer_id GROUP BY T1.customer_id HAVING count(*) > 1	insurance_policies
SELECT T1.customer_details , T1.customer_id FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.Customer_id = T2.Customer_id GROUP BY T1.customer_id HAVING count(*) > 1	insurance_policies
SELECT Date_Claim_Made , Date_Claim_Settled FROM Settlements	insurance_policies
SELECT Date_Claim_Made , Date_Claim_Settled FROM Settlements	insurance_policies
SELECT Payment_Method_Code FROM Payments GROUP BY Payment_Method_Code ORDER BY count(*) DESC LIMIT 1	insurance_policies
SELECT Payment_Method_Code FROM Payments GROUP BY Payment_Method_Code ORDER BY count(*) DESC LIMIT 1	insurance_policies
SELECT Payment_Method_Code FROM Payments GROUP BY Payment_Method_Code ORDER BY count(*) ASC LIMIT 1	insurance_policies
SELECT Payment_Method_Code FROM Payments GROUP BY Payment_Method_Code ORDER BY count(*) ASC LIMIT 1	insurance_policies
SELECT sum(Amount_Payment) FROM Payments	insurance_policies
SELECT sum(Amount_Payment) FROM Payments	insurance_policies
SELECT DISTINCT customer_details FROM Customers	insurance_policies
SELECT DISTINCT customer_details FROM Customers	insurance_policies
SELECT Policy_Type_Code FROM Customer_Policies GROUP BY Policy_Type_Code ORDER BY count(*) DESC LIMIT 1	insurance_policies
SELECT Policy_Type_Code FROM Customer_Policies GROUP BY Policy_Type_Code ORDER BY count(*) DESC LIMIT 1	insurance_policies
SELECT count(*) FROM Settlements	insurance_policies
SELECT count(*) FROM Settlements	insurance_policies
SELECT Payment_ID , Date_Payment_Made , Amount_Payment FROM Payments WHERE Payment_Method_Code = 'Visa'	insurance_policies
SELECT Payment_ID , Date_Payment_Made , Amount_Payment FROM Payments WHERE Payment_Method_Code = 'Visa'	insurance_policies
SELECT customer_details FROM Customers EXCEPT SELECT T1.customer_details FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.customer_id = T2.customer_id	insurance_policies
SELECT customer_details FROM Customers EXCEPT SELECT T1.customer_details FROM Customers AS T1 JOIN Customer_Policies AS T2 ON T1.customer_id = T2.customer_id	insurance_policies
SELECT T1.claim_id , T1.date_claim_made , T1.Date_Claim_Settled FROM Claims AS T1 JOIN Settlements AS T2 ON T1.Claim_id = T2.Claim_id GROUP BY T1.claim_id HAVING count(*) = 1	insurance_policies
SELECT T1.claim_id , T1.date_claim_made , T1.Date_Claim_Settled FROM Claims AS T1 JOIN Settlements AS T2 ON T1.Claim_id = T2.Claim_id GROUP BY T1.claim_id HAVING count(*) = 1	insurance_policies
SELECT sum(Amount_Claimed) FROM Claims	insurance_policies
SELECT sum(Amount_Claimed) FROM Claims	insurance_policies
SELECT name FROM department GROUP BY departmentID ORDER BY count(departmentID) DESC LIMIT 1;	hospital_1
SELECT name FROM department GROUP BY departmentID ORDER BY count(departmentID) DESC LIMIT 1;	hospital_1
SELECT head FROM department GROUP BY departmentID ORDER BY count(departmentID) LIMIT 1;	hospital_1
SELECT head FROM department GROUP BY departmentID ORDER BY count(departmentID) LIMIT 1;	hospital_1
SELECT T2.name , T2.position FROM department AS T1 JOIN physician AS T2 ON T1.head = T2.EmployeeID GROUP BY departmentID ORDER BY count(departmentID) LIMIT 1;	hospital_1
SELECT T2.name , T2.position FROM department AS T1 JOIN physician AS T2 ON T1.head = T2.EmployeeID GROUP BY departmentID ORDER BY count(departmentID) LIMIT 1;	hospital_1
SELECT name FROM appointment AS T1 JOIN patient AS T2 ON T1.patient = T2.ssn	hospital_1
SELECT name FROM appointment AS T1 JOIN patient AS T2 ON T1.patient = T2.ssn	hospital_1
SELECT name , phone FROM appointment AS T1 JOIN patient AS T2 ON T1.patient = T2.ssn GROUP BY T1.patient HAVING count(*) > 1	hospital_1
SELECT name , phone FROM appointment AS T1 JOIN patient AS T2 ON T1.patient = T2.ssn GROUP BY T1.patient HAVING count(*) > 1	hospital_1
SELECT appointmentid FROM appointment ORDER BY START DESC LIMIT 1	hospital_1
SELECT appointmentid FROM appointment ORDER BY START DESC LIMIT 1	hospital_1
SELECT T2.name FROM appointment AS T1 JOIN physician AS T2 ON T1.Physician = T2.EmployeeID	hospital_1
SELECT T2.name FROM appointment AS T1 JOIN physician AS T2 ON T1.Physician = T2.EmployeeID	hospital_1
SELECT name FROM physician EXCEPT SELECT T2.name FROM appointment AS T1 JOIN physician AS T2 ON T1.Physician = T2.EmployeeID	hospital_1
SELECT name FROM physician EXCEPT SELECT T2.name FROM appointment AS T1 JOIN physician AS T2 ON T1.Physician = T2.EmployeeID	hospital_1
SELECT T1.name , T3.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T2.PrimaryAffiliation = 1	hospital_1
SELECT T1.name , T3.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T2.PrimaryAffiliation = 1	hospital_1
SELECT T1.name FROM patient AS T1 JOIN appointment AS T2 ON T1.ssn = T2.patient ORDER BY T2.start DESC LIMIT 1	hospital_1
SELECT T1.name FROM patient AS T1 JOIN appointment AS T2 ON T1.ssn = T2.patient ORDER BY T2.start DESC LIMIT 1	hospital_1
SELECT count(patient) FROM stay WHERE room = 112	hospital_1
SELECT count(patient) FROM stay WHERE room = 112	hospital_1
SELECT count(T1.SSN) FROM patient AS T1 JOIN prescribes AS T2 ON T1.SSN = T2.patient JOIN physician AS T3 ON T2.physician = T3.employeeid WHERE T3.name = "John Dorian"	hospital_1
SELECT count(T1.SSN) FROM patient AS T1 JOIN prescribes AS T2 ON T1.SSN = T2.patient JOIN physician AS T3 ON T2.physician = T3.employeeid WHERE T3.name = "John Dorian"	hospital_1
SELECT T4.name FROM stay AS T1 JOIN patient AS T2 ON T1.Patient = T2.SSN JOIN Prescribes AS T3 ON T3.Patient = T2.SSN JOIN Medication AS T4 ON T3.Medication = T4.Code WHERE room = 111	hospital_1
SELECT T4.name FROM stay AS T1 JOIN patient AS T2 ON T1.Patient = T2.SSN JOIN Prescribes AS T3 ON T3.Patient = T2.SSN JOIN Medication AS T4 ON T3.Medication = T4.Code WHERE room = 111	hospital_1
SELECT patient FROM stay WHERE room = 111 ORDER BY staystart DESC LIMIT 1	hospital_1
SELECT patient FROM stay WHERE room = 111 ORDER BY staystart DESC LIMIT 1	hospital_1
SELECT T1.name FROM nurse AS T1 JOIN appointment AS T2 ON T1.employeeid = T2.prepnurse GROUP BY T1.employeeid ORDER BY count(*) DESC LIMIT 1	hospital_1
SELECT T1.name FROM nurse AS T1 JOIN appointment AS T2 ON T1.employeeid = T2.prepnurse GROUP BY T1.employeeid ORDER BY count(*) DESC LIMIT 1	hospital_1
SELECT T1.name , count(*) FROM physician AS T1 JOIN patient AS T2 ON T1.employeeid = T2.PCP GROUP BY T1.employeeid	hospital_1
SELECT T1.name , count(*) FROM physician AS T1 JOIN patient AS T2 ON T1.employeeid = T2.PCP GROUP BY T1.employeeid	hospital_1
SELECT T1.name FROM physician AS T1 JOIN patient AS T2 ON T1.employeeid = T2.PCP GROUP BY T1.employeeid HAVING count(*) > 1	hospital_1
SELECT T1.name FROM physician AS T1 JOIN patient AS T2 ON T1.employeeid = T2.PCP GROUP BY T1.employeeid HAVING count(*) > 1	hospital_1
SELECT count(*) , T1.blockfloor FROM BLOCK AS T1 JOIN room AS T2 ON T1.blockfloor = T2.blockfloor AND T1.blockcode = T2.blockcode GROUP BY T1.blockfloor	hospital_1
SELECT count(*) , T1.blockfloor FROM BLOCK AS T1 JOIN room AS T2 ON T1.blockfloor = T2.blockfloor AND T1.blockcode = T2.blockcode GROUP BY T1.blockfloor	hospital_1
SELECT count(*) , T1.blockcode FROM BLOCK AS T1 JOIN room AS T2 ON T1.blockfloor = T2.blockfloor AND T1.blockcode = T2.blockcode GROUP BY T1.blockcode	hospital_1
SELECT count(*) , T1.blockcode FROM BLOCK AS T1 JOIN room AS T2 ON T1.blockfloor = T2.blockfloor AND T1.blockcode = T2.blockcode GROUP BY T1.blockcode	hospital_1
SELECT DISTINCT blockcode FROM room WHERE unavailable = 0	hospital_1
SELECT DISTINCT blockcode FROM room WHERE unavailable = 0	hospital_1
SELECT count(DISTINCT roomtype) FROM room	hospital_1
SELECT count(DISTINCT roomtype) FROM room	hospital_1
SELECT DISTINCT T1.name FROM physician AS T1 JOIN prescribes AS T2 ON T1.employeeid = T2.physician JOIN medication AS T3 ON T3.code = T2.medication WHERE T3.name = "Thesisin"	hospital_1
SELECT DISTINCT T1.name FROM physician AS T1 JOIN prescribes AS T2 ON T1.employeeid = T2.physician JOIN medication AS T3 ON T3.code = T2.medication WHERE T3.name = "Thesisin"	hospital_1
SELECT DISTINCT T1.name , T1.position FROM physician AS T1 JOIN prescribes AS T2 ON T1.employeeid = T2.physician JOIN medication AS T3 ON T3.code = T2.medication WHERE T3.Brand = "X"	hospital_1
SELECT DISTINCT T1.name , T1.position FROM physician AS T1 JOIN prescribes AS T2 ON T1.employeeid = T2.physician JOIN medication AS T3 ON T3.code = T2.medication WHERE T3.Brand = "X"	hospital_1
SELECT count(*) , T1.name FROM medication AS T1 JOIN prescribes AS T2 ON T1.code = T2.medication GROUP BY T1.brand	hospital_1
SELECT count(*) , T1.name FROM medication AS T1 JOIN prescribes AS T2 ON T1.code = T2.medication GROUP BY T1.brand	hospital_1
SELECT name FROM physician WHERE POSITION LIKE '%senior%'	hospital_1
SELECT name FROM physician WHERE POSITION LIKE '%senior%'	hospital_1
SELECT patient FROM undergoes ORDER BY dateundergoes LIMIT 1	hospital_1
SELECT patient FROM undergoes ORDER BY dateundergoes LIMIT 1	hospital_1
SELECT DISTINCT T2.name FROM undergoes AS T1 JOIN patient AS T2 ON T1.patient = T2.SSN JOIN stay AS T3 ON T1.Stay = T3.StayID WHERE T3.room = 111	hospital_1
SELECT DISTINCT T2.name FROM undergoes AS T1 JOIN patient AS T2 ON T1.patient = T2.SSN JOIN stay AS T3 ON T1.Stay = T3.StayID WHERE T3.room = 111	hospital_1
SELECT DISTINCT name FROM nurse ORDER BY name	hospital_1
SELECT DISTINCT name FROM nurse ORDER BY name	hospital_1
SELECT DISTINCT T2.name FROM undergoes AS T1 JOIN nurse AS T2 ON T1.AssistingNurse = T2.EmployeeID	hospital_1
SELECT DISTINCT T2.name FROM undergoes AS T1 JOIN nurse AS T2 ON T1.AssistingNurse = T2.EmployeeID	hospital_1
SELECT DISTINCT name FROM medication ORDER BY name	hospital_1
SELECT DISTINCT name FROM medication ORDER BY name	hospital_1
SELECT T1.name FROM physician AS T1 JOIN prescribes AS T2 ON T1.employeeid = T2.physician ORDER BY T2.dose DESC LIMIT 1	hospital_1
SELECT T1.name FROM physician AS T1 JOIN prescribes AS T2 ON T1.employeeid = T2.physician ORDER BY T2.dose DESC LIMIT 1	hospital_1
SELECT physician , department FROM affiliated_with WHERE primaryaffiliation = 1	hospital_1
SELECT physician , department FROM affiliated_with WHERE primaryaffiliation = 1	hospital_1
SELECT DISTINCT T2.name FROM affiliated_with AS T1 JOIN department AS T2 ON T1.department = T2.departmentid WHERE PrimaryAffiliation = 1	hospital_1
SELECT DISTINCT T2.name FROM affiliated_with AS T1 JOIN department AS T2 ON T1.department = T2.departmentid WHERE PrimaryAffiliation = 1	hospital_1
SELECT nurse FROM on_call WHERE blockfloor = 1 AND blockcode = 1	hospital_1
SELECT nurse FROM on_call WHERE blockfloor = 1 AND blockcode = 1	hospital_1
SELECT MAX(cost) , MIN(cost) , AVG(cost) FROM procedures	hospital_1
SELECT MAX(cost) , MIN(cost) , AVG(cost) FROM procedures	hospital_1
SELECT name , cost FROM procedures ORDER BY cost DESC	hospital_1
SELECT name , cost FROM procedures ORDER BY cost DESC	hospital_1
SELECT name FROM procedures ORDER BY cost LIMIT 3	hospital_1
SELECT name FROM procedures ORDER BY cost LIMIT 3	hospital_1
SELECT T1.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T3.cost > 5000	hospital_1
SELECT T1.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T3.cost > 5000	hospital_1
SELECT T1.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment ORDER BY T3.cost DESC LIMIT 1	hospital_1
SELECT T1.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment ORDER BY T3.cost DESC LIMIT 1	hospital_1
SELECT avg(T3.cost) FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT avg(T3.cost) FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT name FROM procedures WHERE cost > 1000 UNION SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT name FROM procedures WHERE cost > 1000 UNION SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT name FROM procedures WHERE cost > 1000 EXCEPT SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT name FROM procedures WHERE cost > 1000 EXCEPT SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT name FROM procedures WHERE cost < 5000 INTERSECT SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT name FROM procedures WHERE cost < 5000 INTERSECT SELECT T3.name FROM physician AS T1 JOIN trained_in AS T2 ON T1.employeeid = T2.physician JOIN procedures AS T3 ON T3.code = T2.treatment WHERE T1.name = "John Wen"	hospital_1
SELECT T1.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T3.name = 'Surgery' INTERSECT SELECT T1.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T3.name = 'Psychiatry'	hospital_1
SELECT T1.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T3.name = 'Surgery' INTERSECT SELECT T1.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T3.name = 'Psychiatry'	hospital_1
SELECT T1.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T3.name = 'Surgery' OR T3.name = 'Psychiatry'	hospital_1
SELECT T1.name FROM physician AS T1 JOIN affiliated_with AS T2 ON T1.EmployeeID = T2.physician JOIN department AS T3 ON T2.department = T3.DepartmentID WHERE T3.name = 'Surgery' OR T3.name = 'Psychiatry'	hospital_1
SELECT name FROM patient EXCEPT SELECT T1.name FROM patient AS T1 JOIN Prescribes AS T2 ON T2.Patient = T1.SSN JOIN Medication AS T3 ON T2.Medication = T3.Code WHERE T3.name = 'Procrastin-X'	hospital_1
SELECT name FROM patient EXCEPT SELECT T1.name FROM patient AS T1 JOIN Prescribes AS T2 ON T2.Patient = T1.SSN JOIN Medication AS T3 ON T2.Medication = T3.Code WHERE T3.name = 'Procrastin-X'	hospital_1
SELECT count(*) FROM patient WHERE SSN NOT IN ( SELECT T1.patient FROM Prescribes AS T1 JOIN Medication AS T2 ON T1.Medication = T2.Code WHERE T2.name = 'Procrastin-X' )	hospital_1
SELECT count(*) FROM patient WHERE SSN NOT IN ( SELECT T1.patient FROM Prescribes AS T1 JOIN Medication AS T2 ON T1.Medication = T2.Code WHERE T2.name = 'Procrastin-X' )	hospital_1
SELECT count(*) FROM appointment	hospital_1
SELECT count(*) FROM appointment	hospital_1
SELECT DISTINCT T1.name FROM nurse AS T1 JOIN on_call AS T2 ON T1.EmployeeID = T2.nurse	hospital_1
SELECT DISTINCT T1.name FROM nurse AS T1 JOIN on_call AS T2 ON T1.EmployeeID = T2.nurse	hospital_1
SELECT count(*) FROM ship	ship_mission
SELECT count(*) FROM ship	ship_mission
SELECT Name FROM ship ORDER BY Tonnage ASC	ship_mission
SELECT Name FROM ship ORDER BY Tonnage ASC	ship_mission
SELECT TYPE , Nationality FROM ship	ship_mission
SELECT TYPE , Nationality FROM ship	ship_mission
SELECT Name FROM ship WHERE Nationality != "United States"	ship_mission
SELECT Name FROM ship WHERE Nationality != "United States"	ship_mission
SELECT Name FROM ship WHERE Nationality = "United States" OR Nationality = "United Kingdom"	ship_mission
SELECT Name FROM ship WHERE Nationality = "United States" OR Nationality = "United Kingdom"	ship_mission
SELECT Name FROM ship ORDER BY Tonnage DESC LIMIT 1	ship_mission
SELECT Name FROM ship ORDER BY Tonnage DESC LIMIT 1	ship_mission
SELECT TYPE , COUNT(*) FROM ship GROUP BY TYPE	ship_mission
SELECT TYPE , COUNT(*) FROM ship GROUP BY TYPE	ship_mission
SELECT TYPE FROM ship GROUP BY TYPE ORDER BY COUNT(*) DESC LIMIT 1	ship_mission
SELECT TYPE FROM ship GROUP BY TYPE ORDER BY COUNT(*) DESC LIMIT 1	ship_mission
SELECT Nationality FROM ship GROUP BY Nationality HAVING COUNT(*) > 2	ship_mission
SELECT Nationality FROM ship GROUP BY Nationality HAVING COUNT(*) > 2	ship_mission
SELECT TYPE , avg(Tonnage) FROM ship GROUP BY TYPE	ship_mission
SELECT TYPE , avg(Tonnage) FROM ship GROUP BY TYPE	ship_mission
SELECT T1.Code , T1.Fate , T2.Name FROM mission AS T1 JOIN ship AS T2 ON T1.Ship_ID = T2.Ship_ID	ship_mission
SELECT T1.Code , T1.Fate , T2.Name FROM mission AS T1 JOIN ship AS T2 ON T1.Ship_ID = T2.Ship_ID	ship_mission
SELECT T2.Name FROM mission AS T1 JOIN ship AS T2 ON T1.Ship_ID = T2.Ship_ID WHERE T1.Launched_Year > 1928	ship_mission
SELECT T2.Name FROM mission AS T1 JOIN ship AS T2 ON T1.Ship_ID = T2.Ship_ID WHERE T1.Launched_Year > 1928	ship_mission
SELECT DISTINCT T1.Fate FROM mission AS T1 JOIN ship AS T2 ON T1.Ship_ID = T2.Ship_ID WHERE T2.Nationality = "United States"	ship_mission
SELECT DISTINCT T1.Fate FROM mission AS T1 JOIN ship AS T2 ON T1.Ship_ID = T2.Ship_ID WHERE T2.Nationality = "United States"	ship_mission
SELECT Name FROM ship WHERE Ship_ID NOT IN (SELECT Ship_ID FROM mission)	ship_mission
SELECT Name FROM ship WHERE Ship_ID NOT IN (SELECT Ship_ID FROM mission)	ship_mission
SELECT TYPE FROM ship WHERE Tonnage > 6000 INTERSECT SELECT TYPE FROM ship WHERE Tonnage < 4000	ship_mission
SELECT TYPE FROM ship WHERE Tonnage > 6000 INTERSECT SELECT TYPE FROM ship WHERE Tonnage < 4000	ship_mission
SELECT count(*) FROM list	student_1
SELECT count(*) FROM list	student_1
SELECT lastname FROM list WHERE classroom = 111	student_1
SELECT lastname FROM list WHERE classroom = 111	student_1
SELECT firstname FROM list WHERE classroom = 108	student_1
SELECT firstname FROM list WHERE classroom = 108	student_1
SELECT DISTINCT firstname FROM list WHERE classroom = 107	student_1
SELECT DISTINCT firstname FROM list WHERE classroom = 107	student_1
SELECT DISTINCT classroom , grade FROM list	student_1
SELECT DISTINCT classroom , grade FROM list	student_1
SELECT DISTINCT grade FROM list WHERE classroom = 103	student_1
SELECT DISTINCT grade FROM list WHERE classroom = 103	student_1
SELECT DISTINCT grade FROM list WHERE classroom = 105	student_1
SELECT DISTINCT grade FROM list WHERE classroom = 105	student_1
SELECT DISTINCT classroom FROM list WHERE grade = 4	student_1
SELECT DISTINCT classroom FROM list WHERE grade = 4	student_1
SELECT DISTINCT classroom FROM list WHERE grade = 5	student_1
SELECT DISTINCT classroom FROM list WHERE grade = 5	student_1
SELECT DISTINCT T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE grade = 5	student_1
SELECT DISTINCT T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE grade = 5	student_1
SELECT DISTINCT T2.firstname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE grade = 1	student_1
SELECT DISTINCT T2.firstname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE grade = 1	student_1
SELECT firstname FROM teachers WHERE classroom = 110	student_1
SELECT firstname FROM teachers WHERE classroom = 110	student_1
SELECT lastname FROM teachers WHERE classroom = 109	student_1
SELECT lastname FROM teachers WHERE classroom = 109	student_1
SELECT DISTINCT firstname , lastname FROM teachers	student_1
SELECT DISTINCT firstname , lastname FROM teachers	student_1
SELECT DISTINCT firstname , lastname FROM list	student_1
SELECT DISTINCT firstname , lastname FROM list	student_1
SELECT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "OTHA" AND T2.lastname = "MOYER"	student_1
SELECT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "OTHA" AND T2.lastname = "MOYER"	student_1
SELECT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "MARROTTE" AND T2.lastname = "KIRK"	student_1
SELECT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "MARROTTE" AND T2.lastname = "KIRK"	student_1
SELECT T2.firstname , T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "EVELINA" AND T1.lastname = "BROMLEY"	student_1
SELECT T2.firstname , T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "EVELINA" AND T1.lastname = "BROMLEY"	student_1
SELECT T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "GELL" AND T1.lastname = "TAMI"	student_1
SELECT T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "GELL" AND T1.lastname = "TAMI"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "LORIA" AND T2.lastname = "ONDERSMA"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "LORIA" AND T2.lastname = "ONDERSMA"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "KAWA" AND T2.lastname = "GORDON"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "KAWA" AND T2.lastname = "GORDON"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "TARRING" AND T2.lastname = "LEIA"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "TARRING" AND T2.lastname = "LEIA"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "CHRISSY" AND T1.lastname = "NABOZNY"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "CHRISSY" AND T1.lastname = "NABOZNY"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "MADLOCK" AND T1.lastname = "RAY"	student_1
SELECT count(*) FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.firstname = "MADLOCK" AND T1.lastname = "RAY"	student_1
SELECT DISTINCT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.grade = 1 EXCEPT SELECT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "OTHA" AND T2.lastname = "MOYER"	student_1
SELECT DISTINCT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.grade = 1 EXCEPT SELECT T1.firstname , T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T2.firstname = "OTHA" AND T2.lastname = "MOYER"	student_1
SELECT DISTINCT T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.grade = 3 AND T2.firstname != "COVIN" AND T2.lastname != "JEROME"	student_1
SELECT DISTINCT T1.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom WHERE T1.grade = 3 AND T2.firstname != "COVIN" AND T2.lastname != "JEROME"	student_1
SELECT grade , count(DISTINCT classroom) , count(*) FROM list GROUP BY grade	student_1
SELECT grade , count(DISTINCT classroom) , count(*) FROM list GROUP BY grade	student_1
SELECT classroom , count(DISTINCT grade) FROM list GROUP BY classroom	student_1
SELECT classroom , count(DISTINCT grade) FROM list GROUP BY classroom	student_1
SELECT classroom FROM list GROUP BY classroom ORDER BY count(*) DESC LIMIT 1	student_1
SELECT classroom FROM list GROUP BY classroom ORDER BY count(*) DESC LIMIT 1	student_1
SELECT classroom , count(*) FROM list GROUP BY classroom	student_1
SELECT classroom , count(*) FROM list GROUP BY classroom	student_1
SELECT classroom , count(*) FROM list WHERE grade = "0" GROUP BY classroom	student_1
SELECT classroom , count(*) FROM list WHERE grade = "0" GROUP BY classroom	student_1
SELECT classroom , count(*) FROM list WHERE grade = "4" GROUP BY classroom	student_1
SELECT classroom , count(*) FROM list WHERE grade = "4" GROUP BY classroom	student_1
SELECT T2.firstname , T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom GROUP BY T2.firstname , T2.lastname ORDER BY count(*) DESC LIMIT 1	student_1
SELECT T2.firstname , T2.lastname FROM list AS T1 JOIN teachers AS T2 ON T1.classroom = T2.classroom GROUP BY T2.firstname , T2.lastname ORDER BY count(*) DESC LIMIT 1	student_1
SELECT count(*) , classroom FROM list GROUP BY classroom	student_1
SELECT count(*) , classroom FROM list GROUP BY classroom	student_1
SELECT count(*) FROM film	film_rank
SELECT count(*) FROM film	film_rank
SELECT DISTINCT Director FROM film	film_rank
SELECT DISTINCT Director FROM film	film_rank
SELECT avg(Gross_in_dollar) FROM film	film_rank
SELECT avg(Gross_in_dollar) FROM film	film_rank
SELECT Low_Estimate , High_Estimate FROM film_market_estimation	film_rank
SELECT Low_Estimate , High_Estimate FROM film_market_estimation	film_rank
SELECT TYPE FROM film_market_estimation WHERE YEAR = 1995	film_rank
SELECT TYPE FROM film_market_estimation WHERE YEAR = 1995	film_rank
SELECT max(Number_cities) , min(Number_cities) FROM market	film_rank
SELECT max(Number_cities) , min(Number_cities) FROM market	film_rank
SELECT count(*) FROM market WHERE Number_cities < 300	film_rank
SELECT count(*) FROM market WHERE Number_cities < 300	film_rank
SELECT Country FROM market ORDER BY Country ASC	film_rank
SELECT Country FROM market ORDER BY Country ASC	film_rank
SELECT Country FROM market ORDER BY Number_cities DESC	film_rank
SELECT Country FROM market ORDER BY Number_cities DESC	film_rank
SELECT T1.Title , T2.Type FROM film AS T1 JOIN film_market_estimation AS T2 ON T1.Film_ID = T2.Film_ID	film_rank
SELECT T1.Title , T2.Type FROM film AS T1 JOIN film_market_estimation AS T2 ON T1.Film_ID = T2.Film_ID	film_rank
SELECT DISTINCT T1.Director FROM film AS T1 JOIN film_market_estimation AS T2 ON T1.Film_ID = T2.Film_ID WHERE T2.Year = 1995	film_rank
SELECT DISTINCT T1.Director FROM film AS T1 JOIN film_market_estimation AS T2 ON T1.Film_ID = T2.Film_ID WHERE T2.Year = 1995	film_rank
SELECT avg(T2.Number_cities) FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID WHERE T1.Low_Estimate > 10000	film_rank
SELECT avg(T2.Number_cities) FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID WHERE T1.Low_Estimate > 10000	film_rank
SELECT T2.Country , T1.Year FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID	film_rank
SELECT T2.Country , T1.Year FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID	film_rank
SELECT T1.Year FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID WHERE T2.Country = "Japan" ORDER BY T1.Year DESC	film_rank
SELECT T1.Year FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.Market_ID = T2.Market_ID WHERE T2.Country = "Japan" ORDER BY T1.Year DESC	film_rank
SELECT Studio , COUNT(*) FROM film GROUP BY Studio	film_rank
SELECT Studio , COUNT(*) FROM film GROUP BY Studio	film_rank
SELECT Studio FROM film GROUP BY Studio ORDER BY COUNT(*) DESC LIMIT 1	film_rank
SELECT Studio FROM film GROUP BY Studio ORDER BY COUNT(*) DESC LIMIT 1	film_rank
SELECT Studio FROM film GROUP BY Studio HAVING COUNT(*) >= 2	film_rank
SELECT Studio FROM film GROUP BY Studio HAVING COUNT(*) >= 2	film_rank
SELECT Title FROM film WHERE Film_ID NOT IN (SELECT Film_ID FROM film_market_estimation)	film_rank
SELECT Title FROM film WHERE Film_ID NOT IN (SELECT Film_ID FROM film_market_estimation)	film_rank
SELECT Studio FROM film WHERE Director = "Nicholas Meyer" INTERSECT SELECT Studio FROM film WHERE Director = "Walter Hill"	film_rank
SELECT Studio FROM film WHERE Director = "Nicholas Meyer" INTERSECT SELECT Studio FROM film WHERE Director = "Walter Hill"	film_rank
SELECT title , Studio FROM film WHERE Studio LIKE "%Universal%"	film_rank
SELECT title , Studio FROM film WHERE Studio LIKE "%Universal%"	film_rank
SELECT Studio FROM film EXCEPT SELECT Studio FROM film WHERE Director = "Walter Hill"	film_rank
SELECT Studio FROM film EXCEPT SELECT Studio FROM film WHERE Director = "Walter Hill"	film_rank
SELECT Studio FROM film GROUP BY Studio HAVING avg(Gross_in_dollar) >= 4500000	film_rank
SELECT Studio FROM film GROUP BY Studio HAVING avg(Gross_in_dollar) >= 4500000	film_rank
SELECT t1.title FROM film AS T1 JOIN film_market_estimation AS T2 ON T1.Film_ID = T2.Film_ID ORDER BY high_estimate DESC LIMIT 1	film_rank
SELECT t1.title FROM film AS T1 JOIN film_market_estimation AS T2 ON T1.Film_ID = T2.Film_ID ORDER BY high_estimate DESC LIMIT 1	film_rank
SELECT title , director FROM film WHERE film_id NOT IN (SELECT film_id FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.market_id = T2.Market_ID WHERE country = 'China')	film_rank
SELECT title , director FROM film WHERE film_id NOT IN (SELECT film_id FROM film_market_estimation AS T1 JOIN market AS T2 ON T1.market_id = T2.Market_ID WHERE country = 'China')	film_rank
SELECT count(T2.friend) FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T1.name = 'Dan'	network_2
SELECT count(T2.friend) FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T1.name = 'Dan'	network_2
SELECT count(*) FROM Person WHERE gender = 'female'	network_2
SELECT count(*) FROM Person WHERE gender = 'female'	network_2
SELECT avg(age) FROM Person	network_2
SELECT avg(age) FROM Person	network_2
SELECT count(DISTINCT city) FROM Person	network_2
SELECT count(DISTINCT city) FROM Person	network_2
SELECT count(DISTINCT job) FROM Person	network_2
SELECT count(DISTINCT job) FROM Person	network_2
SELECT name FROM Person WHERE age = (SELECT max(age) FROM person)	network_2
SELECT name FROM Person WHERE age = (SELECT max(age) FROM person)	network_2
SELECT name FROM Person WHERE job = 'student' AND age = (SELECT max(age) FROM person WHERE job = 'student' )	network_2
SELECT name FROM Person WHERE job = 'student' AND age = (SELECT max(age) FROM person WHERE job = 'student' )	network_2
SELECT name FROM Person WHERE gender = 'male' AND age = (SELECT min(age) FROM person WHERE gender = 'male' )	network_2
SELECT name FROM Person WHERE gender = 'male' AND age = (SELECT min(age) FROM person WHERE gender = 'male' )	network_2
SELECT age FROM Person WHERE job = 'doctor' AND name = 'Zach'	network_2
SELECT age FROM Person WHERE job = 'doctor' AND name = 'Zach'	network_2
SELECT name FROM Person WHERE age < 30	network_2
SELECT name FROM Person WHERE age < 30	network_2
SELECT count(*) FROM Person WHERE age > 30 AND job = 'engineer'	network_2
SELECT count(*) FROM Person WHERE age > 30 AND job = 'engineer'	network_2
SELECT avg(age) , gender FROM Person GROUP BY gender	network_2
SELECT avg(age) , gender FROM Person GROUP BY gender	network_2
SELECT avg(age) , job FROM Person GROUP BY job	network_2
SELECT avg(age) , job FROM Person GROUP BY job	network_2
SELECT avg(age) , job FROM Person WHERE gender = 'male' GROUP BY job	network_2
SELECT avg(age) , job FROM Person WHERE gender = 'male' GROUP BY job	network_2
SELECT min(age) , job FROM Person GROUP BY job	network_2
SELECT min(age) , job FROM Person GROUP BY job	network_2
SELECT count(*) , gender FROM Person WHERE age < 40 GROUP BY gender	network_2
SELECT count(*) , gender FROM Person WHERE age < 40 GROUP BY gender	network_2
SELECT name FROM Person WHERE age > (SELECT min(age) FROM person WHERE job = 'engineer') ORDER BY age	network_2
SELECT name FROM Person WHERE age > (SELECT min(age) FROM person WHERE job = 'engineer') ORDER BY age	network_2
SELECT count(*) FROM Person WHERE age > (SELECT max(age) FROM person WHERE job = 'engineer')	network_2
SELECT count(*) FROM Person WHERE age > (SELECT max(age) FROM person WHERE job = 'engineer')	network_2
SELECT name , job FROM Person ORDER BY name	network_2
SELECT name , job FROM Person ORDER BY name	network_2
SELECT name FROM Person ORDER BY age DESC	network_2
SELECT name FROM Person ORDER BY age DESC	network_2
SELECT name FROM Person WHERE gender = 'male' ORDER BY age	network_2
SELECT name FROM Person WHERE gender = 'male' ORDER BY age	network_2
SELECT T1.name , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Dan' INTERSECT SELECT T1.name , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Alice'	network_2
SELECT T1.name , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Dan' INTERSECT SELECT T1.name , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Alice'	network_2
SELECT DISTINCT T1.name , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Dan' OR T2.friend = 'Alice'	network_2
SELECT DISTINCT T1.name , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Dan' OR T2.friend = 'Alice'	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age > 40) INTERSECT SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age < 30)	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age > 40) INTERSECT SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age < 30)	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age > 40) EXCEPT SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age < 30)	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age > 40) EXCEPT SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend IN (SELECT name FROM Person WHERE age < 30)	network_2
SELECT name FROM person EXCEPT SELECT T2.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.job = 'student'	network_2
SELECT name FROM person EXCEPT SELECT T2.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.job = 'student'	network_2
SELECT name FROM PersonFriend GROUP BY name HAVING count(*) = 1	network_2
SELECT name FROM PersonFriend GROUP BY name HAVING count(*) = 1	network_2
SELECT T2.friend FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T1.name = 'Bob'	network_2
SELECT T2.friend FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T1.name = 'Bob'	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Bob'	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Bob'	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Zach' AND T1.gender = 'female'	network_2
SELECT T1.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Zach' AND T1.gender = 'female'	network_2
SELECT T2.friend FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T2.name = 'Alice' AND T1.gender = 'female'	network_2
SELECT T2.friend FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T2.name = 'Alice' AND T1.gender = 'female'	network_2
SELECT T2.friend FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T2.name = 'Alice' AND T1.gender = 'male' AND T1.job = 'doctor'	network_2
SELECT T2.friend FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T2.name = 'Alice' AND T1.gender = 'male' AND T1.job = 'doctor'	network_2
SELECT T2.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.city = 'new york city'	network_2
SELECT T2.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.city = 'new york city'	network_2
SELECT DISTINCT T2.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.age < (SELECT avg(age) FROM person)	network_2
SELECT DISTINCT T2.name FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.age < (SELECT avg(age) FROM person)	network_2
SELECT DISTINCT T2.name , T2.friend , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.age > (SELECT avg(age) FROM person)	network_2
SELECT DISTINCT T2.name , T2.friend , T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T1.age > (SELECT avg(age) FROM person)	network_2
SELECT friend FROM PersonFriend WHERE name = 'Zach' AND YEAR = (SELECT max(YEAR) FROM PersonFriend WHERE name = 'Zach')	network_2
SELECT friend FROM PersonFriend WHERE name = 'Zach' AND YEAR = (SELECT max(YEAR) FROM PersonFriend WHERE name = 'Zach')	network_2
SELECT T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T2.name = 'Zach' AND T2.year = (SELECT max(YEAR) FROM PersonFriend WHERE name = 'Zach')	network_2
SELECT T1.age FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend WHERE T2.name = 'Zach' AND T2.year = (SELECT max(YEAR) FROM PersonFriend WHERE name = 'Zach')	network_2
SELECT name FROM PersonFriend WHERE friend = 'Alice' AND YEAR = (SELECT min(YEAR) FROM PersonFriend WHERE friend = 'Alice')	network_2
SELECT name FROM PersonFriend WHERE friend = 'Alice' AND YEAR = (SELECT min(YEAR) FROM PersonFriend WHERE friend = 'Alice')	network_2
SELECT T1.name , T1.age , T1.job FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Alice' AND T2.year = (SELECT max(YEAR) FROM PersonFriend WHERE friend = 'Alice')	network_2
SELECT T1.name , T1.age , T1.job FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.name WHERE T2.friend = 'Alice' AND T2.year = (SELECT max(YEAR) FROM PersonFriend WHERE friend = 'Alice')	network_2
SELECT name FROM person EXCEPT SELECT name FROM PersonFriend	network_2
SELECT name FROM person EXCEPT SELECT name FROM PersonFriend	network_2
SELECT T2.name , avg(T1.age) FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend GROUP BY T2.name ORDER BY avg(T1.age) DESC LIMIT 1	network_2
SELECT T2.name , avg(T1.age) FROM Person AS T1 JOIN PersonFriend AS T2 ON T1.name = T2.friend GROUP BY T2.name ORDER BY avg(T1.age) DESC LIMIT 1	network_2
SELECT count(DISTINCT name) FROM PersonFriend WHERE friend NOT IN (SELECT name FROM person WHERE city = 'Austin')	network_2
SELECT count(DISTINCT name) FROM PersonFriend WHERE friend NOT IN (SELECT name FROM person WHERE city = 'Austin')	network_2
SELECT DISTINCT T4.name FROM PersonFriend AS T1 JOIN Person AS T2 ON T1.name = T2.name JOIN PersonFriend AS T3 ON T1.friend = T3.name JOIN PersonFriend AS T4 ON T3.friend = T4.name WHERE T2.name = 'Alice' AND T4.name != 'Alice'	network_2
SELECT DISTINCT T4.name FROM PersonFriend AS T1 JOIN Person AS T2 ON T1.name = T2.name JOIN PersonFriend AS T3 ON T1.friend = T3.name JOIN PersonFriend AS T4 ON T3.friend = T4.name WHERE T2.name = 'Alice' AND T4.name != 'Alice'	network_2
SELECT document_name , access_count FROM documents ORDER BY document_name	document_management
SELECT document_name , access_count FROM documents ORDER BY document_name	document_management
SELECT document_name , access_count FROM documents ORDER BY access_count DESC LIMIT 1	document_management
SELECT document_name , access_count FROM documents ORDER BY access_count DESC LIMIT 1	document_management
SELECT document_type_code FROM documents GROUP BY document_type_code HAVING count(*) > 4	document_management
SELECT document_type_code FROM documents GROUP BY document_type_code HAVING count(*) > 4	document_management
SELECT sum(access_count) FROM documents GROUP BY document_type_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT sum(access_count) FROM documents GROUP BY document_type_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT avg(access_count) FROM documents	document_management
SELECT avg(access_count) FROM documents	document_management
SELECT t2.document_structure_description FROM documents AS t1 JOIN document_structures AS t2 ON t1.document_structure_code = t2.document_structure_code GROUP BY t1.document_structure_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT t2.document_structure_description FROM documents AS t1 JOIN document_structures AS t2 ON t1.document_structure_code = t2.document_structure_code GROUP BY t1.document_structure_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT document_type_code FROM documents WHERE document_name = "David CV"	document_management
SELECT document_type_code FROM documents WHERE document_name = "David CV"	document_management
SELECT document_name FROM documents GROUP BY document_type_code ORDER BY count(*) DESC LIMIT 3 INTERSECT SELECT document_name FROM documents GROUP BY document_structure_code ORDER BY count(*) DESC LIMIT 3	document_management
SELECT document_name FROM documents GROUP BY document_type_code ORDER BY count(*) DESC LIMIT 3 INTERSECT SELECT document_name FROM documents GROUP BY document_structure_code ORDER BY count(*) DESC LIMIT 3	document_management
SELECT document_type_code FROM documents GROUP BY document_type_code HAVING sum(access_count) > 10000	document_management
SELECT document_type_code FROM documents GROUP BY document_type_code HAVING sum(access_count) > 10000	document_management
SELECT t2.section_title FROM documents AS t1 JOIN document_sections AS t2 ON t1.document_code = t2.document_code WHERE t1.document_name = "David CV"	document_management
SELECT t2.section_title FROM documents AS t1 JOIN document_sections AS t2 ON t1.document_code = t2.document_code WHERE t1.document_name = "David CV"	document_management
SELECT document_name FROM documents WHERE document_code NOT IN (SELECT document_code FROM document_sections)	document_management
SELECT document_name FROM documents WHERE document_code NOT IN (SELECT document_code FROM document_sections)	document_management
SELECT user_name , password FROM users GROUP BY role_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT user_name , password FROM users GROUP BY role_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT avg(t1.access_count) FROM documents AS t1 JOIN document_functional_areas AS t2 ON t1.document_code = t2.document_code JOIN functional_areas AS t3 ON t2.functional_area_code = t3.functional_area_code WHERE t3.functional_area_description = "Acknowledgement"	document_management
SELECT avg(t1.access_count) FROM documents AS t1 JOIN document_functional_areas AS t2 ON t1.document_code = t2.document_code JOIN functional_areas AS t3 ON t2.functional_area_code = t3.functional_area_code WHERE t3.functional_area_description = "Acknowledgement"	document_management
SELECT document_name FROM documents EXCEPT SELECT t1.document_name FROM documents AS t1 JOIN document_sections AS t2 ON t1.document_code = t2.document_code JOIN document_sections_images AS t3 ON t2.section_id = t3.section_id	document_management
SELECT document_name FROM documents EXCEPT SELECT t1.document_name FROM documents AS t1 JOIN document_sections AS t2 ON t1.document_code = t2.document_code JOIN document_sections_images AS t3 ON t2.section_id = t3.section_id	document_management
SELECT t1.document_name FROM documents AS t1 JOIN document_sections AS t2 ON t1.document_code = t2.document_code GROUP BY t1.document_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT t1.document_name FROM documents AS t1 JOIN document_sections AS t2 ON t1.document_code = t2.document_code GROUP BY t1.document_code ORDER BY count(*) DESC LIMIT 1	document_management
SELECT document_name FROM documents WHERE document_name LIKE "%CV%"	document_management
SELECT document_name FROM documents WHERE document_name LIKE "%CV%"	document_management
SELECT count(*) FROM users WHERE user_login = 1	document_management
SELECT count(*) FROM users WHERE user_login = 1	document_management
SELECT role_description FROM ROLES WHERE role_code = (SELECT role_code FROM users WHERE user_login = 1 GROUP BY role_code ORDER BY count(*) DESC LIMIT 1)	document_management
SELECT role_description FROM ROLES WHERE role_code = (SELECT role_code FROM users WHERE user_login = 1 GROUP BY role_code ORDER BY count(*) DESC LIMIT 1)	document_management
SELECT avg(access_count) FROM documents GROUP BY document_structure_code ORDER BY count(*) ASC LIMIT 1	document_management
SELECT avg(access_count) FROM documents GROUP BY document_structure_code ORDER BY count(*) ASC LIMIT 1	document_management
SELECT image_name , image_url FROM images ORDER BY image_name	document_management
SELECT image_name , image_url FROM images ORDER BY image_name	document_management
SELECT count(*) , role_code FROM users GROUP BY role_code	document_management
SELECT count(*) , role_code FROM users GROUP BY role_code	document_management
SELECT document_type_code FROM documents GROUP BY document_type_code HAVING count(*) > 2	document_management
SELECT document_type_code FROM documents GROUP BY document_type_code HAVING count(*) > 2	document_management
SELECT count(*) FROM Products	solvency_ii
SELECT Product_Name FROM Products ORDER BY Product_Price ASC	solvency_ii
SELECT Product_Name , Product_Type_Code FROM Products	solvency_ii
SELECT Product_Price FROM Products WHERE Product_Name = "Dining" OR Product_Name = "Trading Policy"	solvency_ii
SELECT avg(Product_Price) FROM Products	solvency_ii
SELECT Product_Name FROM Products ORDER BY Product_Price DESC LIMIT 1	solvency_ii
SELECT Product_Type_Code , COUNT(*) FROM Products GROUP BY Product_Type_Code	solvency_ii
SELECT Product_Type_Code FROM Products GROUP BY Product_Type_Code ORDER BY COUNT(*) DESC LIMIT 1	solvency_ii
SELECT Product_Type_Code FROM Products GROUP BY Product_Type_Code HAVING COUNT(*) >= 2	solvency_ii
SELECT Product_Type_Code FROM Products WHERE Product_Price > 4500 INTERSECT SELECT Product_Type_Code FROM Products WHERE Product_Price < 3000	solvency_ii
SELECT T1.Product_Name , COUNT(*) FROM Products AS T1 JOIN Products_in_Events AS T2 ON T1.Product_ID = T2.Product_ID GROUP BY T1.Product_Name	solvency_ii
SELECT T1.Product_Name , COUNT(*) FROM Products AS T1 JOIN Products_in_Events AS T2 ON T1.Product_ID = T2.Product_ID GROUP BY T1.Product_Name ORDER BY COUNT(*) DESC	solvency_ii
SELECT T1.Product_Name FROM Products AS T1 JOIN Products_in_Events AS T2 ON T1.Product_ID = T2.Product_ID GROUP BY T1.Product_Name HAVING COUNT(*) >= 2	solvency_ii
SELECT T1.Product_Name FROM Products AS T1 JOIN Products_in_Events AS T2 ON T1.Product_ID = T2.Product_ID GROUP BY T1.Product_Name HAVING COUNT(*) >= 2 ORDER BY T1.Product_Name	solvency_ii
SELECT Product_Name FROM Products WHERE Product_ID NOT IN (SELECT Product_ID FROM Products_in_Events)	solvency_ii
SELECT count(*) FROM artwork	entertainment_awards
SELECT Name FROM artwork ORDER BY Name ASC	entertainment_awards
SELECT Name FROM artwork WHERE TYPE != "Program Talent Show"	entertainment_awards
SELECT Festival_Name , LOCATION FROM festival_detail	entertainment_awards
SELECT Chair_Name FROM festival_detail ORDER BY YEAR ASC	entertainment_awards
SELECT LOCATION FROM festival_detail ORDER BY Num_of_Audience DESC LIMIT 1	entertainment_awards
SELECT Festival_Name FROM festival_detail WHERE YEAR = 2007	entertainment_awards
SELECT avg(Num_of_Audience) FROM festival_detail	entertainment_awards
SELECT Festival_Name FROM festival_detail ORDER BY YEAR DESC LIMIT 3	entertainment_awards
SELECT T2.Name , T3.Festival_Name FROM nomination AS T1 JOIN artwork AS T2 ON T1.Artwork_ID = T2.Artwork_ID JOIN festival_detail AS T3 ON T1.Festival_ID = T3.Festival_ID	entertainment_awards
SELECT DISTINCT T2.Type FROM nomination AS T1 JOIN artwork AS T2 ON T1.Artwork_ID = T2.Artwork_ID JOIN festival_detail AS T3 ON T1.Festival_ID = T3.Festival_ID WHERE T3.Year = 2007	entertainment_awards
SELECT T2.Name FROM nomination AS T1 JOIN artwork AS T2 ON T1.Artwork_ID = T2.Artwork_ID JOIN festival_detail AS T3 ON T1.Festival_ID = T3.Festival_ID ORDER BY T3.Year	entertainment_awards
SELECT T3.Festival_Name FROM nomination AS T1 JOIN artwork AS T2 ON T1.Artwork_ID = T2.Artwork_ID JOIN festival_detail AS T3 ON T1.Festival_ID = T3.Festival_ID WHERE T2.Type = "Program Talent Show"	entertainment_awards
SELECT T1.Festival_ID , T3.Festival_Name FROM nomination AS T1 JOIN artwork AS T2 ON T1.Artwork_ID = T2.Artwork_ID JOIN festival_detail AS T3 ON T1.Festival_ID = T3.Festival_ID GROUP BY T1.Festival_ID HAVING COUNT(*) >= 2	entertainment_awards
SELECT T1.Festival_ID , T3.Festival_Name , COUNT(*) FROM nomination AS T1 JOIN artwork AS T2 ON T1.Artwork_ID = T2.Artwork_ID JOIN festival_detail AS T3 ON T1.Festival_ID = T3.Festival_ID GROUP BY T1.Festival_ID	entertainment_awards
SELECT TYPE , COUNT(*) FROM artwork GROUP BY TYPE	entertainment_awards
SELECT TYPE FROM artwork GROUP BY TYPE ORDER BY COUNT(*) DESC LIMIT 1	entertainment_awards
SELECT YEAR FROM festival_detail GROUP BY YEAR HAVING COUNT(*) > 1	entertainment_awards
SELECT Name FROM Artwork WHERE Artwork_ID NOT IN (SELECT Artwork_ID FROM nomination)	entertainment_awards
SELECT Num_of_Audience FROM festival_detail WHERE YEAR = 2008 OR YEAR = 2010	entertainment_awards
SELECT sum(Num_of_Audience) FROM festival_detail	entertainment_awards
SELECT YEAR FROM festival_detail WHERE LOCATION = 'United States' INTERSECT SELECT YEAR FROM festival_detail WHERE LOCATION != 'United States'	entertainment_awards
SELECT count(*) FROM premises	customers_campaigns_ecommerce
SELECT DISTINCT premises_type FROM premises	customers_campaigns_ecommerce
SELECT premises_type , premise_details FROM premises ORDER BY premises_type	customers_campaigns_ecommerce
SELECT premises_type , count(*) FROM premises GROUP BY premises_type	customers_campaigns_ecommerce
SELECT product_category , count(*) FROM mailshot_campaigns GROUP BY product_category	customers_campaigns_ecommerce
SELECT customer_name , customer_phone FROM customers WHERE customer_id NOT IN (SELECT customer_id FROM mailshot_customers)	customers_campaigns_ecommerce
SELECT T1.customer_name , T1.customer_phone FROM customers AS T1 JOIN mailshot_customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.outcome_code = 'No Response'	customers_campaigns_ecommerce
SELECT outcome_code , count(*) FROM mailshot_customers GROUP BY outcome_code	customers_campaigns_ecommerce
SELECT T2.customer_name FROM mailshot_customers AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id WHERE outcome_code = 'Order' GROUP BY T1.customer_id HAVING count(*) >= 2	customers_campaigns_ecommerce
SELECT T2.customer_name FROM mailshot_customers AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	customers_campaigns_ecommerce
SELECT T2.customer_name , T2.payment_method FROM mailshot_customers AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.outcome_code = 'Order' INTERSECT SELECT T2.customer_name , T2.payment_method FROM mailshot_customers AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.outcome_code = 'No Response'	customers_campaigns_ecommerce
SELECT T2.premises_type , T1.address_type_code FROM customer_addresses AS T1 JOIN premises AS T2 ON T1.premise_id = T2.premise_id	customers_campaigns_ecommerce
SELECT DISTINCT address_type_code FROM customer_addresses	customers_campaigns_ecommerce
SELECT order_shipping_charges , customer_id FROM customer_orders WHERE order_status_code = 'Cancelled' OR order_status_code = 'Paid'	customers_campaigns_ecommerce
SELECT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE shipping_method_code = 'FedEx' AND order_status_code = 'Paid'	customers_campaigns_ecommerce
SELECT count(*) FROM COURSE	college_3
SELECT count(*) FROM COURSE	college_3
SELECT count(*) FROM COURSE WHERE Credits > 2	college_3
SELECT count(*) FROM COURSE WHERE Credits > 2	college_3
SELECT CName FROM COURSE WHERE Credits = 1	college_3
SELECT CName FROM COURSE WHERE Credits = 1	college_3
SELECT CName FROM COURSE WHERE Days = "MTW"	college_3
SELECT CName FROM COURSE WHERE Days = "MTW"	college_3
SELECT count(*) FROM DEPARTMENT WHERE Division = "AS"	college_3
SELECT count(*) FROM DEPARTMENT WHERE Division = "AS"	college_3
SELECT DPhone FROM DEPARTMENT WHERE Room = 268	college_3
SELECT DPhone FROM DEPARTMENT WHERE Room = 268	college_3
SELECT COUNT(DISTINCT StuID) FROM ENROLLED_IN WHERE Grade = "B"	college_3
SELECT COUNT(DISTINCT StuID) FROM ENROLLED_IN WHERE Grade = "B"	college_3
SELECT max(gradepoint) , min(gradepoint) FROM GRADECONVERSION	college_3
SELECT max(gradepoint) , min(gradepoint) FROM GRADECONVERSION	college_3
SELECT DISTINCT Fname FROM STUDENT WHERE Fname LIKE '%a%'	college_3
SELECT DISTINCT Fname FROM STUDENT WHERE Fname LIKE '%a%'	college_3
SELECT Fname , Lname FROM FACULTY WHERE sex = "M" AND Building = "NEB"	college_3
SELECT Fname , Lname FROM FACULTY WHERE sex = "M" AND Building = "NEB"	college_3
SELECT Room FROM FACULTY WHERE Rank = "Professor" AND Building = "NEB"	college_3
SELECT Room FROM FACULTY WHERE Rank = "Professor" AND Building = "NEB"	college_3
SELECT DName FROM DEPARTMENT WHERE Building = "Mergenthaler"	college_3
SELECT DName FROM DEPARTMENT WHERE Building = "Mergenthaler"	college_3
SELECT * FROM COURSE ORDER BY Credits	college_3
SELECT * FROM COURSE ORDER BY Credits	college_3
SELECT CName FROM COURSE ORDER BY Credits	college_3
SELECT CName FROM COURSE ORDER BY Credits	college_3
SELECT Fname FROM STUDENT ORDER BY Age DESC	college_3
SELECT Fname FROM STUDENT ORDER BY Age DESC	college_3
SELECT LName FROM STUDENT WHERE Sex = "F" ORDER BY Age DESC	college_3
SELECT LName FROM STUDENT WHERE Sex = "F" ORDER BY Age DESC	college_3
SELECT Lname FROM FACULTY WHERE Building = "Barton" ORDER BY Lname	college_3
SELECT Lname FROM FACULTY WHERE Building = "Barton" ORDER BY Lname	college_3
SELECT Fname FROM FACULTY WHERE Rank = "Professor" ORDER BY Fname	college_3
SELECT Fname FROM FACULTY WHERE Rank = "Professor" ORDER BY Fname	college_3
SELECT T1.DName FROM DEPARTMENT AS T1 JOIN MINOR_IN AS T2 ON T1.DNO = T2.DNO GROUP BY T2.DNO ORDER BY count(*) DESC LIMIT 1	college_3
SELECT T1.DName FROM DEPARTMENT AS T1 JOIN MINOR_IN AS T2 ON T1.DNO = T2.DNO GROUP BY T2.DNO ORDER BY count(*) DESC LIMIT 1	college_3
SELECT DName FROM DEPARTMENT EXCEPT SELECT T1.DName FROM DEPARTMENT AS T1 JOIN MINOR_IN AS T2 ON T1.DNO = T2.DNO	college_3
SELECT DName FROM DEPARTMENT EXCEPT SELECT T1.DName FROM DEPARTMENT AS T1 JOIN MINOR_IN AS T2 ON T1.DNO = T2.DNO	college_3
SELECT T1.DName FROM DEPARTMENT AS T1 JOIN MEMBER_OF AS T2 ON T1.DNO = T2.DNO GROUP BY T2.DNO ORDER BY count(*) ASC LIMIT 1	college_3
SELECT T1.DName FROM DEPARTMENT AS T1 JOIN MEMBER_OF AS T2 ON T1.DNO = T2.DNO GROUP BY T2.DNO ORDER BY count(*) ASC LIMIT 1	college_3
SELECT Rank FROM FACULTY GROUP BY Rank ORDER BY count(*) ASC LIMIT 1	college_3
SELECT Rank FROM FACULTY GROUP BY Rank ORDER BY count(*) ASC LIMIT 1	college_3
SELECT T2.Fname , T2.Lname FROM COURSE AS T1 JOIN FACULTY AS T2 ON T1.Instructor = T2.FacID GROUP BY T1.Instructor ORDER BY count(*) DESC LIMIT 3	college_3
SELECT T2.Fname , T2.Lname FROM COURSE AS T1 JOIN FACULTY AS T2 ON T1.Instructor = T2.FacID GROUP BY T1.Instructor ORDER BY count(*) DESC LIMIT 3	college_3
SELECT T2.Building FROM COURSE AS T1 JOIN FACULTY AS T2 ON T1.Instructor = T2.FacID GROUP BY T1.Instructor ORDER BY count(*) DESC LIMIT 1	college_3
SELECT T2.Building FROM COURSE AS T1 JOIN FACULTY AS T2 ON T1.Instructor = T2.FacID GROUP BY T1.Instructor ORDER BY count(*) DESC LIMIT 1	college_3
SELECT T1.CName FROM COURSE AS T1 JOIN ENROLLED_IN AS T2 ON T1.CID = T2.CID GROUP BY T2.CID HAVING COUNT(*) >= 5	college_3
SELECT T1.CName FROM COURSE AS T1 JOIN ENROLLED_IN AS T2 ON T1.CID = T2.CID GROUP BY T2.CID HAVING COUNT(*) >= 5	college_3
SELECT T2.Fname , T2.Lname FROM COURSE AS T1 JOIN FACULTY AS T2 ON T1.Instructor = T2.FacID WHERE T1.CName = "COMPUTER LITERACY"	college_3
SELECT T2.Fname , T2.Lname FROM COURSE AS T1 JOIN FACULTY AS T2 ON T1.Instructor = T2.FacID WHERE T1.CName = "COMPUTER LITERACY"	college_3
SELECT T2.Dname , T2.Room FROM COURSE AS T1 JOIN DEPARTMENT AS T2 ON T1.DNO = T2.DNO WHERE T1.CName = "INTRODUCTION TO COMPUTER SCIENCE"	college_3
SELECT T2.Dname , T2.Room FROM COURSE AS T1 JOIN DEPARTMENT AS T2 ON T1.DNO = T2.DNO WHERE T1.CName = "INTRODUCTION TO COMPUTER SCIENCE"	college_3
SELECT T3.Fname , T3.LName , T2.gradepoint FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID	college_3
SELECT T3.Fname , T3.LName , T2.gradepoint FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID	college_3
SELECT DISTINCT T3.Fname FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID WHERE T2.gradepoint >= 3.8	college_3
SELECT DISTINCT T3.Fname FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID WHERE T2.gradepoint >= 3.8	college_3
SELECT T1.Fname , T1.Lname FROM FACULTY AS T1 JOIN MEMBER_OF AS T2 ON T1.FacID = T2.FacID WHERE T2.DNO = 520	college_3
SELECT T1.Fname , T1.Lname FROM FACULTY AS T1 JOIN MEMBER_OF AS T2 ON T1.FacID = T2.FacID WHERE T2.DNO = 520	college_3
SELECT T2.Fname , T2.Lname FROM MINOR_IN AS T1 JOIN STUDENT AS T2 ON T1.StuID = T2.StuID WHERE T1.DNO = 140	college_3
SELECT T2.Fname , T2.Lname FROM MINOR_IN AS T1 JOIN STUDENT AS T2 ON T1.StuID = T2.StuID WHERE T1.DNO = 140	college_3
SELECT T2.Lname FROM DEPARTMENT AS T1 JOIN FACULTY AS T2 ON T1.DNO = T3.DNO JOIN MEMBER_OF AS T3 ON T2.FacID = T3.FacID WHERE T1.DName = "Computer Science"	college_3
SELECT T2.Lname FROM DEPARTMENT AS T1 JOIN FACULTY AS T2 ON T1.DNO = T3.DNO JOIN MEMBER_OF AS T3 ON T2.FacID = T3.FacID WHERE T1.DName = "Computer Science"	college_3
SELECT avg(T2.gradepoint) FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID WHERE T3.LName = "Smith"	college_3
SELECT avg(T2.gradepoint) FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID WHERE T3.LName = "Smith"	college_3
SELECT max(T2.gradepoint) , min(T2.gradepoint) FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID WHERE T3.city_code = "NYC"	college_3
SELECT max(T2.gradepoint) , min(T2.gradepoint) FROM ENROLLED_IN AS T1 JOIN GRADECONVERSION AS T2 JOIN STUDENT AS T3 ON T1.Grade = T2.lettergrade AND T1.StuID = T3.StuID WHERE T3.city_code = "NYC"	college_3
SELECT CName FROM COURSE WHERE Credits = 3 UNION SELECT CName FROM COURSE WHERE Credits = 1 AND Hours = 4	college_3
SELECT CName FROM COURSE WHERE Credits = 3 UNION SELECT CName FROM COURSE WHERE Credits = 1 AND Hours = 4	college_3
SELECT DName FROM DEPARTMENT WHERE Division = "AS" UNION SELECT DName FROM DEPARTMENT WHERE Division = "EN" AND Building = "NEB"	college_3
SELECT DName FROM DEPARTMENT WHERE Division = "AS" UNION SELECT DName FROM DEPARTMENT WHERE Division = "EN" AND Building = "NEB"	college_3
SELECT Fname FROM STUDENT WHERE StuID NOT IN (SELECT StuID FROM ENROLLED_IN)	college_3
SELECT Fname FROM STUDENT WHERE StuID NOT IN (SELECT StuID FROM ENROLLED_IN)	college_3
SELECT product_id FROM product_suppliers ORDER BY total_amount_purchased DESC LIMIT 3	department_store
SELECT product_id FROM product_suppliers ORDER BY total_amount_purchased DESC LIMIT 3	department_store
SELECT product_id , product_type_code FROM products ORDER BY product_price LIMIT 1	department_store
SELECT product_id , product_type_code FROM products ORDER BY product_price LIMIT 1	department_store
SELECT count(DISTINCT product_type_code) FROM products	department_store
SELECT count(DISTINCT product_type_code) FROM products	department_store
SELECT T1.address_details FROM addresses AS T1 JOIN customer_addresses AS T2 ON T1.address_id = T2.address_id WHERE T2.customer_id = 10	department_store
SELECT T1.address_details FROM addresses AS T1 JOIN customer_addresses AS T2 ON T1.address_id = T2.address_id WHERE T2.customer_id = 10	department_store
SELECT T1.staff_id , T1.staff_gender FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.job_title_code = "Department Manager"	department_store
SELECT T1.staff_id , T1.staff_gender FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.job_title_code = "Department Manager"	department_store
SELECT payment_method_code , count(*) FROM customers GROUP BY payment_method_code	department_store
SELECT payment_method_code , count(*) FROM customers GROUP BY payment_method_code	department_store
SELECT product_id FROM order_items GROUP BY product_id ORDER BY count(*) DESC LIMIT 1	department_store
SELECT product_id FROM order_items GROUP BY product_id ORDER BY count(*) DESC LIMIT 1	department_store
SELECT T1.customer_name , T1.customer_phone , T1.customer_email FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T2.customer_id ORDER BY count(*) DESC LIMIT 1	department_store
SELECT T1.customer_name , T1.customer_phone , T1.customer_email FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T2.customer_id ORDER BY count(*) DESC LIMIT 1	department_store
SELECT product_type_code , avg(product_price) FROM products GROUP BY product_type_code	department_store
SELECT product_type_code , avg(product_price) FROM products GROUP BY product_type_code	department_store
SELECT count(*) FROM department_stores AS T1 JOIN department_store_chain AS T2 ON T1.dept_store_chain_id = T2.dept_store_chain_id WHERE T2.dept_store_chain_name = "South"	department_store
SELECT count(*) FROM department_stores AS T1 JOIN department_store_chain AS T2 ON T1.dept_store_chain_id = T2.dept_store_chain_id WHERE T2.dept_store_chain_name = "South"	department_store
SELECT T1.staff_name , T2.job_title_code FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id ORDER BY T2.date_assigned_to DESC LIMIT 1	department_store
SELECT T1.staff_name , T2.job_title_code FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id ORDER BY T2.date_assigned_to DESC LIMIT 1	department_store
SELECT T2.product_type_code , T2.product_name , T2.product_price FROM product_suppliers AS T1 JOIN products AS T2 ON T1.product_id = T2.product_id WHERE T1.supplier_id = 3	department_store
SELECT T2.product_type_code , T2.product_name , T2.product_price FROM product_suppliers AS T1 JOIN products AS T2 ON T1.product_id = T2.product_id WHERE T1.supplier_id = 3	department_store
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = "Pending" ORDER BY T2.customer_id	department_store
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = "Pending" ORDER BY T2.customer_id	department_store
SELECT T1.customer_name , T1.customer_address FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = "New" INTERSECT SELECT T1.customer_name , T1.customer_address FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = "Pending"	department_store
SELECT T1.customer_name , T1.customer_address FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = "New" INTERSECT SELECT T1.customer_name , T1.customer_address FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = "Pending"	department_store
SELECT T1.product_id FROM product_suppliers AS T1 JOIN products AS T2 ON T1.product_id = T2.product_id WHERE T1.supplier_id = 2 AND T2.product_price > (SELECT avg(product_price) FROM products)	department_store
SELECT T1.product_id FROM product_suppliers AS T1 JOIN products AS T2 ON T1.product_id = T2.product_id WHERE T1.supplier_id = 2 AND T2.product_price > (SELECT avg(product_price) FROM products)	department_store
SELECT T2.dept_store_id , T2.store_name FROM departments AS T1 JOIN department_stores AS T2 ON T1.dept_store_id = T2.dept_store_id WHERE T1.department_name = "marketing" INTERSECT SELECT T2.dept_store_id , T2.store_name FROM departments AS T1 JOIN department_stores AS T2 ON T1.dept_store_id = T2.dept_store_id WHERE T1.department_name = "managing"	department_store
SELECT T2.dept_store_id , T2.store_name FROM departments AS T1 JOIN department_stores AS T2 ON T1.dept_store_id = T2.dept_store_id WHERE T1.department_name = "marketing" INTERSECT SELECT T2.dept_store_id , T2.store_name FROM departments AS T1 JOIN department_stores AS T2 ON T1.dept_store_id = T2.dept_store_id WHERE T1.department_name = "managing"	department_store
SELECT dept_store_chain_id FROM department_stores GROUP BY dept_store_chain_id ORDER BY count(*) DESC LIMIT 2	department_store
SELECT dept_store_chain_id FROM department_stores GROUP BY dept_store_chain_id ORDER BY count(*) DESC LIMIT 2	department_store
SELECT department_id FROM staff_department_assignments GROUP BY department_id ORDER BY count(*) LIMIT 1	department_store
SELECT department_id FROM staff_department_assignments GROUP BY department_id ORDER BY count(*) LIMIT 1	department_store
SELECT product_type_code , max(product_price) , min(product_price) FROM products GROUP BY product_type_code	department_store
SELECT product_type_code , max(product_price) , min(product_price) FROM products GROUP BY product_type_code	department_store
SELECT product_type_code FROM products GROUP BY product_type_code HAVING avg(product_price) > (SELECT avg(product_price) FROM products)	department_store
SELECT product_type_code FROM products GROUP BY product_type_code HAVING avg(product_price) > (SELECT avg(product_price) FROM products)	department_store
SELECT T1.staff_id , T1.staff_name FROM staff AS T1 JOIN Staff_Department_Assignments AS T2 ON T1.staff_id = T2.staff_id ORDER BY date_assigned_to - date_assigned_from LIMIT 1	department_store
SELECT T1.staff_id , T1.staff_name FROM staff AS T1 JOIN Staff_Department_Assignments AS T2 ON T1.staff_id = T2.staff_id ORDER BY date_assigned_to - date_assigned_from LIMIT 1	department_store
SELECT product_name , product_id FROM products WHERE product_price BETWEEN 600 AND 700	department_store
SELECT product_name , product_id FROM products WHERE product_price BETWEEN 600 AND 700	department_store
SELECT DISTINCT customer_id FROM Customer_Orders WHERE order_date > (SELECT min(order_date) FROM Customer_Orders WHERE order_status_code = "Cancelled")	department_store
SELECT DISTINCT customer_id FROM Customer_Orders WHERE order_date > (SELECT min(order_date) FROM Customer_Orders WHERE order_status_code = "Cancelled")	department_store
SELECT staff_id FROM Staff_Department_Assignments WHERE date_assigned_to < (SELECT max(date_assigned_to) FROM Staff_Department_Assignments WHERE job_title_code = 'Clerical Staff')	department_store
SELECT staff_id FROM Staff_Department_Assignments WHERE date_assigned_to < (SELECT max(date_assigned_to) FROM Staff_Department_Assignments WHERE job_title_code = 'Clerical Staff')	department_store
SELECT customer_name , customer_id FROM customers WHERE customer_address LIKE "%TN%"	department_store
SELECT customer_name , customer_id FROM customers WHERE customer_address LIKE "%TN%"	department_store
SELECT T1.staff_name , T1.staff_gender FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.date_assigned_from LIKE "2016%"	department_store
SELECT T1.staff_name , T1.staff_gender FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.date_assigned_from LIKE "2016%"	department_store
SELECT T1.staff_name FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id GROUP BY T2.staff_id HAVING COUNT (*) > 1	department_store
SELECT T1.staff_name FROM staff AS T1 JOIN staff_department_assignments AS T2 ON T1.staff_id = T2.staff_id GROUP BY T2.staff_id HAVING COUNT (*) > 1	department_store
SELECT T1.supplier_name , T1.supplier_phone FROM Suppliers AS T1 JOIN supplier_addresses AS T2 ON T1.supplier_id = T2.supplier_id JOIN addresses AS T3 ON T2.address_id = T3.address_id ORDER BY T3.address_details	department_store
SELECT T1.supplier_name , T1.supplier_phone FROM Suppliers AS T1 JOIN supplier_addresses AS T2 ON T1.supplier_id = T2.supplier_id JOIN addresses AS T3 ON T2.address_id = T3.address_id ORDER BY T3.address_details	department_store
SELECT customer_phone FROM customers UNION SELECT supplier_phone FROM suppliers	department_store
SELECT customer_phone FROM customers UNION SELECT supplier_phone FROM suppliers	department_store
SELECT product_id FROM Order_Items GROUP BY product_id HAVING count(*) > 3 UNION SELECT product_id FROM Product_Suppliers GROUP BY product_id HAVING sum(total_amount_purchased) > 80000	department_store
SELECT product_id FROM Order_Items GROUP BY product_id HAVING count(*) > 3 UNION SELECT product_id FROM Product_Suppliers GROUP BY product_id HAVING sum(total_amount_purchased) > 80000	department_store
SELECT product_id , product_name FROM products WHERE product_price < 600 OR product_price > 900	department_store
SELECT product_id , product_name FROM products WHERE product_price < 600 OR product_price > 900	department_store
SELECT supplier_id FROM Product_Suppliers GROUP BY supplier_id HAVING avg(total_amount_purchased) > 50000 OR avg(total_amount_purchased) < 30000	department_store
SELECT supplier_id FROM Product_Suppliers GROUP BY supplier_id HAVING avg(total_amount_purchased) > 50000 OR avg(total_amount_purchased) < 30000	department_store
SELECT avg(total_amount_purchased) , avg(total_value_purchased) FROM Product_Suppliers WHERE supplier_id = (SELECT supplier_id FROM Product_Suppliers GROUP BY supplier_id ORDER BY count(*) DESC LIMIT 1)	department_store
SELECT avg(total_amount_purchased) , avg(total_value_purchased) FROM Product_Suppliers WHERE supplier_id = (SELECT supplier_id FROM Product_Suppliers GROUP BY supplier_id ORDER BY count(*) DESC LIMIT 1)	department_store
SELECT max(customer_code) , min(customer_code) FROM Customers	department_store
SELECT max(customer_code) , min(customer_code) FROM Customers	department_store
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id JOIN order_items AS T3 ON T2.order_id = T3.order_id JOIN products AS T4 ON T3.product_id = T4.product_id WHERE T4.product_name = "keyboard"	department_store
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id JOIN order_items AS T3 ON T2.order_id = T3.order_id JOIN products AS T4 ON T3.product_id = T4.product_id WHERE T4.product_name = "keyboard"	department_store
SELECT DISTINCT T1.supplier_name , T1.supplier_phone FROM suppliers AS T1 JOIN product_suppliers AS T2 ON T1.supplier_id = T2.supplier_id JOIN products AS T3 ON T2.product_id = T3.product_id WHERE T3.product_name = "red jeans"	department_store
SELECT DISTINCT T1.supplier_name , T1.supplier_phone FROM suppliers AS T1 JOIN product_suppliers AS T2 ON T1.supplier_id = T2.supplier_id JOIN products AS T3 ON T2.product_id = T3.product_id WHERE T3.product_name = "red jeans"	department_store
SELECT max(product_price) , min(product_price) , product_type_code FROM products GROUP BY product_type_code ORDER BY product_type_code	department_store
SELECT max(product_price) , min(product_price) , product_type_code FROM products GROUP BY product_type_code ORDER BY product_type_code	department_store
SELECT order_id , customer_id FROM customer_orders WHERE order_status_code = "Cancelled" ORDER BY order_date	department_store
SELECT order_id , customer_id FROM customer_orders WHERE order_status_code = "Cancelled" ORDER BY order_date	department_store
SELECT DISTINCT T3.product_name FROM customer_orders AS T1 JOIN order_items AS T2 ON T1.order_id = T2.order_id JOIN products AS T3 ON T2.product_id = T3.product_id GROUP BY T3.product_id HAVING COUNT (DISTINCT T1.customer_id) >= 2	department_store
SELECT DISTINCT T3.product_name FROM customer_orders AS T1 JOIN order_items AS T2 ON T1.order_id = T2.order_id JOIN products AS T3 ON T2.product_id = T3.product_id GROUP BY T3.product_id HAVING COUNT (DISTINCT T1.customer_id) >= 2	department_store
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id JOIN order_items AS T3 ON T2.order_id = T3.order_id GROUP BY T1.customer_id HAVING COUNT (DISTINCT T3.product_id) >= 3	department_store
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id JOIN order_items AS T3 ON T2.order_id = T3.order_id GROUP BY T1.customer_id HAVING COUNT (DISTINCT T3.product_id) >= 3	department_store
SELECT T1.staff_name , T1.staff_gender FROM staff AS T1 JOIN Staff_Department_Assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.job_title_code = "Sales Person" EXCEPT SELECT T1.staff_name , T1.staff_gender FROM staff AS T1 JOIN Staff_Department_Assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.job_title_code = "Clerical Staff"	department_store
SELECT T1.staff_name , T1.staff_gender FROM staff AS T1 JOIN Staff_Department_Assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.job_title_code = "Sales Person" EXCEPT SELECT T1.staff_name , T1.staff_gender FROM staff AS T1 JOIN Staff_Department_Assignments AS T2 ON T1.staff_id = T2.staff_id WHERE T2.job_title_code = "Clerical Staff"	department_store
SELECT customer_id , customer_name FROM customers WHERE customer_address LIKE "%WY%" AND payment_method_code != "Credit Card"	department_store
SELECT customer_id , customer_name FROM customers WHERE customer_address LIKE "%WY%" AND payment_method_code != "Credit Card"	department_store
SELECT avg(product_price) FROM products WHERE product_type_code = 'Clothes'	department_store
SELECT avg(product_price) FROM products WHERE product_type_code = 'Clothes'	department_store
SELECT product_name FROM products WHERE product_type_code = 'Hardware' ORDER BY product_price DESC LIMIT 1	department_store
SELECT product_name FROM products WHERE product_type_code = 'Hardware' ORDER BY product_price DESC LIMIT 1	department_store
SELECT count(*) FROM aircraft	aircraft
SELECT count(*) FROM aircraft	aircraft
SELECT Description FROM aircraft	aircraft
SELECT Description FROM aircraft	aircraft
SELECT avg(International_Passengers) FROM airport	aircraft
SELECT avg(International_Passengers) FROM airport	aircraft
SELECT International_Passengers , Domestic_Passengers FROM airport WHERE Airport_Name = "London Heathrow"	aircraft
SELECT International_Passengers , Domestic_Passengers FROM airport WHERE Airport_Name = "London Heathrow"	aircraft
SELECT sum(Domestic_Passengers) FROM airport WHERE Airport_Name LIKE "%London%"	aircraft
SELECT sum(Domestic_Passengers) FROM airport WHERE Airport_Name LIKE "%London%"	aircraft
SELECT max(Transit_Passengers) , min(Transit_Passengers) FROM airport	aircraft
SELECT max(Transit_Passengers) , min(Transit_Passengers) FROM airport	aircraft
SELECT Name FROM pilot WHERE Age >= 25	aircraft
SELECT Name FROM pilot WHERE Age >= 25	aircraft
SELECT Name FROM pilot ORDER BY Name ASC	aircraft
SELECT Name FROM pilot ORDER BY Name ASC	aircraft
SELECT Name FROM pilot WHERE Age <= 30 ORDER BY Name DESC	aircraft
SELECT Name FROM pilot WHERE Age <= 30 ORDER BY Name DESC	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Airport_Name = "London Gatwick"	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Airport_Name = "London Gatwick"	aircraft
SELECT T1.Aircraft , T1.Description FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Total_Passengers > 10000000	aircraft
SELECT T1.Aircraft , T1.Description FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Total_Passengers > 10000000	aircraft
SELECT avg(T3.Total_Passengers) FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T1.Aircraft = "Robinson R-22"	aircraft
SELECT avg(T3.Total_Passengers) FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T1.Aircraft = "Robinson R-22"	aircraft
SELECT T2.Location , T1.Aircraft FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft	aircraft
SELECT T2.Location , T1.Aircraft FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft GROUP BY T2.Winning_Aircraft ORDER BY COUNT(*) DESC LIMIT 1	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft GROUP BY T2.Winning_Aircraft ORDER BY COUNT(*) DESC LIMIT 1	aircraft
SELECT T1.Aircraft , COUNT(*) FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft GROUP BY T2.Winning_Aircraft	aircraft
SELECT T1.Aircraft , COUNT(*) FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft GROUP BY T2.Winning_Aircraft	aircraft
SELECT Name FROM pilot ORDER BY Age DESC	aircraft
SELECT Name FROM pilot ORDER BY Age DESC	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft GROUP BY T2.Winning_Aircraft HAVING COUNT(*) >= 2	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN MATCH AS T2 ON T1.Aircraft_ID = T2.Winning_Aircraft GROUP BY T2.Winning_Aircraft HAVING COUNT(*) >= 2	aircraft
SELECT Aircraft FROM aircraft WHERE Aircraft_ID NOT IN (SELECT Winning_Aircraft FROM MATCH)	aircraft
SELECT Aircraft FROM aircraft WHERE Aircraft_ID NOT IN (SELECT Winning_Aircraft FROM MATCH)	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Airport_Name = "London Heathrow" INTERSECT SELECT T1.Aircraft FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Airport_Name = "London Gatwick"	aircraft
SELECT T1.Aircraft FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Airport_Name = "London Heathrow" INTERSECT SELECT T1.Aircraft FROM aircraft AS T1 JOIN airport_aircraft AS T2 ON T1.Aircraft_ID = T2.Aircraft_ID JOIN airport AS T3 ON T2.Airport_ID = T3.Airport_ID WHERE T3.Airport_Name = "London Gatwick"	aircraft
SELECT * FROM airport ORDER BY International_Passengers DESC LIMIT 1	aircraft
SELECT * FROM airport ORDER BY International_Passengers DESC LIMIT 1	aircraft
SELECT t1.name , t1.age FROM pilot AS t1 JOIN MATCH AS t2 ON t1.pilot_id = t2.winning_pilot WHERE t1.age < 30 GROUP BY t2.winning_pilot ORDER BY count(*) DESC LIMIT 1	aircraft
SELECT t1.name , t1.age FROM pilot AS t1 JOIN MATCH AS t2 ON t1.pilot_id = t2.winning_pilot WHERE t1.age < 30 GROUP BY t2.winning_pilot ORDER BY count(*) DESC LIMIT 1	aircraft
SELECT t1.name , t1.age FROM pilot AS t1 JOIN MATCH AS t2 ON t1.pilot_id = t2.winning_pilot ORDER BY t1.age LIMIT 1	aircraft
SELECT t1.name , t1.age FROM pilot AS t1 JOIN MATCH AS t2 ON t1.pilot_id = t2.winning_pilot ORDER BY t1.age LIMIT 1	aircraft
SELECT name FROM pilot WHERE pilot_id NOT IN (SELECT Winning_Pilot FROM MATCH WHERE country = 'Australia')	aircraft
SELECT name FROM pilot WHERE pilot_id NOT IN (SELECT Winning_Pilot FROM MATCH WHERE country = 'Australia')	aircraft
SELECT T1.property_id , count(*) FROM properties AS T1 JOIN residents AS T2 ON T1.property_id = T2.property_id GROUP BY T1.property_id	local_govt_and_lot
SELECT DISTINCT T1.service_type_code FROM services AS T1 JOIN organizations AS T2 ON T1.organization_id = T2.organization_id WHERE T2.organization_details = 'Denesik and Sons Party'	local_govt_and_lot
SELECT T1.resident_id , T1.other_details , count(*) FROM Residents AS T1 JOIN Residents_Services AS T2 ON T1.resident_id = T2.resident_id GROUP BY T1.resident_id ORDER BY count(*) DESC	local_govt_and_lot
SELECT T1.service_id , T1.service_details , count(*) FROM Services AS T1 JOIN Residents_Services AS T2 ON T1.service_id = T2.service_id GROUP BY T1.service_id ORDER BY count(*) DESC LIMIT 1	local_govt_and_lot
SELECT T1.thing_id , T1.type_of_Thing_Code , T2.organization_details FROM Things AS T1 JOIN Organizations AS T2 ON T1.organization_id = T2.organization_id	local_govt_and_lot
SELECT T1.customer_id , T1.customer_details FROM Customers AS T1 JOIN Customer_Events AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 3	local_govt_and_lot
SELECT T2.date_moved_in , T1.customer_id , T1.customer_details FROM Customers AS T1 JOIN Customer_Events AS T2 ON T1.customer_id = T2.customer_id	local_govt_and_lot
SELECT T1.Customer_Event_ID , T1.property_id FROM Customer_Events AS T1 JOIN Customer_Event_Notes AS T2 ON T1.Customer_Event_ID = T2.Customer_Event_ID GROUP BY T1.customer_event_id HAVING count(*) BETWEEN 1 AND 3	local_govt_and_lot
SELECT DISTINCT T2.thing_id , T2.Type_of_Thing_Code FROM Timed_Status_of_Things AS T1 JOIN Things AS T2 ON T1.thing_id = T2.thing_id WHERE T1.Status_of_Thing_Code = 'Close' OR T1.Date_and_Date < '2017-06-19 02:59:21'	local_govt_and_lot
SELECT count(DISTINCT T2.Location_Code) FROM Things AS T1 JOIN Timed_Locations_of_Things AS T2 ON T1.thing_id = T2.thing_id WHERE T1.service_details = 'Unsatisfied'	local_govt_and_lot
SELECT count(DISTINCT Status_of_Thing_Code) FROM Timed_Status_of_Things	local_govt_and_lot
SELECT organization_id FROM organizations EXCEPT SELECT parent_organization_id FROM organizations	local_govt_and_lot
SELECT max(date_moved_in) FROM Residents	local_govt_and_lot
SELECT other_details FROM Residents WHERE other_details LIKE '%Miss%'	local_govt_and_lot
SELECT customer_event_id , date_moved_in , property_id FROM customer_events	local_govt_and_lot
SELECT count(*) FROM customers WHERE customer_id NOT IN ( SELECT customer_id FROM customer_events )	local_govt_and_lot
SELECT DISTINCT date_moved_in FROM residents	local_govt_and_lot
SELECT DISTINCT District_name FROM district ORDER BY city_area DESC	store_product
SELECT DISTINCT District_name FROM district ORDER BY city_area DESC	store_product
SELECT max_page_size FROM product GROUP BY max_page_size HAVING count(*) > 3	store_product
SELECT max_page_size FROM product GROUP BY max_page_size HAVING count(*) > 3	store_product
SELECT District_name , City_Population FROM district WHERE City_Population BETWEEN 200000 AND 2000000	store_product
SELECT District_name , City_Population FROM district WHERE City_Population BETWEEN 200000 AND 2000000	store_product
SELECT district_name FROM district WHERE city_area > 10 OR City_Population > 100000	store_product
SELECT district_name FROM district WHERE city_area > 10 OR City_Population > 100000	store_product
SELECT district_name FROM district ORDER BY city_population DESC LIMIT 1	store_product
SELECT district_name FROM district ORDER BY city_population DESC LIMIT 1	store_product
SELECT district_name FROM district ORDER BY city_area ASC LIMIT 1	store_product
SELECT district_name FROM district ORDER BY city_area ASC LIMIT 1	store_product
SELECT sum(city_population) FROM district ORDER BY city_area DESC LIMIT 3	store_product
SELECT sum(city_population) FROM district ORDER BY city_area DESC LIMIT 3	store_product
SELECT TYPE , count(*) FROM store GROUP BY TYPE	store_product
SELECT TYPE , count(*) FROM store GROUP BY TYPE	store_product
SELECT t1.store_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t3.district_name = "Khanewal District"	store_product
SELECT t1.store_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t3.district_name = "Khanewal District"	store_product
SELECT t1.store_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id WHERE district_id = (SELECT district_id FROM district ORDER BY city_population DESC LIMIT 1)	store_product
SELECT t1.store_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id WHERE district_id = (SELECT district_id FROM district ORDER BY city_population DESC LIMIT 1)	store_product
SELECT t3.headquartered_city FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t1.store_name = "Blackville"	store_product
SELECT t3.headquartered_city FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t1.store_name = "Blackville"	store_product
SELECT t3.headquartered_city , count(*) FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id GROUP BY t3.headquartered_city	store_product
SELECT t3.headquartered_city , count(*) FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id GROUP BY t3.headquartered_city	store_product
SELECT t3.headquartered_city FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id GROUP BY t3.headquartered_city ORDER BY count(*) DESC LIMIT 1	store_product
SELECT t3.headquartered_city FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id GROUP BY t3.headquartered_city ORDER BY count(*) DESC LIMIT 1	store_product
SELECT avg(pages_per_minute_color) FROM product	store_product
SELECT avg(pages_per_minute_color) FROM product	store_product
SELECT t1.product FROM product AS t1 JOIN store_product AS t2 ON t1.product_id = t2.product_id JOIN store AS t3 ON t2.store_id = t3.store_id WHERE t3.store_name = "Miramichi"	store_product
SELECT t1.product FROM product AS t1 JOIN store_product AS t2 ON t1.product_id = t2.product_id JOIN store AS t3 ON t2.store_id = t3.store_id WHERE t3.store_name = "Miramichi"	store_product
SELECT product FROM product WHERE max_page_size = "A4" AND pages_per_minute_color < 5	store_product
SELECT product FROM product WHERE max_page_size = "A4" AND pages_per_minute_color < 5	store_product
SELECT product FROM product WHERE max_page_size = "A4" OR pages_per_minute_color < 5	store_product
SELECT product FROM product WHERE max_page_size = "A4" OR pages_per_minute_color < 5	store_product
SELECT product FROM product WHERE product LIKE "%Scanner%"	store_product
SELECT product FROM product WHERE product LIKE "%Scanner%"	store_product
SELECT max_page_size FROM product GROUP BY max_page_size ORDER BY count(*) DESC LIMIT 1	store_product
SELECT max_page_size FROM product GROUP BY max_page_size ORDER BY count(*) DESC LIMIT 1	store_product
SELECT product FROM product WHERE product != (SELECT max_page_size FROM product GROUP BY max_page_size ORDER BY count(*) DESC LIMIT 1)	store_product
SELECT product FROM product WHERE product != (SELECT max_page_size FROM product GROUP BY max_page_size ORDER BY count(*) DESC LIMIT 1)	store_product
SELECT sum(city_population) FROM district WHERE city_area > (SELECT avg(city_area) FROM district)	store_product
SELECT sum(city_population) FROM district WHERE city_area > (SELECT avg(city_area) FROM district)	store_product
SELECT t3.District_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t1.Type = "City Mall" INTERSECT SELECT t3.District_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t1.Type = "Village Store"	store_product
SELECT t3.District_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t1.Type = "City Mall" INTERSECT SELECT t3.District_name FROM store AS t1 JOIN store_district AS t2 ON t1.store_id = t2.store_id JOIN district AS t3 ON t2.district_id = t3.district_id WHERE t1.Type = "Village Store"	store_product
SELECT sum(enr) FROM College	soccer_2
SELECT sum(enr) FROM College	soccer_2
SELECT avg(enr) FROM College	soccer_2
SELECT avg(enr) FROM College	soccer_2
SELECT count(*) FROM College	soccer_2
SELECT count(*) FROM College	soccer_2
SELECT count(*) FROM Player WHERE HS > 1000	soccer_2
SELECT count(*) FROM Player WHERE HS > 1000	soccer_2
SELECT count(*) FROM College WHERE enr > 15000	soccer_2
SELECT count(*) FROM College WHERE enr > 15000	soccer_2
SELECT avg(HS) FROM Player	soccer_2
SELECT avg(HS) FROM Player	soccer_2
SELECT pName , HS FROM Player WHERE HS < 1500	soccer_2
SELECT pName , HS FROM Player WHERE HS < 1500	soccer_2
SELECT count(DISTINCT cName) FROM tryout	soccer_2
SELECT count(DISTINCT cName) FROM tryout	soccer_2
SELECT count(DISTINCT pPos) FROM tryout	soccer_2
SELECT count(DISTINCT pPos) FROM tryout	soccer_2
SELECT count(*) FROM tryout WHERE decision = 'yes'	soccer_2
SELECT count(*) FROM tryout WHERE decision = 'yes'	soccer_2
SELECT count(*) FROM tryout WHERE pPos = 'goalie'	soccer_2
SELECT count(*) FROM tryout WHERE pPos = 'goalie'	soccer_2
SELECT avg(HS) , max(HS) , min(HS) FROM Player	soccer_2
SELECT avg(HS) , max(HS) , min(HS) FROM Player	soccer_2
SELECT avg(enr) FROM College WHERE state = 'FL'	soccer_2
SELECT avg(enr) FROM College WHERE state = 'FL'	soccer_2
SELECT pName FROM Player WHERE HS BETWEEN 500 AND 1500	soccer_2
SELECT pName FROM Player WHERE HS BETWEEN 500 AND 1500	soccer_2
SELECT DISTINCT pName FROM Player WHERE pName LIKE '%a%'	soccer_2
SELECT DISTINCT pName FROM Player WHERE pName LIKE '%a%'	soccer_2
SELECT cName , enr FROM College WHERE enr > 10000 AND state = "LA"	soccer_2
SELECT cName , enr FROM College WHERE enr > 10000 AND state = "LA"	soccer_2
SELECT * FROM College ORDER BY enr	soccer_2
SELECT * FROM College ORDER BY enr	soccer_2
SELECT cName FROM College WHERE enr > 18000 ORDER BY cName	soccer_2
SELECT cName FROM College WHERE enr > 18000 ORDER BY cName	soccer_2
SELECT pName FROM Player WHERE yCard = 'yes' ORDER BY HS DESC	soccer_2
SELECT pName FROM Player WHERE yCard = 'yes' ORDER BY HS DESC	soccer_2
SELECT DISTINCT cName FROM tryout ORDER BY cName	soccer_2
SELECT DISTINCT cName FROM tryout ORDER BY cName	soccer_2
SELECT pPos FROM tryout GROUP BY pPos ORDER BY count(*) DESC LIMIT 1	soccer_2
SELECT pPos FROM tryout GROUP BY pPos ORDER BY count(*) DESC LIMIT 1	soccer_2
SELECT count(*) , cName FROM tryout GROUP BY cName ORDER BY count(*) DESC	soccer_2
SELECT count(*) , cName FROM tryout GROUP BY cName ORDER BY count(*) DESC	soccer_2
SELECT min(T2.HS) , T1.pPos FROM tryout AS T1 JOIN player AS T2 ON T1.pID = T2.pID GROUP BY T1.pPos	soccer_2
SELECT min(T2.HS) , T1.pPos FROM tryout AS T1 JOIN player AS T2 ON T1.pID = T2.pID GROUP BY T1.pPos	soccer_2
SELECT cName FROM college ORDER BY enr DESC LIMIT 3	soccer_2
SELECT cName FROM college ORDER BY enr DESC LIMIT 3	soccer_2
SELECT cName , state , min(enr) FROM college GROUP BY state	soccer_2
SELECT cName , state , min(enr) FROM college GROUP BY state	soccer_2
SELECT DISTINCT state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName	soccer_2
SELECT DISTINCT state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName	soccer_2
SELECT DISTINCT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.decision = 'yes'	soccer_2
SELECT DISTINCT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.decision = 'yes'	soccer_2
SELECT T1.pName , T2.cName FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes'	soccer_2
SELECT T1.pName , T2.cName FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes'	soccer_2
SELECT T1.pName FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID ORDER BY T1.pName	soccer_2
SELECT T1.pName FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID ORDER BY T1.pName	soccer_2
SELECT T1.pName , T1.HS FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes'	soccer_2
SELECT T1.pName , T1.HS FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'striker'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'striker'	soccer_2
SELECT T1.pName FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes' AND T2.pPos = 'striker'	soccer_2
SELECT T1.pName FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes' AND T2.pPos = 'striker'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName JOIN player AS T3 ON T2.pID = T3.pID WHERE T3.pName = 'Charles'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName JOIN player AS T3 ON T2.pID = T3.pID WHERE T3.pName = 'Charles'	soccer_2
SELECT avg(T1.HS) , max(T1.HS) FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes'	soccer_2
SELECT avg(T1.HS) , max(T1.HS) FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'yes'	soccer_2
SELECT avg(T1.HS) FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'no'	soccer_2
SELECT avg(T1.HS) FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T2.decision = 'no'	soccer_2
SELECT max(T1.HS) , pPos FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T1.HS > 1000 GROUP BY T2.pPos	soccer_2
SELECT max(T1.HS) , pPos FROM player AS T1 JOIN tryout AS T2 ON T1.pID = T2.pID WHERE T1.HS > 1000 GROUP BY T2.pPos	soccer_2
SELECT T1.cName FROM tryout AS T1 JOIN player AS T2 ON T1.pID = T2.pID WHERE T2.pName LIKE 'D%'	soccer_2
SELECT T1.cName FROM tryout AS T1 JOIN player AS T2 ON T1.pID = T2.pID WHERE T2.pName LIKE 'D%'	soccer_2
SELECT cName FROM tryout WHERE decision = 'yes' AND pPos = 'goalie'	soccer_2
SELECT cName FROM tryout WHERE decision = 'yes' AND pPos = 'goalie'	soccer_2
SELECT T2.pName FROM tryout AS T1 JOIN player AS T2 ON T1.pID = T2.pID WHERE T1.cName = (SELECT cName FROM college ORDER BY enr DESC LIMIT 1)	soccer_2
SELECT T2.pName FROM tryout AS T1 JOIN player AS T2 ON T1.pID = T2.pID WHERE T1.cName = (SELECT cName FROM college ORDER BY enr DESC LIMIT 1)	soccer_2
SELECT DISTINCT T1.state , T1.enr FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.decision = 'yes'	soccer_2
SELECT DISTINCT T1.state , T1.enr FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.decision = 'yes'	soccer_2
SELECT cName FROM College WHERE enr < 13000 AND state = "AZ" UNION SELECT cName FROM College WHERE enr > 15000 AND state = "LA"	soccer_2
SELECT cName FROM College WHERE enr < 13000 AND state = "AZ" UNION SELECT cName FROM College WHERE enr > 15000 AND state = "LA"	soccer_2
SELECT cName FROM tryout WHERE pPos = 'goalie' INTERSECT SELECT cName FROM tryout WHERE pPos = 'mid'	soccer_2
SELECT cName FROM tryout WHERE pPos = 'goalie' INTERSECT SELECT cName FROM tryout WHERE pPos = 'mid'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'goalie' INTERSECT SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'mid'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'goalie' INTERSECT SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'mid'	soccer_2
SELECT COUNT(*) FROM (SELECT cName FROM tryout WHERE pPos = 'goalie' INTERSECT SELECT cName FROM tryout WHERE pPos = 'mid')	soccer_2
SELECT COUNT(*) FROM (SELECT cName FROM tryout WHERE pPos = 'goalie' INTERSECT SELECT cName FROM tryout WHERE pPos = 'mid')	soccer_2
SELECT cName FROM tryout WHERE pPos = 'mid' EXCEPT SELECT cName FROM tryout WHERE pPos = 'goalie'	soccer_2
SELECT cName FROM tryout WHERE pPos = 'mid' EXCEPT SELECT cName FROM tryout WHERE pPos = 'goalie'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'mid' EXCEPT SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'goalie'	soccer_2
SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'mid' EXCEPT SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'goalie'	soccer_2
SELECT COUNT(*) FROM (SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'mid' EXCEPT SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'goalie')	soccer_2
SELECT COUNT(*) FROM (SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'mid' EXCEPT SELECT T1.state FROM college AS T1 JOIN tryout AS T2 ON T1.cName = T2.cName WHERE T2.pPos = 'goalie')	soccer_2
SELECT DISTINCT state FROM college WHERE enr < (SELECT max(enr) FROM college)	soccer_2
SELECT DISTINCT state FROM college WHERE enr < (SELECT max(enr) FROM college)	soccer_2
SELECT DISTINCT cName FROM college WHERE enr > (SELECT min(enr) FROM college WHERE state = 'FL')	soccer_2
SELECT DISTINCT cName FROM college WHERE enr > (SELECT min(enr) FROM college WHERE state = 'FL')	soccer_2
SELECT cName FROM college WHERE enr > (SELECT max(enr) FROM college WHERE state = 'FL')	soccer_2
SELECT cName FROM college WHERE enr > (SELECT max(enr) FROM college WHERE state = 'FL')	soccer_2
SELECT sum(enr) FROM college WHERE cName NOT IN (SELECT cName FROM tryout WHERE pPos = "goalie")	soccer_2
SELECT sum(enr) FROM college WHERE cName NOT IN (SELECT cName FROM tryout WHERE pPos = "goalie")	soccer_2
SELECT count(DISTINCT state) FROM college WHERE enr > (SELECT avg(enr) FROM college)	soccer_2
SELECT count(DISTINCT state) FROM college WHERE enr > (SELECT avg(enr) FROM college)	soccer_2
SELECT count(DISTINCT state) FROM college WHERE enr < (SELECT avg(enr) FROM college)	soccer_2
SELECT count(DISTINCT state) FROM college WHERE enr < (SELECT avg(enr) FROM college)	soccer_2
SELECT count(*) FROM device	device
SELECT count(*) FROM device	device
SELECT Carrier FROM device ORDER BY Carrier ASC	device
SELECT Carrier FROM device ORDER BY Carrier ASC	device
SELECT Carrier FROM device WHERE Software_Platform != 'Android'	device
SELECT Carrier FROM device WHERE Software_Platform != 'Android'	device
SELECT Shop_Name FROM shop ORDER BY Open_Year ASC	device
SELECT Shop_Name FROM shop ORDER BY Open_Year ASC	device
SELECT avg(Quantity) FROM stock	device
SELECT avg(Quantity) FROM stock	device
SELECT Shop_Name , LOCATION FROM shop ORDER BY Shop_Name ASC	device
SELECT Shop_Name , LOCATION FROM shop ORDER BY Shop_Name ASC	device
SELECT count(DISTINCT Software_Platform) FROM device	device
SELECT count(DISTINCT Software_Platform) FROM device	device
SELECT Open_Date , Open_Year FROM shop WHERE Shop_Name = "Apple"	device
SELECT Open_Date , Open_Year FROM shop WHERE Shop_Name = "Apple"	device
SELECT Shop_Name FROM shop ORDER BY Open_Year DESC LIMIT 1	device
SELECT Shop_Name FROM shop ORDER BY Open_Year DESC LIMIT 1	device
SELECT T3.Shop_Name , T2.Carrier FROM stock AS T1 JOIN device AS T2 ON T1.Device_ID = T2.Device_ID JOIN shop AS T3 ON T1.Shop_ID = T3.Shop_ID	device
SELECT T3.Shop_Name , T2.Carrier FROM stock AS T1 JOIN device AS T2 ON T1.Device_ID = T2.Device_ID JOIN shop AS T3 ON T1.Shop_ID = T3.Shop_ID	device
SELECT T2.Shop_Name FROM stock AS T1 JOIN shop AS T2 ON T1.Shop_ID = T2.Shop_ID GROUP BY T1.Shop_ID HAVING COUNT(*) > 1	device
SELECT T2.Shop_Name FROM stock AS T1 JOIN shop AS T2 ON T1.Shop_ID = T2.Shop_ID GROUP BY T1.Shop_ID HAVING COUNT(*) > 1	device
SELECT T2.Shop_Name FROM stock AS T1 JOIN shop AS T2 ON T1.Shop_ID = T2.Shop_ID GROUP BY T1.Shop_ID ORDER BY COUNT(*) DESC LIMIT 1	device
SELECT T2.Shop_Name FROM stock AS T1 JOIN shop AS T2 ON T1.Shop_ID = T2.Shop_ID GROUP BY T1.Shop_ID ORDER BY COUNT(*) DESC LIMIT 1	device
SELECT T2.Shop_Name FROM stock AS T1 JOIN shop AS T2 ON T1.Shop_ID = T2.Shop_ID GROUP BY T1.Shop_ID ORDER BY SUM(T1.quantity) DESC LIMIT 1	device
SELECT T2.Shop_Name FROM stock AS T1 JOIN shop AS T2 ON T1.Shop_ID = T2.Shop_ID GROUP BY T1.Shop_ID ORDER BY SUM(T1.quantity) DESC LIMIT 1	device
SELECT Software_Platform , COUNT(*) FROM device GROUP BY Software_Platform	device
SELECT Software_Platform , COUNT(*) FROM device GROUP BY Software_Platform	device
SELECT Software_Platform FROM device GROUP BY Software_Platform ORDER BY COUNT(*) DESC	device
SELECT Software_Platform FROM device GROUP BY Software_Platform ORDER BY COUNT(*) DESC	device
SELECT Software_Platform FROM device GROUP BY Software_Platform ORDER BY COUNT(*) DESC LIMIT 1	device
SELECT Software_Platform FROM device GROUP BY Software_Platform ORDER BY COUNT(*) DESC LIMIT 1	device
SELECT Shop_Name FROM shop WHERE Shop_ID NOT IN (SELECT Shop_ID FROM stock)	device
SELECT Shop_Name FROM shop WHERE Shop_ID NOT IN (SELECT Shop_ID FROM stock)	device
SELECT LOCATION FROM shop WHERE Open_Year > 2012 INTERSECT SELECT LOCATION FROM shop WHERE Open_Year < 2008	device
SELECT LOCATION FROM shop WHERE Open_Year > 2012 INTERSECT SELECT LOCATION FROM shop WHERE Open_Year < 2008	device
SELECT Carrier FROM device WHERE Device_ID NOT IN (SELECT Device_ID FROM stock)	device
SELECT Carrier FROM device WHERE Device_ID NOT IN (SELECT Device_ID FROM stock)	device
SELECT T2.Carrier FROM stock AS T1 JOIN device AS T2 ON T1.Device_ID = T2.Device_ID GROUP BY T1.Device_ID HAVING COUNT(*) > 1	device
SELECT T2.Carrier FROM stock AS T1 JOIN device AS T2 ON T1.Device_ID = T2.Device_ID GROUP BY T1.Device_ID HAVING COUNT(*) > 1	device
SELECT count(*) FROM BOOKINGS	cre_Drama_Workshop_Groups
SELECT count(*) FROM BOOKINGS	cre_Drama_Workshop_Groups
SELECT Order_Date FROM BOOKINGS	cre_Drama_Workshop_Groups
SELECT Order_Date FROM BOOKINGS	cre_Drama_Workshop_Groups
SELECT Planned_Delivery_Date , Actual_Delivery_Date FROM BOOKINGS	cre_Drama_Workshop_Groups
SELECT Planned_Delivery_Date , Actual_Delivery_Date FROM BOOKINGS	cre_Drama_Workshop_Groups
SELECT count(*) FROM CUSTOMERS	cre_Drama_Workshop_Groups
SELECT count(*) FROM CUSTOMERS	cre_Drama_Workshop_Groups
SELECT Customer_Phone , Customer_Email_Address FROM CUSTOMERS WHERE Customer_Name = "Harold"	cre_Drama_Workshop_Groups
SELECT Customer_Phone , Customer_Email_Address FROM CUSTOMERS WHERE Customer_Name = "Harold"	cre_Drama_Workshop_Groups
SELECT Store_Name FROM Drama_Workshop_Groups	cre_Drama_Workshop_Groups
SELECT Store_Name FROM Drama_Workshop_Groups	cre_Drama_Workshop_Groups
SELECT min(Order_Quantity) , avg(Order_Quantity) , max(Order_Quantity) FROM INVOICES	cre_Drama_Workshop_Groups
SELECT min(Order_Quantity) , avg(Order_Quantity) , max(Order_Quantity) FROM INVOICES	cre_Drama_Workshop_Groups
SELECT DISTINCT payment_method_code FROM INVOICES	cre_Drama_Workshop_Groups
SELECT DISTINCT payment_method_code FROM INVOICES	cre_Drama_Workshop_Groups
SELECT Marketing_Region_Descriptrion FROM Marketing_Regions WHERE Marketing_Region_Name = "China"	cre_Drama_Workshop_Groups
SELECT Marketing_Region_Descriptrion FROM Marketing_Regions WHERE Marketing_Region_Name = "China"	cre_Drama_Workshop_Groups
SELECT DISTINCT Product_Name FROM PRODUCTS WHERE Product_Price > (SELECT avg(Product_Price) FROM PRODUCTS)	cre_Drama_Workshop_Groups
SELECT DISTINCT Product_Name FROM PRODUCTS WHERE Product_Price > (SELECT avg(Product_Price) FROM PRODUCTS)	cre_Drama_Workshop_Groups
SELECT Product_Name FROM PRODUCTS ORDER BY Product_Price DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT Product_Name FROM PRODUCTS ORDER BY Product_Price DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT Product_Name FROM Products ORDER BY Product_Price ASC	cre_Drama_Workshop_Groups
SELECT Product_Name FROM Products ORDER BY Product_Price ASC	cre_Drama_Workshop_Groups
SELECT Customer_Phone FROM PERFORMERS WHERE Customer_Name = "Ashley"	cre_Drama_Workshop_Groups
SELECT Customer_Phone FROM PERFORMERS WHERE Customer_Name = "Ashley"	cre_Drama_Workshop_Groups
SELECT payment_method_code , count(*) FROM INVOICES GROUP BY payment_method_code	cre_Drama_Workshop_Groups
SELECT payment_method_code , count(*) FROM INVOICES GROUP BY payment_method_code	cre_Drama_Workshop_Groups
SELECT payment_method_code FROM INVOICES GROUP BY payment_method_code ORDER BY count(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT payment_method_code FROM INVOICES GROUP BY payment_method_code ORDER BY count(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT T1.City_Town FROM Addresses AS T1 JOIN Stores AS T2 ON T1.Address_ID = T2.Address_ID WHERE T2.Store_Name = "FJA Filming"	cre_Drama_Workshop_Groups
SELECT T1.City_Town FROM Addresses AS T1 JOIN Stores AS T2 ON T1.Address_ID = T2.Address_ID WHERE T2.Store_Name = "FJA Filming"	cre_Drama_Workshop_Groups
SELECT T1.State_County FROM Addresses AS T1 JOIN Stores AS T2 ON T1.Address_ID = T2.Address_ID WHERE T2.Marketing_Region_Code = "CA"	cre_Drama_Workshop_Groups
SELECT T1.State_County FROM Addresses AS T1 JOIN Stores AS T2 ON T1.Address_ID = T2.Address_ID WHERE T2.Marketing_Region_Code = "CA"	cre_Drama_Workshop_Groups
SELECT T1.Marketing_Region_Name FROM Marketing_Regions AS T1 JOIN Stores AS T2 ON T1.Marketing_Region_Code = T2.Marketing_Region_Code WHERE T2.Store_Name = "Rob Dinning"	cre_Drama_Workshop_Groups
SELECT T1.Marketing_Region_Name FROM Marketing_Regions AS T1 JOIN Stores AS T2 ON T1.Marketing_Region_Code = T2.Marketing_Region_Code WHERE T2.Store_Name = "Rob Dinning"	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code WHERE T2.Product_Price > 100	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code WHERE T2.Product_Price > 100	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description , T2.Service_Type_Code , COUNT(*) FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code GROUP BY T2.Service_Type_Code	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description , T2.Service_Type_Code , COUNT(*) FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code GROUP BY T2.Service_Type_Code	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description , T1.Service_Type_Code FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code GROUP BY T1.Service_Type_Code ORDER BY COUNT(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description , T1.Service_Type_Code FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code GROUP BY T1.Service_Type_Code ORDER BY COUNT(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT T1.Store_Phone , T1.Store_Email_Address FROM Drama_Workshop_Groups AS T1 JOIN Services AS T2 ON T1.Workshop_Group_ID = T2.Workshop_Group_ID	cre_Drama_Workshop_Groups
SELECT T1.Store_Phone , T1.Store_Email_Address FROM Drama_Workshop_Groups AS T1 JOIN Services AS T2 ON T1.Workshop_Group_ID = T2.Workshop_Group_ID	cre_Drama_Workshop_Groups
SELECT T1.Store_Phone , T1.Store_Email_Address FROM Drama_Workshop_Groups AS T1 JOIN Services AS T2 ON T1.Workshop_Group_ID = T2.Workshop_Group_ID WHERE T2.Product_Name = "film"	cre_Drama_Workshop_Groups
SELECT T1.Store_Phone , T1.Store_Email_Address FROM Drama_Workshop_Groups AS T1 JOIN Services AS T2 ON T1.Workshop_Group_ID = T2.Workshop_Group_ID WHERE T2.Product_Name = "film"	cre_Drama_Workshop_Groups
SELECT Product_Name , avg(Product_Price) FROM PRODUCTS GROUP BY Product_Name	cre_Drama_Workshop_Groups
SELECT Product_Name , avg(Product_Price) FROM PRODUCTS GROUP BY Product_Name	cre_Drama_Workshop_Groups
SELECT Product_Name FROM PRODUCTS GROUP BY Product_Name HAVING avg(Product_Price) < 1000000	cre_Drama_Workshop_Groups
SELECT Product_Name FROM PRODUCTS GROUP BY Product_Name HAVING avg(Product_Price) < 1000000	cre_Drama_Workshop_Groups
SELECT sum(T1.Order_Quantity) FROM ORDER_ITEMS AS T1 JOIN Products AS T2 ON T1.Product_ID = T2.Product_ID WHERE T2.Product_Name = "photo"	cre_Drama_Workshop_Groups
SELECT sum(T1.Order_Quantity) FROM ORDER_ITEMS AS T1 JOIN Products AS T2 ON T1.Product_ID = T2.Product_ID WHERE T2.Product_Name = "photo"	cre_Drama_Workshop_Groups
SELECT T1.Other_Item_Details FROM ORDER_ITEMS AS T1 JOIN Products AS T2 ON T1.Product_ID = T2.Product_ID WHERE T2.Product_price > 2000	cre_Drama_Workshop_Groups
SELECT T1.Other_Item_Details FROM ORDER_ITEMS AS T1 JOIN Products AS T2 ON T1.Product_ID = T2.Product_ID WHERE T2.Product_price > 2000	cre_Drama_Workshop_Groups
SELECT T1.Actual_Delivery_Date FROM Customer_Orders AS T1 JOIN ORDER_ITEMS AS T2 ON T1.Order_ID = T2.Order_ID WHERE T2.Order_Quantity = 1	cre_Drama_Workshop_Groups
SELECT T1.Actual_Delivery_Date FROM Customer_Orders AS T1 JOIN ORDER_ITEMS AS T2 ON T1.Order_ID = T2.Order_ID WHERE T2.Order_Quantity = 1	cre_Drama_Workshop_Groups
SELECT T1.Order_Date FROM Customer_Orders AS T1 JOIN ORDER_ITEMS AS T2 ON T1.Order_ID = T2.Order_ID JOIN Products AS T3 ON T2.Product_ID = T3.Product_ID WHERE T3.Product_price > 1000	cre_Drama_Workshop_Groups
SELECT T1.Order_Date FROM Customer_Orders AS T1 JOIN ORDER_ITEMS AS T2 ON T1.Order_ID = T2.Order_ID JOIN Products AS T3 ON T2.Product_ID = T3.Product_ID WHERE T3.Product_price > 1000	cre_Drama_Workshop_Groups
SELECT count(DISTINCT Currency_Code) FROM Drama_Workshop_Groups	cre_Drama_Workshop_Groups
SELECT count(DISTINCT Currency_Code) FROM Drama_Workshop_Groups	cre_Drama_Workshop_Groups
SELECT T2.Store_Name FROM Addresses AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Address_ID = T2.Address_ID WHERE T1.City_Town = "Feliciaberg"	cre_Drama_Workshop_Groups
SELECT T2.Store_Name FROM Addresses AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Address_ID = T2.Address_ID WHERE T1.City_Town = "Feliciaberg"	cre_Drama_Workshop_Groups
SELECT T2.Store_Email_Address FROM Addresses AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Address_ID = T2.Address_ID WHERE T1.State_County = "Alaska"	cre_Drama_Workshop_Groups
SELECT T2.Store_Email_Address FROM Addresses AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Address_ID = T2.Address_ID WHERE T1.State_County = "Alaska"	cre_Drama_Workshop_Groups
SELECT T1.City_Town , count(*) FROM Addresses AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Address_ID = T2.Address_ID GROUP BY T1.City_Town	cre_Drama_Workshop_Groups
SELECT T1.City_Town , count(*) FROM Addresses AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Address_ID = T2.Address_ID GROUP BY T1.City_Town	cre_Drama_Workshop_Groups
SELECT Marketing_Region_Code FROM Drama_Workshop_Groups GROUP BY Marketing_Region_Code ORDER BY count(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT Marketing_Region_Code FROM Drama_Workshop_Groups GROUP BY Marketing_Region_Code ORDER BY count(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT T1.City_Town FROM Addresses AS T1 JOIN Customers AS T2 ON T1.Address_ID = T2.Address_ID EXCEPT SELECT T1.City_Town FROM Addresses AS T1 JOIN Performers AS T2 ON T1.Address_ID = T2.Address_ID	cre_Drama_Workshop_Groups
SELECT T1.City_Town FROM Addresses AS T1 JOIN Customers AS T2 ON T1.Address_ID = T2.Address_ID EXCEPT SELECT T1.City_Town FROM Addresses AS T1 JOIN Performers AS T2 ON T1.Address_ID = T2.Address_ID	cre_Drama_Workshop_Groups
SELECT Status_Code FROM BOOKINGS GROUP BY Status_Code ORDER BY count(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT Status_Code FROM BOOKINGS GROUP BY Status_Code ORDER BY count(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT T2.Store_Name FROM Bookings AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Workshop_Group_ID = T2.Workshop_Group_ID WHERE T1.Status_Code = "stop"	cre_Drama_Workshop_Groups
SELECT T2.Store_Name FROM Bookings AS T1 JOIN Drama_Workshop_Groups AS T2 ON T1.Workshop_Group_ID = T2.Workshop_Group_ID WHERE T1.Status_Code = "stop"	cre_Drama_Workshop_Groups
SELECT Customer_Name FROM Clients EXCEPT SELECT T2.Customer_Name FROM Bookings AS T1 JOIN Clients AS T2 ON T1.Customer_ID = T2.Client_ID	cre_Drama_Workshop_Groups
SELECT Customer_Name FROM Clients EXCEPT SELECT T2.Customer_Name FROM Bookings AS T1 JOIN Clients AS T2 ON T1.Customer_ID = T2.Client_ID	cre_Drama_Workshop_Groups
SELECT avg(Order_Quantity) FROM Invoices WHERE payment_method_code = "MasterCard"	cre_Drama_Workshop_Groups
SELECT avg(Order_Quantity) FROM Invoices WHERE payment_method_code = "MasterCard"	cre_Drama_Workshop_Groups
SELECT Product_ID FROM INVOICES GROUP BY Product_ID ORDER BY COUNT(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT Product_ID FROM INVOICES GROUP BY Product_ID ORDER BY COUNT(*) DESC LIMIT 1	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code WHERE T2.Product_Name = 'photo' INTERSECT SELECT T1.Service_Type_Description FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code WHERE T2.Product_Name = 'film'	cre_Drama_Workshop_Groups
SELECT T1.Service_Type_Description FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code WHERE T2.Product_Name = 'photo' INTERSECT SELECT T1.Service_Type_Description FROM Ref_Service_Types AS T1 JOIN Services AS T2 ON T1.Service_Type_Code = T2.Service_Type_Code WHERE T2.Product_Name = 'film'	cre_Drama_Workshop_Groups
SELECT count(*) FROM Band	music_2
SELECT count(*) FROM Band	music_2
SELECT DISTINCT label FROM Albums	music_2
SELECT DISTINCT label FROM Albums	music_2
SELECT * FROM Albums WHERE YEAR = 2012	music_2
SELECT * FROM Albums WHERE YEAR = 2012	music_2
SELECT DISTINCT T1.stageposition FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id WHERE Firstname = "Solveig"	music_2
SELECT DISTINCT T1.stageposition FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id WHERE Firstname = "Solveig"	music_2
SELECT count(*) FROM Songs	music_2
SELECT count(*) FROM Songs	music_2
SELECT T3.Title FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T2.Lastname = "Heilo"	music_2
SELECT T3.Title FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T2.Lastname = "Heilo"	music_2
SELECT count(*) FROM performance AS T1 JOIN band AS T2 ON T1.bandmate = T2.id JOIN songs AS T3 ON T3.songid = T1.songid WHERE T3.Title = "Flash"	music_2
SELECT count(*) FROM performance AS T1 JOIN band AS T2 ON T1.bandmate = T2.id JOIN songs AS T3 ON T3.songid = T1.songid WHERE T3.Title = "Flash"	music_2
SELECT T3.Title FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T2.firstname = "Marianne"	music_2
SELECT T3.Title FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T2.firstname = "Marianne"	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Badlands"	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Badlands"	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Badlands" AND T1.StagePosition = "back"	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Badlands" AND T1.StagePosition = "back"	music_2
SELECT count(DISTINCT label) FROM albums	music_2
SELECT count(DISTINCT label) FROM albums	music_2
SELECT label FROM albums GROUP BY label ORDER BY count(*) DESC LIMIT 1	music_2
SELECT label FROM albums GROUP BY label ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId GROUP BY lastname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId GROUP BY lastname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id WHERE stageposition = "back" GROUP BY lastname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id WHERE stageposition = "back" GROUP BY lastname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT title FROM songs WHERE title LIKE '% the %'	music_2
SELECT title FROM songs WHERE title LIKE '% the %'	music_2
SELECT DISTINCT instrument FROM Instruments	music_2
SELECT DISTINCT instrument FROM Instruments	music_2
SELECT T4.instrument FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId JOIN Instruments AS T4 ON T4.songid = T3.songid AND T4.bandmateid = T2.id WHERE T2.lastname = "Heilo" AND T3.title = "Le Pop"	music_2
SELECT T4.instrument FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId JOIN Instruments AS T4 ON T4.songid = T3.songid AND T4.bandmateid = T2.id WHERE T2.lastname = "Heilo" AND T3.title = "Le Pop"	music_2
SELECT instrument FROM instruments GROUP BY instrument ORDER BY count(*) DESC LIMIT 1	music_2
SELECT instrument FROM instruments GROUP BY instrument ORDER BY count(*) DESC LIMIT 1	music_2
SELECT count(*) FROM instruments WHERE instrument = "drums"	music_2
SELECT count(*) FROM instruments WHERE instrument = "drums"	music_2
SELECT instrument FROM instruments AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT instrument FROM instruments AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT count(DISTINCT instrument) FROM instruments AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT count(DISTINCT instrument) FROM instruments AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT count(DISTINCT instrument) FROM instruments AS T1 JOIN Band AS T2 ON T1.bandmateid = T2.id WHERE T2.lastname = "Heilo"	music_2
SELECT count(DISTINCT instrument) FROM instruments AS T1 JOIN Band AS T2 ON T1.bandmateid = T2.id WHERE T2.lastname = "Heilo"	music_2
SELECT instrument FROM instruments AS T1 JOIN Band AS T2 ON T1.bandmateid = T2.id WHERE T2.lastname = "Heilo"	music_2
SELECT instrument FROM instruments AS T1 JOIN Band AS T2 ON T1.bandmateid = T2.id WHERE T2.lastname = "Heilo"	music_2
SELECT title FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid GROUP BY T1.songid ORDER BY count(*) DESC LIMIT 1	music_2
SELECT title FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid GROUP BY T1.songid ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN band AS T2 ON T1.bandmate = T2.id WHERE lastname = "Heilo" GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN band AS T2 ON T1.bandmate = T2.id WHERE lastname = "Heilo" GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT count(*) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Demon Kitty Rag"	music_2
SELECT count(*) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Demon Kitty Rag"	music_2
SELECT count(DISTINCT title) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE TYPE = "lead"	music_2
SELECT count(DISTINCT title) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE TYPE = "lead"	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid JOIN band AS T3 ON T1.bandmate = T3.id WHERE T3.firstname = "Solveig" AND T2.title = "A Bar In Amsterdam"	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid JOIN band AS T3 ON T1.bandmate = T3.id WHERE T3.firstname = "Solveig" AND T2.title = "A Bar In Amsterdam"	music_2
SELECT DISTINCT title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid EXCEPT SELECT t2.title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid WHERE TYPE = "lead"	music_2
SELECT DISTINCT title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid EXCEPT SELECT t2.title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid WHERE TYPE = "lead"	music_2
SELECT DISTINCT TYPE FROM vocals	music_2
SELECT DISTINCT TYPE FROM vocals	music_2
SELECT * FROM Albums WHERE YEAR = 2010	music_2
SELECT * FROM Albums WHERE YEAR = 2010	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Le Pop"	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Le Pop"	music_2
SELECT T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId GROUP BY lastname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId GROUP BY lastname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T4.instrument FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId JOIN Instruments AS T4 ON T4.songid = T3.songid AND T4.bandmateid = T2.id WHERE T2.lastname = "Heilo" AND T3.title = "Badlands"	music_2
SELECT T4.instrument FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId JOIN Instruments AS T4 ON T4.songid = T3.songid AND T4.bandmateid = T2.id WHERE T2.lastname = "Heilo" AND T3.title = "Badlands"	music_2
SELECT count(DISTINCT instrument) FROM instruments AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Badlands"	music_2
SELECT count(DISTINCT instrument) FROM instruments AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Badlands"	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Badlands"	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Badlands"	music_2
SELECT count(*) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT count(*) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE title = "Le Pop"	music_2
SELECT count(DISTINCT title) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE TYPE = "shared"	music_2
SELECT count(DISTINCT title) FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid WHERE TYPE = "shared"	music_2
SELECT DISTINCT title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid EXCEPT SELECT t2.title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid WHERE TYPE = "back"	music_2
SELECT DISTINCT title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid EXCEPT SELECT t2.title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid WHERE TYPE = "back"	music_2
SELECT TYPE FROM vocals AS T1 JOIN band AS T2 ON T1.bandmate = T2.id WHERE firstname = "Solveig" GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN band AS T2 ON T1.bandmate = T2.id WHERE firstname = "Solveig" GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid JOIN band AS T3 ON T1.bandmate = T3.id WHERE T3.lastname = "Heilo" AND T2.title = "Der Kapitan"	music_2
SELECT TYPE FROM vocals AS T1 JOIN songs AS T2 ON T1.songid = T2.songid JOIN band AS T3 ON T1.bandmate = T3.id WHERE T3.lastname = "Heilo" AND T2.title = "Der Kapitan"	music_2
SELECT t2.firstname FROM Performance AS t1 JOIN Band AS t2 ON t1.bandmate = t2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId GROUP BY firstname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT t2.firstname FROM Performance AS t1 JOIN Band AS t2 ON t1.bandmate = t2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId GROUP BY firstname ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN band AS T2 ON T1.bandmate = T2.id WHERE firstname = "Marianne" GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT TYPE FROM vocals AS T1 JOIN band AS T2 ON T1.bandmate = T2.id WHERE firstname = "Marianne" GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Der Kapitan" AND T1.StagePosition = "back"	music_2
SELECT T2.firstname , T2.lastname FROM Performance AS T1 JOIN Band AS T2 ON T1.bandmate = T2.id JOIN Songs AS T3 ON T3.SongId = T1.SongId WHERE T3.Title = "Der Kapitan" AND T1.StagePosition = "back"	music_2
SELECT DISTINCT title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid EXCEPT SELECT t2.title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid WHERE TYPE = "back"	music_2
SELECT DISTINCT title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid EXCEPT SELECT t2.title FROM vocals AS t1 JOIN songs AS t2 ON t1.songid = t2.songid WHERE TYPE = "back"	music_2
SELECT T3.title FROM albums AS T1 JOIN tracklists AS T2 ON T1.aid = T2.albumid JOIN songs AS T3 ON T2.songid = T3.songid WHERE T1.title = "A Kiss Before You Go: Live in Hamburg"	music_2
SELECT T3.title FROM albums AS T1 JOIN tracklists AS T2 ON T1.aid = T2.albumid JOIN songs AS T3 ON T2.songid = T3.songid WHERE T1.title = "A Kiss Before You Go: Live in Hamburg"	music_2
SELECT T3.title FROM albums AS T1 JOIN tracklists AS T2 ON T1.aid = T2.albumid JOIN songs AS T3 ON T2.songid = T3.songid WHERE t1.label = "Universal Music Group"	music_2
SELECT T3.title FROM albums AS T1 JOIN tracklists AS T2 ON T1.aid = T2.albumid JOIN songs AS T3 ON T2.songid = T3.songid WHERE t1.label = "Universal Music Group"	music_2
SELECT count(DISTINCT T3.title) FROM albums AS T1 JOIN tracklists AS T2 ON T1.aid = T2.albumid JOIN songs AS T3 ON T2.songid = T3.songid WHERE t1.type = "Studio"	music_2
SELECT count(DISTINCT T3.title) FROM albums AS T1 JOIN tracklists AS T2 ON T1.aid = T2.albumid JOIN songs AS T3 ON T2.songid = T3.songid WHERE t1.type = "Studio"	music_2
SELECT founder FROM manufacturers WHERE name = 'Sony'	manufactory_1
SELECT founder FROM manufacturers WHERE name = 'Sony'	manufactory_1
SELECT headquarter FROM manufacturers WHERE founder = 'James'	manufactory_1
SELECT headquarter FROM manufacturers WHERE founder = 'James'	manufactory_1
SELECT name , headquarter FROM manufacturers ORDER BY revenue DESC	manufactory_1
SELECT name , headquarter FROM manufacturers ORDER BY revenue DESC	manufactory_1
SELECT avg(revenue) , max(revenue) , sum(revenue) FROM manufacturers	manufactory_1
SELECT avg(revenue) , max(revenue) , sum(revenue) FROM manufacturers	manufactory_1
SELECT count(*) FROM manufacturers WHERE founder = 'Andy'	manufactory_1
SELECT count(*) FROM manufacturers WHERE founder = 'Andy'	manufactory_1
SELECT sum(revenue) FROM manufacturers WHERE headquarter = 'Austin'	manufactory_1
SELECT sum(revenue) FROM manufacturers WHERE headquarter = 'Austin'	manufactory_1
SELECT DISTINCT headquarter FROM manufacturers	manufactory_1
SELECT DISTINCT headquarter FROM manufacturers	manufactory_1
SELECT count(*) FROM manufacturers WHERE headquarter = 'Tokyo' OR headquarter = 'Beijing'	manufactory_1
SELECT count(*) FROM manufacturers WHERE headquarter = 'Tokyo' OR headquarter = 'Beijing'	manufactory_1
SELECT founder FROM manufacturers WHERE name LIKE 'S%'	manufactory_1
SELECT founder FROM manufacturers WHERE name LIKE 'S%'	manufactory_1
SELECT name FROM manufacturers WHERE revenue BETWEEN 100 AND 150	manufactory_1
SELECT name FROM manufacturers WHERE revenue BETWEEN 100 AND 150	manufactory_1
SELECT sum(revenue) FROM manufacturers WHERE Headquarter = 'Tokyo' OR Headquarter = 'Taiwan'	manufactory_1
SELECT sum(revenue) FROM manufacturers WHERE Headquarter = 'Tokyo' OR Headquarter = 'Taiwan'	manufactory_1
SELECT T1.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T2.name = 'Creative Labs' INTERSECT SELECT T1.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T2.name = 'Sony'	manufactory_1
SELECT T1.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T2.name = 'Creative Labs' INTERSECT SELECT T1.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T2.name = 'Sony'	manufactory_1
SELECT name , headquarter , founder FROM manufacturers ORDER BY revenue DESC LIMIT 1	manufactory_1
SELECT name , headquarter , founder FROM manufacturers ORDER BY revenue DESC LIMIT 1	manufactory_1
SELECT name , headquarter , revenue FROM manufacturers ORDER BY revenue DESC	manufactory_1
SELECT name , headquarter , revenue FROM manufacturers ORDER BY revenue DESC	manufactory_1
SELECT name FROM manufacturers WHERE revenue > (SELECT avg(revenue) FROM manufacturers)	manufactory_1
SELECT name FROM manufacturers WHERE revenue > (SELECT avg(revenue) FROM manufacturers)	manufactory_1
SELECT name FROM manufacturers WHERE revenue < (SELECT min(revenue) FROM manufacturers WHERE headquarter = 'Austin')	manufactory_1
SELECT name FROM manufacturers WHERE revenue < (SELECT min(revenue) FROM manufacturers WHERE headquarter = 'Austin')	manufactory_1
SELECT sum(revenue) FROM manufacturers WHERE revenue > (SELECT min(revenue) FROM manufacturers WHERE headquarter = 'Austin')	manufactory_1
SELECT sum(revenue) FROM manufacturers WHERE revenue > (SELECT min(revenue) FROM manufacturers WHERE headquarter = 'Austin')	manufactory_1
SELECT sum(revenue) , founder FROM manufacturers GROUP BY founder	manufactory_1
SELECT sum(revenue) , founder FROM manufacturers GROUP BY founder	manufactory_1
SELECT name , max(revenue) , Headquarter FROM manufacturers GROUP BY Headquarter	manufactory_1
SELECT name , max(revenue) , Headquarter FROM manufacturers GROUP BY Headquarter	manufactory_1
SELECT sum(revenue) , name FROM manufacturers GROUP BY name	manufactory_1
SELECT sum(revenue) , name FROM manufacturers GROUP BY name	manufactory_1
SELECT avg(T1.price) , T2.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT avg(T1.price) , T2.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT count(DISTINCT T1.name) , T2.Headquarter FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code GROUP BY T2.Headquarter	manufactory_1
SELECT count(DISTINCT T1.name) , T2.Headquarter FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code GROUP BY T2.Headquarter	manufactory_1
SELECT count(DISTINCT name) FROM products WHERE name NOT IN (SELECT T1.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T2.name = 'Sony')	manufactory_1
SELECT count(DISTINCT name) FROM products WHERE name NOT IN (SELECT T1.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T2.name = 'Sony')	manufactory_1
SELECT name FROM manufacturers EXCEPT SELECT T2.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T1.name = 'DVD drive'	manufactory_1
SELECT name FROM manufacturers EXCEPT SELECT T2.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code WHERE T1.name = 'DVD drive'	manufactory_1
SELECT count(*) , T2.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT count(*) , T2.name FROM products AS T1 JOIN manufacturers AS T2 ON T1.Manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT Name FROM Products	manufactory_1
SELECT Name FROM Products	manufactory_1
SELECT name , price FROM products	manufactory_1
SELECT name , price FROM products	manufactory_1
SELECT name FROM products WHERE price <= 200	manufactory_1
SELECT name FROM products WHERE price <= 200	manufactory_1
SELECT * FROM products WHERE price BETWEEN 60 AND 120	manufactory_1
SELECT * FROM products WHERE price BETWEEN 60 AND 120	manufactory_1
SELECT avg(price) FROM products	manufactory_1
SELECT avg(price) FROM products	manufactory_1
SELECT avg(price) FROM products WHERE Manufacturer = 2	manufactory_1
SELECT avg(price) FROM products WHERE Manufacturer = 2	manufactory_1
SELECT count(*) FROM products WHERE price >= 180	manufactory_1
SELECT count(*) FROM products WHERE price >= 180	manufactory_1
SELECT name , price FROM products WHERE price >= 180 ORDER BY price DESC , name ASC	manufactory_1
SELECT name , price FROM products WHERE price >= 180 ORDER BY price DESC , name ASC	manufactory_1
SELECT * FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code	manufactory_1
SELECT * FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code	manufactory_1
SELECT AVG(Price) , Manufacturer FROM Products GROUP BY Manufacturer	manufactory_1
SELECT AVG(Price) , Manufacturer FROM Products GROUP BY Manufacturer	manufactory_1
SELECT avg(T1.Price) , T2.name FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT avg(T1.Price) , T2.name FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT avg(T1.Price) , T2.name FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code GROUP BY T2.name HAVING avg(T1.price) >= 150	manufactory_1
SELECT avg(T1.Price) , T2.name FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code GROUP BY T2.name HAVING avg(T1.price) >= 150	manufactory_1
SELECT name , price FROM Products ORDER BY price ASC LIMIT 1	manufactory_1
SELECT name , price FROM Products ORDER BY price ASC LIMIT 1	manufactory_1
SELECT T1.Name , max(T1.Price) , T2.name FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT T1.Name , max(T1.Price) , T2.name FROM products AS T1 JOIN Manufacturers AS T2 ON T1.manufacturer = T2.code GROUP BY T2.name	manufactory_1
SELECT code , name , min(price) FROM products GROUP BY name	manufactory_1
SELECT code , name , min(price) FROM products GROUP BY name	manufactory_1
SELECT problem_log_id FROM problem_log ORDER BY log_entry_date DESC LIMIT 1	tracking_software_problems
SELECT problem_log_id FROM problem_log ORDER BY log_entry_date DESC LIMIT 1	tracking_software_problems
SELECT problem_log_id , problem_id FROM problem_log ORDER BY log_entry_date LIMIT 1	tracking_software_problems
SELECT problem_log_id , problem_id FROM problem_log ORDER BY log_entry_date LIMIT 1	tracking_software_problems
SELECT problem_log_id , log_entry_date FROM problem_log WHERE problem_id = 10	tracking_software_problems
SELECT problem_log_id , log_entry_date FROM problem_log WHERE problem_id = 10	tracking_software_problems
SELECT problem_log_id , log_entry_description FROM problem_log	tracking_software_problems
SELECT problem_log_id , log_entry_description FROM problem_log	tracking_software_problems
SELECT DISTINCT staff_first_name , staff_last_name FROM staff AS T1 JOIN problem_log AS T2 ON T1.staff_id = T2.assigned_to_staff_id WHERE T2.problem_id = 1	tracking_software_problems
SELECT DISTINCT staff_first_name , staff_last_name FROM staff AS T1 JOIN problem_log AS T2 ON T1.staff_id = T2.assigned_to_staff_id WHERE T2.problem_id = 1	tracking_software_problems
SELECT DISTINCT T2.problem_id , T2.problem_log_id FROM staff AS T1 JOIN problem_log AS T2 ON T1.staff_id = T2.assigned_to_staff_id WHERE T1.staff_first_name = "Rylan" AND T1.staff_last_name = "Homenick"	tracking_software_problems
SELECT DISTINCT T2.problem_id , T2.problem_log_id FROM staff AS T1 JOIN problem_log AS T2 ON T1.staff_id = T2.assigned_to_staff_id WHERE T1.staff_first_name = "Rylan" AND T1.staff_last_name = "Homenick"	tracking_software_problems
SELECT count(*) FROM product AS T1 JOIN problems AS T2 ON T1.product_id = T2.product_id WHERE T1.product_name = "voluptatem"	tracking_software_problems
SELECT count(*) FROM product AS T1 JOIN problems AS T2 ON T1.product_id = T2.product_id WHERE T1.product_name = "voluptatem"	tracking_software_problems
SELECT count(*) , T1.product_name FROM product AS T1 JOIN problems AS T2 ON T1.product_id = T2.product_id GROUP BY T1.product_name ORDER BY count(*) DESC LIMIT 1	tracking_software_problems
SELECT count(*) , T1.product_name FROM product AS T1 JOIN problems AS T2 ON T1.product_id = T2.product_id GROUP BY T1.product_name ORDER BY count(*) DESC LIMIT 1	tracking_software_problems
SELECT T1.problem_description FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Christop"	tracking_software_problems
SELECT T1.problem_description FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Christop"	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_last_name = "Bosco"	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_last_name = "Bosco"	tracking_software_problems
SELECT problem_id FROM problems WHERE date_problem_reported > "1978-06-26"	tracking_software_problems
SELECT problem_id FROM problems WHERE date_problem_reported > "1978-06-26"	tracking_software_problems
SELECT problem_id FROM problems WHERE date_problem_reported < "1978-06-26"	tracking_software_problems
SELECT problem_id FROM problems WHERE date_problem_reported < "1978-06-26"	tracking_software_problems
SELECT count(*) , T2.product_id FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id GROUP BY T2.product_id	tracking_software_problems
SELECT count(*) , T2.product_id FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id GROUP BY T2.product_id	tracking_software_problems
SELECT count(*) , T2.product_id FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id WHERE T1.date_problem_reported > "1986-11-13" GROUP BY T2.product_id	tracking_software_problems
SELECT count(*) , T2.product_id FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id WHERE T1.date_problem_reported > "1986-11-13" GROUP BY T2.product_id	tracking_software_problems
SELECT DISTINCT product_name FROM product ORDER BY product_name	tracking_software_problems
SELECT DISTINCT product_name FROM product ORDER BY product_name	tracking_software_problems
SELECT DISTINCT product_name FROM product ORDER BY product_id	tracking_software_problems
SELECT DISTINCT product_name FROM product ORDER BY product_id	tracking_software_problems
SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Dameon" AND T2.staff_last_name = "Frami" UNION SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Jolie" AND T2.staff_last_name = "Weber"	tracking_software_problems
SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Dameon" AND T2.staff_last_name = "Frami" UNION SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Jolie" AND T2.staff_last_name = "Weber"	tracking_software_problems
SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Christop" AND T2.staff_last_name = "Berge" INTERSECT SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.closure_authorised_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Ashley" AND T2.staff_last_name = "Medhurst"	tracking_software_problems
SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Christop" AND T2.staff_last_name = "Berge" INTERSECT SELECT product_id FROM problems AS T1 JOIN staff AS T2 ON T1.closure_authorised_by_staff_id = T2.staff_id WHERE T2.staff_first_name = "Ashley" AND T2.staff_last_name = "Medhurst"	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE date_problem_reported < ( SELECT min(date_problem_reported) FROM problems AS T3 JOIN staff AS T4 ON T3.reported_by_staff_id = T4.staff_id WHERE T4.staff_first_name = "Lysanne" AND T4.staff_last_name = "Turcotte" )	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE date_problem_reported < ( SELECT min(date_problem_reported) FROM problems AS T3 JOIN staff AS T4 ON T3.reported_by_staff_id = T4.staff_id WHERE T4.staff_first_name = "Lysanne" AND T4.staff_last_name = "Turcotte" )	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE date_problem_reported > ( SELECT max(date_problem_reported) FROM problems AS T3 JOIN staff AS T4 ON T3.reported_by_staff_id = T4.staff_id WHERE T4.staff_first_name = "Rylan" AND T4.staff_last_name = "Homenick" )	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN staff AS T2 ON T1.reported_by_staff_id = T2.staff_id WHERE date_problem_reported > ( SELECT max(date_problem_reported) FROM problems AS T3 JOIN staff AS T4 ON T3.reported_by_staff_id = T4.staff_id WHERE T4.staff_first_name = "Rylan" AND T4.staff_last_name = "Homenick" )	tracking_software_problems
SELECT T2.product_name FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id GROUP BY T2.product_name ORDER BY count(*) DESC LIMIT 3	tracking_software_problems
SELECT T2.product_name FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id GROUP BY T2.product_name ORDER BY count(*) DESC LIMIT 3	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id WHERE T2.product_name = "voluptatem" AND T1.date_problem_reported > "1995"	tracking_software_problems
SELECT T1.problem_id FROM problems AS T1 JOIN product AS T2 ON T1.product_id = T2.product_id WHERE T2.product_name = "voluptatem" AND T1.date_problem_reported > "1995"	tracking_software_problems
SELECT T3.staff_first_name , T3.staff_last_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T2.product_name = "rem" EXCEPT SELECT T3.staff_first_name , T3.staff_last_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T2.product_name = "aut"	tracking_software_problems
SELECT T3.staff_first_name , T3.staff_last_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T2.product_name = "rem" EXCEPT SELECT T3.staff_first_name , T3.staff_last_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T2.product_name = "aut"	tracking_software_problems
SELECT T2.product_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T3.staff_first_name = "Lacey" AND T3.staff_last_name = "Bosco" INTERSECT SELECT T2.product_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T3.staff_first_name = "Kenton" AND T3.staff_last_name = "Champlin"	tracking_software_problems
SELECT T2.product_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T3.staff_first_name = "Lacey" AND T3.staff_last_name = "Bosco" INTERSECT SELECT T2.product_name FROM problems AS T1 JOIN product AS T2 JOIN staff AS T3 ON T1.product_id = T2.product_id AND T1.reported_by_staff_id = T3.staff_id WHERE T3.staff_first_name = "Kenton" AND T3.staff_last_name = "Champlin"	tracking_software_problems
SELECT count(*) FROM branch WHERE membership_amount > (SELECT avg(membership_amount) FROM branch)	shop_membership
SELECT count(*) FROM branch WHERE membership_amount > (SELECT avg(membership_amount) FROM branch)	shop_membership
SELECT name , address_road , city FROM branch ORDER BY open_year	shop_membership
SELECT name , address_road , city FROM branch ORDER BY open_year	shop_membership
SELECT name FROM branch ORDER BY membership_amount DESC LIMIT 3	shop_membership
SELECT name FROM branch ORDER BY membership_amount DESC LIMIT 3	shop_membership
SELECT DISTINCT city FROM branch WHERE membership_amount >= 100	shop_membership
SELECT DISTINCT city FROM branch WHERE membership_amount >= 100	shop_membership
SELECT open_year FROM branch GROUP BY open_year HAVING count(*) >= 2	shop_membership
SELECT open_year FROM branch GROUP BY open_year HAVING count(*) >= 2	shop_membership
SELECT min(membership_amount) , max(membership_amount) FROM branch WHERE open_year = 2011 OR city = 'London'	shop_membership
SELECT min(membership_amount) , max(membership_amount) FROM branch WHERE open_year = 2011 OR city = 'London'	shop_membership
SELECT city , count(*) FROM branch WHERE open_year < 2010 GROUP BY city	shop_membership
SELECT city , count(*) FROM branch WHERE open_year < 2010 GROUP BY city	shop_membership
SELECT count(DISTINCT LEVEL) FROM member	shop_membership
SELECT count(DISTINCT LEVEL) FROM member	shop_membership
SELECT card_number , name , hometown FROM member ORDER BY LEVEL DESC	shop_membership
SELECT card_number , name , hometown FROM member ORDER BY LEVEL DESC	shop_membership
SELECT LEVEL FROM member GROUP BY LEVEL ORDER BY count(*) DESC LIMIT 1	shop_membership
SELECT LEVEL FROM member GROUP BY LEVEL ORDER BY count(*) DESC LIMIT 1	shop_membership
SELECT T3.name , T2.name FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id JOIN member AS T3 ON T1.member_id = T3.member_id ORDER BY T1.register_year	shop_membership
SELECT T3.name , T2.name FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id JOIN member AS T3 ON T1.member_id = T3.member_id ORDER BY T1.register_year	shop_membership
SELECT T2.name , count(*) FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id WHERE T1.register_year > 2015 GROUP BY T2.branch_id	shop_membership
SELECT T2.name , count(*) FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id WHERE T1.register_year > 2015 GROUP BY T2.branch_id	shop_membership
SELECT name FROM member WHERE member_id NOT IN (SELECT member_id FROM membership_register_branch)	shop_membership
SELECT name FROM member WHERE member_id NOT IN (SELECT member_id FROM membership_register_branch)	shop_membership
SELECT name , city FROM branch WHERE branch_id NOT IN (SELECT branch_id FROM membership_register_branch)	shop_membership
SELECT name , city FROM branch WHERE branch_id NOT IN (SELECT branch_id FROM membership_register_branch)	shop_membership
SELECT T2.name , T2.open_year FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id WHERE T1.register_year = 2016 GROUP BY T2.branch_id ORDER BY count(*) DESC LIMIT 1	shop_membership
SELECT T2.name , T2.open_year FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id WHERE T1.register_year = 2016 GROUP BY T2.branch_id ORDER BY count(*) DESC LIMIT 1	shop_membership
SELECT T2.name , T2.hometown FROM membership_register_branch AS T1 JOIN member AS T2 ON T1.member_id = T2.member_id WHERE T1.register_year = 2016	shop_membership
SELECT T2.name , T2.hometown FROM membership_register_branch AS T1 JOIN member AS T2 ON T1.member_id = T2.member_id WHERE T1.register_year = 2016	shop_membership
SELECT city FROM branch WHERE open_year = 2001 AND membership_amount > 100	shop_membership
SELECT city FROM branch WHERE open_year = 2001 AND membership_amount > 100	shop_membership
SELECT city FROM branch EXCEPT SELECT city FROM branch WHERE membership_amount > 100	shop_membership
SELECT city FROM branch EXCEPT SELECT city FROM branch WHERE membership_amount > 100	shop_membership
SELECT sum(total_pounds) FROM purchase AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id WHERE T2.city = 'London' AND T1.year = 2018	shop_membership
SELECT sum(total_pounds) FROM purchase AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id WHERE T2.city = 'London' AND T1.year = 2018	shop_membership
SELECT count(*) FROM purchase AS T1 JOIN member AS T2 ON T1.member_id = T2.member_id WHERE T2.level = 6	shop_membership
SELECT count(*) FROM purchase AS T1 JOIN member AS T2 ON T1.member_id = T2.member_id WHERE T2.level = 6	shop_membership
SELECT T2.name FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id JOIN member AS T3 ON T1.member_id = T3.member_id WHERE T3.Hometown = 'Louisville , Kentucky' INTERSECT SELECT T2.name FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id JOIN member AS T3 ON T1.member_id = T3.member_id WHERE T3.Hometown = 'Hiram , Georgia'	shop_membership
SELECT T2.name FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id JOIN member AS T3 ON T1.member_id = T3.member_id WHERE T3.Hometown = 'Louisville , Kentucky' INTERSECT SELECT T2.name FROM membership_register_branch AS T1 JOIN branch AS T2 ON T1.branch_id = T2.branch_id JOIN member AS T3 ON T1.member_id = T3.member_id WHERE T3.Hometown = 'Hiram , Georgia'	shop_membership
SELECT card_number FROM member WHERE Hometown LIKE "%Kentucky%"	shop_membership
SELECT card_number FROM member WHERE Hometown LIKE "%Kentucky%"	shop_membership
SELECT count(*) FROM products	products_gen_characteristics
SELECT count(*) FROM products	products_gen_characteristics
SELECT count(*) FROM ref_colors	products_gen_characteristics
SELECT count(*) FROM ref_colors	products_gen_characteristics
SELECT count(*) FROM CHARACTERISTICS	products_gen_characteristics
SELECT count(*) FROM CHARACTERISTICS	products_gen_characteristics
SELECT product_name , typical_buying_price FROM products	products_gen_characteristics
SELECT product_name , typical_buying_price FROM products	products_gen_characteristics
SELECT color_description FROM ref_colors	products_gen_characteristics
SELECT color_description FROM ref_colors	products_gen_characteristics
SELECT DISTINCT characteristic_name FROM CHARACTERISTICS	products_gen_characteristics
SELECT DISTINCT characteristic_name FROM CHARACTERISTICS	products_gen_characteristics
SELECT product_name FROM products WHERE product_category_code = "Spices"	products_gen_characteristics
SELECT product_name FROM products WHERE product_category_code = "Spices"	products_gen_characteristics
SELECT T1.product_name , T2.color_description , T1.product_description FROM products AS T1 JOIN Ref_colors AS T2 ON T1.color_code = T2.color_code WHERE product_category_code = "Herbs"	products_gen_characteristics
SELECT T1.product_name , T2.color_description , T1.product_description FROM products AS T1 JOIN Ref_colors AS T2 ON T1.color_code = T2.color_code WHERE product_category_code = "Herbs"	products_gen_characteristics
SELECT count(*) FROM products WHERE product_category_code = "Seeds"	products_gen_characteristics
SELECT count(*) FROM products WHERE product_category_code = "Seeds"	products_gen_characteristics
SELECT count(*) FROM products WHERE product_category_code = "Spices" AND typical_buying_price > 1000	products_gen_characteristics
SELECT count(*) FROM products WHERE product_category_code = "Spices" AND typical_buying_price > 1000	products_gen_characteristics
SELECT product_category_code , typical_buying_price FROM products WHERE product_name = "cumin"	products_gen_characteristics
SELECT product_category_code , typical_buying_price FROM products WHERE product_name = "cumin"	products_gen_characteristics
SELECT product_category_code FROM products WHERE product_name = "flax"	products_gen_characteristics
SELECT product_category_code FROM products WHERE product_name = "flax"	products_gen_characteristics
SELECT T1.product_name FROM products AS T1 JOIN ref_colors AS T2 ON T1.color_code = T2.color_code WHERE T2.color_description = 'yellow'	products_gen_characteristics
SELECT T1.product_name FROM products AS T1 JOIN ref_colors AS T2 ON T1.color_code = T2.color_code WHERE T2.color_description = 'yellow'	products_gen_characteristics
SELECT T1.product_category_description FROM ref_product_categories AS T1 JOIN products AS T2 ON T1.product_category_code = T2.product_category_code WHERE T2.product_description LIKE '%t%'	products_gen_characteristics
SELECT T1.product_category_description FROM ref_product_categories AS T1 JOIN products AS T2 ON T1.product_category_code = T2.product_category_code WHERE T2.product_description LIKE '%t%'	products_gen_characteristics
SELECT t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t1.product_name = "catnip"	products_gen_characteristics
SELECT t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t1.product_name = "catnip"	products_gen_characteristics
SELECT t1.color_code , t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t1.product_name = "chervil"	products_gen_characteristics
SELECT t1.color_code , t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t1.product_name = "chervil"	products_gen_characteristics
SELECT t1.product_id , t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code JOIN product_characteristics AS t3 ON t1.product_id = t3.product_id GROUP BY t1.product_id HAVING count(*) >= 2	products_gen_characteristics
SELECT t1.product_id , t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code JOIN product_characteristics AS t3 ON t1.product_id = t3.product_id GROUP BY t1.product_id HAVING count(*) >= 2	products_gen_characteristics
SELECT t1.product_name FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t2.color_description = "white"	products_gen_characteristics
SELECT t1.product_name FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t2.color_description = "white"	products_gen_characteristics
SELECT t1.product_name , t1.typical_buying_price , t1.typical_selling_price FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t2.color_description = "yellow"	products_gen_characteristics
SELECT t1.product_name , t1.typical_buying_price , t1.typical_selling_price FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code WHERE t2.color_description = "yellow"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id WHERE t1.product_name = "sesame"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id WHERE t1.product_name = "sesame"	products_gen_characteristics
SELECT count(DISTINCT t3.characteristic_name) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "sesame"	products_gen_characteristics
SELECT count(DISTINCT t3.characteristic_name) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "sesame"	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "sesame"	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "sesame"	products_gen_characteristics
SELECT t3.characteristic_name , t3.characteristic_data_type FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "cumin"	products_gen_characteristics
SELECT t3.characteristic_name , t3.characteristic_data_type FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "cumin"	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "sesame" AND t3.characteristic_type_code = "Grade"	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "sesame" AND t3.characteristic_type_code = "Grade"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "laurel"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "laurel"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "flax"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t1.product_name = "flax"	products_gen_characteristics
SELECT product_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id JOIN ref_colors AS t4 ON t1.color_code = t4.color_code WHERE t4.color_description = "red" AND t3.characteristic_name = "fast"	products_gen_characteristics
SELECT product_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id JOIN ref_colors AS t4 ON t1.color_code = t4.color_code WHERE t4.color_description = "red" AND t3.characteristic_name = "fast"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t3.characteristic_name = "hot"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t3.characteristic_name = "hot"	products_gen_characteristics
SELECT DISTINCT t1.product_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t3.characteristic_name = "warm"	products_gen_characteristics
SELECT DISTINCT t1.product_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id WHERE t3.characteristic_name = "warm"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id JOIN ref_colors AS t4 ON t1.color_code = t4.color_code WHERE t4.color_description = "red" AND t3.characteristic_name = "slow"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id JOIN ref_colors AS t4 ON t1.color_code = t4.color_code WHERE t4.color_description = "red" AND t3.characteristic_name = "slow"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id JOIN ref_colors AS t4 ON t1.color_code = t4.color_code WHERE t4.color_description = "white" OR t3.characteristic_name = "hot"	products_gen_characteristics
SELECT count(*) FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id JOIN ref_colors AS t4 ON t1.color_code = t4.color_code WHERE t4.color_description = "white" OR t3.characteristic_name = "hot"	products_gen_characteristics
SELECT unit_of_measure FROM ref_product_categories WHERE product_category_code = "Herbs"	products_gen_characteristics
SELECT unit_of_measure FROM ref_product_categories WHERE product_category_code = "Herbs"	products_gen_characteristics
SELECT product_category_description FROM ref_product_categories WHERE product_category_code = "Spices"	products_gen_characteristics
SELECT product_category_description FROM ref_product_categories WHERE product_category_code = "Spices"	products_gen_characteristics
SELECT product_category_description , unit_of_measure FROM ref_product_categories WHERE product_category_code = "Herbs"	products_gen_characteristics
SELECT product_category_description , unit_of_measure FROM ref_product_categories WHERE product_category_code = "Herbs"	products_gen_characteristics
SELECT t2.unit_of_measure FROM products AS t1 JOIN ref_product_categories AS t2 ON t1.product_category_code = t2.product_category_code WHERE t1.product_name = "cumin"	products_gen_characteristics
SELECT t2.unit_of_measure FROM products AS t1 JOIN ref_product_categories AS t2 ON t1.product_category_code = t2.product_category_code WHERE t1.product_name = "cumin"	products_gen_characteristics
SELECT t2.unit_of_measure , t2.product_category_code FROM products AS t1 JOIN ref_product_categories AS t2 ON t1.product_category_code = t2.product_category_code WHERE t1.product_name = "chervil"	products_gen_characteristics
SELECT t2.unit_of_measure , t2.product_category_code FROM products AS t1 JOIN ref_product_categories AS t2 ON t1.product_category_code = t2.product_category_code WHERE t1.product_name = "chervil"	products_gen_characteristics
SELECT t1.product_name FROM products AS t1 JOIN ref_product_categories AS t2 ON t1.product_category_code = t2.product_category_code JOIN ref_colors AS t3 ON t1.color_code = t3.color_code WHERE t3.color_description = "white" AND t2.unit_of_measure != "Handful"	products_gen_characteristics
SELECT t1.product_name FROM products AS t1 JOIN ref_product_categories AS t2 ON t1.product_category_code = t2.product_category_code JOIN ref_colors AS t3 ON t1.color_code = t3.color_code WHERE t3.color_description = "white" AND t2.unit_of_measure != "Handful"	products_gen_characteristics
SELECT t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code GROUP BY t2.color_description ORDER BY count(*) DESC LIMIT 1	products_gen_characteristics
SELECT t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code GROUP BY t2.color_description ORDER BY count(*) DESC LIMIT 1	products_gen_characteristics
SELECT t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code GROUP BY t2.color_description ORDER BY count(*) ASC LIMIT 1	products_gen_characteristics
SELECT t2.color_description FROM products AS t1 JOIN ref_colors AS t2 ON t1.color_code = t2.color_code GROUP BY t2.color_description ORDER BY count(*) ASC LIMIT 1	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id GROUP BY t3.characteristic_name ORDER BY count(*) DESC LIMIT 1	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id GROUP BY t3.characteristic_name ORDER BY count(*) DESC LIMIT 1	products_gen_characteristics
SELECT characteristic_name , other_characteristic_details , characteristic_data_type FROM CHARACTERISTICS EXCEPT SELECT t1.characteristic_name , t1.other_characteristic_details , t1.characteristic_data_type FROM CHARACTERISTICS AS t1 JOIN product_characteristics AS t2 ON t1.characteristic_id = t2.characteristic_id	products_gen_characteristics
SELECT characteristic_name , other_characteristic_details , characteristic_data_type FROM CHARACTERISTICS EXCEPT SELECT t1.characteristic_name , t1.other_characteristic_details , t1.characteristic_data_type FROM CHARACTERISTICS AS t1 JOIN product_characteristics AS t2 ON t1.characteristic_id = t2.characteristic_id	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id GROUP BY t3.characteristic_name HAVING count(*) >= 2	products_gen_characteristics
SELECT t3.characteristic_name FROM products AS t1 JOIN product_characteristics AS t2 ON t1.product_id = t2.product_id JOIN CHARACTERISTICS AS t3 ON t2.characteristic_id = t3.characteristic_id GROUP BY t3.characteristic_name HAVING count(*) >= 2	products_gen_characteristics
SELECT count(*) FROM Ref_colors WHERE color_code NOT IN ( SELECT color_code FROM products )	products_gen_characteristics
SELECT count(*) FROM Ref_colors WHERE color_code NOT IN ( SELECT color_code FROM products )	products_gen_characteristics
SELECT count(*) FROM event	swimming
SELECT name FROM event ORDER BY YEAR DESC	swimming
SELECT name FROM event ORDER BY YEAR DESC LIMIT 1	swimming
SELECT count(*) FROM stadium	swimming
SELECT name FROM stadium ORDER BY capacity DESC LIMIT 1	swimming
SELECT name FROM stadium WHERE capacity < (SELECT avg(capacity) FROM stadium)	swimming
SELECT country FROM stadium GROUP BY country ORDER BY count(*) DESC LIMIT 1	swimming
SELECT country FROM stadium GROUP BY country HAVING count(*) <= 3	swimming
SELECT country FROM stadium WHERE capacity > 60000 INTERSECT SELECT country FROM stadium WHERE capacity < 50000	swimming
SELECT count(DISTINCT city) FROM stadium WHERE opening_year < 2006	swimming
SELECT country , count(*) FROM stadium GROUP BY country	swimming
SELECT country FROM stadium EXCEPT SELECT country FROM stadium WHERE opening_year > 2006	swimming
SELECT count(*) FROM stadium WHERE country != 'Russia'	swimming
SELECT name FROM swimmer ORDER BY meter_100	swimming
SELECT count(DISTINCT nationality) FROM swimmer	swimming
SELECT nationality , count(*) FROM swimmer GROUP BY nationality HAVING count(*) > 1	swimming
SELECT meter_200 , meter_300 FROM swimmer WHERE nationality = 'Australia'	swimming
SELECT t1.name FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id WHERE RESULT = 'Win'	swimming
SELECT t1.name FROM stadium AS t1 JOIN event AS t2 ON t1.id = t2.stadium_id GROUP BY t2.stadium_id ORDER BY count(*) DESC LIMIT 1	swimming
SELECT t1.name , t1.capacity FROM stadium AS t1 JOIN event AS t2 ON t1.id = t2.stadium_id WHERE t2.name = 'World Junior'	swimming
SELECT name FROM stadium WHERE id NOT IN (SELECT stadium_id FROM event)	swimming
SELECT t1.name FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id GROUP BY t2.swimmer_id ORDER BY count(*) DESC LIMIT 1	swimming
SELECT t1.name FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id GROUP BY t2.swimmer_id HAVING count(*) >= 2	swimming
SELECT t1.name , t1.nationality FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id WHERE RESULT = 'Win' GROUP BY t2.swimmer_id HAVING count(*) > 1	swimming
SELECT name FROM swimmer WHERE id NOT IN (SELECT swimmer_id FROM record)	swimming
SELECT t1.name FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id WHERE RESULT = 'Win' INTERSECT SELECT t1.name FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id WHERE RESULT = 'Loss'	swimming
SELECT t4.name FROM swimmer AS t1 JOIN record AS t2 ON t1.id = t2.swimmer_id JOIN event AS t3 ON t2.event_id = t3.id JOIN stadium AS t4 ON t4.id = t3.stadium_id WHERE t1.nationality = 'Australia'	swimming
SELECT t3.name FROM record AS t1 JOIN event AS t2 ON t1.event_id = t2.id JOIN stadium AS t3 ON t3.id = t2.stadium_id GROUP BY t2.stadium_id ORDER BY count(*) DESC LIMIT 1	swimming
SELECT * FROM swimmer	swimming
SELECT avg(capacity) FROM stadium WHERE opening_year = 2005	swimming
SELECT count(*) FROM railway	railway
SELECT Builder FROM railway ORDER BY Builder ASC	railway
SELECT Wheels , LOCATION FROM railway	railway
SELECT max(LEVEL) FROM manager WHERE Country != "Australia "	railway
SELECT avg(Age) FROM manager	railway
SELECT Name FROM manager ORDER BY LEVEL ASC	railway
SELECT Name , Arrival FROM train	railway
SELECT Name FROM manager ORDER BY Age DESC LIMIT 1	railway
SELECT T2.Name , T1.Location FROM railway AS T1 JOIN train AS T2 ON T1.Railway_ID = T2.Railway_ID	railway
SELECT T1.Builder FROM railway AS T1 JOIN train AS T2 ON T1.Railway_ID = T2.Railway_ID WHERE T2.Name = "Andaman Exp"	railway
SELECT T2.Railway_ID , T1.Location FROM railway AS T1 JOIN train AS T2 ON T1.Railway_ID = T2.Railway_ID GROUP BY T2.Railway_ID HAVING COUNT(*) > 1	railway
SELECT T2.Railway_ID , T1.Builder FROM railway AS T1 JOIN train AS T2 ON T1.Railway_ID = T2.Railway_ID GROUP BY T2.Railway_ID ORDER BY COUNT(*) DESC LIMIT 1	railway
SELECT Builder , COUNT(*) FROM railway GROUP BY Builder	railway
SELECT Builder FROM railway GROUP BY Builder ORDER BY COUNT(*) DESC LIMIT 1	railway
SELECT LOCATION , COUNT(*) FROM railway GROUP BY LOCATION	railway
SELECT LOCATION FROM railway GROUP BY LOCATION HAVING COUNT(*) > 1	railway
SELECT ObjectNumber FROM railway WHERE Railway_ID NOT IN (SELECT Railway_ID FROM train)	railway
SELECT Country FROM manager WHERE Age > 50 INTERSECT SELECT Country FROM manager WHERE Age < 46	railway
SELECT DISTINCT Country FROM manager	railway
SELECT Working_year_starts FROM manager ORDER BY LEVEL DESC	railway
SELECT Country FROM manager WHERE Age > 50 OR Age < 46	railway
SELECT count(*) FROM addresses WHERE country = 'USA'	customers_and_products_contacts
SELECT DISTINCT city FROM addresses	customers_and_products_contacts
SELECT state_province_county , count(*) FROM addresses GROUP BY state_province_county	customers_and_products_contacts
SELECT customer_name , customer_phone FROM customers WHERE customer_id NOT IN (SELECT customer_id FROM customer_address_history)	customers_and_products_contacts
SELECT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	customers_and_products_contacts
SELECT product_type_code FROM products GROUP BY product_type_code HAVING count(*) >= 2	customers_and_products_contacts
SELECT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = 'Completed' INTERSECT SELECT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status_code = 'Part'	customers_and_products_contacts
SELECT customer_name , customer_phone , payment_method_code FROM customers ORDER BY customer_number DESC	customers_and_products_contacts
SELECT T1.product_name , sum(T2.order_quantity) FROM products AS T1 JOIN order_items AS T2 ON T1.product_id = T2.product_id GROUP BY T1.product_id	customers_and_products_contacts
SELECT min(product_price) , max(product_price) , avg(product_price) FROM products	customers_and_products_contacts
SELECT count(*) FROM products WHERE product_price > (SELECT avg(product_price) FROM products)	customers_and_products_contacts
SELECT T2.customer_name , T3.city , T1.date_from , T1.date_to FROM customer_address_history AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id JOIN addresses AS T3 ON T1.address_id = T3.address_id	customers_and_products_contacts
SELECT T1.customer_name FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id WHERE T1.payment_method_code = 'Credit Card' GROUP BY T1.customer_id HAVING count(*) > 2	customers_and_products_contacts
SELECT T1.customer_name , T1.customer_phone FROM customers AS T1 JOIN customer_orders AS T2 ON T1.customer_id = T2.customer_id JOIN order_items AS T3 ON T3.order_id = T2.order_id GROUP BY T1.customer_id ORDER BY sum(T3.order_quantity) DESC LIMIT 1	customers_and_products_contacts
SELECT product_type_code , product_name FROM products WHERE product_price > 1000 OR product_price < 500	customers_and_products_contacts
SELECT dorm_name FROM dorm WHERE gender = 'F'	dorm_1
SELECT dorm_name FROM dorm WHERE gender = 'F'	dorm_1
SELECT dorm_name FROM dorm WHERE student_capacity > 300	dorm_1
SELECT dorm_name FROM dorm WHERE student_capacity > 300	dorm_1
SELECT count(*) FROM student WHERE sex = 'F' AND age < 25	dorm_1
SELECT count(*) FROM student WHERE sex = 'F' AND age < 25	dorm_1
SELECT fname FROM student WHERE age > 20	dorm_1
SELECT fname FROM student WHERE age > 20	dorm_1
SELECT fname FROM student WHERE city_code = 'PHL' AND age BETWEEN 20 AND 25	dorm_1
SELECT fname FROM student WHERE city_code = 'PHL' AND age BETWEEN 20 AND 25	dorm_1
SELECT count(*) FROM dorm	dorm_1
SELECT count(*) FROM dorm	dorm_1
SELECT count(*) FROM dorm_amenity	dorm_1
SELECT count(*) FROM dorm_amenity	dorm_1
SELECT sum(student_capacity) FROM dorm	dorm_1
SELECT sum(student_capacity) FROM dorm	dorm_1
SELECT count(*) FROM student	dorm_1
SELECT count(*) FROM student	dorm_1
SELECT avg(age) , city_code FROM student GROUP BY city_code	dorm_1
SELECT avg(age) , city_code FROM student GROUP BY city_code	dorm_1
SELECT avg(student_capacity) , sum(student_capacity) FROM dorm WHERE gender = 'X'	dorm_1
SELECT avg(student_capacity) , sum(student_capacity) FROM dorm WHERE gender = 'X'	dorm_1
SELECT count(DISTINCT dormid) FROM has_amenity	dorm_1
SELECT count(DISTINCT dormid) FROM has_amenity	dorm_1
SELECT dorm_name FROM dorm WHERE dormid NOT IN (SELECT dormid FROM has_amenity)	dorm_1
SELECT dorm_name FROM dorm WHERE dormid NOT IN (SELECT dormid FROM has_amenity)	dorm_1
SELECT count(DISTINCT gender) FROM dorm	dorm_1
SELECT count(DISTINCT gender) FROM dorm	dorm_1
SELECT student_capacity , gender FROM dorm WHERE dorm_name LIKE '%Donor%'	dorm_1
SELECT student_capacity , gender FROM dorm WHERE dorm_name LIKE '%Donor%'	dorm_1
SELECT dorm_name , gender FROM dorm WHERE student_capacity > 300 OR student_capacity < 100	dorm_1
SELECT dorm_name , gender FROM dorm WHERE student_capacity > 300 OR student_capacity < 100	dorm_1
SELECT count(DISTINCT major) , count(DISTINCT city_code) FROM student	dorm_1
SELECT count(DISTINCT major) , count(DISTINCT city_code) FROM student	dorm_1
SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'TV Lounge' INTERSECT SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'Study Room'	dorm_1
SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'TV Lounge' INTERSECT SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'Study Room'	dorm_1
SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'TV Lounge' EXCEPT SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'Study Room'	dorm_1
SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'TV Lounge' EXCEPT SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'Study Room'	dorm_1
SELECT lname FROM student WHERE sex = 'F' AND city_code = 'BAL' UNION SELECT lname FROM student WHERE sex = 'M' AND age < 20	dorm_1
SELECT lname FROM student WHERE sex = 'F' AND city_code = 'BAL' UNION SELECT lname FROM student WHERE sex = 'M' AND age < 20	dorm_1
SELECT dorm_name FROM dorm ORDER BY student_capacity DESC LIMIT 1	dorm_1
SELECT dorm_name FROM dorm ORDER BY student_capacity DESC LIMIT 1	dorm_1
SELECT amenity_name FROM dorm_amenity ORDER BY amenity_name	dorm_1
SELECT amenity_name FROM dorm_amenity ORDER BY amenity_name	dorm_1
SELECT city_code FROM student GROUP BY city_code ORDER BY count(*) DESC LIMIT 1	dorm_1
SELECT city_code FROM student GROUP BY city_code ORDER BY count(*) DESC LIMIT 1	dorm_1
SELECT fname , lname FROM student WHERE age < (SELECT avg(age) FROM student)	dorm_1
SELECT fname , lname FROM student WHERE age < (SELECT avg(age) FROM student)	dorm_1
SELECT fname , lname FROM student WHERE city_code != 'HKG' ORDER BY age	dorm_1
SELECT fname , lname FROM student WHERE city_code != 'HKG' ORDER BY age	dorm_1
SELECT T1.amenity_name FROM dorm_amenity AS T1 JOIN has_amenity AS T2 ON T2.amenid = T1.amenid JOIN dorm AS T3 ON T2.dormid = T3.dormid WHERE T3.dorm_name = 'Anonymous Donor Hall' ORDER BY T1.amenity_name	dorm_1
SELECT T1.amenity_name FROM dorm_amenity AS T1 JOIN has_amenity AS T2 ON T2.amenid = T1.amenid JOIN dorm AS T3 ON T2.dormid = T3.dormid WHERE T3.dorm_name = 'Anonymous Donor Hall' ORDER BY T1.amenity_name	dorm_1
SELECT count(*) , sum(student_capacity) , gender FROM dorm GROUP BY gender	dorm_1
SELECT count(*) , sum(student_capacity) , gender FROM dorm GROUP BY gender	dorm_1
SELECT avg(age) , max(age) , sex FROM student GROUP BY sex	dorm_1
SELECT avg(age) , max(age) , sex FROM student GROUP BY sex	dorm_1
SELECT count(*) , major FROM student GROUP BY major	dorm_1
SELECT count(*) , major FROM student GROUP BY major	dorm_1
SELECT count(*) , avg(age) , city_code FROM student GROUP BY city_code	dorm_1
SELECT count(*) , avg(age) , city_code FROM student GROUP BY city_code	dorm_1
SELECT count(*) , avg(age) , city_code FROM student WHERE sex = 'M' GROUP BY city_code	dorm_1
SELECT count(*) , avg(age) , city_code FROM student WHERE sex = 'M' GROUP BY city_code	dorm_1
SELECT count(*) , city_code FROM student GROUP BY city_code HAVING count(*) > 1	dorm_1
SELECT count(*) , city_code FROM student GROUP BY city_code HAVING count(*) > 1	dorm_1
SELECT fname , lname FROM student WHERE major != (SELECT major FROM student GROUP BY major ORDER BY count(*) DESC LIMIT 1)	dorm_1
SELECT fname , lname FROM student WHERE major != (SELECT major FROM student GROUP BY major ORDER BY count(*) DESC LIMIT 1)	dorm_1
SELECT count(*) , sex FROM student WHERE age > (SELECT avg(age) FROM student) GROUP BY sex	dorm_1
SELECT count(*) , sex FROM student WHERE age > (SELECT avg(age) FROM student) GROUP BY sex	dorm_1
SELECT avg(T1.age) , T3.dorm_name FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid GROUP BY T3.dorm_name	dorm_1
SELECT avg(T1.age) , T3.dorm_name FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid GROUP BY T3.dorm_name	dorm_1
SELECT count(*) , T1.dormid FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid WHERE T1.student_capacity > 100 GROUP BY T1.dormid	dorm_1
SELECT count(*) , T1.dormid FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid WHERE T1.student_capacity > 100 GROUP BY T1.dormid	dorm_1
SELECT count(*) , T3.dorm_name FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T1.age > 20 GROUP BY T3.dorm_name	dorm_1
SELECT count(*) , T3.dorm_name FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T1.age > 20 GROUP BY T3.dorm_name	dorm_1
SELECT T1.fname FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.dorm_name = 'Smith Hall'	dorm_1
SELECT T1.fname FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.dorm_name = 'Smith Hall'	dorm_1
SELECT avg(T1.age) FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.student_capacity = (SELECT max(student_capacity) FROM dorm)	dorm_1
SELECT avg(T1.age) FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.student_capacity = (SELECT max(student_capacity) FROM dorm)	dorm_1
SELECT count(*) FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.gender = 'M'	dorm_1
SELECT count(*) FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.gender = 'M'	dorm_1
SELECT count(*) FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.dorm_name = 'Smith Hall' AND T1.sex = 'F'	dorm_1
SELECT count(*) FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid JOIN dorm AS T3 ON T3.dormid = T2.dormid WHERE T3.dorm_name = 'Smith Hall' AND T1.sex = 'F'	dorm_1
SELECT T3.amenity_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T1.dorm_name = 'Smith Hall'	dorm_1
SELECT T3.amenity_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T1.dorm_name = 'Smith Hall'	dorm_1
SELECT T3.amenity_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T1.dorm_name = 'Smith Hall' ORDER BY T3.amenity_name	dorm_1
SELECT T3.amenity_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T1.dorm_name = 'Smith Hall' ORDER BY T3.amenity_name	dorm_1
SELECT T1.amenity_name FROM dorm_amenity AS T1 JOIN has_amenity AS T2 ON T1.amenid = T2.amenid GROUP BY T2.amenid ORDER BY count(*) DESC LIMIT 1	dorm_1
SELECT T1.amenity_name FROM dorm_amenity AS T1 JOIN has_amenity AS T2 ON T1.amenid = T2.amenid GROUP BY T2.amenid ORDER BY count(*) DESC LIMIT 1	dorm_1
SELECT T1.fname FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid WHERE T2.dormid IN (SELECT T2.dormid FROM dorm AS T3 JOIN has_amenity AS T4 ON T3.dormid = T4.dormid JOIN dorm_amenity AS T5 ON T4.amenid = T5.amenid GROUP BY T3.dormid ORDER BY count(*) DESC LIMIT 1)	dorm_1
SELECT T1.fname FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid WHERE T2.dormid IN (SELECT T2.dormid FROM dorm AS T3 JOIN has_amenity AS T4 ON T3.dormid = T4.dormid JOIN dorm_amenity AS T5 ON T4.amenid = T5.amenid GROUP BY T3.dormid ORDER BY count(*) DESC LIMIT 1)	dorm_1
SELECT T1.dorm_name , T1.student_capacity FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid GROUP BY T2.dormid ORDER BY count(*) LIMIT 1	dorm_1
SELECT T1.dorm_name , T1.student_capacity FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid GROUP BY T2.dormid ORDER BY count(*) LIMIT 1	dorm_1
SELECT dorm_name FROM dorm EXCEPT SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'TV Lounge'	dorm_1
SELECT dorm_name FROM dorm EXCEPT SELECT T1.dorm_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid WHERE T3.amenity_name = 'TV Lounge'	dorm_1
SELECT T1.fname , T1.lname FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid WHERE T2.dormid IN (SELECT T3.dormid FROM has_amenity AS T3 JOIN dorm_amenity AS T4 ON T3.amenid = T4.amenid WHERE T4.amenity_name = 'TV Lounge')	dorm_1
SELECT T1.fname , T1.lname FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid WHERE T2.dormid IN (SELECT T3.dormid FROM has_amenity AS T3 JOIN dorm_amenity AS T4 ON T3.amenid = T4.amenid WHERE T4.amenity_name = 'TV Lounge')	dorm_1
SELECT T1.fname , T1.age FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid WHERE T2.dormid NOT IN (SELECT T3.dormid FROM has_amenity AS T3 JOIN dorm_amenity AS T4 ON T3.amenid = T4.amenid WHERE T4.amenity_name = 'TV Lounge')	dorm_1
SELECT T1.fname , T1.age FROM student AS T1 JOIN lives_in AS T2 ON T1.stuid = T2.stuid WHERE T2.dormid NOT IN (SELECT T3.dormid FROM has_amenity AS T3 JOIN dorm_amenity AS T4 ON T3.amenid = T4.amenid WHERE T4.amenity_name = 'TV Lounge')	dorm_1
SELECT T3.amenity_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid JOIN lives_in AS T4 ON T4.dormid = T1.dormid JOIN student AS T5 ON T5.stuid = T4.stuid WHERE T5.lname = 'Smith'	dorm_1
SELECT T3.amenity_name FROM dorm AS T1 JOIN has_amenity AS T2 ON T1.dormid = T2.dormid JOIN dorm_amenity AS T3 ON T2.amenid = T3.amenid JOIN lives_in AS T4 ON T4.dormid = T1.dormid JOIN student AS T5 ON T5.stuid = T4.stuid WHERE T5.lname = 'Smith'	dorm_1
SELECT count(*) FROM customers	customer_complaints
SELECT count(*) FROM customers	customer_complaints
SELECT email_address , phone_number FROM customers ORDER BY email_address , phone_number	customer_complaints
SELECT email_address , phone_number FROM customers ORDER BY email_address , phone_number	customer_complaints
SELECT town_city FROM customers WHERE customer_type_code = "Good Credit Rating" GROUP BY town_city ORDER BY count(*) LIMIT 1	customer_complaints
SELECT town_city FROM customers WHERE customer_type_code = "Good Credit Rating" GROUP BY town_city ORDER BY count(*) LIMIT 1	customer_complaints
SELECT t1.product_name , count(*) FROM products AS t1 JOIN complaints AS t2 ON t1.product_id = t2.product_id GROUP BY t1.product_name	customer_complaints
SELECT t1.product_name , count(*) FROM products AS t1 JOIN complaints AS t2 ON t1.product_id = t2.product_id GROUP BY t1.product_name	customer_complaints
SELECT t1.email_address FROM customers AS t1 JOIN complaints AS t2 ON t1.customer_id = t2.customer_id GROUP BY t1.customer_id ORDER BY count(*) LIMIT 1	customer_complaints
SELECT t1.email_address FROM customers AS t1 JOIN complaints AS t2 ON t1.customer_id = t2.customer_id GROUP BY t1.customer_id ORDER BY count(*) LIMIT 1	customer_complaints
SELECT DISTINCT t1.product_name FROM products AS t1 JOIN complaints AS t2 ON t1.product_id = t2.product_id JOIN customers AS t3 GROUP BY t3.customer_id ORDER BY count(*) LIMIT 1	customer_complaints
SELECT DISTINCT t1.product_name FROM products AS t1 JOIN complaints AS t2 ON t1.product_id = t2.product_id JOIN customers AS t3 GROUP BY t3.customer_id ORDER BY count(*) LIMIT 1	customer_complaints
SELECT t1.phone_number FROM customers AS t1 JOIN complaints AS t2 ON t1.customer_id = t2.customer_id ORDER BY t2.date_complaint_raised DESC LIMIT 1	customer_complaints
SELECT t1.phone_number FROM customers AS t1 JOIN complaints AS t2 ON t1.customer_id = t2.customer_id ORDER BY t2.date_complaint_raised DESC LIMIT 1	customer_complaints
SELECT email_address , phone_number FROM customers WHERE customer_id NOT IN (SELECT customer_id FROM complaints)	customer_complaints
SELECT email_address , phone_number FROM customers WHERE customer_id NOT IN (SELECT customer_id FROM complaints)	customer_complaints
SELECT phone_number FROM customers UNION SELECT phone_number FROM staff	customer_complaints
SELECT phone_number FROM customers UNION SELECT phone_number FROM staff	customer_complaints
SELECT product_description FROM products WHERE product_name = "Chocolate"	customer_complaints
SELECT product_description FROM products WHERE product_name = "Chocolate"	customer_complaints
SELECT product_name , product_category_code FROM products ORDER BY product_price DESC LIMIT 1	customer_complaints
SELECT product_name , product_category_code FROM products ORDER BY product_price DESC LIMIT 1	customer_complaints
SELECT product_price FROM products WHERE product_id NOT IN (SELECT product_id FROM complaints)	customer_complaints
SELECT product_price FROM products WHERE product_id NOT IN (SELECT product_id FROM complaints)	customer_complaints
SELECT avg(product_price) , product_category_code FROM products GROUP BY product_category_code	customer_complaints
SELECT avg(product_price) , product_category_code FROM products GROUP BY product_category_code	customer_complaints
SELECT t1.last_name FROM staff AS t1 JOIN complaints AS t2 ON t1.staff_id = t2.staff_id JOIN products AS t3 ON t2.product_id = t3.product_id ORDER BY t3.product_price LIMIT 1	customer_complaints
SELECT t1.last_name FROM staff AS t1 JOIN complaints AS t2 ON t1.staff_id = t2.staff_id JOIN products AS t3 ON t2.product_id = t3.product_id ORDER BY t3.product_price LIMIT 1	customer_complaints
SELECT complaint_status_code FROM complaints GROUP BY complaint_status_code HAVING count(*) > 3	customer_complaints
SELECT complaint_status_code FROM complaints GROUP BY complaint_status_code HAVING count(*) > 3	customer_complaints
SELECT last_name FROM staff WHERE email_address LIKE "%wrau%"	customer_complaints
SELECT last_name FROM staff WHERE email_address LIKE "%wrau%"	customer_complaints
SELECT count(*) FROM customers GROUP BY customer_type_code ORDER BY count(*) DESC LIMIT 1	customer_complaints
SELECT count(*) FROM customers GROUP BY customer_type_code ORDER BY count(*) DESC LIMIT 1	customer_complaints
SELECT t1.last_name FROM staff AS t1 JOIN complaints AS t2 ON t1.staff_id = t2.staff_id ORDER BY t2.date_complaint_raised LIMIT 1	customer_complaints
SELECT t1.last_name FROM staff AS t1 JOIN complaints AS t2 ON t1.staff_id = t2.staff_id ORDER BY t2.date_complaint_raised LIMIT 1	customer_complaints
SELECT count(DISTINCT complaint_type_code) FROM complaints	customer_complaints
SELECT count(DISTINCT complaint_type_code) FROM complaints	customer_complaints
SELECT address_line_1 , address_line_2 FROM customers WHERE email_address = "vbogisich@example.org"	customer_complaints
SELECT address_line_1 , address_line_2 FROM customers WHERE email_address = "vbogisich@example.org"	customer_complaints
SELECT complaint_status_code , count(*) FROM complaints WHERE complaint_type_code = "Product Failure" GROUP BY complaint_status_code	customer_complaints
SELECT complaint_status_code , count(*) FROM complaints WHERE complaint_type_code = "Product Failure" GROUP BY complaint_status_code	customer_complaints
SELECT t1.first_name FROM staff AS t1 JOIN complaints AS t2 ON t1.staff_id = t2.staff_id GROUP BY t2.staff_id ORDER BY count(*) LIMIT 5	customer_complaints
SELECT t1.first_name FROM staff AS t1 JOIN complaints AS t2 ON t1.staff_id = t2.staff_id GROUP BY t2.staff_id ORDER BY count(*) LIMIT 5	customer_complaints
SELECT state FROM customers GROUP BY state ORDER BY count(*) LIMIT 1	customer_complaints
SELECT state FROM customers GROUP BY state ORDER BY count(*) LIMIT 1	customer_complaints
SELECT count(*) FROM submission	workshop_paper
SELECT count(*) FROM submission	workshop_paper
SELECT Author FROM submission ORDER BY Scores ASC	workshop_paper
SELECT Author FROM submission ORDER BY Scores ASC	workshop_paper
SELECT Author , College FROM submission	workshop_paper
SELECT Author , College FROM submission	workshop_paper
SELECT Author FROM submission WHERE College = "Florida" OR College = "Temple"	workshop_paper
SELECT Author FROM submission WHERE College = "Florida" OR College = "Temple"	workshop_paper
SELECT avg(Scores) FROM submission	workshop_paper
SELECT avg(Scores) FROM submission	workshop_paper
SELECT Author FROM submission ORDER BY Scores DESC LIMIT 1	workshop_paper
SELECT Author FROM submission ORDER BY Scores DESC LIMIT 1	workshop_paper
SELECT College , COUNT(*) FROM submission GROUP BY College	workshop_paper
SELECT College , COUNT(*) FROM submission GROUP BY College	workshop_paper
SELECT College FROM submission GROUP BY College ORDER BY COUNT(*) DESC LIMIT 1	workshop_paper
SELECT College FROM submission GROUP BY College ORDER BY COUNT(*) DESC LIMIT 1	workshop_paper
SELECT College FROM submission WHERE Scores > 90 INTERSECT SELECT College FROM submission WHERE Scores < 80	workshop_paper
SELECT College FROM submission WHERE Scores > 90 INTERSECT SELECT College FROM submission WHERE Scores < 80	workshop_paper
SELECT T2.Author , T1.Result FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID	workshop_paper
SELECT T2.Author , T1.Result FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID	workshop_paper
SELECT T1.Result FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID ORDER BY T2.Scores DESC LIMIT 1	workshop_paper
SELECT T1.Result FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID ORDER BY T2.Scores DESC LIMIT 1	workshop_paper
SELECT T2.Author , COUNT(DISTINCT T1.workshop_id) FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID GROUP BY T2.Author	workshop_paper
SELECT T2.Author , COUNT(DISTINCT T1.workshop_id) FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID GROUP BY T2.Author	workshop_paper
SELECT T2.Author FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID GROUP BY T2.Author HAVING COUNT(DISTINCT T1.workshop_id) > 1	workshop_paper
SELECT T2.Author FROM acceptance AS T1 JOIN submission AS T2 ON T1.Submission_ID = T2.Submission_ID GROUP BY T2.Author HAVING COUNT(DISTINCT T1.workshop_id) > 1	workshop_paper
SELECT Date , Venue FROM workshop ORDER BY Venue	workshop_paper
SELECT Date , Venue FROM workshop ORDER BY Venue	workshop_paper
SELECT Author FROM submission WHERE Submission_ID NOT IN (SELECT Submission_ID FROM acceptance)	workshop_paper
SELECT Author FROM submission WHERE Submission_ID NOT IN (SELECT Submission_ID FROM acceptance)	workshop_paper
SELECT count(*) FROM INVESTORS	tracking_share_transactions
SELECT Investor_details FROM INVESTORS	tracking_share_transactions
SELECT DISTINCT lot_details FROM LOTS	tracking_share_transactions
SELECT max(amount_of_transaction) FROM TRANSACTIONS	tracking_share_transactions
SELECT date_of_transaction , share_count FROM TRANSACTIONS	tracking_share_transactions
SELECT sum(share_count) FROM TRANSACTIONS	tracking_share_transactions
SELECT transaction_id FROM TRANSACTIONS WHERE transaction_type_code = 'PUR'	tracking_share_transactions
SELECT date_of_transaction FROM TRANSACTIONS WHERE transaction_type_code = "SALE"	tracking_share_transactions
SELECT avg(amount_of_transaction) FROM TRANSACTIONS WHERE transaction_type_code = "SALE"	tracking_share_transactions
SELECT transaction_type_description FROM Ref_Transaction_Types WHERE transaction_type_code = "PUR"	tracking_share_transactions
SELECT min(amount_of_transaction) FROM TRANSACTIONS WHERE transaction_type_code = "PUR" AND share_count > 50	tracking_share_transactions
SELECT max(share_count) FROM TRANSACTIONS WHERE amount_of_transaction < 10000	tracking_share_transactions
SELECT date_of_transaction FROM TRANSACTIONS WHERE share_count > 100 OR amount_of_transaction > 1000	tracking_share_transactions
SELECT T1.transaction_type_description , T2.date_of_transaction FROM Ref_Transaction_Types AS T1 JOIN TRANSACTIONS AS T2 ON T1.transaction_type_code = T2.transaction_type_code WHERE T2.share_count < 10	tracking_share_transactions
SELECT T1.Investor_details FROM INVESTORS AS T1 JOIN TRANSACTIONS AS T2 ON T1.investor_id = T2.investor_id WHERE T2.share_count > 100	tracking_share_transactions
SELECT COUNT(DISTINCT transaction_type_code) FROM TRANSACTIONS	tracking_share_transactions
SELECT lot_details , investor_id FROM LOTS	tracking_share_transactions
SELECT T2.lot_details FROM INVESTORS AS T1 JOIN LOTS AS T2 ON T1.investor_id = T2.investor_id WHERE T1.Investor_details = "l"	tracking_share_transactions
SELECT T1.purchase_details FROM PURCHASES AS T1 JOIN TRANSACTIONS AS T2 ON T1.purchase_transaction_id = T2.transaction_id WHERE T2.amount_of_transaction > 10000	tracking_share_transactions
SELECT T1.sales_details , T2.date_of_transaction FROM SALES AS T1 JOIN TRANSACTIONS AS T2 ON T1.sales_transaction_id = T2.transaction_id WHERE T2.amount_of_transaction < 3000	tracking_share_transactions
SELECT T1.lot_details FROM LOTS AS T1 JOIN TRANSACTIONS_LOTS AS T2 ON T1.lot_id = T2.transaction_id JOIN TRANSACTIONS AS T3 ON T2.transaction_id = T3.transaction_id WHERE T3.share_count < 50	tracking_share_transactions
SELECT T1.lot_details FROM LOTS AS T1 JOIN TRANSACTIONS_LOTS AS T2 ON T1.lot_id = T2.transaction_id JOIN TRANSACTIONS AS T3 ON T2.transaction_id = T3.transaction_id WHERE T3.share_count > 100 AND T3.transaction_type_code = "PUR"	tracking_share_transactions
SELECT transaction_type_code , avg(amount_of_transaction) FROM TRANSACTIONS GROUP BY transaction_type_code	tracking_share_transactions
SELECT transaction_type_code , max(share_count) , min(share_count) FROM TRANSACTIONS GROUP BY transaction_type_code	tracking_share_transactions
SELECT investor_id , avg(share_count) FROM TRANSACTIONS GROUP BY investor_id	tracking_share_transactions
SELECT investor_id , avg(share_count) FROM TRANSACTIONS GROUP BY investor_id ORDER BY avg(share_count)	tracking_share_transactions
SELECT investor_id , avg(amount_of_transaction) FROM TRANSACTIONS GROUP BY investor_id	tracking_share_transactions
SELECT T2.lot_id , avg(amount_of_transaction) FROM TRANSACTIONS AS T1 JOIN Transactions_Lots AS T2 ON T1.transaction_id = T2.transaction_id GROUP BY T2.lot_id	tracking_share_transactions
SELECT T2.lot_id , avg(amount_of_transaction) FROM TRANSACTIONS AS T1 JOIN Transactions_Lots AS T2 ON T1.transaction_id = T2.transaction_id GROUP BY T2.lot_id ORDER BY avg(amount_of_transaction)	tracking_share_transactions
SELECT investor_id , COUNT(*) FROM TRANSACTIONS WHERE transaction_type_code = "SALE" GROUP BY investor_id	tracking_share_transactions
SELECT investor_id , COUNT(*) FROM TRANSACTIONS GROUP BY investor_id	tracking_share_transactions
SELECT transaction_type_code FROM TRANSACTIONS GROUP BY transaction_type_code ORDER BY COUNT(*) ASC LIMIT 1	tracking_share_transactions
SELECT transaction_type_code FROM TRANSACTIONS GROUP BY transaction_type_code ORDER BY COUNT(*) DESC LIMIT 1	tracking_share_transactions
SELECT T1.transaction_type_description FROM Ref_Transaction_Types AS T1 JOIN TRANSACTIONS AS T2 ON T1.transaction_type_code = T2.transaction_type_code GROUP BY T1.transaction_type_code ORDER BY COUNT(*) DESC LIMIT 1	tracking_share_transactions
SELECT T2.investor_id , T1.Investor_details FROM INVESTORS AS T1 JOIN TRANSACTIONS AS T2 ON T1.investor_id = T2.investor_id GROUP BY T2.investor_id ORDER BY COUNT(*) DESC LIMIT 1	tracking_share_transactions
SELECT T2.investor_id , T1.Investor_details FROM INVESTORS AS T1 JOIN TRANSACTIONS AS T2 ON T1.investor_id = T2.investor_id GROUP BY T2.investor_id ORDER BY COUNT(*) DESC LIMIT 3	tracking_share_transactions
SELECT T2.investor_id FROM INVESTORS AS T1 JOIN TRANSACTIONS AS T2 ON T1.investor_id = T2.investor_id GROUP BY T2.investor_id HAVING COUNT(*) >= 2	tracking_share_transactions
SELECT T2.investor_id , T1.Investor_details FROM INVESTORS AS T1 JOIN TRANSACTIONS AS T2 ON T1.investor_id = T2.investor_id WHERE T2.transaction_type_code = "SALE" GROUP BY T2.investor_id HAVING COUNT(*) >= 2	tracking_share_transactions
SELECT date_of_transaction FROM TRANSACTIONS WHERE share_count >= 100 OR amount_of_transaction >= 100	tracking_share_transactions
SELECT sales_details FROM sales UNION SELECT purchase_details FROM purchases	tracking_share_transactions
SELECT lot_details FROM Lots EXCEPT SELECT T1.lot_details FROM Lots AS T1 JOIN transactions_lots AS T2 ON T1.lot_id = T2.lot_id	tracking_share_transactions
SELECT customer_name FROM customers	customers_and_addresses
SELECT customer_name FROM customers	customers_and_addresses
SELECT count(*) FROM customers	customers_and_addresses
SELECT count(*) FROM customers	customers_and_addresses
SELECT avg(order_quantity) FROM order_items	customers_and_addresses
SELECT avg(order_quantity) FROM order_items	customers_and_addresses
SELECT customer_name FROM customers WHERE payment_method = "Cash"	customers_and_addresses
SELECT customer_name FROM customers WHERE payment_method = "Cash"	customers_and_addresses
SELECT date_became_customer FROM customers WHERE customer_id BETWEEN 10 AND 20	customers_and_addresses
SELECT date_became_customer FROM customers WHERE customer_id BETWEEN 10 AND 20	customers_and_addresses
SELECT payment_method FROM customers GROUP BY payment_method ORDER BY count(*) DESC LIMIT 1	customers_and_addresses
SELECT payment_method FROM customers GROUP BY payment_method ORDER BY count(*) DESC LIMIT 1	customers_and_addresses
SELECT customer_name FROM customers WHERE payment_method = (SELECT payment_method FROM customers GROUP BY payment_method ORDER BY count(*) DESC LIMIT 1)	customers_and_addresses
SELECT customer_name FROM customers WHERE payment_method = (SELECT payment_method FROM customers GROUP BY payment_method ORDER BY count(*) DESC LIMIT 1)	customers_and_addresses
SELECT DISTINCT payment_method FROM customers	customers_and_addresses
SELECT DISTINCT payment_method FROM customers	customers_and_addresses
SELECT DISTINCT product_details FROM products	customers_and_addresses
SELECT DISTINCT product_details FROM products	customers_and_addresses
SELECT customer_name FROM customers WHERE customer_name LIKE "%Alex%"	customers_and_addresses
SELECT customer_name FROM customers WHERE customer_name LIKE "%Alex%"	customers_and_addresses
SELECT product_details FROM products WHERE product_details LIKE "%Latte%" OR product_details LIKE "%Americano%"	customers_and_addresses
SELECT product_details FROM products WHERE product_details LIKE "%Latte%" OR product_details LIKE "%Americano%"	customers_and_addresses
SELECT t3.address_content FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t1.customer_name = "Maudie Kertzmann"	customers_and_addresses
SELECT t3.address_content FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t1.customer_name = "Maudie Kertzmann"	customers_and_addresses
SELECT count(*) FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t3.city = "Lake Geovannyton"	customers_and_addresses
SELECT count(*) FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t3.city = "Lake Geovannyton"	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t3.state_province_county = "Colorado"	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id WHERE t3.state_province_county = "Colorado"	customers_and_addresses
SELECT city FROM addresses WHERE city NOT IN ( SELECT DISTINCT t3.city FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id)	customers_and_addresses
SELECT city FROM addresses WHERE city NOT IN ( SELECT DISTINCT t3.city FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id)	customers_and_addresses
SELECT t3.city FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id GROUP BY t3.city ORDER BY count(*) DESC LIMIT 1	customers_and_addresses
SELECT t3.city FROM customers AS t1 JOIN customer_addresses AS t2 ON t1.customer_id = t2.customer_id JOIN addresses AS t3 ON t2.address_id = t3.address_id GROUP BY t3.city ORDER BY count(*) DESC LIMIT 1	customers_and_addresses
SELECT DISTINCT city FROM addresses	customers_and_addresses
SELECT DISTINCT city FROM addresses	customers_and_addresses
SELECT city FROM addresses WHERE zip_postcode = 255	customers_and_addresses
SELECT city FROM addresses WHERE zip_postcode = 255	customers_and_addresses
SELECT state_province_county , country FROM addresses WHERE zip_postcode LIKE "4%"	customers_and_addresses
SELECT state_province_county , country FROM addresses WHERE zip_postcode LIKE "4%"	customers_and_addresses
SELECT country FROM addresses GROUP BY country HAVING count(address_id) > 4	customers_and_addresses
SELECT country FROM addresses GROUP BY country HAVING count(address_id) > 4	customers_and_addresses
SELECT channel_code FROM customer_contact_channels GROUP BY channel_code HAVING count(customer_id) < 5	customers_and_addresses
SELECT channel_code FROM customer_contact_channels GROUP BY channel_code HAVING count(customer_id) < 5	customers_and_addresses
SELECT DISTINCT channel_code FROM customers AS t1 JOIN customer_contact_channels AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name = "Tillman Ernser"	customers_and_addresses
SELECT DISTINCT channel_code FROM customers AS t1 JOIN customer_contact_channels AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name = "Tillman Ernser"	customers_and_addresses
SELECT max(t2.active_to_date) FROM customers AS t1 JOIN customer_contact_channels AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name = "Tillman Ernser"	customers_and_addresses
SELECT max(t2.active_to_date) FROM customers AS t1 JOIN customer_contact_channels AS t2 ON t1.customer_id = t2.customer_id WHERE t1.customer_name = "Tillman Ernser"	customers_and_addresses
SELECT avg(active_to_date - active_from_date) FROM customer_contact_channels	customers_and_addresses
SELECT avg(active_to_date - active_from_date) FROM customer_contact_channels	customers_and_addresses
SELECT channel_code , contact_number FROM customer_contact_channels WHERE active_to_date - active_from_date = (SELECT active_to_date - active_from_date FROM customer_contact_channels ORDER BY (active_to_date - active_from_date) DESC LIMIT 1)	customers_and_addresses
SELECT channel_code , contact_number FROM customer_contact_channels WHERE active_to_date - active_from_date = (SELECT active_to_date - active_from_date FROM customer_contact_channels ORDER BY (active_to_date - active_from_date) DESC LIMIT 1)	customers_and_addresses
SELECT t1.customer_name , t2.active_from_date FROM customers AS t1 JOIN customer_contact_channels AS t2 ON t1.customer_id = t2.customer_id WHERE t2.channel_code = 'Email'	customers_and_addresses
SELECT t1.customer_name , t2.active_from_date FROM customers AS t1 JOIN customer_contact_channels AS t2 ON t1.customer_id = t2.customer_id WHERE t2.channel_code = 'Email'	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id WHERE t3.order_quantity = ( SELECT max(order_quantity) FROM order_items)	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id WHERE t3.order_quantity = ( SELECT max(order_quantity) FROM order_items)	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id GROUP BY t1.customer_name ORDER BY sum(t3.order_quantity) DESC LIMIT 1	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id GROUP BY t1.customer_name ORDER BY sum(t3.order_quantity) DESC LIMIT 1	customers_and_addresses
SELECT t1.payment_method FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id GROUP BY t1.customer_name ORDER BY sum(t3.order_quantity) LIMIT 1	customers_and_addresses
SELECT t1.payment_method FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id GROUP BY t1.customer_name ORDER BY sum(t3.order_quantity) LIMIT 1	customers_and_addresses
SELECT count(DISTINCT t3.product_id) FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id WHERE t1.customer_name = "Rodrick Heaney"	customers_and_addresses
SELECT count(DISTINCT t3.product_id) FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id WHERE t1.customer_name = "Rodrick Heaney"	customers_and_addresses
SELECT sum(t3.order_quantity) FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id WHERE t1.customer_name = "Rodrick Heaney"	customers_and_addresses
SELECT sum(t3.order_quantity) FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id WHERE t1.customer_name = "Rodrick Heaney"	customers_and_addresses
SELECT count(DISTINCT customer_id) FROM customer_orders WHERE order_status = "Cancelled"	customers_and_addresses
SELECT count(DISTINCT customer_id) FROM customer_orders WHERE order_status = "Cancelled"	customers_and_addresses
SELECT count(*) FROM customer_orders WHERE order_details = "Second time"	customers_and_addresses
SELECT count(*) FROM customer_orders WHERE order_details = "Second time"	customers_and_addresses
SELECT t1.customer_name , t2.order_date FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id WHERE order_status = "Delivered"	customers_and_addresses
SELECT t1.customer_name , t2.order_date FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id WHERE order_status = "Delivered"	customers_and_addresses
SELECT sum(t2.order_quantity) FROM customer_orders AS t1 JOIN order_items AS t2 ON t1.order_id = t2.order_id WHERE t1.order_status = "Cancelled"	customers_and_addresses
SELECT sum(t2.order_quantity) FROM customer_orders AS t1 JOIN order_items AS t2 ON t1.order_id = t2.order_id WHERE t1.order_status = "Cancelled"	customers_and_addresses
SELECT sum(t2.order_quantity) FROM customer_orders AS t1 JOIN order_items AS t2 ON t1.order_id = t2.order_id WHERE t1.order_date < "2018-03-17 07:13:53"	customers_and_addresses
SELECT sum(t2.order_quantity) FROM customer_orders AS t1 JOIN order_items AS t2 ON t1.order_id = t2.order_id WHERE t1.order_date < "2018-03-17 07:13:53"	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id ORDER BY t2.order_date DESC LIMIT 1	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id ORDER BY t2.order_date DESC LIMIT 1	customers_and_addresses
SELECT t2.product_details FROM order_items AS t1 JOIN products AS t2 ON t1.product_id = t2.product_id GROUP BY t1.product_id ORDER BY count(*) DESC LIMIT 1	customers_and_addresses
SELECT t2.product_details FROM order_items AS t1 JOIN products AS t2 ON t1.product_id = t2.product_id GROUP BY t1.product_id ORDER BY count(*) DESC LIMIT 1	customers_and_addresses
SELECT t2.product_details , t2.product_id FROM order_items AS t1 JOIN products AS t2 ON t1.product_id = t2.product_id GROUP BY t1.product_id ORDER BY sum(t1.order_quantity) LIMIT 1	customers_and_addresses
SELECT t2.product_details , t2.product_id FROM order_items AS t1 JOIN products AS t2 ON t1.product_id = t2.product_id GROUP BY t1.product_id ORDER BY sum(t1.order_quantity) LIMIT 1	customers_and_addresses
SELECT address_content FROM addresses WHERE city = "East Julianaside" AND state_province_county = "Texas" UNION SELECT address_content FROM addresses WHERE city = "Gleasonmouth" AND state_province_county = "Arizona"	customers_and_addresses
SELECT address_content FROM addresses WHERE city = "East Julianaside" AND state_province_county = "Texas" UNION SELECT address_content FROM addresses WHERE city = "Gleasonmouth" AND state_province_county = "Arizona"	customers_and_addresses
SELECT customer_name FROM customers WHERE payment_method != 'Cash'	customers_and_addresses
SELECT customer_name FROM customers WHERE payment_method != 'Cash'	customers_and_addresses
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id JOIN products AS t4 ON t3.product_id = t4.product_id WHERE t4.product_details = 'Latte'	customers_and_addresses
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id JOIN products AS t4 ON t3.product_id = t4.product_id WHERE t4.product_details = 'Latte'	customers_and_addresses
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id	customers_and_addresses
SELECT customer_name FROM customers EXCEPT SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id JOIN products AS t4 ON t3.product_id = t4.product_id WHERE t4.product_details = 'Latte' INTERSECT SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id JOIN products AS t4 ON t3.product_id = t4.product_id WHERE t4.product_details = 'Americano'	customers_and_addresses
SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id JOIN products AS t4 ON t3.product_id = t4.product_id WHERE t4.product_details = 'Latte' INTERSECT SELECT t1.customer_name FROM customers AS t1 JOIN customer_orders AS t2 ON t1.customer_id = t2.customer_id JOIN order_items AS t3 ON t2.order_id = t3.order_id JOIN products AS t4 ON t3.product_id = t4.product_id WHERE t4.product_details = 'Americano'	customers_and_addresses
SELECT count(*) FROM artist	music_4
SELECT count(*) FROM artist	music_4
SELECT Age FROM artist	music_4
SELECT Age FROM artist	music_4
SELECT avg(Age) FROM artist	music_4
SELECT avg(Age) FROM artist	music_4
SELECT Famous_Title FROM artist WHERE Artist = "Triumfall"	music_4
SELECT Famous_Title FROM artist WHERE Artist = "Triumfall"	music_4
SELECT distinct(Famous_Release_date) FROM artist	music_4
SELECT distinct(Famous_Release_date) FROM artist	music_4
SELECT Date_of_ceremony , RESULT FROM music_festival	music_4
SELECT Date_of_ceremony , RESULT FROM music_festival	music_4
SELECT Category FROM music_festival WHERE RESULT = "Awarded"	music_4
SELECT Category FROM music_festival WHERE RESULT = "Awarded"	music_4
SELECT max(Weeks_on_Top) , min(Weeks_on_Top) FROM volume	music_4
SELECT max(Weeks_on_Top) , min(Weeks_on_Top) FROM volume	music_4
SELECT Song FROM volume WHERE Weeks_on_Top > 1	music_4
SELECT Song FROM volume WHERE Weeks_on_Top > 1	music_4
SELECT Song FROM volume ORDER BY Song	music_4
SELECT Song FROM volume ORDER BY Song	music_4
SELECT COUNT(DISTINCT Artist_ID) FROM volume	music_4
SELECT COUNT(DISTINCT Artist_ID) FROM volume	music_4
SELECT T1.Date_of_ceremony FROM music_festival AS T1 JOIN volume AS T2 ON T1.Volume = T2.Volume_ID WHERE T2.Weeks_on_Top > 2	music_4
SELECT T1.Date_of_ceremony FROM music_festival AS T1 JOIN volume AS T2 ON T1.Volume = T2.Volume_ID WHERE T2.Weeks_on_Top > 2	music_4
SELECT T2.Song FROM music_festival AS T1 JOIN volume AS T2 ON T1.Volume = T2.Volume_ID WHERE T1.Result = "Nominated"	music_4
SELECT T2.Song FROM music_festival AS T1 JOIN volume AS T2 ON T1.Volume = T2.Volume_ID WHERE T1.Result = "Nominated"	music_4
SELECT T2.Issue_Date FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.Artist = "Gorgoroth"	music_4
SELECT T2.Issue_Date FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.Artist = "Gorgoroth"	music_4
SELECT T2.Song FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.age >= 32	music_4
SELECT T2.Song FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.age >= 32	music_4
SELECT avg(T2.Weeks_on_Top) FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.age <= 25	music_4
SELECT avg(T2.Weeks_on_Top) FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.age <= 25	music_4
SELECT T1.Famous_Title FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T2.Weeks_on_Top > 2	music_4
SELECT T1.Famous_Title FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T2.Weeks_on_Top > 2	music_4
SELECT Famous_Title , Age FROM artist ORDER BY Age DESC	music_4
SELECT Famous_Title , Age FROM artist ORDER BY Age DESC	music_4
SELECT Famous_Release_date FROM artist ORDER BY Age DESC LIMIT 1	music_4
SELECT Famous_Release_date FROM artist ORDER BY Age DESC LIMIT 1	music_4
SELECT Category , COUNT(*) FROM music_festival GROUP BY Category	music_4
SELECT Category , COUNT(*) FROM music_festival GROUP BY Category	music_4
SELECT RESULT FROM music_festival GROUP BY RESULT ORDER BY COUNT(*) DESC LIMIT 1	music_4
SELECT RESULT FROM music_festival GROUP BY RESULT ORDER BY COUNT(*) DESC LIMIT 1	music_4
SELECT Category FROM music_festival GROUP BY Category HAVING COUNT(*) > 1	music_4
SELECT Category FROM music_festival GROUP BY Category HAVING COUNT(*) > 1	music_4
SELECT Song FROM volume ORDER BY Weeks_on_Top DESC LIMIT 1	music_4
SELECT Song FROM volume ORDER BY Weeks_on_Top DESC LIMIT 1	music_4
SELECT Famous_Title FROM artist WHERE Artist_ID NOT IN(SELECT Artist_ID FROM volume)	music_4
SELECT Famous_Title FROM artist WHERE Artist_ID NOT IN(SELECT Artist_ID FROM volume)	music_4
SELECT T1.Famous_Title FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T2.Weeks_on_Top > 2 INTERSECT SELECT T1.Famous_Title FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T2.Weeks_on_Top < 2	music_4
SELECT T1.Famous_Title FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T2.Weeks_on_Top > 2 INTERSECT SELECT T1.Famous_Title FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T2.Weeks_on_Top < 2	music_4
SELECT Date_of_ceremony FROM music_festival WHERE Category = "Best Song" AND RESULT = "Awarded"	music_4
SELECT Date_of_ceremony FROM music_festival WHERE Category = "Best Song" AND RESULT = "Awarded"	music_4
SELECT Issue_Date FROM volume ORDER BY Weeks_on_Top ASC LIMIT 1	music_4
SELECT Issue_Date FROM volume ORDER BY Weeks_on_Top ASC LIMIT 1	music_4
SELECT COUNT(DISTINCT Artist_ID) FROM volume	music_4
SELECT COUNT(DISTINCT Artist_ID) FROM volume	music_4
SELECT RESULT , COUNT(*) FROM music_festival GROUP BY RESULT ORDER BY COUNT(*) DESC	music_4
SELECT RESULT , COUNT(*) FROM music_festival GROUP BY RESULT ORDER BY COUNT(*) DESC	music_4
SELECT Issue_Date FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.age <= 23	music_4
SELECT Issue_Date FROM artist AS T1 JOIN volume AS T2 ON T1.Artist_ID = T2.Artist_ID WHERE T1.age <= 23	music_4
SELECT count(*) FROM roller_coaster	roller_coaster
SELECT Name FROM roller_coaster ORDER BY LENGTH ASC	roller_coaster
SELECT LENGTH , Height FROM roller_coaster	roller_coaster
SELECT Name FROM country WHERE Languages != "German"	roller_coaster
SELECT Status FROM roller_coaster WHERE LENGTH > 3300 OR Height > 100	roller_coaster
SELECT Speed FROM roller_coaster ORDER BY LENGTH DESC LIMIT 1	roller_coaster
SELECT avg(Speed) FROM roller_coaster	roller_coaster
SELECT Status , COUNT(*) FROM roller_coaster GROUP BY Status	roller_coaster
SELECT Status FROM roller_coaster GROUP BY Status ORDER BY COUNT(*) DESC LIMIT 1	roller_coaster
SELECT Status FROM roller_coaster GROUP BY Status HAVING COUNT(*) > 2	roller_coaster
SELECT Park FROM roller_coaster ORDER BY Speed DESC LIMIT 1	roller_coaster
SELECT T2.Name , T1.Name FROM country AS T1 JOIN roller_coaster AS T2 ON T1.Country_ID = T2.Country_ID	roller_coaster
SELECT T1.Name FROM country AS T1 JOIN roller_coaster AS T2 ON T1.Country_ID = T2.Country_ID GROUP BY T1.Name HAVING COUNT(*) > 1	roller_coaster
SELECT T1.Name , T1.population FROM country AS T1 JOIN roller_coaster AS T2 ON T1.Country_ID = T2.Country_ID ORDER BY T2.Height DESC LIMIT 1	roller_coaster
SELECT T1.Name , avg(T2.Speed) FROM country AS T1 JOIN roller_coaster AS T2 ON T1.Country_ID = T2.Country_ID GROUP BY T1.Name	roller_coaster
SELECT count(*) FROM country WHERE country_id NOT IN ( SELECT country_id FROM roller_coaster WHERE LENGTH > 3000 )	roller_coaster
SELECT T1.name , T1.area , T1.population FROM country AS T1 JOIN roller_coaster AS T2 ON T1.Country_ID = T2.Country_ID WHERE T2.speed > 60 INTERSECT SELECT T1.name , T1.area , T1.population FROM country AS T1 JOIN roller_coaster AS T2 ON T1.Country_ID = T2.Country_ID WHERE T2.speed < 55	roller_coaster
SELECT count(*) FROM driver	school_bus
SELECT name , home_city , age FROM driver	school_bus
SELECT party , count(*) FROM driver GROUP BY party	school_bus
SELECT name FROM driver ORDER BY age DESC	school_bus
SELECT DISTINCT home_city FROM driver	school_bus
SELECT home_city FROM driver GROUP BY home_city ORDER BY count(*) DESC LIMIT 1	school_bus
SELECT party FROM driver WHERE home_city = 'Hartford' AND age > 40	school_bus
SELECT home_city FROM driver WHERE age > 40 GROUP BY home_city HAVING count(*) >= 2	school_bus
SELECT home_city FROM driver EXCEPT SELECT home_city FROM driver WHERE age > 40	school_bus
SELECT name FROM driver WHERE driver_id NOT IN (SELECT driver_id FROM school_bus)	school_bus
SELECT TYPE FROM school GROUP BY TYPE HAVING count(*) = 2	school_bus
SELECT T2.school , T3.name FROM school_bus AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id JOIN driver AS T3 ON T1.driver_id = T3.driver_id	school_bus
SELECT max(years_working) , min(years_working) , avg(years_working) FROM school_bus	school_bus
SELECT school , TYPE FROM school WHERE school_id NOT IN (SELECT school_id FROM school_bus)	school_bus
SELECT T2.type , count(*) FROM school_bus AS T1 JOIN school AS T2 ON T1.school_id = T2.school_id GROUP BY T2.type	school_bus
SELECT count(*) FROM driver WHERE home_city = 'Hartford' OR age < 40	school_bus
SELECT name FROM driver WHERE home_city = 'Hartford' AND age < 40	school_bus
SELECT t1.name FROM driver AS t1 JOIN school_bus AS t2 ON t1.driver_id = t2.driver_id ORDER BY years_working DESC LIMIT 1	school_bus
SELECT count(*) FROM flight WHERE velocity > 200	flight_company
SELECT vehicle_flight_number , date , pilot FROM flight ORDER BY altitude ASC	flight_company
SELECT id , country , city , name FROM airport ORDER BY name	flight_company
SELECT max(group_equity_shareholding) FROM operate_company	flight_company
SELECT avg(velocity) FROM flight WHERE pilot = 'Thompson'	flight_company
SELECT T1.name , T1.type FROM operate_company AS T1 JOIN flight AS t2 ON T1.id = T2.company_id	flight_company
SELECT name FROM airport WHERE country != 'Iceland'	flight_company
SELECT DISTINCT T1.type FROM operate_company AS T1 JOIN flight AS t2 ON T1.id = T2.company_id WHERE T2.velocity < 200	flight_company
SELECT T1.id , T1.name FROM operate_company AS T1 JOIN flight AS t2 ON T1.id = T2.company_id GROUP BY T1.id HAVING count(*) > 1	flight_company
SELECT T1.id , T1.name , T1.IATA FROM airport AS T1 JOIN flight AS T2 ON T1.id = T2.airport_id GROUP BY T2.id ORDER BY count(*) DESC LIMIT 1	flight_company
SELECT DISTINCT T2.pilot FROM airport AS T1 JOIN flight AS T2 ON T1.id = T2.airport_id WHERE T1.country = 'United States' OR T1.name = 'Billund Airport'	flight_company
SELECT TYPE , count(*) FROM operate_company GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	flight_company
SELECT count(*) FROM airport WHERE id NOT IN ( SELECT airport_id FROM flight WHERE pilot = 'Thompson' );	flight_company
SELECT T2.pilot FROM operate_company AS T1 JOIN flight AS t2 ON T1.id = T2.company_id WHERE T1.principal_activities = 'Cargo' INTERSECT SELECT T2.pilot FROM operate_company AS T1 JOIN flight AS t2 ON T1.id = T2.company_id WHERE T1.principal_activities = 'Catering services'	flight_company
SELECT name FROM airport WHERE name LIKE '%international%'	flight_company
SELECT T3.id , count(*) FROM operate_company AS T1 JOIN flight AS t2 ON T1.id = T2.company_id JOIN airport AS T3 ON T2.airport_id = T3.id GROUP BY T3.id	flight_company
SELECT count(*) , country FROM airport GROUP BY country	flight_company
SELECT country FROM airport GROUP BY country HAVING count(*) > 2	flight_company
SELECT pilot FROM flight GROUP BY pilot ORDER BY count(*) DESC LIMIT 1	flight_company
SELECT count(*) FROM Accounts	cre_Docs_and_Epenses
SELECT count(*) FROM Accounts	cre_Docs_and_Epenses
SELECT account_id , account_details FROM Accounts	cre_Docs_and_Epenses
SELECT account_id , account_details FROM Accounts	cre_Docs_and_Epenses
SELECT count(*) FROM Statements	cre_Docs_and_Epenses
SELECT count(*) FROM Statements	cre_Docs_and_Epenses
SELECT STATEMENT_ID , statement_details FROM Statements	cre_Docs_and_Epenses
SELECT STATEMENT_ID , statement_details FROM Statements	cre_Docs_and_Epenses
SELECT T1.statement_id , T2.statement_details , T1.account_details FROM Accounts AS T1 JOIN Statements AS T2 ON T1.statement_id = T2.statement_id	cre_Docs_and_Epenses
SELECT T1.statement_id , T2.statement_details , T1.account_details FROM Accounts AS T1 JOIN Statements AS T2 ON T1.statement_id = T2.statement_id	cre_Docs_and_Epenses
SELECT STATEMENT_ID , count(*) FROM Accounts GROUP BY STATEMENT_ID	cre_Docs_and_Epenses
SELECT STATEMENT_ID , count(*) FROM Accounts GROUP BY STATEMENT_ID	cre_Docs_and_Epenses
SELECT T1.statement_id , T2.statement_details FROM Accounts AS T1 JOIN Statements AS T2 ON T1.statement_id = T2.statement_id GROUP BY T1.statement_id ORDER BY count(*) DESC LIMIT 1	cre_Docs_and_Epenses
SELECT T1.statement_id , T2.statement_details FROM Accounts AS T1 JOIN Statements AS T2 ON T1.statement_id = T2.statement_id GROUP BY T1.statement_id ORDER BY count(*) DESC LIMIT 1	cre_Docs_and_Epenses
SELECT count(*) FROM Documents	cre_Docs_and_Epenses
SELECT count(*) FROM Documents	cre_Docs_and_Epenses
SELECT document_type_code , document_name , document_description FROM Documents WHERE document_name = 'Noel CV' OR document_name = 'King Book'	cre_Docs_and_Epenses
SELECT document_type_code , document_name , document_description FROM Documents WHERE document_name = 'Noel CV' OR document_name = 'King Book'	cre_Docs_and_Epenses
SELECT document_id , document_name FROM Documents	cre_Docs_and_Epenses
SELECT document_id , document_name FROM Documents	cre_Docs_and_Epenses
SELECT document_name , document_id FROM Documents WHERE document_type_code = "BK"	cre_Docs_and_Epenses
SELECT document_name , document_id FROM Documents WHERE document_type_code = "BK"	cre_Docs_and_Epenses
SELECT count(*) , project_id FROM Documents WHERE document_type_code = "BK" GROUP BY project_id	cre_Docs_and_Epenses
SELECT count(*) , project_id FROM Documents WHERE document_type_code = "BK" GROUP BY project_id	cre_Docs_and_Epenses
SELECT document_name , document_date FROM Documents AS T1 JOIN projects AS T2 ON T1.project_id = T2.project_id WHERE T2.project_details = 'Graph Database project'	cre_Docs_and_Epenses
SELECT document_name , document_date FROM Documents AS T1 JOIN projects AS T2 ON T1.project_id = T2.project_id WHERE T2.project_details = 'Graph Database project'	cre_Docs_and_Epenses
SELECT project_id , count(*) FROM Documents GROUP BY project_id	cre_Docs_and_Epenses
SELECT project_id , count(*) FROM Documents GROUP BY project_id	cre_Docs_and_Epenses
SELECT project_id FROM Documents GROUP BY project_id ORDER BY count(*) ASC LIMIT 1	cre_Docs_and_Epenses
SELECT project_id FROM Documents GROUP BY project_id ORDER BY count(*) ASC LIMIT 1	cre_Docs_and_Epenses
SELECT project_id FROM Documents GROUP BY project_id HAVING count(*) >= 2	cre_Docs_and_Epenses
SELECT project_id FROM Documents GROUP BY project_id HAVING count(*) >= 2	cre_Docs_and_Epenses
SELECT document_type_code , count(*) FROM Documents GROUP BY document_type_code	cre_Docs_and_Epenses
SELECT document_type_code , count(*) FROM Documents GROUP BY document_type_code	cre_Docs_and_Epenses
SELECT document_type_code FROM Documents GROUP BY document_type_code ORDER BY count(*) DESC LIMIT 1	cre_Docs_and_Epenses
SELECT document_type_code FROM Documents GROUP BY document_type_code ORDER BY count(*) DESC LIMIT 1	cre_Docs_and_Epenses
SELECT document_type_code FROM Documents GROUP BY document_type_code HAVING count(*) < 3	cre_Docs_and_Epenses
SELECT document_type_code FROM Documents GROUP BY document_type_code HAVING count(*) < 3	cre_Docs_and_Epenses
SELECT T1.statement_details , T2.document_name FROM Statements AS T1 JOIN Documents AS T2 ON T1.statement_id = T2.document_id WHERE T1.statement_details = 'Private Project'	cre_Docs_and_Epenses
SELECT T1.statement_details , T2.document_name FROM Statements AS T1 JOIN Documents AS T2 ON T1.statement_id = T2.document_id WHERE T1.statement_details = 'Private Project'	cre_Docs_and_Epenses
SELECT document_type_code , document_type_name , document_type_description FROM Ref_document_types	cre_Docs_and_Epenses
SELECT document_type_code , document_type_name , document_type_description FROM Ref_document_types	cre_Docs_and_Epenses
SELECT document_type_description FROM Ref_document_types WHERE document_type_name = "Film"	cre_Docs_and_Epenses
SELECT document_type_description FROM Ref_document_types WHERE document_type_name = "Film"	cre_Docs_and_Epenses
SELECT T1.document_type_name , T1.document_type_description , T2.Document_date FROM Ref_document_types AS T1 JOIN Documents AS T2 ON T1.document_type_code = T2.document_type_code	cre_Docs_and_Epenses
SELECT T1.document_type_name , T1.document_type_description , T2.Document_date FROM Ref_document_types AS T1 JOIN Documents AS T2 ON T1.document_type_code = T2.document_type_code	cre_Docs_and_Epenses
SELECT count(*) FROM Projects	cre_Docs_and_Epenses
SELECT count(*) FROM Projects	cre_Docs_and_Epenses
SELECT project_id , project_details FROM Projects	cre_Docs_and_Epenses
SELECT project_id , project_details FROM Projects	cre_Docs_and_Epenses
SELECT T1.project_id , T1.project_details FROM Projects AS T1 JOIN Documents AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id HAVING count(*) > 2	cre_Docs_and_Epenses
SELECT T1.project_id , T1.project_details FROM Projects AS T1 JOIN Documents AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id HAVING count(*) > 2	cre_Docs_and_Epenses
SELECT T1.project_details FROM Projects AS T1 JOIN Documents AS T2 ON T1.project_id = T2.project_id WHERE T2.document_name = "King Book"	cre_Docs_and_Epenses
SELECT T1.project_details FROM Projects AS T1 JOIN Documents AS T2 ON T1.project_id = T2.project_id WHERE T2.document_name = "King Book"	cre_Docs_and_Epenses
SELECT count(*) FROM Ref_budget_codes	cre_Docs_and_Epenses
SELECT count(*) FROM Ref_budget_codes	cre_Docs_and_Epenses
SELECT budget_type_code , budget_type_description FROM Ref_budget_codes	cre_Docs_and_Epenses
SELECT budget_type_code , budget_type_description FROM Ref_budget_codes	cre_Docs_and_Epenses
SELECT budget_type_description FROM Ref_budget_codes WHERE budget_type_code = "ORG"	cre_Docs_and_Epenses
SELECT budget_type_description FROM Ref_budget_codes WHERE budget_type_code = "ORG"	cre_Docs_and_Epenses
SELECT count(*) FROM Documents_with_expenses	cre_Docs_and_Epenses
SELECT count(*) FROM Documents_with_expenses	cre_Docs_and_Epenses
SELECT document_id FROM Documents_with_expenses WHERE budget_type_code = 'SF'	cre_Docs_and_Epenses
SELECT document_id FROM Documents_with_expenses WHERE budget_type_code = 'SF'	cre_Docs_and_Epenses
SELECT T2.budget_type_code , T2.budget_type_description , T1.document_id FROM Documents_with_expenses AS T1 JOIN Ref_budget_codes AS T2 ON T1.budget_type_code = T2.budget_type_code	cre_Docs_and_Epenses
SELECT T2.budget_type_code , T2.budget_type_description , T1.document_id FROM Documents_with_expenses AS T1 JOIN Ref_budget_codes AS T2 ON T1.budget_type_code = T2.budget_type_code	cre_Docs_and_Epenses
SELECT T1.document_id FROM Documents_with_expenses AS T1 JOIN Ref_Budget_Codes AS T2 ON T1.Budget_Type_code = T2.Budget_Type_code WHERE T2.budget_type_Description = "Government"	cre_Docs_and_Epenses
SELECT T1.document_id FROM Documents_with_expenses AS T1 JOIN Ref_Budget_Codes AS T2 ON T1.Budget_Type_code = T2.Budget_Type_code WHERE T2.budget_type_Description = "Government"	cre_Docs_and_Epenses
SELECT budget_type_code , count(*) FROM Documents_with_expenses GROUP BY budget_type_code	cre_Docs_and_Epenses
SELECT budget_type_code , count(*) FROM Documents_with_expenses GROUP BY budget_type_code	cre_Docs_and_Epenses
SELECT budget_type_code FROM Documents_with_expenses GROUP BY budget_type_code ORDER BY count(*) DESC LIMIT 1	cre_Docs_and_Epenses
SELECT budget_type_code FROM Documents_with_expenses GROUP BY budget_type_code ORDER BY count(*) DESC LIMIT 1	cre_Docs_and_Epenses
SELECT document_id FROM Documents EXCEPT SELECT document_id FROM Documents_with_expenses	cre_Docs_and_Epenses
SELECT document_id FROM Documents EXCEPT SELECT document_id FROM Documents_with_expenses	cre_Docs_and_Epenses
SELECT document_id FROM Documents WHERE document_type_code = "CV" EXCEPT SELECT document_id FROM Documents_with_expenses	cre_Docs_and_Epenses
SELECT document_id FROM Documents WHERE document_type_code = "CV" EXCEPT SELECT document_id FROM Documents_with_expenses	cre_Docs_and_Epenses
SELECT T1.document_id FROM Documents AS T1 JOIN Documents_with_expenses AS T2 ON T1.document_id = T2.document_id WHERE T1.document_name LIKE '%s%'	cre_Docs_and_Epenses
SELECT T1.document_id FROM Documents AS T1 JOIN Documents_with_expenses AS T2 ON T1.document_id = T2.document_id WHERE T1.document_name LIKE '%s%'	cre_Docs_and_Epenses
SELECT count(*) FROM Documents WHERE document_id NOT IN ( SELECT document_id FROM Documents_with_expenses )	cre_Docs_and_Epenses
SELECT count(*) FROM Documents WHERE document_id NOT IN ( SELECT document_id FROM Documents_with_expenses )	cre_Docs_and_Epenses
SELECT T1.document_date FROM Documents AS T1 JOIN Documents_with_Expenses AS T2 ON T1.document_id = T2.document_id WHERE T2.budget_type_code = 'GV' INTERSECT SELECT T1.document_date FROM Documents AS T1 JOIN Documents_with_Expenses AS T2 ON T1.document_id = T2.document_id WHERE T2.budget_type_code = 'SF'	cre_Docs_and_Epenses
SELECT T1.document_date FROM Documents AS T1 JOIN Documents_with_Expenses AS T2 ON T1.document_id = T2.document_id WHERE T2.budget_type_code = 'GV' INTERSECT SELECT T1.document_date FROM Documents AS T1 JOIN Documents_with_Expenses AS T2 ON T1.document_id = T2.document_id WHERE T2.budget_type_code = 'SF'	cre_Docs_and_Epenses
SELECT max(Account_details) FROM Accounts UNION SELECT Account_details FROM Accounts WHERE Account_details LIKE "%5%"	cre_Docs_and_Epenses
SELECT max(Account_details) FROM Accounts UNION SELECT Account_details FROM Accounts WHERE Account_details LIKE "%5%"	cre_Docs_and_Epenses
SELECT count(*) FROM scientists	scientist_1
SELECT count(*) FROM scientists	scientist_1
SELECT sum(hours) FROM projects	scientist_1
SELECT sum(hours) FROM projects	scientist_1
SELECT count(DISTINCT scientist) FROM assignedto	scientist_1
SELECT count(DISTINCT scientist) FROM assignedto	scientist_1
SELECT count(DISTINCT name) FROM projects	scientist_1
SELECT count(DISTINCT name) FROM projects	scientist_1
SELECT avg(hours) FROM projects	scientist_1
SELECT avg(hours) FROM projects	scientist_1
SELECT name FROM projects ORDER BY hours DESC LIMIT 1	scientist_1
SELECT name FROM projects ORDER BY hours DESC LIMIT 1	scientist_1
SELECT name FROM projects WHERE hours > (SELECT avg(hours) FROM projects)	scientist_1
SELECT name FROM projects WHERE hours > (SELECT avg(hours) FROM projects)	scientist_1
SELECT T1.name , T1.hours FROM projects AS T1 JOIN assignedto AS T2 ON T1.code = T2.project GROUP BY T2.project ORDER BY count(*) DESC LIMIT 1	scientist_1
SELECT T1.name , T1.hours FROM projects AS T1 JOIN assignedto AS T2 ON T1.code = T2.project GROUP BY T2.project ORDER BY count(*) DESC LIMIT 1	scientist_1
SELECT T2.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T3.name LIKE '%Smith%'	scientist_1
SELECT T2.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T3.name LIKE '%Smith%'	scientist_1
SELECT sum(T2.hours) FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T3.name = 'Michael Rogers' OR T3.name = 'Carol Smith'	scientist_1
SELECT sum(T2.hours) FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T3.name = 'Michael Rogers' OR T3.name = 'Carol Smith'	scientist_1
SELECT name FROM projects WHERE hours BETWEEN 100 AND 300	scientist_1
SELECT name FROM projects WHERE hours BETWEEN 100 AND 300	scientist_1
SELECT T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.name = 'Matter of Time' INTERSECT SELECT T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.name = 'A Puzzling Parallax'	scientist_1
SELECT T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.name = 'Matter of Time' INTERSECT SELECT T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.name = 'A Puzzling Parallax'	scientist_1
SELECT name FROM scientists ORDER BY name	scientist_1
SELECT name FROM scientists ORDER BY name	scientist_1
SELECT count(*) , T1.name FROM projects AS T1 JOIN assignedto AS T2 ON T1.code = T2.project GROUP BY T1.name	scientist_1
SELECT count(*) , T1.name FROM projects AS T1 JOIN assignedto AS T2 ON T1.code = T2.project GROUP BY T1.name	scientist_1
SELECT count(*) , T1.name FROM projects AS T1 JOIN assignedto AS T2 ON T1.code = T2.project WHERE T1.hours > 300 GROUP BY T1.name	scientist_1
SELECT count(*) , T1.name FROM projects AS T1 JOIN assignedto AS T2 ON T1.code = T2.project WHERE T1.hours > 300 GROUP BY T1.name	scientist_1
SELECT count(*) , T1.name FROM scientists AS T1 JOIN assignedto AS T2 ON T1.ssn = T2.scientist GROUP BY T1.name	scientist_1
SELECT count(*) , T1.name FROM scientists AS T1 JOIN assignedto AS T2 ON T1.ssn = T2.scientist GROUP BY T1.name	scientist_1
SELECT T3.ssn , T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.hours = (SELECT max(hours) FROM projects)	scientist_1
SELECT T3.ssn , T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.hours = (SELECT max(hours) FROM projects)	scientist_1
SELECT T2.name FROM assignedto AS T1 JOIN scientists AS T2 ON T1.scientist = T2.ssn	scientist_1
SELECT T2.name FROM assignedto AS T1 JOIN scientists AS T2 ON T1.scientist = T2.ssn	scientist_1
SELECT Name FROM Projects WHERE Code NOT IN (SELECT Project FROM AssignedTo)	scientist_1
SELECT Name FROM Projects WHERE Code NOT IN (SELECT Project FROM AssignedTo)	scientist_1
SELECT Name FROM scientists WHERE ssn NOT IN (SELECT scientist FROM AssignedTo)	scientist_1
SELECT Name FROM scientists WHERE ssn NOT IN (SELECT scientist FROM AssignedTo)	scientist_1
SELECT count(*) FROM scientists WHERE ssn NOT IN (SELECT scientist FROM AssignedTo)	scientist_1
SELECT count(*) FROM scientists WHERE ssn NOT IN (SELECT scientist FROM AssignedTo)	scientist_1
SELECT name FROM scientists EXCEPT SELECT T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.hours = (SELECT max(hours) FROM projects)	scientist_1
SELECT name FROM scientists EXCEPT SELECT T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.hours = (SELECT max(hours) FROM projects)	scientist_1
SELECT T1.Name , T3.Name , T3.Hours FROM Scientists AS T1 JOIN AssignedTo AS T2 ON T1.SSN = T2.Scientist JOIN Projects AS T3 ON T2.Project = T3.Code ORDER BY T3.Name , T1.Name	scientist_1
SELECT T1.Name , T3.Name , T3.Hours FROM Scientists AS T1 JOIN AssignedTo AS T2 ON T1.SSN = T2.Scientist JOIN Projects AS T3 ON T2.Project = T3.Code ORDER BY T3.Name , T1.Name	scientist_1
SELECT T2.name , T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.hours = (SELECT min(hours) FROM projects)	scientist_1
SELECT T2.name , T3.name FROM assignedto AS T1 JOIN projects AS T2 ON T1.project = T2.code JOIN scientists AS T3 ON T1.scientist = T3.SSN WHERE T2.hours = (SELECT min(hours) FROM projects)	scientist_1
SELECT Name FROM WINE ORDER BY Score LIMIT 1	wine_1
SELECT Name FROM WINE ORDER BY Score LIMIT 1	wine_1
SELECT Winery FROM WINE ORDER BY SCORE LIMIT 1	wine_1
SELECT Winery FROM WINE ORDER BY SCORE LIMIT 1	wine_1
SELECT Name FROM WINE WHERE YEAR = "2008"	wine_1
SELECT Name FROM WINE WHERE YEAR = "2008"	wine_1
SELECT Grape , Appelation FROM WINE	wine_1
SELECT Grape , Appelation FROM WINE	wine_1
SELECT Name , Score FROM WINE	wine_1
SELECT Name , Score FROM WINE	wine_1
SELECT Area , County FROM APPELLATIONS	wine_1
SELECT Area , County FROM APPELLATIONS	wine_1
SELECT Price FROM WINE WHERE YEAR < 2010	wine_1
SELECT Price FROM WINE WHERE YEAR < 2010	wine_1
SELECT Name FROM WINE WHERE score > 90	wine_1
SELECT Name FROM WINE WHERE score > 90	wine_1
SELECT DISTINCT T2.Name FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "Red"	wine_1
SELECT DISTINCT T2.Name FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "Red"	wine_1
SELECT DISTINCT T2.Name FROM APPELLATIONs AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.Area = "North Coast"	wine_1
SELECT DISTINCT T2.Name FROM APPELLATIONs AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.Area = "North Coast"	wine_1
SELECT count(*) FROM WINE WHERE Winery = "Robert Biale"	wine_1
SELECT count(*) FROM WINE WHERE Winery = "Robert Biale"	wine_1
SELECT count(*) FROM APPELLATIONS WHERE County = "Napa"	wine_1
SELECT count(*) FROM APPELLATIONS WHERE County = "Napa"	wine_1
SELECT AVG(T2.Price) FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.County = "Sonoma"	wine_1
SELECT AVG(T2.Price) FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.County = "Sonoma"	wine_1
SELECT T2.Name , T2.Score FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "White"	wine_1
SELECT T2.Name , T2.Score FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "White"	wine_1
SELECT max(T2.Price) FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.Area = "Central Coast" AND T2.year < 2005	wine_1
SELECT max(T2.Price) FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.Area = "Central Coast" AND T2.year < 2005	wine_1
SELECT DISTINCT T1.Grape FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "White" AND T2.score > 90	wine_1
SELECT DISTINCT T1.Grape FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "White" AND T2.score > 90	wine_1
SELECT T2.Name FROM Grapes AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "Red" AND T2.price > 50	wine_1
SELECT T2.Name FROM Grapes AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape WHERE T1.Color = "Red" AND T2.price > 50	wine_1
SELECT T2.Name FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.County = "Monterey" AND T2.price < 50	wine_1
SELECT T2.Name FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.County = "Monterey" AND T2.price < 50	wine_1
SELECT count(*) , Grape FROM WINE GROUP BY Grape	wine_1
SELECT count(*) , Grape FROM WINE GROUP BY Grape	wine_1
SELECT avg(Price) , YEAR FROM WINE GROUP BY YEAR	wine_1
SELECT avg(Price) , YEAR FROM WINE GROUP BY YEAR	wine_1
SELECT DISTINCT Name FROM WINE WHERE Price > (SELECT min(Price) FROM wine WHERE Winery = "John Anthony")	wine_1
SELECT DISTINCT Name FROM WINE WHERE Price > (SELECT min(Price) FROM wine WHERE Winery = "John Anthony")	wine_1
SELECT DISTINCT Name FROM WINE ORDER BY Name	wine_1
SELECT DISTINCT Name FROM WINE ORDER BY Name	wine_1
SELECT DISTINCT Name FROM WINE ORDER BY price	wine_1
SELECT DISTINCT Name FROM WINE ORDER BY price	wine_1
SELECT T1.Area FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation GROUP BY T2.Appelation HAVING T2.year < 2010 ORDER BY count(*) DESC LIMIT 1	wine_1
SELECT T1.Area FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation GROUP BY T2.Appelation HAVING T2.year < 2010 ORDER BY count(*) DESC LIMIT 1	wine_1
SELECT T1.Color FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape GROUP BY T2.Grape ORDER BY AVG(Price) DESC LIMIT 1	wine_1
SELECT T1.Color FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.Grape = T2.Grape GROUP BY T2.Grape ORDER BY AVG(Price) DESC LIMIT 1	wine_1
SELECT DISTINCT Name FROM WINE WHERE YEAR < 2000 OR YEAR > 2010	wine_1
SELECT DISTINCT Name FROM WINE WHERE YEAR < 2000 OR YEAR > 2010	wine_1
SELECT DISTINCT Winery FROM WINE WHERE Price BETWEEN 50 AND 100	wine_1
SELECT DISTINCT Winery FROM WINE WHERE Price BETWEEN 50 AND 100	wine_1
SELECT AVG(Price) , AVG(Cases) FROM WINE WHERE YEAR = 2009 AND Grape = "Zinfandel"	wine_1
SELECT AVG(Price) , AVG(Cases) FROM WINE WHERE YEAR = 2009 AND Grape = "Zinfandel"	wine_1
SELECT max(Price) , max(Score) FROM WINE WHERE Appelation = "St. Helena"	wine_1
SELECT max(Price) , max(Score) FROM WINE WHERE Appelation = "St. Helena"	wine_1
SELECT max(Price) , max(Score) , YEAR FROM WINE GROUP BY YEAR	wine_1
SELECT max(Price) , max(Score) , YEAR FROM WINE GROUP BY YEAR	wine_1
SELECT avg(Price) , avg(Score) , Appelation FROM WINE GROUP BY Appelation	wine_1
SELECT avg(Price) , avg(Score) , Appelation FROM WINE GROUP BY Appelation	wine_1
SELECT Winery FROM WINE GROUP BY Winery HAVING count(*) >= 4	wine_1
SELECT Winery FROM WINE GROUP BY Winery HAVING count(*) >= 4	wine_1
SELECT T1.County FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation GROUP BY T2.Appelation HAVING count(*) <= 3	wine_1
SELECT T1.County FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation GROUP BY T2.Appelation HAVING count(*) <= 3	wine_1
SELECT Name FROM WINE WHERE YEAR < (SELECT min(YEAR) FROM WINE WHERE Winery = "Brander")	wine_1
SELECT Name FROM WINE WHERE YEAR < (SELECT min(YEAR) FROM WINE WHERE Winery = "Brander")	wine_1
SELECT Name FROM WINE WHERE Price > (SELECT max(Price) FROM WINE WHERE YEAR = 2006)	wine_1
SELECT Name FROM WINE WHERE Price > (SELECT max(Price) FROM WINE WHERE YEAR = 2006)	wine_1
SELECT T2.Winery FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.GRAPE = T2.GRAPE WHERE T1.Color = "White" GROUP BY T2.Winery ORDER BY count(*) DESC LIMIT 3	wine_1
SELECT T2.Winery FROM GRAPES AS T1 JOIN WINE AS T2 ON T1.GRAPE = T2.GRAPE WHERE T1.Color = "White" GROUP BY T2.Winery ORDER BY count(*) DESC LIMIT 3	wine_1
SELECT Grape , Winery , YEAR FROM WINE WHERE Price > 100 ORDER BY YEAR	wine_1
SELECT Grape , Winery , YEAR FROM WINE WHERE Price > 100 ORDER BY YEAR	wine_1
SELECT Grape , Appelation , Name FROM WINE WHERE Score > 93 ORDER BY Name	wine_1
SELECT Grape , Appelation , Name FROM WINE WHERE Score > 93 ORDER BY Name	wine_1
SELECT Appelation FROM WINE WHERE YEAR > 2008 EXCEPT SELECT Appelation FROM APPELLATIONS WHERE Area = "Central Coast"	wine_1
SELECT Appelation FROM WINE WHERE YEAR > 2008 EXCEPT SELECT Appelation FROM APPELLATIONS WHERE Area = "Central Coast"	wine_1
SELECT avg(price) FROM wine WHERE Appelation NOT IN (SELECT T1.Appelation FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.County = 'Sonoma')	wine_1
SELECT avg(price) FROM wine WHERE Appelation NOT IN (SELECT T1.Appelation FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T1.County = 'Sonoma')	wine_1
SELECT T1.County FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T2.Score > 90 GROUP BY T1.County ORDER BY count(*) DESC LIMIT 1	wine_1
SELECT T1.County FROM APPELLATIONS AS T1 JOIN WINE AS T2 ON T1.Appelation = T2.Appelation WHERE T2.Score > 90 GROUP BY T1.County ORDER BY count(*) DESC LIMIT 1	wine_1
SELECT vehicle_id FROM Vehicles;	driving_school
SELECT vehicle_id FROM Vehicles;	driving_school
SELECT count(*) FROM Vehicles;	driving_school
SELECT count(*) FROM Vehicles;	driving_school
SELECT vehicle_details FROM Vehicles WHERE vehicle_id = 1;	driving_school
SELECT vehicle_details FROM Vehicles WHERE vehicle_id = 1;	driving_school
SELECT first_name , middle_name , last_name FROM Staff;	driving_school
SELECT first_name , middle_name , last_name FROM Staff;	driving_school
SELECT date_of_birth FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT date_of_birth FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT date_joined_staff FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT date_joined_staff FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT date_left_staff FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT date_left_staff FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT count(*) FROM Staff WHERE first_name = "Ludie";	driving_school
SELECT count(*) FROM Staff WHERE first_name = "Ludie";	driving_school
SELECT nickname FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT nickname FROM Staff WHERE first_name = "Janessa" AND last_name = "Sawayn";	driving_school
SELECT count(*) FROM Staff;	driving_school
SELECT count(*) FROM Staff;	driving_school
SELECT T1.city FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT T1.city FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT T1.country , T1.state_province_county FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT T1.country , T1.state_province_county FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT sum(T1.lesson_time) FROM Lessons AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Rylan" AND T2.last_name = "Goodwin";	driving_school
SELECT sum(T1.lesson_time) FROM Lessons AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Rylan" AND T2.last_name = "Goodwin";	driving_school
SELECT T1.zip_postcode FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT T1.zip_postcode FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT count(*) FROM Addresses WHERE state_province_county = "Georgia";	driving_school
SELECT count(*) FROM Addresses WHERE state_province_county = "Georgia";	driving_school
SELECT T2.first_name , T2.last_name FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T1.city = "Damianfort";	driving_school
SELECT T2.first_name , T2.last_name FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id WHERE T1.city = "Damianfort";	driving_school
SELECT T1.city , count(*) FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id GROUP BY T1.city ORDER BY count(*) DESC LIMIT 1;	driving_school
SELECT T1.city , count(*) FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id GROUP BY T1.city ORDER BY count(*) DESC LIMIT 1;	driving_school
SELECT T1.state_province_county FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id GROUP BY T1.state_province_county HAVING count(*) BETWEEN 2 AND 4;	driving_school
SELECT T1.state_province_county FROM Addresses AS T1 JOIN Staff AS T2 ON T1.address_id = T2.staff_address_id GROUP BY T1.state_province_county HAVING count(*) BETWEEN 2 AND 4;	driving_school
SELECT first_name , last_name FROM Customers;	driving_school
SELECT first_name , last_name FROM Customers;	driving_school
SELECT email_address , date_of_birth FROM Customers WHERE first_name = "Carole"	driving_school
SELECT email_address , date_of_birth FROM Customers WHERE first_name = "Carole"	driving_school
SELECT phone_number , email_address FROM Customers WHERE amount_outstanding > 2000;	driving_school
SELECT phone_number , email_address FROM Customers WHERE amount_outstanding > 2000;	driving_school
SELECT customer_status_code , cell_mobile_phone_number , email_address FROM Customers WHERE first_name = "Marina" OR last_name = "Kohler"	driving_school
SELECT customer_status_code , cell_mobile_phone_number , email_address FROM Customers WHERE first_name = "Marina" OR last_name = "Kohler"	driving_school
SELECT date_of_birth FROM Customers WHERE customer_status_code = 'Good Customer'	driving_school
SELECT date_of_birth FROM Customers WHERE customer_status_code = 'Good Customer'	driving_school
SELECT date_became_customer FROM Customers WHERE first_name = "Carole" AND last_name = "Bernhard";	driving_school
SELECT date_became_customer FROM Customers WHERE first_name = "Carole" AND last_name = "Bernhard";	driving_school
SELECT count(*) FROM Customers;	driving_school
SELECT count(*) FROM Customers;	driving_school
SELECT customer_status_code , count(*) FROM Customers GROUP BY customer_status_code;	driving_school
SELECT customer_status_code , count(*) FROM Customers GROUP BY customer_status_code;	driving_school
SELECT customer_status_code FROM Customers GROUP BY customer_status_code ORDER BY count(*) ASC LIMIT 1;	driving_school
SELECT customer_status_code FROM Customers GROUP BY customer_status_code ORDER BY count(*) ASC LIMIT 1;	driving_school
SELECT count(*) FROM Lessons AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Rylan" AND T2.last_name = "Goodwin" AND T1.lesson_status_code = "Completed";	driving_school
SELECT count(*) FROM Lessons AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Rylan" AND T2.last_name = "Goodwin" AND T1.lesson_status_code = "Completed";	driving_school
SELECT max(amount_outstanding) , min(amount_outstanding) , avg(amount_outstanding) FROM Customers;	driving_school
SELECT max(amount_outstanding) , min(amount_outstanding) , avg(amount_outstanding) FROM Customers;	driving_school
SELECT first_name , last_name FROM Customers WHERE amount_outstanding BETWEEN 1000 AND 3000;	driving_school
SELECT first_name , last_name FROM Customers WHERE amount_outstanding BETWEEN 1000 AND 3000;	driving_school
SELECT T1.first_name , T1.last_name FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id WHERE T2.city = "Lockmanfurt";	driving_school
SELECT T1.first_name , T1.last_name FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id WHERE T2.city = "Lockmanfurt";	driving_school
SELECT T2.country FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id WHERE T1.first_name = "Carole" AND T1.last_name = "Bernhard"	driving_school
SELECT T2.country FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id WHERE T1.first_name = "Carole" AND T1.last_name = "Bernhard"	driving_school
SELECT T2.zip_postcode FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id WHERE T1.first_name = "Carole" AND T1.last_name = "Bernhard"	driving_school
SELECT T2.zip_postcode FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id WHERE T1.first_name = "Carole" AND T1.last_name = "Bernhard"	driving_school
SELECT T2.city FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id GROUP BY T2.city ORDER BY count(*) DESC LIMIT 1;	driving_school
SELECT T2.city FROM Customers AS T1 JOIN Addresses AS T2 ON T1.customer_address_id = T2.address_id GROUP BY T2.city ORDER BY count(*) DESC LIMIT 1;	driving_school
SELECT sum(T1.amount_payment) FROM Customer_Payments AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Carole" AND T2.last_name = "Bernhard"	driving_school
SELECT sum(T1.amount_payment) FROM Customer_Payments AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Carole" AND T2.last_name = "Bernhard"	driving_school
SELECT count(*) FROM Customers WHERE customer_id NOT IN ( SELECT customer_id FROM Customer_Payments );	driving_school
SELECT count(*) FROM Customers WHERE customer_id NOT IN ( SELECT customer_id FROM Customer_Payments );	driving_school
SELECT T2.first_name , T2.last_name FROM Customer_Payments AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) > 2;	driving_school
SELECT T2.first_name , T2.last_name FROM Customer_Payments AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) > 2;	driving_school
SELECT payment_method_code , count(*) FROM Customer_Payments GROUP BY payment_method_code;	driving_school
SELECT payment_method_code , count(*) FROM Customer_Payments GROUP BY payment_method_code;	driving_school
SELECT count(*) FROM Lessons WHERE lesson_status_code = "Cancelled";	driving_school
SELECT count(*) FROM Lessons WHERE lesson_status_code = "Cancelled";	driving_school
SELECT T1.lesson_id FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn" AND nickname LIKE "%s%";	driving_school
SELECT T1.lesson_id FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn" AND nickname LIKE "%s%";	driving_school
SELECT count(*) FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name LIKE "%a%"	driving_school
SELECT count(*) FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name LIKE "%a%"	driving_school
SELECT sum(lesson_time) FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT sum(lesson_time) FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT avg(price) FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT avg(price) FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id WHERE T2.first_name = "Janessa" AND T2.last_name = "Sawayn";	driving_school
SELECT count(*) FROM Lessons AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Ray"	driving_school
SELECT count(*) FROM Lessons AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.first_name = "Ray"	driving_school
SELECT last_name FROM Customers INTERSECT SELECT last_name FROM Staff	driving_school
SELECT last_name FROM Customers INTERSECT SELECT last_name FROM Staff	driving_school
SELECT first_name FROM Staff EXCEPT SELECT T2.first_name FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id	driving_school
SELECT first_name FROM Staff EXCEPT SELECT T2.first_name FROM Lessons AS T1 JOIN Staff AS T2 ON T1.staff_id = T2.staff_id	driving_school
SELECT T1.vehicle_id , T1.vehicle_details FROM Vehicles AS T1 JOIN Lessons AS T2 ON T1.vehicle_id = T2.vehicle_id GROUP BY T1.vehicle_id ORDER BY count(*) DESC LIMIT 1	driving_school
SELECT count(*) FROM Faculty	activity_1
SELECT count(*) FROM Faculty	activity_1
SELECT DISTINCT rank FROM Faculty	activity_1
SELECT DISTINCT rank FROM Faculty	activity_1
SELECT DISTINCT building FROM Faculty	activity_1
SELECT DISTINCT building FROM Faculty	activity_1
SELECT rank , Fname , Lname FROM Faculty	activity_1
SELECT rank , Fname , Lname FROM Faculty	activity_1
SELECT Fname , Lname , phone FROM Faculty WHERE Sex = 'F'	activity_1
SELECT Fname , Lname , phone FROM Faculty WHERE Sex = 'F'	activity_1
SELECT FacID FROM Faculty WHERE Sex = 'M'	activity_1
SELECT FacID FROM Faculty WHERE Sex = 'M'	activity_1
SELECT count(*) FROM Faculty WHERE Sex = 'F' AND Rank = "Professor"	activity_1
SELECT count(*) FROM Faculty WHERE Sex = 'F' AND Rank = "Professor"	activity_1
SELECT phone , room , building FROM Faculty WHERE Fname = "Jerry" AND Lname = "Prince"	activity_1
SELECT phone , room , building FROM Faculty WHERE Fname = "Jerry" AND Lname = "Prince"	activity_1
SELECT count(*) FROM Faculty WHERE Rank = "Professor" AND building = "NEB"	activity_1
SELECT count(*) FROM Faculty WHERE Rank = "Professor" AND building = "NEB"	activity_1
SELECT fname , lname FROM Faculty WHERE Rank = "Instructor"	activity_1
SELECT fname , lname FROM Faculty WHERE Rank = "Instructor"	activity_1
SELECT building , count(*) FROM Faculty GROUP BY building	activity_1
SELECT building , count(*) FROM Faculty GROUP BY building	activity_1
SELECT building FROM Faculty GROUP BY building ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT building FROM Faculty GROUP BY building ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT building FROM Faculty WHERE rank = "Professor" GROUP BY building HAVING count(*) >= 10	activity_1
SELECT building FROM Faculty WHERE rank = "Professor" GROUP BY building HAVING count(*) >= 10	activity_1
SELECT rank , count(*) FROM Faculty GROUP BY rank	activity_1
SELECT rank , count(*) FROM Faculty GROUP BY rank	activity_1
SELECT rank , sex , count(*) FROM Faculty GROUP BY rank , sex	activity_1
SELECT rank , sex , count(*) FROM Faculty GROUP BY rank , sex	activity_1
SELECT rank FROM Faculty GROUP BY rank ORDER BY count(*) ASC LIMIT 1	activity_1
SELECT rank FROM Faculty GROUP BY rank ORDER BY count(*) ASC LIMIT 1	activity_1
SELECT sex , count(*) FROM Faculty WHERE rank = "AsstProf" GROUP BY sex	activity_1
SELECT sex , count(*) FROM Faculty WHERE rank = "AsstProf" GROUP BY sex	activity_1
SELECT T1.fname , T1.lname FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor WHERE T2.fname = "Linda" AND T2.lname = "Smith"	activity_1
SELECT T1.fname , T1.lname FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor WHERE T2.fname = "Linda" AND T2.lname = "Smith"	activity_1
SELECT T2.StuID FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor WHERE T1.rank = "Professor"	activity_1
SELECT T2.StuID FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor WHERE T1.rank = "Professor"	activity_1
SELECT T2.fname , T2.lname FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor WHERE T1.fname = "Michael" AND T1.lname = "Goodrich"	activity_1
SELECT T2.fname , T2.lname FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor WHERE T1.fname = "Michael" AND T1.lname = "Goodrich"	activity_1
SELECT T1.FacID , count(*) FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.FacID	activity_1
SELECT T1.FacID , count(*) FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.FacID	activity_1
SELECT T1.rank , count(*) FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.rank	activity_1
SELECT T1.rank , count(*) FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.rank	activity_1
SELECT T1.fname , T1.lname FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.FacID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.fname , T1.lname FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.FacID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.FacID FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.FacID HAVING count(*) >= 2	activity_1
SELECT T1.FacID FROM Faculty AS T1 JOIN Student AS T2 ON T1.FacID = T2.advisor GROUP BY T1.FacID HAVING count(*) >= 2	activity_1
SELECT FacID FROM Faculty EXCEPT SELECT advisor FROM Student	activity_1
SELECT FacID FROM Faculty EXCEPT SELECT advisor FROM Student	activity_1
SELECT activity_name FROM Activity	activity_1
SELECT activity_name FROM Activity	activity_1
SELECT count(*) FROM Activity	activity_1
SELECT count(*) FROM Activity	activity_1
SELECT count(DISTINCT FacID) FROM Faculty_participates_in	activity_1
SELECT count(DISTINCT FacID) FROM Faculty_participates_in	activity_1
SELECT FacID FROM Faculty EXCEPT SELECT FacID FROM Faculty_participates_in	activity_1
SELECT FacID FROM Faculty EXCEPT SELECT FacID FROM Faculty_participates_in	activity_1
SELECT FacID FROM Faculty_participates_in INTERSECT SELECT advisor FROM Student	activity_1
SELECT FacID FROM Faculty_participates_in INTERSECT SELECT advisor FROM Student	activity_1
SELECT count(*) FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID WHERE T1.fname = "Mark" AND T1.lname = "Giuliano"	activity_1
SELECT count(*) FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID WHERE T1.fname = "Mark" AND T1.lname = "Giuliano"	activity_1
SELECT T3.activity_name FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN Activity AS T3 ON T3.actid = T2.actid WHERE T1.fname = "Mark" AND T1.lname = "Giuliano"	activity_1
SELECT T3.activity_name FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN Activity AS T3 ON T3.actid = T2.actid WHERE T1.fname = "Mark" AND T1.lname = "Giuliano"	activity_1
SELECT T1.fname , T1.lname , count(*) , T1.FacID FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID GROUP BY T1.FacID	activity_1
SELECT T1.fname , T1.lname , count(*) , T1.FacID FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID GROUP BY T1.FacID	activity_1
SELECT T1.activity_name , count(*) FROM Activity AS T1 JOIN Faculty_participates_in AS T2 ON T1.actID = T2.actID GROUP BY T1.actID	activity_1
SELECT T1.activity_name , count(*) FROM Activity AS T1 JOIN Faculty_participates_in AS T2 ON T1.actID = T2.actID GROUP BY T1.actID	activity_1
SELECT T1.fname , T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID GROUP BY T1.FacID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.fname , T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID GROUP BY T1.FacID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.activity_name FROM Activity AS T1 JOIN Faculty_participates_in AS T2 ON T1.actID = T2.actID GROUP BY T1.actID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.activity_name FROM Activity AS T1 JOIN Faculty_participates_in AS T2 ON T1.actID = T2.actID GROUP BY T1.actID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Participates_in	activity_1
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Participates_in	activity_1
SELECT StuID FROM Participates_in INTERSECT SELECT StuID FROM Student WHERE age < 20	activity_1
SELECT StuID FROM Participates_in INTERSECT SELECT StuID FROM Student WHERE age < 20	activity_1
SELECT T1.fname , T1.lname FROM Student AS T1 JOIN Participates_in AS T2 ON T1.StuID = T2.StuID GROUP BY T1.StuID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.fname , T1.lname FROM Student AS T1 JOIN Participates_in AS T2 ON T1.StuID = T2.StuID GROUP BY T1.StuID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.activity_name FROM Activity AS T1 JOIN Participates_in AS T2 ON T1.actID = T2.actID GROUP BY T1.actID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT T1.activity_name FROM Activity AS T1 JOIN Participates_in AS T2 ON T1.actID = T2.actID GROUP BY T1.actID ORDER BY count(*) DESC LIMIT 1	activity_1
SELECT DISTINCT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Canoeing' OR T3.activity_name = 'Kayaking'	activity_1
SELECT DISTINCT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Canoeing' OR T3.activity_name = 'Kayaking'	activity_1
SELECT lname FROM faculty WHERE rank = 'Professor' EXCEPT SELECT DISTINCT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Canoeing' OR T3.activity_name = 'Kayaking'	activity_1
SELECT lname FROM faculty WHERE rank = 'Professor' EXCEPT SELECT DISTINCT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Canoeing' OR T3.activity_name = 'Kayaking'	activity_1
SELECT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Canoeing' INTERSECT SELECT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Kayaking'	activity_1
SELECT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Canoeing' INTERSECT SELECT T1.lname FROM Faculty AS T1 JOIN Faculty_participates_in AS T2 ON T1.facID = T2.facID JOIN activity AS T3 ON T2.actid = T2.actid WHERE T3.activity_name = 'Kayaking'	activity_1
SELECT T1.stuid FROM participates_in AS T1 JOIN activity AS T2 ON T2.actid = T2.actid WHERE T2.activity_name = 'Canoeing' INTERSECT SELECT T1.stuid FROM participates_in AS T1 JOIN activity AS T2 ON T2.actid = T2.actid WHERE T2.activity_name = 'Kayaking'	activity_1
SELECT T1.stuid FROM participates_in AS T1 JOIN activity AS T2 ON T2.actid = T2.actid WHERE T2.activity_name = 'Canoeing' INTERSECT SELECT T1.stuid FROM participates_in AS T1 JOIN activity AS T2 ON T2.actid = T2.actid WHERE T2.activity_name = 'Kayaking'	activity_1
SELECT order_id FROM orders ORDER BY date_order_placed DESC LIMIT 1	tracking_orders
SELECT order_id FROM orders ORDER BY date_order_placed DESC LIMIT 1	tracking_orders
SELECT order_id , customer_id FROM orders ORDER BY date_order_placed LIMIT 1	tracking_orders
SELECT order_id , customer_id FROM orders ORDER BY date_order_placed LIMIT 1	tracking_orders
SELECT order_id FROM shipments WHERE shipment_tracking_number = "3452"	tracking_orders
SELECT order_id FROM shipments WHERE shipment_tracking_number = "3452"	tracking_orders
SELECT order_item_id FROM order_items WHERE product_id = 11	tracking_orders
SELECT order_item_id FROM order_items WHERE product_id = 11	tracking_orders
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "Packing"	tracking_orders
SELECT DISTINCT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "Packing"	tracking_orders
SELECT DISTINCT T1.customer_details FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "On Road"	tracking_orders
SELECT DISTINCT T1.customer_details FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "On Road"	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T1.customer_id FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T1.customer_id FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T2.order_id , T2.order_status FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T1.customer_name = "Jeramie"	tracking_orders
SELECT T2.order_id , T2.order_status FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T1.customer_name = "Jeramie"	tracking_orders
SELECT T2.date_order_placed FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T1.customer_name = "Jeramie"	tracking_orders
SELECT T2.date_order_placed FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T1.customer_name = "Jeramie"	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.date_order_placed >= "2009-01-01" AND T2.date_order_placed <= "2010-01-01"	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.date_order_placed >= "2009-01-01" AND T2.date_order_placed <= "2010-01-01"	tracking_orders
SELECT DISTINCT T2.product_id FROM orders AS T1 JOIN order_items AS T2 ON T1.order_id = T2.order_id WHERE T1.date_order_placed >= "1975-01-01" AND T1.date_order_placed <= "1976-01-01"	tracking_orders
SELECT DISTINCT T2.product_id FROM orders AS T1 JOIN order_items AS T2 ON T1.order_id = T2.order_id WHERE T1.date_order_placed >= "1975-01-01" AND T1.date_order_placed <= "1976-01-01"	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "On Road" INTERSECT SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "Shipped"	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "On Road" INTERSECT SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "Shipped"	tracking_orders
SELECT T1.customer_id FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "On Road" INTERSECT SELECT T1.customer_id FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "Shipped"	tracking_orders
SELECT T1.customer_id FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "On Road" INTERSECT SELECT T1.customer_id FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id WHERE T2.order_status = "Shipped"	tracking_orders
SELECT T1.date_order_placed FROM orders AS T1 JOIN shipments AS T2 ON T1.order_id = T2.order_id WHERE T2.shipment_tracking_number = 3452	tracking_orders
SELECT T1.date_order_placed FROM orders AS T1 JOIN shipments AS T2 ON T1.order_id = T2.order_id WHERE T2.shipment_tracking_number = 3452	tracking_orders
SELECT T1.date_order_placed FROM orders AS T1 JOIN shipments AS T2 ON T1.order_id = T2.order_id WHERE T2.invoice_number = 10	tracking_orders
SELECT T1.date_order_placed FROM orders AS T1 JOIN shipments AS T2 ON T1.order_id = T2.order_id WHERE T2.invoice_number = 10	tracking_orders
SELECT count(*) , T3.product_id FROM orders AS T1 JOIN order_items AS T2 JOIN products AS T3 ON T1.order_id = T2.order_id AND T2.product_id = T3.product_id GROUP BY T3.product_id	tracking_orders
SELECT count(*) , T3.product_id FROM orders AS T1 JOIN order_items AS T2 JOIN products AS T3 ON T1.order_id = T2.order_id AND T2.product_id = T3.product_id GROUP BY T3.product_id	tracking_orders
SELECT T3.product_name , count(*) FROM orders AS T1 JOIN order_items AS T2 JOIN products AS T3 ON T1.order_id = T2.order_id AND T2.product_id = T3.product_id GROUP BY T3.product_id	tracking_orders
SELECT T3.product_name , count(*) FROM orders AS T1 JOIN order_items AS T2 JOIN products AS T3 ON T1.order_id = T2.order_id AND T2.product_id = T3.product_id GROUP BY T3.product_id	tracking_orders
SELECT order_id FROM shipments WHERE shipment_date > "2000-01-01"	tracking_orders
SELECT order_id FROM shipments WHERE shipment_date > "2000-01-01"	tracking_orders
SELECT order_id FROM shipments WHERE shipment_date = (SELECT max(shipment_date) FROM shipments)	tracking_orders
SELECT order_id FROM shipments WHERE shipment_date = (SELECT max(shipment_date) FROM shipments)	tracking_orders
SELECT DISTINCT product_name FROM products ORDER BY product_name	tracking_orders
SELECT DISTINCT product_name FROM products ORDER BY product_name	tracking_orders
SELECT DISTINCT order_id FROM orders ORDER BY date_order_placed	tracking_orders
SELECT DISTINCT order_id FROM orders ORDER BY date_order_placed	tracking_orders
SELECT T1.order_id FROM orders AS T1 JOIN order_items AS T2 ON T1.order_id = T2.order_id GROUP BY T1.order_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T1.order_id FROM orders AS T1 JOIN order_items AS T2 ON T1.order_id = T2.order_id GROUP BY T1.order_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1	tracking_orders
SELECT invoice_number FROM invoices WHERE invoice_date < "1989-09-03" OR invoice_date > "2007-12-25"	tracking_orders
SELECT invoice_number FROM invoices WHERE invoice_date < "1989-09-03" OR invoice_date > "2007-12-25"	tracking_orders
SELECT DISTINCT invoice_details FROM invoices WHERE invoice_date < "1989-09-03" OR invoice_date > "2007-12-25"	tracking_orders
SELECT DISTINCT invoice_details FROM invoices WHERE invoice_date < "1989-09-03" OR invoice_date > "2007-12-25"	tracking_orders
SELECT T2.customer_name , count(*) FROM orders AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T2.customer_id HAVING count(*) >= 2	tracking_orders
SELECT T2.customer_name , count(*) FROM orders AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T2.customer_id HAVING count(*) >= 2	tracking_orders
SELECT T2.customer_name FROM orders AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T2.customer_id HAVING count(*) <= 2	tracking_orders
SELECT T2.customer_name FROM orders AS T1 JOIN customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T2.customer_id HAVING count(*) <= 2	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 JOIN order_items AS T3 JOIN products AS T4 ON T1.customer_id = T2.customer_id AND T2.order_id = T3.order_id AND T3.product_id = T4.product_id WHERE T4.product_name = "food" GROUP BY T1.customer_id HAVING count(*) >= 1	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 JOIN order_items AS T3 JOIN products AS T4 ON T1.customer_id = T2.customer_id AND T2.order_id = T3.order_id AND T3.product_id = T4.product_id WHERE T4.product_name = "food" GROUP BY T1.customer_id HAVING count(*) >= 1	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 JOIN order_items AS T3 JOIN products AS T4 ON T1.customer_id = T2.customer_id AND T2.order_id = T3.order_id AND T3.product_id = T4.product_id WHERE T3.order_item_status = "Cancel" AND T4.product_name = "food" GROUP BY T1.customer_id HAVING count(*) >= 1	tracking_orders
SELECT T1.customer_name FROM customers AS T1 JOIN orders AS T2 JOIN order_items AS T3 JOIN products AS T4 ON T1.customer_id = T2.customer_id AND T2.order_id = T3.order_id AND T3.product_id = T4.product_id WHERE T3.order_item_status = "Cancel" AND T4.product_name = "food" GROUP BY T1.customer_id HAVING count(*) >= 1	tracking_orders
SELECT count(*) FROM architect WHERE gender = 'female'	architecture
SELECT name , nationality , id FROM architect WHERE gender = 'male' ORDER BY name	architecture
SELECT max(T1.length_meters) , T2.name FROM bridge AS T1 JOIN architect AS T2 ON T1.architect_id = T2.id	architecture
SELECT avg(length_feet) FROM bridge	architecture
SELECT name , built_year FROM mill WHERE TYPE = 'Grondzeiler'	architecture
SELECT DISTINCT T1.name , T1.nationality FROM architect AS T1 JOIN mill AS t2 ON T1.id = T2.architect_id	architecture
SELECT name FROM mill WHERE LOCATION != 'Donceel'	architecture
SELECT DISTINCT T1.type FROM mill AS T1 JOIN architect AS t2 ON T1.architect_id = T2.id WHERE T2.nationality = 'American' OR T2.nationality = 'Canadian'	architecture
SELECT T1.id , T1.name FROM architect AS T1 JOIN bridge AS T2 ON T1.id = T2.architect_id GROUP BY T1.id HAVING count(*) >= 3	architecture
SELECT T1.id , T1.name , T1.nationality FROM architect AS T1 JOIN mill AS T2 ON T1.id = T2.architect_id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1	architecture
SELECT T1.id , T1.name , T1.gender FROM architect AS T1 JOIN bridge AS T2 ON T1.id = T2.architect_id GROUP BY T1.id HAVING count(*) = 2 UNION SELECT T1.id , T1.name , T1.gender FROM architect AS T1 JOIN mill AS T2 ON T1.id = T2.architect_id GROUP BY T1.id HAVING count(*) = 1	architecture
SELECT LOCATION FROM bridge WHERE name = 'Kolob Arch' OR name = 'Rainbow Bridge'	architecture
SELECT name FROM mill WHERE name LIKE '%Moulin%'	architecture
SELECT DISTINCT T1.name FROM mill AS T1 JOIN architect AS t2 ON T1.architect_id = T2.id JOIN bridge AS T3 ON T3.architect_id = T2.id WHERE T3.length_meters > 80	architecture
SELECT TYPE , count(*) FROM mill GROUP BY TYPE ORDER BY count(*) DESC LIMIT 1	architecture
SELECT count(*) FROM architect WHERE id NOT IN ( SELECT architect_id FROM mill WHERE built_year < 1850 );	architecture
SELECT t1.name FROM bridge AS t1 JOIN architect AS t2 ON t1.architect_id = t2.id WHERE t2.nationality = 'American' ORDER BY t1.length_feet	architecture
SELECT count(*) FROM singer	concert_singer
SELECT count(*) FROM singer	concert_singer
SELECT name , country , age FROM singer ORDER BY age DESC	concert_singer
SELECT name , country , age FROM singer ORDER BY age DESC	concert_singer
SELECT avg(age) , min(age) , max(age) FROM singer WHERE country = 'France'	concert_singer
SELECT avg(age) , min(age) , max(age) FROM singer WHERE country = 'France'	concert_singer
SELECT song_name , song_release_year FROM singer ORDER BY age LIMIT 1	concert_singer
SELECT song_name , song_release_year FROM singer ORDER BY age LIMIT 1	concert_singer
SELECT DISTINCT country FROM singer WHERE age > 20	concert_singer
SELECT DISTINCT country FROM singer WHERE age > 20	concert_singer
SELECT country , count(*) FROM singer GROUP BY country	concert_singer
SELECT country , count(*) FROM singer GROUP BY country	concert_singer
SELECT song_name FROM singer WHERE age > (SELECT avg(age) FROM singer)	concert_singer
SELECT song_name FROM singer WHERE age > (SELECT avg(age) FROM singer)	concert_singer
SELECT LOCATION , name FROM stadium WHERE capacity BETWEEN 5000 AND 10000	concert_singer
SELECT LOCATION , name FROM stadium WHERE capacity BETWEEN 5000 AND 10000	concert_singer
SELECT avg(capacity) , max(capacity) FROM stadium	concert_singer
SELECT avg(capacity) , max(capacity) FROM stadium	concert_singer
SELECT name , capacity FROM stadium ORDER BY average DESC LIMIT 1	concert_singer
SELECT name , capacity FROM stadium ORDER BY average DESC LIMIT 1	concert_singer
SELECT count(*) FROM concert WHERE YEAR = 2014 OR YEAR = 2015	concert_singer
SELECT count(*) FROM concert WHERE YEAR = 2014 OR YEAR = 2015	concert_singer
SELECT T2.name , count(*) FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id GROUP BY T1.stadium_id	concert_singer
SELECT T2.name , count(*) FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id GROUP BY T1.stadium_id	concert_singer
SELECT T2.name , T2.capacity FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.year >= 2014 GROUP BY T2.stadium_id ORDER BY count(*) DESC LIMIT 1	concert_singer
SELECT T2.name , T2.capacity FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.year >= 2014 GROUP BY T2.stadium_id ORDER BY count(*) DESC LIMIT 1	concert_singer
SELECT YEAR FROM concert GROUP BY YEAR ORDER BY count(*) DESC LIMIT 1	concert_singer
SELECT YEAR FROM concert GROUP BY YEAR ORDER BY count(*) DESC LIMIT 1	concert_singer
SELECT name FROM stadium WHERE stadium_id NOT IN (SELECT stadium_id FROM concert)	concert_singer
SELECT name FROM stadium WHERE stadium_id NOT IN (SELECT stadium_id FROM concert)	concert_singer
SELECT country FROM singer WHERE age > 40 INTERSECT SELECT country FROM singer WHERE age < 30	concert_singer
SELECT name FROM stadium EXCEPT SELECT T2.name FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.year = 2014	concert_singer
SELECT name FROM stadium EXCEPT SELECT T2.name FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.year = 2014	concert_singer
SELECT T2.concert_name , T2.theme , count(*) FROM singer_in_concert AS T1 JOIN concert AS T2 ON T1.concert_id = T2.concert_id GROUP BY T2.concert_id	concert_singer
SELECT T2.concert_name , T2.theme , count(*) FROM singer_in_concert AS T1 JOIN concert AS T2 ON T1.concert_id = T2.concert_id GROUP BY T2.concert_id	concert_singer
SELECT T2.name , count(*) FROM singer_in_concert AS T1 JOIN singer AS T2 ON T1.singer_id = T2.singer_id GROUP BY T2.singer_id	concert_singer
SELECT T2.name , count(*) FROM singer_in_concert AS T1 JOIN singer AS T2 ON T1.singer_id = T2.singer_id GROUP BY T2.singer_id	concert_singer
SELECT T2.name FROM singer_in_concert AS T1 JOIN singer AS T2 ON T1.singer_id = T2.singer_id JOIN concert AS T3 ON T1.concert_id = T3.concert_id WHERE T3.year = 2014	concert_singer
SELECT T2.name FROM singer_in_concert AS T1 JOIN singer AS T2 ON T1.singer_id = T2.singer_id JOIN concert AS T3 ON T1.concert_id = T3.concert_id WHERE T3.year = 2014	concert_singer
SELECT name , country FROM singer WHERE song_name LIKE '%Hey%'	concert_singer
SELECT name , country FROM singer WHERE song_name LIKE '%Hey%'	concert_singer
SELECT T2.name , T2.location FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.Year = 2014 INTERSECT SELECT T2.name , T2.location FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.Year = 2015	concert_singer
SELECT T2.name , T2.location FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.Year = 2014 INTERSECT SELECT T2.name , T2.location FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id WHERE T1.Year = 2015	concert_singer
SELECT count(*) FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id ORDER BY T2.Capacity DESC LIMIT 1	concert_singer
SELECT count(*) FROM concert AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.stadium_id ORDER BY T2.Capacity DESC LIMIT 1	concert_singer
SELECT count(*) FROM pets WHERE weight > 10	pets_1
SELECT count(*) FROM pets WHERE weight > 10	pets_1
SELECT weight FROM pets ORDER BY pet_age LIMIT 1	pets_1
SELECT weight FROM pets ORDER BY pet_age LIMIT 1	pets_1
SELECT max(weight) , petType FROM pets GROUP BY petType	pets_1
SELECT max(weight) , petType FROM pets GROUP BY petType	pets_1
SELECT count(*) FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid WHERE T1.age > 20	pets_1
SELECT count(*) FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid WHERE T1.age > 20	pets_1
SELECT count(*) FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T2.petid = T3.petid WHERE T1.sex = 'F' AND T3.pettype = 'dog'	pets_1
SELECT count(*) FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T2.petid = T3.petid WHERE T1.sex = 'F' AND T3.pettype = 'dog'	pets_1
SELECT count(DISTINCT pettype) FROM pets	pets_1
SELECT count(DISTINCT pettype) FROM pets	pets_1
SELECT DISTINCT T1.Fname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat' OR T3.pettype = 'dog'	pets_1
SELECT DISTINCT T1.Fname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat' OR T3.pettype = 'dog'	pets_1
SELECT T1.Fname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat' INTERSECT SELECT T1.Fname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'dog'	pets_1
SELECT T1.Fname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat' INTERSECT SELECT T1.Fname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'dog'	pets_1
SELECT major , age FROM student WHERE stuid NOT IN (SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat')	pets_1
SELECT major , age FROM student WHERE stuid NOT IN (SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat')	pets_1
SELECT stuid FROM student EXCEPT SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat'	pets_1
SELECT stuid FROM student EXCEPT SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat'	pets_1
SELECT T1.fname , T1.age FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'dog' AND T1.stuid NOT IN (SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat')	pets_1
SELECT T1.fname , T1.age FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'dog' AND T1.stuid NOT IN (SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pettype = 'cat')	pets_1
SELECT pettype , weight FROM pets ORDER BY pet_age LIMIT 1	pets_1
SELECT pettype , weight FROM pets ORDER BY pet_age LIMIT 1	pets_1
SELECT petid , weight FROM pets WHERE pet_age > 1	pets_1
SELECT petid , weight FROM pets WHERE pet_age > 1	pets_1
SELECT avg(pet_age) , max(pet_age) , pettype FROM pets GROUP BY pettype	pets_1
SELECT avg(pet_age) , max(pet_age) , pettype FROM pets GROUP BY pettype	pets_1
SELECT avg(weight) , pettype FROM pets GROUP BY pettype	pets_1
SELECT avg(weight) , pettype FROM pets GROUP BY pettype	pets_1
SELECT DISTINCT T1.fname , T1.age FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid	pets_1
SELECT DISTINCT T1.fname , T1.age FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid	pets_1
SELECT T2.petid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid WHERE T1.Lname = 'Smith'	pets_1
SELECT T2.petid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid WHERE T1.Lname = 'Smith'	pets_1
SELECT count(*) , T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid GROUP BY T1.stuid	pets_1
SELECT count(*) , T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid GROUP BY T1.stuid	pets_1
SELECT T1.fname , T1.sex FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid GROUP BY T1.stuid HAVING count(*) > 1	pets_1
SELECT T1.fname , T1.sex FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid GROUP BY T1.stuid HAVING count(*) > 1	pets_1
SELECT T1.lname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pet_age = 3 AND T3.pettype = 'cat'	pets_1
SELECT T1.lname FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid JOIN pets AS T3 ON T3.petid = T2.petid WHERE T3.pet_age = 3 AND T3.pettype = 'cat'	pets_1
SELECT avg(age) FROM student WHERE stuid NOT IN (SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid)	pets_1
SELECT avg(age) FROM student WHERE stuid NOT IN (SELECT T1.stuid FROM student AS T1 JOIN has_pet AS T2 ON T1.stuid = T2.stuid)	pets_1
SELECT count(*) FROM CONTINENTS;	car_1
SELECT count(*) FROM CONTINENTS;	car_1
SELECT T1.ContId , T1.Continent , count(*) FROM CONTINENTS AS T1 JOIN COUNTRIES AS T2 ON T1.ContId = T2.Continent GROUP BY T1.ContId;	car_1
SELECT T1.ContId , T1.Continent , count(*) FROM CONTINENTS AS T1 JOIN COUNTRIES AS T2 ON T1.ContId = T2.Continent GROUP BY T1.ContId;	car_1
SELECT count(*) FROM COUNTRIES;	car_1
SELECT count(*) FROM COUNTRIES;	car_1
SELECT T1.FullName , T1.Id , count(*) FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker GROUP BY T1.Id;	car_1
SELECT T1.FullName , T1.Id , count(*) FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker GROUP BY T1.Id;	car_1
SELECT T1.Model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id ORDER BY T2.horsepower ASC LIMIT 1;	car_1
SELECT T1.Model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id ORDER BY T2.horsepower ASC LIMIT 1;	car_1
SELECT T1.model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T2.Weight < (SELECT avg(Weight) FROM CARS_DATA)	car_1
SELECT T1.model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T2.Weight < (SELECT avg(Weight) FROM CARS_DATA)	car_1
SELECT DISTINCT T1.Maker FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker JOIN CAR_NAMES AS T3 ON T2.model = T3.model JOIN CARS_DATA AS T4 ON T3.MakeId = T4.id WHERE T4.year = '1970';	car_1
SELECT DISTINCT T1.Maker FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker JOIN CAR_NAMES AS T3 ON T2.model = T3.model JOIN CARS_DATA AS T4 ON T3.MakeId = T4.id WHERE T4.year = '1970';	car_1
SELECT T2.Make , T1.Year FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T1.Year = (SELECT min(YEAR) FROM CARS_DATA);	car_1
SELECT T2.Make , T1.Year FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T1.Year = (SELECT min(YEAR) FROM CARS_DATA);	car_1
SELECT DISTINCT T1.model FROM MODEL_LIST AS T1 JOIN CAR_NAMES AS T2 ON T1.model = T2.model JOIN CARS_DATA AS T3 ON T2.MakeId = T3.id WHERE T3.year > 1980;	car_1
SELECT DISTINCT T1.model FROM MODEL_LIST AS T1 JOIN CAR_NAMES AS T2 ON T1.model = T2.model JOIN CARS_DATA AS T3 ON T2.MakeId = T3.id WHERE T3.year > 1980;	car_1
SELECT T1.Continent , count(*) FROM CONTINENTS AS T1 JOIN COUNTRIES AS T2 ON T1.ContId = T2.continent JOIN car_makers AS T3 ON T2.CountryId = T3.Country GROUP BY T1.Continent;	car_1
SELECT T1.Continent , count(*) FROM CONTINENTS AS T1 JOIN COUNTRIES AS T2 ON T1.ContId = T2.continent JOIN car_makers AS T3 ON T2.CountryId = T3.Country GROUP BY T1.Continent;	car_1
SELECT T2.CountryName FROM CAR_MAKERS AS T1 JOIN COUNTRIES AS T2 ON T1.Country = T2.CountryId GROUP BY T1.Country ORDER BY Count(*) DESC LIMIT 1;	car_1
SELECT T2.CountryName FROM CAR_MAKERS AS T1 JOIN COUNTRIES AS T2 ON T1.Country = T2.CountryId GROUP BY T1.Country ORDER BY Count(*) DESC LIMIT 1;	car_1
SELECT Count(*) , T2.FullName , T2.id FROM MODEL_LIST AS T1 JOIN CAR_MAKERS AS T2 ON T1.Maker = T2.Id GROUP BY T2.id;	car_1
SELECT Count(*) , T2.FullName , T2.id FROM MODEL_LIST AS T1 JOIN CAR_MAKERS AS T2 ON T1.Maker = T2.Id GROUP BY T2.id;	car_1
SELECT T1.Accelerate FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T2.Make = 'amc hornet sportabout (sw)';	car_1
SELECT T1.Accelerate FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T2.Make = 'amc hornet sportabout (sw)';	car_1
SELECT count(*) FROM CAR_MAKERS AS T1 JOIN COUNTRIES AS T2 ON T1.Country = T2.CountryId WHERE T2.CountryName = 'france';	car_1
SELECT count(*) FROM CAR_MAKERS AS T1 JOIN COUNTRIES AS T2 ON T1.Country = T2.CountryId WHERE T2.CountryName = 'france';	car_1
SELECT count(*) FROM MODEL_LIST AS T1 JOIN CAR_MAKERS AS T2 ON T1.Maker = T2.Id JOIN COUNTRIES AS T3 ON T2.Country = T3.CountryId WHERE T3.CountryName = 'usa';	car_1
SELECT count(*) FROM MODEL_LIST AS T1 JOIN CAR_MAKERS AS T2 ON T1.Maker = T2.Id JOIN COUNTRIES AS T3 ON T2.Country = T3.CountryId WHERE T3.CountryName = 'usa';	car_1
SELECT avg(mpg) FROM CARS_DATA WHERE Cylinders = 4;	car_1
SELECT avg(mpg) FROM CARS_DATA WHERE Cylinders = 4;	car_1
SELECT Weight FROM CARS_DATA WHERE Cylinders = 4 AND YEAR = 1974 ORDER BY Weight ASC LIMIT 1;	car_1
SELECT Weight FROM CARS_DATA WHERE Cylinders = 4 AND YEAR = 1974 ORDER BY Weight ASC LIMIT 1;	car_1
SELECT Maker , Model FROM MODEL_LIST;	car_1
SELECT Maker , Model FROM MODEL_LIST;	car_1
SELECT T1.CountryName , T1.CountryId FROM COUNTRIES AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country GROUP BY T1.CountryId HAVING count(*) >= 1;	car_1
SELECT T1.CountryName , T1.CountryId FROM COUNTRIES AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country GROUP BY T1.CountryId HAVING count(*) >= 1;	car_1
SELECT count(*) FROM CARS_DATA WHERE horsepower > 150;	car_1
SELECT count(*) FROM CARS_DATA WHERE horsepower > 150;	car_1
SELECT avg(Weight) , YEAR FROM CARS_DATA GROUP BY YEAR;	car_1
SELECT avg(Weight) , YEAR FROM CARS_DATA GROUP BY YEAR;	car_1
SELECT T1.CountryName FROM COUNTRIES AS T1 JOIN CONTINENTS AS T2 ON T1.Continent = T2.ContId JOIN CAR_MAKERS AS T3 ON T1.CountryId = T3.Country WHERE T2.Continent = 'europe' GROUP BY T1.CountryName HAVING count(*) >= 3;	car_1
SELECT T1.CountryName FROM COUNTRIES AS T1 JOIN CONTINENTS AS T2 ON T1.Continent = T2.ContId JOIN CAR_MAKERS AS T3 ON T1.CountryId = T3.Country WHERE T2.Continent = 'europe' GROUP BY T1.CountryName HAVING count(*) >= 3;	car_1
SELECT T2.horsepower , T1.Make FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T2.cylinders = 3 ORDER BY T2.horsepower DESC LIMIT 1;	car_1
SELECT T2.horsepower , T1.Make FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T2.cylinders = 3 ORDER BY T2.horsepower DESC LIMIT 1;	car_1
SELECT T1.Model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id ORDER BY T2.mpg DESC LIMIT 1;	car_1
SELECT T1.Model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id ORDER BY T2.mpg DESC LIMIT 1;	car_1
SELECT avg(horsepower) FROM CARS_DATA WHERE YEAR < 1980;	car_1
SELECT avg(horsepower) FROM CARS_DATA WHERE YEAR < 1980;	car_1
SELECT avg(T2.edispl) FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T1.Model = 'volvo';	car_1
SELECT avg(T2.edispl) FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T1.Model = 'volvo';	car_1
SELECT max(Accelerate) , Cylinders FROM CARS_DATA GROUP BY Cylinders;	car_1
SELECT max(Accelerate) , Cylinders FROM CARS_DATA GROUP BY Cylinders;	car_1
SELECT Model FROM CAR_NAMES GROUP BY Model ORDER BY count(*) DESC LIMIT 1;	car_1
SELECT Model FROM CAR_NAMES GROUP BY Model ORDER BY count(*) DESC LIMIT 1;	car_1
SELECT count(*) FROM CARS_DATA WHERE Cylinders > 4;	car_1
SELECT count(*) FROM CARS_DATA WHERE Cylinders > 4;	car_1
SELECT count(*) FROM CARS_DATA WHERE YEAR = 1980;	car_1
SELECT count(*) FROM CARS_DATA WHERE YEAR = 1980;	car_1
SELECT count(*) FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker WHERE T1.FullName = 'American Motor Company';	car_1
SELECT count(*) FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker WHERE T1.FullName = 'American Motor Company';	car_1
SELECT T1.FullName , T1.Id FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker GROUP BY T1.Id HAVING count(*) > 3;	car_1
SELECT T1.FullName , T1.Id FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker GROUP BY T1.Id HAVING count(*) > 3;	car_1
SELECT DISTINCT T2.Model FROM CAR_NAMES AS T1 JOIN MODEL_LIST AS T2 ON T1.Model = T2.Model JOIN CAR_MAKERS AS T3 ON T2.Maker = T3.Id JOIN CARS_DATA AS T4 ON T1.MakeId = T4.Id WHERE T3.FullName = 'General Motors' OR T4.weight > 3500;	car_1
SELECT DISTINCT T2.Model FROM CAR_NAMES AS T1 JOIN MODEL_LIST AS T2 ON T1.Model = T2.Model JOIN CAR_MAKERS AS T3 ON T2.Maker = T3.Id JOIN CARS_DATA AS T4 ON T1.MakeId = T4.Id WHERE T3.FullName = 'General Motors' OR T4.weight > 3500;	car_1
SELECT DISTINCT T1.Year FROM CARS_DATA AS T1 WHERE T1.Weight > 3000 AND T1.weight < 4000;	car_1
SELECT DISTINCT T1.Year FROM CARS_DATA AS T1 WHERE T1.Weight > 3000 AND T1.weight < 4000;	car_1
SELECT T1.horsepower FROM CARS_DATA AS T1 ORDER BY T1.accelerate DESC LIMIT 1;	car_1
SELECT T1.horsepower FROM CARS_DATA AS T1 ORDER BY T1.accelerate DESC LIMIT 1;	car_1
SELECT T1.cylinders FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T2.Model = 'volvo' ORDER BY T1.accelerate ASC LIMIT 1;	car_1
SELECT T1.cylinders FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T2.Model = 'volvo' ORDER BY T1.accelerate ASC LIMIT 1;	car_1
SELECT COUNT(*) FROM CARS_DATA WHERE Accelerate > ( SELECT Accelerate FROM CARS_DATA ORDER BY Horsepower DESC LIMIT 1 );	car_1
SELECT COUNT(*) FROM CARS_DATA WHERE Accelerate > ( SELECT Accelerate FROM CARS_DATA ORDER BY Horsepower DESC LIMIT 1 );	car_1
SELECT COUNT(*) FROM ( SELECT T1.CountryId , COUNT(*) FROM COUNTRIES AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country GROUP BY T1.CountryId HAVING count(*) > 2 );	car_1
SELECT COUNT(*) FROM ( SELECT T1.CountryId , COUNT(*) FROM COUNTRIES AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country GROUP BY T1.CountryId HAVING count(*) > 2 );	car_1
SELECT COUNT(*) FROM CARS_DATA WHERE Cylinders > 6;	car_1
SELECT COUNT(*) FROM CARS_DATA WHERE Cylinders > 6;	car_1
SELECT T1.Model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T2.Cylinders = 4 ORDER BY T2.horsepower DESC LIMIT 1;	car_1
SELECT T1.Model FROM CAR_NAMES AS T1 JOIN CARS_DATA AS T2 ON T1.MakeId = T2.Id WHERE T2.Cylinders = 4 ORDER BY T2.horsepower DESC LIMIT 1;	car_1
SELECT T2.MakeId , T2.Make FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T1.Horsepower > (SELECT min(Horsepower) FROM CARS_DATA) AND T1.Cylinders <= 3;	car_1
SELECT T2.MakeId , T2.Make FROM CARS_DATA AS T1 JOIN CAR_NAMES AS T2 ON T1.Id = T2.MakeId WHERE T1.Horsepower > (SELECT min(Horsepower) FROM CARS_DATA) AND T1.Cylinders <= 3;	car_1
SELECT mpg FROM CARS_DATA WHERE Cylinders = 8 OR YEAR < 1980 ORDER BY mpg DESC LIMIT 1;	car_1
SELECT mpg FROM CARS_DATA WHERE Cylinders = 8 OR YEAR < 1980 ORDER BY mpg DESC LIMIT 1;	car_1
SELECT DISTINCT T1.model FROM MODEL_LIST AS T1 JOIN CAR_NAMES AS T2 ON T1.Model = T2.Model JOIN CARS_DATA AS T3 ON T2.MakeId = T3.Id JOIN CAR_MAKERS AS T4 ON T1.Maker = T4.Id WHERE T3.weight < 3500 AND T4.FullName != 'Ford Motor Company';	car_1
SELECT DISTINCT T1.model FROM MODEL_LIST AS T1 JOIN CAR_NAMES AS T2 ON T1.Model = T2.Model JOIN CARS_DATA AS T3 ON T2.MakeId = T3.Id JOIN CAR_MAKERS AS T4 ON T1.Maker = T4.Id WHERE T3.weight < 3500 AND T4.FullName != 'Ford Motor Company';	car_1
SELECT CountryName FROM countries EXCEPT SELECT T1.CountryName FROM countries AS T1 JOIN CAR_MAKERS AS T2 ON T1.countryId = T2.Country;	car_1
SELECT CountryName FROM countries EXCEPT SELECT T1.CountryName FROM countries AS T1 JOIN CAR_MAKERS AS T2 ON T1.countryId = T2.Country;	car_1
SELECT T1.Id , T1.Maker FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker GROUP BY T1.Id HAVING count(*) >= 2 INTERSECT SELECT T1.Id , T1.Maker FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker JOIN CAR_NAMES AS T3 ON T2.model = T3.model GROUP BY T1.Id HAVING count(*) > 3;	car_1
SELECT T1.Id , T1.Maker FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker GROUP BY T1.Id HAVING count(*) >= 2 INTERSECT SELECT T1.Id , T1.Maker FROM CAR_MAKERS AS T1 JOIN MODEL_LIST AS T2 ON T1.Id = T2.Maker JOIN CAR_NAMES AS T3 ON T2.model = T3.model GROUP BY T1.Id HAVING count(*) > 3;	car_1
SELECT T1.countryId , T1.CountryName FROM Countries AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country GROUP BY T1.countryId HAVING count(*) > 3 UNION SELECT T1.countryId , T1.CountryName FROM Countries AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country JOIN MODEL_LIST AS T3 ON T2.Id = T3.Maker WHERE T3.Model = 'fiat';	car_1
SELECT T1.countryId , T1.CountryName FROM Countries AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country GROUP BY T1.countryId HAVING count(*) > 3 UNION SELECT T1.countryId , T1.CountryName FROM Countries AS T1 JOIN CAR_MAKERS AS T2 ON T1.CountryId = T2.Country JOIN MODEL_LIST AS T3 ON T2.Id = T3.Maker WHERE T3.Model = 'fiat';	car_1
SELECT Country FROM AIRLINES WHERE Airline = "JetBlue Airways"	flight_2
SELECT Country FROM AIRLINES WHERE Airline = "JetBlue Airways"	flight_2
SELECT Abbreviation FROM AIRLINES WHERE Airline = "JetBlue Airways"	flight_2
SELECT Abbreviation FROM AIRLINES WHERE Airline = "JetBlue Airways"	flight_2
SELECT Airline , Abbreviation FROM AIRLINES WHERE Country = "USA"	flight_2
SELECT Airline , Abbreviation FROM AIRLINES WHERE Country = "USA"	flight_2
SELECT AirportCode , AirportName FROM AIRPORTS WHERE city = "Anthony"	flight_2
SELECT AirportCode , AirportName FROM AIRPORTS WHERE city = "Anthony"	flight_2
SELECT count(*) FROM AIRLINES	flight_2
SELECT count(*) FROM AIRLINES	flight_2
SELECT count(*) FROM AIRPORTS	flight_2
SELECT count(*) FROM AIRPORTS	flight_2
SELECT count(*) FROM FLIGHTS	flight_2
SELECT count(*) FROM FLIGHTS	flight_2
SELECT Airline FROM AIRLINES WHERE Abbreviation = "UAL"	flight_2
SELECT Airline FROM AIRLINES WHERE Abbreviation = "UAL"	flight_2
SELECT count(*) FROM AIRLINES WHERE Country = "USA"	flight_2
SELECT count(*) FROM AIRLINES WHERE Country = "USA"	flight_2
SELECT City , Country FROM AIRPORTS WHERE AirportName = "Alton"	flight_2
SELECT City , Country FROM AIRPORTS WHERE AirportName = "Alton"	flight_2
SELECT AirportName FROM AIRPORTS WHERE AirportCode = "AKO"	flight_2
SELECT AirportName FROM AIRPORTS WHERE AirportCode = "AKO"	flight_2
SELECT AirportName FROM AIRPORTS WHERE City = "Aberdeen"	flight_2
SELECT AirportName FROM AIRPORTS WHERE City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS WHERE SourceAirport = "APG"	flight_2
SELECT count(*) FROM FLIGHTS WHERE SourceAirport = "APG"	flight_2
SELECT count(*) FROM FLIGHTS WHERE DestAirport = "ATO"	flight_2
SELECT count(*) FROM FLIGHTS WHERE DestAirport = "ATO"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.SourceAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.SourceAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode JOIN AIRPORTS AS T3 ON T1.SourceAirport = T3.AirportCode WHERE T2.City = "Ashley" AND T3.City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode JOIN AIRPORTS AS T3 ON T1.SourceAirport = T3.AirportCode WHERE T2.City = "Ashley" AND T3.City = "Aberdeen"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRLINES AS T2 ON T1.Airline = T2.uid WHERE T2.Airline = "JetBlue Airways"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRLINES AS T2 ON T1.Airline = T2.uid WHERE T2.Airline = "JetBlue Airways"	flight_2
SELECT count(*) FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T2.Airline = T1.uid WHERE T1.Airline = "United Airlines" AND T2.DestAirport = "ASY"	flight_2
SELECT count(*) FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T2.Airline = T1.uid WHERE T1.Airline = "United Airlines" AND T2.DestAirport = "ASY"	flight_2
SELECT count(*) FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T2.Airline = T1.uid WHERE T1.Airline = "United Airlines" AND T2.SourceAirport = "AHD"	flight_2
SELECT count(*) FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T2.Airline = T1.uid WHERE T1.Airline = "United Airlines" AND T2.SourceAirport = "AHD"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode JOIN AIRLINES AS T3 ON T3.uid = T1.Airline WHERE T2.City = "Aberdeen" AND T3.Airline = "United Airlines"	flight_2
SELECT count(*) FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode JOIN AIRLINES AS T3 ON T3.uid = T1.Airline WHERE T2.City = "Aberdeen" AND T3.Airline = "United Airlines"	flight_2
SELECT T1.City FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.DestAirport GROUP BY T1.City ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.City FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.DestAirport GROUP BY T1.City ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.City FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.SourceAirport GROUP BY T1.City ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.City FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.SourceAirport GROUP BY T1.City ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.AirportCode FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.DestAirport OR T1.AirportCode = T2.SourceAirport GROUP BY T1.AirportCode ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.AirportCode FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.DestAirport OR T1.AirportCode = T2.SourceAirport GROUP BY T1.AirportCode ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.AirportCode FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.DestAirport OR T1.AirportCode = T2.SourceAirport GROUP BY T1.AirportCode ORDER BY count(*) LIMIT 1	flight_2
SELECT T1.AirportCode FROM AIRPORTS AS T1 JOIN FLIGHTS AS T2 ON T1.AirportCode = T2.DestAirport OR T1.AirportCode = T2.SourceAirport GROUP BY T1.AirportCode ORDER BY count(*) LIMIT 1	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline ORDER BY count(*) DESC LIMIT 1	flight_2
SELECT T1.Abbreviation , T1.Country FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline ORDER BY count(*) LIMIT 1	flight_2
SELECT T1.Abbreviation , T1.Country FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline ORDER BY count(*) LIMIT 1	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "AHD"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "AHD"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.DestAirport = "AHD"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.DestAirport = "AHD"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "APG" INTERSECT SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "CVO"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "APG" INTERSECT SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "CVO"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "CVO" EXCEPT SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "APG"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "CVO" EXCEPT SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline WHERE T2.SourceAirport = "APG"	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline HAVING count(*) > 10	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline HAVING count(*) > 10	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline HAVING count(*) < 200	flight_2
SELECT T1.Airline FROM AIRLINES AS T1 JOIN FLIGHTS AS T2 ON T1.uid = T2.Airline GROUP BY T1.Airline HAVING count(*) < 200	flight_2
SELECT T1.FlightNo FROM FLIGHTS AS T1 JOIN AIRLINES AS T2 ON T2.uid = T1.Airline WHERE T2.Airline = "United Airlines"	flight_2
SELECT T1.FlightNo FROM FLIGHTS AS T1 JOIN AIRLINES AS T2 ON T2.uid = T1.Airline WHERE T2.Airline = "United Airlines"	flight_2
SELECT FlightNo FROM FLIGHTS WHERE SourceAirport = "APG"	flight_2
SELECT FlightNo FROM FLIGHTS WHERE SourceAirport = "APG"	flight_2
SELECT FlightNo FROM FLIGHTS WHERE DestAirport = "APG"	flight_2
SELECT FlightNo FROM FLIGHTS WHERE DestAirport = "APG"	flight_2
SELECT T1.FlightNo FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.SourceAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT T1.FlightNo FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.SourceAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT T1.FlightNo FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT T1.FlightNo FROM FLIGHTS AS T1 JOIN AIRPORTS AS T2 ON T1.DestAirport = T2.AirportCode WHERE T2.City = "Aberdeen"	flight_2
SELECT count(*) FROM Flights AS T1 JOIN Airports AS T2 ON T1.DestAirport = T2.AirportCode WHERE T2.city = "Aberdeen" OR T2.city = "Abilene"	flight_2
SELECT count(*) FROM Flights AS T1 JOIN Airports AS T2 ON T1.DestAirport = T2.AirportCode WHERE T2.city = "Aberdeen" OR T2.city = "Abilene"	flight_2
SELECT AirportName FROM Airports WHERE AirportCode NOT IN (SELECT SourceAirport FROM Flights UNION SELECT DestAirport FROM Flights)	flight_2
SELECT AirportName FROM Airports WHERE AirportCode NOT IN (SELECT SourceAirport FROM Flights UNION SELECT DestAirport FROM Flights)	flight_2
SELECT count(*) FROM employee	employee_hire_evaluation
SELECT count(*) FROM employee	employee_hire_evaluation
SELECT name FROM employee ORDER BY age	employee_hire_evaluation
SELECT name FROM employee ORDER BY age	employee_hire_evaluation
SELECT count(*) , city FROM employee GROUP BY city	employee_hire_evaluation
SELECT count(*) , city FROM employee GROUP BY city	employee_hire_evaluation
SELECT city FROM employee WHERE age < 30 GROUP BY city HAVING count(*) > 1	employee_hire_evaluation
SELECT city FROM employee WHERE age < 30 GROUP BY city HAVING count(*) > 1	employee_hire_evaluation
SELECT count(*) , LOCATION FROM shop GROUP BY LOCATION	employee_hire_evaluation
SELECT count(*) , LOCATION FROM shop GROUP BY LOCATION	employee_hire_evaluation
SELECT manager_name , district FROM shop ORDER BY number_products DESC LIMIT 1	employee_hire_evaluation
SELECT manager_name , district FROM shop ORDER BY number_products DESC LIMIT 1	employee_hire_evaluation
SELECT min(Number_products) , max(Number_products) FROM shop	employee_hire_evaluation
SELECT min(Number_products) , max(Number_products) FROM shop	employee_hire_evaluation
SELECT name , LOCATION , district FROM shop ORDER BY number_products DESC	employee_hire_evaluation
SELECT name , LOCATION , district FROM shop ORDER BY number_products DESC	employee_hire_evaluation
SELECT name FROM shop WHERE number_products > (SELECT avg(number_products) FROM shop)	employee_hire_evaluation
SELECT name FROM shop WHERE number_products > (SELECT avg(number_products) FROM shop)	employee_hire_evaluation
SELECT t1.name FROM employee AS t1 JOIN evaluation AS t2 ON t1.Employee_ID = t2.Employee_ID GROUP BY t2.Employee_ID ORDER BY count(*) DESC LIMIT 1	employee_hire_evaluation
SELECT t1.name FROM employee AS t1 JOIN evaluation AS t2 ON t1.Employee_ID = t2.Employee_ID GROUP BY t2.Employee_ID ORDER BY count(*) DESC LIMIT 1	employee_hire_evaluation
SELECT t1.name FROM employee AS t1 JOIN evaluation AS t2 ON t1.Employee_ID = t2.Employee_ID ORDER BY t2.bonus DESC LIMIT 1	employee_hire_evaluation
SELECT t1.name FROM employee AS t1 JOIN evaluation AS t2 ON t1.Employee_ID = t2.Employee_ID ORDER BY t2.bonus DESC LIMIT 1	employee_hire_evaluation
SELECT name FROM employee WHERE Employee_ID NOT IN (SELECT Employee_ID FROM evaluation)	employee_hire_evaluation
SELECT name FROM employee WHERE Employee_ID NOT IN (SELECT Employee_ID FROM evaluation)	employee_hire_evaluation
SELECT t2.name FROM hiring AS t1 JOIN shop AS t2 ON t1.shop_id = t2.shop_id GROUP BY t1.shop_id ORDER BY count(*) DESC LIMIT 1	employee_hire_evaluation
SELECT t2.name FROM hiring AS t1 JOIN shop AS t2 ON t1.shop_id = t2.shop_id GROUP BY t1.shop_id ORDER BY count(*) DESC LIMIT 1	employee_hire_evaluation
SELECT name FROM shop WHERE shop_id NOT IN (SELECT shop_id FROM hiring)	employee_hire_evaluation
SELECT name FROM shop WHERE shop_id NOT IN (SELECT shop_id FROM hiring)	employee_hire_evaluation
SELECT count(*) , t2.name FROM hiring AS t1 JOIN shop AS t2 ON t1.shop_id = t2.shop_id GROUP BY t2.name	employee_hire_evaluation
SELECT count(*) , t2.name FROM hiring AS t1 JOIN shop AS t2 ON t1.shop_id = t2.shop_id GROUP BY t2.name	employee_hire_evaluation
SELECT sum(bonus) FROM evaluation	employee_hire_evaluation
SELECT sum(bonus) FROM evaluation	employee_hire_evaluation
SELECT * FROM hiring	employee_hire_evaluation
SELECT * FROM hiring	employee_hire_evaluation
SELECT district FROM shop WHERE Number_products < 3000 INTERSECT SELECT district FROM shop WHERE Number_products > 10000	employee_hire_evaluation
SELECT district FROM shop WHERE Number_products < 3000 INTERSECT SELECT district FROM shop WHERE Number_products > 10000	employee_hire_evaluation
SELECT count(DISTINCT LOCATION) FROM shop	employee_hire_evaluation
SELECT count(DISTINCT LOCATION) FROM shop	employee_hire_evaluation
SELECT count(*) FROM Documents	cre_Doc_Template_Mgt
SELECT count(*) FROM Documents	cre_Doc_Template_Mgt
SELECT document_id , document_name , document_description FROM Documents	cre_Doc_Template_Mgt
SELECT document_id , document_name , document_description FROM Documents	cre_Doc_Template_Mgt
SELECT document_name , template_id FROM Documents WHERE Document_Description LIKE "%w%"	cre_Doc_Template_Mgt
SELECT document_name , template_id FROM Documents WHERE Document_Description LIKE "%w%"	cre_Doc_Template_Mgt
SELECT document_id , template_id , Document_Description FROM Documents WHERE document_name = "Robbin CV"	cre_Doc_Template_Mgt
SELECT document_id , template_id , Document_Description FROM Documents WHERE document_name = "Robbin CV"	cre_Doc_Template_Mgt
SELECT count(DISTINCT template_id) FROM Documents	cre_Doc_Template_Mgt
SELECT count(DISTINCT template_id) FROM Documents	cre_Doc_Template_Mgt
SELECT count(*) FROM Documents AS T1 JOIN Templates AS T2 ON T1.Template_ID = T2.Template_ID WHERE T2.Template_Type_Code = 'PPT'	cre_Doc_Template_Mgt
SELECT count(*) FROM Documents AS T1 JOIN Templates AS T2 ON T1.Template_ID = T2.Template_ID WHERE T2.Template_Type_Code = 'PPT'	cre_Doc_Template_Mgt
SELECT template_id , count(*) FROM Documents GROUP BY template_id	cre_Doc_Template_Mgt
SELECT template_id , count(*) FROM Documents GROUP BY template_id	cre_Doc_Template_Mgt
SELECT T1.template_id , T2.Template_Type_Code FROM Documents AS T1 JOIN Templates AS T2 ON T1.template_id = T2.template_id GROUP BY T1.template_id ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT T1.template_id , T2.Template_Type_Code FROM Documents AS T1 JOIN Templates AS T2 ON T1.template_id = T2.template_id GROUP BY T1.template_id ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT template_id FROM Documents GROUP BY template_id HAVING count(*) > 1	cre_Doc_Template_Mgt
SELECT template_id FROM Documents GROUP BY template_id HAVING count(*) > 1	cre_Doc_Template_Mgt
SELECT template_id FROM Templates EXCEPT SELECT template_id FROM Documents	cre_Doc_Template_Mgt
SELECT template_id FROM Templates EXCEPT SELECT template_id FROM Documents	cre_Doc_Template_Mgt
SELECT count(*) FROM Templates	cre_Doc_Template_Mgt
SELECT count(*) FROM Templates	cre_Doc_Template_Mgt
SELECT template_id , version_number , template_type_code FROM Templates	cre_Doc_Template_Mgt
SELECT template_id , version_number , template_type_code FROM Templates	cre_Doc_Template_Mgt
SELECT DISTINCT template_type_code FROM Templates	cre_Doc_Template_Mgt
SELECT DISTINCT template_type_code FROM Templates	cre_Doc_Template_Mgt
SELECT template_id FROM Templates WHERE template_type_code = "PP" OR template_type_code = "PPT"	cre_Doc_Template_Mgt
SELECT template_id FROM Templates WHERE template_type_code = "PP" OR template_type_code = "PPT"	cre_Doc_Template_Mgt
SELECT count(*) FROM Templates WHERE template_type_code = "CV"	cre_Doc_Template_Mgt
SELECT count(*) FROM Templates WHERE template_type_code = "CV"	cre_Doc_Template_Mgt
SELECT version_number , template_type_code FROM Templates WHERE version_number > 5	cre_Doc_Template_Mgt
SELECT version_number , template_type_code FROM Templates WHERE version_number > 5	cre_Doc_Template_Mgt
SELECT template_type_code , count(*) FROM Templates GROUP BY template_type_code	cre_Doc_Template_Mgt
SELECT template_type_code , count(*) FROM Templates GROUP BY template_type_code	cre_Doc_Template_Mgt
SELECT template_type_code FROM Templates GROUP BY template_type_code ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT template_type_code FROM Templates GROUP BY template_type_code ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT template_type_code FROM Templates GROUP BY template_type_code HAVING count(*) < 3	cre_Doc_Template_Mgt
SELECT template_type_code FROM Templates GROUP BY template_type_code HAVING count(*) < 3	cre_Doc_Template_Mgt
SELECT min(Version_Number) , template_type_code FROM Templates	cre_Doc_Template_Mgt
SELECT min(Version_Number) , template_type_code FROM Templates	cre_Doc_Template_Mgt
SELECT T1.template_type_code FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id WHERE T2.document_name = "Data base"	cre_Doc_Template_Mgt
SELECT T1.template_type_code FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id WHERE T2.document_name = "Data base"	cre_Doc_Template_Mgt
SELECT T2.document_name FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id WHERE T1.template_type_code = "BK"	cre_Doc_Template_Mgt
SELECT T2.document_name FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id WHERE T1.template_type_code = "BK"	cre_Doc_Template_Mgt
SELECT T1.template_type_code , count(*) FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id GROUP BY T1.template_type_code	cre_Doc_Template_Mgt
SELECT T1.template_type_code , count(*) FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id GROUP BY T1.template_type_code	cre_Doc_Template_Mgt
SELECT T1.template_type_code FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id GROUP BY T1.template_type_code ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT T1.template_type_code FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id GROUP BY T1.template_type_code ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT template_type_code FROM Templates EXCEPT SELECT template_type_code FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id	cre_Doc_Template_Mgt
SELECT template_type_code FROM Templates EXCEPT SELECT template_type_code FROM Templates AS T1 JOIN Documents AS T2 ON T1.template_id = T2.template_id	cre_Doc_Template_Mgt
SELECT template_type_code , template_type_description FROM Ref_template_types	cre_Doc_Template_Mgt
SELECT template_type_code , template_type_description FROM Ref_template_types	cre_Doc_Template_Mgt
SELECT template_type_description FROM Ref_template_types WHERE template_type_code = "AD"	cre_Doc_Template_Mgt
SELECT template_type_description FROM Ref_template_types WHERE template_type_code = "AD"	cre_Doc_Template_Mgt
SELECT template_type_code FROM Ref_template_types WHERE template_type_description = "Book"	cre_Doc_Template_Mgt
SELECT template_type_code FROM Ref_template_types WHERE template_type_description = "Book"	cre_Doc_Template_Mgt
SELECT DISTINCT T1.template_type_description FROM Ref_template_types AS T1 JOIN Templates AS T2 ON T1.template_type_code = T2.template_type_code JOIN Documents AS T3 ON T2.Template_ID = T3.template_ID	cre_Doc_Template_Mgt
SELECT DISTINCT T1.template_type_description FROM Ref_template_types AS T1 JOIN Templates AS T2 ON T1.template_type_code = T2.template_type_code JOIN Documents AS T3 ON T2.Template_ID = T3.template_ID	cre_Doc_Template_Mgt
SELECT T2.template_id FROM Ref_template_types AS T1 JOIN Templates AS T2 ON T1.template_type_code = T2.template_type_code WHERE T1.template_type_description = "Presentation"	cre_Doc_Template_Mgt
SELECT T2.template_id FROM Ref_template_types AS T1 JOIN Templates AS T2 ON T1.template_type_code = T2.template_type_code WHERE T1.template_type_description = "Presentation"	cre_Doc_Template_Mgt
SELECT count(*) FROM Paragraphs	cre_Doc_Template_Mgt
SELECT count(*) FROM Paragraphs	cre_Doc_Template_Mgt
SELECT count(*) FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_ID = T2.document_ID WHERE T2.document_name = 'Summer Show'	cre_Doc_Template_Mgt
SELECT count(*) FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_ID = T2.document_ID WHERE T2.document_name = 'Summer Show'	cre_Doc_Template_Mgt
SELECT Other_Details FROM Paragraphs WHERE paragraph_text = 'Korea'	cre_Doc_Template_Mgt
SELECT Other_Details FROM Paragraphs WHERE paragraph_text = 'Korea'	cre_Doc_Template_Mgt
SELECT T1.paragraph_id , T1.paragraph_text FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id WHERE T2.Document_Name = 'Welcome to NY'	cre_Doc_Template_Mgt
SELECT T1.paragraph_id , T1.paragraph_text FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id WHERE T2.Document_Name = 'Welcome to NY'	cre_Doc_Template_Mgt
SELECT T1.paragraph_text FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id WHERE T2.document_name = "Customer reviews"	cre_Doc_Template_Mgt
SELECT T1.paragraph_text FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id WHERE T2.document_name = "Customer reviews"	cre_Doc_Template_Mgt
SELECT document_id , count(*) FROM Paragraphs GROUP BY document_id ORDER BY document_id	cre_Doc_Template_Mgt
SELECT document_id , count(*) FROM Paragraphs GROUP BY document_id ORDER BY document_id	cre_Doc_Template_Mgt
SELECT T1.document_id , T2.document_name , count(*) FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id GROUP BY T1.document_id	cre_Doc_Template_Mgt
SELECT T1.document_id , T2.document_name , count(*) FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id GROUP BY T1.document_id	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs GROUP BY document_id HAVING count(*) >= 2	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs GROUP BY document_id HAVING count(*) >= 2	cre_Doc_Template_Mgt
SELECT T1.document_id , T2.document_name FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id GROUP BY T1.document_id ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT T1.document_id , T2.document_name FROM Paragraphs AS T1 JOIN Documents AS T2 ON T1.document_id = T2.document_id GROUP BY T1.document_id ORDER BY count(*) DESC LIMIT 1	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs GROUP BY document_id ORDER BY count(*) ASC LIMIT 1	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs GROUP BY document_id ORDER BY count(*) ASC LIMIT 1	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs GROUP BY document_id HAVING count(*) BETWEEN 1 AND 2	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs GROUP BY document_id HAVING count(*) BETWEEN 1 AND 2	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs WHERE paragraph_text = 'Brazil' INTERSECT SELECT document_id FROM Paragraphs WHERE paragraph_text = 'Ireland'	cre_Doc_Template_Mgt
SELECT document_id FROM Paragraphs WHERE paragraph_text = 'Brazil' INTERSECT SELECT document_id FROM Paragraphs WHERE paragraph_text = 'Ireland'	cre_Doc_Template_Mgt
SELECT count(*) FROM teacher	course_teach
SELECT count(*) FROM teacher	course_teach
SELECT Name FROM teacher ORDER BY Age ASC	course_teach
SELECT Name FROM teacher ORDER BY Age ASC	course_teach
SELECT Age , Hometown FROM teacher	course_teach
SELECT Age , Hometown FROM teacher	course_teach
SELECT Name FROM teacher WHERE Hometown != "Little Lever Urban Distric"	course_teach
SELECT Name FROM teacher WHERE Hometown != "Little Lever Urban Distric"	course_teach
SELECT Name FROM teacher WHERE Age = 32 OR Age = 33	course_teach
SELECT Name FROM teacher WHERE Age = 32 OR Age = 33	course_teach
SELECT Hometown FROM teacher ORDER BY Age ASC LIMIT 1	course_teach
SELECT Hometown FROM teacher ORDER BY Age ASC LIMIT 1	course_teach
SELECT Hometown , COUNT(*) FROM teacher GROUP BY Hometown	course_teach
SELECT Hometown , COUNT(*) FROM teacher GROUP BY Hometown	course_teach
SELECT Hometown FROM teacher GROUP BY Hometown ORDER BY COUNT(*) DESC LIMIT 1	course_teach
SELECT Hometown FROM teacher GROUP BY Hometown ORDER BY COUNT(*) DESC LIMIT 1	course_teach
SELECT Hometown FROM teacher GROUP BY Hometown HAVING COUNT(*) >= 2	course_teach
SELECT Hometown FROM teacher GROUP BY Hometown HAVING COUNT(*) >= 2	course_teach
SELECT T3.Name , T2.Course FROM course_arrange AS T1 JOIN course AS T2 ON T1.Course_ID = T2.Course_ID JOIN teacher AS T3 ON T1.Teacher_ID = T3.Teacher_ID	course_teach
SELECT T3.Name , T2.Course FROM course_arrange AS T1 JOIN course AS T2 ON T1.Course_ID = T2.Course_ID JOIN teacher AS T3 ON T1.Teacher_ID = T3.Teacher_ID	course_teach
SELECT T3.Name , T2.Course FROM course_arrange AS T1 JOIN course AS T2 ON T1.Course_ID = T2.Course_ID JOIN teacher AS T3 ON T1.Teacher_ID = T3.Teacher_ID ORDER BY T3.Name	course_teach
SELECT T3.Name , T2.Course FROM course_arrange AS T1 JOIN course AS T2 ON T1.Course_ID = T2.Course_ID JOIN teacher AS T3 ON T1.Teacher_ID = T3.Teacher_ID ORDER BY T3.Name	course_teach
SELECT T3.Name FROM course_arrange AS T1 JOIN course AS T2 ON T1.Course_ID = T2.Course_ID JOIN teacher AS T3 ON T1.Teacher_ID = T3.Teacher_ID WHERE T2.Course = "Math"	course_teach
SELECT T3.Name FROM course_arrange AS T1 JOIN course AS T2 ON T1.Course_ID = T2.Course_ID JOIN teacher AS T3 ON T1.Teacher_ID = T3.Teacher_ID WHERE T2.Course = "Math"	course_teach
SELECT T2.Name , COUNT(*) FROM course_arrange AS T1 JOIN teacher AS T2 ON T1.Teacher_ID = T2.Teacher_ID GROUP BY T2.Name	course_teach
SELECT T2.Name , COUNT(*) FROM course_arrange AS T1 JOIN teacher AS T2 ON T1.Teacher_ID = T2.Teacher_ID GROUP BY T2.Name	course_teach
SELECT T2.Name FROM course_arrange AS T1 JOIN teacher AS T2 ON T1.Teacher_ID = T2.Teacher_ID GROUP BY T2.Name HAVING COUNT(*) >= 2	course_teach
SELECT T2.Name FROM course_arrange AS T1 JOIN teacher AS T2 ON T1.Teacher_ID = T2.Teacher_ID GROUP BY T2.Name HAVING COUNT(*) >= 2	course_teach
SELECT Name FROM teacher WHERE Teacher_id NOT IN (SELECT Teacher_id FROM course_arrange)	course_teach
SELECT Name FROM teacher WHERE Teacher_id NOT IN (SELECT Teacher_id FROM course_arrange)	course_teach
SELECT count(*) FROM players	wta_1
SELECT count(*) FROM players	wta_1
SELECT count(*) FROM matches	wta_1
SELECT count(*) FROM matches	wta_1
SELECT first_name , birth_date FROM players WHERE country_code = 'USA'	wta_1
SELECT first_name , birth_date FROM players WHERE country_code = 'USA'	wta_1
SELECT avg(loser_age) , avg(winner_age) FROM matches	wta_1
SELECT avg(loser_age) , avg(winner_age) FROM matches	wta_1
SELECT avg(winner_rank) FROM matches	wta_1
SELECT avg(winner_rank) FROM matches	wta_1
SELECT min(loser_rank) FROM matches	wta_1
SELECT min(loser_rank) FROM matches	wta_1
SELECT count(DISTINCT country_code) FROM players	wta_1
SELECT count(DISTINCT country_code) FROM players	wta_1
SELECT count(DISTINCT loser_name) FROM matches	wta_1
SELECT count(DISTINCT loser_name) FROM matches	wta_1
SELECT tourney_name FROM matches GROUP BY tourney_name HAVING count(*) > 10	wta_1
SELECT tourney_name FROM matches GROUP BY tourney_name HAVING count(*) > 10	wta_1
SELECT winner_name FROM matches WHERE YEAR = 2013 INTERSECT SELECT winner_name FROM matches WHERE YEAR = 2016	wta_1
SELECT winner_name FROM matches WHERE YEAR = 2013 INTERSECT SELECT winner_name FROM matches WHERE YEAR = 2016	wta_1
SELECT count(*) FROM matches WHERE YEAR = 2013 OR YEAR = 2016	wta_1
SELECT count(*) FROM matches WHERE YEAR = 2013 OR YEAR = 2016	wta_1
SELECT T1.country_code , T1.first_name FROM players AS T1 JOIN matches AS T2 ON T1.player_id = T2.winner_id WHERE T2.tourney_name = 'WTA Championships' INTERSECT SELECT T1.country_code , T1.first_name FROM players AS T1 JOIN matches AS T2 ON T1.player_id = T2.winner_id WHERE T2.tourney_name = 'Australian Open'	wta_1
SELECT T1.country_code , T1.first_name FROM players AS T1 JOIN matches AS T2 ON T1.player_id = T2.winner_id WHERE T2.tourney_name = 'WTA Championships' INTERSECT SELECT T1.country_code , T1.first_name FROM players AS T1 JOIN matches AS T2 ON T1.player_id = T2.winner_id WHERE T2.tourney_name = 'Australian Open'	wta_1
SELECT first_name , country_code FROM players ORDER BY birth_date LIMIT 1	wta_1
SELECT first_name , country_code FROM players ORDER BY birth_date LIMIT 1	wta_1
SELECT first_name , last_name FROM players ORDER BY birth_date	wta_1
SELECT first_name , last_name FROM players ORDER BY birth_date	wta_1
SELECT first_name , last_name FROM players WHERE hand = 'L' ORDER BY birth_date	wta_1
SELECT first_name , last_name FROM players WHERE hand = 'L' ORDER BY birth_date	wta_1
SELECT T1.country_code , T1.first_name FROM players AS T1 JOIN rankings AS T2 ON T1.player_id = T2.player_id ORDER BY T2.tours DESC LIMIT 1	wta_1
SELECT T1.country_code , T1.first_name FROM players AS T1 JOIN rankings AS T2 ON T1.player_id = T2.player_id ORDER BY T2.tours DESC LIMIT 1	wta_1
SELECT YEAR FROM matches GROUP BY YEAR ORDER BY count(*) DESC LIMIT 1	wta_1
SELECT YEAR FROM matches GROUP BY YEAR ORDER BY count(*) DESC LIMIT 1	wta_1
SELECT winner_name , winner_rank_points FROM matches GROUP BY winner_name ORDER BY count(*) DESC LIMIT 1	wta_1
SELECT winner_name , winner_rank_points FROM matches GROUP BY winner_name ORDER BY count(*) DESC LIMIT 1	wta_1
SELECT winner_name FROM matches WHERE tourney_name = 'Australian Open' ORDER BY winner_rank_points DESC LIMIT 1	wta_1
SELECT winner_name FROM matches WHERE tourney_name = 'Australian Open' ORDER BY winner_rank_points DESC LIMIT 1	wta_1
SELECT winner_name , loser_name FROM matches ORDER BY minutes DESC LIMIT 1	wta_1
SELECT winner_name , loser_name FROM matches ORDER BY minutes DESC LIMIT 1	wta_1
SELECT avg(ranking) , T1.first_name FROM players AS T1 JOIN rankings AS T2 ON T1.player_id = T2.player_id GROUP BY T1.first_name	wta_1
SELECT avg(ranking) , T1.first_name FROM players AS T1 JOIN rankings AS T2 ON T1.player_id = T2.player_id GROUP BY T1.first_name	wta_1
SELECT sum(ranking_points) , T1.first_name FROM players AS T1 JOIN rankings AS T2 ON T1.player_id = T2.player_id GROUP BY T1.first_name	wta_1
SELECT sum(ranking_points) , T1.first_name FROM players AS T1 JOIN rankings AS T2 ON T1.player_id = T2.player_id GROUP BY T1.first_name	wta_1
SELECT count(*) , country_code FROM players GROUP BY country_code	wta_1
SELECT count(*) , country_code FROM players GROUP BY country_code	wta_1
SELECT country_code FROM players GROUP BY country_code ORDER BY count(*) DESC LIMIT 1	wta_1
SELECT country_code FROM players GROUP BY country_code ORDER BY count(*) DESC LIMIT 1	wta_1
SELECT country_code FROM players GROUP BY country_code HAVING count(*) > 50	wta_1
SELECT country_code FROM players GROUP BY country_code HAVING count(*) > 50	wta_1
SELECT sum(tours) , ranking_date FROM rankings GROUP BY ranking_date	wta_1
SELECT sum(tours) , ranking_date FROM rankings GROUP BY ranking_date	wta_1
SELECT count(*) , YEAR FROM matches GROUP BY YEAR	wta_1
SELECT count(*) , YEAR FROM matches GROUP BY YEAR	wta_1
SELECT DISTINCT winner_name , winner_rank FROM matches ORDER BY winner_age LIMIT 3	wta_1
SELECT DISTINCT winner_name , winner_rank FROM matches ORDER BY winner_age LIMIT 3	wta_1
SELECT count(DISTINCT winner_name) FROM matches WHERE tourney_name = 'WTA Championships' AND winner_hand = 'L'	wta_1
SELECT count(DISTINCT winner_name) FROM matches WHERE tourney_name = 'WTA Championships' AND winner_hand = 'L'	wta_1
SELECT T1.first_name , T1.country_code , T1.birth_date FROM players AS T1 JOIN matches AS T2 ON T1.player_id = T2.winner_id ORDER BY T2.winner_rank_points DESC LIMIT 1	wta_1
SELECT T1.first_name , T1.country_code , T1.birth_date FROM players AS T1 JOIN matches AS T2 ON T1.player_id = T2.winner_id ORDER BY T2.winner_rank_points DESC LIMIT 1	wta_1
SELECT count(*) , hand FROM players GROUP BY hand	wta_1
SELECT count(*) , hand FROM players GROUP BY hand	wta_1
SELECT count(*) FROM ship WHERE disposition_of_ship = 'Captured'	battle_death
SELECT name , tonnage FROM ship ORDER BY name DESC	battle_death
SELECT name , date FROM battle	battle_death
SELECT max(killed) , min(killed) FROM death	battle_death
SELECT avg(injured) FROM death	battle_death
SELECT T1.killed , T1.injured FROM death AS T1 JOIN ship AS t2 ON T1.caused_by_ship_id = T2.id WHERE T2.tonnage = 't'	battle_death
SELECT name , RESULT FROM battle WHERE bulgarian_commander != 'Boril'	battle_death
SELECT DISTINCT T1.id , T1.name FROM battle AS T1 JOIN ship AS T2 ON T1.id = T2.lost_in_battle WHERE T2.ship_type = 'Brig'	battle_death
SELECT T1.id , T1.name FROM battle AS T1 JOIN ship AS T2 ON T1.id = T2.lost_in_battle JOIN death AS T3 ON T2.id = T3.caused_by_ship_id GROUP BY T1.id HAVING sum(T3.killed) > 10	battle_death
SELECT T2.id , T2.name FROM death AS T1 JOIN ship AS t2 ON T1.caused_by_ship_id = T2.id GROUP BY T2.id ORDER BY count(*) DESC LIMIT 1	battle_death
SELECT name FROM battle WHERE bulgarian_commander = 'Kaloyan' AND latin_commander = 'Baldwin I'	battle_death
SELECT count(DISTINCT RESULT) FROM battle	battle_death
SELECT count(*) FROM battle WHERE id NOT IN ( SELECT lost_in_battle FROM ship WHERE tonnage = '225' );	battle_death
SELECT T1.name , T1.date FROM battle AS T1 JOIN ship AS T2 ON T1.id = T2.lost_in_battle WHERE T2.name = 'Lettice' INTERSECT SELECT T1.name , T1.date FROM battle AS T1 JOIN ship AS T2 ON T1.id = T2.lost_in_battle WHERE T2.name = 'HMS Atalanta'	battle_death
SELECT name , RESULT , bulgarian_commander FROM battle EXCEPT SELECT T1.name , T1.result , T1.bulgarian_commander FROM battle AS T1 JOIN ship AS T2 ON T1.id = T2.lost_in_battle WHERE T2.location = 'English Channel'	battle_death
SELECT note FROM death WHERE note LIKE '%East%'	battle_death
SELECT line_1 , line_2 FROM addresses	student_transcripts_tracking
SELECT line_1 , line_2 FROM addresses	student_transcripts_tracking
SELECT count(*) FROM Courses	student_transcripts_tracking
SELECT count(*) FROM Courses	student_transcripts_tracking
SELECT course_description FROM Courses WHERE course_name = 'math'	student_transcripts_tracking
SELECT course_description FROM Courses WHERE course_name = 'math'	student_transcripts_tracking
SELECT zip_postcode FROM Addresses WHERE city = 'Port Chelsea'	student_transcripts_tracking
SELECT zip_postcode FROM Addresses WHERE city = 'Port Chelsea'	student_transcripts_tracking
SELECT T2.department_name , T1.department_id FROM Degree_Programs AS T1 JOIN Departments AS T2 ON T1.department_id = T2.department_id GROUP BY T1.department_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T2.department_name , T1.department_id FROM Degree_Programs AS T1 JOIN Departments AS T2 ON T1.department_id = T2.department_id GROUP BY T1.department_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT count(DISTINCT department_id) FROM Degree_Programs	student_transcripts_tracking
SELECT count(DISTINCT department_id) FROM Degree_Programs	student_transcripts_tracking
SELECT count(DISTINCT degree_summary_name) FROM Degree_Programs	student_transcripts_tracking
SELECT count(DISTINCT degree_summary_name) FROM Degree_Programs	student_transcripts_tracking
SELECT count(*) FROM Departments AS T1 JOIN Degree_Programs AS T2 ON T1.department_id = T2.department_id WHERE T1.department_name = 'engineer'	student_transcripts_tracking
SELECT count(*) FROM Departments AS T1 JOIN Degree_Programs AS T2 ON T1.department_id = T2.department_id WHERE T1.department_name = 'engineer'	student_transcripts_tracking
SELECT section_name , section_description FROM Sections	student_transcripts_tracking
SELECT section_name , section_description FROM Sections	student_transcripts_tracking
SELECT T1.course_name , T1.course_id FROM Courses AS T1 JOIN Sections AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_id HAVING count(*) <= 2	student_transcripts_tracking
SELECT T1.course_name , T1.course_id FROM Courses AS T1 JOIN Sections AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_id HAVING count(*) <= 2	student_transcripts_tracking
SELECT section_name FROM Sections ORDER BY section_name DESC	student_transcripts_tracking
SELECT section_name FROM Sections ORDER BY section_name DESC	student_transcripts_tracking
SELECT T1.semester_name , T1.semester_id FROM Semesters AS T1 JOIN Student_Enrolment AS T2 ON T1.semester_id = T2.semester_id GROUP BY T1.semester_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.semester_name , T1.semester_id FROM Semesters AS T1 JOIN Student_Enrolment AS T2 ON T1.semester_id = T2.semester_id GROUP BY T1.semester_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT department_description FROM Departments WHERE department_name LIKE '%computer%'	student_transcripts_tracking
SELECT department_description FROM Departments WHERE department_name LIKE '%computer%'	student_transcripts_tracking
SELECT T1.first_name , T1.middle_name , T1.last_name , T1.student_id FROM Students AS T1 JOIN Student_Enrolment AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id HAVING count(*) = 2	student_transcripts_tracking
SELECT T1.first_name , T1.middle_name , T1.last_name , T1.student_id FROM Students AS T1 JOIN Student_Enrolment AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id HAVING count(*) = 2	student_transcripts_tracking
SELECT DISTINCT T1.first_name , T1.middle_name , T1.last_name FROM Students AS T1 JOIN Student_Enrolment AS T2 ON T1.student_id = T2.student_id JOIN Degree_Programs AS T3 ON T2.degree_program_id = T3.degree_program_id WHERE T3.degree_summary_name = 'Bachelor'	student_transcripts_tracking
SELECT DISTINCT T1.first_name , T1.middle_name , T1.last_name FROM Students AS T1 JOIN Student_Enrolment AS T2 ON T1.student_id = T2.student_id JOIN Degree_Programs AS T3 ON T2.degree_program_id = T3.degree_program_id WHERE T3.degree_summary_name = 'Bachelor'	student_transcripts_tracking
SELECT T1.degree_summary_name FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id GROUP BY T1.degree_summary_name ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.degree_summary_name FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id GROUP BY T1.degree_summary_name ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.degree_program_id , T1.degree_summary_name FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id GROUP BY T1.degree_program_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.degree_program_id , T1.degree_summary_name FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id GROUP BY T1.degree_program_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.student_id , T1.first_name , T1.middle_name , T1.last_name , count(*) , T1.student_id FROM Students AS T1 JOIN Student_Enrolment AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.student_id , T1.first_name , T1.middle_name , T1.last_name , count(*) , T1.student_id FROM Students AS T1 JOIN Student_Enrolment AS T2 ON T1.student_id = T2.student_id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT semester_name FROM Semesters WHERE semester_id NOT IN( SELECT semester_id FROM Student_Enrolment )	student_transcripts_tracking
SELECT semester_name FROM Semesters WHERE semester_id NOT IN( SELECT semester_id FROM Student_Enrolment )	student_transcripts_tracking
SELECT DISTINCT T1.course_name FROM Courses AS T1 JOIN Student_Enrolment_Courses AS T2 ON T1.course_id = T2.course_id	student_transcripts_tracking
SELECT DISTINCT T1.course_name FROM Courses AS T1 JOIN Student_Enrolment_Courses AS T2 ON T1.course_id = T2.course_id	student_transcripts_tracking
SELECT T1.course_name FROM Courses AS T1 JOIN Student_Enrolment_Courses AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.course_name FROM Courses AS T1 JOIN Student_Enrolment_Courses AS T2 ON T1.course_id = T2.course_id GROUP BY T1.course_name ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.last_name FROM Students AS T1 JOIN Addresses AS T2 ON T1.current_address_id = T2.address_id WHERE T2.state_province_county = 'NorthCarolina' EXCEPT SELECT DISTINCT T3.last_name FROM Students AS T3 JOIN Student_Enrolment AS T4 ON T3.student_id = T4.student_id	student_transcripts_tracking
SELECT T1.last_name FROM Students AS T1 JOIN Addresses AS T2 ON T1.current_address_id = T2.address_id WHERE T2.state_province_county = 'NorthCarolina' EXCEPT SELECT DISTINCT T3.last_name FROM Students AS T3 JOIN Student_Enrolment AS T4 ON T3.student_id = T4.student_id	student_transcripts_tracking
SELECT T2.transcript_date , T1.transcript_id FROM Transcript_Contents AS T1 JOIN Transcripts AS T2 ON T1.transcript_id = T2.transcript_id GROUP BY T1.transcript_id HAVING count(*) >= 2	student_transcripts_tracking
SELECT T2.transcript_date , T1.transcript_id FROM Transcript_Contents AS T1 JOIN Transcripts AS T2 ON T1.transcript_id = T2.transcript_id GROUP BY T1.transcript_id HAVING count(*) >= 2	student_transcripts_tracking
SELECT cell_mobile_number FROM Students WHERE first_name = 'Timmothy' AND last_name = 'Ward'	student_transcripts_tracking
SELECT cell_mobile_number FROM Students WHERE first_name = 'Timmothy' AND last_name = 'Ward'	student_transcripts_tracking
SELECT first_name , middle_name , last_name FROM Students ORDER BY date_first_registered ASC LIMIT 1	student_transcripts_tracking
SELECT first_name , middle_name , last_name FROM Students ORDER BY date_first_registered ASC LIMIT 1	student_transcripts_tracking
SELECT first_name , middle_name , last_name FROM Students ORDER BY date_left ASC LIMIT 1	student_transcripts_tracking
SELECT first_name , middle_name , last_name FROM Students ORDER BY date_left ASC LIMIT 1	student_transcripts_tracking
SELECT first_name FROM Students WHERE current_address_id != permanent_address_id	student_transcripts_tracking
SELECT first_name FROM Students WHERE current_address_id != permanent_address_id	student_transcripts_tracking
SELECT T1.address_id , T1.line_1 , T1.line_2 FROM Addresses AS T1 JOIN Students AS T2 ON T1.address_id = T2.current_address_id GROUP BY T1.address_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T1.address_id , T1.line_1 , T1.line_2 FROM Addresses AS T1 JOIN Students AS T2 ON T1.address_id = T2.current_address_id GROUP BY T1.address_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT avg(transcript_date) FROM Transcripts	student_transcripts_tracking
SELECT avg(transcript_date) FROM Transcripts	student_transcripts_tracking
SELECT transcript_date , other_details FROM Transcripts ORDER BY transcript_date ASC LIMIT 1	student_transcripts_tracking
SELECT transcript_date , other_details FROM Transcripts ORDER BY transcript_date ASC LIMIT 1	student_transcripts_tracking
SELECT count(*) FROM Transcripts	student_transcripts_tracking
SELECT count(*) FROM Transcripts	student_transcripts_tracking
SELECT transcript_date FROM Transcripts ORDER BY transcript_date DESC LIMIT 1	student_transcripts_tracking
SELECT transcript_date FROM Transcripts ORDER BY transcript_date DESC LIMIT 1	student_transcripts_tracking
SELECT count(*) , student_course_id FROM Transcript_Contents GROUP BY student_course_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT count(*) , student_course_id FROM Transcript_Contents GROUP BY student_course_id ORDER BY count(*) DESC LIMIT 1	student_transcripts_tracking
SELECT T2.transcript_date , T1.transcript_id FROM Transcript_Contents AS T1 JOIN Transcripts AS T2 ON T1.transcript_id = T2.transcript_id GROUP BY T1.transcript_id ORDER BY count(*) ASC LIMIT 1	student_transcripts_tracking
SELECT T2.transcript_date , T1.transcript_id FROM Transcript_Contents AS T1 JOIN Transcripts AS T2 ON T1.transcript_id = T2.transcript_id GROUP BY T1.transcript_id ORDER BY count(*) ASC LIMIT 1	student_transcripts_tracking
SELECT DISTINCT T2.semester_id FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id WHERE degree_summary_name = 'Master' INTERSECT SELECT DISTINCT T2.semester_id FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id WHERE degree_summary_name = 'Bachelor'	student_transcripts_tracking
SELECT DISTINCT T2.semester_id FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id WHERE degree_summary_name = 'Master' INTERSECT SELECT DISTINCT T2.semester_id FROM Degree_Programs AS T1 JOIN Student_Enrolment AS T2 ON T1.degree_program_id = T2.degree_program_id WHERE degree_summary_name = 'Bachelor'	student_transcripts_tracking
SELECT count(DISTINCT current_address_id) FROM Students	student_transcripts_tracking
SELECT count(DISTINCT current_address_id) FROM Students	student_transcripts_tracking
SELECT other_student_details FROM Students ORDER BY other_student_details DESC	student_transcripts_tracking
SELECT other_student_details FROM Students ORDER BY other_student_details DESC	student_transcripts_tracking
SELECT section_description FROM Sections WHERE section_name = 'h'	student_transcripts_tracking
SELECT section_description FROM Sections WHERE section_name = 'h'	student_transcripts_tracking
SELECT T1.first_name FROM Students AS T1 JOIN Addresses AS T2 ON T1.permanent_address_id = T2.address_id WHERE T2.country = 'Haiti' UNION SELECT first_name FROM Students WHERE cell_mobile_number = '09700166582'	student_transcripts_tracking
SELECT T1.first_name FROM Students AS T1 JOIN Addresses AS T2 ON T1.permanent_address_id = T2.address_id WHERE T2.country = 'Haiti' UNION SELECT first_name FROM Students WHERE cell_mobile_number = '09700166582'	student_transcripts_tracking
SELECT count(*) FROM area_code_state	voter_1
SELECT contestant_number , contestant_name FROM contestants ORDER BY contestant_name DESC	voter_1
SELECT vote_id , phone_number , state FROM votes	voter_1
SELECT max(area_code) , min(area_code) FROM area_code_state	voter_1
SELECT max(created) FROM votes WHERE state = 'CA'	voter_1
SELECT contestant_name FROM contestants WHERE contestant_name != 'Jessie Alloway'	voter_1
SELECT DISTINCT state , created FROM votes	voter_1
SELECT T1.contestant_number , T1.contestant_name FROM contestants AS T1 JOIN votes AS T2 ON T1.contestant_number = T2.contestant_number GROUP BY T1.contestant_number HAVING count(*) >= 2	voter_1
SELECT T1.contestant_number , T1.contestant_name FROM contestants AS T1 JOIN votes AS T2 ON T1.contestant_number = T2.contestant_number GROUP BY T1.contestant_number ORDER BY count(*) ASC LIMIT 1	voter_1
SELECT count(*) FROM votes WHERE state = 'NY' OR state = 'CA'	voter_1
SELECT count(*) FROM contestants WHERE contestant_number NOT IN ( SELECT contestant_number FROM votes )	voter_1
SELECT T1.area_code FROM area_code_state AS T1 JOIN votes AS T2 ON T1.state = T2.state GROUP BY T1.area_code ORDER BY count(*) DESC LIMIT 1	voter_1
SELECT T2.created , T2.state , T2.phone_number FROM contestants AS T1 JOIN votes AS T2 ON T1.contestant_number = T2.contestant_number WHERE T1.contestant_name = 'Tabatha Gehling'	voter_1
SELECT T3.area_code FROM contestants AS T1 JOIN votes AS T2 ON T1.contestant_number = T2.contestant_number JOIN area_code_state AS T3 ON T2.state = T3.state WHERE T1.contestant_name = 'Tabatha Gehling' INTERSECT SELECT T3.area_code FROM contestants AS T1 JOIN votes AS T2 ON T1.contestant_number = T2.contestant_number JOIN area_code_state AS T3 ON T2.state = T3.state WHERE T1.contestant_name = 'Kelly Clauss'	voter_1
SELECT contestant_name FROM contestants WHERE contestant_name LIKE "%Al%"	voter_1
SELECT count(*) FROM conductor	orchestra
SELECT count(*) FROM conductor	orchestra
SELECT Name FROM conductor ORDER BY Age ASC	orchestra
SELECT Name FROM conductor ORDER BY Age ASC	orchestra
SELECT Name FROM conductor WHERE Nationality != 'USA'	orchestra
SELECT Name FROM conductor WHERE Nationality != 'USA'	orchestra
SELECT Record_Company FROM orchestra ORDER BY Year_of_Founded DESC	orchestra
SELECT Record_Company FROM orchestra ORDER BY Year_of_Founded DESC	orchestra
SELECT avg(Attendance) FROM SHOW	orchestra
SELECT avg(Attendance) FROM SHOW	orchestra
SELECT max(SHARE) , min(SHARE) FROM performance WHERE TYPE != "Live final"	orchestra
SELECT max(SHARE) , min(SHARE) FROM performance WHERE TYPE != "Live final"	orchestra
SELECT count(DISTINCT Nationality) FROM conductor	orchestra
SELECT count(DISTINCT Nationality) FROM conductor	orchestra
SELECT Name FROM conductor ORDER BY Year_of_Work DESC	orchestra
SELECT Name FROM conductor ORDER BY Year_of_Work DESC	orchestra
SELECT Name FROM conductor ORDER BY Year_of_Work DESC LIMIT 1	orchestra
SELECT Name FROM conductor ORDER BY Year_of_Work DESC LIMIT 1	orchestra
SELECT T1.Name , T2.Orchestra FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID	orchestra
SELECT T1.Name , T2.Orchestra FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID	orchestra
SELECT T1.Name FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID GROUP BY T2.Conductor_ID HAVING COUNT(*) > 1	orchestra
SELECT T1.Name FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID GROUP BY T2.Conductor_ID HAVING COUNT(*) > 1	orchestra
SELECT T1.Name FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID GROUP BY T2.Conductor_ID ORDER BY COUNT(*) DESC LIMIT 1	orchestra
SELECT T1.Name FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID GROUP BY T2.Conductor_ID ORDER BY COUNT(*) DESC LIMIT 1	orchestra
SELECT T1.Name FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID WHERE Year_of_Founded > 2008	orchestra
SELECT T1.Name FROM conductor AS T1 JOIN orchestra AS T2 ON T1.Conductor_ID = T2.Conductor_ID WHERE Year_of_Founded > 2008	orchestra
SELECT Record_Company , COUNT(*) FROM orchestra GROUP BY Record_Company	orchestra
SELECT Record_Company , COUNT(*) FROM orchestra GROUP BY Record_Company	orchestra
SELECT Major_Record_Format FROM orchestra GROUP BY Major_Record_Format ORDER BY COUNT(*) ASC	orchestra
SELECT Major_Record_Format FROM orchestra GROUP BY Major_Record_Format ORDER BY COUNT(*) ASC	orchestra
SELECT Record_Company FROM orchestra GROUP BY Record_Company ORDER BY COUNT(*) DESC LIMIT 1	orchestra
SELECT Record_Company FROM orchestra GROUP BY Record_Company ORDER BY COUNT(*) DESC LIMIT 1	orchestra
SELECT Orchestra FROM orchestra WHERE Orchestra_ID NOT IN (SELECT Orchestra_ID FROM performance)	orchestra
SELECT Orchestra FROM orchestra WHERE Orchestra_ID NOT IN (SELECT Orchestra_ID FROM performance)	orchestra
SELECT Record_Company FROM orchestra WHERE Year_of_Founded < 2003 INTERSECT SELECT Record_Company FROM orchestra WHERE Year_of_Founded > 2003	orchestra
SELECT Record_Company FROM orchestra WHERE Year_of_Founded < 2003 INTERSECT SELECT Record_Company FROM orchestra WHERE Year_of_Founded > 2003	orchestra
SELECT COUNT(*) FROM orchestra WHERE Major_Record_Format = "CD" OR Major_Record_Format = "DVD"	orchestra
SELECT COUNT(*) FROM orchestra WHERE Major_Record_Format = "CD" OR Major_Record_Format = "DVD"	orchestra
SELECT Year_of_Founded FROM orchestra AS T1 JOIN performance AS T2 ON T1.Orchestra_ID = T2.Orchestra_ID GROUP BY T2.Orchestra_ID HAVING COUNT(*) > 1	orchestra
SELECT Year_of_Founded FROM orchestra AS T1 JOIN performance AS T2 ON T1.Orchestra_ID = T2.Orchestra_ID GROUP BY T2.Orchestra_ID HAVING COUNT(*) > 1	orchestra
SELECT state FROM Owners INTERSECT SELECT state FROM Professionals	dog_kennels
SELECT state FROM Owners INTERSECT SELECT state FROM Professionals	dog_kennels
SELECT avg(age) FROM Dogs WHERE dog_id IN ( SELECT dog_id FROM Treatments )	dog_kennels
SELECT avg(age) FROM Dogs WHERE dog_id IN ( SELECT dog_id FROM Treatments )	dog_kennels
SELECT professional_id , last_name , cell_number FROM Professionals WHERE state = 'Indiana' UNION SELECT T1.professional_id , T1.last_name , T1.cell_number FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id GROUP BY T1.professional_id HAVING count(*) > 2	dog_kennels
SELECT professional_id , last_name , cell_number FROM Professionals WHERE state = 'Indiana' UNION SELECT T1.professional_id , T1.last_name , T1.cell_number FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id GROUP BY T1.professional_id HAVING count(*) > 2	dog_kennels
SELECT name FROM Dogs WHERE dog_id NOT IN( SELECT dog_id FROM Treatments GROUP BY dog_id HAVING sum(cost_of_treatment) > 1000 )	dog_kennels
SELECT name FROM Dogs WHERE dog_id NOT IN( SELECT dog_id FROM Treatments GROUP BY dog_id HAVING sum(cost_of_treatment) > 1000 )	dog_kennels
SELECT first_name FROM Professionals UNION SELECT first_name FROM Owners EXCEPT SELECT name FROM Dogs	dog_kennels
SELECT first_name FROM Professionals UNION SELECT first_name FROM Owners EXCEPT SELECT name FROM Dogs	dog_kennels
SELECT professional_id , role_code , email_address FROM Professionals EXCEPT SELECT T1.professional_id , T1.role_code , T1.email_address FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id	dog_kennels
SELECT professional_id , role_code , email_address FROM Professionals EXCEPT SELECT T1.professional_id , T1.role_code , T1.email_address FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id	dog_kennels
SELECT T1.owner_id , T2.first_name , T2.last_name FROM Dogs AS T1 JOIN Owners AS T2 ON T1.owner_id = T2.owner_id GROUP BY T1.owner_id ORDER BY count(*) DESC LIMIT 1	dog_kennels
SELECT T1.owner_id , T2.first_name , T2.last_name FROM Dogs AS T1 JOIN Owners AS T2 ON T1.owner_id = T2.owner_id GROUP BY T1.owner_id ORDER BY count(*) DESC LIMIT 1	dog_kennels
SELECT T1.professional_id , T1.role_code , T1.first_name FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id GROUP BY T1.professional_id HAVING count(*) >= 2	dog_kennels
SELECT T1.professional_id , T1.role_code , T1.first_name FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id GROUP BY T1.professional_id HAVING count(*) >= 2	dog_kennels
SELECT T1.breed_name FROM Breeds AS T1 JOIN Dogs AS T2 ON T1.breed_code = T2.breed_code GROUP BY T1.breed_name ORDER BY count(*) DESC LIMIT 1	dog_kennels
SELECT T1.breed_name FROM Breeds AS T1 JOIN Dogs AS T2 ON T1.breed_code = T2.breed_code GROUP BY T1.breed_name ORDER BY count(*) DESC LIMIT 1	dog_kennels
SELECT T1.owner_id , T1.last_name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id JOIN Treatments AS T3 ON T2.dog_id = T3.dog_id GROUP BY T1.owner_id ORDER BY count(*) DESC LIMIT 1	dog_kennels
SELECT T1.owner_id , T1.last_name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id JOIN Treatments AS T3 ON T2.dog_id = T3.dog_id GROUP BY T1.owner_id ORDER BY count(*) DESC LIMIT 1	dog_kennels
SELECT T1.treatment_type_description FROM Treatment_types AS T1 JOIN Treatments AS T2 ON T1.treatment_type_code = T2.treatment_type_code GROUP BY T1.treatment_type_code ORDER BY sum(cost_of_treatment) ASC LIMIT 1	dog_kennels
SELECT T1.treatment_type_description FROM Treatment_types AS T1 JOIN Treatments AS T2 ON T1.treatment_type_code = T2.treatment_type_code GROUP BY T1.treatment_type_code ORDER BY sum(cost_of_treatment) ASC LIMIT 1	dog_kennels
SELECT T1.owner_id , T1.zip_code FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id JOIN Treatments AS T3 ON T2.dog_id = T3.dog_id GROUP BY T1.owner_id ORDER BY sum(T3.cost_of_treatment) DESC LIMIT 1	dog_kennels
SELECT T1.owner_id , T1.zip_code FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id JOIN Treatments AS T3 ON T2.dog_id = T3.dog_id GROUP BY T1.owner_id ORDER BY sum(T3.cost_of_treatment) DESC LIMIT 1	dog_kennels
SELECT T1.professional_id , T1.cell_number FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id GROUP BY T1.professional_id HAVING count(*) >= 2	dog_kennels
SELECT T1.professional_id , T1.cell_number FROM Professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id GROUP BY T1.professional_id HAVING count(*) >= 2	dog_kennels
SELECT DISTINCT T1.first_name , T1.last_name FROM Professionals AS T1 JOIN Treatments AS T2 WHERE cost_of_treatment < ( SELECT avg(cost_of_treatment) FROM Treatments )	dog_kennels
SELECT DISTINCT T1.first_name , T1.last_name FROM Professionals AS T1 JOIN Treatments AS T2 WHERE cost_of_treatment < ( SELECT avg(cost_of_treatment) FROM Treatments )	dog_kennels
SELECT T1.date_of_treatment , T2.first_name FROM Treatments AS T1 JOIN Professionals AS T2 ON T1.professional_id = T2.professional_id	dog_kennels
SELECT T1.date_of_treatment , T2.first_name FROM Treatments AS T1 JOIN Professionals AS T2 ON T1.professional_id = T2.professional_id	dog_kennels
SELECT T1.cost_of_treatment , T2.treatment_type_description FROM Treatments AS T1 JOIN treatment_types AS T2 ON T1.treatment_type_code = T2.treatment_type_code	dog_kennels
SELECT T1.cost_of_treatment , T2.treatment_type_description FROM Treatments AS T1 JOIN treatment_types AS T2 ON T1.treatment_type_code = T2.treatment_type_code	dog_kennels
SELECT T1.first_name , T1.last_name , T2.size_code FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id	dog_kennels
SELECT T1.first_name , T1.last_name , T2.size_code FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id	dog_kennels
SELECT T1.first_name , T2.name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id	dog_kennels
SELECT T1.first_name , T2.name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id	dog_kennels
SELECT T1.name , T2.date_of_treatment FROM Dogs AS T1 JOIN Treatments AS T2 ON T1.dog_id = T2.dog_id WHERE T1.breed_code = ( SELECT breed_code FROM Dogs GROUP BY breed_code ORDER BY count(*) ASC LIMIT 1 )	dog_kennels
SELECT T1.name , T2.date_of_treatment FROM Dogs AS T1 JOIN Treatments AS T2 ON T1.dog_id = T2.dog_id WHERE T1.breed_code = ( SELECT breed_code FROM Dogs GROUP BY breed_code ORDER BY count(*) ASC LIMIT 1 )	dog_kennels
SELECT T1.first_name , T2.name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id WHERE T1.state = 'Virginia'	dog_kennels
SELECT T1.first_name , T2.name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id WHERE T1.state = 'Virginia'	dog_kennels
SELECT DISTINCT T1.date_arrived , T1.date_departed FROM Dogs AS T1 JOIN Treatments AS T2 ON T1.dog_id = T2.dog_id	dog_kennels
SELECT DISTINCT T1.date_arrived , T1.date_departed FROM Dogs AS T1 JOIN Treatments AS T2 ON T1.dog_id = T2.dog_id	dog_kennels
SELECT T1.last_name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id WHERE T2.age = ( SELECT max(age) FROM Dogs )	dog_kennels
SELECT T1.last_name FROM Owners AS T1 JOIN Dogs AS T2 ON T1.owner_id = T2.owner_id WHERE T2.age = ( SELECT max(age) FROM Dogs )	dog_kennels
SELECT email_address FROM Professionals WHERE state = 'Hawaii' OR state = 'Wisconsin'	dog_kennels
SELECT email_address FROM Professionals WHERE state = 'Hawaii' OR state = 'Wisconsin'	dog_kennels
SELECT date_arrived , date_departed FROM Dogs	dog_kennels
SELECT date_arrived , date_departed FROM Dogs	dog_kennels
SELECT count(DISTINCT dog_id) FROM Treatments	dog_kennels
SELECT count(DISTINCT dog_id) FROM Treatments	dog_kennels
SELECT count(DISTINCT professional_id) FROM Treatments	dog_kennels
SELECT count(DISTINCT professional_id) FROM Treatments	dog_kennels
SELECT role_code , street , city , state FROM professionals WHERE city LIKE '%West%'	dog_kennels
SELECT role_code , street , city , state FROM professionals WHERE city LIKE '%West%'	dog_kennels
SELECT first_name , last_name , email_address FROM Owners WHERE state LIKE '%North%'	dog_kennels
SELECT first_name , last_name , email_address FROM Owners WHERE state LIKE '%North%'	dog_kennels
SELECT count(*) FROM Dogs WHERE age < ( SELECT avg(age) FROM Dogs )	dog_kennels
SELECT count(*) FROM Dogs WHERE age < ( SELECT avg(age) FROM Dogs )	dog_kennels
SELECT cost_of_treatment FROM Treatments ORDER BY date_of_treatment DESC LIMIT 1	dog_kennels
SELECT cost_of_treatment FROM Treatments ORDER BY date_of_treatment DESC LIMIT 1	dog_kennels
SELECT count(*) FROM Dogs WHERE dog_id NOT IN ( SELECT dog_id FROM Treatments )	dog_kennels
SELECT count(*) FROM Dogs WHERE dog_id NOT IN ( SELECT dog_id FROM Treatments )	dog_kennels
SELECT count(*) FROM Owners WHERE owner_id NOT IN ( SELECT owner_id FROM Dogs )	dog_kennels
SELECT count(*) FROM Owners WHERE owner_id NOT IN ( SELECT owner_id FROM Dogs )	dog_kennels
SELECT count(*) FROM Professionals WHERE professional_id NOT IN ( SELECT professional_id FROM Treatments )	dog_kennels
SELECT count(*) FROM Professionals WHERE professional_id NOT IN ( SELECT professional_id FROM Treatments )	dog_kennels
SELECT name , age , weight FROM Dogs WHERE abandoned_yn = 1	dog_kennels
SELECT name , age , weight FROM Dogs WHERE abandoned_yn = 1	dog_kennels
SELECT avg(age) FROM Dogs	dog_kennels
SELECT avg(age) FROM Dogs	dog_kennels
SELECT max(age) FROM Dogs	dog_kennels
SELECT max(age) FROM Dogs	dog_kennels
SELECT charge_type , charge_amount FROM Charges	dog_kennels
SELECT charge_type , charge_amount FROM Charges	dog_kennels
SELECT max(charge_amount) FROM Charges	dog_kennels
SELECT max(charge_amount) FROM Charges	dog_kennels
SELECT email_address , cell_number , home_phone FROM professionals	dog_kennels
SELECT email_address , cell_number , home_phone FROM professionals	dog_kennels
SELECT DISTINCT breed_code , size_code FROM dogs	dog_kennels
SELECT DISTINCT breed_code , size_code FROM dogs	dog_kennels
SELECT DISTINCT T1.first_name , T3.treatment_type_description FROM professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id JOIN Treatment_types AS T3 ON T2.treatment_type_code = T3.treatment_type_code	dog_kennels
SELECT DISTINCT T1.first_name , T3.treatment_type_description FROM professionals AS T1 JOIN Treatments AS T2 ON T1.professional_id = T2.professional_id JOIN Treatment_types AS T3 ON T2.treatment_type_code = T3.treatment_type_code	dog_kennels
SELECT count(*) FROM Other_Available_Features	real_estate_properties
SELECT T2.feature_type_name FROM Other_Available_Features AS T1 JOIN Ref_Feature_Types AS T2 ON T1.feature_type_code = T2.feature_type_code WHERE T1.feature_name = "AirCon"	real_estate_properties
SELECT T2.property_type_description FROM Properties AS T1 JOIN Ref_Property_Types AS T2 ON T1.property_type_code = T2.property_type_code GROUP BY T1.property_type_code	real_estate_properties
SELECT property_name FROM Properties WHERE property_type_code = "House" UNION SELECT property_name FROM Properties WHERE property_type_code = "Apartment" AND room_count > 1	real_estate_properties
