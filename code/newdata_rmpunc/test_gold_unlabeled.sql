SELECT count(*) FROM member WHERE Membership_card = 'Black'
SELECT count(*) , address FROM member GROUP BY address
SELECT name FROM member WHERE address = 'Harford' OR address = 'Waterbury'
SELECT name , member_id FROM member WHERE Membership_card = 'Black' OR age < 30
SELECT Time_of_purchase , age , address FROM member ORDER BY Time_of_purchase
SELECT Membership_card FROM member GROUP BY Membership_card HAVING count(*) > 5
SELECT address FROM member WHERE age < 30 INTERSECT SELECT address FROM member WHERE age > 40
SELECT membership_card FROM member WHERE address = 'Hartford' INTERSECT SELECT membership_card FROM member WHERE address = 'Waterbury'
SELECT count(*) FROM member WHERE address != 'Hartford'
SELECT address FROM member EXCEPT SELECT address FROM member WHERE Membership_card = 'Black'
SELECT address FROM shop ORDER BY open_year
SELECT avg(num_of_staff) , avg(score) FROM shop
SELECT shop_id , address FROM shop WHERE score < (SELECT avg(score) FROM shop)
SELECT address , num_of_staff FROM shop WHERE shop_id NOT IN (SELECT shop_id FROM happy_hour)
SELECT t1.address , t1.shop_id FROM shop AS t1 JOIN happy_hour AS t2 ON t1.shop_id = t2.shop_id WHERE MONTH = 'May'
SELECT shop_id , count(*) FROM happy_hour GROUP BY shop_id ORDER BY count(*) DESC LIMIT 1
SELECT MONTH FROM happy_hour GROUP BY MONTH ORDER BY count(*) DESC LIMIT 1
SELECT MONTH FROM happy_hour GROUP BY MONTH HAVING count(*) > 2
SELECT count(*) FROM ALBUM
SELECT count(*) FROM ALBUM
SELECT Name FROM GENRE
SELECT Name FROM GENRE
SELECT * FROM CUSTOMER WHERE State = "NY"
SELECT * FROM CUSTOMER WHERE State = "NY"
SELECT FirstName , LastName FROM EMPLOYEE WHERE City = "Calgary"
SELECT FirstName , LastName FROM EMPLOYEE WHERE City = "Calgary"
SELECT (BillingCountry) FROM INVOICE
SELECT (BillingCountry) FROM INVOICE
SELECT Name FROM ARTIST WHERE Name LIKE "%a%"
SELECT Name FROM ARTIST WHERE Name LIKE "%a%"
SELECT Title FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId WHERE T2.Name = "AC/DC"
SELECT Title FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId WHERE T2.Name = "AC/DC"
SELECT COUNT(*) FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId WHERE T2.Name = "Metallica"
SELECT COUNT(*) FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId WHERE T2.Name = "Metallica"
SELECT T2.Name FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId WHERE T1.Title = "Balls to the Wall"
SELECT T2.Name FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId WHERE T1.Title = "Balls to the Wall"
SELECT T2.Name FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId GROUP BY T2.Name ORDER BY COUNT(*) DESC LIMIT 1
SELECT T2.Name FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId GROUP BY T2.Name ORDER BY COUNT(*) DESC LIMIT 1
SELECT Name FROM TRACK WHERE Name LIKE '%you%'
SELECT Name FROM TRACK WHERE Name LIKE '%you%'
SELECT AVG(UnitPrice) FROM TRACK
SELECT AVG(UnitPrice) FROM TRACK
SELECT max(Milliseconds) , min(Milliseconds) FROM TRACK
SELECT max(Milliseconds) , min(Milliseconds) FROM TRACK
SELECT T1.Title , T2.AlbumID , COUNT(*) FROM ALBUM AS T1 JOIN TRACK AS T2 ON T1.AlbumId = T2.AlbumId GROUP BY T2.AlbumID
SELECT T1.Title , T2.AlbumID , COUNT(*) FROM ALBUM AS T1 JOIN TRACK AS T2 ON T1.AlbumId = T2.AlbumId GROUP BY T2.AlbumID
SELECT T1.Name FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId GROUP BY T2.GenreId ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Name FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId GROUP BY T2.GenreId ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Name FROM MEDIATYPE AS T1 JOIN TRACK AS T2 ON T1.MediaTypeId = T2.MediaTypeId GROUP BY T2.MediaTypeId ORDER BY COUNT(*) ASC LIMIT 1
SELECT T1.Name FROM MEDIATYPE AS T1 JOIN TRACK AS T2 ON T1.MediaTypeId = T2.MediaTypeId GROUP BY T2.MediaTypeId ORDER BY COUNT(*) ASC LIMIT 1
SELECT T1.Title , T2.AlbumID FROM ALBUM AS T1 JOIN TRACK AS T2 ON T1.AlbumId = T2.AlbumId WHERE T2.UnitPrice > 1 GROUP BY T2.AlbumID
SELECT T1.Title , T2.AlbumID FROM ALBUM AS T1 JOIN TRACK AS T2 ON T1.AlbumId = T2.AlbumId WHERE T2.UnitPrice > 1 GROUP BY T2.AlbumID
SELECT COUNT(*) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Rock"
SELECT COUNT(*) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Rock"
SELECT AVG(UnitPrice) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Jazz"
SELECT AVG(UnitPrice) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Jazz"
SELECT FirstName , LastName FROM CUSTOMER WHERE Email = "luisg@embraer.com.br"
SELECT FirstName , LastName FROM CUSTOMER WHERE Email = "luisg@embraer.com.br"
SELECT COUNT(*) FROM CUSTOMER WHERE Email LIKE "%gmail.com%"
SELECT COUNT(*) FROM CUSTOMER WHERE Email LIKE "%gmail.com%"
SELECT T2.FirstName , T2.LastName FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId WHERE T1.FirstName = "Leonie"
SELECT T2.FirstName , T2.LastName FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId WHERE T1.FirstName = "Leonie"
SELECT T2.City FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId WHERE T1.PostalCode = "70174"
SELECT T2.City FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId WHERE T1.PostalCode = "70174"
SELECT COUNT( city) FROM EMPLOYEE
SELECT COUNT( city) FROM EMPLOYEE
SELECT T2.InvoiceDate FROM CUSTOMER AS T1 JOIN INVOICE AS T2 ON T1.CustomerId = T2.CustomerId WHERE T1.FirstName = "Astrid" AND LastName = "Gruber"
SELECT T2.InvoiceDate FROM CUSTOMER AS T1 JOIN INVOICE AS T2 ON T1.CustomerId = T2.CustomerId WHERE T1.FirstName = "Astrid" AND LastName = "Gruber"
SELECT LastName FROM CUSTOMER EXCEPT SELECT T1.LastName FROM CUSTOMER AS T1 JOIN Invoice AS T2 ON T1.CustomerId = T2.CustomerId WHERE T2.total > 20
SELECT LastName FROM CUSTOMER EXCEPT SELECT T1.LastName FROM CUSTOMER AS T1 JOIN Invoice AS T2 ON T1.CustomerId = T2.CustomerId WHERE T2.total > 20
SELECT T1.FirstName FROM CUSTOMER AS T1 JOIN INVOICE AS T2 ON T1.CustomerId = T2.CustomerId WHERE T1.country = "Brazil"
SELECT T1.FirstName FROM CUSTOMER AS T1 JOIN INVOICE AS T2 ON T1.CustomerId = T2.CustomerId WHERE T1.country = "Brazil"
SELECT T1.Address FROM CUSTOMER AS T1 JOIN INVOICE AS T2 ON T1.CustomerId = T2.CustomerId WHERE T1.country = "Germany"
SELECT T1.Address FROM CUSTOMER AS T1 JOIN INVOICE AS T2 ON T1.CustomerId = T2.CustomerId WHERE T1.country = "Germany"
SELECT Phone FROM EMPLOYEE
SELECT Phone FROM EMPLOYEE
SELECT COUNT(*) FROM MEDIATYPE AS T1 JOIN TRACK AS T2 ON T1.MediaTypeId = T2.MediaTypeId WHERE T1.Name = "AAC audio file"
SELECT COUNT(*) FROM MEDIATYPE AS T1 JOIN TRACK AS T2 ON T1.MediaTypeId = T2.MediaTypeId WHERE T1.Name = "AAC audio file"
SELECT AVG(Milliseconds) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Latin" OR T1.Name = "Pop"
SELECT AVG(Milliseconds) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Latin" OR T1.Name = "Pop"
SELECT T1.FirstName , T1.SupportRepId FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId GROUP BY T1.SupportRepId HAVING COUNT(*) >= 10
SELECT T1.FirstName , T1.SupportRepId FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId GROUP BY T1.SupportRepId HAVING COUNT(*) >= 10
SELECT T1.LastName FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId GROUP BY T1.SupportRepId HAVING COUNT(*) <= 20
SELECT T1.LastName FROM CUSTOMER AS T1 JOIN EMPLOYEE AS T2 ON T1.SupportRepId = T2.EmployeeId GROUP BY T1.SupportRepId HAVING COUNT(*) <= 20
SELECT Title FROM ALBUM ORDER BY Title
SELECT Title FROM ALBUM ORDER BY Title
SELECT T2.Name , T1.ArtistId FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistID GROUP BY T1.ArtistId HAVING COUNT(*) >= 3 ORDER BY T2.Name
SELECT T2.Name , T1.ArtistId FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistID GROUP BY T1.ArtistId HAVING COUNT(*) >= 3 ORDER BY T2.Name
SELECT Name FROM ARTIST EXCEPT SELECT T2.Name FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId
SELECT Name FROM ARTIST EXCEPT SELECT T2.Name FROM ALBUM AS T1 JOIN ARTIST AS T2 ON T1.ArtistId = T2.ArtistId
SELECT AVG(T2.UnitPrice) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Rock"
SELECT AVG(T2.UnitPrice) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Rock"
SELECT max(Milliseconds) , min(Milliseconds) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Pop"
SELECT max(Milliseconds) , min(Milliseconds) FROM GENRE AS T1 JOIN TRACK AS T2 ON T1.GenreId = T2.GenreId WHERE T1.Name = "Pop"
SELECT BirthDate FROM EMPLOYEE WHERE City = "Edmonton"
SELECT BirthDate FROM EMPLOYEE WHERE City = "Edmonton"
SELECT (UnitPrice) FROM TRACK
SELECT (UnitPrice) FROM TRACK
SELECT count(*) FROM ARTIST WHERE artistid NOT IN(SELECT artistid FROM ALBUM)
SELECT count(*) FROM ARTIST WHERE artistid NOT IN(SELECT artistid FROM ALBUM)
SELECT T1.Title FROM Album AS T1 JOIN Track AS T2 ON T1.AlbumId = T2.AlbumId JOIN Genre AS T3 ON T2.GenreID = T3.GenreID WHERE T3.Name = 'Reggae' INTERSECT SELECT T1.Title FROM Album AS T1 JOIN Track AS T2 ON T1.AlbumId = T2.AlbumId JOIN Genre AS T3 ON T2.GenreID = T3.GenreID WHERE T3.Name = 'Rock'
SELECT T1.Title FROM Album AS T1 JOIN Track AS T2 ON T1.AlbumId = T2.AlbumId JOIN Genre AS T3 ON T2.GenreID = T3.GenreID WHERE T3.Name = 'Reggae' INTERSECT SELECT T1.Title FROM Album AS T1 JOIN Track AS T2 ON T1.AlbumId = T2.AlbumId JOIN Genre AS T3 ON T2.GenreID = T3.GenreID WHERE T3.Name = 'Rock'
SELECT Model_name FROM chip_model WHERE Launch_year BETWEEN 2002 AND 2004;
SELECT Model_name , RAM_MiB FROM chip_model ORDER BY RAM_MiB ASC LIMIT 1;
SELECT chip_model , screen_mode FROM phone WHERE Hardware_Model_name = "LG-P760";
SELECT count(*) FROM phone WHERE Company_name = "Nokia Corporation";
SELECT max(T1.RAM_MiB) , min(T1.RAM_MiB) FROM chip_model AS T1 JOIN phone AS T2 ON T1.Model_name = T2.chip_model WHERE T2.Company_name = "Nokia Corporation";
SELECT avg(T1.ROM_MiB) FROM chip_model AS T1 JOIN phone AS T2 ON T1.Model_name = T2.chip_model WHERE T2.Company_name = "Nokia Corporation";
SELECT T2.Hardware_Model_name , T2.Company_name FROM chip_model AS T1 JOIN phone AS T2 ON T1.Model_name = T2.chip_model WHERE T1.Launch_year = 2002 OR T1.RAM_MiB > 32;
SELECT Hardware_Model_name , Company_name FROM phone WHERE Accreditation_type LIKE 'Full';
SELECT T1.Char_cells , T1.Pixels , T1.Hardware_colours FROM screen_mode AS T1 JOIN phone AS T2 ON T1.Graphics_mode = T2.screen_mode WHERE T2.Hardware_Model_name = "LG-P760";
SELECT T2.Hardware_Model_name , T2.Company_name FROM screen_mode AS T1 JOIN phone AS T2 ON T1.Graphics_mode = T2.screen_mode WHERE T1.Type = "Graphics";
SELECT Company_name , count(*) FROM phone GROUP BY Company_name ORDER BY count(*) ASC LIMIT 1;
SELECT Company_name FROM phone GROUP BY Company_name HAVING count(*) > 1;
SELECT max(used_kb) , min(used_kb) , avg(used_kb) FROM screen_mode;
SELECT T2.Hardware_Model_name FROM chip_model AS T1 JOIN phone AS T2 ON T1.Model_name = T2.chip_model WHERE T1.Launch_year = 2002 ORDER BY T1.RAM_MiB DESC LIMIT 1;
SELECT T1.WiFi , T3.Type FROM chip_model AS T1 JOIN phone AS T2 ON T1.Model_name = T2.chip_model JOIN screen_mode AS T3 ON T2.screen_mode = T3.Graphics_mode WHERE T2.Hardware_Model_name = "LG-P760";
SELECT T2.Hardware_Model_name FROM chip_model AS T1 JOIN phone AS T2 ON T1.Model_name = T2.chip_model JOIN screen_mode AS T3 ON T2.screen_mode = T3.Graphics_mode WHERE T3.Type = "Text" OR T1.RAM_MiB > 32;
SELECT T2.Hardware_Model_name FROM screen_mode AS T1 JOIN phone AS T2 ON T1.Graphics_mode = T2.screen_mode WHERE T1.Type = "Graphics" OR t2.Company_name = "Nokia Corporation"
SELECT T2.Hardware_Model_name FROM screen_mode AS T1 JOIN phone AS T2 ON T1.Graphics_mode = T2.screen_mode WHERE t2.Company_name = "Nokia Corporation" AND T1.Type != "Text";
SELECT T2.Hardware_Model_name , T2.Company_name FROM screen_mode AS T1 JOIN phone AS T2 ON T1.Graphics_mode = T2.screen_mode WHERE T1.used_kb BETWEEN 10 AND 15;
SELECT Accreditation_type , count(*) FROM phone GROUP BY Accreditation_type
SELECT Accreditation_type , count(*) FROM phone GROUP BY Accreditation_type
SELECT Accreditation_level FROM phone GROUP BY Accreditation_level HAVING count(*) > 3
SELECT * FROM chip_model
SELECT count(*) FROM chip_model WHERE wifi = 'No'
SELECT count(*) FROM chip_model WHERE wifi = 'No'
SELECT model_name FROM chip_model ORDER BY launch_year
SELECT avg(RAM_MiB) FROM chip_model WHERE model_name NOT IN (SELECT chip_model FROM phone)
SELECT model_name FROM chip_model EXCEPT SELECT chip_model FROM phone WHERE Accreditation_type = 'Full'
SELECT t1.pixels FROM screen_mode AS t1 JOIN phone AS t2 ON t1.Graphics_mode = t2.screen_mode WHERE t2.Accreditation_type = 'Provisional' INTERSECT SELECT t1.pixels FROM screen_mode AS t1 JOIN phone AS t2 ON t1.Graphics_mode = t2.screen_mode WHERE t2.Accreditation_type = 'Full'
SELECT count(*) FROM game WHERE season > 2007
SELECT Date FROM game ORDER BY home_team DESC
SELECT season , home_team , away_team FROM game
SELECT max(home_games) , min(home_games) , avg(home_games) FROM stadium
SELECT average_attendance FROM stadium WHERE capacity_percentage > 100
SELECT player , number_of_matches , SOURCE FROM injury_accident WHERE injury != 'Knee problem'
SELECT T1.season FROM game AS T1 JOIN injury_accident AS T2 ON T1.id = T2.game_id WHERE T2.player = 'Walter Samuel'
SELECT T1.id , T1.score , T1.date FROM game AS T1 JOIN injury_accident AS T2 ON T2.game_id = T1.id GROUP BY T1.id HAVING count(*) >= 2
SELECT T1.id , T1.name FROM stadium AS T1 JOIN game AS T2 ON T1.id = T2.stadium_id JOIN injury_accident AS T3 ON T2.id = T3.game_id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1
SELECT T1.id , T1.name FROM stadium AS T1 JOIN game AS T2 ON T1.id = T2.stadium_id JOIN injury_accident AS T3 ON T2.id = T3.game_id GROUP BY T1.id ORDER BY count(*) DESC LIMIT 1
SELECT T1.season , T2.name FROM game AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.id JOIN injury_accident AS T3 ON T1.id = T3.game_id WHERE T3.injury = 'Foot injury' OR T3.injury = 'Knee problem'
SELECT count( SOURCE) FROM injury_accident
SELECT count(*) FROM game WHERE id NOT IN ( SELECT game_id FROM injury_accident )
SELECT count( T1.injury) FROM injury_accident AS T1 JOIN game AS T2 ON T1.game_id = T2.id WHERE T2.season > 2010
SELECT T2.name FROM game AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.id JOIN injury_accident AS T3 ON T1.id = T3.game_id WHERE T3.player = 'Walter Samuel' INTERSECT SELECT T2.name FROM game AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.id JOIN injury_accident AS T3 ON T1.id = T3.game_id WHERE T3.player = 'Thiago Motta'
SELECT name , average_attendance , total_attendance FROM stadium EXCEPT SELECT T2.name , T2.average_attendance , T2.total_attendance FROM game AS T1 JOIN stadium AS T2 ON T1.stadium_id = T2.id JOIN injury_accident AS T3 ON T1.id = T3.game_id
SELECT name FROM stadium WHERE name LIKE "%Bank%"
SELECT T1.id , count(*) FROM stadium AS T1 JOIN game AS T2 ON T1.id = T2.stadium_id GROUP BY T1.id
SELECT T1.date , T2.player FROM game AS T1 JOIN injury_accident AS T2 ON T1.id = T2.game_id ORDER BY T1.season DESC
SELECT count(*) FROM player
SELECT Player_name FROM player ORDER BY Votes ASC
SELECT Gender , Occupation FROM player
SELECT Player_name , residence FROM player WHERE Occupation != "Researcher"
SELECT Sponsor_name FROM player WHERE Residence = "Brandon" OR Residence = "Birtle"
SELECT Player_name FROM player ORDER BY Votes DESC LIMIT 1
SELECT Occupation , COUNT(*) FROM player GROUP BY Occupation
SELECT Occupation FROM player GROUP BY Occupation ORDER BY COUNT(*) DESC LIMIT 1
SELECT Residence FROM player GROUP BY Residence HAVING COUNT(*) >= 2
SELECT T3.Player_name , T2.coach_name FROM player_coach AS T1 JOIN coach AS T2 ON T1.Coach_ID = T2.Coach_ID JOIN player AS T3 ON T1.Player_ID = T3.Player_ID
SELECT T3.Player_name FROM player_coach AS T1 JOIN coach AS T2 ON T1.Coach_ID = T2.Coach_ID JOIN player AS T3 ON T1.Player_ID = T3.Player_ID WHERE T2.Rank = 1
SELECT T3.Player_name , T3.gender FROM player_coach AS T1 JOIN coach AS T2 ON T1.Coach_ID = T2.Coach_ID JOIN player AS T3 ON T1.Player_ID = T3.Player_ID WHERE T1.Starting_year > 2011
SELECT T3.Player_name , T2.coach_name FROM player_coach AS T1 JOIN coach AS T2 ON T1.Coach_ID = T2.Coach_ID JOIN player AS T3 ON T1.Player_ID = T3.Player_ID ORDER BY T3.Votes DESC
SELECT Player_name FROM player WHERE Player_ID NOT IN (SELECT Player_ID FROM player_coach)
SELECT Residence FROM player WHERE gender = "M" INTERSECT SELECT Residence FROM player WHERE gender = "F"
SELECT T1.club_id , T1.club_name, count(*) FROM club AS T1 JOIN coach AS T2 ON T1.club_id = T2.club_id GROUP BY T1.club_id
SELECT T1.club_id , T1.gold FROM match_result AS T1 JOIN coach AS T2 ON T1.club_id = T2.club_id GROUP BY T1.club_id ORDER BY count(*) DESC LIMIT 1
SELECT count(*) FROM wrestler
SELECT count(*) FROM wrestler
SELECT Name FROM wrestler ORDER BY Days_held DESC
SELECT Name FROM wrestler ORDER BY Days_held DESC
SELECT Name FROM wrestler ORDER BY Days_held ASC LIMIT 1
SELECT Name FROM wrestler ORDER BY Days_held ASC LIMIT 1
SELECT Reign FROM wrestler WHERE LOCATION != "Tokyo , Japan"
SELECT Reign FROM wrestler WHERE LOCATION != "Tokyo , Japan"
SELECT Name , LOCATION FROM wrestler
SELECT Name , LOCATION FROM wrestler
SELECT Elimination_Move FROM Elimination WHERE Team = "Team Orton"
SELECT Elimination_Move FROM Elimination WHERE Team = "Team Orton"
SELECT T2.Name , T1.Elimination_Move FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID
SELECT T2.Name , T1.Elimination_Move FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID
SELECT T2.Name , T1.Team FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID ORDER BY T2.Days_held DESC
SELECT T2.Name , T1.Team FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID ORDER BY T2.Days_held DESC
SELECT T1.Time FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID ORDER BY T2.Days_held DESC LIMIT 1
SELECT T1.Time FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID ORDER BY T2.Days_held DESC LIMIT 1
SELECT T1.Time FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID WHERE T2.Days_held > 50
SELECT T1.Time FROM elimination AS T1 JOIN wrestler AS T2 ON T1.Wrestler_ID = T2.Wrestler_ID WHERE T2.Days_held > 50
SELECT Team , COUNT(*) FROM elimination GROUP BY Team
SELECT Team , COUNT(*) FROM elimination GROUP BY Team
SELECT Team FROM elimination GROUP BY Team HAVING COUNT(*) > 3
SELECT Team FROM elimination GROUP BY Team HAVING COUNT(*) > 3
SELECT Reign , Days_held FROM wrestler
SELECT Reign , Days_held FROM wrestler
SELECT Name FROM wrestler WHERE Days_held < 100
SELECT Name FROM wrestler WHERE Days_held < 100
SELECT Reign FROM wrestler GROUP BY Reign ORDER BY COUNT(*) DESC LIMIT 1
SELECT Reign FROM wrestler GROUP BY Reign ORDER BY COUNT(*) DESC LIMIT 1
SELECT LOCATION FROM wrestler GROUP BY LOCATION HAVING COUNT(*) > 2
SELECT LOCATION FROM wrestler GROUP BY LOCATION HAVING COUNT(*) > 2
SELECT Name FROM wrestler WHERE Wrestler_ID NOT IN (SELECT Wrestler_ID FROM elimination)
SELECT Name FROM wrestler WHERE Wrestler_ID NOT IN (SELECT Wrestler_ID FROM elimination)
SELECT Team FROM Elimination WHERE Eliminated_By = "Orton" INTERSECT SELECT Team FROM Elimination WHERE Eliminated_By = "Benjamin"
SELECT Team FROM Elimination WHERE Eliminated_By = "Orton" INTERSECT SELECT Team FROM Elimination WHERE Eliminated_By = "Benjamin"
SELECT COUNT ( team) FROM elimination
SELECT COUNT ( team) FROM elimination
SELECT TIME FROM elimination WHERE Eliminated_By = "Punk" OR Eliminated_By = "Orton"
SELECT TIME FROM elimination WHERE Eliminated_By = "Punk" OR Eliminated_By = "Orton"
SELECT count(*) FROM company
SELECT count(*) FROM company
SELECT company , rank FROM company ORDER BY Sales_billion DESC
SELECT company , rank FROM company ORDER BY Sales_billion DESC
SELECT company , main_industry FROM company WHERE headquarters != 'USA'
SELECT company , main_industry FROM company WHERE headquarters != 'USA'
SELECT company , headquarters FROM company ORDER BY market_value DESC
SELECT company , headquarters FROM company ORDER BY market_value DESC
SELECT min(market_value) , max(market_value) , avg(market_value) FROM company
SELECT min(market_value) , max(market_value) , avg(market_value) FROM company
SELECT main_industry FROM company
SELECT main_industry FROM company
SELECT headquarters , count(*) FROM company GROUP BY headquarters
SELECT headquarters , count(*) FROM company GROUP BY headquarters
SELECT main_industry , sum(market_value) FROM company GROUP BY main_industry
SELECT main_industry , sum(market_value) FROM company GROUP BY main_industry
SELECT main_industry , count(*) FROM company GROUP BY main_industry ORDER BY sum(market_value) DESC LIMIT 1
SELECT main_industry , count(*) FROM company GROUP BY main_industry ORDER BY sum(market_value) DESC LIMIT 1
SELECT headquarters FROM company WHERE main_industry = 'Banking' GROUP BY headquarters HAVING count(*) >= 2
SELECT headquarters FROM company WHERE main_industry = 'Banking' GROUP BY headquarters HAVING count(*) >= 2
SELECT station_id , LOCATION , manager_name FROM gas_station ORDER BY open_year
SELECT station_id , LOCATION , manager_name FROM gas_station ORDER BY open_year
SELECT count(*) FROM gas_station WHERE open_year BETWEEN 2000 AND 2005
SELECT count(*) FROM gas_station WHERE open_year BETWEEN 2000 AND 2005
SELECT LOCATION , count(*) FROM gas_station GROUP BY LOCATION ORDER BY count(*)
SELECT LOCATION , count(*) FROM gas_station GROUP BY LOCATION ORDER BY count(*)
SELECT headquarters FROM company WHERE main_industry = 'Banking' INTERSECT SELECT headquarters FROM company WHERE main_industry = 'Oil and gas'
SELECT headquarters FROM company WHERE main_industry = 'Banking' INTERSECT SELECT headquarters FROM company WHERE main_industry = 'Oil and gas'
SELECT headquarters FROM company EXCEPT SELECT headquarters FROM company WHERE main_industry = 'Banking'
SELECT headquarters FROM company EXCEPT SELECT headquarters FROM company WHERE main_industry = 'Banking'
SELECT T2.company , count(*) FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id GROUP BY T1.company_id
SELECT T2.company , count(*) FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id GROUP BY T1.company_id
SELECT company , main_industry FROM company WHERE company_id NOT IN (SELECT company_id FROM station_company)
SELECT company , main_industry FROM company WHERE company_id NOT IN (SELECT company_id FROM station_company)
SELECT T3.manager_name FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id JOIN gas_station AS T3 ON T1.station_id = T3.station_id WHERE T2.company = 'ExxonMobil'
SELECT T3.manager_name FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id JOIN gas_station AS T3 ON T1.station_id = T3.station_id WHERE T2.company = 'ExxonMobil'
SELECT T3.location FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id JOIN gas_station AS T3 ON T1.station_id = T3.station_id WHERE T2.market_value > 100
SELECT T3.location FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id JOIN gas_station AS T3 ON T1.station_id = T3.station_id WHERE T2.market_value > 100
SELECT manager_name FROM gas_station WHERE open_year > 2000 GROUP BY manager_name ORDER BY count(*) DESC LIMIT 1
SELECT manager_name FROM gas_station WHERE open_year > 2000 GROUP BY manager_name ORDER BY count(*) DESC LIMIT 1
SELECT LOCATION FROM gas_station ORDER BY open_year
SELECT LOCATION FROM gas_station ORDER BY open_year
SELECT rank , company , market_value FROM company WHERE main_industry = 'Banking' ORDER BY sales_billion , profits_billion
SELECT rank , company , market_value FROM company WHERE main_industry = 'Banking' ORDER BY sales_billion , profits_billion
SELECT T3.location , T3.Representative_Name FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id JOIN gas_station AS T3 ON T1.station_id = T3.station_id ORDER BY T2.Assets_billion DESC LIMIT 3
SELECT T3.location , T3.Representative_Name FROM station_company AS T1 JOIN company AS T2 ON T1.company_id = T2.company_id JOIN gas_station AS T3 ON T1.station_id = T3.station_id ORDER BY T2.Assets_billion DESC LIMIT 3
SELECT dname FROM department ORDER BY mgr_start_date
SELECT Dependent_name FROM dependent WHERE relationship = 'Spouse'
SELECT count(*) FROM dependent WHERE sex = 'F'
SELECT t1.dname FROM department AS t1 JOIN dept_locations AS t2 ON t1.dnumber = t2.dnumber WHERE t2.dlocation = 'Houston'
SELECT fname , lname FROM employee WHERE salary > 30000
SELECT count(*) , sex FROM employee WHERE salary < 50000 GROUP BY sex
SELECT fname , lname , address FROM employee ORDER BY Bdate
SELECT T1.event_details FROM EVENTS AS T1 JOIN Services AS T2 ON T1.Service_ID = T2.Service_ID WHERE T2.Service_Type_Code = 'Marriage'
SELECT T1.event_id , T1.event_details FROM EVENTS AS T1 JOIN Participants_in_Events AS T2 ON T1.Event_ID = T2.Event_ID GROUP BY T1.Event_ID HAVING count(*) > 1
SELECT T1.Participant_ID , T1.Participant_Type_Code , count(*) FROM Participants AS T1 JOIN Participants_in_Events AS T2 ON T1.Participant_ID = T2.Participant_ID GROUP BY T1.Participant_ID
SELECT Participant_ID , Participant_Type_Code , Participant_Details FROM Participants
SELECT count(*) FROM participants WHERE participant_type_code = 'Organizer'
SELECT service_type_code FROM services ORDER BY service_type_code
SELECT service_id , event_details FROM EVENTS
SELECT count(*) FROM participants AS T1 JOIN Participants_in_Events AS T2 ON T1.Participant_ID = T2.Participant_ID WHERE T1.participant_details LIKE '%Dr.%'
SELECT participant_type_code FROM participants GROUP BY participant_type_code ORDER BY count(*) DESC LIMIT 1
SELECT T3.service_id , T4.Service_Type_Code FROM participants AS T1 JOIN Participants_in_Events AS T2 ON T1.Participant_ID = T2.Participant_ID JOIN EVENTS AS T3 ON T2.Event_ID = T3.Event_ID JOIN services AS T4 ON T3.service_id = T4.service_id GROUP BY T3.service_id ORDER BY count(*) ASC LIMIT 1
SELECT Event_ID FROM Participants_in_Events GROUP BY Event_ID ORDER BY count(*) DESC LIMIT 1
SELECT event_id FROM EVENTS EXCEPT SELECT T1.event_id FROM Participants_in_Events AS T1 JOIN Participants AS T2 ON T1.Participant_ID = T2.Participant_ID WHERE Participant_Details = 'Kenyatta Kuhn'
SELECT T1.service_type_code FROM services AS T1 JOIN EVENTS AS T2 ON T1.service_id = T2.service_id WHERE T2.event_details = 'Success' INTERSECT SELECT T1.service_type_code FROM services AS T1 JOIN EVENTS AS T2 ON T1.service_id = T2.service_id WHERE T2.event_details = 'Fail'
SELECT count(*) FROM EVENTS WHERE event_id NOT IN (SELECT event_id FROM Participants_in_Events)
SELECT count( participant_id) FROM participants_in_Events
SELECT count(*) FROM perpetrator
SELECT Date FROM perpetrator ORDER BY Killed DESC
SELECT Injured FROM perpetrator ORDER BY Injured ASC
SELECT avg(Injured) FROM perpetrator
SELECT LOCATION FROM perpetrator ORDER BY Killed DESC LIMIT 1
SELECT Name FROM People ORDER BY Height ASC
SELECT T1.Name FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID
SELECT T1.Name FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Country != "China"
SELECT T1.Name FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Weight DESC LIMIT 1
SELECT sum(T2.Killed) FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Height > 1.84
SELECT T1.Name FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Country = "China" OR T2.Country = "Japan"
SELECT T1.Height FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Injured DESC
SELECT Country , COUNT(*) FROM perpetrator GROUP BY Country
SELECT Country , COUNT(*) FROM perpetrator GROUP BY Country ORDER BY COUNT(*) DESC LIMIT 1
SELECT Country , COUNT(*) FROM perpetrator GROUP BY Country HAVING COUNT(*) >= 2
SELECT T1.Name FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Year DESC
SELECT Name FROM people WHERE People_ID NOT IN (SELECT People_ID FROM perpetrator)
SELECT Country FROM perpetrator WHERE Injured > 50 INTERSECT SELECT Country FROM perpetrator WHERE Injured < 20
SELECT count( LOCATION) FROM perpetrator
SELECT T2.Date FROM people AS T1 JOIN perpetrator AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Height DESC LIMIT 1
SELECT max(YEAR) FROM perpetrator;
SELECT roomName FROM Rooms WHERE basePrice < 160 AND beds = 2 AND decor = 'modern';
SELECT roomName FROM Rooms WHERE basePrice < 160 AND beds = 2 AND decor = 'modern';
SELECT roomName , RoomId FROM Rooms WHERE basePrice > 160 AND maxOccupancy > 2;
SELECT roomName , RoomId FROM Rooms WHERE basePrice > 160 AND maxOccupancy > 2;
SELECT T2.roomName FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room ORDER BY count(*) DESC LIMIT 1;
SELECT T2.roomName FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room ORDER BY count(*) DESC LIMIT 1;
SELECT kids FROM Reservations WHERE FirstName = "ROY" AND LastName = "SWEAZY";
SELECT kids FROM Reservations WHERE FirstName = "ROY" AND LastName = "SWEAZY";
SELECT count(*) FROM Reservations WHERE FirstName = "ROY" AND LastName = "SWEAZY";
SELECT count(*) FROM Reservations WHERE FirstName = "ROY" AND LastName = "SWEAZY";
SELECT T2.roomName , T1.Rate , T1.CheckIn , T1.CheckOut FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room ORDER BY T1.Rate DESC LIMIT 1;
SELECT T2.roomName , T1.Rate , T1.CheckIn , T1.CheckOut FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room ORDER BY T1.Rate DESC LIMIT 1;
SELECT Adults FROM Reservations WHERE CheckIn = "2010-10-23" AND FirstName = "CONRAD" AND LastName = "SELBIG";
SELECT Adults FROM Reservations WHERE CheckIn = "2010-10-23" AND FirstName = "CONRAD" AND LastName = "SELBIG";
SELECT Kids FROM Reservations WHERE CheckIn = "2010-09-21" AND FirstName = "DAMIEN" AND LastName = "TRACHSEL";
SELECT Kids FROM Reservations WHERE CheckIn = "2010-09-21" AND FirstName = "DAMIEN" AND LastName = "TRACHSEL";
SELECT sum(beds) FROM Rooms WHERE bedtype = 'King';
SELECT sum(beds) FROM Rooms WHERE bedtype = 'King';
SELECT roomName , decor FROM Rooms WHERE bedtype = 'King' ORDER BY basePrice;
SELECT roomName , decor FROM Rooms WHERE bedtype = 'King' ORDER BY basePrice;
SELECT roomName , basePrice FROM Rooms ORDER BY basePrice ASC LIMIT 1;
SELECT roomName , basePrice FROM Rooms ORDER BY basePrice ASC LIMIT 1;
SELECT decor FROM Rooms WHERE roomName = "Recluse and defiance";
SELECT decor FROM Rooms WHERE roomName = "Recluse and defiance";
SELECT bedType , avg(basePrice) FROM Rooms GROUP BY bedType;
SELECT bedType , avg(basePrice) FROM Rooms GROUP BY bedType;
SELECT sum(maxOccupancy) FROM Rooms WHERE decor = 'modern';
SELECT sum(maxOccupancy) FROM Rooms WHERE decor = 'modern';
SELECT T2.decor FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T2.decor ORDER BY count(T2.decor) ASC LIMIT 1;
SELECT T2.decor FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T2.decor ORDER BY count(T2.decor) ASC LIMIT 1;
SELECT count(*) FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId WHERE T2.maxOccupancy = T1.Adults + T1.Kids;
SELECT count(*) FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId WHERE T2.maxOccupancy = T1.Adults + T1.Kids;
SELECT T1.firstname , T1.lastname FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId WHERE T1.Rate - T2.basePrice > 0
SELECT T1.firstname , T1.lastname FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId WHERE T1.Rate - T2.basePrice > 0
SELECT count(*) FROM Rooms;
SELECT count(*) FROM Rooms;
SELECT count(*) FROM Rooms WHERE bedType = "King";
SELECT count(*) FROM Rooms WHERE bedType = "King";
SELECT bedType , count(*) FROM Rooms GROUP BY bedType;
SELECT bedType , count(*) FROM Rooms GROUP BY bedType;
SELECT roomName FROM Rooms ORDER BY maxOccupancy DESC LIMIT 1;
SELECT roomName FROM Rooms ORDER BY maxOccupancy DESC LIMIT 1;
SELECT RoomId , roomName FROM Rooms ORDER BY basePrice DESC LIMIT 1;
SELECT RoomId , roomName FROM Rooms ORDER BY basePrice DESC LIMIT 1;
SELECT roomName , bedType FROM Rooms WHERE decor = "traditional";
SELECT roomName , bedType FROM Rooms WHERE decor = "traditional";
SELECT decor , count(*) FROM Rooms WHERE bedType = "King" GROUP BY decor;
SELECT decor , count(*) FROM Rooms WHERE bedType = "King" GROUP BY decor;
SELECT decor , avg(basePrice) , min(basePrice) FROM Rooms GROUP BY decor;
SELECT decor , avg(basePrice) , min(basePrice) FROM Rooms GROUP BY decor;
SELECT roomName FROM Rooms ORDER BY basePrice;
SELECT roomName FROM Rooms ORDER BY basePrice;
SELECT decor , count(*) FROM Rooms WHERE basePrice > 120 GROUP BY decor;
SELECT decor , count(*) FROM Rooms WHERE basePrice > 120 GROUP BY decor;
SELECT bedType , avg(basePrice) FROM Rooms GROUP BY bedType;
SELECT bedType , avg(basePrice) FROM Rooms GROUP BY bedType;
SELECT roomName FROM Rooms WHERE bedType = "King" OR bedType = "Queen";
SELECT roomName FROM Rooms WHERE bedType = "King" OR bedType = "Queen";
SELECT count( bedType) FROM Rooms;
SELECT count( bedType) FROM Rooms;
SELECT RoomId , roomName FROM Rooms ORDER BY basePrice DESC LIMIT 3;
SELECT RoomId , roomName FROM Rooms ORDER BY basePrice DESC LIMIT 3;
SELECT roomName FROM Rooms WHERE basePrice > ( SELECT avg(basePrice) FROM Rooms );
SELECT roomName FROM Rooms WHERE basePrice > ( SELECT avg(basePrice) FROM Rooms );
SELECT count(*) FROM rooms WHERE roomid NOT IN (SELECT room FROM reservations)
SELECT count(*) FROM rooms WHERE roomid NOT IN (SELECT room FROM reservations)
SELECT T2.roomName , count(*) , T1.Room FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room
SELECT T2.roomName , count(*) , T1.Room FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room
SELECT T2.roomName FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room HAVING count(*) > 60
SELECT T2.roomName FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId GROUP BY T1.Room HAVING count(*) > 60
SELECT roomname FROM rooms WHERE baseprice BETWEEN 120 AND 150
SELECT roomname FROM rooms WHERE baseprice BETWEEN 120 AND 150
SELECT T2.roomName FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId WHERE firstname LIKE '%ROY%'
SELECT T2.roomName FROM Reservations AS T1 JOIN Rooms AS T2 ON T1.Room = T2.RoomId WHERE firstname LIKE '%ROY%'
SELECT T1.cmi_details FROM Customer_Master_Index AS T1 JOIN CMI_Cross_References AS T2 ON T1.master_customer_id = T2.master_customer_id WHERE T2.source_system_code = 'Tax'
SELECT T1.cmi_cross_ref_id , T1.source_system_code FROM CMI_Cross_References AS T1 JOIN Council_Tax AS T2 ON T1.cmi_cross_ref_id = T2.cmi_cross_ref_id GROUP BY T1.cmi_cross_ref_id HAVING count(*) >= 1
SELECT T2.cmi_cross_ref_id , T2.master_customer_id , count(*) FROM Business_Rates AS T1 JOIN CMI_Cross_References AS T2 ON T1.cmi_cross_ref_id = T2.cmi_cross_ref_id GROUP BY T2.cmi_cross_ref_id
SELECT T1.source_system_code , T2.council_tax_id FROM CMI_Cross_References AS T1 JOIN Benefits_Overpayments AS T2 ON T1.cmi_cross_ref_id = T2.cmi_cross_ref_id ORDER BY T2.council_tax_id
SELECT T1.source_system_code , T1.master_customer_id , T2.council_tax_id FROM CMI_Cross_References AS T1 JOIN Parking_Fines AS T2 ON T1.cmi_cross_ref_id = T2.cmi_cross_ref_id
SELECT T1.council_tax_id FROM Rent_Arrears AS T1 JOIN CMI_Cross_References AS T2 ON T1.cmi_cross_ref_id = T2.cmi_cross_ref_id JOIN Customer_Master_Index AS T3 ON T3.master_customer_id = T2.master_customer_id WHERE T3.cmi_details != 'Schmidt , Kertzmann and Lubowitz'
SELECT T1.electoral_register_id FROM Electoral_Register AS T1 JOIN CMI_Cross_References AS T2 ON T1.cmi_cross_ref_id = T2.cmi_cross_ref_id WHERE T2.source_system_code = 'Electoral' OR T2.source_system_code = 'Tax'
SELECT count( source_system_code) FROM CMI_cross_references
SELECT * FROM customer_master_index ORDER BY cmi_details DESC
SELECT council_tax_id , cmi_cross_ref_id FROM parking_fines
SELECT count(*) FROM rent_arrears
SELECT T2.source_system_code FROM customer_master_index AS T1 JOIN cmi_cross_references AS T2 ON T1.master_customer_id = T2.master_customer_id WHERE T1.cmi_details = 'Gottlieb , Becker and Wyman'
SELECT cmi_cross_ref_id FROM cmi_cross_references EXCEPT SELECT cmi_cross_ref_id FROM parking_fines
SELECT source_system_code FROM cmi_cross_references WHERE source_system_code LIKE '%en%'
SELECT count(*) FROM party
SELECT count(*) FROM party
SELECT Party_Theme FROM party ORDER BY Number_of_hosts ASC
SELECT Party_Theme FROM party ORDER BY Number_of_hosts ASC
SELECT Party_Theme , LOCATION FROM party
SELECT Party_Theme , LOCATION FROM party
SELECT First_year , Last_year FROM party WHERE Party_Theme = "Spring" OR Party_Theme = "Teqnology"
SELECT First_year , Last_year FROM party WHERE Party_Theme = "Spring" OR Party_Theme = "Teqnology"
SELECT avg(Number_of_hosts) FROM party
SELECT avg(Number_of_hosts) FROM party
SELECT LOCATION FROM party ORDER BY Number_of_hosts DESC LIMIT 1
SELECT LOCATION FROM party ORDER BY Number_of_hosts DESC LIMIT 1
SELECT Nationality , COUNT(*) FROM HOST GROUP BY Nationality
SELECT Nationality , COUNT(*) FROM HOST GROUP BY Nationality
SELECT Nationality FROM HOST GROUP BY Nationality ORDER BY COUNT(*) DESC LIMIT 1
SELECT Nationality FROM HOST GROUP BY Nationality ORDER BY COUNT(*) DESC LIMIT 1
SELECT Nationality FROM HOST WHERE Age > 45 INTERSECT SELECT Nationality FROM HOST WHERE Age < 35
SELECT Nationality FROM HOST WHERE Age > 45 INTERSECT SELECT Nationality FROM HOST WHERE Age < 35
SELECT T3.Party_Theme , T2.Name FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID
SELECT T3.Party_Theme , T2.Name FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID
SELECT T3.Location , T2.Name FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID ORDER BY T2.Age
SELECT T3.Location , T2.Name FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID ORDER BY T2.Age
SELECT T3.Location FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID WHERE T2.Age > 50
SELECT T3.Location FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID WHERE T2.Age > 50
SELECT T2.Name FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID WHERE T3.Number_of_hosts > 20
SELECT T2.Name FROM party_host AS T1 JOIN HOST AS T2 ON T1.Host_ID = T2.Host_ID JOIN party AS T3 ON T1.Party_ID = T3.Party_ID WHERE T3.Number_of_hosts > 20
SELECT Name , Nationality FROM HOST ORDER BY Age DESC LIMIT 1
SELECT Name , Nationality FROM HOST ORDER BY Age DESC LIMIT 1
SELECT Name FROM HOST WHERE Host_ID NOT IN (SELECT Host_ID FROM party_host)
SELECT Name FROM HOST WHERE Host_ID NOT IN (SELECT Host_ID FROM party_host)
SELECT ResName FROM Restaurant;
SELECT Address FROM Restaurant WHERE ResName = "Subway";
SELECT Rating FROM Restaurant WHERE ResName = "Subway";
SELECT ResTypeName FROM Restaurant_Type;
SELECT ResTypeDescription FROM Restaurant_Type WHERE ResTypeName = "Sandwich";
SELECT ResName , Rating FROM Restaurant ORDER BY Rating DESC LIMIT 1;
SELECT Age FROM Student WHERE Fname = "Linda" AND Lname = "Smith";
SELECT Sex FROM Student WHERE Fname = "Linda" AND Lname = "Smith";
SELECT Fname , Lname FROM Student WHERE Major = 600;
SELECT city_code FROM Student WHERE Fname = "Linda" AND Lname = "Smith";
SELECT count(*) FROM Student WHERE Advisor = 1121;
SELECT Advisor , count(*) FROM Student GROUP BY Advisor ORDER BY count(Advisor) DESC LIMIT 1;
SELECT Major , count(*) FROM Student GROUP BY Major ORDER BY count(Major) ASC LIMIT 1;
SELECT Major , count(*) FROM Student GROUP BY Major HAVING count(Major) BETWEEN 2 AND 30;
SELECT Fname , Lname FROM Student WHERE Age > 18 AND Major = 600;
SELECT Fname , Lname FROM Student WHERE Age > 18 AND Major != 600 AND Sex = 'F';
SELECT count(*) FROM Restaurant AS T1 JOIN Type_Of_Restaurant AS T2 ON T1.ResID = T2.ResID JOIN Restaurant_Type AS T3 ON T2.ResTypeID = T3.ResTypeID GROUP BY T2.ResTypeID HAVING T3.ResTypeName = 'Sandwich'
SELECT sum(T2.Spent) FROM Student AS T1 JOIN Visits_Restaurant AS T2 ON T1.StuID = T2.StuID WHERE T1.Fname = "Linda" AND T1.Lname = "Smith";
SELECT count(*) FROM Student AS T1 JOIN Visits_Restaurant AS T2 ON T1.StuID = T2.StuID JOIN Restaurant AS T3 ON T2.ResID = T3.ResID WHERE T1.Fname = "Linda" AND T1.Lname = "Smith" AND T3.ResName = "Subway";
SELECT TIME FROM Student AS T1 JOIN Visits_Restaurant AS T2 ON T1.StuID = T2.StuID JOIN Restaurant AS T3 ON T2.ResID = T3.ResID WHERE T1.Fname = "Linda" AND T1.Lname = "Smith" AND T3.ResName = "Subway";
SELECT Restaurant.ResName , sum(Visits_Restaurant.Spent) FROM Visits_Restaurant JOIN Restaurant ON Visits_Restaurant.ResID = Restaurant.ResID GROUP BY Restaurant.ResID ORDER BY sum(Visits_Restaurant.Spent) ASC LIMIT 1;
SELECT T1.Fname , T1.Lname FROM Student AS T1 JOIN Visits_Restaurant AS T2 ON T1.StuID = T2.StuID GROUP BY T1.StuID ORDER BY count(*) DESC LIMIT 1;
SELECT T1.name_full , T1.college_id FROM college AS T1 JOIN player_college AS T2 ON T1.college_id = T2.college_id GROUP BY T1.college_id ORDER BY count(*) DESC LIMIT 1;
SELECT T1.name_full , T1.college_id FROM college AS T1 JOIN player_college AS T2 ON T1.college_id = T2.college_id GROUP BY T1.college_id ORDER BY count(*) DESC LIMIT 1;
SELECT avg(T1.salary) FROM salary AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings'
SELECT avg(T1.salary) FROM salary AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings'
SELECT name_first , name_last FROM player AS T1 JOIN all_star AS T2 ON T1.player_id = T2.player_id WHERE YEAR = 1998
SELECT name_first , name_last FROM player AS T1 JOIN all_star AS T2 ON T1.player_id = T2.player_id WHERE YEAR = 1998
SELECT T1.name_first , T1.name_last , T1.player_id , count(*) FROM player AS T1 JOIN all_star AS T2 ON T1.player_id = T2.player_id GROUP BY T1.player_id ORDER BY count(*) DESC LIMIT 1;
SELECT T1.name_first , T1.name_last , T1.player_id , count(*) FROM player AS T1 JOIN all_star AS T2 ON T1.player_id = T2.player_id GROUP BY T1.player_id ORDER BY count(*) DESC LIMIT 1;
SELECT yearid , count(*) FROM hall_of_fame GROUP BY yearid;
SELECT yearid , count(*) FROM hall_of_fame GROUP BY yearid;
SELECT YEAR , avg(attendance) FROM home_game GROUP BY YEAR;
SELECT YEAR , avg(attendance) FROM home_game GROUP BY YEAR;
SELECT T2.team_id , T2.rank FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id WHERE T1.year = 2014 GROUP BY T1.team_id ORDER BY avg(T1.attendance) DESC LIMIT 1;
SELECT T2.team_id , T2.rank FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id WHERE T1.year = 2014 GROUP BY T1.team_id ORDER BY avg(T1.attendance) DESC LIMIT 1;
SELECT T1.name_first , T1.name_last , T2.player_id FROM player AS T1 JOIN manager_award AS T2 ON T1.player_id = T2.player_id GROUP BY T2.player_id ORDER BY count(*) DESC LIMIT 1;
SELECT T1.name_first , T1.name_last , T2.player_id FROM player AS T1 JOIN manager_award AS T2 ON T1.player_id = T2.player_id GROUP BY T2.player_id ORDER BY count(*) DESC LIMIT 1;
SELECT count(*) FROM park WHERE state = 'NY';
SELECT count(*) FROM park WHERE state = 'NY';
SELECT T1.name_first , T1.name_last , T1.player_id FROM player AS T1 JOIN player_award AS T2 ON T1.player_id = T2.player_id GROUP BY T1.player_id ORDER BY count(*) DESC LIMIT 3;
SELECT T1.name_first , T1.name_last , T1.player_id FROM player AS T1 JOIN player_award AS T2 ON T1.player_id = T2.player_id GROUP BY T1.player_id ORDER BY count(*) DESC LIMIT 3;
SELECT birth_country FROM player GROUP BY birth_country ORDER BY count(*) ASC LIMIT 3;
SELECT birth_country FROM player GROUP BY birth_country ORDER BY count(*) ASC LIMIT 3;
SELECT name_first , name_last FROM player WHERE death_year = '';
SELECT name_first , name_last FROM player WHERE death_year = '';
SELECT count(*) FROM player WHERE birth_country = 'USA' AND bats = 'R';
SELECT count(*) FROM player WHERE birth_country = 'USA' AND bats = 'R';
SELECT avg(T1.height) FROM player AS T1 JOIN player_college AS T2 ON T1.player_id = T2.player_id JOIN college AS T3 ON T3.college_id = T2.college_id WHERE T3.name_full = 'Yale University';
SELECT avg(T1.height) FROM player AS T1 JOIN player_college AS T2 ON T1.player_id = T2.player_id JOIN college AS T3 ON T3.college_id = T2.college_id WHERE T3.name_full = 'Yale University';
SELECT T1.name , T1.team_id , max(T2.salary) FROM team AS T1 JOIN salary AS T2 ON T1.team_id = T2.team_id GROUP BY T1.team_id;
SELECT T1.name , T1.team_id , max(T2.salary) FROM team AS T1 JOIN salary AS T2 ON T1.team_id = T2.team_id GROUP BY T1.team_id;
SELECT T1.name , T1.team_id FROM team AS T1 JOIN salary AS T2 ON T1.team_id = T2.team_id GROUP BY T1.team_id ORDER BY avg(T2.salary) ASC LIMIT 1;
SELECT T1.name , T1.team_id FROM team AS T1 JOIN salary AS T2 ON T1.team_id = T2.team_id GROUP BY T1.team_id ORDER BY avg(T2.salary) ASC LIMIT 1;
SELECT T1.name_first , T1.name_last FROM player AS T1 JOIN player_award AS T2 WHERE T2.year = 1960 INTERSECT SELECT T1.name_first , T1.name_last FROM player AS T1 JOIN player_award AS T2 WHERE T2.year = 1961
SELECT T1.name_first , T1.name_last FROM player AS T1 JOIN player_award AS T2 WHERE T2.year = 1960 INTERSECT SELECT T1.name_first , T1.name_last FROM player AS T1 JOIN player_award AS T2 WHERE T2.year = 1961
SELECT name_first , name_last FROM player WHERE weight > 220 OR height < 75
SELECT name_first , name_last FROM player WHERE weight > 220 OR height < 75
SELECT max(T1.wins) FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T2.name = 'Boston Red Stockings';
SELECT max(T1.wins) FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T2.name = 'Boston Red Stockings';
SELECT count(*) FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_loser = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year = 2009;
SELECT count(*) FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_loser = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year = 2009;
SELECT T2.name , T1.team_id_winner FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T1.year = 2008 GROUP BY T1.team_id_winner ORDER BY count(*) DESC LIMIT 1;
SELECT T2.name , T1.team_id_winner FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T1.year = 2008 GROUP BY T1.team_id_winner ORDER BY count(*) DESC LIMIT 1;
SELECT count(*) , T1.year FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' GROUP BY T1.year
SELECT count(*) , T1.year FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' GROUP BY T1.year
SELECT count(*) FROM ( SELECT * FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' UNION SELECT * FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_loser = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' );
SELECT count(*) FROM ( SELECT * FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_winner = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' UNION SELECT * FROM postseason AS T1 JOIN team AS T2 ON T1.team_id_loser = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' );
SELECT count(*) FROM postseason WHERE YEAR = 1885 AND ties = 1;
SELECT count(*) FROM postseason WHERE YEAR = 1885 AND ties = 1;
SELECT sum(T1.salary) FROM salary AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year = 2010
SELECT sum(T1.salary) FROM salary AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year = 2010
SELECT count(*) FROM salary AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year = 2000
SELECT count(*) FROM salary AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year = 2000
SELECT salary FROM salary WHERE YEAR = 2001 ORDER BY salary DESC LIMIT 3;
SELECT salary FROM salary WHERE YEAR = 2001 ORDER BY salary DESC LIMIT 3;
SELECT salary FROM salary WHERE YEAR = 2010 UNION SELECT salary FROM salary WHERE YEAR = 2001
SELECT salary FROM salary WHERE YEAR = 2010 UNION SELECT salary FROM salary WHERE YEAR = 2001
SELECT yearid FROM hall_of_fame GROUP BY yearid ORDER BY count(*) ASC LIMIT 1;
SELECT yearid FROM hall_of_fame GROUP BY yearid ORDER BY count(*) ASC LIMIT 1;
SELECT count(*) FROM park WHERE city = 'Atlanta';
SELECT count(*) FROM park WHERE city = 'Atlanta';
SELECT count(*) FROM home_game AS T1 JOIN park AS T2 ON T1.park_id = T2.park_id WHERE T1.year = 1907 AND T2.park_name = 'Columbia Park';
SELECT count(*) FROM home_game AS T1 JOIN park AS T2 ON T1.park_id = T2.park_id WHERE T1.year = 1907 AND T2.park_name = 'Columbia Park';
SELECT count(*) FROM home_game AS T1 JOIN park AS T2 ON T1.park_id = T2.park_id WHERE T1.year = 2000 AND T2.city = 'Atlanta';
SELECT count(*) FROM home_game AS T1 JOIN park AS T2 ON T1.park_id = T2.park_id WHERE T1.year = 2000 AND T2.city = 'Atlanta';
SELECT sum(T1.attendance) FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year BETWEEN 2000 AND 2010;
SELECT sum(T1.attendance) FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year BETWEEN 2000 AND 2010;
SELECT sum(T1.salary) FROM salary AS T1 JOIN player AS T2 ON T1.player_id = T2.player_id WHERE T2.name_first = 'Len' AND T2.name_last = 'Barker' AND T1.year BETWEEN 1985 AND 1990;
SELECT sum(T1.salary) FROM salary AS T1 JOIN player AS T2 ON T1.player_id = T2.player_id WHERE T2.name_first = 'Len' AND T2.name_last = 'Barker' AND T1.year BETWEEN 1985 AND 1990;
SELECT T2.name_first , T2.name_last FROM salary AS T1 JOIN player AS T2 ON T1.player_id = T2.player_id JOIN team AS T3 ON T3.team_id_br = T1.team_id WHERE T1.year = 2005 AND T3.name = 'Washington Nationals' INTERSECT SELECT T2.name_first , T2.name_last FROM salary AS T1 JOIN player AS T2 ON T1.player_id = T2.player_id JOIN team AS T3 ON T3.team_id_br = T1.team_id WHERE T1.year = 2007 AND T3.name = 'Washington Nationals'
SELECT T2.name_first , T2.name_last FROM salary AS T1 JOIN player AS T2 ON T1.player_id = T2.player_id JOIN team AS T3 ON T3.team_id_br = T1.team_id WHERE T1.year = 2005 AND T3.name = 'Washington Nationals' INTERSECT SELECT T2.name_first , T2.name_last FROM salary AS T1 JOIN player AS T2 ON T1.player_id = T2.player_id JOIN team AS T3 ON T3.team_id_br = T1.team_id WHERE T1.year = 2007 AND T3.name = 'Washington Nationals'
SELECT sum(T1.games) FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year BETWEEN 1990 AND 2000;
SELECT sum(T1.games) FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T2.name = 'Boston Red Stockings' AND T1.year BETWEEN 1990 AND 2000;
SELECT T2.name FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T1.year = 1980 ORDER BY T1.attendance ASC LIMIT 1;
SELECT T2.name FROM home_game AS T1 JOIN team AS T2 ON T1.team_id = T2.team_id_br WHERE T1.year = 1980 ORDER BY T1.attendance ASC LIMIT 1;
SELECT state FROM park GROUP BY state HAVING count(*) > 2;
SELECT state FROM park GROUP BY state HAVING count(*) > 2;
SELECT count(*) FROM team_franchise WHERE active = 'Y';
SELECT count(*) FROM team_franchise WHERE active = 'Y';
SELECT city FROM park GROUP BY city HAVING count(*) BETWEEN 2 AND 4;
SELECT city FROM park GROUP BY city HAVING count(*) BETWEEN 2 AND 4;
SELECT T2.park_name FROM home_game AS T1 JOIN park AS T2 ON T1.park_id = T2.park_id WHERE T1.year = 2008 ORDER BY T1.attendance DESC LIMIT 1;
SELECT T2.park_name FROM home_game AS T1 JOIN park AS T2 ON T1.park_id = T2.park_id WHERE T1.year = 2008 ORDER BY T1.attendance DESC LIMIT 1;
SELECT count(*) FROM company WHERE Headquarters = 'USA'
SELECT Name FROM company ORDER BY Sales_in_Billion ASC
SELECT Headquarters , Industry FROM company
SELECT Name FROM company WHERE Industry = "Banking" OR Industry = "Retailing"
SELECT max(Market_Value_in_Billion) , min(Market_Value_in_Billion) FROM company
SELECT Headquarters FROM company ORDER BY Sales_in_Billion DESC LIMIT 1
SELECT Headquarters , COUNT(*) FROM company GROUP BY Headquarters
SELECT Headquarters FROM company GROUP BY Headquarters ORDER BY COUNT(*) DESC LIMIT 1
SELECT Headquarters FROM company GROUP BY Headquarters HAVING COUNT(*) >= 2
SELECT Headquarters FROM company WHERE Industry = "Banking" INTERSECT SELECT Headquarters FROM company WHERE Industry = "Oil and gas"
SELECT T3.Name , T2.Name FROM employment AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID JOIN company AS T3 ON T1.Company_ID = T3.Company_ID
SELECT T3.Name , T2.Name FROM employment AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID JOIN company AS T3 ON T1.Company_ID = T3.Company_ID ORDER BY T1.Year_working
SELECT T2.Name FROM employment AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID JOIN company AS T3 ON T1.Company_ID = T3.Company_ID WHERE T3.Sales_in_Billion > 200
SELECT T3.Name , COUNT(*) FROM employment AS T1 JOIN people AS T2 ON T1.People_ID = T2.People_ID JOIN company AS T3 ON T1.Company_ID = T3.Company_ID GROUP BY T3.Name
SELECT Name FROM people WHERE People_ID NOT IN (SELECT People_ID FROM employment)
SELECT name FROM company WHERE Sales_in_Billion > 200 ORDER BY Sales_in_Billion , Profits_in_Billion DESC
SELECT count(*) FROM Ref_calendar
SELECT count(*) FROM Ref_calendar
SELECT calendar_date , day_Number FROM Ref_calendar
SELECT calendar_date , day_Number FROM Ref_calendar
SELECT count(*) FROM Ref_document_types
SELECT count(*) FROM Ref_document_types
SELECT document_type_code , document_type_name FROM Ref_document_types
SELECT document_type_code , document_type_name FROM Ref_document_types
SELECT document_type_name , document_type_description FROM Ref_document_types WHERE document_type_code = "RV"
SELECT document_type_name , document_type_description FROM Ref_document_types WHERE document_type_code = "RV"
SELECT document_type_code FROM Ref_document_types WHERE document_type_name = "Paper"
SELECT document_type_code FROM Ref_document_types WHERE document_type_name = "Paper"
SELECT count(*) FROM All_documents WHERE document_type_code = "CV" OR document_type_code = "BK"
SELECT count(*) FROM All_documents WHERE document_type_code = "CV" OR document_type_code = "BK"
SELECT date_stored FROM All_documents WHERE Document_name = "Marry CV"
SELECT date_stored FROM All_documents WHERE Document_name = "Marry CV"
SELECT T2.day_Number , T1.Date_Stored FROM All_documents AS T1 JOIN Ref_calendar AS T2 ON T1.date_stored = T2.calendar_date
SELECT T2.day_Number , T1.Date_Stored FROM All_documents AS T1 JOIN Ref_calendar AS T2 ON T1.date_stored = T2.calendar_date
SELECT T2.document_type_name FROM All_documents AS T1 JOIN Ref_document_types AS T2 ON T1.document_type_code = T2.document_type_code WHERE T1.document_name = "How to read a book"
SELECT T2.document_type_name FROM All_documents AS T1 JOIN Ref_document_types AS T2 ON T1.document_type_code = T2.document_type_code WHERE T1.document_name = "How to read a book"
SELECT count(*) FROM Ref_locations
SELECT count(*) FROM Ref_locations
SELECT location_code , location_name FROM Ref_locations
SELECT location_code , location_name FROM Ref_locations
SELECT location_name , location_description FROM Ref_locations WHERE location_code = "x"
SELECT location_name , location_description FROM Ref_locations WHERE location_code = "x"
SELECT location_code FROM Ref_locations WHERE location_name = "Canada"
SELECT location_code FROM Ref_locations WHERE location_name = "Canada"
SELECT count(*) FROM ROLES
SELECT count(*) FROM ROLES
SELECT role_code , role_name , role_description FROM ROLES
SELECT role_code , role_name , role_description FROM ROLES
SELECT role_name , role_description FROM ROLES WHERE role_code = "MG"
SELECT role_name , role_description FROM ROLES WHERE role_code = "MG"
SELECT role_description FROM ROLES WHERE role_name = "Proof Reader"
SELECT role_description FROM ROLES WHERE role_name = "Proof Reader"
SELECT count(*) FROM Employees
SELECT count(*) FROM Employees
SELECT employee_name , role_code , date_of_birth FROM Employees WHERE employee_Name = 'Armani'
SELECT employee_name , role_code , date_of_birth FROM Employees WHERE employee_Name = 'Armani'
SELECT employee_ID FROM Employees WHERE employee_name = "Ebba"
SELECT employee_ID FROM Employees WHERE employee_name = "Ebba"
SELECT employee_name FROM Employees WHERE role_code = "HR"
SELECT employee_name FROM Employees WHERE role_code = "HR"
SELECT role_code , count(*) FROM Employees GROUP BY role_code
SELECT role_code , count(*) FROM Employees GROUP BY role_code
SELECT role_code FROM Employees GROUP BY role_code ORDER BY count(*) DESC LIMIT 1
SELECT role_code FROM Employees GROUP BY role_code ORDER BY count(*) DESC LIMIT 1
SELECT role_code FROM Employees GROUP BY role_code HAVING count(*) >= 3
SELECT role_code FROM Employees GROUP BY role_code HAVING count(*) >= 3
SELECT role_code FROM Employees GROUP BY role_code ORDER BY count(*) ASC LIMIT 1
SELECT role_code FROM Employees GROUP BY role_code ORDER BY count(*) ASC LIMIT 1
SELECT T2.role_name , T2.role_description FROM Employees AS T1 JOIN ROLES AS T2 ON T1.role_code = T2.role_code WHERE T1.employee_name = "Ebba"
SELECT T2.role_name , T2.role_description FROM Employees AS T1 JOIN ROLES AS T2 ON T1.role_code = T2.role_code WHERE T1.employee_name = "Ebba"
SELECT T1.employee_name FROM Employees AS T1 JOIN ROLES AS T2 ON T1.role_code = T2.role_code WHERE T2.role_name = "Editor"
SELECT T1.employee_name FROM Employees AS T1 JOIN ROLES AS T2 ON T1.role_code = T2.role_code WHERE T2.role_name = "Editor"
SELECT T1.employee_id FROM Employees AS T1 JOIN ROLES AS T2 ON T1.role_code = T2.role_code WHERE T2.role_name = "Human Resource" OR T2.role_name = "Manager"
SELECT T1.employee_id FROM Employees AS T1 JOIN ROLES AS T2 ON T1.role_code = T2.role_code WHERE T2.role_name = "Human Resource" OR T2.role_name = "Manager"
SELECT location_code FROM Document_locations
SELECT location_code FROM Document_locations
SELECT T3.location_name FROM All_documents AS T1 JOIN Document_locations AS T2 ON T1.document_id = T2.document_id JOIN Ref_locations AS T3 ON T2.location_code = T3.location_code WHERE T1.document_name = "Robin CV"
SELECT T3.location_name FROM All_documents AS T1 JOIN Document_locations AS T2 ON T1.document_id = T2.document_id JOIN Ref_locations AS T3 ON T2.location_code = T3.location_code WHERE T1.document_name = "Robin CV"
SELECT location_code , date_in_location_from , date_in_locaton_to FROM Document_locations
SELECT location_code , date_in_location_from , date_in_locaton_to FROM Document_locations
SELECT T1.date_in_location_from , T1.date_in_locaton_to FROM Document_locations AS T1 JOIN All_documents AS T2 ON T1.document_id = T2.document_id WHERE T2.document_name = "Robin CV"
SELECT T1.date_in_location_from , T1.date_in_locaton_to FROM Document_locations AS T1 JOIN All_documents AS T2 ON T1.document_id = T2.document_id WHERE T2.document_name = "Robin CV"
SELECT location_code , count(*) FROM Document_locations GROUP BY location_code
SELECT location_code , count(*) FROM Document_locations GROUP BY location_code
SELECT location_code FROM Document_locations GROUP BY location_code ORDER BY count(*) DESC LIMIT 1
SELECT location_code FROM Document_locations GROUP BY location_code ORDER BY count(*) DESC LIMIT 1
SELECT location_code FROM Document_locations GROUP BY location_code HAVING count(*) >= 3
SELECT location_code FROM Document_locations GROUP BY location_code HAVING count(*) >= 3
SELECT T2.location_name , T1.location_code FROM Document_locations AS T1 JOIN Ref_locations AS T2 ON T1.location_code = T2.location_code GROUP BY T1.location_code ORDER BY count(*) ASC LIMIT 1
SELECT T2.location_name , T1.location_code FROM Document_locations AS T1 JOIN Ref_locations AS T2 ON T1.location_code = T2.location_code GROUP BY T1.location_code ORDER BY count(*) ASC LIMIT 1
SELECT T2.employee_name , T3.employee_name FROM Documents_to_be_destroyed AS T1 JOIN Employees AS T2 ON T1.Destruction_Authorised_by_Employee_ID = T2.employee_id JOIN Employees AS T3 ON T1.Destroyed_by_Employee_ID = T3.employee_id;
SELECT T2.employee_name , T3.employee_name FROM Documents_to_be_destroyed AS T1 JOIN Employees AS T2 ON T1.Destruction_Authorised_by_Employee_ID = T2.employee_id JOIN Employees AS T3 ON T1.Destroyed_by_Employee_ID = T3.employee_id;
SELECT Destruction_Authorised_by_Employee_ID , count(*) FROM Documents_to_be_destroyed GROUP BY Destruction_Authorised_by_Employee_ID
SELECT Destruction_Authorised_by_Employee_ID , count(*) FROM Documents_to_be_destroyed GROUP BY Destruction_Authorised_by_Employee_ID
SELECT Destroyed_by_Employee_ID , count(*) FROM Documents_to_be_destroyed GROUP BY Destroyed_by_Employee_ID
SELECT Destroyed_by_Employee_ID , count(*) FROM Documents_to_be_destroyed GROUP BY Destroyed_by_Employee_ID
SELECT employee_id FROM Employees EXCEPT SELECT Destruction_Authorised_by_Employee_ID FROM Documents_to_be_destroyed
SELECT employee_id FROM Employees EXCEPT SELECT Destruction_Authorised_by_Employee_ID FROM Documents_to_be_destroyed
SELECT Destruction_Authorised_by_Employee_ID FROM Documents_to_be_destroyed
SELECT Destruction_Authorised_by_Employee_ID FROM Documents_to_be_destroyed
SELECT Destroyed_by_Employee_ID FROM Documents_to_be_destroyed
SELECT Destroyed_by_Employee_ID FROM Documents_to_be_destroyed
SELECT employee_id FROM Employees EXCEPT SELECT Destroyed_by_Employee_ID FROM Documents_to_be_destroyed
SELECT employee_id FROM Employees EXCEPT SELECT Destroyed_by_Employee_ID FROM Documents_to_be_destroyed
SELECT Destroyed_by_Employee_ID FROM Documents_to_be_destroyed UNION SELECT Destruction_Authorised_by_Employee_ID FROM Documents_to_be_destroyed
SELECT Destroyed_by_Employee_ID FROM Documents_to_be_destroyed UNION SELECT Destruction_Authorised_by_Employee_ID FROM Documents_to_be_destroyed
SELECT count(*) FROM club
SELECT count(*) FROM club
SELECT clubname FROM club
SELECT clubname FROM club
SELECT count(*) FROM student
SELECT count(*) FROM student
SELECT fname FROM student
SELECT fname FROM student
SELECT t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore"
SELECT t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore"
SELECT t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises"
SELECT t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Tennis Club"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Tennis Club"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Pen and Paper Gaming"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Pen and Paper Gaming"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Linda" AND t3.lname = "Smith"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Linda" AND t3.lname = "Smith"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Tracy" AND t3.lname = "Kim"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Tracy" AND t3.lname = "Kim"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.sex = "F"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.sex = "F"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises" AND t3.sex = "M"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises" AND t3.sex = "M"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.major = "600"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.major = "600"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.major = "600" GROUP BY t1.clubname ORDER BY count(*) DESC LIMIT 1
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.major = "600" GROUP BY t1.clubname ORDER BY count(*) DESC LIMIT 1
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.sex = "F" GROUP BY t1.clubname ORDER BY count(*) DESC LIMIT 1
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.sex = "F" GROUP BY t1.clubname ORDER BY count(*) DESC LIMIT 1
SELECT clubdesc FROM club WHERE clubname = "Tennis Club"
SELECT clubdesc FROM club WHERE clubname = "Tennis Club"
SELECT clubdesc FROM club WHERE clubname = "Pen and Paper Gaming"
SELECT clubdesc FROM club WHERE clubname = "Pen and Paper Gaming"
SELECT clublocation FROM club WHERE clubname = "Tennis Club"
SELECT clublocation FROM club WHERE clubname = "Tennis Club"
SELECT clublocation FROM club WHERE clubname = "Pen and Paper Gaming"
SELECT clublocation FROM club WHERE clubname = "Pen and Paper Gaming"
SELECT clublocation FROM club WHERE clubname = "Hopkins Student Enterprises"
SELECT clublocation FROM club WHERE clubname = "Hopkins Student Enterprises"
SELECT clubname FROM club WHERE clublocation = "AKW"
SELECT clubname FROM club WHERE clublocation = "AKW"
SELECT count(*) FROM club WHERE clublocation = "HHH"
SELECT count(*) FROM club WHERE clublocation = "HHH"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t2.position = "President"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t2.position = "President"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises" AND t2.position = "CTO"
SELECT t3.fname , t3.lname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises" AND t2.position = "CTO"
SELECT count( t2.position) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid WHERE t1.clubname = "Bootup Baltimore"
SELECT count( t2.position) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid WHERE t1.clubname = "Bootup Baltimore"
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.age > 18
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.age > 18
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.age < 18
SELECT count(*) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore" AND t3.age < 18
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.city_code = "BAL"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.city_code = "BAL"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.city_code = "HOU"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.city_code = "HOU"
SELECT count( t1.clubname) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Eric" AND t3.lname = "Tai"
SELECT count( t1.clubname) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Eric" AND t3.lname = "Tai"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Davis" AND t3.lname = "Steven"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.fname = "Davis" AND t3.lname = "Steven"
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.advisor = 1121
SELECT t1.clubname FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t3.advisor = 1121
SELECT avg(t3.age) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore"
SELECT avg(t3.age) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Bootup Baltimore"
SELECT avg(t3.age) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises"
SELECT avg(t3.age) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Hopkins Student Enterprises"
SELECT avg(t3.age) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Tennis Club"
SELECT avg(t3.age) FROM club AS t1 JOIN member_of_club AS t2 ON t1.clubid = t2.clubid JOIN student AS t3 ON t2.stuid = t3.stuid WHERE t1.clubname = "Tennis Club"
SELECT T1.grant_amount FROM Grants AS T1 JOIN Documents AS T2 ON T1.grant_id = T2.grant_id WHERE T2.sent_date < '1986-08-26 20:49:27' INTERSECT SELECT grant_amount FROM grants WHERE grant_end_date > '1989-03-16 18:27:16'
SELECT T1.grant_amount FROM Grants AS T1 JOIN Documents AS T2 ON T1.grant_id = T2.grant_id WHERE T2.sent_date < '1986-08-26 20:49:27' INTERSECT SELECT grant_amount FROM grants WHERE grant_end_date > '1989-03-16 18:27:16'
SELECT T1.project_details FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id WHERE T2.outcome_code = 'Paper' INTERSECT SELECT T1.project_details FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id WHERE T2.outcome_code = 'Patent'
SELECT T1.project_details FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id WHERE T2.outcome_code = 'Paper' INTERSECT SELECT T1.project_details FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id WHERE T2.outcome_code = 'Patent'
SELECT sum(grant_amount) FROM Grants AS T1 JOIN Organisations AS T2 ON T1.organisation_id = T2.organisation_id JOIN organisation_Types AS T3 ON T2.organisation_type = T3.organisation_type WHERE T3.organisation_type_description = 'Research'
SELECT sum(grant_amount) FROM Grants AS T1 JOIN Organisations AS T2 ON T1.organisation_id = T2.organisation_id JOIN organisation_Types AS T3 ON T2.organisation_type = T3.organisation_type WHERE T3.organisation_type_description = 'Research'
SELECT date_from , date_to FROM Project_Staff WHERE project_id IN( SELECT project_id FROM Project_Staff GROUP BY project_id ORDER BY count(*) DESC LIMIT 1 ) UNION SELECT date_from , date_to FROM Project_Staff WHERE role_code = 'leader'
SELECT date_from , date_to FROM Project_Staff WHERE project_id IN( SELECT project_id FROM Project_Staff GROUP BY project_id ORDER BY count(*) DESC LIMIT 1 ) UNION SELECT date_from , date_to FROM Project_Staff WHERE role_code = 'leader'
SELECT T2.organisation_id , T2.organisation_details FROM Grants AS T1 JOIN Organisations AS T2 ON T1.organisation_id = T2.organisation_id GROUP BY T2.organisation_id HAVING sum(T1.grant_amount) > 6000
SELECT T2.organisation_id , T2.organisation_details FROM Grants AS T1 JOIN Organisations AS T2 ON T1.organisation_id = T2.organisation_id GROUP BY T2.organisation_id HAVING sum(T1.grant_amount) > 6000
SELECT T1.organisation_type , T1.organisation_id FROM Organisations AS T1 JOIN Research_Staff AS T2 ON T1.organisation_id = T2.employer_organisation_id GROUP BY T1.organisation_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.organisation_type , T1.organisation_id FROM Organisations AS T1 JOIN Research_Staff AS T2 ON T1.organisation_id = T2.employer_organisation_id GROUP BY T1.organisation_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.organisation_type FROM Organisations AS T1 JOIN Research_Staff AS T2 ON T1.organisation_id = T2.employer_organisation_id GROUP BY T1.organisation_type ORDER BY count(*) DESC LIMIT 1
SELECT T1.organisation_type FROM Organisations AS T1 JOIN Research_Staff AS T2 ON T1.organisation_id = T2.employer_organisation_id GROUP BY T1.organisation_type ORDER BY count(*) DESC LIMIT 1
SELECT T1.sent_date FROM documents AS T1 JOIN Grants AS T2 ON T1.grant_id = T2.grant_id JOIN Organisations AS T3 ON T2.organisation_id = T3.organisation_id JOIN organisation_Types AS T4 ON T3.organisation_type = T4.organisation_type WHERE T2.grant_amount > 5000 AND T4.organisation_type_description = 'Research'
SELECT T1.sent_date FROM documents AS T1 JOIN Grants AS T2 ON T1.grant_id = T2.grant_id JOIN Organisations AS T3 ON T2.organisation_id = T3.organisation_id JOIN organisation_Types AS T4 ON T3.organisation_type = T4.organisation_type WHERE T2.grant_amount > 5000 AND T4.organisation_type_description = 'Research'
SELECT T1.response_received_date FROM Documents AS T1 JOIN Document_Types AS T2 ON T1.document_type_code = T2.document_type_code JOIN Grants AS T3 ON T1.grant_id = T3.grant_id WHERE T2.document_description = 'Regular' OR T3.grant_amount > 100
SELECT T1.response_received_date FROM Documents AS T1 JOIN Document_Types AS T2 ON T1.document_type_code = T2.document_type_code JOIN Grants AS T3 ON T1.grant_id = T3.grant_id WHERE T2.document_description = 'Regular' OR T3.grant_amount > 100
SELECT project_details FROM Projects WHERE project_id NOT IN ( SELECT project_id FROM Project_Staff WHERE role_code = 'researcher' )
SELECT project_details FROM Projects WHERE project_id NOT IN ( SELECT project_id FROM Project_Staff WHERE role_code = 'researcher' )
SELECT T1.task_details , T1.task_id , T2.project_id FROM Tasks AS T1 JOIN Projects AS T2 ON T1.project_id = T2.project_id WHERE T2.project_details = 'omnis' UNION SELECT T1.task_details , T1.task_id , T2.project_id FROM Tasks AS T1 JOIN Projects AS T2 ON T1.project_id = T2.project_id JOIN Project_outcomes AS T3 ON T2.project_id = T3.project_id GROUP BY T2.project_id HAVING count(*) > 2
SELECT T1.task_details , T1.task_id , T2.project_id FROM Tasks AS T1 JOIN Projects AS T2 ON T1.project_id = T2.project_id WHERE T2.project_details = 'omnis' UNION SELECT T1.task_details , T1.task_id , T2.project_id FROM Tasks AS T1 JOIN Projects AS T2 ON T1.project_id = T2.project_id JOIN Project_outcomes AS T3 ON T2.project_id = T3.project_id GROUP BY T2.project_id HAVING count(*) > 2
SELECT date_from , date_to FROM Project_Staff WHERE role_code = 'researcher'
SELECT date_from , date_to FROM Project_Staff WHERE role_code = 'researcher'
SELECT count( role_code) FROM Project_Staff
SELECT count( role_code) FROM Project_Staff
SELECT sum(grant_amount) , organisation_id FROM Grants GROUP BY organisation_id
SELECT sum(grant_amount) , organisation_id FROM Grants GROUP BY organisation_id
SELECT T1.project_details FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id JOIN Research_outcomes AS T3 ON T2.outcome_code = T3.outcome_code WHERE T3.outcome_description LIKE '%Published%'
SELECT T1.project_details FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id JOIN Research_outcomes AS T3 ON T2.outcome_code = T3.outcome_code WHERE T3.outcome_description LIKE '%Published%'
SELECT T1.project_id , count(*) FROM Project_Staff AS T1 JOIN Projects AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id ORDER BY count(*) ASC
SELECT T1.project_id , count(*) FROM Project_Staff AS T1 JOIN Projects AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id ORDER BY count(*) ASC
SELECT role_description FROM Staff_Roles WHERE role_code = 'researcher'
SELECT role_description FROM Staff_Roles WHERE role_code = 'researcher'
SELECT date_from FROM Project_Staff ORDER BY date_from ASC LIMIT 1
SELECT date_from FROM Project_Staff ORDER BY date_from ASC LIMIT 1
SELECT T1.project_details , T1.project_id FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.project_details , T1.project_id FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id ORDER BY count(*) DESC LIMIT 1
SELECT project_details FROM Projects WHERE project_id NOT IN ( SELECT project_id FROM Project_outcomes )
SELECT project_details FROM Projects WHERE project_id NOT IN ( SELECT project_id FROM Project_outcomes )
SELECT T1.organisation_id , T1.organisation_type , T1.organisation_details FROM Organisations AS T1 JOIN Research_Staff AS T2 ON T1.organisation_id = T2.employer_organisation_id GROUP BY T1.organisation_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.organisation_id , T1.organisation_type , T1.organisation_details FROM Organisations AS T1 JOIN Research_Staff AS T2 ON T1.organisation_id = T2.employer_organisation_id GROUP BY T1.organisation_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.role_description , T2.staff_id FROM Staff_Roles AS T1 JOIN Project_Staff AS T2 ON T1.role_code = T2.role_code JOIN Project_outcomes AS T3 ON T2.project_id = T3.project_id GROUP BY T2.staff_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.role_description , T2.staff_id FROM Staff_Roles AS T1 JOIN Project_Staff AS T2 ON T1.role_code = T2.role_code JOIN Project_outcomes AS T3 ON T2.project_id = T3.project_id GROUP BY T2.staff_id ORDER BY count(*) DESC LIMIT 1
SELECT document_type_code FROM Document_Types WHERE document_description LIKE 'Initial%'
SELECT document_type_code FROM Document_Types WHERE document_description LIKE 'Initial%'
SELECT T1.grant_start_date FROM Grants AS T1 JOIN Documents AS T2 ON T1.grant_id = T2.grant_id JOIN Document_Types AS T3 ON T2.document_type_code = T3.document_type_code WHERE T3.document_description = 'Regular' INTERSECT SELECT T1.grant_start_date FROM Grants AS T1 JOIN Documents AS T2 ON T1.grant_id = T2.grant_id JOIN Document_Types AS T3 ON T2.document_type_code = T3.document_type_code WHERE T3.document_description = 'Initial Application'
SELECT T1.grant_start_date FROM Grants AS T1 JOIN Documents AS T2 ON T1.grant_id = T2.grant_id JOIN Document_Types AS T3 ON T2.document_type_code = T3.document_type_code WHERE T3.document_description = 'Regular' INTERSECT SELECT T1.grant_start_date FROM Grants AS T1 JOIN Documents AS T2 ON T1.grant_id = T2.grant_id JOIN Document_Types AS T3 ON T2.document_type_code = T3.document_type_code WHERE T3.document_description = 'Initial Application'
SELECT grant_id , count(*) FROM Documents GROUP BY grant_id ORDER BY count(*) DESC LIMIT 1
SELECT grant_id , count(*) FROM Documents GROUP BY grant_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.organisation_type_description FROM organisation_Types AS T1 JOIN Organisations AS T2 ON T1.organisation_type = T2.organisation_type WHERE T2.organisation_details = 'quo'
SELECT T1.organisation_type_description FROM organisation_Types AS T1 JOIN Organisations AS T2 ON T1.organisation_type = T2.organisation_type WHERE T2.organisation_details = 'quo'
SELECT organisation_details FROM Organisations AS T1 JOIN organisation_Types AS T2 ON T1.organisation_type = T2.organisation_type WHERE T2.organisation_type_description = 'Sponsor' ORDER BY organisation_details
SELECT organisation_details FROM Organisations AS T1 JOIN organisation_Types AS T2 ON T1.organisation_type = T2.organisation_type WHERE T2.organisation_type_description = 'Sponsor' ORDER BY organisation_details
SELECT count(*) FROM Project_outcomes WHERE outcome_code = 'Patent'
SELECT count(*) FROM Project_outcomes WHERE outcome_code = 'Patent'
SELECT count(*) FROM Project_Staff WHERE role_code = 'leader' OR date_from < '1989-04-24 23:51:54'
SELECT count(*) FROM Project_Staff WHERE role_code = 'leader' OR date_from < '1989-04-24 23:51:54'
SELECT date_to FROM Project_Staff ORDER BY date_to DESC LIMIT 1
SELECT date_to FROM Project_Staff ORDER BY date_to DESC LIMIT 1
SELECT T1.outcome_description FROM Research_outcomes AS T1 JOIN Project_outcomes AS T2 ON T1.outcome_code = T2.outcome_code JOIN Projects AS T3 ON T2.project_id = T3.project_id WHERE T3.project_details = 'sint'
SELECT T1.outcome_description FROM Research_outcomes AS T1 JOIN Project_outcomes AS T2 ON T1.outcome_code = T2.outcome_code JOIN Projects AS T3 ON T2.project_id = T3.project_id WHERE T3.project_details = 'sint'
SELECT T1.organisation_id , count(*) FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id GROUP BY T1.organisation_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.organisation_id , count(*) FROM Projects AS T1 JOIN Project_outcomes AS T2 ON T1.project_id = T2.project_id GROUP BY T1.organisation_id ORDER BY count(*) DESC LIMIT 1
SELECT project_details FROM Projects WHERE organisation_id IN ( SELECT organisation_id FROM Projects GROUP BY organisation_id ORDER BY count(*) DESC LIMIT 1 )
SELECT project_details FROM Projects WHERE organisation_id IN ( SELECT organisation_id FROM Projects GROUP BY organisation_id ORDER BY count(*) DESC LIMIT 1 )
SELECT staff_details FROM Research_Staff ORDER BY staff_details ASC
SELECT staff_details FROM Research_Staff ORDER BY staff_details ASC
SELECT count(*) FROM Tasks
SELECT count(*) FROM Tasks
SELECT count(*) , T1.project_details FROM Projects AS T1 JOIN Tasks AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id
SELECT count(*) , T1.project_details FROM Projects AS T1 JOIN Tasks AS T2 ON T1.project_id = T2.project_id GROUP BY T1.project_id
SELECT role_code FROM Project_Staff WHERE date_from > '2003-04-19 15:06:20' AND date_to < '2016-03-15 00:33:18'
SELECT role_code FROM Project_Staff WHERE date_from > '2003-04-19 15:06:20' AND date_to < '2016-03-15 00:33:18'
SELECT T1.outcome_description FROM Research_outcomes AS T1 JOIN Project_outcomes AS T2 ON T1.outcome_code = T2.outcome_code
SELECT T1.outcome_description FROM Research_outcomes AS T1 JOIN Project_outcomes AS T2 ON T1.outcome_code = T2.outcome_code
SELECT role_code FROM Project_Staff GROUP BY role_code ORDER BY count(*) DESC LIMIT 1
SELECT role_code FROM Project_Staff GROUP BY role_code ORDER BY count(*) DESC LIMIT 1
SELECT count(*) FROM member
SELECT Name FROM member ORDER BY Name ASC
SELECT Name , Country FROM member
SELECT Name FROM member WHERE Country = "United States" OR Country = "Canada"
SELECT Country , COUNT(*) FROM member GROUP BY Country
SELECT Country FROM member GROUP BY Country ORDER BY COUNT(*) DESC LIMIT 1
SELECT Country FROM member GROUP BY Country HAVING COUNT(*) > 2
SELECT Leader_Name , College_Location FROM college
SELECT T2.Name , T1.Name FROM college AS T1 JOIN member AS T2 ON T1.College_ID = T2.College_ID
SELECT T2.Name , T1.College_Location FROM college AS T1 JOIN member AS T2 ON T1.College_ID = T2.College_ID ORDER BY T2.Name ASC
SELECT T1.Leader_Name FROM college AS T1 JOIN member AS T2 ON T1.College_ID = T2.College_ID WHERE T2.Country = "Canada"
SELECT T1.Name , T2.Decoration_Theme FROM member AS T1 JOIN round AS T2 ON T1.Member_ID = T2.Member_ID
SELECT T1.Name FROM member AS T1 JOIN round AS T2 ON T1.Member_ID = T2.Member_ID WHERE T2.Rank_in_Round > 3
SELECT T1.Name FROM member AS T1 JOIN round AS T2 ON T1.Member_ID = T2.Member_ID ORDER BY Rank_in_Round ASC
SELECT Name FROM member WHERE Member_ID NOT IN (SELECT Member_ID FROM round)
SELECT count(*) FROM Companies
SELECT count(*) FROM Companies
SELECT name FROM Companies ORDER BY Market_Value_billion DESC
SELECT name FROM Companies ORDER BY Market_Value_billion DESC
SELECT name FROM Companies WHERE Headquarters != 'USA'
SELECT name FROM Companies WHERE Headquarters != 'USA'
SELECT name , Assets_billion FROM Companies ORDER BY name ASC
SELECT name , Assets_billion FROM Companies ORDER BY name ASC
SELECT avg(Profits_billion) FROM Companies
SELECT avg(Profits_billion) FROM Companies
SELECT max(Sales_billion) , min(Sales_billion) FROM Companies WHERE Industry != "Banking"
SELECT max(Sales_billion) , min(Sales_billion) FROM Companies WHERE Industry != "Banking"
SELECT count( Industry) FROM Companies
SELECT count( Industry) FROM Companies
SELECT name FROM buildings ORDER BY Height DESC
SELECT name FROM buildings ORDER BY Height DESC
SELECT Stories FROM buildings ORDER BY Height DESC LIMIT 1
SELECT Stories FROM buildings ORDER BY Height DESC LIMIT 1
SELECT T3.name , T2.name FROM Office_locations AS T1 JOIN buildings AS T2 ON T1.building_id = T2.id JOIN Companies AS T3 ON T1.company_id = T3.id
SELECT T3.name , T2.name FROM Office_locations AS T1 JOIN buildings AS T2 ON T1.building_id = T2.id JOIN Companies AS T3 ON T1.company_id = T3.id
SELECT T2.name FROM Office_locations AS T1 JOIN buildings AS T2 ON T1.building_id = T2.id JOIN Companies AS T3 ON T1.company_id = T3.id GROUP BY T1.building_id HAVING COUNT(*) > 1
SELECT T2.name FROM Office_locations AS T1 JOIN buildings AS T2 ON T1.building_id = T2.id JOIN Companies AS T3 ON T1.company_id = T3.id GROUP BY T1.building_id HAVING COUNT(*) > 1
SELECT T2.name FROM Office_locations AS T1 JOIN buildings AS T2 ON T1.building_id = T2.id JOIN Companies AS T3 ON T1.company_id = T3.id GROUP BY T1.building_id ORDER BY COUNT(*) DESC LIMIT 1
SELECT T2.name FROM Office_locations AS T1 JOIN buildings AS T2 ON T1.building_id = T2.id JOIN Companies AS T3 ON T1.company_id = T3.id GROUP BY T1.building_id ORDER BY COUNT(*) DESC LIMIT 1
SELECT name FROM buildings WHERE Status = "on-hold" ORDER BY Stories ASC
SELECT name FROM buildings WHERE Status = "on-hold" ORDER BY Stories ASC
SELECT Industry , COUNT(*) FROM Companies GROUP BY Industry
SELECT Industry , COUNT(*) FROM Companies GROUP BY Industry
SELECT Industry FROM Companies GROUP BY Industry ORDER BY COUNT(*) DESC
SELECT Industry FROM Companies GROUP BY Industry ORDER BY COUNT(*) DESC
SELECT Industry FROM Companies GROUP BY Industry ORDER BY COUNT(*) DESC LIMIT 1
SELECT Industry FROM Companies GROUP BY Industry ORDER BY COUNT(*) DESC LIMIT 1
SELECT name FROM buildings WHERE id NOT IN (SELECT building_id FROM Office_locations)
SELECT name FROM buildings WHERE id NOT IN (SELECT building_id FROM Office_locations)
SELECT Industry FROM Companies WHERE Headquarters = "USA" INTERSECT SELECT Industry FROM Companies WHERE Headquarters = "China"
SELECT Industry FROM Companies WHERE Headquarters = "USA" INTERSECT SELECT Industry FROM Companies WHERE Headquarters = "China"
SELECT count(*) FROM Companies WHERE Industry = "Banking" OR Industry = "Conglomerate"
SELECT count(*) FROM Companies WHERE Industry = "Banking" OR Industry = "Conglomerate"
SELECT Headquarters FROM Companies GROUP BY Headquarters HAVING COUNT(*) > 2
SELECT Headquarters FROM Companies GROUP BY Headquarters HAVING COUNT(*) > 2
SELECT count(*) FROM STUDENT
SELECT count(*) FROM STUDENT
SELECT count(*) FROM VOTING_RECORD
SELECT count(*) FROM VOTING_RECORD
SELECT count( President_Vote) FROM VOTING_RECORD
SELECT count( President_Vote) FROM VOTING_RECORD
SELECT max(Age) FROM STUDENT
SELECT max(Age) FROM STUDENT
SELECT LName FROM STUDENT WHERE Major = 50
SELECT LName FROM STUDENT WHERE Major = 50
SELECT Fname FROM STUDENT WHERE Age > 22
SELECT Fname FROM STUDENT WHERE Age > 22
SELECT Major FROM STUDENT WHERE Sex = "M"
SELECT Major FROM STUDENT WHERE Sex = "M"
SELECT avg(Age) FROM STUDENT WHERE Sex = "F"
SELECT avg(Age) FROM STUDENT WHERE Sex = "F"
SELECT max(Age) , min(Age) FROM STUDENT WHERE Major = 600
SELECT max(Age) , min(Age) FROM STUDENT WHERE Major = 600
SELECT Advisor FROM STUDENT WHERE city_code = "BAL"
SELECT Advisor FROM STUDENT WHERE city_code = "BAL"
SELECT Secretary_Vote FROM VOTING_RECORD WHERE ELECTION_CYCLE = "Fall"
SELECT Secretary_Vote FROM VOTING_RECORD WHERE ELECTION_CYCLE = "Fall"
SELECT PRESIDENT_Vote FROM VOTING_RECORD WHERE Registration_Date = "08/30/2015"
SELECT PRESIDENT_Vote FROM VOTING_RECORD WHERE Registration_Date = "08/30/2015"
SELECT Registration_Date , Election_Cycle FROM VOTING_RECORD
SELECT Registration_Date , Election_Cycle FROM VOTING_RECORD
SELECT President_Vote , VICE_President_Vote FROM VOTING_RECORD
SELECT President_Vote , VICE_President_Vote FROM VOTING_RECORD
SELECT T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.CLASS_President_VOTE
SELECT T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.CLASS_President_VOTE
SELECT T1.Fname FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.CLASS_Senator_VOTE
SELECT T1.Fname FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.CLASS_Senator_VOTE
SELECT T1.Age FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.Secretary_Vote WHERE T2.Election_Cycle = "Fall"
SELECT T1.Age FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.Secretary_Vote WHERE T2.Election_Cycle = "Fall"
SELECT T1.Advisor FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.Treasurer_Vote WHERE T2.Election_Cycle = "Spring"
SELECT T1.Advisor FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.Treasurer_Vote WHERE T2.Election_Cycle = "Spring"
SELECT T1.Major FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.Treasurer_Vote
SELECT T1.Major FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.Treasurer_Vote
SELECT T1.Fname , T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.President_VOTE WHERE T1.sex = "F"
SELECT T1.Fname , T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.President_VOTE WHERE T1.sex = "F"
SELECT T1.Fname , T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.VICE_President_VOTE WHERE T1.age = 18
SELECT T1.Fname , T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.VICE_President_VOTE WHERE T1.age = 18
SELECT count(*) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = Class_Senator_Vote WHERE T1.Sex = "M" AND T2.Election_Cycle = "Fall"
SELECT count(*) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = Class_Senator_Vote WHERE T1.Sex = "M" AND T2.Election_Cycle = "Fall"
SELECT count(*) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = Class_Senator_Vote WHERE T1.city_code = "NYC" AND T2.Election_Cycle = "Spring"
SELECT count(*) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = Class_Senator_Vote WHERE T1.city_code = "NYC" AND T2.Election_Cycle = "Spring"
SELECT avg(T1.Age) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = SECRETARY_Vote WHERE T1.city_code = "NYC" AND T2.Election_Cycle = "Spring"
SELECT avg(T1.Age) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = SECRETARY_Vote WHERE T1.city_code = "NYC" AND T2.Election_Cycle = "Spring"
SELECT avg(T1.Age) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = SECRETARY_Vote WHERE T1.Sex = "F" AND T2.Election_Cycle = "Spring"
SELECT avg(T1.Age) FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = SECRETARY_Vote WHERE T1.Sex = "F" AND T2.Election_Cycle = "Spring"
SELECT T1.Fname FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.VICE_PRESIDENT_Vote EXCEPT SELECT Fname FROM STUDENT WHERE city_code = "PIT"
SELECT T1.Fname FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = T2.VICE_PRESIDENT_Vote EXCEPT SELECT Fname FROM STUDENT WHERE city_code = "PIT"
SELECT T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = PRESIDENT_Vote EXCEPT SELECT LName FROM STUDENT WHERE Advisor = "2192"
SELECT T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = PRESIDENT_Vote EXCEPT SELECT LName FROM STUDENT WHERE Advisor = "2192"
SELECT T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = PRESIDENT_Vote INTERSECT SELECT LName FROM STUDENT WHERE Advisor = "8741"
SELECT T1.LName FROM STUDENT AS T1 JOIN VOTING_RECORD AS T2 ON T1.StuID = PRESIDENT_Vote INTERSECT SELECT LName FROM STUDENT WHERE Advisor = "8741"
SELECT Advisor , count(*) FROM STUDENT GROUP BY Advisor
SELECT Advisor , count(*) FROM STUDENT GROUP BY Advisor
SELECT Advisor FROM STUDENT GROUP BY Advisor HAVING COUNT(*) > 2
SELECT Advisor FROM STUDENT GROUP BY Advisor HAVING COUNT(*) > 2
SELECT Major FROM STUDENT GROUP BY Major HAVING COUNT(*) < 3
SELECT Major FROM STUDENT GROUP BY Major HAVING COUNT(*) < 3
SELECT Election_Cycle , count(*) FROM VOTING_RECORD GROUP BY Election_Cycle
SELECT Election_Cycle , count(*) FROM VOTING_RECORD GROUP BY Election_Cycle
SELECT Major FROM STUDENT GROUP BY major ORDER BY count(*) DESC LIMIT 1
SELECT Major FROM STUDENT GROUP BY major ORDER BY count(*) DESC LIMIT 1
SELECT Major FROM STUDENT WHERE Sex = "F" GROUP BY major ORDER BY count(*) DESC LIMIT 1
SELECT Major FROM STUDENT WHERE Sex = "F" GROUP BY major ORDER BY count(*) DESC LIMIT 1
SELECT city_code FROM STUDENT GROUP BY city_code ORDER BY count(*) DESC LIMIT 1
SELECT city_code FROM STUDENT GROUP BY city_code ORDER BY count(*) DESC LIMIT 1
SELECT Advisor FROM STUDENT GROUP BY Advisor HAVING count(*) > 2
SELECT Advisor FROM STUDENT GROUP BY Advisor HAVING count(*) > 2
SELECT count(*) FROM Video_games
SELECT count(*) FROM Video_games
SELECT count( gtype) FROM Video_games
SELECT count( gtype) FROM Video_games
SELECT gtype FROM Video_games
SELECT gtype FROM Video_games
SELECT gname , gtype FROM Video_games ORDER BY gname
SELECT gname , gtype FROM Video_games ORDER BY gname
SELECT gname FROM Video_games WHERE gtype = "Collectible card game"
SELECT gname FROM Video_games WHERE gtype = "Collectible card game"
SELECT gtype FROM Video_games WHERE gname = "Call of Destiny"
SELECT gtype FROM Video_games WHERE gname = "Call of Destiny"
SELECT count(*) FROM Video_games WHERE gtype = "Massively multiplayer online game"
SELECT count(*) FROM Video_games WHERE gtype = "Massively multiplayer online game"
SELECT gtype , count(*) FROM Video_games GROUP BY gtype
SELECT gtype , count(*) FROM Video_games GROUP BY gtype
SELECT gtype FROM Video_games GROUP BY gtype ORDER BY count(*) DESC LIMIT 1
SELECT gtype FROM Video_games GROUP BY gtype ORDER BY count(*) DESC LIMIT 1
SELECT gtype FROM Video_games GROUP BY gtype ORDER BY count(*) LIMIT 1
SELECT gtype FROM Video_games GROUP BY gtype ORDER BY count(*) LIMIT 1
SELECT StuID FROM Student WHERE city_code = "CHI"
SELECT StuID FROM Student WHERE city_code = "CHI"
SELECT StuID FROM Student WHERE Advisor = 1121
SELECT StuID FROM Student WHERE Advisor = 1121
SELECT Fname FROM Student WHERE Major = 600
SELECT Fname FROM Student WHERE Major = 600
SELECT major , avg(age) , min(age) , max(age) FROM Student GROUP BY major
SELECT major , avg(age) , min(age) , max(age) FROM Student GROUP BY major
SELECT advisor FROM Student GROUP BY advisor HAVING count(*) >= 2
SELECT advisor FROM Student GROUP BY advisor HAVING count(*) >= 2
SELECT count( sportname) FROM Sportsinfo
SELECT count( sportname) FROM Sportsinfo
SELECT count( StuID) FROM Sportsinfo
SELECT count( StuID) FROM Sportsinfo
SELECT StuID FROM Sportsinfo WHERE onscholarship = 'Y'
SELECT StuID FROM Sportsinfo WHERE onscholarship = 'Y'
SELECT T2.Lname FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T1.onscholarship = 'Y'
SELECT T2.Lname FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T1.onscholarship = 'Y'
SELECT sum(gamesplayed) FROM Sportsinfo
SELECT sum(gamesplayed) FROM Sportsinfo
SELECT sum(gamesplayed) FROM Sportsinfo WHERE sportname = "Football" AND onscholarship = 'Y'
SELECT sum(gamesplayed) FROM Sportsinfo WHERE sportname = "Football" AND onscholarship = 'Y'
SELECT sportname , count(*) FROM Sportsinfo GROUP BY sportname
SELECT sportname , count(*) FROM Sportsinfo GROUP BY sportname
SELECT StuID , count(*) , sum(gamesplayed) FROM Sportsinfo GROUP BY StuID
SELECT StuID , count(*) , sum(gamesplayed) FROM Sportsinfo GROUP BY StuID
SELECT StuID FROM Sportsinfo GROUP BY StuID HAVING sum(hoursperweek) > 10
SELECT StuID FROM Sportsinfo GROUP BY StuID HAVING sum(hoursperweek) > 10
SELECT T2.Fname , T2.Lname FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID GROUP BY T1.StuID ORDER BY count(*) DESC LIMIT 1
SELECT T2.Fname , T2.Lname FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID GROUP BY T1.StuID ORDER BY count(*) DESC LIMIT 1
SELECT sportname FROM Sportsinfo WHERE onscholarship = 'Y' GROUP BY sportname ORDER BY count(*) DESC LIMIT 1
SELECT sportname FROM Sportsinfo WHERE onscholarship = 'Y' GROUP BY sportname ORDER BY count(*) DESC LIMIT 1
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Sportsinfo
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Sportsinfo
SELECT StuID FROM Student WHERE major = 600 INTERSECT SELECT StuID FROM Sportsinfo WHERE onscholarship = 'Y'
SELECT StuID FROM Student WHERE major = 600 INTERSECT SELECT StuID FROM Sportsinfo WHERE onscholarship = 'Y'
SELECT StuID FROM Student WHERE sex = 'F' INTERSECT SELECT StuID FROM Sportsinfo WHERE sportname = "Football"
SELECT StuID FROM Student WHERE sex = 'F' INTERSECT SELECT StuID FROM Sportsinfo WHERE sportname = "Football"
SELECT StuID FROM Student WHERE sex = 'M' EXCEPT SELECT StuID FROM Sportsinfo WHERE sportname = "Football"
SELECT StuID FROM Student WHERE sex = 'M' EXCEPT SELECT StuID FROM Sportsinfo WHERE sportname = "Football"
SELECT sum(hoursperweek) , sum(gamesplayed) FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T2.Fname = "David" AND T2.Lname = "Shieber"
SELECT sum(hoursperweek) , sum(gamesplayed) FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T2.Fname = "David" AND T2.Lname = "Shieber"
SELECT sum(hoursperweek) , sum(gamesplayed) FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T2.age < 20
SELECT sum(hoursperweek) , sum(gamesplayed) FROM Sportsinfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T2.age < 20
SELECT count( StuID) FROM Plays_games
SELECT count( StuID) FROM Plays_games
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Plays_games
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Plays_games
SELECT StuID FROM Sportsinfo INTERSECT SELECT StuID FROM Plays_games
SELECT StuID FROM Sportsinfo INTERSECT SELECT StuID FROM Plays_games
SELECT gameid , sum(hours_played) FROM Plays_games GROUP BY gameid
SELECT gameid , sum(hours_played) FROM Plays_games GROUP BY gameid
SELECT Stuid , sum(hours_played) FROM Plays_games GROUP BY Stuid
SELECT Stuid , sum(hours_played) FROM Plays_games GROUP BY Stuid
SELECT gname FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.gameid = T2.gameid GROUP BY T1.gameid ORDER BY sum(hours_played) DESC LIMIT 1
SELECT gname FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.gameid = T2.gameid GROUP BY T1.gameid ORDER BY sum(hours_played) DESC LIMIT 1
SELECT gname FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.gameid = T2.gameid GROUP BY T1.gameid HAVING sum(hours_played) >= 1000
SELECT gname FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.gameid = T2.gameid GROUP BY T1.gameid HAVING sum(hours_played) >= 1000
SELECT Gname FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.gameid = T2.gameid JOIN Student AS T3 ON T3.Stuid = T1.Stuid WHERE T3.Lname = "Smith" AND T3.Fname = "Linda"
SELECT Gname FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.gameid = T2.gameid JOIN Student AS T3 ON T3.Stuid = T1.Stuid WHERE T3.Lname = "Smith" AND T3.Fname = "Linda"
SELECT T2.lname , T2.fname FROM SportsInfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T1.SportName = "Football" OR T1.SportName = "Lacrosse"
SELECT T2.lname , T2.fname FROM SportsInfo AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T1.SportName = "Football" OR T1.SportName = "Lacrosse"
SELECT fname , age FROM Student WHERE StuID IN (SELECT StuID FROM Sportsinfo WHERE SportName = "Football" INTERSECT SELECT StuID FROM Sportsinfo WHERE SportName = "Lacrosse")
SELECT fname , age FROM Student WHERE StuID IN (SELECT StuID FROM Sportsinfo WHERE SportName = "Football" INTERSECT SELECT StuID FROM Sportsinfo WHERE SportName = "Lacrosse")
SELECT lname , sex FROM Student WHERE StuID IN (SELECT T1.StuID FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.GameID = T2.GameID WHERE T2.Gname = "Call of Destiny" INTERSECT SELECT T1.StuID FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.GameID = T2.GameID WHERE T2.Gname = "Works of Widenius")
SELECT lname , sex FROM Student WHERE StuID IN (SELECT T1.StuID FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.GameID = T2.GameID WHERE T2.Gname = "Call of Destiny" INTERSECT SELECT T1.StuID FROM Plays_games AS T1 JOIN Video_games AS T2 ON T1.GameID = T2.GameID WHERE T2.Gname = "Works of Widenius")
SELECT host_city FROM hosting_city ORDER BY YEAR DESC LIMIT 1
SELECT host_city FROM hosting_city ORDER BY YEAR DESC LIMIT 1
SELECT match_id FROM MATCH WHERE competition = "1994 FIFA World Cup qualification"
SELECT match_id FROM MATCH WHERE competition = "1994 FIFA World Cup qualification"
SELECT T1.city FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city WHERE T2.year > 2010
SELECT T1.city FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city WHERE T2.year > 2010
SELECT T1.city FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city GROUP BY T2.host_city ORDER BY count(*) DESC LIMIT 1
SELECT T1.city FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city GROUP BY T2.host_city ORDER BY count(*) DESC LIMIT 1
SELECT T3.venue FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city JOIN MATCH AS T3 ON T2.match_id = T3.match_id WHERE T1.city = "Nanjing ( Jiangsu )" AND T3.competition = "1994 FIFA World Cup qualification"
SELECT T3.venue FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city JOIN MATCH AS T3 ON T2.match_id = T3.match_id WHERE T1.city = "Nanjing ( Jiangsu )" AND T3.competition = "1994 FIFA World Cup qualification"
SELECT T2.Jan FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T1.city = "Shanghai"
SELECT T2.Jan FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T1.city = "Shanghai"
SELECT T2.year FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city WHERE T1.city = "Taizhou ( Zhejiang )"
SELECT T2.year FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city WHERE T1.city = "Taizhou ( Zhejiang )"
SELECT city FROM city ORDER BY regional_population DESC LIMIT 3
SELECT city FROM city ORDER BY regional_population DESC LIMIT 3
SELECT city , GDP FROM city ORDER BY GDP LIMIT 1
SELECT city , GDP FROM city ORDER BY GDP LIMIT 1
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id ORDER BY T2.Feb DESC LIMIT 1
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id ORDER BY T2.Feb DESC LIMIT 1
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Mar < T2.Jul OR T2.Mar > T2.Oct
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Mar < T2.Jul OR T2.Mar > T2.Oct
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Mar < T2.Jul INTERSECT SELECT T3.city FROM city AS T3 JOIN hosting_city AS T4 ON T3.city_id = T4.host_city
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Mar < T2.Jul INTERSECT SELECT T3.city FROM city AS T3 JOIN hosting_city AS T4 ON T3.city_id = T4.host_city
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Mar < T2.Dec EXCEPT SELECT T3.city FROM city AS T3 JOIN hosting_city AS T4 ON T3.city_id = T4.host_city
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Mar < T2.Dec EXCEPT SELECT T3.city FROM city AS T3 JOIN hosting_city AS T4 ON T3.city_id = T4.host_city
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Feb > T2.Jun UNION SELECT T3.city FROM city AS T3 JOIN hosting_city AS T4 ON T3.city_id = T4.host_city
SELECT T1.city FROM city AS T1 JOIN temperature AS T2 ON T1.city_id = T2.city_id WHERE T2.Feb > T2.Jun UNION SELECT T3.city FROM city AS T3 JOIN hosting_city AS T4 ON T3.city_id = T4.host_city
SELECT city FROM city WHERE regional_population > 10000000
SELECT city FROM city WHERE regional_population > 10000000
SELECT city FROM city WHERE regional_population > 10000000 UNION SELECT city FROM city WHERE regional_population < 5000000
SELECT city FROM city WHERE regional_population > 10000000 UNION SELECT city FROM city WHERE regional_population < 5000000
SELECT count(*) , Competition FROM MATCH GROUP BY Competition
SELECT count(*) , Competition FROM MATCH GROUP BY Competition
SELECT venue FROM MATCH ORDER BY date DESC
SELECT venue FROM MATCH ORDER BY date DESC
SELECT gdp FROM city ORDER BY Regional_Population DESC LIMIT 1
SELECT gdp FROM city ORDER BY Regional_Population DESC LIMIT 1
SELECT t1.gdp , t1.Regional_Population FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city GROUP BY t2.Host_City HAVING count(*) > 1
SELECT t1.gdp , t1.Regional_Population FROM city AS T1 JOIN hosting_city AS T2 ON T1.city_id = T2.host_city GROUP BY t2.Host_City HAVING count(*) > 1
SELECT individual_first_name , individual_middle_name , individual_last_name FROM individuals ORDER BY individual_last_name
SELECT individual_first_name , individual_middle_name , individual_last_name FROM individuals ORDER BY individual_last_name
SELECT form_type_code FROM forms
SELECT form_type_code FROM forms
SELECT t1.form_name FROM forms AS t1 JOIN party_forms AS t2 ON t1.form_id = t2.form_id GROUP BY t2.form_id ORDER BY count(*) DESC LIMIT 1
SELECT t1.form_name FROM forms AS t1 JOIN party_forms AS t2 ON t1.form_id = t2.form_id GROUP BY t2.form_id ORDER BY count(*) DESC LIMIT 1
SELECT payment_method_code , party_phone FROM parties WHERE party_email = "enrico09@example.com"
SELECT payment_method_code , party_phone FROM parties WHERE party_email = "enrico09@example.com"
SELECT t1.party_email FROM parties AS t1 JOIN party_forms AS t2 ON t1.party_id = t2.party_id WHERE t2.form_id = (SELECT form_id FROM party_forms GROUP BY form_id ORDER BY count(*) DESC LIMIT 1)
SELECT t1.party_email FROM parties AS t1 JOIN party_forms AS t2 ON t1.party_id = t2.party_id WHERE t2.form_id = (SELECT form_id FROM party_forms GROUP BY form_id ORDER BY count(*) DESC LIMIT 1)
SELECT organization_name FROM organizations ORDER BY date_formed ASC
SELECT organization_name FROM organizations ORDER BY date_formed ASC
SELECT organization_name FROM organizations ORDER BY date_formed DESC LIMIT 1
SELECT organization_name FROM organizations ORDER BY date_formed DESC LIMIT 1
SELECT t3.individual_last_name FROM organizations AS t1 JOIN organization_contact_individuals AS t2 ON t1.organization_id = t2.organization_id JOIN individuals AS t3 ON t2.individual_id = t3.individual_id WHERE t1.organization_name = "Labour Party" ORDER BY t2.date_contact_to DESC LIMIT 1
SELECT t3.individual_last_name FROM organizations AS t1 JOIN organization_contact_individuals AS t2 ON t1.organization_id = t2.organization_id JOIN individuals AS t3 ON t2.individual_id = t3.individual_id WHERE t1.organization_name = "Labour Party" ORDER BY t2.date_contact_to DESC LIMIT 1
SELECT t3.individual_last_name FROM organizations AS t1 JOIN organization_contact_individuals AS t2 ON t1.organization_id = t2.organization_id JOIN individuals AS t3 ON t2.individual_id = t3.individual_id WHERE t1.uk_vat_number = (SELECT max(uk_vat_number) FROM organizations) ORDER BY t2.date_contact_to ASC LIMIT 1
SELECT t3.individual_last_name FROM organizations AS t1 JOIN organization_contact_individuals AS t2 ON t1.organization_id = t2.organization_id JOIN individuals AS t3 ON t2.individual_id = t3.individual_id WHERE t1.uk_vat_number = (SELECT max(uk_vat_number) FROM organizations) ORDER BY t2.date_contact_to ASC LIMIT 1
SELECT count(*) FROM services
SELECT count(*) FROM services
SELECT service_name FROM services EXCEPT SELECT t1.service_name FROM services AS t1 JOIN party_services AS t2 ON t1.service_id = t2.service_id
SELECT service_name FROM services EXCEPT SELECT t1.service_name FROM services AS t1 JOIN party_services AS t2 ON t1.service_id = t2.service_id
SELECT town_city FROM addresses UNION SELECT state_province_county FROM addresses
SELECT town_city FROM addresses UNION SELECT state_province_county FROM addresses
SELECT count(*) FROM addresses WHERE state_province_county = "Colorado"
SELECT count(*) FROM addresses WHERE state_province_county = "Colorado"
SELECT payment_method_code FROM parties GROUP BY payment_method_code HAVING count(*) > 3
SELECT payment_method_code FROM parties GROUP BY payment_method_code HAVING count(*) > 3
SELECT organization_name FROM organizations WHERE organization_name LIKE "%Party%"
SELECT organization_name FROM organizations WHERE organization_name LIKE "%Party%"
SELECT count( payment_method_code) FROM parties
SELECT count( payment_method_code) FROM parties
SELECT t1.party_email FROM parties AS t1 JOIN party_services AS t2 ON t1.party_id = t2.customer_id GROUP BY t1.party_email ORDER BY count(*) DESC LIMIT 1
SELECT t1.party_email FROM parties AS t1 JOIN party_services AS t2 ON t1.party_id = t2.customer_id GROUP BY t1.party_email ORDER BY count(*) DESC LIMIT 1
SELECT state_province_county FROM addresses WHERE line_1_number_building LIKE "%6862 Kaitlyn Knolls%"
SELECT state_province_county FROM addresses WHERE line_1_number_building LIKE "%6862 Kaitlyn Knolls%"
SELECT t1.organization_name FROM organizations AS t1 JOIN organization_contact_individuals AS t2 ON t1.organization_id = t2.organization_id GROUP BY t1.organization_name ORDER BY count(*) DESC LIMIT 1
SELECT t1.organization_name FROM organizations AS t1 JOIN organization_contact_individuals AS t2 ON t1.organization_id = t2.organization_id GROUP BY t1.organization_name ORDER BY count(*) DESC LIMIT 1
SELECT t1.individual_last_name FROM individuals AS t1 JOIN organization_contact_individuals AS t2 ON t1.individual_id = t2.individual_id
SELECT t1.individual_last_name FROM individuals AS t1 JOIN organization_contact_individuals AS t2 ON t1.individual_id = t2.individual_id
SELECT count(*) FROM station
SELECT name , LOCATION , number_of_platforms FROM station
SELECT LOCATION FROM station
SELECT name , total_passengers FROM station WHERE LOCATION != 'London'
SELECT name , main_services FROM station ORDER BY total_passengers DESC LIMIT 3
SELECT avg(total_passengers) , max(total_passengers) FROM station WHERE LOCATION = 'London' OR LOCATION = 'Glasgow'
SELECT LOCATION , sum(number_of_platforms) , sum(total_passengers) FROM station GROUP BY LOCATION
SELECT LOCATION FROM station WHERE number_of_platforms >= 15 AND total_passengers > 25
SELECT LOCATION FROM station EXCEPT SELECT LOCATION FROM station WHERE number_of_platforms >= 15
SELECT LOCATION FROM station GROUP BY LOCATION ORDER BY count(*) DESC LIMIT 1
SELECT name , TIME , service FROM train
SELECT count(*) FROM train
SELECT name , service FROM train ORDER BY TIME
SELECT T2.name , count(*) FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id GROUP BY T1.station_id
SELECT T2.name , T3.name FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id JOIN train AS T3 ON T3.train_id = T1.train_id
SELECT T3.name , T3.time FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id JOIN train AS T3 ON T3.train_id = T1.train_id WHERE T2.location = 'London' ORDER BY T3.time DESC
SELECT T2.name FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id GROUP BY T1.station_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.name FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id GROUP BY T1.station_id HAVING count(*) >= 2
SELECT LOCATION FROM station GROUP BY LOCATION HAVING count(*) = 1
SELECT name FROM station WHERE station_id NOT IN (SELECT station_id FROM train_station)
SELECT T2.name FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id JOIN train AS T3 ON T3.train_id = T1.train_id WHERE T3.Name = "Ananthapuri Express" INTERSECT SELECT T2.name FROM train_station AS T1 JOIN station AS T2 ON T1.station_id = T2.station_id JOIN train AS T3 ON T3.train_id = T1.train_id WHERE T3.Name = "Guruvayur Express"
SELECT T2.name FROM train_station AS T1 JOIN train AS T2 ON T1.train_id = T2.train_id WHERE T1.station_id NOT IN (SELECT T4.station_id FROM train_station AS T3 JOIN station AS T4 ON T3.station_id = T4.station_id WHERE t4.location = "London")
SELECT name , LOCATION FROM station ORDER BY Annual_entry_exit , Annual_interchanges
SELECT count(*) FROM visitor WHERE age < 30
SELECT name FROM visitor WHERE Level_of_membership > 4 ORDER BY Level_of_membership DESC
SELECT avg(age) FROM visitor WHERE Level_of_membership <= 4
SELECT name , Level_of_membership FROM visitor WHERE Level_of_membership > 4 ORDER BY age DESC
SELECT museum_id , name FROM museum ORDER BY num_of_staff DESC LIMIT 1
SELECT avg(num_of_staff) FROM museum WHERE open_year < 2009
SELECT Num_of_Staff , Open_Year FROM museum WHERE name = 'Plaza Museum'
SELECT name FROM museum WHERE num_of_staff > (SELECT min(num_of_staff) FROM museum WHERE open_year > 2010)
SELECT t1.id , t1.name , t1.age FROM visitor AS t1 JOIN visit AS t2 ON t1.id = t2.visitor_id GROUP BY t1.id HAVING count(*) > 1
SELECT t2.visitor_id , t1.name , t1.Level_of_membership FROM visitor AS t1 JOIN visit AS t2 ON t1.id = t2.visitor_id GROUP BY t2.visitor_id ORDER BY sum(t2.Total_spent) DESC LIMIT 1
SELECT t2.Museum_ID , t1.name FROM museum AS t1 JOIN visit AS t2 ON t1.Museum_ID = t2.Museum_ID GROUP BY t2.Museum_ID ORDER BY count(*) DESC LIMIT 1
SELECT name FROM museum WHERE Museum_ID NOT IN (SELECT museum_id FROM visit)
SELECT t1.name , t1.age FROM visitor AS t1 JOIN visit AS t2 ON t1.id = t2.visitor_id ORDER BY t2.num_of_ticket DESC LIMIT 1
SELECT avg(num_of_ticket) , max(num_of_ticket) FROM visit
SELECT sum(t2.Total_spent) FROM visitor AS t1 JOIN visit AS t2 ON t1.id = t2.visitor_id WHERE t1.Level_of_membership = 1
SELECT t1.name FROM visitor AS t1 JOIN visit AS t2 ON t1.id = t2.visitor_id JOIN museum AS t3 ON t3.Museum_ID = t2.Museum_ID WHERE t3.open_year < 2009 INTERSECT SELECT t1.name FROM visitor AS t1 JOIN visit AS t2 ON t1.id = t2.visitor_id JOIN museum AS t3 ON t3.Museum_ID = t2.Museum_ID WHERE t3.open_year > 2011
SELECT count(*) FROM visitor WHERE id NOT IN (SELECT t2.visitor_id FROM museum AS t1 JOIN visit AS t2 ON t1.Museum_ID = t2.Museum_ID WHERE t1.open_year > 2010)
SELECT count(*) FROM museum WHERE open_year > 2013 OR open_year < 2008
SELECT Title FROM Cartoon ORDER BY title
SELECT Title FROM Cartoon ORDER BY title
SELECT Title FROM Cartoon WHERE Directed_by = "Ben Jones";
SELECT Title FROM Cartoon WHERE Directed_by = "Ben Jones";
SELECT count(*) FROM Cartoon WHERE Written_by = "Joseph Kuhr";
SELECT count(*) FROM Cartoon WHERE Written_by = "Joseph Kuhr";
SELECT title , Directed_by FROM Cartoon ORDER BY Original_air_date
SELECT title , Directed_by FROM Cartoon ORDER BY Original_air_date
SELECT Title FROM Cartoon WHERE Directed_by = "Ben Jones" OR Directed_by = "Brandon Vietti";
SELECT Title FROM Cartoon WHERE Directed_by = "Ben Jones" OR Directed_by = "Brandon Vietti";
SELECT Country , count(*) FROM TV_Channel GROUP BY Country ORDER BY count(*) DESC LIMIT 1;
SELECT Country , count(*) FROM TV_Channel GROUP BY Country ORDER BY count(*) DESC LIMIT 1;
SELECT count( series_name) , count( content) FROM TV_Channel;
SELECT count( series_name) , count( content) FROM TV_Channel;
SELECT Content FROM TV_Channel WHERE series_name = "Sky Radio";
SELECT Content FROM TV_Channel WHERE series_name = "Sky Radio";
SELECT Package_Option FROM TV_Channel WHERE series_name = "Sky Radio";
SELECT Package_Option FROM TV_Channel WHERE series_name = "Sky Radio";
SELECT count(*) FROM TV_Channel WHERE LANGUAGE = "English";
SELECT count(*) FROM TV_Channel WHERE LANGUAGE = "English";
SELECT LANGUAGE , count(*) FROM TV_Channel GROUP BY LANGUAGE ORDER BY count(*) ASC LIMIT 1;
SELECT LANGUAGE , count(*) FROM TV_Channel GROUP BY LANGUAGE ORDER BY count(*) ASC LIMIT 1;
SELECT LANGUAGE , count(*) FROM TV_Channel GROUP BY LANGUAGE
SELECT LANGUAGE , count(*) FROM TV_Channel GROUP BY LANGUAGE
SELECT T1.series_name FROM TV_Channel AS T1 JOIN Cartoon AS T2 ON T1.id = T2.Channel WHERE T2.Title = "The Rise of the Blue Beetle!";
SELECT T1.series_name FROM TV_Channel AS T1 JOIN Cartoon AS T2 ON T1.id = T2.Channel WHERE T2.Title = "The Rise of the Blue Beetle!";
SELECT T2.Title FROM TV_Channel AS T1 JOIN Cartoon AS T2 ON T1.id = T2.Channel WHERE T1.series_name = "Sky Radio";
SELECT T2.Title FROM TV_Channel AS T1 JOIN Cartoon AS T2 ON T1.id = T2.Channel WHERE T1.series_name = "Sky Radio";
SELECT Episode FROM TV_series ORDER BY rating
SELECT Episode FROM TV_series ORDER BY rating
SELECT Episode , Rating FROM TV_series ORDER BY Rating DESC LIMIT 3;
SELECT Episode , Rating FROM TV_series ORDER BY Rating DESC LIMIT 3;
SELECT max(SHARE) , min(SHARE) FROM TV_series;
SELECT max(SHARE) , min(SHARE) FROM TV_series;
SELECT Air_Date FROM TV_series WHERE Episode = "A Love of a Lifetime";
SELECT Air_Date FROM TV_series WHERE Episode = "A Love of a Lifetime";
SELECT Weekly_Rank FROM TV_series WHERE Episode = "A Love of a Lifetime";
SELECT Weekly_Rank FROM TV_series WHERE Episode = "A Love of a Lifetime";
SELECT T1.series_name FROM TV_Channel AS T1 JOIN TV_series AS T2 ON T1.id = T2.Channel WHERE T2.Episode = "A Love of a Lifetime";
SELECT T1.series_name FROM TV_Channel AS T1 JOIN TV_series AS T2 ON T1.id = T2.Channel WHERE T2.Episode = "A Love of a Lifetime";
SELECT T2.Episode FROM TV_Channel AS T1 JOIN TV_series AS T2 ON T1.id = T2.Channel WHERE T1.series_name = "Sky Radio";
SELECT T2.Episode FROM TV_Channel AS T1 JOIN TV_series AS T2 ON T1.id = T2.Channel WHERE T1.series_name = "Sky Radio";
SELECT count(*) , Directed_by FROM cartoon GROUP BY Directed_by
SELECT count(*) , Directed_by FROM cartoon GROUP BY Directed_by
SELECT production_code , channel FROM cartoon ORDER BY original_air_date LIMIT 1
SELECT production_code , channel FROM cartoon ORDER BY original_air_date LIMIT 1
SELECT package_option , series_name FROM TV_Channel WHERE hight_definition_TV = "yes"
SELECT package_option , series_name FROM TV_Channel WHERE hight_definition_TV = "yes"
SELECT T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.written_by = 'Todd Casey'
SELECT T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.written_by = 'Todd Casey'
SELECT country FROM TV_Channel EXCEPT SELECT T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.written_by = 'Todd Casey'
SELECT country FROM TV_Channel EXCEPT SELECT T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.written_by = 'Todd Casey'
SELECT T1.series_name , T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.directed_by = 'Michael Chang' INTERSECT SELECT T1.series_name , T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.directed_by = 'Ben Jones'
SELECT T1.series_name , T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.directed_by = 'Michael Chang' INTERSECT SELECT T1.series_name , T1.country FROM TV_Channel AS T1 JOIN cartoon AS T2 ON T1.id = T2.Channel WHERE T2.directed_by = 'Ben Jones'
SELECT Pixel_aspect_ratio_PAR , country FROM tv_channel WHERE LANGUAGE != 'English'
SELECT Pixel_aspect_ratio_PAR , country FROM tv_channel WHERE LANGUAGE != 'English'
SELECT id FROM tv_channel GROUP BY country HAVING count(*) > 2
SELECT id FROM tv_channel GROUP BY country HAVING count(*) > 2
SELECT id FROM TV_Channel EXCEPT SELECT channel FROM cartoon WHERE directed_by = 'Ben Jones'
SELECT id FROM TV_Channel EXCEPT SELECT channel FROM cartoon WHERE directed_by = 'Ben Jones'
SELECT package_option FROM TV_Channel WHERE id NOT IN (SELECT channel FROM cartoon WHERE directed_by = 'Ben Jones')
SELECT package_option FROM TV_Channel WHERE id NOT IN (SELECT channel FROM cartoon WHERE directed_by = 'Ben Jones')
SELECT Name FROM country WHERE IndepYear > 1950
SELECT Name FROM country WHERE IndepYear > 1950
SELECT count(*) FROM country WHERE GovernmentForm = "Republic"
SELECT count(*) FROM country WHERE GovernmentForm = "Republic"
SELECT sum(SurfaceArea) FROM country WHERE Region = "Caribbean"
SELECT sum(SurfaceArea) FROM country WHERE Region = "Caribbean"
SELECT Continent FROM country WHERE Name = "Anguilla"
SELECT Continent FROM country WHERE Name = "Anguilla"
SELECT Region FROM country AS T1 JOIN city AS T2 ON T1.Code = T2.CountryCode WHERE T2.Name = "Kabul"
SELECT Region FROM country AS T1 JOIN city AS T2 ON T1.Code = T2.CountryCode WHERE T2.Name = "Kabul"
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Name = "Aruba" ORDER BY Percentage DESC LIMIT 1
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Name = "Aruba" ORDER BY Percentage DESC LIMIT 1
SELECT Population , LifeExpectancy FROM country WHERE Name = "Brazil"
SELECT Population , LifeExpectancy FROM country WHERE Name = "Brazil"
SELECT Population , Region FROM country WHERE Name = "Angola"
SELECT Population , Region FROM country WHERE Name = "Angola"
SELECT avg(LifeExpectancy) FROM country WHERE Region = "Central Africa"
SELECT avg(LifeExpectancy) FROM country WHERE Region = "Central Africa"
SELECT Name FROM country WHERE Continent = "Asia" ORDER BY LifeExpectancy LIMIT 1
SELECT Name FROM country WHERE Continent = "Asia" ORDER BY LifeExpectancy LIMIT 1
SELECT sum(Population) , max(GNP) FROM country WHERE Continent = "Asia"
SELECT sum(Population) , max(GNP) FROM country WHERE Continent = "Asia"
SELECT avg(LifeExpectancy) FROM country WHERE Continent = "Africa" AND GovernmentForm = "Republic"
SELECT avg(LifeExpectancy) FROM country WHERE Continent = "Africa" AND GovernmentForm = "Republic"
SELECT sum(SurfaceArea) FROM country WHERE Continent = "Asia" OR Continent = "Europe"
SELECT sum(SurfaceArea) FROM country WHERE Continent = "Asia" OR Continent = "Europe"
SELECT sum(Population) FROM city WHERE District = "Gelderland"
SELECT sum(Population) FROM city WHERE District = "Gelderland"
SELECT avg(GNP) , sum(population) FROM country WHERE GovernmentForm = "US Territory"
SELECT avg(GNP) , sum(population) FROM country WHERE GovernmentForm = "US Territory"
SELECT count( LANGUAGE) FROM countrylanguage
SELECT count( LANGUAGE) FROM countrylanguage
SELECT count( GovernmentForm) FROM country WHERE Continent = "Africa"
SELECT count( GovernmentForm) FROM country WHERE Continent = "Africa"
SELECT COUNT(T2.Language) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Name = "Aruba"
SELECT COUNT(T2.Language) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Name = "Aruba"
SELECT COUNT(*) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Name = "Afghanistan" AND IsOfficial = "T"
SELECT COUNT(*) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Name = "Afghanistan" AND IsOfficial = "T"
SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode GROUP BY T1.Name ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode GROUP BY T1.Name ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Continent FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode GROUP BY T1.Continent ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Continent FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode GROUP BY T1.Continent ORDER BY COUNT(*) DESC LIMIT 1
SELECT COUNT(*) FROM (SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" INTERSECT SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "Dutch")
SELECT COUNT(*) FROM (SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" INTERSECT SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "Dutch")
SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" INTERSECT SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "French"
SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" INTERSECT SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "French"
SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" AND T2.IsOfficial = "T" INTERSECT SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "French" AND T2.IsOfficial = "T"
SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" AND T2.IsOfficial = "T" INTERSECT SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "French" AND T2.IsOfficial = "T"
SELECT COUNT( Continent) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "Chinese"
SELECT COUNT( Continent) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "Chinese"
SELECT T1.Region FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" OR T2.Language = "Dutch"
SELECT T1.Region FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" OR T2.Language = "Dutch"
SELECT * FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" AND IsOfficial = "T" UNION SELECT * FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "Dutch" AND IsOfficial = "T"
SELECT * FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" AND IsOfficial = "T" UNION SELECT * FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "Dutch" AND IsOfficial = "T"
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Continent = "Asia" GROUP BY T2.Language ORDER BY COUNT (*) DESC LIMIT 1
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.Continent = "Asia" GROUP BY T2.Language ORDER BY COUNT (*) DESC LIMIT 1
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.GovernmentForm = "Republic" GROUP BY T2.Language HAVING COUNT(*) = 1
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.GovernmentForm = "Republic" GROUP BY T2.Language HAVING COUNT(*) = 1
SELECT T1.Name , T1.Population FROM city AS T1 JOIN countrylanguage AS T2 ON T1.CountryCode = T2.CountryCode WHERE T2.Language = "English" ORDER BY T1.Population DESC LIMIT 1
SELECT T1.Name , T1.Population FROM city AS T1 JOIN countrylanguage AS T2 ON T1.CountryCode = T2.CountryCode WHERE T2.Language = "English" ORDER BY T1.Population DESC LIMIT 1
SELECT Name , Population , LifeExpectancy FROM country WHERE Continent = "Asia" ORDER BY SurfaceArea DESC LIMIT 1
SELECT Name , Population , LifeExpectancy FROM country WHERE Continent = "Asia" ORDER BY SurfaceArea DESC LIMIT 1
SELECT avg(LifeExpectancy) FROM country WHERE Name NOT IN (SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" AND T2.IsOfficial = "T")
SELECT avg(LifeExpectancy) FROM country WHERE Name NOT IN (SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English" AND T2.IsOfficial = "T")
SELECT sum(Population) FROM country WHERE Name NOT IN (SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English")
SELECT sum(Population) FROM country WHERE Name NOT IN (SELECT T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T2.Language = "English")
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.HeadOfState = "Beatrix" AND T2.IsOfficial = "T"
SELECT T2.Language FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE T1.HeadOfState = "Beatrix" AND T2.IsOfficial = "T"
SELECT count( T2.Language) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE IndepYear < 1930 AND T2.IsOfficial = "T"
SELECT count( T2.Language) FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode WHERE IndepYear < 1930 AND T2.IsOfficial = "T"
SELECT Name FROM country WHERE SurfaceArea > (SELECT min(SurfaceArea) FROM country WHERE Continent = "Europe")
SELECT Name FROM country WHERE SurfaceArea > (SELECT min(SurfaceArea) FROM country WHERE Continent = "Europe")
SELECT Name FROM country WHERE Continent = "Africa" AND population < (SELECT max(population) FROM country WHERE Continent = "Asia")
SELECT Name FROM country WHERE Continent = "Africa" AND population < (SELECT min(population) FROM country WHERE Continent = "Asia")
SELECT Name FROM country WHERE Continent = "Asia" AND population > (SELECT max(population) FROM country WHERE Continent = "Africa")
SELECT Name FROM country WHERE Continent = "Asia" AND population > (SELECT min(population) FROM country WHERE Continent = "Africa")
SELECT CountryCode FROM countrylanguage EXCEPT SELECT CountryCode FROM countrylanguage WHERE LANGUAGE = "English"
SELECT CountryCode FROM countrylanguage EXCEPT SELECT CountryCode FROM countrylanguage WHERE LANGUAGE = "English"
SELECT CountryCode FROM countrylanguage WHERE LANGUAGE != "English"
SELECT CountryCode FROM countrylanguage WHERE LANGUAGE != "English"
SELECT Code FROM country WHERE GovernmentForm != "Republic" EXCEPT SELECT CountryCode FROM countrylanguage WHERE LANGUAGE = "English"
SELECT Code FROM country WHERE GovernmentForm != "Republic" EXCEPT SELECT CountryCode FROM countrylanguage WHERE LANGUAGE = "English"
SELECT T2.Name FROM country AS T1 JOIN city AS T2 ON T2.CountryCode = T1.Code WHERE T1.Continent = 'Europe' AND T1.Name NOT IN (SELECT T3.Name FROM country AS T3 JOIN countrylanguage AS T4 ON T3.Code = T4.CountryCode WHERE T4.IsOfficial = 'T' AND T4.Language = 'English')
SELECT T2.Name FROM country AS T1 JOIN city AS T2 ON T2.CountryCode = T1.Code WHERE T1.Continent = 'Europe' AND T1.Name NOT IN (SELECT T3.Name FROM country AS T3 JOIN countrylanguage AS T4 ON T3.Code = T4.CountryCode WHERE T4.IsOfficial = 'T' AND T4.Language = 'English')
SELECT T3.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode JOIN city AS T3 ON T1.Code = T3.CountryCode WHERE T2.IsOfficial = 'T' AND T2.Language = 'Chinese' AND T1.Continent = "Asia"
SELECT T3.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode JOIN city AS T3 ON T1.Code = T3.CountryCode WHERE T2.IsOfficial = 'T' AND T2.Language = 'Chinese' AND T1.Continent = "Asia"
SELECT Name , SurfaceArea , IndepYear FROM country ORDER BY Population LIMIT 1
SELECT Name , SurfaceArea , IndepYear FROM country ORDER BY Population LIMIT 1
SELECT Name , population , HeadOfState FROM country ORDER BY SurfaceArea DESC LIMIT 1
SELECT Name , population , HeadOfState FROM country ORDER BY SurfaceArea DESC LIMIT 1
SELECT COUNT(T2.Language) , T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode GROUP BY T1.Name HAVING COUNT(*) > 2
SELECT COUNT(T2.Language) , T1.Name FROM country AS T1 JOIN countrylanguage AS T2 ON T1.Code = T2.CountryCode GROUP BY T1.Name HAVING COUNT(*) > 2
SELECT count(*) , District FROM city WHERE Population > (SELECT avg(Population) FROM city) GROUP BY District
SELECT count(*) , District FROM city WHERE Population > (SELECT avg(Population) FROM city) GROUP BY District
SELECT sum(Population) , GovernmentForm FROM country GROUP BY GovernmentForm HAVING avg(LifeExpectancy) > 72
SELECT sum(Population) , GovernmentForm FROM country GROUP BY GovernmentForm HAVING avg(LifeExpectancy) > 72
SELECT sum(Population) , avg(LifeExpectancy) , Continent FROM country GROUP BY Continent HAVING avg(LifeExpectancy) < 72
SELECT sum(Population) , avg(LifeExpectancy) , Continent FROM country GROUP BY Continent HAVING avg(LifeExpectancy) < 72
SELECT Name , SurfaceArea FROM country ORDER BY SurfaceArea DESC LIMIT 5
SELECT Name , SurfaceArea FROM country ORDER BY SurfaceArea DESC LIMIT 5
SELECT Name FROM country ORDER BY Population DESC LIMIT 3
SELECT Name FROM country ORDER BY Population DESC LIMIT 3
SELECT Name FROM country ORDER BY Population ASC LIMIT 3
SELECT Name FROM country ORDER BY Population ASC LIMIT 3
SELECT count(*) FROM country WHERE continent = "Asia"
SELECT count(*) FROM country WHERE continent = "Asia"
SELECT Name FROM country WHERE continent = "Europe" AND Population = "80000"
SELECT Name FROM country WHERE continent = "Europe" AND Population = "80000"
SELECT sum(Population) , avg(SurfaceArea) FROM country WHERE Continent = "North America" AND SurfaceArea > 3000
SELECT sum(Population) , avg(SurfaceArea) FROM country WHERE Continent = "North America" AND SurfaceArea > 3000
SELECT name FROM city WHERE Population BETWEEN 160000 AND 900000
SELECT name FROM city WHERE Population BETWEEN 160000 AND 90000
SELECT LANGUAGE FROM countrylanguage GROUP BY LANGUAGE ORDER BY count(*) DESC LIMIT 1
SELECT LANGUAGE FROM countrylanguage GROUP BY LANGUAGE ORDER BY count(*) DESC LIMIT 1
SELECT LANGUAGE , CountryCode , max(Percentage) FROM countrylanguage GROUP BY CountryCode
SELECT LANGUAGE , CountryCode , max(Percentage) FROM countrylanguage GROUP BY CountryCode
SELECT count(*) , max(Percentage) FROM countrylanguage WHERE LANGUAGE = "Spanish" GROUP BY CountryCode
SELECT count(*) , max(Percentage) FROM countrylanguage WHERE LANGUAGE = "Spanish" GROUP BY CountryCode
SELECT CountryCode , max(Percentage) FROM countrylanguage WHERE LANGUAGE = "Spanish" GROUP BY CountryCode
SELECT CountryCode , max(Percentage) FROM countrylanguage WHERE LANGUAGE = "Spanish" GROUP BY CountryCode
SELECT count(*) FROM Highschooler
SELECT count(*) FROM Highschooler
SELECT name , grade FROM Highschooler
SELECT name , grade FROM Highschooler
SELECT grade FROM Highschooler
SELECT grade FROM Highschooler
SELECT grade FROM Highschooler WHERE name = "Kyle"
SELECT grade FROM Highschooler WHERE name = "Kyle"
SELECT name FROM Highschooler WHERE grade = 10
SELECT name FROM Highschooler WHERE grade = 10
SELECT ID FROM Highschooler WHERE name = "Kyle"
SELECT ID FROM Highschooler WHERE name = "Kyle"
SELECT count(*) FROM Highschooler WHERE grade = 9 OR grade = 10
SELECT count(*) FROM Highschooler WHERE grade = 9 OR grade = 10
SELECT grade , count(*) FROM Highschooler GROUP BY grade
SELECT grade , count(*) FROM Highschooler GROUP BY grade
SELECT grade FROM Highschooler GROUP BY grade ORDER BY count(*) DESC LIMIT 1
SELECT grade FROM Highschooler GROUP BY grade ORDER BY count(*) DESC LIMIT 1
SELECT grade FROM Highschooler GROUP BY grade HAVING count(*) >= 4
SELECT grade FROM Highschooler GROUP BY grade HAVING count(*) >= 4
SELECT student_id , count(*) FROM Friend GROUP BY student_id
SELECT student_id , count(*) FROM Friend GROUP BY student_id
SELECT T2.name , count(*) FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id
SELECT T2.name , count(*) FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id HAVING count(*) >= 3
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id HAVING count(*) >= 3
SELECT T3.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id JOIN Highschooler AS T3 ON T1.friend_id = T3.id WHERE T2.name = "Kyle"
SELECT T3.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id JOIN Highschooler AS T3 ON T1.friend_id = T3.id WHERE T2.name = "Kyle"
SELECT count(*) FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id WHERE T2.name = "Kyle"
SELECT count(*) FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id WHERE T2.name = "Kyle"
SELECT id FROM Highschooler EXCEPT SELECT student_id FROM Friend
SELECT id FROM Highschooler EXCEPT SELECT student_id FROM Friend
SELECT name FROM Highschooler EXCEPT SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id
SELECT name FROM Highschooler EXCEPT SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id
SELECT student_id FROM Friend INTERSECT SELECT liked_id FROM Likes
SELECT student_id FROM Friend INTERSECT SELECT liked_id FROM Likes
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id INTERSECT SELECT T2.name FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.liked_id = T2.id
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id INTERSECT SELECT T2.name FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.liked_id = T2.id
SELECT student_id , count(*) FROM Likes GROUP BY student_id
SELECT student_id , count(*) FROM Likes GROUP BY student_id
SELECT T2.name , count(*) FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id
SELECT T2.name , count(*) FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id
SELECT T2.name FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.name FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.name FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id HAVING count(*) >= 2
SELECT T2.name FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id GROUP BY T1.student_id HAVING count(*) >= 2
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id WHERE T2.grade > 5 GROUP BY T1.student_id HAVING count(*) >= 2
SELECT T2.name FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id WHERE T2.grade > 5 GROUP BY T1.student_id HAVING count(*) >= 2
SELECT count(*) FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id WHERE T2.name = "Kyle"
SELECT count(*) FROM Likes AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id WHERE T2.name = "Kyle"
SELECT avg(grade) FROM Highschooler WHERE id IN (SELECT T1.student_id FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id)
SELECT avg(grade) FROM Highschooler WHERE id IN (SELECT T1.student_id FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id)
SELECT min(grade) FROM Highschooler WHERE id NOT IN (SELECT T1.student_id FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id)
SELECT min(grade) FROM Highschooler WHERE id NOT IN (SELECT T1.student_id FROM Friend AS T1 JOIN Highschooler AS T2 ON T1.student_id = T2.id)
