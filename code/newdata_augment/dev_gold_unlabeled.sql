SELECT count( allergy) FROM Allergy_type
SELECT count( allergy) FROM Allergy_type
SELECT count( allergytype) FROM Allergy_type
SELECT count( allergytype) FROM Allergy_type
SELECT allergytype FROM Allergy_type
SELECT allergytype FROM Allergy_type
SELECT allergy , allergytype FROM Allergy_type
SELECT allergy , allergytype FROM Allergy_type
SELECT allergy FROM Allergy_type WHERE allergytype = "food"
SELECT allergy FROM Allergy_type WHERE allergytype = "food"
SELECT allergytype FROM Allergy_type WHERE allergy = "Cat"
SELECT allergytype FROM Allergy_type WHERE allergy = "Cat"
SELECT count(*) FROM Allergy_type WHERE allergytype = "animal"
SELECT count(*) FROM Allergy_type WHERE allergytype = "animal"
SELECT allergytype , count(*) FROM Allergy_type GROUP BY allergytype
SELECT allergytype , count(*) FROM Allergy_type GROUP BY allergytype
SELECT allergytype FROM Allergy_type GROUP BY allergytype ORDER BY count(*) DESC LIMIT 1
SELECT allergytype FROM Allergy_type GROUP BY allergytype ORDER BY count(*) DESC LIMIT 1
SELECT allergytype FROM Allergy_type GROUP BY allergytype ORDER BY count(*) ASC LIMIT 1
SELECT allergytype FROM Allergy_type GROUP BY allergytype ORDER BY count(*) ASC LIMIT 1
SELECT count(*) FROM Student
SELECT count(*) FROM Student
SELECT Fname , Lname FROM Student
SELECT Fname , Lname FROM Student
SELECT count( advisor) FROM Student
SELECT count( advisor) FROM Student
SELECT Major FROM Student
SELECT Major FROM Student
SELECT city_code FROM Student
SELECT city_code FROM Student
SELECT Fname , Lname , Age FROM Student WHERE Sex = 'F'
SELECT Fname , Lname , Age FROM Student WHERE Sex = 'F'
SELECT StuID FROM Student WHERE Sex = 'M'
SELECT StuID FROM Student WHERE Sex = 'M'
SELECT count(*) FROM Student WHERE age = 18
SELECT count(*) FROM Student WHERE age = 18
SELECT StuID FROM Student WHERE age > 20
SELECT StuID FROM Student WHERE age > 20
SELECT city_code FROM Student WHERE LName = "Kim"
SELECT city_code FROM Student WHERE LName = "Kim"
SELECT Advisor FROM Student WHERE StuID = 1004
SELECT Advisor FROM Student WHERE StuID = 1004
SELECT count(*) FROM Student WHERE city_code = "HKG" OR city_code = "CHI"
SELECT count(*) FROM Student WHERE city_code = "HKG" OR city_code = "CHI"
SELECT min(age) , avg(age) , max(age) FROM Student
SELECT min(age) , avg(age) , max(age) FROM Student
SELECT LName FROM Student WHERE age = (SELECT min(age) FROM Student)
SELECT LName FROM Student WHERE age = (SELECT min(age) FROM Student)
SELECT StuID FROM Student WHERE age = (SELECT max(age) FROM Student)
SELECT StuID FROM Student WHERE age = (SELECT max(age) FROM Student)
SELECT major , count(*) FROM Student GROUP BY major
SELECT major , count(*) FROM Student GROUP BY major
SELECT major FROM Student GROUP BY major ORDER BY count(*) DESC LIMIT 1
SELECT major FROM Student GROUP BY major ORDER BY count(*) DESC LIMIT 1
SELECT age , count(*) FROM Student GROUP BY age
SELECT age , count(*) FROM Student GROUP BY age
SELECT avg(age) , sex FROM Student GROUP BY sex
SELECT avg(age) , sex FROM Student GROUP BY sex
SELECT city_code , count(*) FROM Student GROUP BY city_code
SELECT city_code , count(*) FROM Student GROUP BY city_code
SELECT advisor , count(*) FROM Student GROUP BY advisor
SELECT advisor , count(*) FROM Student GROUP BY advisor
SELECT advisor FROM Student GROUP BY advisor ORDER BY count(*) DESC LIMIT 1
SELECT advisor FROM Student GROUP BY advisor ORDER BY count(*) DESC LIMIT 1
SELECT count(*) FROM Has_allergy WHERE Allergy = "Cat"
SELECT count(*) FROM Has_allergy WHERE Allergy = "Cat"
SELECT StuID FROM Has_allergy GROUP BY StuID HAVING count(*) >= 2
SELECT StuID FROM Has_allergy GROUP BY StuID HAVING count(*) >= 2
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Has_allergy
SELECT StuID FROM Student EXCEPT SELECT StuID FROM Has_allergy
SELECT count(*) FROM has_allergy AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T2.sex = "F" AND T1.allergy = "Milk" OR T1.allergy = "Eggs"
SELECT count(*) FROM has_allergy AS T1 JOIN Student AS T2 ON T1.StuID = T2.StuID WHERE T2.sex = "F" AND T1.allergy = "Milk" OR T1.allergy = "Eggs"
SELECT count(*) FROM Has_allergy AS T1 JOIN Allergy_type AS T2 ON T1.allergy = T2.allergy WHERE T2.allergytype = "food"
SELECT count(*) FROM Has_allergy AS T1 JOIN Allergy_type AS T2 ON T1.allergy = T2.allergy WHERE T2.allergytype = "food"
SELECT Allergy FROM Has_allergy GROUP BY Allergy ORDER BY count(*) DESC LIMIT 1
SELECT Allergy FROM Has_allergy GROUP BY Allergy ORDER BY count(*) DESC LIMIT 1
SELECT Allergy , count(*) FROM Has_allergy GROUP BY Allergy
SELECT Allergy , count(*) FROM Has_allergy GROUP BY Allergy
SELECT T2.allergytype , count(*) FROM Has_allergy AS T1 JOIN Allergy_type AS T2 ON T1.allergy = T2.allergy GROUP BY T2.allergytype
SELECT T2.allergytype , count(*) FROM Has_allergy AS T1 JOIN Allergy_type AS T2 ON T1.allergy = T2.allergy GROUP BY T2.allergytype
SELECT lname , age FROM Student WHERE StuID IN (SELECT StuID FROM Has_allergy WHERE Allergy = "Milk" INTERSECT SELECT StuID FROM Has_allergy WHERE Allergy = "Cat")
SELECT lname , age FROM Student WHERE StuID IN (SELECT StuID FROM Has_allergy WHERE Allergy = "Milk" INTERSECT SELECT StuID FROM Has_allergy WHERE Allergy = "Cat")
SELECT T1.Allergy , T1.AllergyType FROM Allergy_type AS T1 JOIN Has_allergy AS T2 ON T1.Allergy = T2.Allergy JOIN Student AS T3 ON T3.StuID = T2.StuID WHERE T3.Fname = "Lisa" ORDER BY T1.Allergy
SELECT T1.Allergy , T1.AllergyType FROM Allergy_type AS T1 JOIN Has_allergy AS T2 ON T1.Allergy = T2.Allergy JOIN Student AS T3 ON T3.StuID = T2.StuID WHERE T3.Fname = "Lisa" ORDER BY T1.Allergy
SELECT fname , sex FROM Student WHERE StuID IN (SELECT StuID FROM Has_allergy WHERE Allergy = "Milk" EXCEPT SELECT StuID FROM Has_allergy WHERE Allergy = "Cat")
SELECT fname , sex FROM Student WHERE StuID IN (SELECT StuID FROM Has_allergy WHERE Allergy = "Milk" EXCEPT SELECT StuID FROM Has_allergy WHERE Allergy = "Cat")
SELECT avg(age) FROM Student WHERE StuID IN ( SELECT T1.StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food" INTERSECT SELECT T1.StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "animal")
SELECT avg(age) FROM Student WHERE StuID IN ( SELECT T1.StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food" INTERSECT SELECT T1.StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "animal")
SELECT fname , lname FROM Student WHERE StuID NOT IN (SELECT T1.StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food")
SELECT fname , lname FROM Student WHERE StuID NOT IN (SELECT T1.StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food")
SELECT count(*) FROM Student WHERE sex = "M" AND StuID IN (SELECT StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food")
SELECT count(*) FROM Student WHERE sex = "M" AND StuID IN (SELECT StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food")
SELECT T1.fname , T1.city_code FROM Student AS T1 JOIN Has_Allergy AS T2 ON T1.stuid = T2.stuid WHERE T2.Allergy = "Milk" OR T2.Allergy = "Cat"
SELECT T1.fname , T1.city_code FROM Student AS T1 JOIN Has_Allergy AS T2 ON T1.stuid = T2.stuid WHERE T2.Allergy = "Milk" OR T2.Allergy = "Cat"
SELECT count(*) FROM Student WHERE age > 18 AND StuID NOT IN ( SELECT StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food" OR T2.allergytype = "animal")
SELECT count(*) FROM Student WHERE age > 18 AND StuID NOT IN ( SELECT StuID FROM Has_allergy AS T1 JOIN Allergy_Type AS T2 ON T1.Allergy = T2.Allergy WHERE T2.allergytype = "food" OR T2.allergytype = "animal")
SELECT fname , major FROM Student WHERE StuID NOT IN (SELECT StuID FROM Has_allergy WHERE Allergy = "Soy")
SELECT fname , major FROM Student WHERE StuID NOT IN (SELECT StuID FROM Has_allergy WHERE Allergy = "Soy")
SELECT building FROM classroom WHERE capacity > 50
SELECT building FROM classroom WHERE capacity > 50
SELECT count(*) FROM classroom WHERE building != 'Lamberton'
SELECT count(*) FROM classroom WHERE building != 'Lamberton'
SELECT dept_name , building FROM department WHERE budget > (SELECT avg(budget) FROM department)
SELECT dept_name , building FROM department WHERE budget > (SELECT avg(budget) FROM department)
SELECT building , room_number FROM classroom WHERE capacity BETWEEN 50 AND 100
SELECT building , room_number FROM classroom WHERE capacity BETWEEN 50 AND 100
SELECT dept_name , building FROM department ORDER BY budget DESC LIMIT 1
SELECT dept_name , building FROM department ORDER BY budget DESC LIMIT 1
SELECT name FROM student WHERE dept_name = 'History' ORDER BY tot_cred DESC LIMIT 1
SELECT name FROM student WHERE dept_name = 'History' ORDER BY tot_cred DESC LIMIT 1
SELECT count(*) FROM classroom WHERE building = 'Lamberton'
SELECT count(*) FROM classroom WHERE building = 'Lamberton'
SELECT count( s_id) FROM advisor
SELECT count( s_id) FROM advisor
SELECT count( dept_name) FROM course
SELECT count( dept_name) FROM course
SELECT count( course_id) FROM course WHERE dept_name = 'Physics'
SELECT count( course_id) FROM course WHERE dept_name = 'Physics'
SELECT T1.title FROM course AS T1 JOIN prereq AS T2 ON T1.course_id = T2.course_id GROUP BY T2.course_id HAVING count(*) = 2
SELECT T1.title FROM course AS T1 JOIN prereq AS T2 ON T1.course_id = T2.course_id GROUP BY T2.course_id HAVING count(*) = 2
SELECT T1.title , T1.credits , T1.dept_name FROM course AS T1 JOIN prereq AS T2 ON T1.course_id = T2.course_id GROUP BY T2.course_id HAVING count(*) > 1
SELECT T1.title , T1.credits , T1.dept_name FROM course AS T1 JOIN prereq AS T2 ON T1.course_id = T2.course_id GROUP BY T2.course_id HAVING count(*) > 1
SELECT count(*) FROM course WHERE course_id NOT IN (SELECT course_id FROM prereq)
SELECT count(*) FROM course WHERE course_id NOT IN (SELECT course_id FROM prereq)
SELECT title FROM course WHERE course_id NOT IN (SELECT course_id FROM prereq)
SELECT title FROM course WHERE course_id NOT IN (SELECT course_id FROM prereq)
SELECT COUNT ( id) FROM teaches
SELECT COUNT ( id) FROM teaches
SELECT sum(budget) FROM department WHERE dept_name = 'Marketing' OR dept_name = 'Finance'
SELECT sum(budget) FROM department WHERE dept_name = 'Marketing' OR dept_name = 'Finance'
SELECT dept_name FROM instructor WHERE name LIKE '%Soisalon%'
SELECT dept_name FROM instructor WHERE name LIKE '%Soisalon%'
SELECT count(*) FROM classroom WHERE building = 'Lamberton' AND capacity < 50
SELECT count(*) FROM classroom WHERE building = 'Lamberton' AND capacity < 50
SELECT dept_name , budget FROM department WHERE budget > (SELECT avg(budget) FROM department)
SELECT dept_name , budget FROM department WHERE budget > (SELECT avg(budget) FROM department)
SELECT name FROM instructor WHERE dept_name = 'Statistics' ORDER BY salary LIMIT 1
SELECT name FROM instructor WHERE dept_name = 'Statistics' ORDER BY salary LIMIT 1
SELECT title FROM course WHERE dept_name = 'Statistics' INTERSECT SELECT title FROM course WHERE dept_name = 'Psychology'
SELECT title FROM course WHERE dept_name = 'Statistics' INTERSECT SELECT title FROM course WHERE dept_name = 'Psychology'
SELECT title FROM course WHERE dept_name = 'Statistics' EXCEPT SELECT title FROM course WHERE dept_name = 'Psychology'
SELECT title FROM course WHERE dept_name = 'Statistics' EXCEPT SELECT title FROM course WHERE dept_name = 'Psychology'
SELECT id FROM teaches WHERE semester = 'Fall' AND YEAR = 2009 EXCEPT SELECT id FROM teaches WHERE semester = 'Spring' AND YEAR = 2010
SELECT id FROM teaches WHERE semester = 'Fall' AND YEAR = 2009 EXCEPT SELECT id FROM teaches WHERE semester = 'Spring' AND YEAR = 2010
SELECT T1.name FROM student AS T1 JOIN takes AS T2 ON T1.id = T2.id WHERE YEAR = 2009 OR YEAR = 2010
SELECT T1.name FROM student AS T1 JOIN takes AS T2 ON T1.id = T2.id WHERE YEAR = 2009 OR YEAR = 2010
SELECT dept_name FROM course GROUP BY dept_name ORDER BY count(*) DESC LIMIT 3
SELECT dept_name FROM course GROUP BY dept_name ORDER BY count(*) DESC LIMIT 3
SELECT dept_name FROM course GROUP BY dept_name ORDER BY sum(credits) DESC LIMIT 1
SELECT dept_name FROM course GROUP BY dept_name ORDER BY sum(credits) DESC LIMIT 1
SELECT title FROM course ORDER BY title , credits
SELECT title FROM course ORDER BY title , credits
SELECT dept_name FROM department ORDER BY budget LIMIT 1
SELECT dept_name FROM department ORDER BY budget LIMIT 1
SELECT dept_name , building FROM department ORDER BY budget DESC
SELECT dept_name , building FROM department ORDER BY budget DESC
SELECT name FROM instructor ORDER BY salary DESC LIMIT 1
SELECT name FROM instructor ORDER BY salary DESC LIMIT 1
SELECT * FROM instructor ORDER BY salary
SELECT * FROM instructor ORDER BY salary
SELECT name , dept_name FROM student ORDER BY tot_cred
SELECT name , dept_name FROM student ORDER BY tot_cred
SELECT T1.title , T3.name FROM course AS T1 JOIN teaches AS T2 ON T1.course_id = T2.course_id JOIN instructor AS T3 ON T2.id = T3.id WHERE YEAR = 2008 ORDER BY T1.title
SELECT T1.title , T3.name FROM course AS T1 JOIN teaches AS T2 ON T1.course_id = T2.course_id JOIN instructor AS T3 ON T2.id = T3.id WHERE YEAR = 2008 ORDER BY T1.title
SELECT T1.name FROM instructor AS T1 JOIN advisor AS T2 ON T1.id = T2.i_id GROUP BY T2.i_id HAVING count(*) > 1
SELECT T1.name FROM instructor AS T1 JOIN advisor AS T2 ON T1.id = T2.i_id GROUP BY T2.i_id HAVING count(*) > 1
SELECT T1.name FROM student AS T1 JOIN advisor AS T2 ON T1.id = T2.s_id GROUP BY T2.s_id HAVING count(*) > 1
SELECT T1.name FROM student AS T1 JOIN advisor AS T2 ON T1.id = T2.s_id GROUP BY T2.s_id HAVING count(*) > 1
SELECT count(*) , building FROM classroom WHERE capacity > 50 GROUP BY building
SELECT count(*) , building FROM classroom WHERE capacity > 50 GROUP BY building
SELECT max(capacity) , avg(capacity) , building FROM classroom GROUP BY building
SELECT max(capacity) , avg(capacity) , building FROM classroom GROUP BY building
SELECT title FROM course GROUP BY title HAVING count(*) > 1
SELECT title FROM course GROUP BY title HAVING count(*) > 1
SELECT sum(credits) , dept_name FROM course GROUP BY dept_name
SELECT sum(credits) , dept_name FROM course GROUP BY dept_name
SELECT min(salary) , dept_name FROM instructor GROUP BY dept_name HAVING avg(salary) > (SELECT avg(salary) FROM instructor)
SELECT min(salary) , dept_name FROM instructor GROUP BY dept_name HAVING avg(salary) > (SELECT avg(salary) FROM instructor)
SELECT count(*) , semester , YEAR FROM SECTION GROUP BY semester , YEAR
SELECT count(*) , semester , YEAR FROM SECTION GROUP BY semester , YEAR
SELECT YEAR FROM SECTION GROUP BY YEAR ORDER BY count(*) DESC LIMIT 1
SELECT YEAR FROM SECTION GROUP BY YEAR ORDER BY count(*) DESC LIMIT 1
SELECT semester , YEAR FROM SECTION GROUP BY semester , YEAR ORDER BY count(*) DESC LIMIT 1
SELECT semester , YEAR FROM SECTION GROUP BY semester , YEAR ORDER BY count(*) DESC LIMIT 1
SELECT dept_name FROM student GROUP BY dept_name ORDER BY count(*) DESC LIMIT 1
SELECT dept_name FROM student GROUP BY dept_name ORDER BY count(*) DESC LIMIT 1
SELECT count(*) , dept_name FROM student GROUP BY dept_name
SELECT count(*) , dept_name FROM student GROUP BY dept_name
SELECT semester , YEAR FROM takes GROUP BY semester , YEAR ORDER BY count(*) LIMIT 1
SELECT semester , YEAR FROM takes GROUP BY semester , YEAR ORDER BY count(*) LIMIT 1
SELECT i_id FROM advisor AS T1 JOIN student AS T2 ON T1.s_id = T2.id WHERE T2.dept_name = 'History'
SELECT i_id FROM advisor AS T1 JOIN student AS T2 ON T1.s_id = T2.id WHERE T2.dept_name = 'History'
SELECT T2.name , T2.salary FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id WHERE T3.dept_name = 'History'
SELECT T2.name , T2.salary FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id WHERE T3.dept_name = 'History'
SELECT course_id FROM course EXCEPT SELECT course_id FROM prereq
SELECT course_id FROM course EXCEPT SELECT course_id FROM prereq
SELECT title FROM course WHERE course_id NOT IN (SELECT course_id FROM prereq)
SELECT title FROM course WHERE course_id NOT IN (SELECT course_id FROM prereq)
SELECT title FROM course WHERE course_id IN (SELECT T1.prereq_id FROM prereq AS T1 JOIN course AS T2 ON T1.course_id = T2.course_id WHERE T2.title = 'International Finance')
SELECT title FROM course WHERE course_id IN (SELECT T1.prereq_id FROM prereq AS T1 JOIN course AS T2 ON T1.course_id = T2.course_id WHERE T2.title = 'International Finance')
SELECT title FROM course WHERE course_id IN (SELECT T1.course_id FROM prereq AS T1 JOIN course AS T2 ON T1.prereq_id = T2.course_id WHERE T2.title = 'Differential Geometry')
SELECT title FROM course WHERE course_id IN (SELECT T1.course_id FROM prereq AS T1 JOIN course AS T2 ON T1.prereq_id = T2.course_id WHERE T2.title = 'Differential Geometry')
SELECT name FROM student WHERE id IN (SELECT id FROM takes WHERE semester = 'Fall' AND YEAR = 2003)
SELECT name FROM student WHERE id IN (SELECT id FROM takes WHERE semester = 'Fall' AND YEAR = 2003)
SELECT T1.title FROM course AS T1 JOIN SECTION AS T2 ON T1.course_id = T2.course_id WHERE building = 'Chandler' AND semester = 'Fall' AND YEAR = 2010
SELECT T1.title FROM course AS T1 JOIN SECTION AS T2 ON T1.course_id = T2.course_id WHERE building = 'Chandler' AND semester = 'Fall' AND YEAR = 2010
SELECT T1.name FROM instructor AS T1 JOIN teaches AS T2 ON T1.id = T2.id JOIN course AS T3 ON T2.course_id = T3.course_id WHERE T3.title = 'C Programming'
SELECT T1.name FROM instructor AS T1 JOIN teaches AS T2 ON T1.id = T2.id JOIN course AS T3 ON T2.course_id = T3.course_id WHERE T3.title = 'C Programming'
SELECT T2.name , T2.salary FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id WHERE T3.dept_name = 'Math'
SELECT T2.name , T2.salary FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id WHERE T3.dept_name = 'Math'
SELECT T2.name FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id WHERE T3.dept_name = 'Math' ORDER BY T3.tot_cred
SELECT T2.name FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id WHERE T3.dept_name = 'Math' ORDER BY T3.tot_cred
SELECT title FROM course WHERE course_id IN (SELECT T1.prereq_id FROM prereq AS T1 JOIN course AS T2 ON T1.course_id = T2.course_id WHERE T2.title = 'Mobile Computing')
SELECT title FROM course WHERE course_id IN (SELECT T1.prereq_id FROM prereq AS T1 JOIN course AS T2 ON T1.course_id = T2.course_id WHERE T2.title = 'Mobile Computing')
SELECT T2.name FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id ORDER BY T3.tot_cred DESC LIMIT 1
SELECT T2.name FROM advisor AS T1 JOIN instructor AS T2 ON T1.i_id = T2.id JOIN student AS T3 ON T1.s_id = T3.id ORDER BY T3.tot_cred DESC LIMIT 1
SELECT name FROM instructor WHERE id NOT IN (SELECT id FROM teaches)
SELECT name FROM instructor WHERE id NOT IN (SELECT id FROM teaches)
SELECT id FROM instructor EXCEPT SELECT id FROM teaches
SELECT id FROM instructor EXCEPT SELECT id FROM teaches
SELECT name FROM instructor WHERE id NOT IN (SELECT id FROM teaches WHERE semester = 'Spring')
SELECT name FROM instructor WHERE id NOT IN (SELECT id FROM teaches WHERE semester = 'Spring')
SELECT dept_name FROM instructor GROUP BY dept_name ORDER BY avg(salary) DESC LIMIT 1
SELECT dept_name FROM instructor GROUP BY dept_name ORDER BY avg(salary) DESC LIMIT 1
SELECT avg(T1.salary) , count(*) FROM instructor AS T1 JOIN department AS T2 ON T1.dept_name = T2.dept_name ORDER BY T2.budget DESC LIMIT 1
SELECT avg(T1.salary) , count(*) FROM instructor AS T1 JOIN department AS T2 ON T1.dept_name = T2.dept_name ORDER BY T2.budget DESC LIMIT 1
SELECT T3.title , T3.credits FROM classroom AS T1 JOIN SECTION AS T2 ON T1.building = T2.building AND T1.room_number = T2.room_number JOIN course AS T3 ON T2.course_id = T3.course_id WHERE T1.capacity = (SELECT max(capacity) FROM classroom)
SELECT T3.title , T3.credits FROM classroom AS T1 JOIN SECTION AS T2 ON T1.building = T2.building AND T1.room_number = T2.room_number JOIN course AS T3 ON T2.course_id = T3.course_id WHERE T1.capacity = (SELECT max(capacity) FROM classroom)
SELECT name FROM student WHERE id NOT IN (SELECT T1.id FROM takes AS T1 JOIN course AS T2 ON T1.course_id = T2.course_id WHERE T2.dept_name = 'Biology')
SELECT name FROM student WHERE id NOT IN (SELECT T1.id FROM takes AS T1 JOIN course AS T2 ON T1.course_id = T2.course_id WHERE T2.dept_name = 'Biology')
SELECT count( T2.id) , count( T3.id) , T3.dept_name FROM department AS T1 JOIN student AS T2 ON T1.dept_name = T2.dept_name JOIN instructor AS T3 ON T1.dept_name = T3.dept_name GROUP BY T3.dept_name
SELECT count( T2.id) , count( T3.id) , T3.dept_name FROM department AS T1 JOIN student AS T2 ON T1.dept_name = T2.dept_name JOIN instructor AS T3 ON T1.dept_name = T3.dept_name GROUP BY T3.dept_name
SELECT T1.name FROM student AS T1 JOIN takes AS T2 ON T1.id = T2.id WHERE T2.course_id IN (SELECT T4.prereq_id FROM course AS T3 JOIN prereq AS T4 ON T3.course_id = T4.course_id WHERE T3.title = 'International Finance')
SELECT T1.name FROM student AS T1 JOIN takes AS T2 ON T1.id = T2.id WHERE T2.course_id IN (SELECT T4.prereq_id FROM course AS T3 JOIN prereq AS T4 ON T3.course_id = T4.course_id WHERE T3.title = 'International Finance')
SELECT name , salary FROM instructor WHERE salary < (SELECT avg(salary) FROM instructor WHERE dept_name = 'Physics')
SELECT name , salary FROM instructor WHERE salary < (SELECT avg(salary) FROM instructor WHERE dept_name = 'Physics')
SELECT T3.name FROM course AS T1 JOIN takes AS T2 ON T1.course_id = T2.course_id JOIN student AS T3 ON T2.id = T3.id WHERE T1.dept_name = 'Statistics'
SELECT T3.name FROM course AS T1 JOIN takes AS T2 ON T1.course_id = T2.course_id JOIN student AS T3 ON T2.id = T3.id WHERE T1.dept_name = 'Statistics'
SELECT T2.building , T2.room_number , T2.semester , T2.year FROM course AS T1 JOIN SECTION AS T2 ON T1.course_id = T2.course_id WHERE T1.dept_name = 'Psychology' ORDER BY T1.title
SELECT T2.building , T2.room_number , T2.semester , T2.year FROM course AS T1 JOIN SECTION AS T2 ON T1.course_id = T2.course_id WHERE T1.dept_name = 'Psychology' ORDER BY T1.title
SELECT name FROM instructor WHERE dept_name = 'Comp. Sci.'
SELECT name FROM instructor WHERE dept_name = 'Comp. Sci.'
SELECT name FROM instructor WHERE dept_name = 'Comp. Sci.' AND salary > 80000
SELECT name FROM instructor WHERE dept_name = 'Comp. Sci.' AND salary > 80000
SELECT name , course_id FROM instructor AS T1 JOIN teaches AS T2 ON T1.ID = T2.ID
SELECT name , course_id FROM instructor AS T1 JOIN teaches AS T2 ON T1.ID = T2.ID
SELECT name , course_id FROM instructor AS T1 JOIN teaches AS T2 ON T1.ID = T2.ID WHERE T1.dept_name = 'Art'
SELECT name , course_id FROM instructor AS T1 JOIN teaches AS T2 ON T1.ID = T2.ID WHERE T1.dept_name = 'Art'
SELECT name FROM instructor WHERE name LIKE '%dar%'
SELECT name FROM instructor WHERE name LIKE '%dar%'
SELECT name FROM instructor ORDER BY name
SELECT name FROM instructor ORDER BY name
SELECT course_id FROM SECTION WHERE semester = 'Fall' AND YEAR = 2009 UNION SELECT course_id FROM SECTION WHERE semester = 'Spring' AND YEAR = 2010
SELECT course_id FROM SECTION WHERE semester = 'Fall' AND YEAR = 2009 UNION SELECT course_id FROM SECTION WHERE semester = 'Spring' AND YEAR = 2010
SELECT course_id FROM SECTION WHERE semester = 'Fall' AND YEAR = 2009 INTERSECT SELECT course_id FROM SECTION WHERE semester = 'Spring' AND YEAR = 2010
SELECT course_id FROM SECTION WHERE semester = 'Fall' AND YEAR = 2009 INTERSECT SELECT course_id FROM SECTION WHERE semester = 'Spring' AND YEAR = 2010
SELECT course_id FROM SECTION WHERE semester = 'Fall' AND YEAR = 2009 EXCEPT SELECT course_id FROM SECTION WHERE semester = 'Spring' AND YEAR = 2010
SELECT course_id FROM SECTION WHERE semester = 'Fall' AND YEAR = 2009 EXCEPT SELECT course_id FROM SECTION WHERE semester = 'Spring' AND YEAR = 2010
SELECT salary FROM instructor WHERE salary < (SELECT max(salary) FROM instructor)
SELECT salary FROM instructor WHERE salary < (SELECT max(salary) FROM instructor)
SELECT COUNT ( ID) FROM teaches WHERE semester = 'Spring' AND YEAR = 2010
SELECT COUNT ( ID) FROM teaches WHERE semester = 'Spring' AND YEAR = 2010
SELECT dept_name , AVG (salary) FROM instructor GROUP BY dept_name HAVING AVG (salary) > 42000
SELECT dept_name , AVG (salary) FROM instructor GROUP BY dept_name HAVING AVG (salary) > 42000
SELECT name FROM instructor WHERE salary > (SELECT min(salary) FROM instructor WHERE dept_name = 'Biology')
SELECT name FROM instructor WHERE salary > (SELECT min(salary) FROM instructor WHERE dept_name = 'Biology')
SELECT name FROM instructor WHERE salary > (SELECT max(salary) FROM instructor WHERE dept_name = 'Biology')
SELECT name FROM instructor WHERE salary > (SELECT max(salary) FROM instructor WHERE dept_name = 'Biology')
SELECT count(*) FROM Accounts
SELECT count(*) FROM Accounts
SELECT count( customer_id) FROM Accounts
SELECT count( customer_id) FROM Accounts
SELECT account_id , date_account_opened , account_name , other_account_details FROM Accounts
SELECT account_id , date_account_opened , account_name , other_account_details FROM Accounts
SELECT T1.account_id , T1.date_account_opened , T1.account_name , T1.other_account_details FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = 'Meaghan'
SELECT T1.account_id , T1.date_account_opened , T1.account_name , T1.other_account_details FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = 'Meaghan'
SELECT T1.account_name , T1.other_account_details FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Meaghan" AND T2.customer_last_name = "Keeling"
SELECT T1.account_name , T1.other_account_details FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T2.customer_first_name = "Meaghan" AND T2.customer_last_name = "Keeling"
SELECT T2.customer_first_name , T2.customer_last_name FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.account_name = "900"
SELECT T2.customer_first_name , T2.customer_last_name FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id WHERE T1.account_name = "900"
SELECT count(*) FROM Customers WHERE customer_id NOT IN (SELECT customer_id FROM Accounts)
SELECT count(*) FROM Customers WHERE customer_id NOT IN (SELECT customer_id FROM Accounts)
SELECT T1.customer_first_name , T1.customer_last_name , T1.phone_number FROM Customers AS T1 JOIN Accounts AS T2 ON T1.customer_id = T2.customer_id
SELECT T1.customer_first_name , T1.customer_last_name , T1.phone_number FROM Customers AS T1 JOIN Accounts AS T2 ON T1.customer_id = T2.customer_id
SELECT customer_id FROM Customers EXCEPT SELECT customer_id FROM Accounts
SELECT customer_id FROM Customers EXCEPT SELECT customer_id FROM Accounts
SELECT count(*) , customer_id FROM Accounts GROUP BY customer_id
SELECT count(*) , customer_id FROM Accounts GROUP BY customer_id
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name , count(*) FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id
SELECT T1.customer_id , T2.customer_first_name , T2.customer_last_name , count(*) FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id
SELECT T2.customer_first_name , T1.customer_id FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 2
SELECT T2.customer_first_name , T1.customer_id FROM Accounts AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id HAVING count(*) >= 2
SELECT count(*) FROM Customers
SELECT count(*) FROM Customers
SELECT gender , count(*) FROM Customers GROUP BY gender
SELECT gender , count(*) FROM Customers GROUP BY gender
SELECT count(*) FROM Financial_transactions
SELECT count(*) FROM Financial_transactions
SELECT count(*) , account_id FROM Financial_transactions
SELECT count(*) , account_id FROM Financial_transactions
SELECT count(*) FROM Financial_transactions AS T1 JOIN Accounts AS T2 ON T1.account_id = T2.account_id WHERE T2.account_name = "337"
SELECT count(*) FROM Financial_transactions AS T1 JOIN Accounts AS T2 ON T1.account_id = T2.account_id WHERE T2.account_name = "337"
SELECT avg(transaction_amount) , min(transaction_amount) , max(transaction_amount) , sum(transaction_amount) FROM Financial_transactions
SELECT avg(transaction_amount) , min(transaction_amount) , max(transaction_amount) , sum(transaction_amount) FROM Financial_transactions
SELECT transaction_id FROM Financial_transactions WHERE transaction_amount > (SELECT avg(transaction_amount) FROM Financial_transactions)
SELECT transaction_id FROM Financial_transactions WHERE transaction_amount > (SELECT avg(transaction_amount) FROM Financial_transactions)
SELECT transaction_type , sum(transaction_amount) FROM Financial_transactions GROUP BY transaction_type
SELECT transaction_type , sum(transaction_amount) FROM Financial_transactions GROUP BY transaction_type
SELECT T2.account_name , T1.account_id , count(*) FROM Financial_transactions AS T1 JOIN Accounts AS T2 ON T1.account_id = T2.account_id GROUP BY T1.account_id
SELECT T2.account_name , T1.account_id , count(*) FROM Financial_transactions AS T1 JOIN Accounts AS T2 ON T1.account_id = T2.account_id GROUP BY T1.account_id
SELECT account_id FROM Financial_transactions GROUP BY account_id ORDER BY count(*) DESC LIMIT 1
SELECT account_id FROM Financial_transactions GROUP BY account_id ORDER BY count(*) DESC LIMIT 1
SELECT T1.account_id , T2.account_name FROM Financial_transactions AS T1 JOIN Accounts AS T2 ON T1.account_id = T2.account_id GROUP BY T1.account_id HAVING count(*) >= 4
SELECT T1.account_id , T2.account_name FROM Financial_transactions AS T1 JOIN Accounts AS T2 ON T1.account_id = T2.account_id GROUP BY T1.account_id HAVING count(*) >= 4
SELECT product_size FROM Products
SELECT product_size FROM Products
SELECT product_color FROM Products
SELECT product_color FROM Products
SELECT invoice_number , count(*) FROM Financial_transactions GROUP BY invoice_number
SELECT invoice_number , count(*) FROM Financial_transactions GROUP BY invoice_number
SELECT T2.invoice_number , T2.invoice_date FROM Financial_transactions AS T1 JOIN Invoices AS T2 ON T1.invoice_number = T2.invoice_number GROUP BY T1.invoice_number ORDER BY count(*) DESC LIMIT 1
SELECT T2.invoice_number , T2.invoice_date FROM Financial_transactions AS T1 JOIN Invoices AS T2 ON T1.invoice_number = T2.invoice_number GROUP BY T1.invoice_number ORDER BY count(*) DESC LIMIT 1
SELECT count(*) FROM Invoices
SELECT count(*) FROM Invoices
SELECT T1.invoice_date , T1.order_id , T2.order_details FROM Invoices AS T1 JOIN Orders AS T2 ON T1.order_id = T2.order_id
SELECT T1.invoice_date , T1.order_id , T2.order_details FROM Invoices AS T1 JOIN Orders AS T2 ON T1.order_id = T2.order_id
SELECT order_id , count(*) FROM Invoices GROUP BY order_id
SELECT order_id , count(*) FROM Invoices GROUP BY order_id
SELECT T2.order_id , T2.order_details FROM Invoices AS T1 JOIN Orders AS T2 ON T1.order_id = T2.order_id GROUP BY T2.order_id HAVING count(*) > 2
SELECT T2.order_id , T2.order_details FROM Invoices AS T1 JOIN Orders AS T2 ON T1.order_id = T2.order_id GROUP BY T2.order_id HAVING count(*) > 2
SELECT T2.customer_last_name , T1.customer_id , T2.phone_number FROM Orders AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.customer_last_name , T1.customer_id , T2.phone_number FROM Orders AS T1 JOIN Customers AS T2 ON T1.customer_id = T2.customer_id GROUP BY T1.customer_id ORDER BY count(*) DESC LIMIT 1
SELECT product_name FROM Products EXCEPT SELECT T1.product_name FROM Products AS T1 JOIN Order_items AS T2 ON T1.product_id = T2.product_id
SELECT product_name FROM Products EXCEPT SELECT T1.product_name FROM Products AS T1 JOIN Order_items AS T2 ON T1.product_id = T2.product_id
SELECT T2.product_name , sum(T1.product_quantity) FROM Order_items AS T1 JOIN Products AS T2 ON T1.product_id = T2.product_id GROUP BY T2.product_name
SELECT T2.product_name , sum(T1.product_quantity) FROM Order_items AS T1 JOIN Products AS T2 ON T1.product_id = T2.product_id GROUP BY T2.product_name
SELECT order_id , count(*) FROM Order_items GROUP BY order_id
SELECT order_id , count(*) FROM Order_items GROUP BY order_id
SELECT product_id , count( order_id) FROM Order_items GROUP BY product_id
SELECT product_id , count( order_id) FROM Order_items GROUP BY product_id
SELECT T2.product_name , count(*) FROM Order_items AS T1 JOIN Products AS T2 ON T1.product_id = T2.product_id JOIN Orders AS T3 ON T3.order_id = T1.order_id GROUP BY T2.product_name
SELECT T2.product_name , count(*) FROM Order_items AS T1 JOIN Products AS T2 ON T1.product_id = T2.product_id JOIN Orders AS T3 ON T3.order_id = T1.order_id GROUP BY T2.product_name
SELECT order_id , count( product_id) FROM Order_items GROUP BY order_id
SELECT order_id , count( product_id) FROM Order_items GROUP BY order_id
SELECT order_id , sum(product_quantity) FROM Order_items GROUP BY order_id
SELECT order_id , sum(product_quantity) FROM Order_items GROUP BY order_id
SELECT count(*) FROM products WHERE product_id NOT IN ( SELECT product_id FROM Order_items )
SELECT count(*) FROM products WHERE product_id NOT IN ( SELECT product_id FROM Order_items )
SELECT count(*) FROM artist
SELECT count(*) FROM artist
SELECT name , age , country FROM artist ORDER BY Year_Join
SELECT name , age , country FROM artist ORDER BY Year_Join
SELECT country FROM artist
SELECT country FROM artist
SELECT name , year_join FROM artist WHERE country != 'United States'
SELECT name , year_join FROM artist WHERE country != 'United States'
SELECT count(*) FROM artist WHERE age > 46 AND year_join > 1990
SELECT count(*) FROM artist WHERE age > 46 AND year_join > 1990
SELECT avg(age) , min(age) FROM artist WHERE country = 'United States'
SELECT avg(age) , min(age) FROM artist WHERE country = 'United States'
SELECT name FROM artist ORDER BY year_join DESC LIMIT 1
SELECT name FROM artist ORDER BY year_join DESC LIMIT 1
SELECT count(*) FROM exhibition WHERE YEAR >= 2005
SELECT count(*) FROM exhibition WHERE YEAR >= 2005
SELECT theme , YEAR FROM exhibition WHERE ticket_price < 15
SELECT theme , YEAR FROM exhibition WHERE ticket_price < 15
SELECT T2.name , count(*) FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id GROUP BY T1.artist_id
SELECT T2.name , count(*) FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id GROUP BY T1.artist_id
SELECT T2.name , T2.country FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id GROUP BY T1.artist_id ORDER BY count(*) DESC LIMIT 1
SELECT T2.name , T2.country FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id GROUP BY T1.artist_id ORDER BY count(*) DESC LIMIT 1
SELECT name FROM artist WHERE artist_id NOT IN (SELECT artist_id FROM exhibition)
SELECT name FROM artist WHERE artist_id NOT IN (SELECT artist_id FROM exhibition)
SELECT T1.theme , T2.name FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id WHERE T1.ticket_price > (SELECT avg(ticket_price) FROM exhibition)
SELECT T1.theme , T2.name FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id WHERE T1.ticket_price > (SELECT avg(ticket_price) FROM exhibition)
SELECT avg(ticket_price) , min(ticket_price) , max(ticket_price) FROM exhibition WHERE YEAR < 2009
SELECT avg(ticket_price) , min(ticket_price) , max(ticket_price) FROM exhibition WHERE YEAR < 2009
SELECT theme , YEAR FROM exhibition ORDER BY ticket_price DESC
SELECT theme , YEAR FROM exhibition ORDER BY ticket_price DESC
SELECT T2.theme , T1.date , T1.attendance FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T2.year = 2004
SELECT T2.theme , T1.date , T1.attendance FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T2.year = 2004
SELECT name FROM artist EXCEPT SELECT T2.name FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id WHERE T1.year = 2004
SELECT name FROM artist EXCEPT SELECT T2.name FROM exhibition AS T1 JOIN artist AS T2 ON T1.artist_id = T2.artist_id WHERE T1.year = 2004
SELECT T2.theme FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T1.attendance < 100 INTERSECT SELECT T2.theme FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T1.attendance > 500
SELECT T2.theme FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T1.attendance < 100 INTERSECT SELECT T2.theme FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T1.attendance > 500
SELECT count(*) FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T1.attendance > 100 OR T2.ticket_price < 10
SELECT count(*) FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id WHERE T1.attendance > 100 OR T2.ticket_price < 10
SELECT T3.name FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id JOIN artist AS T3 ON T3.artist_id = T2.artist_id GROUP BY T3.artist_id HAVING avg(T1.attendance) > 200
SELECT T3.name FROM exhibition_record AS T1 JOIN exhibition AS T2 ON T1.exhibition_id = T2.exhibition_id JOIN artist AS T3 ON T3.artist_id = T2.artist_id GROUP BY T3.artist_id HAVING avg(T1.attendance) > 200
SELECT count(*) FROM county_public_safety
SELECT count(*) FROM county_public_safety
SELECT Name FROM county_public_safety ORDER BY Population DESC
SELECT Name FROM county_public_safety ORDER BY Population DESC
SELECT Police_force FROM county_public_safety WHERE LOCATION != "East"
SELECT Police_force FROM county_public_safety WHERE LOCATION != "East"
SELECT min(Crime_rate) , max(Crime_rate) FROM county_public_safety
SELECT min(Crime_rate) , max(Crime_rate) FROM county_public_safety
SELECT Crime_rate FROM county_public_safety ORDER BY Police_officers ASC
SELECT Crime_rate FROM county_public_safety ORDER BY Police_officers ASC
SELECT Name FROM city ORDER BY Name ASC
SELECT Name FROM city ORDER BY Name ASC
SELECT Hispanic FROM city WHERE Black > 10
SELECT Hispanic FROM city WHERE Black > 10
SELECT Name FROM county_public_safety ORDER BY Population DESC LIMIT 1
SELECT Name FROM county_public_safety ORDER BY Population DESC LIMIT 1
SELECT Name FROM city ORDER BY White DESC LIMIT 5
SELECT Name FROM city ORDER BY White DESC LIMIT 5
SELECT T1.Name , T2.Name FROM city AS T1 JOIN county_public_safety AS T2 ON T1.County_ID = T2.County_ID
SELECT T1.Name , T2.Name FROM city AS T1 JOIN county_public_safety AS T2 ON T1.County_ID = T2.County_ID
SELECT T1.White , T2.Crime_rate FROM city AS T1 JOIN county_public_safety AS T2 ON T1.County_ID = T2.County_ID
SELECT T1.White , T2.Crime_rate FROM city AS T1 JOIN county_public_safety AS T2 ON T1.County_ID = T2.County_ID
SELECT name FROM city WHERE county_ID = (SELECT county_ID FROM county_public_safety ORDER BY Police_officers DESC LIMIT 1)
SELECT name FROM city WHERE county_ID = (SELECT county_ID FROM county_public_safety ORDER BY Police_officers DESC LIMIT 1)
SELECT count(*) FROM city WHERE county_ID IN (SELECT county_ID FROM county_public_safety WHERE population > 20000)
SELECT count(*) FROM city WHERE county_ID IN (SELECT county_ID FROM county_public_safety WHERE population > 20000)
SELECT T2.Crime_rate FROM city AS T1 JOIN county_public_safety AS T2 ON T1.County_ID = T2.County_ID WHERE T1.White > 90
SELECT T2.Crime_rate FROM city AS T1 JOIN county_public_safety AS T2 ON T1.County_ID = T2.County_ID WHERE T1.White > 90
SELECT Police_force , COUNT(*) FROM county_public_safety GROUP BY Police_force
SELECT Police_force , COUNT(*) FROM county_public_safety GROUP BY Police_force
SELECT LOCATION FROM county_public_safety GROUP BY LOCATION ORDER BY COUNT(*) DESC LIMIT 1
SELECT LOCATION FROM county_public_safety GROUP BY LOCATION ORDER BY COUNT(*) DESC LIMIT 1
SELECT Name FROM county_public_safety WHERE County_ID NOT IN (SELECT County_ID FROM city)
SELECT Name FROM county_public_safety WHERE County_ID NOT IN (SELECT County_ID FROM city)
SELECT Police_force FROM county_public_safety WHERE LOCATION = "East" INTERSECT SELECT Police_force FROM county_public_safety WHERE LOCATION = "West"
SELECT Police_force FROM county_public_safety WHERE LOCATION = "East" INTERSECT SELECT Police_force FROM county_public_safety WHERE LOCATION = "West"
SELECT name FROM city WHERE county_id IN (SELECT county_id FROM county_public_safety WHERE Crime_rate < 100)
SELECT name FROM city WHERE county_id IN (SELECT county_id FROM county_public_safety WHERE Crime_rate < 100)
SELECT Case_burden FROM county_public_safety ORDER BY Population DESC
SELECT Case_burden FROM county_public_safety ORDER BY Population DESC
SELECT count(*) FROM bank
SELECT count(*) FROM bank
SELECT sum(no_of_customers) FROM bank
SELECT sum(no_of_customers) FROM bank
SELECT sum(no_of_customers) FROM bank WHERE city = 'New York City'
SELECT sum(no_of_customers) FROM bank WHERE city = 'New York City'
SELECT avg(no_of_customers) FROM bank WHERE state = 'Utah'
SELECT avg(no_of_customers) FROM bank WHERE state = 'Utah'
SELECT avg(no_of_customers) FROM bank
SELECT avg(no_of_customers) FROM bank
SELECT city , state FROM bank WHERE bname = 'morningside'
SELECT city , state FROM bank WHERE bname = 'morningside'
SELECT bname FROM bank WHERE state = 'New York'
SELECT bname FROM bank WHERE state = 'New York'
SELECT cust_name FROM customer ORDER BY acc_bal
SELECT cust_name FROM customer ORDER BY acc_bal
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name ORDER BY sum(T2.amount)
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name ORDER BY sum(T2.amount)
SELECT state , acc_type , credit_score FROM customer WHERE no_of_loans = 0
SELECT state , acc_type , credit_score FROM customer WHERE no_of_loans = 0
SELECT count( city) FROM bank
SELECT count( city) FROM bank
SELECT count( state) FROM bank
SELECT count( state) FROM bank
SELECT count( acc_type) FROM customer
SELECT count( acc_type) FROM customer
SELECT cust_name , acc_bal FROM customer WHERE cust_name LIKE '%a%'
SELECT cust_name , acc_bal FROM customer WHERE cust_name LIKE '%a%'
SELECT sum(acc_bal) FROM customer WHERE state = 'Utah' OR state = 'Texas'
SELECT sum(acc_bal) FROM customer WHERE state = 'Utah' OR state = 'Texas'
SELECT cust_name FROM customer WHERE acc_type = 'saving' INTERSECT SELECT cust_name FROM customer WHERE acc_type = 'checking'
SELECT cust_name FROM customer WHERE acc_type = 'saving' INTERSECT SELECT cust_name FROM customer WHERE acc_type = 'checking'
SELECT cust_name FROM customer EXCEPT SELECT cust_name FROM customer WHERE acc_type = 'saving'
SELECT cust_name FROM customer EXCEPT SELECT cust_name FROM customer WHERE acc_type = 'saving'
SELECT cust_name FROM customer EXCEPT SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE T2.loan_type = 'Mortgages'
SELECT cust_name FROM customer EXCEPT SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE T2.loan_type = 'Mortgages'
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE loan_type = 'Mortgages' INTERSECT SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE loan_type = 'Auto'
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE loan_type = 'Mortgages' INTERSECT SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE loan_type = 'Auto'
SELECT cust_name FROM customer WHERE credit_score < (SELECT avg(credit_score) FROM customer)
SELECT cust_name FROM customer WHERE credit_score < (SELECT avg(credit_score) FROM customer)
SELECT bname FROM bank ORDER BY no_of_customers DESC LIMIT 1
SELECT bname FROM bank ORDER BY no_of_customers DESC LIMIT 1
SELECT cust_name FROM customer ORDER BY credit_score LIMIT 1
SELECT cust_name FROM customer ORDER BY credit_score LIMIT 1
SELECT cust_name , acc_type , acc_bal FROM customer ORDER BY credit_score DESC LIMIT 1
SELECT cust_name , acc_type , acc_bal FROM customer ORDER BY credit_score DESC LIMIT 1
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name ORDER BY sum(T2.amount) DESC LIMIT 1
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name ORDER BY sum(T2.amount) DESC LIMIT 1
SELECT state FROM bank GROUP BY state ORDER BY sum(no_of_customers) DESC LIMIT 1
SELECT state FROM bank GROUP BY state ORDER BY sum(no_of_customers) DESC LIMIT 1
SELECT avg(acc_bal) , acc_type FROM customer WHERE credit_score < 50 GROUP BY acc_type
SELECT avg(acc_bal) , acc_type FROM customer WHERE credit_score < 50 GROUP BY acc_type
SELECT sum(acc_bal) , state FROM customer WHERE credit_score > 100 GROUP BY state
SELECT sum(acc_bal) , state FROM customer WHERE credit_score > 100 GROUP BY state
SELECT sum(amount) , T1.bname FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id GROUP BY T1.bname
SELECT sum(amount) , T1.bname FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id GROUP BY T1.bname
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name HAVING count(*) > 1
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name HAVING count(*) > 1
SELECT T1.cust_name , T1.acc_type FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name HAVING sum(T2.amount) > 5000
SELECT T1.cust_name , T1.acc_type FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id GROUP BY T1.cust_name HAVING sum(T2.amount) > 5000
SELECT T1.bname FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id GROUP BY T1.bname ORDER BY sum(T2.amount) DESC LIMIT 1
SELECT T1.bname FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id GROUP BY T1.bname ORDER BY sum(T2.amount) DESC LIMIT 1
SELECT T2.bname FROM loan AS T1 JOIN bank AS T2 ON T1.branch_id = T2.branch_id JOIN customer AS T3 ON T1.cust_id = T3.cust_id WHERE T3.credit_score < 100 GROUP BY T2.bname ORDER BY sum(T1.amount) DESC LIMIT 1
SELECT T2.bname FROM loan AS T1 JOIN bank AS T2 ON T1.branch_id = T2.branch_id JOIN customer AS T3 ON T1.cust_id = T3.cust_id WHERE T3.credit_score < 100 GROUP BY T2.bname ORDER BY sum(T1.amount) DESC LIMIT 1
SELECT T1.bname FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id
SELECT T1.bname FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id
SELECT T1.cust_name , T1.credit_score FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id
SELECT T1.cust_name , T1.credit_score FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE amount > 3000
SELECT T1.cust_name FROM customer AS T1 JOIN loan AS T2 ON T1.cust_id = T2.cust_id WHERE amount > 3000
SELECT T1.bname , T1.city FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id WHERE T2.loan_type = 'Business'
SELECT T1.bname , T1.city FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id WHERE T2.loan_type = 'Business'
SELECT T2.bname FROM loan AS T1 JOIN bank AS T2 ON T1.branch_id = T2.branch_id JOIN customer AS T3 ON T1.cust_id = T3.cust_id WHERE T3.credit_score < 100
SELECT T2.bname FROM loan AS T1 JOIN bank AS T2 ON T1.branch_id = T2.branch_id JOIN customer AS T3 ON T1.cust_id = T3.cust_id WHERE T3.credit_score < 100
SELECT sum(T2.amount) FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id WHERE T1.state = 'New York'
SELECT sum(T2.amount) FROM bank AS T1 JOIN loan AS T2 ON T1.branch_id = T2.branch_id WHERE T1.state = 'New York'
SELECT avg(credit_score) FROM customer WHERE cust_id IN (SELECT cust_id FROM loan)
SELECT avg(credit_score) FROM customer WHERE cust_id IN (SELECT cust_id FROM loan)
SELECT avg(credit_score) FROM customer WHERE cust_id NOT IN (SELECT cust_id FROM loan)
SELECT avg(credit_score) FROM customer WHERE cust_id NOT IN (SELECT cust_id FROM loan)
SELECT count(*) FROM club
SELECT count(*) FROM club
SELECT Region FROM club ORDER BY Region ASC
SELECT Region FROM club ORDER BY Region ASC
SELECT avg(Gold) FROM club_rank
SELECT avg(Gold) FROM club_rank
SELECT Competition_type , Country FROM competition
SELECT Competition_type , Country FROM competition
SELECT YEAR FROM competition WHERE Competition_type != "Tournament"
SELECT YEAR FROM competition WHERE Competition_type != "Tournament"
SELECT max(Silver) , min(Silver) FROM club_rank
SELECT max(Silver) , min(Silver) FROM club_rank
SELECT count(*) FROM club_rank WHERE Total < 10
SELECT count(*) FROM club_rank WHERE Total < 10
SELECT name FROM club ORDER BY Start_year ASC
SELECT name FROM club ORDER BY Start_year ASC
SELECT name FROM club ORDER BY name DESC
SELECT name FROM club ORDER BY name DESC
SELECT T1.name , T2.Player_id FROM club AS T1 JOIN player AS T2 ON T1.Club_ID = T2.Club_ID
SELECT T1.name , T2.Player_id FROM club AS T1 JOIN player AS T2 ON T1.Club_ID = T2.Club_ID
SELECT T1.name FROM club AS T1 JOIN player AS T2 ON T1.Club_ID = T2.Club_ID WHERE T2.Position = "Right Wing"
SELECT T1.name FROM club AS T1 JOIN player AS T2 ON T1.Club_ID = T2.Club_ID WHERE T2.Position = "Right Wing"
SELECT avg(T2.Points) FROM club AS T1 JOIN player AS T2 ON T1.Club_ID = T2.Club_ID WHERE T1.name = "AIB"
SELECT avg(T2.Points) FROM club AS T1 JOIN player AS T2 ON T1.Club_ID = T2.Club_ID WHERE T1.name = "AIB"
SELECT POSITION , avg(Points) FROM player GROUP BY POSITION
SELECT POSITION , avg(Points) FROM player GROUP BY POSITION
SELECT POSITION FROM player GROUP BY name HAVING avg(Points) >= 20
SELECT POSITION FROM player GROUP BY name HAVING avg(Points) >= 20
SELECT Competition_type , COUNT(*) FROM competition GROUP BY Competition_type
SELECT Competition_type , COUNT(*) FROM competition GROUP BY Competition_type
SELECT Competition_type FROM competition GROUP BY Competition_type ORDER BY COUNT(*) DESC LIMIT 1
SELECT Competition_type FROM competition GROUP BY Competition_type ORDER BY COUNT(*) DESC LIMIT 1
SELECT Competition_type FROM competition GROUP BY Competition_type HAVING COUNT(*) <= 5
SELECT Competition_type FROM competition GROUP BY Competition_type HAVING COUNT(*) <= 5
SELECT name FROM CLub WHERE Club_ID NOT IN (SELECT Club_ID FROM player)
SELECT name FROM CLub WHERE Club_ID NOT IN (SELECT Club_ID FROM player)
SELECT POSITION FROM player WHERE Points > 20 INTERSECT SELECT POSITION FROM player WHERE Points < 10
SELECT POSITION FROM player WHERE Points > 20 INTERSECT SELECT POSITION FROM player WHERE Points < 10
SELECT sum(Points) FROM player
SELECT sum(Points) FROM player
SELECT count( POSITION) FROM player
SELECT count( POSITION) FROM player
SELECT name FROM player WHERE points > (SELECT avg(points) FROM player)
SELECT name FROM player WHERE points > (SELECT avg(points) FROM player)
SELECT count(*) , POSITION FROM player WHERE points < 30 GROUP BY POSITION
SELECT count(*) , POSITION FROM player WHERE points < 30 GROUP BY POSITION
SELECT country FROM competition WHERE competition_type = 'Tournament' GROUP BY country ORDER BY count(*) DESC LIMIT 1
SELECT country FROM competition WHERE competition_type = 'Tournament' GROUP BY country ORDER BY count(*) DESC LIMIT 1
SELECT country FROM competition WHERE competition_type = 'Friendly' INTERSECT SELECT country FROM competition WHERE competition_type = 'Tournament'
SELECT country FROM competition WHERE competition_type = 'Friendly' INTERSECT SELECT country FROM competition WHERE competition_type = 'Tournament'
SELECT country FROM competition EXCEPT SELECT country FROM competition WHERE competition_type = 'Friendly'
SELECT country FROM competition EXCEPT SELECT country FROM competition WHERE competition_type = 'Friendly'
SELECT count(*) FROM camera_lens WHERE focal_length_mm > 15
SELECT brand , name FROM camera_lens ORDER BY max_aperture DESC
SELECT id , color , name FROM photos
SELECT max(height) , avg(height) FROM mountain
SELECT avg(prominence) FROM mountain WHERE country = 'Morocco'
SELECT name , height , prominence FROM mountain WHERE range != 'Aberdare Range'
SELECT T1.id , T1.name FROM mountain AS T1 JOIN photos AS T2 ON T1.id = T2.mountain_id WHERE T1.height > 4000
SELECT T1.id , T1.name FROM mountain AS T1 JOIN photos AS T2 ON T1.id = T2.mountain_id GROUP BY T1.id HAVING count(*) >= 2
SELECT T2.name FROM photos AS T1 JOIN camera_lens AS T2 ON T1.camera_lens_id = T2.id GROUP BY T2.id ORDER BY count(*) DESC LIMIT 1
SELECT T1.name FROM camera_lens AS T1 JOIN photos AS T2 ON T2.camera_lens_id = T1.id WHERE T1.brand = 'Sigma' OR T1.brand = 'Olympus'
SELECT count( brand) FROM camera_lens
SELECT count(*) FROM camera_lens WHERE id NOT IN ( SELECT camera_lens_id FROM photos )
SELECT count( T2.camera_lens_id) FROM mountain AS T1 JOIN photos AS T2 ON T1.id = T2.mountain_id WHERE T1.country = 'Ethiopia'
SELECT T3.brand FROM mountain AS T1 JOIN photos AS T2 ON T1.id = T2.mountain_id JOIN camera_lens AS T3 ON T2.camera_lens_id = T3.id WHERE T1.range = 'Toubkal Atlas' INTERSECT SELECT T3.brand FROM mountain AS T1 JOIN photos AS T2 ON T1.id = T2.mountain_id JOIN camera_lens AS T3 ON T2.camera_lens_id = T3.id WHERE T1.range = 'Lasta Massif'
SELECT name , prominence FROM mountain EXCEPT SELECT T1.name , T1.prominence FROM mountain AS T1 JOIN photos AS T2 ON T1.id = T2.mountain_id JOIN camera_lens AS T3 ON T2.camera_lens_id = T3.id WHERE T3.brand = 'Sigma'
SELECT name FROM camera_lens WHERE name LIKE "%Digital%"
SELECT T1.name , count(*) FROM camera_lens AS T1 JOIN photos AS T2 ON T1.id = T2.camera_lens_id GROUP BY T1.id ORDER BY count(*)
SELECT count(*) FROM school
SELECT count(*) FROM school
SELECT LOCATION FROM school ORDER BY Enrollment ASC
SELECT LOCATION FROM school ORDER BY Enrollment ASC
SELECT LOCATION FROM school ORDER BY Founded DESC
SELECT LOCATION FROM school ORDER BY Founded DESC
SELECT Enrollment FROM school WHERE Denomination != "Catholic"
SELECT Enrollment FROM school WHERE Denomination != "Catholic"
SELECT avg(Enrollment) FROM school
SELECT avg(Enrollment) FROM school
SELECT Team FROM player ORDER BY Team ASC
SELECT Team FROM player ORDER BY Team ASC
SELECT count( POSITION) FROM player
SELECT count( POSITION) FROM player
SELECT Team FROM player ORDER BY Age DESC LIMIT 1
SELECT Team FROM player ORDER BY Age DESC LIMIT 1
SELECT Team FROM player ORDER BY Age DESC LIMIT 5
SELECT Team FROM player ORDER BY Age DESC LIMIT 5
SELECT T1.Team , T2.Location FROM player AS T1 JOIN school AS T2 ON T1.School_ID = T2.School_ID
SELECT T1.Team , T2.Location FROM player AS T1 JOIN school AS T2 ON T1.School_ID = T2.School_ID
SELECT T2.Location FROM player AS T1 JOIN school AS T2 ON T1.School_ID = T2.School_ID GROUP BY T1.School_ID HAVING COUNT(*) > 1
SELECT T2.Location FROM player AS T1 JOIN school AS T2 ON T1.School_ID = T2.School_ID GROUP BY T1.School_ID HAVING COUNT(*) > 1
SELECT T2.Denomination FROM player AS T1 JOIN school AS T2 ON T1.School_ID = T2.School_ID GROUP BY T1.School_ID ORDER BY COUNT(*) DESC LIMIT 1
SELECT T2.Denomination FROM player AS T1 JOIN school AS T2 ON T1.School_ID = T2.School_ID GROUP BY T1.School_ID ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Location , T2.Nickname FROM school AS T1 JOIN school_details AS T2 ON T1.School_ID = T2.School_ID
SELECT T1.Location , T2.Nickname FROM school AS T1 JOIN school_details AS T2 ON T1.School_ID = T2.School_ID
SELECT Denomination , COUNT(*) FROM school GROUP BY Denomination
SELECT Denomination , COUNT(*) FROM school GROUP BY Denomination
SELECT Denomination , COUNT(*) FROM school GROUP BY Denomination ORDER BY COUNT(*) DESC
SELECT Denomination , COUNT(*) FROM school GROUP BY Denomination ORDER BY COUNT(*) DESC
SELECT School_Colors FROM school ORDER BY Enrollment DESC LIMIT 1
SELECT School_Colors FROM school ORDER BY Enrollment DESC LIMIT 1
SELECT LOCATION FROM school WHERE School_ID NOT IN (SELECT School_ID FROM Player)
SELECT LOCATION FROM school WHERE School_ID NOT IN (SELECT School_ID FROM Player)
SELECT Denomination FROM school WHERE Founded < 1890 INTERSECT SELECT Denomination FROM school WHERE Founded > 1900
SELECT Denomination FROM school WHERE Founded < 1890 INTERSECT SELECT Denomination FROM school WHERE Founded > 1900
SELECT Nickname FROM school_details WHERE Division != "Division 1"
SELECT Nickname FROM school_details WHERE Division != "Division 1"
SELECT Denomination FROM school GROUP BY Denomination HAVING COUNT(*) > 1
SELECT Denomination FROM school GROUP BY Denomination HAVING COUNT(*) > 1
SELECT count(*) FROM HOTELS
SELECT count(*) FROM HOTELS
SELECT price_range FROM HOTELS
SELECT price_range FROM HOTELS
SELECT Location_Name FROM LOCATIONS
SELECT Location_Name FROM LOCATIONS
SELECT Name , Other_Details FROM Staff
SELECT Name , Other_Details FROM Staff
SELECT Tourist_Details FROM VISITORS
SELECT Tourist_Details FROM VISITORS
SELECT price_range FROM HOTELS WHERE star_rating_code = "5"
SELECT price_range FROM HOTELS WHERE star_rating_code = "5"
SELECT avg(price_range) FROM HOTELS WHERE star_rating_code = "5" AND pets_allowed_yn = 1
SELECT avg(price_range) FROM HOTELS WHERE star_rating_code = "5" AND pets_allowed_yn = 1
SELECT Address FROM LOCATIONS WHERE Location_Name = "UK Gallery"
SELECT Address FROM LOCATIONS WHERE Location_Name = "UK Gallery"
SELECT Other_Details FROM LOCATIONS WHERE Location_Name = "UK Gallery"
SELECT Other_Details FROM LOCATIONS WHERE Location_Name = "UK Gallery"
SELECT Location_Name FROM LOCATIONS WHERE Location_Name LIKE "%film%"
SELECT Location_Name FROM LOCATIONS WHERE Location_Name LIKE "%film%"
SELECT count( Name) FROM PHOTOS
SELECT count( Name) FROM PHOTOS
SELECT Visit_Date FROM VISITS
SELECT Visit_Date FROM VISITS
SELECT Name FROM TOURIST_ATTRACTIONS WHERE How_to_Get_There = "bus"
SELECT Name FROM TOURIST_ATTRACTIONS WHERE How_to_Get_There = "bus"
SELECT Name , Opening_Hours FROM TOURIST_ATTRACTIONS WHERE How_to_Get_There = "bus" OR How_to_Get_There = "walk"
SELECT Name , Opening_Hours FROM TOURIST_ATTRACTIONS WHERE How_to_Get_There = "bus" OR How_to_Get_There = "walk"
SELECT T2.star_rating_description FROM HOTELS AS T1 JOIN Ref_Hotel_Star_Ratings AS T2 ON T1.star_rating_code = T2.star_rating_code WHERE T1.price_range > 10000
SELECT T2.star_rating_description FROM HOTELS AS T1 JOIN Ref_Hotel_Star_Ratings AS T2 ON T1.star_rating_code = T2.star_rating_code WHERE T1.price_range > 10000
SELECT T1.Museum_Details , T2.Opening_Hours FROM MUSEUMS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Museum_ID = T2.Tourist_Attraction_ID
SELECT T1.Museum_Details , T2.Opening_Hours FROM MUSEUMS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Museum_ID = T2.Tourist_Attraction_ID
SELECT T2.Name FROM PHOTOS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID WHERE T1.Name = "game1"
SELECT T2.Name FROM PHOTOS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID WHERE T1.Name = "game1"
SELECT T1.Name , T1.Description FROM PHOTOS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID WHERE T2.Name = "film festival"
SELECT T1.Name , T1.Description FROM PHOTOS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID WHERE T2.Name = "film festival"
SELECT T1.Royal_Family_Details , T2.How_to_Get_There FROM ROYAL_FAMILY AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Royal_Family_ID = T2.Tourist_Attraction_ID
SELECT T1.Royal_Family_Details , T2.How_to_Get_There FROM ROYAL_FAMILY AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Royal_Family_ID = T2.Tourist_Attraction_ID
SELECT T1.Shop_Details FROM SHOPS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Shop_ID = T2.Tourist_Attraction_ID WHERE T2.How_to_Get_There = "walk"
SELECT T1.Shop_Details FROM SHOPS AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Shop_ID = T2.Tourist_Attraction_ID WHERE T2.How_to_Get_There = "walk"
SELECT T1.Name FROM STAFF AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID WHERE T2.Name = "US museum"
SELECT T1.Name FROM STAFF AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID WHERE T2.Name = "US museum"
SELECT T1.Market_Details FROM Street_Markets AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Market_ID = T2.Tourist_Attraction_ID WHERE T2.How_to_Get_There = "walk" OR T2.How_to_Get_There = "bus"
SELECT T1.Market_Details FROM Street_Markets AS T1 JOIN TOURIST_ATTRACTIONS AS T2 ON T1.Market_ID = T2.Tourist_Attraction_ID WHERE T2.How_to_Get_There = "walk" OR T2.How_to_Get_There = "bus"
SELECT T2.Visit_Date , T2.Visit_Details FROM VISITORS AS T1 JOIN VISITS AS T2 ON T1.Tourist_ID = T2.Tourist_ID WHERE T1.Tourist_Details = "Vincent"
SELECT T2.Visit_Date , T2.Visit_Details FROM VISITORS AS T1 JOIN VISITS AS T2 ON T1.Tourist_ID = T2.Tourist_ID WHERE T1.Tourist_Details = "Vincent"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID JOIN VISITORS AS T3 ON T2.Tourist_ID = T3.Tourist_ID WHERE T3.Tourist_Details = "Vincent"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID JOIN VISITORS AS T3 ON T2.Tourist_ID = T3.Tourist_ID WHERE T3.Tourist_Details = "Vincent"
SELECT T1.Name , T3.Visit_Date FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Vincent" OR T2.Tourist_Details = "Vivian"
SELECT T1.Name , T3.Visit_Date FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Vincent" OR T2.Tourist_Details = "Vivian"
SELECT star_rating_code , avg(price_range) FROM HOTELS GROUP BY star_rating_code
SELECT star_rating_code , avg(price_range) FROM HOTELS GROUP BY star_rating_code
SELECT pets_allowed_yn , avg(price_range) FROM HOTELS GROUP BY pets_allowed_yn
SELECT pets_allowed_yn , avg(price_range) FROM HOTELS GROUP BY pets_allowed_yn
SELECT hotel_id , star_rating_code FROM HOTELS ORDER BY price_range ASC
SELECT hotel_id , star_rating_code FROM HOTELS ORDER BY price_range ASC
SELECT other_hotel_details FROM HOTELS ORDER BY price_range DESC LIMIT 3
SELECT other_hotel_details FROM HOTELS ORDER BY price_range DESC LIMIT 3
SELECT other_hotel_details , star_rating_code FROM HOTELS ORDER BY price_range ASC LIMIT 3
SELECT other_hotel_details , star_rating_code FROM HOTELS ORDER BY price_range ASC LIMIT 3
SELECT How_to_Get_There FROM Tourist_Attractions GROUP BY How_to_Get_There ORDER BY COUNT(*) DESC LIMIT 1
SELECT How_to_Get_There FROM Tourist_Attractions GROUP BY How_to_Get_There ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Attraction_Type_Description , T2.Attraction_Type_Code FROM Ref_Attraction_Types AS T1 JOIN Tourist_Attractions AS T2 ON T1.Attraction_Type_Code = T2.Attraction_Type_Code GROUP BY T2.Attraction_Type_Code ORDER BY COUNT(*) DESC LIMIT 1
SELECT T1.Attraction_Type_Description , T2.Attraction_Type_Code FROM Ref_Attraction_Types AS T1 JOIN Tourist_Attractions AS T2 ON T1.Attraction_Type_Code = T2.Attraction_Type_Code GROUP BY T2.Attraction_Type_Code ORDER BY COUNT(*) DESC LIMIT 1
SELECT How_to_Get_There , COUNT(*) FROM Tourist_Attractions GROUP BY How_to_Get_There
SELECT How_to_Get_There , COUNT(*) FROM Tourist_Attractions GROUP BY How_to_Get_There
SELECT T1.Name , T2.Tourist_Attraction_ID , COUNT(*) FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID GROUP BY T2.Tourist_Attraction_ID
SELECT T1.Name , T2.Tourist_Attraction_ID , COUNT(*) FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID GROUP BY T2.Tourist_Attraction_ID
SELECT T1.Name , T2.Tourist_Attraction_ID FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID GROUP BY T2.Tourist_Attraction_ID HAVING count(*) >= 2
SELECT T1.Name , T2.Tourist_Attraction_ID FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID GROUP BY T2.Tourist_Attraction_ID HAVING count(*) >= 2
SELECT T1.Name , T1.Tourist_Attraction_ID FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID GROUP BY T2.Tourist_Attraction_ID HAVING count(*) <= 1
SELECT T1.Name , T1.Tourist_Attraction_ID FROM Tourist_Attractions AS T1 JOIN VISITS AS T2 ON T1.Tourist_Attraction_ID = T2.Tourist_Attraction_ID GROUP BY T2.Tourist_Attraction_ID HAVING count(*) <= 1
SELECT T2.Name FROM Locations AS T1 JOIN Tourist_Attractions AS T2 ON T1.Location_ID = T2.Location_ID WHERE T1.Address = "660 Shea Crescent" OR T2.How_to_Get_There = "walk"
SELECT T2.Name FROM Locations AS T1 JOIN Tourist_Attractions AS T2 ON T1.Location_ID = T2.Location_ID WHERE T1.Address = "660 Shea Crescent" OR T2.How_to_Get_There = "walk"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN Tourist_Attraction_Features AS T2 ON T1.tourist_attraction_id = T2.tourist_attraction_id JOIN Features AS T3 ON T2.Feature_ID = T3.Feature_ID WHERE T3.feature_Details = 'park' UNION SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN Tourist_Attraction_Features AS T2 ON T1.tourist_attraction_id = T2.tourist_attraction_id JOIN Features AS T3 ON T2.Feature_ID = T3.Feature_ID WHERE T3.feature_Details = 'shopping'
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN Tourist_Attraction_Features AS T2 ON T1.tourist_attraction_id = T2.tourist_attraction_id JOIN Features AS T3 ON T2.Feature_ID = T3.Feature_ID WHERE T3.feature_Details = 'park' UNION SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN Tourist_Attraction_Features AS T2 ON T1.tourist_attraction_id = T2.tourist_attraction_id JOIN Features AS T3 ON T2.Feature_ID = T3.Feature_ID WHERE T3.feature_Details = 'shopping'
SELECT T2.Name FROM Locations AS T1 JOIN Tourist_Attractions AS T2 ON T1.Location_ID = T2.Location_ID WHERE T1.Address = "254 Ottilie Junction" OR T2.How_to_Get_There = "bus"
SELECT T2.Name FROM Locations AS T1 JOIN Tourist_Attractions AS T2 ON T1.Location_ID = T2.Location_ID WHERE T1.Address = "254 Ottilie Junction" OR T2.How_to_Get_There = "bus"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Vincent" INTERSECT SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Marcelle"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Vincent" INTERSECT SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Marcelle"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Alison" EXCEPT SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Rosalind"
SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Alison" EXCEPT SELECT T1.Name FROM Tourist_Attractions AS T1 JOIN VISITORS AS T2 JOIN VISITS AS T3 ON T1.Tourist_Attraction_ID = T3.Tourist_Attraction_ID AND T2.Tourist_ID = T3.Tourist_ID WHERE T2.Tourist_Details = "Rosalind"
SELECT count(*) FROM Visitors WHERE Tourist_ID NOT IN ( SELECT Tourist_ID FROM Visits )
SELECT count(*) FROM Visitors WHERE Tourist_ID NOT IN ( SELECT Tourist_ID FROM Visits )
SELECT count( rank) FROM captain
SELECT count( rank) FROM captain
SELECT count(*) , rank FROM captain GROUP BY rank
SELECT count(*) , rank FROM captain GROUP BY rank
SELECT count(*) , rank FROM captain WHERE age < 50 GROUP BY rank
SELECT count(*) , rank FROM captain WHERE age < 50 GROUP BY rank
SELECT name FROM captain ORDER BY age DESC
SELECT name FROM captain ORDER BY age DESC
SELECT name , CLASS , rank FROM captain
SELECT name , CLASS , rank FROM captain
SELECT rank FROM captain GROUP BY rank ORDER BY count(*) DESC LIMIT 1
SELECT rank FROM captain GROUP BY rank ORDER BY count(*) DESC LIMIT 1
SELECT CLASS FROM captain GROUP BY CLASS HAVING count(*) > 2
SELECT CLASS FROM captain GROUP BY CLASS HAVING count(*) > 2
SELECT name FROM captain WHERE rank = 'Midshipman' OR rank = 'Lieutenant'
SELECT name FROM captain WHERE rank = 'Midshipman' OR rank = 'Lieutenant'
SELECT avg(age) , min(age) , CLASS FROM captain GROUP BY CLASS
SELECT avg(age) , min(age) , CLASS FROM captain GROUP BY CLASS
SELECT rank FROM captain WHERE CLASS = 'Cutter' INTERSECT SELECT rank FROM captain WHERE CLASS = 'Armed schooner'
SELECT rank FROM captain WHERE CLASS = 'Cutter' INTERSECT SELECT rank FROM captain WHERE CLASS = 'Armed schooner'
SELECT rank FROM captain EXCEPT SELECT rank FROM captain WHERE CLASS = 'Third-rate ship of the line'
SELECT rank FROM captain EXCEPT SELECT rank FROM captain WHERE CLASS = 'Third-rate ship of the line'
SELECT name FROM captain ORDER BY age LIMIT 1
SELECT name FROM captain ORDER BY age LIMIT 1
SELECT count(*) FROM ship
SELECT count(*) FROM ship
SELECT name , TYPE , flag FROM ship ORDER BY built_year DESC LIMIT 1
SELECT name , TYPE , flag FROM ship ORDER BY built_year DESC LIMIT 1
SELECT count(*) , flag FROM ship GROUP BY flag
SELECT count(*) , flag FROM ship GROUP BY flag
SELECT flag FROM ship GROUP BY flag ORDER BY count(*) DESC LIMIT 1
SELECT flag FROM ship GROUP BY flag ORDER BY count(*) DESC LIMIT 1
SELECT name FROM ship ORDER BY built_year , CLASS
SELECT name FROM ship ORDER BY built_year , CLASS
SELECT TYPE FROM ship WHERE flag = 'Panama' INTERSECT SELECT TYPE FROM ship WHERE flag = 'Malta'
SELECT TYPE FROM ship WHERE flag = 'Panama' INTERSECT SELECT TYPE FROM ship WHERE flag = 'Malta'
SELECT built_year FROM ship GROUP BY built_year ORDER BY count(*) DESC LIMIT 1
SELECT built_year FROM ship GROUP BY built_year ORDER BY count(*) DESC LIMIT 1
SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id GROUP BY t2.ship_id HAVING count(*) > 1
SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id GROUP BY t2.ship_id HAVING count(*) > 1
SELECT name , CLASS FROM ship WHERE ship_id NOT IN (SELECT ship_id FROM captain)
SELECT name , CLASS FROM ship WHERE ship_id NOT IN (SELECT ship_id FROM captain)
SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id ORDER BY t2.age LIMIT 1
SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id ORDER BY t2.age LIMIT 1
SELECT name , flag FROM ship WHERE ship_id NOT IN (SELECT ship_id FROM captain WHERE rank = 'Midshipman')
SELECT name , flag FROM ship WHERE ship_id NOT IN (SELECT ship_id FROM captain WHERE rank = 'Midshipman')
SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id WHERE t2.rank = 'Midshipman' INTERSECT SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id WHERE t2.rank = 'Lieutenant'
SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id WHERE t2.rank = 'Midshipman' INTERSECT SELECT t1.name FROM ship AS t1 JOIN captain AS t2 ON t1.ship_id = t2.ship_id WHERE t2.rank = 'Lieutenant'
SELECT name FROM airports WHERE city = 'Goroka'
SELECT name FROM airports WHERE city = 'Goroka'
SELECT name , city , country , elevation FROM airports WHERE city = 'New York'
SELECT name , city , country , elevation FROM airports WHERE city = 'New York'
SELECT count(*) FROM airlines
SELECT count(*) FROM airlines
SELECT count(*) FROM airlines WHERE country = 'Russia'
SELECT count(*) FROM airlines WHERE country = 'Russia'
SELECT max(elevation) FROM airports WHERE country = 'Iceland'
SELECT max(elevation) FROM airports WHERE country = 'Iceland'
SELECT name FROM airports WHERE country = 'Cuba' OR country = 'Argentina'
SELECT name FROM airports WHERE country = 'Cuba' OR country = 'Argentina'
SELECT country FROM airlines WHERE name LIKE 'Orbit%'
SELECT country FROM airlines WHERE name LIKE 'Orbit%'
SELECT name FROM airports WHERE elevation BETWEEN -50 AND 50
SELECT name FROM airports WHERE elevation BETWEEN -50 AND 50
SELECT country FROM airports ORDER BY elevation DESC LIMIT 1
SELECT country FROM airports ORDER BY elevation DESC LIMIT 1
SELECT count(*) FROM airports WHERE name LIKE '%International%'
SELECT count(*) FROM airports WHERE name LIKE '%International%'
SELECT count( city) FROM airports WHERE country = 'Greenland'
SELECT count( city) FROM airports WHERE country = 'Greenland'
SELECT count(*) FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid WHERE T1.name = 'American Airlines'
SELECT count(*) FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid WHERE T1.name = 'American Airlines'
SELECT count(*) FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid WHERE country = 'Canada'
SELECT count(*) FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid WHERE country = 'Canada'
SELECT name , city , country FROM airports ORDER BY elevation LIMIT 1
SELECT name , city , country FROM airports ORDER BY elevation LIMIT 1
SELECT name , city , country FROM airports ORDER BY elevation DESC LIMIT 1
SELECT name , city , country FROM airports ORDER BY elevation DESC LIMIT 1
SELECT T1.name , T1.city , T2.dst_apid FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid GROUP BY T2.dst_apid ORDER BY count(*) DESC LIMIT 1
SELECT T1.name , T1.city , T2.dst_apid FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid GROUP BY T2.dst_apid ORDER BY count(*) DESC LIMIT 1
SELECT T1.name , T2.alid FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid GROUP BY T2.alid ORDER BY count(*) DESC LIMIT 10
SELECT T1.name , T2.alid FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid GROUP BY T2.alid ORDER BY count(*) DESC LIMIT 10
SELECT T1.name , T1.city , T2.src_apid FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid GROUP BY T2.src_apid ORDER BY count(*) DESC LIMIT 1
SELECT T1.name , T1.city , T2.src_apid FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid GROUP BY T2.src_apid ORDER BY count(*) DESC LIMIT 1
SELECT count( dst_apid) FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid WHERE T1.name = 'American Airlines'
SELECT count( dst_apid) FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid WHERE T1.name = 'American Airlines'
SELECT country FROM airlines GROUP BY country ORDER BY count(*) DESC LIMIT 1
SELECT country FROM airlines GROUP BY country ORDER BY count(*) DESC LIMIT 1
SELECT country FROM airlines WHERE active = 'Y' GROUP BY country ORDER BY count(*) DESC LIMIT 1
SELECT country FROM airlines WHERE active = 'Y' GROUP BY country ORDER BY count(*) DESC LIMIT 1
SELECT country , count(*) FROM airlines GROUP BY country ORDER BY count(*) DESC
SELECT country , count(*) FROM airlines GROUP BY country ORDER BY count(*) DESC
SELECT count(*) , country FROM airports GROUP BY country ORDER BY count(*) DESC
SELECT count(*) , country FROM airports GROUP BY country ORDER BY count(*) DESC
SELECT count(*) , city FROM airports WHERE country = 'United States' GROUP BY city ORDER BY count(*) DESC
SELECT count(*) , city FROM airports WHERE country = 'United States' GROUP BY city ORDER BY count(*) DESC
SELECT city FROM airports WHERE country = 'United States' GROUP BY city HAVING count(*) > 3
SELECT city FROM airports WHERE country = 'United States' GROUP BY city HAVING count(*) > 3
SELECT count(*) FROM (SELECT city FROM airports GROUP BY city HAVING count(*) > 3)
SELECT count(*) FROM (SELECT city FROM airports GROUP BY city HAVING count(*) > 3)
SELECT city , count(*) FROM airports GROUP BY city HAVING count(*) > 1
SELECT city , count(*) FROM airports GROUP BY city HAVING count(*) > 1
SELECT city FROM airports GROUP BY city HAVING count(*) > 2 ORDER BY count(*)
SELECT city FROM airports GROUP BY city HAVING count(*) > 2 ORDER BY count(*)
SELECT count(*) , T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid GROUP BY T1.name
SELECT count(*) , T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid GROUP BY T1.name
SELECT count(*) , T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid GROUP BY T1.name ORDER BY count(*) DESC
SELECT count(*) , T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid GROUP BY T1.name ORDER BY count(*) DESC
SELECT avg(elevation) , country FROM airports GROUP BY country
SELECT avg(elevation) , country FROM airports GROUP BY country
SELECT city FROM airports GROUP BY city HAVING count(*) = 2
SELECT city FROM airports GROUP BY city HAVING count(*) = 2
SELECT T1.country , T1.name , count(*) FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid GROUP BY T1.country , T1.name
SELECT T1.country , T1.name , count(*) FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid GROUP BY T1.country , T1.name
SELECT count(*) FROM routes AS T1 JOIN airports AS T2 ON T1.dst_apid = T2.apid WHERE T2.country = 'Italy'
SELECT count(*) FROM routes AS T1 JOIN airports AS T2 ON T1.dst_apid = T2.apid WHERE T2.country = 'Italy'
SELECT count(*) FROM routes AS T1 JOIN airports AS T2 ON T1.dst_apid = T2.apid JOIN airlines AS T3 ON T1.alid = T3.alid WHERE T2.country = 'Italy' AND T3.name = 'American Airlines'
SELECT count(*) FROM routes AS T1 JOIN airports AS T2 ON T1.dst_apid = T2.apid JOIN airlines AS T3 ON T1.alid = T3.alid WHERE T2.country = 'Italy' AND T3.name = 'American Airlines'
SELECT count(*) FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid WHERE T1.name = 'John F Kennedy International Airport'
SELECT count(*) FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid WHERE T1.name = 'John F Kennedy International Airport'
SELECT count(*) FROM routes WHERE dst_apid IN (SELECT apid FROM airports WHERE country = 'Canada') AND src_apid IN (SELECT apid FROM airports WHERE country = 'United States')
SELECT count(*) FROM routes WHERE dst_apid IN (SELECT apid FROM airports WHERE country = 'Canada') AND src_apid IN (SELECT apid FROM airports WHERE country = 'United States')
SELECT rid FROM routes WHERE dst_apid IN (SELECT apid FROM airports WHERE country = 'United States') AND src_apid IN (SELECT apid FROM airports WHERE country = 'United States')
SELECT rid FROM routes WHERE dst_apid IN (SELECT apid FROM airports WHERE country = 'United States') AND src_apid IN (SELECT apid FROM airports WHERE country = 'United States')
SELECT T1.name FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid GROUP BY T1.name ORDER BY count(*) DESC LIMIT 1
SELECT T1.name FROM airlines AS T1 JOIN routes AS T2 ON T1.alid = T2.alid GROUP BY T1.name ORDER BY count(*) DESC LIMIT 1
SELECT T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid WHERE T1.country = 'China' GROUP BY T1.name ORDER BY count(*) DESC LIMIT 1
SELECT T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.src_apid WHERE T1.country = 'China' GROUP BY T1.name ORDER BY count(*) DESC LIMIT 1
SELECT T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid WHERE T1.country = 'China' GROUP BY T1.name ORDER BY count(*) DESC LIMIT 1
SELECT T1.name FROM airports AS T1 JOIN routes AS T2 ON T1.apid = T2.dst_apid WHERE T1.country = 'China' GROUP BY T1.name ORDER BY count(*) DESC LIMIT 1
SELECT count(*) FROM book_club
SELECT count(*) FROM book_club
SELECT book_title , author_or_editor FROM book_club WHERE YEAR > 1989
SELECT book_title , author_or_editor FROM book_club WHERE YEAR > 1989
SELECT publisher FROM book_club
SELECT publisher FROM book_club
SELECT YEAR , book_title , publisher FROM book_club ORDER BY YEAR DESC
SELECT YEAR , book_title , publisher FROM book_club ORDER BY YEAR DESC
SELECT publisher , count(*) FROM book_club GROUP BY publisher
SELECT publisher , count(*) FROM book_club GROUP BY publisher
SELECT publisher FROM book_club GROUP BY publisher ORDER BY count(*) DESC LIMIT 1
SELECT publisher FROM book_club GROUP BY publisher ORDER BY count(*) DESC LIMIT 1
SELECT category , count(*) FROM book_club GROUP BY category
SELECT category , count(*) FROM book_club GROUP BY category
SELECT category FROM book_club WHERE YEAR > 1989 GROUP BY category HAVING count(*) >= 2
SELECT category FROM book_club WHERE YEAR > 1989 GROUP BY category HAVING count(*) >= 2
SELECT publisher FROM book_club WHERE YEAR = 1989 INTERSECT SELECT publisher FROM book_club WHERE YEAR = 1990
SELECT publisher FROM book_club WHERE YEAR = 1989 INTERSECT SELECT publisher FROM book_club WHERE YEAR = 1990
SELECT publisher FROM book_club EXCEPT SELECT publisher FROM book_club WHERE YEAR = 1989
SELECT publisher FROM book_club EXCEPT SELECT publisher FROM book_club WHERE YEAR = 1989
SELECT title , YEAR , director FROM movie ORDER BY budget_million
SELECT title , YEAR , director FROM movie ORDER BY budget_million
SELECT COUNT ( director) FROM movie
SELECT COUNT ( director) FROM movie
SELECT title , director FROM movie WHERE YEAR <= 2000 ORDER BY gross_worldwide DESC LIMIT 1
SELECT title , director FROM movie WHERE YEAR <= 2000 ORDER BY gross_worldwide DESC LIMIT 1
SELECT director FROM movie WHERE YEAR = 2000 INTERSECT SELECT director FROM movie WHERE YEAR = 1999
SELECT director FROM movie WHERE YEAR = 2000 INTERSECT SELECT director FROM movie WHERE YEAR = 1999
SELECT director FROM movie WHERE YEAR = 1999 OR YEAR = 2000
SELECT director FROM movie WHERE YEAR = 1999 OR YEAR = 2000
SELECT avg(budget_million) , max(budget_million) , min(budget_million) FROM movie WHERE YEAR < 2000
SELECT avg(budget_million) , max(budget_million) , min(budget_million) FROM movie WHERE YEAR < 2000
SELECT T1.company_name FROM culture_company AS T1 JOIN book_club AS T2 ON T1.book_club_id = T2.book_club_id WHERE T2.publisher = 'Alyson'
SELECT T1.company_name FROM culture_company AS T1 JOIN book_club AS T2 ON T1.book_club_id = T2.book_club_id WHERE T2.publisher = 'Alyson'
SELECT T1.title , T3.book_title FROM movie AS T1 JOIN culture_company AS T2 ON T1.movie_id = T2.movie_id JOIN book_club AS T3 ON T3.book_club_id = T2.book_club_id WHERE T2.incorporated_in = 'China'
SELECT T1.title , T3.book_title FROM movie AS T1 JOIN culture_company AS T2 ON T1.movie_id = T2.movie_id JOIN book_club AS T3 ON T3.book_club_id = T2.book_club_id WHERE T2.incorporated_in = 'China'
SELECT T2.company_name FROM movie AS T1 JOIN culture_company AS T2 ON T1.movie_id = T2.movie_id WHERE T1.year = 1999
SELECT T2.company_name FROM movie AS T1 JOIN culture_company AS T2 ON T1.movie_id = T2.movie_id WHERE T1.year = 1999
SELECT count(*) FROM poker_player
SELECT count(*) FROM poker_player
SELECT Earnings FROM poker_player ORDER BY Earnings DESC
SELECT Earnings FROM poker_player ORDER BY Earnings DESC
SELECT Final_Table_Made , Best_Finish FROM poker_player
SELECT Final_Table_Made , Best_Finish FROM poker_player
SELECT avg(Earnings) FROM poker_player
SELECT avg(Earnings) FROM poker_player
SELECT Money_Rank FROM poker_player ORDER BY Earnings DESC LIMIT 1
SELECT Money_Rank FROM poker_player ORDER BY Earnings DESC LIMIT 1
SELECT max(Final_Table_Made) FROM poker_player WHERE Earnings < 200000
SELECT max(Final_Table_Made) FROM poker_player WHERE Earnings < 200000
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Earnings > 300000
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID WHERE T2.Earnings > 300000
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Final_Table_Made
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Final_Table_Made
SELECT T1.Birth_Date FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Earnings ASC LIMIT 1
SELECT T1.Birth_Date FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Earnings ASC LIMIT 1
SELECT T2.Money_Rank FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Height DESC LIMIT 1
SELECT T2.Money_Rank FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T1.Height DESC LIMIT 1
SELECT avg(T2.Earnings) FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Height > 200
SELECT avg(T2.Earnings) FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID WHERE T1.Height > 200
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Earnings DESC
SELECT T1.Name FROM people AS T1 JOIN poker_player AS T2 ON T1.People_ID = T2.People_ID ORDER BY T2.Earnings DESC
SELECT Nationality , COUNT(*) FROM people GROUP BY Nationality
SELECT Nationality , COUNT(*) FROM people GROUP BY Nationality
SELECT Nationality FROM people GROUP BY Nationality ORDER BY COUNT(*) DESC LIMIT 1
SELECT Nationality FROM people GROUP BY Nationality ORDER BY COUNT(*) DESC LIMIT 1
SELECT Nationality FROM people GROUP BY Nationality HAVING COUNT(*) >= 2
SELECT Nationality FROM people GROUP BY Nationality HAVING COUNT(*) >= 2
SELECT Name , Birth_Date FROM people ORDER BY Name ASC
SELECT Name , Birth_Date FROM people ORDER BY Name ASC
SELECT Name FROM people WHERE Nationality != "Russia"
SELECT Name FROM people WHERE Nationality != "Russia"
SELECT Name FROM people WHERE People_ID NOT IN (SELECT People_ID FROM poker_player)
SELECT Name FROM people WHERE People_ID NOT IN (SELECT People_ID FROM poker_player)
SELECT count( Nationality) FROM people
SELECT count( Nationality) FROM people
SELECT count(*) FROM singer
SELECT count(*) FROM singer
SELECT Name FROM singer ORDER BY Net_Worth_Millions ASC
SELECT Name FROM singer ORDER BY Net_Worth_Millions ASC
SELECT Birth_Year , Citizenship FROM singer
SELECT Birth_Year , Citizenship FROM singer
SELECT Name FROM singer WHERE Citizenship != "France"
SELECT Name FROM singer WHERE Citizenship != "France"
SELECT Name FROM singer WHERE Birth_Year = 1948 OR Birth_Year = 1949
SELECT Name FROM singer WHERE Birth_Year = 1948 OR Birth_Year = 1949
SELECT Name FROM singer ORDER BY Net_Worth_Millions DESC LIMIT 1
SELECT Name FROM singer ORDER BY Net_Worth_Millions DESC LIMIT 1
SELECT Citizenship , COUNT(*) FROM singer GROUP BY Citizenship
SELECT Citizenship , COUNT(*) FROM singer GROUP BY Citizenship
SELECT Citizenship FROM singer GROUP BY Citizenship ORDER BY COUNT(*) DESC LIMIT 1
SELECT Citizenship FROM singer GROUP BY Citizenship ORDER BY COUNT(*) DESC LIMIT 1
SELECT Citizenship , max(Net_Worth_Millions) FROM singer GROUP BY Citizenship
SELECT Citizenship , max(Net_Worth_Millions) FROM singer GROUP BY Citizenship
SELECT T2.Title , T1.Name FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID
SELECT T2.Title , T1.Name FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID
SELECT T1.Name FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID WHERE T2.Sales > 300000
SELECT T1.Name FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID WHERE T2.Sales > 300000
SELECT T1.Name FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID GROUP BY T1.Name HAVING COUNT(*) > 1
SELECT T1.Name FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID GROUP BY T1.Name HAVING COUNT(*) > 1
SELECT T1.Name , sum(T2.Sales) FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID GROUP BY T1.Name
SELECT T1.Name , sum(T2.Sales) FROM singer AS T1 JOIN song AS T2 ON T1.Singer_ID = T2.Singer_ID GROUP BY T1.Name
SELECT Name FROM singer WHERE Singer_ID NOT IN (SELECT Singer_ID FROM song)
SELECT Name FROM singer WHERE Singer_ID NOT IN (SELECT Singer_ID FROM song)
SELECT Citizenship FROM singer WHERE Birth_Year < 1945 INTERSECT SELECT Citizenship FROM singer WHERE Birth_Year > 1955
SELECT Citizenship FROM singer WHERE Birth_Year < 1945 INTERSECT SELECT Citizenship FROM singer WHERE Birth_Year > 1955
