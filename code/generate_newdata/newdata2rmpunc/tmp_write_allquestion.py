# 1. remove the punct at the end of the sentence.
# 2. replace all question mark in the middle with 
# lower case all the words
# checked 

import sys, re

fold = sys.argv[1]

fin = open("../../newdata/" + fold + ".json", "r")

regex = re.compile("\?[ a-z]")

def process_question(question_str):
	return re.sub("[^\x00-\x7F]+", "", question_str)

import json
alldata = json.loads(fin.read())

fout = open("tmp_allques.txt", "w")

for i in range(0, len(alldata)):
	thisdata = alldata[i]
	thisquestion = thisdata["question"]	
	if "-" in thisdata["question_toks"]:
		print thisquestion
	newquestion = process_question(thisquestion)
	fout.write(newquestion + "\n")

fout.close()

#json.dump(alldata, open("../../newdata_rmpunc/" + fold + ".json", "w"))
