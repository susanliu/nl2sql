# 1. remove the punct at the end of the sentence.
# 2. replace all question mark in the middle with, so there are only two puncs left: , (to separate words), . (to separate sentence)
# 3. remove "distinct"
# 4. remove "query_toks no value" because it's not in the training data ../saved_model
# lower case all the words
# checked 

import json
import sys, re

sys.path.insert(0, "../../")
from process_sql import * 

fold = sys.argv[1]

fin = open("../../newdata/" + fold + ".json", "r")
countsame = countdiff = 0

regex = re.compile("\?[ a-z]")

char2count = {}

class Schema:
    """
    Simple schema which maps table&column to a unique identifier
    """
    def __init__(self, schema, table):
        self._schema = schema
        self._table = table
        self._idMap = self._map(self._schema, self._table)

    @property
    def schema(self):
        return self._schema

    @property
    def idMap(self):
        return self._idMap

    def _map(self, schema, table):
        column_names_original = table['column_names_original']
        table_names_original = table['table_names_original']
        #print 'column_names_original: ', column_names_original
        #print 'table_names_original: ', table_names_original
        for i, (tab_id, col) in enumerate(column_names_original):
            if tab_id == -1:
                idMap = {'*': i}
            else:
                key = table_names_original[tab_id].lower()
                val = col.lower()
                idMap[key + "." + val] = i

        for i, tab in enumerate(table_names_original):
            key = tab.lower()
            idMap[key] = i

        return idMap

def get_schemas_from_json(fpath):
    with open(fpath) as f:
        data = json.load(f)
    db_names = [db['db_id'] for db in data]

    tables = {}
    schemas = {}
    for db in data:
        db_id = db['db_id']
        schema = {} #{'table': [col.lower, ..., ]} * -> __all__
        column_names_original = db['column_names_original']
        table_names_original = db['table_names_original']
        tables[db_id] = {'column_names_original': column_names_original, 'table_names_original': table_names_original}
        for i, tabn in enumerate(table_names_original):
            table = str(tabn.lower())
            cols = [str(col.lower()) for td, col in column_names_original if td == i]
            schema[table] = cols
        schemas[db_id] = schema

    return schemas, db_names, tables

def rm_distinct_query(query, db_id):
	old_query = query
	old_len = len(query)
	query = re.sub("(DISTINCT|distinct)(\s+|\()", r"\2", query)
	assert "distinct" not in query.lower()
	query = re.sub("\s+", " ", query)
	table_file = "../../newdata_rmpunc/tables.json"
	schemas, db_names, tables = get_schemas_from_json(table_file)
	schema = schemas[db_id]	
	table = tables[db_id]
	schema = Schema(schema, table)
	sql_label = get_sql(schema, query)
	return sql_label, query

def rm_uniqueword(question_list):
	old_len = len(question_list)
	newquestion_list = []
	for word in question_list:
		# here I haven't removed "each" and "all", b/c these are also in the query that does not contain "DISTINCT"
		if word in set(["different", "unique", "distinct", "distinctive"]):
			continue
		newquestion_list.append(word)
	return newquestion_list

def rm_distinct(query, question_list, db_id):
	sql_label, newquery = rm_distinct_query(query, db_id) 
	question_list = rm_uniqueword(question_list)	
	return sql_label, question_list, newquery

def process_question(question_list):
	newquestion_list = []
	for i in range(0, len(question_list)):
		thisword = re.sub("[^\x00-\x7F]+", "", question_list[i].lower())
		if re.search("^[^a-z0-9]+$", thisword): 
			if thisword != "?" and thisword != "." and thisword != ",":
				continue
			if thisword == "?":
				thisword = "."
		newquestion_list.append(thisword)		
	oldlength = len(newquestion_list)
	while re.search("^[^a-z0-9\)]+$", newquestion_list[-1]):
		newquestion_list = newquestion_list[:-1]
	return newquestion_list	

alldata = json.loads(fin.read())
new_alldata = []

fout1 = open("../../newdata_rmpunc/" + fold + "_gold.sql", "w")
fout2 = open("../../newdata_rmpunc/" + fold + "_gold_unlabeled.sql", "w")

for i in range(0, len(alldata)):
	if i % 100 == 0:
		print i
	thisdata = alldata[i]
	newdata = {}
	question_list = thisdata["question_toks"]	
	db_id = thisdata["db_id"]
	newquestion_list = process_question(question_list)
	thisquery = thisdata["query"]
	#if "DISTINCT" in thisquery or "distinct" in thisquery:
	sql, newquestion_list, thisquery = rm_distinct(thisquery, newquestion_list, db_id)
	#else:
	#	sql = thisdata["sql"]
	newdata["question_toks"] = newquestion_list
	newdata["sql"] = sql
	newdata["db_id"] = db_id
	newdata["question"] = " ".join(newquestion_list)
	newdata["query"] = thisquery
	fout1.write(thisquery + "\t" + db_id + "\n")
	fout2.write(thisquery + "\n")
	new_alldata.append(newdata)

json.dump(new_alldata, open("../../newdata_rmpunc/" + fold + ".json", "w"))

fout1.close()
fout2.close()
