fin = open("../../data_augment/wikisql_tables_2.json", "r")

import json

tables = []
typeset = set([])
for line in fin:
	thistable = json.loads(line.strip("\n"))
        typelist = thistable["types"] 
        typeset = typeset.union(set(typelist)) 
	tables.append(thistable)

json.dump(tables, open("../../data_augment/wikisql_tables.json", "w"))
print typeset
