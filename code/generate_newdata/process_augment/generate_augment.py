import sys, os, re, traceback
sys.path.insert(0, "../../")
from process_sql import * 

in_path = "../../data_augment/"
out_path = "../../newdata_augment/"

regex = re.compile("\?[ a-z]")

class Schema:
    """
    Simple schema which maps table&column to a unique identifier
    """
    def __init__(self, schema, table):
        self._schema = schema
        self._table = table
        self._idMap = self._map(self._schema, self._table)

    @property
    def schema(self):
        return self._schema

    @property
    def idMap(self):
        return self._idMap

    def _map(self, schema, table):
        column_names_original = table['column_names_original']
        table_names_original = table['table_names_original']
        #print 'column_names_original: ', column_names_original
        #print 'table_names_original: ', table_names_original
        for i, (tab_id, col) in enumerate(column_names_original):
            if tab_id == -1:
                idMap = {'*': i}
            else:
                key = table_names_original[tab_id].lower()
                val = col.lower()
                idMap[key + "." + val] = i

        for i, tab in enumerate(table_names_original):
            key = tab.lower()
            idMap[key] = i

        return idMap

def get_schemas_from_json(fpath):
    with open(fpath) as f:
        data = json.load(f)
    db_names = [db['db_id'] for db in data]

    tables = {}
    schemas = {}
    for db in data:
        db_id = db['db_id']
        schema = {} #{'table': [col.lower, ..., ]} * -> __all__
        column_names_original = db['column_names_original']
        table_names_original = db['table_names_original']
        tables[db_id] = {'column_names_original': column_names_original, 'table_names_original': table_names_original}
        for i, tabn in enumerate(table_names_original):
	    tabn = re.sub("[^\x00-\x7F]+", "", tabn)
            table = str(tabn.lower())
            cols = [str(re.sub("[^\x00-\x7F]+", "", col).lower()) for td, col in column_names_original if td == i]
            schema[table] = cols
        schemas[db_id] = schema

    return schemas, db_names, tables


def rm_distinct_query(query, db_id, schemas, tables):
	old_query = query
	old_len = len(query)
	query = re.sub("(DISTINCT|distinct)(\s+|\()", r"\2", query)
	assert "distinct" not in query.lower()
	query = re.sub("\s+", " ", query)
	schema = schemas[db_id]	
	table = tables[db_id]
	schema = Schema(schema, table)
	sql_label = get_sql(schema, query)
	return sql_label, query

def rm_uniqueword(question_list):
	old_len = len(question_list)
	newquestion_list = []
	for word in question_list:
		# here I haven't removed "each" and "all", b/c these are also in the query that does not contain "DISTINCT"
		if word in set(["different", "unique", "distinct", "distinctive"]):
			continue
		newquestion_list.append(word)
	return newquestion_list


def rm_distinct(query, question_list, db_id, schemas, tables):
	sql_label, newquery = rm_distinct_query(query, db_id, schemas, tables) 
	question_list = rm_uniqueword(question_list)	
	return sql_label, question_list, newquery


def process_question(question_list):
	newquestion_list = []
	for i in range(0, len(question_list)):
		thisword = re.sub("[^\x00-\x7F]+", "", question_list[i].lower())
		if re.search("^[^a-z0-9]+$", thisword): 
			if thisword != "?" and thisword != "." and thisword != ",":
				continue
			if thisword == "?":
				thisword = "."
		newquestion_list.append(thisword)		
	oldlength = len(newquestion_list)
	while re.search("^[^a-z0-9]+$", newquestion_list[-1]):
		newquestion_list = newquestion_list[:-1]
	return newquestion_list	

table_file = "../../data_augment/wikisql_tables.json"
schemas, db_names, tables = get_schemas_from_json(table_file)

fout1 = open("../../newdata_augment/augment_gold.sql", "w")
fout2 = open("../../newdata_augment/augment_gold_unlabeled.sql", "w")

new_alldata = []

linecount = 0
for eachfile in os.listdir(in_path):
	if linecount % 100 == 0:
		print linecount
	linecount += 1
	if eachfile.endswith(".txt"):
		file_path = os.path.join(in_path, eachfile)
		fin = open(file_path)
		for line in fin:
			newdata = {}
			tokens = line.strip("\n").split("\t")
			if len(tokens) < 3:
				print tokens, eachfile
			question_list = process_question(tokens[0].split())
			query = tokens[1]
			if "{" in line or "}" in line:
				continue
			db_name = tokens[2]
			try:
				sql, question_list, query = rm_distinct(query, question_list, db_name, schemas, tables)
			except Exception as e:
				if e.message == "column is not in here" or e.message == "Unexpected quote" or e.message == "too many values to unpack" or e.message == "Error col: t1" or e.message == "Alias as has the same name in table":
					continue
				elif isinstance(e, IndexError) or isinstance(e, KeyError):
					continue
				else:
					print query
					print traceback.format_exc() 
					print e.message
					import pdb
					pdb.set_trace()
			newdata["question_toks"] = question_list
			newdata["sql"] = sql
			newdata["db_id"] = db_name
			newdata["question"] = " ".join(question_list)
			newdata["query"] = query
			fout1.write(query + "\t" + db_name + "\n")
			fout2.write(query + "\n")
			new_alldata.append(newdata)

json.dump(new_alldata, open("../../newdata_augment/augment.json", "w"))
print len(new_alldata)

fout1.close()
fout2.close()
