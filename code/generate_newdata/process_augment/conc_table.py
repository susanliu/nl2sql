import json, re

table1 = json.load(open("../../newdata_rmpunc/tables.json", "r"))
table2 = json.load(open("../../data_augment/wikisql_tables.json", "r"))

for eachtable in table2:
	for key in set(["table_names", "table_names_original"]):
		thislist = eachtable[key]
		for i in range(0, len(thislist)):
			thislist[i] = re.sub("[^\x00-\x7F]+", "", thislist[i])		
	for key in set(["column_names", "column_names_original"]):
		thislist = eachtable[key]
		for i in range(0, len(thislist)):
			thislist[i][1] = re.sub("[^\x00-\x7F]+", "", thislist[i][1])
	
tables = table1 + table2
json.dump(tables, open("../../newdata_augment/tables.json", "w"))
