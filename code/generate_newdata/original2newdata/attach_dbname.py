import sys, json

fold = sys.argv[1]

fin = open("../newdata/" + fold + "_gold_unlabeled.sql", "r")
fout = open("../newdata/" + fold + "_gold.sql", "w")

datajson = json.loads(open("../newdata/" + fold + ".json", "r").read())

print len(datajson)	

for i in range(0, len(datajson)):
	dbnamei = datajson[i]["db_id"]
	line = fin.readline()
	fout.write(line.strip("\n") + "\t" + dbnamei + "\n")

fout.close()
