import json, re
import random

data_conc = json.loads(open("../data/train.json", "r").read()) + json.loads(open("../data/dev.json", "r").read())
type_conc = json.loads(open("../data/train_type.json", "r").read()) + json.loads(open("../data/dev_type.json", "r").read())

db_names = set([])

for i in range(0, len(data_conc)):
	db_names.add(data_conc[i]["db_id"])
	
sample_train = set(random.sample(db_names, 115))
sample_rest = db_names.difference(sample_train)
sample_test = set(random.sample(sample_rest, 30))
sample_dev = set(sample_rest.difference(sample_test))

def write_fold(foldname, fold_sample):
	fout_trainfold = open("../newdata/" + foldname + "_fold.txt", "w")
	for eachsample in fold_sample:
		fout_trainfold.write(eachsample + "\n")
	fout_trainfold.close()

write_fold("train", sample_train)
write_fold("dev", sample_dev)
write_fold("test", sample_test)

new_train = []
new_dev = []
new_test = []

new_traintype = []
new_devtype = []
new_testtype = []

new_traingold = []
new_devgold = []
new_testgold = []

fout_train = open("../newdata/train_gold.sql", "w")
fout_dev = open("../newdata/dev_gold.sql", "w")
fout_test = open("../newdata/test_gold.sql", "w")

for i in range(0, len(data_conc)):
	this_data = data_conc[i]
	this_datatype = type_conc[i] 
	this_gold = this_data["query"]
	this_gold = re.sub("\s+", " ", this_gold)
	if this_data["db_id"] in sample_train:
		new_train.append(this_data)
		new_traintype.append(this_datatype)
		fout_gold = fout_train
	elif this_data["db_id"] in sample_dev:
		new_dev.append(this_data)
		new_devtype.append(this_datatype)
		fout_gold = fout_dev
	else:
		new_test.append(this_data)
		new_testtype.append(this_datatype)
		fout_gold = fout_test
	fout_gold.write(this_gold + "\n")

fout_gold.close()

def dump_result(foldname, data, datatype):
	json.dump(data, open("../newdata/" + foldname + ".json", "w"))
	json.dump(datatype, open("../newdata/" + foldname + "_type.json", "w"))

dump_result("train", new_train, new_traintype)
dump_result("dev", new_dev, new_devtype)
dump_result("test", new_test, new_testtype)

