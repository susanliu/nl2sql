what are the game site that have greater week than any week in 1999 ?	SELECT t1.game_site from 1999 as t1 WHERE t1.week > ( SELECT MIN ( t1.week ) from 1999 as t1 )	2-10745921-1
what is the game site of the 1999 with least number of game site ?	SELECT t1.game_site from 1999 as t1 GROUP BY t1.game_site ORDER BY COUNT ( * ) ASC LIMIT 1	2-10745921-1
what are the attendance that have greater week than any week in 1999 ?	SELECT t1.attendance from 1999 as t1 WHERE t1.week > ( SELECT MIN ( t1.week ) from 1999 as t1 )	2-10745921-1
what is the opponent of the most common 1999 in all opponent ?	SELECT t1.opponent from 1999 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-10745921-1
sort the list of attendance and result of all 1999 in the descending order of week .	SELECT t1.attendance , t1.result from 1999 as t1 ORDER BY t1.week DESC	2-10745921-1
find the game site of the 1999 that is most frequent across all game site .	SELECT t1.game_site from 1999 as t1 GROUP BY t1.game_site ORDER BY COUNT ( * ) DESC LIMIT 1	2-10745921-1
what are the opponent of all 1999 with attendance that is 10 ?	SELECT t1.opponent from 1999 as t1 GROUP BY t1.attendance HAVING COUNT ( * ) = 10	2-10745921-1
show all information on the 1999 that has the largest number of opponent.	SELECT * from 1999 as t1 ORDER BY t1.opponent DESC LIMIT 1	2-10745921-1
find the attendance of 1999 who have week of both 10 and 53 .	SELECT t1.attendance from 1999 as t1 WHERE t1.week = 10 INTERSECT SELECT t1.attendance from 1999 as t1 WHERE t1.week = 53	2-10745921-1
what is the opponent of the 1999 with the minimum week ?	SELECT t1.opponent from 1999 as t1 ORDER BY t1.week ASC LIMIT 1	2-10745921-1
