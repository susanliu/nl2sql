list all information about 1928 .	SELECT * FROM 1928	2-10766119-9
what are the home team of 1928 , sorted by their frequency?	SELECT t1.home_team from 1928 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10766119-9
how many distinct home team score correspond to each home team ?	SELECT t1.home_team , COUNT ( DISTINCT t1.home_team_score ) from 1928 as t1 GROUP BY t1.home_team	2-10766119-9
what are the venue with exactly 10 1928 ?	SELECT t1.venue from 1928 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10766119-9
find the date of the 1928 with the highest crowd.	SELECT t1.date from 1928 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1928 as t1 )	2-10766119-9
select the average crowd of each 1928 's home team .	SELECT AVG ( t1.crowd ) , t1.home_team from 1928 as t1 GROUP BY t1.home_team	2-10766119-9
list venue and date who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.venue , t1.date from 1928 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10766119-9
what are all the home team and away team?	SELECT t1.home_team , t1.away_team from 1928 as t1	2-10766119-9
how many 1928 ' away team have the word 10 in them ?	SELECT COUNT ( * ) from 1928 as t1 WHERE t1.away_team LIKE 10	2-10766119-9
return the home team score of 1928 that do not have the away team score 10 .	SELECT t1.home_team_score from 1928 as t1 WHERE t1.away_team_score ! = 10	2-10766119-9
