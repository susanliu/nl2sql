return the player of the 2004 that has the fewest corresponding player .	SELECT t1.player from 2004 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-14614110-4
what is the place and country of the 2004 with maximum score ?	SELECT t1.place , t1.country from 2004 as t1 WHERE t1.score = ( SELECT MAX ( t1.score ) {FROM, 3} )	2-14614110-4
how many 2004 have place that contains 10 ?	SELECT COUNT ( * ) from 2004 as t1 WHERE t1.place LIKE 10	2-14614110-4
show the player of 2004 who have at least 10 2004 .	SELECT t1.player from 2004 as t1 GROUP BY t1.player HAVING COUNT ( * ) >= 10	2-14614110-4
find the player of the 2004 with the highest score.	SELECT t1.player from 2004 as t1 WHERE t1.score = ( SELECT MAX ( t1.score ) from 2004 as t1 )	2-14614110-4
what are the maximum and minimum score across all 2004 ?	SELECT MAX ( t1.score ) , MIN ( t1.score ) from 2004 as t1	2-14614110-4
what is the count of 2004 with more than 10 to par ?	SELECT COUNT ( * ) from 2004 as t1 GROUP BY t1.to_par HAVING COUNT ( * ) > 10 	2-14614110-4
select the average score of each 2004 's country .	SELECT AVG ( t1.score ) , t1.country from 2004 as t1 GROUP BY t1.country	2-14614110-4
what is the to par and to par of every 2004 that has a score lower than average ?	SELECT t1.to_par , t1.to_par from 2004 as t1 WHERE t1.score < ( SELECT AVG ( t1.score ) {FROM, 3} )	2-14614110-4
what is the minimum score in each to par ?	SELECT MIN ( t1.score ) , t1.to_par from 2004 as t1 GROUP BY t1.to_par	2-14614110-4
