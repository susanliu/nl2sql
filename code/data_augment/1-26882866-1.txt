give the season that has the most houston .	SELECT t1.season from houston as t1 GROUP BY t1.season ORDER BY COUNT ( * ) DESC LIMIT 1	1-26882866-1
return the different standing of houston , in ascending order of frequency .	SELECT t1.standing from houston as t1 GROUP BY t1.standing ORDER BY COUNT ( * ) ASC LIMIT 1	1-26882866-1
find the distinct season of houston having ties between 10 and 101 .	SELECT DISTINCT t1.season from houston as t1 WHERE t1.ties BETWEEN 10 AND 101	1-26882866-1
find the number of houston that have more than 10 standing .	SELECT COUNT ( * ) from houston as t1 GROUP BY t1.standing HAVING COUNT ( * ) > 10 	1-26882866-1
what are the playoffs and standing of the {COLUMN} who have games above five or goals against below ten ?	SELECT t1.playoffs , t1.standing from houston as t1 WHERE t1.games > 5 OR t1.goals_against < 10	1-26882866-1
count the number of houston that have an standing containing 10 .	SELECT COUNT ( * ) from houston as t1 WHERE t1.standing LIKE 10	1-26882866-1
what is the minimum goals for in each standing ?	SELECT MIN ( t1.goals_for ) , t1.standing from houston as t1 GROUP BY t1.standing	1-26882866-1
what are all the season and season?	SELECT t1.season , t1.season from houston as t1	1-26882866-1
list the standing , standing and the standing of the houston .	SELECT t1.standing , t1.standing , t1.standing from houston as t1	1-26882866-1
show the playoffs of houston who have at least 10 houston .	SELECT t1.playoffs from houston as t1 GROUP BY t1.playoffs HAVING COUNT ( * ) >= 10	1-26882866-1
