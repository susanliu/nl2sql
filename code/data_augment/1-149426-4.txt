find the prefix size of subnetwork which are network mask 10 but not network mask 27 .	SELECT t1.prefix_size from subnetwork as t1 WHERE t1.network_mask = 10 EXCEPT SELECT t1.prefix_size from subnetwork as t1 WHERE t1.network_mask = 27	1-149426-4
what is the minimum available subnets in each network mask ?	SELECT MIN ( t1.available_subnets ) , t1.network_mask from subnetwork as t1 GROUP BY t1.network_mask	1-149426-4
how many different network mask correspond to each prefix size ?	SELECT t1.prefix_size , COUNT ( DISTINCT t1.network_mask ) from subnetwork as t1 GROUP BY t1.prefix_size	1-149426-4
show the prefix size shared by more than 10 subnetwork .	SELECT t1.prefix_size from subnetwork as t1 GROUP BY t1.prefix_size HAVING COUNT ( * ) > 10	1-149426-4
find the prefix size of subnetwork whose available subnets is higher than the average available subnets .	SELECT t1.prefix_size from subnetwork as t1 WHERE t1.available_subnets > ( SELECT AVG ( t1.available_subnets ) from subnetwork as t1	1-149426-4
what is the maximum and mininum total usable hosts {COLUMN} for all subnetwork ?	SELECT MAX ( t1.total_usable_hosts ) , MIN ( t1.total_usable_hosts ) from subnetwork as t1	1-149426-4
find the network mask and prefix size of the subnetwork with at least 10 network mask .	SELECT t1.network_mask , t1.prefix_size from subnetwork as t1 GROUP BY t1.network_mask HAVING COUNT ( * ) >= 10	1-149426-4
what is the network mask of the subnetwork who has the highest number of subnetwork ?	SELECT t1.network_mask from subnetwork as t1 GROUP BY t1.network_mask ORDER BY COUNT ( * ) DESC LIMIT 1	1-149426-4
which prefix size have less than 3 in subnetwork ?	SELECT t1.prefix_size from subnetwork as t1 GROUP BY t1.prefix_size HAVING COUNT ( * ) < 3	1-149426-4
return the smallest usable hosts per subnet for every prefix size .	SELECT MIN ( t1.usable_hosts_per_subnet ) , t1.prefix_size from subnetwork as t1 GROUP BY t1.prefix_size	1-149426-4
