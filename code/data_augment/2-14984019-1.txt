what are the distinct COLUMN_NAME,0} of 1954 with attendance higher than any 1954 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.record from 1954 as t1 WHERE t1.attendance > ( SELECT MIN ( t1.record ) from 1954 as t1 WHERE t1.game_site = 10 )	2-14984019-1
what is the record and game site of every 1954 that has a attendance lower than average ?	SELECT t1.record , t1.game_site from 1954 as t1 WHERE t1.attendance < ( SELECT AVG ( t1.attendance ) {FROM, 3} )	2-14984019-1
show the record and the number of unique record containing each record .	SELECT t1.record , COUNT ( DISTINCT t1.record ) from 1954 as t1 GROUP BY t1.record	2-14984019-1
find all record that have fewer than three in 1954 .	SELECT t1.record from 1954 as t1 GROUP BY t1.record HAVING COUNT ( * ) < 3	2-14984019-1
show all information on the 1954 that has the largest number of result.	SELECT * from 1954 as t1 ORDER BY t1.result DESC LIMIT 1	2-14984019-1
Return all columns in 1954 .	SELECT * FROM 1954	2-14984019-1
what is the game site of the 1954 with the smallest week ?	SELECT t1.game_site from 1954 as t1 ORDER BY t1.week ASC LIMIT 1	2-14984019-1
return each game site with the number of 1954 in ascending order of the number of game site .	SELECT t1.game_site , COUNT ( * ) from 1954 as t1 GROUP BY t1.game_site ORDER BY COUNT ( * )	2-14984019-1
which record have greater week than that of any week in 1954 ?	SELECT t1.record from 1954 as t1 WHERE t1.week > ( SELECT MIN ( t1.week ) from 1954 as t1 )	2-14984019-1
what is the t1.result of 1954 that has fewest number of 1954 ?	SELECT t1.result from 1954 as t1 GROUP BY t1.result ORDER BY COUNT ( * ) LIMIT 1	2-14984019-1
