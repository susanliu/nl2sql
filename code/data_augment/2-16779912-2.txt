how many 1984 are there that have more than {VALUE},0 result ?	SELECT COUNT ( * ) from 1984 as t1 GROUP BY t1.result HAVING COUNT ( * ) > 10 	2-16779912-2
what are the average week of 1984 , grouped by result ?	SELECT AVG ( t1.week ) , t1.result from 1984 as t1 GROUP BY t1.result	2-16779912-2
count the number of 1984 in result 10 or 73 .	SELECT COUNT ( * ) from 1984 as t1 WHERE t1.result = 10 OR t1.result = 73	2-16779912-2
return the different date of 1984 , in ascending order of frequency .	SELECT t1.date from 1984 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-16779912-2
give the opponent that has the most 1984 .	SELECT t1.opponent from 1984 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-16779912-2
Show everything on 1984	SELECT * FROM 1984	2-16779912-2
find the distinct opponent of all 1984 that have a higher week than some 1984 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.opponent from 1984 as t1 WHERE t1.week > ( SELECT MIN ( t1.opponent ) from 1984 as t1 WHERE t1.opponent = 10 )	2-16779912-2
list date and date who have week greater than 5 or attendance shorter than 10 .	SELECT t1.date , t1.date from 1984 as t1 WHERE t1.week > 5 OR t1.attendance < 10	2-16779912-2
what are the date , opponent , and date for each 1984 ?	SELECT t1.date , t1.opponent , t1.date from 1984 as t1	2-16779912-2
count the number of 1984 that have an date containing 10 .	SELECT COUNT ( * ) from 1984 as t1 WHERE t1.date LIKE 10	2-16779912-2
