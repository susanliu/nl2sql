which original air date have less than 3 in none ?	SELECT t1.original_air_date from none as t1 GROUP BY t1.original_air_date HAVING COUNT ( * ) < 3	1-19897294-11
find the family/families of the none which have no . in series 10 but not 72 .	SELECT t1.family/families from none as t1 WHERE t1.no_._in_series = 10 EXCEPT SELECT t1.family/families from none as t1 WHERE t1.no_._in_series = 72	1-19897294-11
count the number of none in no . in series 10 or 85 .	SELECT COUNT ( * ) from none as t1 WHERE t1.no_._in_series = 10 OR t1.no_._in_series = 85	1-19897294-11
what are the average no . in season of none , grouped by no . in series ?	SELECT AVG ( t1.no_._in_season ) , t1.no_._in_series from none as t1 GROUP BY t1.no_._in_series	1-19897294-11
which no . in series has most number of none ?	SELECT t1.no_._in_series from none as t1 GROUP BY t1.no_._in_series ORDER BY COUNT ( * ) DESC LIMIT 1	1-19897294-11
list original air date and location ( s ) who have no . in season greater than 5 or no . in season shorter than 10 .	SELECT t1.original_air_date , t1.location_ from none as t1 WHERE t1.no_._in_season > 5 OR t1.no_._in_season < 10	1-19897294-11
what is the maximum and mininum no . in season {COLUMN} for all none ?	SELECT MAX ( t1.no_._in_season ) , MIN ( t1.no_._in_season ) from none as t1	1-19897294-11
what are the maximum and minimum no . in season across all none ?	SELECT MAX ( t1.no_._in_season ) , MIN ( t1.no_._in_season ) from none as t1	1-19897294-11
give the family/families that has the most none .	SELECT t1.family/families from none as t1 GROUP BY t1.family/families ORDER BY COUNT ( * ) DESC LIMIT 1	1-19897294-11
what is the original air date of the none with the largest no . in season ?	SELECT t1.original_air_date from none as t1 WHERE t1.no_._in_season = ( SELECT MAX ( t1.no_._in_season ) from none as t1 )	1-19897294-11
