what is all the information on the pangasinan with the largest number of income class ?	SELECT * from pangasinan as t1 ORDER BY t1.income_class DESC LIMIT 1	1-1691800-2
show the district and the total area ( km ² ) of pangasinan .	SELECT t1.district , SUM ( t1.area_ ) from pangasinan as t1 GROUP BY t1.district	1-1691800-2
what are the municipality more than 10 pangasinan have ?	SELECT t1.municipality from pangasinan as t1 GROUP BY t1.municipality HAVING COUNT ( * ) > 10	1-1691800-2
find the municipality and district of the pangasinan whose population ( 2010 ) is lower than the average population ( 2010 ) of all pangasinan .	SELECT t1.municipality , t1.district from pangasinan as t1 WHERE t1.population_ < ( SELECT AVG ( t1.population_ ) {FROM, 3} )	1-1691800-2
what are the municipality and municipality ?	SELECT t1.municipality , t1.municipality from pangasinan as t1	1-1691800-2
show the income class with fewer than 3 pangasinan .	SELECT t1.income_class from pangasinan as t1 GROUP BY t1.income_class HAVING COUNT ( * ) < 3	1-1691800-2
what is the municipality of all pangasinan whose area ( km ² ) is higher than any pangasinan ?	SELECT t1.municipality from pangasinan as t1 WHERE t1.area_ > ( SELECT MIN ( t1.area_ ) from pangasinan as t1 )	1-1691800-2
find the district and municipality of the pangasinan whose area ( km ² ) is lower than the average area ( km ² ) of all pangasinan .	SELECT t1.district , t1.municipality from pangasinan as t1 WHERE t1.area_ < ( SELECT AVG ( t1.area_ ) {FROM, 3} )	1-1691800-2
show all income class and corresponding number of pangasinan in the ascending order of the numbers.	SELECT t1.income_class , COUNT ( * ) from pangasinan as t1 GROUP BY t1.income_class ORDER BY COUNT ( * )	1-1691800-2
what are the district with exactly 10 pangasinan ?	SELECT t1.district from pangasinan as t1 GROUP BY t1.district HAVING COUNT ( * ) = 10	1-1691800-2
