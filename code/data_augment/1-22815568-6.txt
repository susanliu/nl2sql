sort the list of status and status of all list in the descending order of population .	SELECT t1.status , t1.status from list as t1 ORDER BY t1.population DESC	1-22815568-6
find the status and unemployment rate of the list whose population is lower than the average population of all list .	SELECT t1.status , t1.unemployment_rate from list as t1 WHERE t1.population < ( SELECT AVG ( t1.population ) {FROM, 3} )	1-22815568-6
give the maximum and minimum population of all list .	SELECT MAX ( t1.population ) , MIN ( t1.population ) from list as t1	1-22815568-6
what are the average population of list for different county ?	SELECT AVG ( t1.population ) , t1.county from list as t1 GROUP BY t1.county	1-22815568-6
what are the unemployment rate of all list with poverty rate that is 10 ?	SELECT t1.unemployment_rate from list as t1 GROUP BY t1.poverty_rate HAVING COUNT ( * ) = 10	1-22815568-6
how many different status correspond to each county ?	SELECT t1.county , COUNT ( DISTINCT t1.status ) from list as t1 GROUP BY t1.county	1-22815568-6
show all information on the list that has the largest number of status.	SELECT * from list as t1 ORDER BY t1.status DESC LIMIT 1	1-22815568-6
what is the maximum and mininum population {COLUMN} for all list ?	SELECT MAX ( t1.population ) , MIN ( t1.population ) from list as t1	1-22815568-6
how many different county correspond to each county ?	SELECT t1.county , COUNT ( DISTINCT t1.county ) from list as t1 GROUP BY t1.county	1-22815568-6
what are the poverty rate , unemployment rate , and unemployment rate for each list ?	SELECT t1.poverty_rate , t1.unemployment_rate , t1.unemployment_rate from list as t1	1-22815568-6
