show the original airdate with fewer than 3 none .	SELECT t1.original_airdate from none as t1 GROUP BY t1.original_airdate HAVING COUNT ( * ) < 3	1-12919003-3
find the viewers ( millions ) and writer of the none with at least 10 writer .	SELECT t1.viewers_ , t1.writer from none as t1 GROUP BY t1.writer HAVING COUNT ( * ) >= 10	1-12919003-3
what are the viewers ( millions ) for none who have more than the average #?	SELECT t1.viewers_ from none as t1 WHERE t1.# > ( SELECT AVG ( t1.# ) from none as t1	1-12919003-3
find the director of none which have 10 but no 92 as writer .	SELECT t1.director from none as t1 WHERE t1.writer = 10 EXCEPT SELECT t1.director from none as t1 WHERE t1.writer = 92	1-12919003-3
what is all the information on the none with the largest number of viewers ( millions ) ?	SELECT * from none as t1 ORDER BY t1.viewers_ DESC LIMIT 1	1-12919003-3
show the writer , writer , and viewers ( millions ) of all the none .	SELECT t1.writer , t1.writer , t1.viewers_ from none as t1	1-12919003-3
which episode has most number of none ?	SELECT t1.episode from none as t1 GROUP BY t1.episode ORDER BY COUNT ( * ) DESC LIMIT 1	1-12919003-3
show episode and director of none .	SELECT t1.episode , t1.director from none as t1	1-12919003-3
what is the writer of all none whose # is higher than any none ?	SELECT t1.writer from none as t1 WHERE t1.# > ( SELECT MIN ( t1.# ) from none as t1 )	1-12919003-3
what is the original airdate and viewers ( millions ) of the none with the top 5 smallest # ?	SELECT t1.original_airdate , t1.viewers_ from none as t1 ORDER BY t1.# LIMIT 5	1-12919003-3
