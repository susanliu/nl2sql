what are the average year set of erica for different event ?	SELECT AVG ( t1.year_set ) , t1.event from erica as t1 GROUP BY t1.event	2-16209355-1
return the smallest year set for every event .	SELECT MIN ( t1.year_set ) , t1.event from erica as t1 GROUP BY t1.event	2-16209355-1
what are the meet of erica whose meet are not 10 ?	SELECT t1.meet from erica as t1 WHERE t1.meet ! = 10	2-16209355-1
what is the event of the erica with the largest year set ?	SELECT t1.event from erica as t1 WHERE t1.year_set = ( SELECT MAX ( t1.year_set ) from erica as t1 )	2-16209355-1
Show everything on erica	SELECT * FROM erica	2-16209355-1
what are the meet of the erica with meet other than 10 ?	SELECT t1.meet from erica as t1 WHERE t1.time ! = 10	2-16209355-1
please list the time and long course/short course of erica in descending order of year set .	SELECT t1.time , t1.long_course/short_course from erica as t1 ORDER BY t1.year_set DESC	2-16209355-1
how many erica are there in long course/short course 10 or 10 ?	SELECT COUNT ( * ) from erica as t1 WHERE t1.long_course/short_course = 10 OR t1.long_course/short_course = 10	2-16209355-1
show the time and their total year set of erica .	SELECT t1.time , SUM ( t1.year_set ) from erica as t1 GROUP BY t1.time	2-16209355-1
what are the meet of all erica with long course/short course that is 10 ?	SELECT t1.meet from erica as t1 GROUP BY t1.long_course/short_course HAVING COUNT ( * ) = 10	2-16209355-1
