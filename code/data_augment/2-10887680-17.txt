what are the away team score of all 1978 with away team score that is 10 ?	SELECT t1.away_team_score from 1978 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) = 10	2-10887680-17
what are the home team score and home team score of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.home_team_score , t1.home_team_score from 1978 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10887680-17
which home team score have an average crowd over 10 ?	SELECT t1.home_team_score from 1978 as t1 GROUP BY t1.home_team_score HAVING AVG ( t1.crowd ) >= 10	2-10887680-17
what are the home team and home team score of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.home_team , t1.home_team_score from 1978 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10887680-17
how many 1978 are there in away team 10 or 26 ?	SELECT COUNT ( * ) from 1978 as t1 WHERE t1.away_team = 10 OR t1.away_team = 26	2-10887680-17
what are the away team score and away team of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.away_team_score , t1.away_team from 1978 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10887680-17
what are the home team that have greater crowd than any crowd in 1978 ?	SELECT t1.home_team from 1978 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1978 as t1 )	2-10887680-17
find all venue that have fewer than three in 1978 .	SELECT t1.venue from 1978 as t1 GROUP BY t1.venue HAVING COUNT ( * ) < 3	2-10887680-17
show the date shared by more than 10 1978 .	SELECT t1.date from 1978 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10	2-10887680-17
give the t1.date with the fewest 1978 .	SELECT t1.date from 1978 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) LIMIT 1	2-10887680-17
