what is the object type and declination ( j2000 ) of the list with maximum ngc number ?	SELECT t1.object_type , t1.declination_ from list as t1 WHERE t1.ngc_number = ( SELECT MAX ( t1.ngc_number ) {FROM, 3} )	2-11097664-1
what is the constellation of the list with the largest ngc number ?	SELECT t1.constellation from list as t1 WHERE t1.ngc_number = ( SELECT MAX ( t1.ngc_number ) from list as t1 )	2-11097664-1
find the object type of list which are constellation 10 but not constellation 75 .	SELECT t1.object_type from list as t1 WHERE t1.constellation = 10 EXCEPT SELECT t1.object_type from list as t1 WHERE t1.constellation = 75	2-11097664-1
find the right ascension ( j2000 ) of list who have ngc number of both 10 and 12 .	SELECT t1.right_ascension_ from list as t1 WHERE t1.ngc_number = 10 INTERSECT SELECT t1.right_ascension_ from list as t1 WHERE t1.ngc_number = 12	2-11097664-1
show the object type and the number of list for each object type in the ascending order.	SELECT t1.object_type , COUNT ( * ) from list as t1 GROUP BY t1.object_type ORDER BY COUNT ( * )	2-11097664-1
Return all columns in list .	SELECT * FROM list	2-11097664-1
which constellation has both list with less than 10 ngc number and list with more than 53 ngc number ?	SELECT t1.constellation from list as t1 WHERE t1.ngc_number < 10 INTERSECT SELECT t1.constellation from list as t1 WHERE t1.ngc_number > 53	2-11097664-1
find the constellation of the list with the highest ngc number .	SELECT t1.constellation from list as t1 ORDER BY t1.ngc_number DESC LIMIT 1	2-11097664-1
find the distinct constellation of list having ngc number between 10 and 36 .	SELECT DISTINCT t1.constellation from list as t1 WHERE t1.ngc_number BETWEEN 10 AND 36	2-11097664-1
what are the object type and object type of the {COLUMN} who have ngc number above five or ngc number below ten ?	SELECT t1.object_type , t1.object_type from list as t1 WHERE t1.ngc_number > 5 OR t1.ngc_number < 10	2-11097664-1
