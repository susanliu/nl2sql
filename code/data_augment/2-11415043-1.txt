show the nation and the corresponding number of 2007 sorted by the number of nation in ascending order .	SELECT t1.nation , COUNT ( * ) from 2007 as t1 GROUP BY t1.nation ORDER BY COUNT ( * )	2-11415043-1
find the number of 2007 that have more than 10 nation .	SELECT COUNT ( * ) from 2007 as t1 GROUP BY t1.nation HAVING COUNT ( * ) > 10 	2-11415043-1
what are the nation of all 2007 with nation that is 10 ?	SELECT t1.nation from 2007 as t1 GROUP BY t1.nation HAVING COUNT ( * ) = 10	2-11415043-1
count the number of 2007 in nation 10 or 45 .	SELECT COUNT ( * ) from 2007 as t1 WHERE t1.nation = 10 OR t1.nation = 45	2-11415043-1
what is the nation of highest total ?	SELECT t1.nation from 2007 as t1 ORDER BY t1.total DESC LIMIT 1	2-11415043-1
what is the nation of highest gold ?	SELECT t1.nation from 2007 as t1 ORDER BY t1.gold DESC LIMIT 1	2-11415043-1
how many 2007 are there in nation 10 or 12 ?	SELECT COUNT ( * ) from 2007 as t1 WHERE t1.nation = 10 OR t1.nation = 12	2-11415043-1
return the different nation of 2007 , in ascending order of frequency .	SELECT t1.nation from 2007 as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) ASC LIMIT 1	2-11415043-1
find the distinct nation of 2007 having gold between 10 and 33 .	SELECT DISTINCT t1.nation from 2007 as t1 WHERE t1.gold BETWEEN 10 AND 33	2-11415043-1
give the nation that has the most 2007 .	SELECT t1.nation from 2007 as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) DESC LIMIT 1	2-11415043-1
