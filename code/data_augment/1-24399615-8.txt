list the airdate which average viewers is above 10 .	SELECT t1.airdate from russell as t1 GROUP BY t1.airdate HAVING AVG ( t1.viewers ) >= 10	1-24399615-8
select the average episode no . of each russell 's bbc three weekly ranking .	SELECT AVG ( t1.episode_no_ ) , t1.bbc_three_weekly_ranking from russell as t1 GROUP BY t1.bbc_three_weekly_ranking	1-24399615-8
what is the bbc three weekly ranking and cable rank of every russell that has a episode no . lower than average ?	SELECT t1.bbc_three_weekly_ranking , t1.cable_rank from russell as t1 WHERE t1.episode_no_ < ( SELECT AVG ( t1.episode_no_ ) {FROM, 3} )	1-24399615-8
return the different cable rank of russell , in ascending order of frequency .	SELECT t1.cable_rank from russell as t1 GROUP BY t1.cable_rank ORDER BY COUNT ( * ) ASC LIMIT 1	1-24399615-8
list the cable rank which average episode no . is above 10 .	SELECT t1.cable_rank from russell as t1 GROUP BY t1.cable_rank HAVING AVG ( t1.episode_no_ ) >= 10	1-24399615-8
what are the cable rank and cable rank of the {COLUMN} who have viewers above five or episode no . below ten ?	SELECT t1.cable_rank , t1.cable_rank from russell as t1 WHERE t1.viewers > 5 OR t1.episode_no_ < 10	1-24399615-8
what are the cable rank that have greater episode no . than any episode no . in russell ?	SELECT t1.cable_rank from russell as t1 WHERE t1.episode_no_ > ( SELECT MIN ( t1.episode_no_ ) from russell as t1 )	1-24399615-8
what is the maximum and mininum episode no . {COLUMN} for all russell ?	SELECT MAX ( t1.episode_no_ ) , MIN ( t1.episode_no_ ) from russell as t1	1-24399615-8
return the airdate of the russell with the fewest viewers .	SELECT t1.airdate from russell as t1 ORDER BY t1.viewers ASC LIMIT 1	1-24399615-8
return the maximum and minimum viewers across all russell .	SELECT MAX ( t1.viewers ) , MIN ( t1.viewers ) from russell as t1	1-24399615-8
