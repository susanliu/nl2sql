which womens singles has both austrian with less than 10 year and austrian with more than 51 year ?	SELECT t1.womens_singles from austrian as t1 WHERE t1.year < 10 INTERSECT SELECT t1.womens_singles from austrian as t1 WHERE t1.year > 51	1-15002265-1
what is the womens doubles of austrian with the maximum year across all austrian ?	SELECT t1.womens_doubles from austrian as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from austrian as t1 )	1-15002265-1
what is the mixed doubles of austrian with the maximum year across all austrian ?	SELECT t1.mixed_doubles from austrian as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from austrian as t1 )	1-15002265-1
what is the mixed doubles and mens doubles for the austrian with the rank 5 smallest year ?	SELECT t1.mixed_doubles , t1.mens_doubles from austrian as t1 ORDER BY t1.year LIMIT 5	1-15002265-1
what is the womens doubles of the austrian with the minimum year ?	SELECT t1.womens_doubles from austrian as t1 ORDER BY t1.year ASC LIMIT 1	1-15002265-1
list the mixed doubles , womens doubles and the mens doubles of the austrian .	SELECT t1.mixed_doubles , t1.womens_doubles , t1.mens_doubles from austrian as t1	1-15002265-1
show the womens singles shared by more than 10 austrian .	SELECT t1.womens_singles from austrian as t1 GROUP BY t1.womens_singles HAVING COUNT ( * ) > 10	1-15002265-1
return the mens singles and womens doubles of austrian with the five lowest year .	SELECT t1.mens_singles , t1.womens_doubles from austrian as t1 ORDER BY t1.year LIMIT 5	1-15002265-1
what is the mens doubles of all austrian whose year is higher than any austrian ?	SELECT t1.mens_doubles from austrian as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from austrian as t1 )	1-15002265-1
which mens doubles have less than 3 in austrian ?	SELECT t1.mens_doubles from austrian as t1 GROUP BY t1.mens_doubles HAVING COUNT ( * ) < 3	1-15002265-1
