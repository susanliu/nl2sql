Show everything on hidehiko	SELECT * FROM hidehiko	2-1472898-2
what is the res . and time of the hidehiko with the top 5 smallest round ?	SELECT t1.res_ , t1.time from hidehiko as t1 ORDER BY t1.round LIMIT 5	2-1472898-2
find the time of the hidehiko with the largest round .	SELECT t1.time from hidehiko as t1 ORDER BY t1.round DESC LIMIT 1	2-1472898-2
what is the opponent and record of every hidehiko that has a round lower than average ?	SELECT t1.opponent , t1.record from hidehiko as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-1472898-2
show the method and their total round of hidehiko .	SELECT t1.method , SUM ( t1.round ) from hidehiko as t1 GROUP BY t1.method	2-1472898-2
what are the event and record of the {COLUMN} who have round above five or round below ten ?	SELECT t1.event , t1.record from hidehiko as t1 WHERE t1.round > 5 OR t1.round < 10	2-1472898-2
find the number of hidehiko that have more than 10 res . .	SELECT COUNT ( * ) from hidehiko as t1 GROUP BY t1.res_ HAVING COUNT ( * ) > 10 	2-1472898-2
which opponent has both hidehiko with less than 10 round and hidehiko with more than 38 round ?	SELECT t1.opponent from hidehiko as t1 WHERE t1.round < 10 INTERSECT SELECT t1.opponent from hidehiko as t1 WHERE t1.round > 38	2-1472898-2
find the method and event of the hidehiko with at least 10 method .	SELECT t1.method , t1.event from hidehiko as t1 GROUP BY t1.method HAVING COUNT ( * ) >= 10	2-1472898-2
display the event , record , and res . for each hidehiko .	SELECT t1.event , t1.record , t1.res_ from hidehiko as t1	2-1472898-2
