what are the away team of all 1974 that have 10 or more 1974 ?	SELECT t1.away_team from 1974 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) >= 10	2-10869646-9
what is the venue of the 1974 who has the highest number of 1974 ?	SELECT t1.venue from 1974 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) DESC LIMIT 1	2-10869646-9
which away team has both 1974 with less than 10 crowd and 1974 with more than 79 crowd ?	SELECT t1.away_team from 1974 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.away_team from 1974 as t1 WHERE t1.crowd > 79	2-10869646-9
find all date that have fewer than three in 1974 .	SELECT t1.date from 1974 as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	2-10869646-9
return the away team score and away team score of 1974 with the five lowest crowd .	SELECT t1.away_team_score , t1.away_team_score from 1974 as t1 ORDER BY t1.crowd LIMIT 5	2-10869646-9
what is the home team score of the most common 1974 in all home team score ?	SELECT t1.home_team_score from 1974 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-10869646-9
what are the away team score for 1974 that have an crowd greater than the average .	SELECT t1.away_team_score from 1974 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1974 as t1	2-10869646-9
what are the distinct home team score with crowd between 10 and 23 ?	SELECT DISTINCT t1.home_team_score from 1974 as t1 WHERE t1.crowd BETWEEN 10 AND 23	2-10869646-9
what is the minimum crowd in each venue ?	SELECT MIN ( t1.crowd ) , t1.venue from 1974 as t1 GROUP BY t1.venue	2-10869646-9
what is the home team score of all 1974 whose crowd is higher than any 1974 ?	SELECT t1.home_team_score from 1974 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1974 as t1 )	2-10869646-9
