which venue has most number of 1959 ?	SELECT t1.venue from 1959 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) DESC LIMIT 1	2-10775038-17
what are the home team and home team score ?	SELECT t1.home_team , t1.home_team_score from 1959 as t1	2-10775038-17
show all home team score and corresponding number of 1959 in the ascending order of the numbers.	SELECT t1.home_team_score , COUNT ( * ) from 1959 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * )	2-10775038-17
which venue is the most frequent venue?	SELECT t1.venue from 1959 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) DESC LIMIT 1	2-10775038-17
list away team score of 1959 that have the number of 1959 greater than 10 .	SELECT t1.away_team_score from 1959 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10	2-10775038-17
show the home team with fewer than 3 1959 .	SELECT t1.home_team from 1959 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) < 3	2-10775038-17
what is the home team of all 1959 whose crowd is higher than any 1959 ?	SELECT t1.home_team from 1959 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1959 as t1 )	2-10775038-17
find all venue that have fewer than three in 1959 .	SELECT t1.venue from 1959 as t1 GROUP BY t1.venue HAVING COUNT ( * ) < 3	2-10775038-17
show the away team score shared by more than 10 1959 .	SELECT t1.away_team_score from 1959 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10	2-10775038-17
how many 1959 have away team that contain the word 10 ?	SELECT COUNT ( * ) from 1959 as t1 WHERE t1.away_team LIKE 10	2-10775038-17
