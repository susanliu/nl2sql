what is the player of the list with the minimum overall ?	SELECT t1.player from list as t1 ORDER BY t1.overall ASC LIMIT 1	2-11803648-10
what are the player of all list with club team that is 10 ?	SELECT t1.player from list as t1 GROUP BY t1.club_team HAVING COUNT ( * ) = 10	2-11803648-10
give the maximum and minimum round of all list .	SELECT MAX ( t1.round ) , MIN ( t1.round ) from list as t1	2-11803648-10
what is the club team and nationality for the list with the rank 5 smallest round ?	SELECT t1.club_team , t1.nationality from list as t1 ORDER BY t1.round LIMIT 5	2-11803648-10
show all nationality and the total round for each .	SELECT t1.nationality , SUM ( t1.round ) from list as t1 GROUP BY t1.nationality	2-11803648-10
what is the nationality and player of the list with maximum overall ?	SELECT t1.nationality , t1.player from list as t1 WHERE t1.overall = ( SELECT MAX ( t1.overall ) {FROM, 3} )	2-11803648-10
what is the nationality and nationality of the list with maximum overall ?	SELECT t1.nationality , t1.nationality from list as t1 WHERE t1.overall = ( SELECT MAX ( t1.overall ) {FROM, 3} )	2-11803648-10
what are the player and nationality of each list ?	SELECT t1.player , t1.nationality from list as t1	2-11803648-10
select the average overall of each list 's player .	SELECT AVG ( t1.overall ) , t1.player from list as t1 GROUP BY t1.player	2-11803648-10
what are the distinct COLUMN_NAME,0} of every list that has a greater overall than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.club_team from list as t1 WHERE t1.overall > ( SELECT MIN ( t1.club_team ) from list as t1 WHERE t1.player = 10 )	2-11803648-10
