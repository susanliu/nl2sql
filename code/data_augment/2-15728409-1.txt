how many different fcc info correspond to each call sign ?	SELECT t1.call_sign , COUNT ( DISTINCT t1.fcc_info ) from kruc as t1 GROUP BY t1.call_sign	2-15728409-1
find the distinct frequency mhz of kruc having erp w between 10 and 35 .	SELECT DISTINCT t1.frequency_mhz from kruc as t1 WHERE t1.erp_w BETWEEN 10 AND 35	2-15728409-1
which call sign have an average erp w over 10 ?	SELECT t1.call_sign from kruc as t1 GROUP BY t1.call_sign HAVING AVG ( t1.erp_w ) >= 10	2-15728409-1
please show the frequency mhz of the kruc that have at least 10 records .	SELECT t1.frequency_mhz from kruc as t1 GROUP BY t1.frequency_mhz HAVING COUNT ( * ) >= 10	2-15728409-1
return the smallest erp w for every call sign .	SELECT MIN ( t1.erp_w ) , t1.call_sign from kruc as t1 GROUP BY t1.call_sign	2-15728409-1
which call sign have greater erp w than that of any erp w in kruc ?	SELECT t1.call_sign from kruc as t1 WHERE t1.erp_w > ( SELECT MIN ( t1.erp_w ) from kruc as t1 )	2-15728409-1
which city of license has both kruc with less than 10 erp w and kruc with more than 49 erp w ?	SELECT t1.city_of_license from kruc as t1 WHERE t1.erp_w < 10 INTERSECT SELECT t1.city_of_license from kruc as t1 WHERE t1.erp_w > 49	2-15728409-1
find the call sign that have 10 kruc .	SELECT t1.call_sign from kruc as t1 GROUP BY t1.call_sign HAVING COUNT ( * ) = 10	2-15728409-1
what is the minimum erp w in each frequency mhz ?	SELECT MIN ( t1.erp_w ) , t1.frequency_mhz from kruc as t1 GROUP BY t1.frequency_mhz	2-15728409-1
show the city of license and the total erp w of kruc .	SELECT t1.city_of_license , SUM ( t1.erp_w ) from kruc as t1 GROUP BY t1.city_of_license	2-15728409-1
