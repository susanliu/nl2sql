show the nfc and camera with at least 10 android version .	SELECT t1.nfc , t1.camera from sony as t1 GROUP BY t1.android_version HAVING COUNT ( * ) >= 10	1-23556331-4
what is the display of highest battery ( mah ) ?	SELECT t1.display from sony as t1 ORDER BY t1.battery_ DESC LIMIT 1	1-23556331-4
list all information about sony .	SELECT * FROM sony	1-23556331-4
return the nfc of sony for which the rom is not 10 ?	SELECT t1.nfc from sony as t1 WHERE t1.rom ! = 10	1-23556331-4
what are the average battery ( mah ) of sony , grouped by system on chip ?	SELECT AVG ( t1.battery_ ) , t1.system_on_chip from sony as t1 GROUP BY t1.system_on_chip	1-23556331-4
find the weight of sony who have both 10 and 96 battery ( mah ) .	SELECT t1.weight from sony as t1 WHERE t1.battery_ = 10 INTERSECT SELECT t1.weight from sony as t1 WHERE t1.battery_ = 96	1-23556331-4
select the average battery ( mah ) of each sony 's release date .	SELECT AVG ( t1.battery_ ) , t1.release_date from sony as t1 GROUP BY t1.release_date	1-23556331-4
what is the bluetooth of highest battery ( mah ) ?	SELECT t1.bluetooth from sony as t1 ORDER BY t1.battery_ DESC LIMIT 1	1-23556331-4
what are the platform of all sony with ram that is 10 ?	SELECT t1.platform from sony as t1 GROUP BY t1.ram HAVING COUNT ( * ) = 10	1-23556331-4
sort the list of nfc and wi-fi of all sony in the descending order of battery ( mah ) .	SELECT t1.nfc , t1.wi-fi from sony as t1 ORDER BY t1.battery_ DESC	1-23556331-4
