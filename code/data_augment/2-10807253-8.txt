how many 1940 are there in home team 10 or 7 ?	SELECT COUNT ( * ) from 1940 as t1 WHERE t1.home_team = 10 OR t1.home_team = 7	2-10807253-8
show all home team and corresponding number of 1940 sorted by the count .	SELECT t1.home_team , COUNT ( * ) from 1940 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * )	2-10807253-8
what are the distinct COLUMN_NAME,0} of every 1940 that has a greater crowd than some 1940 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.date from 1940 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.date ) from 1940 as t1 WHERE t1.venue = 10 )	2-10807253-8
what are the away team of 1940 , sorted by their frequency?	SELECT t1.away_team from 1940 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10807253-8
list the away team score and home team score of all 1940 sorted by crowd in descending order .	SELECT t1.away_team_score , t1.home_team_score from 1940 as t1 ORDER BY t1.crowd DESC	2-10807253-8
which venue has both 1940 with less than 10 crowd and 1940 with more than 29 crowd ?	SELECT t1.venue from 1940 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.venue from 1940 as t1 WHERE t1.crowd > 29	2-10807253-8
how many 1940 have venue that contain the word 10 ?	SELECT COUNT ( * ) from 1940 as t1 WHERE t1.venue LIKE 10	2-10807253-8
find the venue of 1940 who have both 10 and 44 crowd .	SELECT t1.venue from 1940 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.venue from 1940 as t1 WHERE t1.crowd = 44	2-10807253-8
what is the home team of 1940 with the maximum crowd across all 1940 ?	SELECT t1.home_team from 1940 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1940 as t1 )	2-10807253-8
what is the maximum and mininum crowd {COLUMN} for all 1940 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1940 as t1	2-10807253-8
