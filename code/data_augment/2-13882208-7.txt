find the score of the 2008 which have date 10 but not 70 .	SELECT t1.score from 2008 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.score from 2008 as t1 WHERE t1.date = 70	2-13882208-7
return the record of the 2008 that has the fewest corresponding record .	SELECT t1.record from 2008 as t1 GROUP BY t1.record ORDER BY COUNT ( * ) ASC LIMIT 1	2-13882208-7
what is the opponent and record for the 2008 with the rank 5 smallest attendance ?	SELECT t1.opponent , t1.record from 2008 as t1 ORDER BY t1.attendance LIMIT 5	2-13882208-7
what is the count of 2008 with more than 10 opponent ?	SELECT COUNT ( * ) from 2008 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	2-13882208-7
what is the opponent and record of the 2008 with maximum attendance ?	SELECT t1.opponent , t1.record from 2008 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) {FROM, 3} )	2-13882208-7
list record and date who have attendance greater than 5 or attendance shorter than 10 .	SELECT t1.record , t1.date from 2008 as t1 WHERE t1.attendance > 5 OR t1.attendance < 10	2-13882208-7
return the record of the largest attendance.	SELECT t1.record from 2008 as t1 ORDER BY t1.attendance DESC LIMIT 1	2-13882208-7
find the score of 2008 which are in 10 date but not in 12 date .	SELECT t1.score from 2008 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.score from 2008 as t1 WHERE t1.date = 12	2-13882208-7
what is the record of the 2008 with the smallest attendance ?	SELECT t1.record from 2008 as t1 ORDER BY t1.attendance ASC LIMIT 1	2-13882208-7
return the record of the largest attendance.	SELECT t1.record from 2008 as t1 ORDER BY t1.attendance DESC LIMIT 1	2-13882208-7
