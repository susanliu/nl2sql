show the longitude with fewer than 3 list .	SELECT t1.longitude from list as t1 GROUP BY t1.longitude HAVING COUNT ( * ) < 3	2-16799784-4
what are the name of all list with name that is 10 ?	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) = 10	2-16799784-4
what are the name of all list that have 10 or more list ?	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) >= 10	2-16799784-4
what are the distinct COLUMN_NAME,0} of list with diameter ( km ) higher than any list from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.longitude from list as t1 WHERE t1.diameter_ > ( SELECT MIN ( t1.longitude ) from list as t1 WHERE t1.name = 10 )	2-16799784-4
find the name and longitude of the list whose diameter ( km ) is lower than the average diameter ( km ) of all list .	SELECT t1.name , t1.longitude from list as t1 WHERE t1.diameter_ < ( SELECT AVG ( t1.diameter_ ) {FROM, 3} )	2-16799784-4
what are the distinct COLUMN_NAME,0} of every list that has a greater diameter ( km ) than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.longitude from list as t1 WHERE t1.diameter_ > ( SELECT MIN ( t1.longitude ) from list as t1 WHERE t1.latitude = 10 )	2-16799784-4
list the longitude and year named of all list sorted by diameter ( km ) in descending order .	SELECT t1.longitude , t1.year_named from list as t1 ORDER BY t1.diameter_ DESC	2-16799784-4
how many list correspond to each latitude? show the result in ascending order.	SELECT t1.latitude , COUNT ( * ) from list as t1 GROUP BY t1.latitude ORDER BY COUNT ( * )	2-16799784-4
list all information about list .	SELECT * FROM list	2-16799784-4
give the year named that has the most list .	SELECT t1.year_named from list as t1 GROUP BY t1.year_named ORDER BY COUNT ( * ) DESC LIMIT 1	2-16799784-4
