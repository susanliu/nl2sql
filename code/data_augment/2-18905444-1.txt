how many distinct kilkenny score correspond to each competition ?	SELECT t1.competition , COUNT ( DISTINCT t1.kilkenny_score ) from 2008 as t1 GROUP BY t1.competition	2-18905444-1
find the waterford score of the 2008 who has the largest number of 2008 .	SELECT t1.waterford_score from 2008 as t1 GROUP BY t1.waterford_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-18905444-1
what are the kilkenny score more than 10 2008 have ?	SELECT t1.kilkenny_score from 2008 as t1 GROUP BY t1.kilkenny_score HAVING COUNT ( * ) > 10	2-18905444-1
what is all the information on the 2008 with the largest number of venue ?	SELECT * from 2008 as t1 ORDER BY t1.venue DESC LIMIT 1	2-18905444-1
what are the venue of all 2008 with waterford score that is 10 ?	SELECT t1.venue from 2008 as t1 GROUP BY t1.waterford_score HAVING COUNT ( * ) = 10	2-18905444-1
count the number of 2008 in competition 10 or 2 .	SELECT COUNT ( * ) from 2008 as t1 WHERE t1.competition = 10 OR t1.competition = 2	2-18905444-1
list the kilkenny score which average year is above 10 .	SELECT t1.kilkenny_score from 2008 as t1 GROUP BY t1.kilkenny_score HAVING AVG ( t1.year ) >= 10	2-18905444-1
what is the competition and competition of the 2008 with the top 5 smallest year ?	SELECT t1.competition , t1.competition from 2008 as t1 ORDER BY t1.year LIMIT 5	2-18905444-1
what is the kilkenny score of all 2008 whose year is higher than any 2008 ?	SELECT t1.kilkenny_score from 2008 as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from 2008 as t1 )	2-18905444-1
return the different kilkenny score of 2008 , in ascending order of frequency .	SELECT t1.kilkenny_score from 2008 as t1 GROUP BY t1.kilkenny_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-18905444-1
