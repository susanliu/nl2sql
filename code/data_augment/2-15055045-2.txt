which team have an average laps over 10 ?	SELECT t1.team from 2005 as t1 GROUP BY t1.team HAVING AVG ( t1.laps ) >= 10	2-15055045-2
what is the time/retired of all 2005 whose laps is higher than any 2005 ?	SELECT t1.time/retired from 2005 as t1 WHERE t1.laps > ( SELECT MIN ( t1.laps ) from 2005 as t1 )	2-15055045-2
what is the driver of 2005 with the maximum grid across all 2005 ?	SELECT t1.driver from 2005 as t1 WHERE t1.grid = ( SELECT MAX ( t1.grid ) from 2005 as t1 )	2-15055045-2
what is the minimum laps in each time/retired ?	SELECT MIN ( t1.laps ) , t1.time/retired from 2005 as t1 GROUP BY t1.time/retired	2-15055045-2
count the number of 2005 in team 10 or 23 .	SELECT COUNT ( * ) from 2005 as t1 WHERE t1.team = 10 OR t1.team = 23	2-15055045-2
what is the minimum grid in each time/retired ?	SELECT MIN ( t1.grid ) , t1.time/retired from 2005 as t1 GROUP BY t1.time/retired	2-15055045-2
what is the minimum laps in each time/retired ?	SELECT MIN ( t1.laps ) , t1.time/retired from 2005 as t1 GROUP BY t1.time/retired	2-15055045-2
please list the team and driver of 2005 in descending order of grid .	SELECT t1.team , t1.driver from 2005 as t1 ORDER BY t1.grid DESC	2-15055045-2
show the time/retired and the number of unique driver containing each time/retired .	SELECT t1.time/retired , COUNT ( DISTINCT t1.driver ) from 2005 as t1 GROUP BY t1.time/retired	2-15055045-2
show the driver and the total laps of 2005 .	SELECT t1.driver , SUM ( t1.laps ) from 2005 as t1 GROUP BY t1.driver	2-15055045-2
