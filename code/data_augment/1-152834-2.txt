what is the maximum and mininum tujia population {COLUMN} for all tujia ?	SELECT MAX ( t1.tujia_population ) , MIN ( t1.tujia_population ) from tujia as t1	1-152834-2
what is the prefecture of all tujia whose tujia population is higher than any tujia ?	SELECT t1.prefecture from tujia as t1 WHERE t1.tujia_population > ( SELECT MIN ( t1.tujia_population ) from tujia as t1 )	1-152834-2
return the smallest tujia population for every % of chinas tujia population .	SELECT MIN ( t1.tujia_population ) , t1.% from tujia as t1 GROUP BY t1.%	1-152834-2
show the province and % of chinas tujia population with at least 10 prefecture .	SELECT t1.province , t1.% from tujia as t1 GROUP BY t1.prefecture HAVING COUNT ( * ) >= 10	1-152834-2
list county of tujia that have the number of tujia greater than 10 .	SELECT t1.county from tujia as t1 GROUP BY t1.county HAVING COUNT ( * ) > 10	1-152834-2
return the different province of tujia , in ascending order of frequency .	SELECT t1.province from tujia as t1 GROUP BY t1.province ORDER BY COUNT ( * ) ASC LIMIT 1	1-152834-2
find the province which have exactly 10 tujia .	SELECT t1.province from tujia as t1 GROUP BY t1.province HAVING COUNT ( * ) = 10	1-152834-2
what is the province of the tujia with the smallest tujia population ?	SELECT t1.province from tujia as t1 ORDER BY t1.tujia_population ASC LIMIT 1	1-152834-2
show the province and the corresponding number of tujia sorted by the number of province in ascending order .	SELECT t1.province , COUNT ( * ) from tujia as t1 GROUP BY t1.province ORDER BY COUNT ( * )	1-152834-2
which % of chinas tujia population has the least tujia population ?	SELECT t1.% from tujia as t1 ORDER BY t1.tujia_population ASC LIMIT 1	1-152834-2
