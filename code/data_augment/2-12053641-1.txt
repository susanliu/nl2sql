what are the uk indie singles chart and uk singles chart of all operator sorted by decreasing year ?	SELECT t1.uk_indie_singles_chart , t1.uk_singles_chart from operator as t1 ORDER BY t1.year DESC	2-12053641-1
show the album of operator who have at least 10 operator .	SELECT t1.album from operator as t1 GROUP BY t1.album HAVING COUNT ( * ) >= 10	2-12053641-1
what is the aria singles chart of all operator whose year is higher than any operator ?	SELECT t1.aria_singles_chart from operator as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from operator as t1 )	2-12053641-1
show uk indie singles chart and triple j hottest 100 of operator .	SELECT t1.uk_indie_singles_chart , t1.triple_j_hottest_100 from operator as t1	2-12053641-1
what is the uk indie singles chart and album of the operator with maximum year ?	SELECT t1.uk_indie_singles_chart , t1.album from operator as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-12053641-1
how many operator are there that have more than {VALUE},0 uk singles chart ?	SELECT COUNT ( * ) from operator as t1 GROUP BY t1.uk_singles_chart HAVING COUNT ( * ) > 10 	2-12053641-1
count the number of operator that have an song containing 10 .	SELECT COUNT ( * ) from operator as t1 WHERE t1.song LIKE 10	2-12053641-1
find the aria singles chart for all operator who have more than the average year .	SELECT t1.aria_singles_chart from operator as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from operator as t1	2-12053641-1
what is the song and triple j hottest 100 of every operator that has a year lower than average ?	SELECT t1.song , t1.triple_j_hottest_100 from operator as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-12053641-1
what are the uk singles chart of the operator with uk singles chart other than 10 ?	SELECT t1.uk_singles_chart from operator as t1 WHERE t1.uk_singles_chart ! = 10	2-12053641-1
