what are the straight to # 1 ? and number-one single ( s ) of the {COLUMN} who have weeks at # 1 above five or year below ten ?	SELECT t1.straight_to_ , t1.number-one_single_ from list as t1 WHERE t1.weeks_at_ > 5 OR t1.year < 10	1-27441210-11
what are the straight to # 1 ? , country , and number-one single ( s ) for each list ?	SELECT t1.straight_to_ , t1.country , t1.number-one_single_ from list as t1	1-27441210-11
what is the number-one single ( s ) and straight to # 1 ? for the list with the rank 5 smallest weeks at # 1 ?	SELECT t1.number-one_single_ , t1.straight_to_ from list as t1 ORDER BY t1.weeks_at_ LIMIT 5	1-27441210-11
return each number-one single ( s ) with the number of list in ascending order of the number of number-one single ( s ) .	SELECT t1.number-one_single_ , COUNT ( * ) from list as t1 GROUP BY t1.number-one_single_ ORDER BY COUNT ( * )	1-27441210-11
what is the country of the list with the minimum weeks at # 1 ?	SELECT t1.country from list as t1 ORDER BY t1.weeks_at_ ASC LIMIT 1	1-27441210-11
find the country of the list with the highest year .	SELECT t1.country from list as t1 ORDER BY t1.year DESC LIMIT 1	1-27441210-11
count the number of list in straight to # 1 ? 10 or 43 .	SELECT COUNT ( * ) from list as t1 WHERE t1.straight_to_ = 10 OR t1.straight_to_ = 43	1-27441210-11
return the number-one single ( s ) of the list that has the fewest corresponding number-one single ( s ) .	SELECT t1.number-one_single_ from list as t1 GROUP BY t1.number-one_single_ ORDER BY COUNT ( * ) ASC LIMIT 1	1-27441210-11
how many list are there that have more than {VALUE},0 artist ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.artist HAVING COUNT ( * ) > 10 	1-27441210-11
find the artist of the list with the highest year.	SELECT t1.artist from list as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from list as t1 )	1-27441210-11
