what is the studio ( s ) of the list who has the highest number of list ?	SELECT t1.studio_ from list as t1 GROUP BY t1.studio_ ORDER BY COUNT ( * ) DESC LIMIT 1	2-11872185-10
find the studio ( s ) of list whose rank is higher than the average rank .	SELECT t1.studio_ from list as t1 WHERE t1.rank > ( SELECT AVG ( t1.rank ) from list as t1	2-11872185-10
find the studio ( s ) who has exactly 10 list .	SELECT t1.studio_ from list as t1 GROUP BY t1.studio_ HAVING COUNT ( * ) = 10	2-11872185-10
list all movie which have year higher than the average .	SELECT t1.movie from list as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from list as t1	2-11872185-10
select the average second week nett . gross of each list 's movie .	SELECT AVG ( t1.second_week_nett_._gross ) , t1.movie from list as t1 GROUP BY t1.movie	2-11872185-10
what is the movie and studio ( s ) of the list with maximum rank ?	SELECT t1.movie , t1.studio_ from list as t1 WHERE t1.rank = ( SELECT MAX ( t1.rank ) {FROM, 3} )	2-11872185-10
what are the movie and movie of all list sorted by decreasing rank ?	SELECT t1.movie , t1.movie from list as t1 ORDER BY t1.rank DESC	2-11872185-10
find the movie of list whose second week nett . gross is higher than the average second week nett . gross .	SELECT t1.movie from list as t1 WHERE t1.second_week_nett_._gross > ( SELECT AVG ( t1.second_week_nett_._gross ) from list as t1	2-11872185-10
show all information on the list that has the largest number of movie.	SELECT * from list as t1 ORDER BY t1.movie DESC LIMIT 1	2-11872185-10
find the studio ( s ) who has exactly 10 list .	SELECT t1.studio_ from list as t1 GROUP BY t1.studio_ HAVING COUNT ( * ) = 10	2-11872185-10
