list the women 's doubles which average year is above 10 .	SELECT t1.women_'s_doubles from polish as t1 GROUP BY t1.women_'s_doubles HAVING AVG ( t1.year ) >= 10	2-12152903-1
how many polish ' mixed doubles have the word 10 in them ?	SELECT COUNT ( * ) from polish as t1 WHERE t1.mixed_doubles LIKE 10	2-12152903-1
what are the women 's singles of all polish that have 10 or more polish ?	SELECT t1.women_'s_singles from polish as t1 GROUP BY t1.women_'s_singles HAVING COUNT ( * ) >= 10	2-12152903-1
show all women 's singles and the total year for each .	SELECT t1.women_'s_singles , SUM ( t1.year ) from polish as t1 GROUP BY t1.women_'s_singles	2-12152903-1
what are the men 's doubles with exactly 10 polish ?	SELECT t1.men_'s_doubles from polish as t1 GROUP BY t1.men_'s_doubles HAVING COUNT ( * ) = 10	2-12152903-1
find the men 's singles for all polish who have more than the average year .	SELECT t1.men_'s_singles from polish as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from polish as t1	2-12152903-1
what is the minimum year in each men 's singles ?	SELECT MIN ( t1.year ) , t1.men_'s_singles from polish as t1 GROUP BY t1.men_'s_singles	2-12152903-1
which men 's singles has both polish with less than 10 year and polish with more than 85 year ?	SELECT t1.men_'s_singles from polish as t1 WHERE t1.year < 10 INTERSECT SELECT t1.men_'s_singles from polish as t1 WHERE t1.year > 85	2-12152903-1
what is the women 's singles of the polish with the largest year ?	SELECT t1.women_'s_singles from polish as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from polish as t1 )	2-12152903-1
which women 's doubles has both polish with less than 10 year and polish with more than 4 year ?	SELECT t1.women_'s_doubles from polish as t1 WHERE t1.year < 10 INTERSECT SELECT t1.women_'s_doubles from polish as t1 WHERE t1.year > 4	2-12152903-1
