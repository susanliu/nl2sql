find the time/retired and driver of the 1967 whose laps is lower than the average laps of all 1967 .	SELECT t1.time/retired , t1.driver from 1967 as t1 WHERE t1.laps < ( SELECT AVG ( t1.laps ) {FROM, 3} )	2-1122365-1
find the driver of 1967 which have both 10 and 68 as driver .	SELECT t1.driver from 1967 as t1 WHERE t1.laps = 10 INTERSECT SELECT t1.driver from 1967 as t1 WHERE t1.laps = 68	2-1122365-1
what are the distinct time/retired with grid between 10 and 3 ?	SELECT DISTINCT t1.time/retired from 1967 as t1 WHERE t1.grid BETWEEN 10 AND 3	2-1122365-1
what is the driver and constructor for the 1967 with the rank 5 smallest laps ?	SELECT t1.driver , t1.constructor from 1967 as t1 ORDER BY t1.laps LIMIT 5	2-1122365-1
what is all the information on the 1967 with the largest number of time/retired ?	SELECT * from 1967 as t1 ORDER BY t1.time/retired DESC LIMIT 1	2-1122365-1
which t1.constructor has the smallest amount of 1967?	SELECT t1.constructor from 1967 as t1 GROUP BY t1.constructor ORDER BY COUNT ( * ) LIMIT 1	2-1122365-1
find the driver who has exactly 10 1967 .	SELECT t1.driver from 1967 as t1 GROUP BY t1.driver HAVING COUNT ( * ) = 10	2-1122365-1
list all information about 1967 .	SELECT * FROM 1967	2-1122365-1
what is the constructor of the 1967 with least number of constructor ?	SELECT t1.constructor from 1967 as t1 GROUP BY t1.constructor ORDER BY COUNT ( * ) ASC LIMIT 1	2-1122365-1
find the time/retired of 1967 who have both 10 and 59 laps .	SELECT t1.time/retired from 1967 as t1 WHERE t1.laps = 10 INTERSECT SELECT t1.time/retired from 1967 as t1 WHERE t1.laps = 59	2-1122365-1
