find the distinct edition of all polona that have a higher year than some polona with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.edition from polona as t1 WHERE t1.year > ( SELECT MIN ( t1.edition ) from polona as t1 WHERE t1.result = 10 )	2-17717526-11
what is the date of the polona with the minimum year ?	SELECT t1.date from polona as t1 ORDER BY t1.year ASC LIMIT 1	2-17717526-11
count the number of polona in result 10 or 44 .	SELECT COUNT ( * ) from polona as t1 WHERE t1.result = 10 OR t1.result = 44	2-17717526-11
return the smallest year for every round .	SELECT MIN ( t1.year ) , t1.round from polona as t1 GROUP BY t1.round	2-17717526-11
how many result did each polona do, ordered by number of result ?	SELECT t1.result , COUNT ( * ) from polona as t1 GROUP BY t1.result ORDER BY COUNT ( * )	2-17717526-11
what are the result and opponent ( s ) ?	SELECT t1.result , t1.opponent_ from polona as t1	2-17717526-11
find the score of the polona that have just 10 polona .	SELECT t1.score from polona as t1 GROUP BY t1.score HAVING COUNT ( * ) = 10	2-17717526-11
find the distinct COLUMN_NAME,0} of all polona that have year higher than some polona from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.result from polona as t1 WHERE t1.year > ( SELECT MIN ( t1.result ) from polona as t1 WHERE t1.round = 10 )	2-17717526-11
what are the against and score of polona with 10 or more date ?	SELECT t1.against , t1.score from polona as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-17717526-11
list all information about polona .	SELECT * FROM polona	2-17717526-11
