show the player and the number of unique college/junior/club team containing each player .	SELECT t1.player , COUNT ( DISTINCT t1.college/junior/club_team ) from 1983 as t1 GROUP BY t1.player	1-2679061-1
return the nationality of 1983 for which the position is not 10 ?	SELECT t1.nationality from 1983 as t1 WHERE t1.position ! = 10	1-2679061-1
find the college/junior/club team of 1983 who have pick # of both 10 and 75 .	SELECT t1.college/junior/club_team from 1983 as t1 WHERE t1.pick_ = 10 INTERSECT SELECT t1.college/junior/club_team from 1983 as t1 WHERE t1.pick_ = 75	1-2679061-1
which player have an average pick # over 10 ?	SELECT t1.player from 1983 as t1 GROUP BY t1.player HAVING AVG ( t1.pick_ ) >= 10	1-2679061-1
give the college/junior/club team that has the most 1983 .	SELECT t1.college/junior/club_team from 1983 as t1 GROUP BY t1.college/junior/club_team ORDER BY COUNT ( * ) DESC LIMIT 1	1-2679061-1
sort the list of player and player of all 1983 in the descending order of pick # .	SELECT t1.player , t1.player from 1983 as t1 ORDER BY t1.pick_ DESC	1-2679061-1
find the nationality of 1983 who have both 10 and 47 pick # .	SELECT t1.nationality from 1983 as t1 WHERE t1.pick_ = 10 INTERSECT SELECT t1.nationality from 1983 as t1 WHERE t1.pick_ = 47	1-2679061-1
find the distinct position of 1983 having pick # between 10 and 62 .	SELECT DISTINCT t1.position from 1983 as t1 WHERE t1.pick_ BETWEEN 10 AND 62	1-2679061-1
show the nationality , nationality , and nationality of all the 1983 .	SELECT t1.nationality , t1.nationality , t1.nationality from 1983 as t1	1-2679061-1
how many distinct college/junior/club team correspond to each nhl team ?	SELECT t1.nhl_team , COUNT ( DISTINCT t1.college/junior/club_team ) from 1983 as t1 GROUP BY t1.nhl_team	1-2679061-1
