what is all the information on the 1982 with the largest number of home team score ?	SELECT * from 1982 as t1 ORDER BY t1.home_team_score DESC LIMIT 1	2-10824095-10
what are the {away team score of all the 1982 , and the total crowd by each ?	SELECT t1.away_team_score , SUM ( t1.crowd ) from 1982 as t1 GROUP BY t1.away_team_score	2-10824095-10
how many distinct home team score correspond to each venue ?	SELECT t1.venue , COUNT ( DISTINCT t1.home_team_score ) from 1982 as t1 GROUP BY t1.venue	2-10824095-10
show the venue with fewer than 3 1982 .	SELECT t1.venue from 1982 as t1 GROUP BY t1.venue HAVING COUNT ( * ) < 3	2-10824095-10
show home team and the number of distinct venue in each home team .	SELECT t1.home_team , COUNT ( DISTINCT t1.venue ) from 1982 as t1 GROUP BY t1.home_team	2-10824095-10
how many 1982 have date that contains 10 ?	SELECT COUNT ( * ) from 1982 as t1 WHERE t1.date LIKE 10	2-10824095-10
find the distinct home team score of 1982 having crowd between 10 and 98 .	SELECT DISTINCT t1.home_team_score from 1982 as t1 WHERE t1.crowd BETWEEN 10 AND 98	2-10824095-10
show all information on the 1982 that has the largest number of away team score.	SELECT * from 1982 as t1 ORDER BY t1.away_team_score DESC LIMIT 1	2-10824095-10
which home team has both 1982 with less than 10 crowd and 1982 with more than 25 crowd ?	SELECT t1.home_team from 1982 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1982 as t1 WHERE t1.crowd > 25	2-10824095-10
return the smallest crowd for every away team .	SELECT MIN ( t1.crowd ) , t1.away_team from 1982 as t1 GROUP BY t1.away_team	2-10824095-10
