what is the player and college of the 2004 with maximum pick # ?	SELECT t1.player , t1.college from 2004 as t1 WHERE t1.pick_ = ( SELECT MAX ( t1.pick_ ) {FROM, 3} )	1-10975034-5
what are the college of 2004 with pick # greater than the average of all 2004 ?	SELECT t1.college from 2004 as t1 WHERE t1.pick_ > ( SELECT AVG ( t1.pick_ ) from 2004 as t1	1-10975034-5
find the player of 2004 which are in 10 cfl team but not in 57 cfl team .	SELECT t1.player from 2004 as t1 WHERE t1.cfl_team = 10 EXCEPT SELECT t1.player from 2004 as t1 WHERE t1.cfl_team = 57	1-10975034-5
give the maximum and minimum pick # of all 2004 .	SELECT MAX ( t1.pick_ ) , MIN ( t1.pick_ ) from 2004 as t1	1-10975034-5
what are the position of 2004 whose position is not 10 ?	SELECT t1.position from 2004 as t1 WHERE t1.position ! = 10	1-10975034-5
count the number of 2004 in cfl team 10 or 74 .	SELECT COUNT ( * ) from 2004 as t1 WHERE t1.cfl_team = 10 OR t1.cfl_team = 74	1-10975034-5
find all position that have fewer than three in 2004 .	SELECT t1.position from 2004 as t1 GROUP BY t1.position HAVING COUNT ( * ) < 3	1-10975034-5
what is the maximum and mininum pick # {COLUMN} for all 2004 ?	SELECT MAX ( t1.pick_ ) , MIN ( t1.pick_ ) from 2004 as t1	1-10975034-5
show all information on the 2004 that has the largest number of position.	SELECT * from 2004 as t1 ORDER BY t1.position DESC LIMIT 1	1-10975034-5
give the cfl team that has the most 2004 .	SELECT t1.cfl_team from 2004 as t1 GROUP BY t1.cfl_team ORDER BY COUNT ( * ) DESC LIMIT 1	1-10975034-5
