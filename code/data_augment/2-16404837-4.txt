find the school of the blue that is most frequent across all school .	SELECT t1.school from blue as t1 GROUP BY t1.school ORDER BY COUNT ( * ) DESC LIMIT 1	2-16404837-4
find the affiliation of blue which are location 10 but not location 50 .	SELECT t1.affiliation from blue as t1 WHERE t1.location = 10 EXCEPT SELECT t1.affiliation from blue as t1 WHERE t1.location = 50	2-16404837-4
show the nickname and the total founded of blue .	SELECT t1.nickname , SUM ( t1.founded ) from blue as t1 GROUP BY t1.nickname	2-16404837-4
how many blue are there in affiliation 10 or 85 ?	SELECT COUNT ( * ) from blue as t1 WHERE t1.affiliation = 10 OR t1.affiliation = 85	2-16404837-4
list the location of {blue which has number of blue greater than 10 .	SELECT t1.location from blue as t1 GROUP BY t1.location HAVING COUNT ( * ) > 10	2-16404837-4
list the nickname of {blue which has number of blue greater than 10 .	SELECT t1.nickname from blue as t1 GROUP BY t1.nickname HAVING COUNT ( * ) > 10	2-16404837-4
what are the affiliation of blue whose school are not 10 ?	SELECT t1.affiliation from blue as t1 WHERE t1.school ! = 10	2-16404837-4
return the location of the blue that has the fewest corresponding location .	SELECT t1.location from blue as t1 GROUP BY t1.location ORDER BY COUNT ( * ) ASC LIMIT 1	2-16404837-4
what is the school and affiliation of the blue with maximum founded ?	SELECT t1.school , t1.affiliation from blue as t1 WHERE t1.founded = ( SELECT MAX ( t1.founded ) {FROM, 3} )	2-16404837-4
show the school with fewer than 3 blue .	SELECT t1.school from blue as t1 GROUP BY t1.school HAVING COUNT ( * ) < 3	2-16404837-4
