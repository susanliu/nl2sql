what is the borough of demographics with the maximum 2000 across all demographics ?	SELECT t1.borough from demographics as t1 WHERE t1.2000 = ( SELECT MAX ( t1.2000 ) from demographics as t1 )	2-1729017-2
what is the borough of the demographics with the minimum 2006 ?	SELECT t1.borough from demographics as t1 ORDER BY t1.2006 ASC LIMIT 1	2-1729017-2
what is the maximum and mininum 2006 {COLUMN} for all demographics ?	SELECT MAX ( t1.2006 ) , MIN ( t1.2006 ) from demographics as t1	2-1729017-2
find the number of demographics whose borough contain the word 10 .	SELECT COUNT ( * ) from demographics as t1 WHERE t1.borough LIKE 10	2-1729017-2
find the borough of demographics which have both 10 and 82 as borough .	SELECT t1.borough from demographics as t1 WHERE t1.2006 = 10 INTERSECT SELECT t1.borough from demographics as t1 WHERE t1.2006 = 82	2-1729017-2
give the maximum and minimum 1980 of all demographics .	SELECT MAX ( t1.1980 ) , MIN ( t1.1980 ) from demographics as t1	2-1729017-2
how many demographics does each borough have ?	SELECT t1.borough , COUNT ( * ) from demographics as t1 GROUP BY t1.borough ORDER BY COUNT ( * )	2-1729017-2
return the smallest 2000 for every borough .	SELECT MIN ( t1.2000 ) , t1.borough from demographics as t1 GROUP BY t1.borough	2-1729017-2
show the borough of the demographics that has the most demographics .	SELECT t1.borough from demographics as t1 GROUP BY t1.borough ORDER BY COUNT ( * ) DESC LIMIT 1	2-1729017-2
what are the borough and borough of each demographics ?	SELECT t1.borough , t1.borough from demographics as t1	2-1729017-2
