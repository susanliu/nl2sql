how many ivan ' engine have the word 10 in them ?	SELECT COUNT ( * ) from ivan as t1 WHERE t1.engine LIKE 10	2-1181411-2
what are the distinct COLUMN_NAME,0} of every ivan that has a greater year than some ivan with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.entrant from ivan as t1 WHERE t1.year > ( SELECT MIN ( t1.entrant ) from ivan as t1 WHERE t1.entrant = 10 )	2-1181411-2
which engine has both ivan with less than 10 pts . and ivan with more than 6 pts . ?	SELECT t1.engine from ivan as t1 WHERE t1.pts_ < 10 INTERSECT SELECT t1.engine from ivan as t1 WHERE t1.pts_ > 6	2-1181411-2
which entrant have an average pts . over 10 ?	SELECT t1.entrant from ivan as t1 GROUP BY t1.entrant HAVING AVG ( t1.pts_ ) >= 10	2-1181411-2
which chassis has the least pts . ?	SELECT t1.chassis from ivan as t1 ORDER BY t1.pts_ ASC LIMIT 1	2-1181411-2
please list the entrant and engine of ivan in descending order of pts . .	SELECT t1.entrant , t1.engine from ivan as t1 ORDER BY t1.pts_ DESC	2-1181411-2
find the chassis of the ivan with the highest pts ..	SELECT t1.chassis from ivan as t1 WHERE t1.pts_ = ( SELECT MAX ( t1.pts_ ) from ivan as t1 )	2-1181411-2
how many ivan correspond to each entrant? show the result in ascending order.	SELECT t1.entrant , COUNT ( * ) from ivan as t1 GROUP BY t1.entrant ORDER BY COUNT ( * )	2-1181411-2
give the engine that has the most ivan .	SELECT t1.engine from ivan as t1 GROUP BY t1.engine ORDER BY COUNT ( * ) DESC LIMIT 1	2-1181411-2
what are the engine of all ivan with chassis that is 10 ?	SELECT t1.engine from ivan as t1 GROUP BY t1.chassis HAVING COUNT ( * ) = 10	2-1181411-2
