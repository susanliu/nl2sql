list type of list that have the number of list greater than 10 .	SELECT t1.type from list as t1 GROUP BY t1.type HAVING COUNT ( * ) > 10	1-256286-40
give the type that has the most list .	SELECT t1.type from list as t1 GROUP BY t1.type ORDER BY COUNT ( * ) DESC LIMIT 1	1-256286-40
which passed have greater no votes than that of any no votes in list ?	SELECT t1.passed from list as t1 WHERE t1.no_votes > ( SELECT MIN ( t1.no_votes ) from list as t1 )	1-256286-40
which const . amd . ? have greater yes votes than that of any yes votes in list ?	SELECT t1.const_._amd_._ from list as t1 WHERE t1.yes_votes > ( SELECT MIN ( t1.yes_votes ) from list as t1 )	1-256286-40
which description have greater meas . num . than that of any meas . num . in list ?	SELECT t1.description from list as t1 WHERE t1.meas_._num_ > ( SELECT MIN ( t1.meas_._num_ ) from list as t1 )	1-256286-40
find the % yes and description of the list whose no votes is lower than the average no votes of all list .	SELECT t1.% , t1.description from list as t1 WHERE t1.no_votes < ( SELECT AVG ( t1.no_votes ) {FROM, 3} )	1-256286-40
return the smallest meas . num . for every % yes .	SELECT MIN ( t1.meas_._num_ ) , t1.% from list as t1 GROUP BY t1.%	1-256286-40
how many list have % yes that contain the word 10 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.% LIKE 10	1-256286-40
what are the % yes , passed , and passed of each list ?	SELECT t1.% , t1.passed , t1.passed from list as t1	1-256286-40
what is all the information on the list with the largest number of % yes ?	SELECT * from list as t1 ORDER BY t1.% DESC LIMIT 1	1-256286-40
