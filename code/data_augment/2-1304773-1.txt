show the county shared by more than 10 united .	SELECT t1.county from united as t1 GROUP BY t1.county HAVING COUNT ( * ) > 10	2-1304773-1
show the bush % and kerry % with at least 10 others % .	SELECT t1.bush_ , t1.kerry_ from united as t1 GROUP BY t1.others_ HAVING COUNT ( * ) >= 10	2-1304773-1
give the t1.bush_ with the fewest united .	SELECT t1.bush_ from united as t1 GROUP BY t1.bush_ ORDER BY COUNT ( * ) LIMIT 1	2-1304773-1
what is the minimum others # in each bush % ?	SELECT MIN ( t1.others_ ) , t1.bush_ from united as t1 GROUP BY t1.bush_	2-1304773-1
return the smallest bush # for every kerry % .	SELECT MIN ( t1.bush_ ) , t1.kerry_ from united as t1 GROUP BY t1.kerry_	2-1304773-1
find the others % of the united which have county 10 but not 71 .	SELECT t1.others_ from united as t1 WHERE t1.county = 10 EXCEPT SELECT t1.others_ from united as t1 WHERE t1.county = 71	2-1304773-1
give the kerry % that has the most united .	SELECT t1.kerry_ from united as t1 GROUP BY t1.kerry_ ORDER BY COUNT ( * ) DESC LIMIT 1	2-1304773-1
find the distinct county of united having others # between 10 and 13 .	SELECT DISTINCT t1.county from united as t1 WHERE t1.others_ BETWEEN 10 AND 13	2-1304773-1
what is the kerry % of all united whose kerry # is higher than any united ?	SELECT t1.kerry_ from united as t1 WHERE t1.kerry_ > ( SELECT MIN ( t1.kerry_ ) from united as t1 )	2-1304773-1
what are the others % of united with kerry # above the average kerry # across all united ?	SELECT t1.others_ from united as t1 WHERE t1.kerry_ > ( SELECT AVG ( t1.kerry_ ) from united as t1	2-1304773-1
