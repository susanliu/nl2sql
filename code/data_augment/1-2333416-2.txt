how many ken are there that have more than {VALUE},0 avg . start ?	SELECT COUNT ( * ) from ken as t1 GROUP BY t1.avg_._start HAVING COUNT ( * ) > 10 	1-2333416-2
what are the distinct COLUMN_NAME,0} of ken with poles higher than any ken from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.avg_._finish from ken as t1 WHERE t1.poles > ( SELECT MIN ( t1.avg_._finish ) from ken as t1 WHERE t1.winnings = 10 )	1-2333416-2
what are the average wins of ken for different team ( s ) ?	SELECT AVG ( t1.wins ) , t1.team_ from ken as t1 GROUP BY t1.team_	1-2333416-2
return the smallest poles for every team ( s ) .	SELECT MIN ( t1.poles ) , t1.team_ from ken as t1 GROUP BY t1.team_	1-2333416-2
which avg . finish has both ken with less than 10 top 10 and ken with more than 24 top 10 ?	SELECT t1.avg_._finish from ken as t1 WHERE t1.top_10 < 10 INTERSECT SELECT t1.avg_._finish from ken as t1 WHERE t1.top_10 > 24	1-2333416-2
what are the average year of ken , grouped by avg . finish ?	SELECT AVG ( t1.year ) , t1.avg_._finish from ken as t1 GROUP BY t1.avg_._finish	1-2333416-2
find the avg . start of the ken with the highest poles.	SELECT t1.avg_._start from ken as t1 WHERE t1.poles = ( SELECT MAX ( t1.poles ) from ken as t1 )	1-2333416-2
what is the position of the most common ken in all position ?	SELECT t1.position from ken as t1 GROUP BY t1.position ORDER BY COUNT ( * ) DESC LIMIT 1	1-2333416-2
display the avg . finish , avg . start , and winnings for each ken .	SELECT t1.avg_._finish , t1.avg_._start , t1.winnings from ken as t1	1-2333416-2
return the winnings of the ken with the fewest starts .	SELECT t1.winnings from ken as t1 ORDER BY t1.starts ASC LIMIT 1	1-2333416-2
