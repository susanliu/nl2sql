what is the ship 's name and ship 's name of the stena with maximum tonnage ?	SELECT t1.ship_'s_name , t1.ship_'s_name from stena as t1 WHERE t1.tonnage = ( SELECT MAX ( t1.tonnage ) {FROM, 3} )	2-11662133-3
return the date entered service of the largest tonnage.	SELECT t1.date_entered_service from stena as t1 ORDER BY t1.tonnage DESC LIMIT 1	2-11662133-3
give the date entered service that has the most stena .	SELECT t1.date_entered_service from stena as t1 GROUP BY t1.date_entered_service ORDER BY COUNT ( * ) DESC LIMIT 1	2-11662133-3
what is the ship 's name of the stena with least number of ship 's name ?	SELECT t1.ship_'s_name from stena as t1 GROUP BY t1.ship_'s_name ORDER BY COUNT ( * ) ASC LIMIT 1	2-11662133-3
what are the average tonnage of stena , grouped by type of ship ?	SELECT AVG ( t1.tonnage ) , t1.type_of_ship from stena as t1 GROUP BY t1.type_of_ship	2-11662133-3
what are all the date entered service and ship 's name?	SELECT t1.date_entered_service , t1.ship_'s_name from stena as t1	2-11662133-3
find the date withdrawn of stena who have tonnage of both 10 and 100 .	SELECT t1.date_withdrawn from stena as t1 WHERE t1.tonnage = 10 INTERSECT SELECT t1.date_withdrawn from stena as t1 WHERE t1.tonnage = 100	2-11662133-3
which date withdrawn have less than 3 in stena ?	SELECT t1.date_withdrawn from stena as t1 GROUP BY t1.date_withdrawn HAVING COUNT ( * ) < 3	2-11662133-3
which date entered service have an average tonnage over 10 ?	SELECT t1.date_entered_service from stena as t1 GROUP BY t1.date_entered_service HAVING AVG ( t1.tonnage ) >= 10	2-11662133-3
find the ship 's name of stena who have both 10 and 86 tonnage .	SELECT t1.ship_'s_name from stena as t1 WHERE t1.tonnage = 10 INTERSECT SELECT t1.ship_'s_name from stena as t1 WHERE t1.tonnage = 86	2-11662133-3
