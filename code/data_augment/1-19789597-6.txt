show the location shared by more than 10 1999 .	SELECT t1.location from 1999 as t1 GROUP BY t1.location HAVING COUNT ( * ) > 10	1-19789597-6
what are the distinct score with game between 10 and 57 ?	SELECT DISTINCT t1.score from 1999 as t1 WHERE t1.game BETWEEN 10 AND 57	1-19789597-6
find the high assists of 1999 who have both 10 and 6 game .	SELECT t1.high_assists from 1999 as t1 WHERE t1.game = 10 INTERSECT SELECT t1.high_assists from 1999 as t1 WHERE t1.game = 6	1-19789597-6
find the opponent of the 1999 with the largest game .	SELECT t1.opponent from 1999 as t1 ORDER BY t1.game DESC LIMIT 1	1-19789597-6
what is the count of 1999 with more than 10 location ?	SELECT COUNT ( * ) from 1999 as t1 GROUP BY t1.location HAVING COUNT ( * ) > 10 	1-19789597-6
list the date and high rebounds of all 1999 sorted by game in descending order .	SELECT t1.date , t1.high_rebounds from 1999 as t1 ORDER BY t1.game DESC	1-19789597-6
find the opponent and high points of the 1999 whose game is lower than the average game of all 1999 .	SELECT t1.opponent , t1.high_points from 1999 as t1 WHERE t1.game < ( SELECT AVG ( t1.game ) {FROM, 3} )	1-19789597-6
what are the record of the 1999 that have exactly 10 1999 ?	SELECT t1.record from 1999 as t1 GROUP BY t1.record HAVING COUNT ( * ) = 10	1-19789597-6
list the score which average game is above 10 .	SELECT t1.score from 1999 as t1 GROUP BY t1.score HAVING AVG ( t1.game ) >= 10	1-19789597-6
list record and date who have game greater than 5 or game shorter than 10 .	SELECT t1.record , t1.date from 1999 as t1 WHERE t1.game > 5 OR t1.game < 10	1-19789597-6
