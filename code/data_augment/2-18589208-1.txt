find the result of the 1958 with the largest attendance .	SELECT t1.result from 1958 as t1 ORDER BY t1.attendance DESC LIMIT 1	2-18589208-1
what is the result and date of every 1958 that has a attendance lower than average ?	SELECT t1.result , t1.date from 1958 as t1 WHERE t1.attendance < ( SELECT AVG ( t1.attendance ) {FROM, 3} )	2-18589208-1
find the result of 1958 who have attendance of both 10 and 30 .	SELECT t1.result from 1958 as t1 WHERE t1.attendance = 10 INTERSECT SELECT t1.result from 1958 as t1 WHERE t1.attendance = 30	2-18589208-1
what are the date of all 1958 with result that is 10 ?	SELECT t1.date from 1958 as t1 GROUP BY t1.result HAVING COUNT ( * ) = 10	2-18589208-1
what is the opponent of the most common 1958 in all opponent ?	SELECT t1.opponent from 1958 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-18589208-1
find the date of the 1958 with the highest attendance .	SELECT t1.date from 1958 as t1 ORDER BY t1.attendance DESC LIMIT 1	2-18589208-1
please show the opponent of the 1958 with count more than 10 .	SELECT t1.opponent from 1958 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10	2-18589208-1
find the distinct opponent of 1958 having attendance between 10 and 33 .	SELECT DISTINCT t1.opponent from 1958 as t1 WHERE t1.attendance BETWEEN 10 AND 33	2-18589208-1
what is the maximum and mininum attendance {COLUMN} for all 1958 ?	SELECT MAX ( t1.attendance ) , MIN ( t1.attendance ) from 1958 as t1	2-18589208-1
what are the result with exactly 10 1958 ?	SELECT t1.result from 1958 as t1 GROUP BY t1.result HAVING COUNT ( * ) = 10	2-18589208-1
