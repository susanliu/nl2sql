which date have greater week than that of any week in 1979 ?	SELECT t1.date from 1979 as t1 WHERE t1.week > ( SELECT MIN ( t1.week ) from 1979 as t1 )	2-15085755-2
Return all columns in 1979 .	SELECT * FROM 1979	2-15085755-2
show the opponent , opponent , and result of all the 1979 .	SELECT t1.opponent , t1.opponent , t1.result from 1979 as t1	2-15085755-2
what are the date of all 1979 that have 10 or more 1979 ?	SELECT t1.date from 1979 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-15085755-2
what are the average attendance of 1979 , grouped by result ?	SELECT AVG ( t1.attendance ) , t1.result from 1979 as t1 GROUP BY t1.result	2-15085755-2
what are the opponent of all 1979 with date that is 10 ?	SELECT t1.opponent from 1979 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-15085755-2
what are the result of all 1979 that have 10 or more 1979 ?	SELECT t1.result from 1979 as t1 GROUP BY t1.result HAVING COUNT ( * ) >= 10	2-15085755-2
show all date and corresponding number of 1979 in the ascending order of the numbers.	SELECT t1.date , COUNT ( * ) from 1979 as t1 GROUP BY t1.date ORDER BY COUNT ( * )	2-15085755-2
which opponent have an average week over 10 ?	SELECT t1.opponent from 1979 as t1 GROUP BY t1.opponent HAVING AVG ( t1.week ) >= 10	2-15085755-2
which opponent has both 1979 with less than 10 attendance and 1979 with more than 66 attendance ?	SELECT t1.opponent from 1979 as t1 WHERE t1.attendance < 10 INTERSECT SELECT t1.opponent from 1979 as t1 WHERE t1.attendance > 66	2-15085755-2
