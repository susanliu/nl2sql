show the poverty rate with fewer than 3 list .	SELECT t1.poverty_rate from list as t1 GROUP BY t1.poverty_rate HAVING COUNT ( * ) < 3	1-22815568-12
list county and unemployment rate who have population greater than 5 or population shorter than 10 .	SELECT t1.county , t1.unemployment_rate from list as t1 WHERE t1.population > 5 OR t1.population < 10	1-22815568-12
what are the unemployment rate of list with population above the average population across all list ?	SELECT t1.unemployment_rate from list as t1 WHERE t1.population > ( SELECT AVG ( t1.population ) from list as t1	1-22815568-12
what are the unemployment rate of list , sorted by their frequency?	SELECT t1.unemployment_rate from list as t1 GROUP BY t1.unemployment_rate ORDER BY COUNT ( * ) ASC LIMIT 1	1-22815568-12
what is the county and market income per capita of the list with maximum population ?	SELECT t1.county , t1.market_income_per_capita from list as t1 WHERE t1.population = ( SELECT MAX ( t1.population ) {FROM, 3} )	1-22815568-12
show all information on the list that has the largest number of status.	SELECT * from list as t1 ORDER BY t1.status DESC LIMIT 1	1-22815568-12
show all information on the list that has the largest number of unemployment rate.	SELECT * from list as t1 ORDER BY t1.unemployment_rate DESC LIMIT 1	1-22815568-12
which t1.market_income_per_capita has the smallest amount of list?	SELECT t1.market_income_per_capita from list as t1 GROUP BY t1.market_income_per_capita ORDER BY COUNT ( * ) LIMIT 1	1-22815568-12
what are the average population of list for different poverty rate ?	SELECT AVG ( t1.population ) , t1.poverty_rate from list as t1 GROUP BY t1.poverty_rate	1-22815568-12
select the average population of each list 's unemployment rate .	SELECT AVG ( t1.population ) , t1.unemployment_rate from list as t1 GROUP BY t1.unemployment_rate	1-22815568-12
