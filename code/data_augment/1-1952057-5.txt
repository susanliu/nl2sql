what are the team of afc , sorted by their frequency?	SELECT t1.team from afc as t1 GROUP BY t1.team ORDER BY COUNT ( * ) ASC LIMIT 1	1-1952057-5
return the different team of afc , in ascending order of frequency .	SELECT t1.team from afc as t1 GROUP BY t1.team ORDER BY COUNT ( * ) ASC LIMIT 1	1-1952057-5
what is all the information on the afc with the largest number of team ?	SELECT * from afc as t1 ORDER BY t1.team DESC LIMIT 1	1-1952057-5
what is the minimum playoff berths in each team ?	SELECT MIN ( t1.playoff_berths ) , t1.team from afc as t1 GROUP BY t1.team	1-1952057-5
show team for all afc whose super bowl championships are greater than the average .	SELECT t1.team from afc as t1 WHERE t1.super_bowl_championships > ( SELECT AVG ( t1.super_bowl_championships ) from afc as t1	1-1952057-5
which team has the least afl titles ?	SELECT t1.team from afc as t1 ORDER BY t1.afl_titles ASC LIMIT 1	1-1952057-5
find the team of afc who have more than 10 afc .	SELECT t1.team from afc as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10	1-1952057-5
how many different team correspond to each team ?	SELECT t1.team , COUNT ( DISTINCT t1.team ) from afc as t1 GROUP BY t1.team	1-1952057-5
find the team of afc who have both 10 and 22 playoff berths .	SELECT t1.team from afc as t1 WHERE t1.playoff_berths = 10 INTERSECT SELECT t1.team from afc as t1 WHERE t1.playoff_berths = 22	1-1952057-5
what is the team of afc with the maximum playoff berths across all afc ?	SELECT t1.team from afc as t1 WHERE t1.playoff_berths = ( SELECT MAX ( t1.playoff_berths ) from afc as t1 )	1-1952057-5
