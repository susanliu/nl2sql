please show the different away team score , ordered by the number of 1945 that have each .	SELECT t1.away_team_score from 1945 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-10809271-18
what is the venue and venue of the 1945 with maximum crowd ?	SELECT t1.venue , t1.venue from 1945 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809271-18
which venue has the least crowd ?	SELECT t1.venue from 1945 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10809271-18
return the away team of the largest crowd.	SELECT t1.away_team from 1945 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10809271-18
what are the date of the 1945 with date other than 10 ?	SELECT t1.date from 1945 as t1 WHERE t1.home_team ! = 10	2-10809271-18
return the different venue of 1945 , in ascending order of frequency .	SELECT t1.venue from 1945 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) ASC LIMIT 1	2-10809271-18
what are the date of all 1945 with date that is 10 ?	SELECT t1.date from 1945 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10809271-18
how many away team score did each 1945 do, ordered by number of away team score ?	SELECT t1.away_team_score , COUNT ( * ) from 1945 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * )	2-10809271-18
what is the count of 1945 with more than 10 away team score ?	SELECT COUNT ( * ) from 1945 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10 	2-10809271-18
which away team have less than 3 in 1945 ?	SELECT t1.away_team from 1945 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) < 3	2-10809271-18
