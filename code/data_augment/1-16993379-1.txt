which rating has both howie with less than 10 share and howie with more than 30 share ?	SELECT t1.rating from howie as t1 WHERE t1.share < 10 INTERSECT SELECT t1.rating from howie as t1 WHERE t1.share > 30	1-16993379-1
return the viewers ( millions ) of the howie with the fewest episode number .	SELECT t1.viewers_ from howie as t1 ORDER BY t1.episode_number ASC LIMIT 1	1-16993379-1
what are the episode and viewers ( millions ) of howie with 10 or more rank ( overall ) ?	SELECT t1.episode , t1.viewers_ from howie as t1 GROUP BY t1.rank_ HAVING COUNT ( * ) >= 10	1-16993379-1
what are the average share of howie , grouped by episode ?	SELECT AVG ( t1.share ) , t1.episode from howie as t1 GROUP BY t1.episode	1-16993379-1
find the rating/share ( 18-49 ) of the howie with the largest share .	SELECT t1.rating/share_ from howie as t1 ORDER BY t1.share DESC LIMIT 1	1-16993379-1
find the rank ( overall ) and viewers ( millions ) of the howie whose share is lower than the average share of all howie .	SELECT t1.rank_ , t1.viewers_ from howie as t1 WHERE t1.share < ( SELECT AVG ( t1.share ) {FROM, 3} )	1-16993379-1
find the rating/share ( 18-49 ) of howie which have both 10 and 88 as rating/share ( 18-49 ) .	SELECT t1.rating/share_ from howie as t1 WHERE t1.share = 10 INTERSECT SELECT t1.rating/share_ from howie as t1 WHERE t1.share = 88	1-16993379-1
what are the viewers ( millions ) of all howie with rating that is 10 ?	SELECT t1.viewers_ from howie as t1 GROUP BY t1.rating HAVING COUNT ( * ) = 10	1-16993379-1
give the maximum and minimum episode number of all howie .	SELECT MAX ( t1.episode_number ) , MIN ( t1.episode_number ) from howie as t1	1-16993379-1
what is the rank ( overall ) and viewers ( millions ) for the howie with the rank 5 smallest episode number ?	SELECT t1.rank_ , t1.viewers_ from howie as t1 ORDER BY t1.episode_number LIMIT 5	1-16993379-1
