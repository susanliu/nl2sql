find the school of 1978 which have both 10 and 78 as school .	SELECT t1.school from 1978 as t1 WHERE t1.round = 10 INTERSECT SELECT t1.school from 1978 as t1 WHERE t1.round = 78	2-14656160-1
what are the maximum and minimum pick across all 1978 ?	SELECT MAX ( t1.pick ) , MIN ( t1.pick ) from 1978 as t1	2-14656160-1
show school and the number of distinct position in each school .	SELECT t1.school , COUNT ( DISTINCT t1.position ) from 1978 as t1 GROUP BY t1.school	2-14656160-1
return the position of the largest pick.	SELECT t1.position from 1978 as t1 ORDER BY t1.pick DESC LIMIT 1	2-14656160-1
find the position of the 1978 that have just 10 1978 .	SELECT t1.position from 1978 as t1 GROUP BY t1.position HAVING COUNT ( * ) = 10	2-14656160-1
what are the position that have greater round than any round in 1978 ?	SELECT t1.position from 1978 as t1 WHERE t1.round > ( SELECT MIN ( t1.round ) from 1978 as t1 )	2-14656160-1
what is the position of the 1978 with the smallest pick ?	SELECT t1.position from 1978 as t1 ORDER BY t1.pick ASC LIMIT 1	2-14656160-1
how many 1978 are there that have more than {VALUE},0 player ?	SELECT COUNT ( * ) from 1978 as t1 GROUP BY t1.player HAVING COUNT ( * ) > 10 	2-14656160-1
which player has both 1978 with less than 10 round and 1978 with more than 28 round ?	SELECT t1.player from 1978 as t1 WHERE t1.round < 10 INTERSECT SELECT t1.player from 1978 as t1 WHERE t1.round > 28	2-14656160-1
show the position shared by more than 10 1978 .	SELECT t1.position from 1978 as t1 GROUP BY t1.position HAVING COUNT ( * ) > 10	2-14656160-1
