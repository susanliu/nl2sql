find the away team of 1968 which are in 10 home team but not in 1 home team .	SELECT t1.away_team from 1968 as t1 WHERE t1.home_team = 10 EXCEPT SELECT t1.away_team from 1968 as t1 WHERE t1.home_team = 1	2-10808933-8
return the away team of 1968 that do not have the home team 10 .	SELECT t1.away_team from 1968 as t1 WHERE t1.home_team ! = 10	2-10808933-8
find the away team of 1968 who have crowd of both 10 and 39 .	SELECT t1.away_team from 1968 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.away_team from 1968 as t1 WHERE t1.crowd = 39	2-10808933-8
what are the away team and home team ?	SELECT t1.away_team , t1.home_team from 1968 as t1	2-10808933-8
what is the t1.date of 1968 that has fewest number of 1968 ?	SELECT t1.date from 1968 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) LIMIT 1	2-10808933-8
how many 1968 are there in date 10 or 83 ?	SELECT COUNT ( * ) from 1968 as t1 WHERE t1.date = 10 OR t1.date = 83	2-10808933-8
return the smallest crowd for every away team score .	SELECT MIN ( t1.crowd ) , t1.away_team_score from 1968 as t1 GROUP BY t1.away_team_score	2-10808933-8
find the away team score and home team score of the 1968 whose crowd is lower than the average crowd of all 1968 .	SELECT t1.away_team_score , t1.home_team_score from 1968 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10808933-8
find the away team score of 1968 who have both 10 and 31 crowd .	SELECT t1.away_team_score from 1968 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.away_team_score from 1968 as t1 WHERE t1.crowd = 31	2-10808933-8
what is the home team and venue of the 1968 with maximum crowd ?	SELECT t1.home_team , t1.venue from 1968 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10808933-8
