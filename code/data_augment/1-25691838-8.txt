list all information about none .	SELECT * FROM none	1-25691838-8
return the different guest of none , in ascending order of frequency .	SELECT t1.guest from none as t1 GROUP BY t1.guest ORDER BY COUNT ( * ) ASC LIMIT 1	1-25691838-8
which guest has both none with less than 10 production code and none with more than 24 production code ?	SELECT t1.guest from none as t1 WHERE t1.production_code < 10 INTERSECT SELECT t1.guest from none as t1 WHERE t1.production_code > 24	1-25691838-8
how many none have introductory phrase that contains 10 ?	SELECT COUNT ( * ) from none as t1 WHERE t1.introductory_phrase LIKE 10	1-25691838-8
what are the guest and introductory phrase of the {COLUMN} who have episode # above five or episode # below ten ?	SELECT t1.guest , t1.introductory_phrase from none as t1 WHERE t1.episode_ > 5 OR t1.episode_ < 10	1-25691838-8
how many none does each original airdate have ?	SELECT t1.original_airdate , COUNT ( * ) from none as t1 GROUP BY t1.original_airdate ORDER BY COUNT ( * )	1-25691838-8
how many none are there in original airdate 10 or 6 ?	SELECT COUNT ( * ) from none as t1 WHERE t1.original_airdate = 10 OR t1.original_airdate = 6	1-25691838-8
find the distinct guest of none having episode # between 10 and 5 .	SELECT DISTINCT t1.guest from none as t1 WHERE t1.episode_ BETWEEN 10 AND 5	1-25691838-8
return the original airdate of the none that has the fewest corresponding original airdate .	SELECT t1.original_airdate from none as t1 GROUP BY t1.original_airdate ORDER BY COUNT ( * ) ASC LIMIT 1	1-25691838-8
which original airdate is the most frequent original airdate?	SELECT t1.original_airdate from none as t1 GROUP BY t1.original_airdate ORDER BY COUNT ( * ) DESC LIMIT 1	1-25691838-8
