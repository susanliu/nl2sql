list the venue , venue and the home team of the 1934 .	SELECT t1.venue , t1.venue , t1.home_team from 1934 as t1	2-10790510-16
find the date of 1934 which are in 10 home team score but not in 41 home team score .	SELECT t1.date from 1934 as t1 WHERE t1.home_team_score = 10 EXCEPT SELECT t1.date from 1934 as t1 WHERE t1.home_team_score = 41	2-10790510-16
show the venue and the total crowd of 1934 .	SELECT t1.venue , SUM ( t1.crowd ) from 1934 as t1 GROUP BY t1.venue	2-10790510-16
which date have an average crowd over 10 ?	SELECT t1.date from 1934 as t1 GROUP BY t1.date HAVING AVG ( t1.crowd ) >= 10	2-10790510-16
what are the date of all 1934 that have 10 or more 1934 ?	SELECT t1.date from 1934 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-10790510-16
how many different venue correspond to each away team score ?	SELECT t1.away_team_score , COUNT ( DISTINCT t1.venue ) from 1934 as t1 GROUP BY t1.away_team_score	2-10790510-16
how many 1934 does each date have ?	SELECT t1.date , COUNT ( * ) from 1934 as t1 GROUP BY t1.date ORDER BY COUNT ( * )	2-10790510-16
show the home team and the number of unique venue containing each home team .	SELECT t1.home_team , COUNT ( DISTINCT t1.venue ) from 1934 as t1 GROUP BY t1.home_team	2-10790510-16
show date and the number of distinct venue in each date .	SELECT t1.date , COUNT ( DISTINCT t1.venue ) from 1934 as t1 GROUP BY t1.date	2-10790510-16
what are the home team score more than 10 1934 have ?	SELECT t1.home_team_score from 1934 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) > 10	2-10790510-16
