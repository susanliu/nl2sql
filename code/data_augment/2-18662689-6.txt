which athlete has most number of rowing ?	SELECT t1.athlete from rowing as t1 GROUP BY t1.athlete ORDER BY COUNT ( * ) DESC LIMIT 1	2-18662689-6
what are the time and athlete of the {COLUMN} who have rank above five or rank below ten ?	SELECT t1.time , t1.athlete from rowing as t1 WHERE t1.rank > 5 OR t1.rank < 10	2-18662689-6
list all information about rowing .	SELECT * FROM rowing	2-18662689-6
what are the time of all rowing with notes that is 10 ?	SELECT t1.time from rowing as t1 GROUP BY t1.notes HAVING COUNT ( * ) = 10	2-18662689-6
find the notes of rowing which are in 10 notes but not in 47 notes .	SELECT t1.notes from rowing as t1 WHERE t1.notes = 10 EXCEPT SELECT t1.notes from rowing as t1 WHERE t1.notes = 47	2-18662689-6
list athlete and athlete who have rank greater than 5 or rank shorter than 10 .	SELECT t1.athlete , t1.athlete from rowing as t1 WHERE t1.rank > 5 OR t1.rank < 10	2-18662689-6
show all notes and corresponding number of rowing sorted by the count .	SELECT t1.notes , COUNT ( * ) from rowing as t1 GROUP BY t1.notes ORDER BY COUNT ( * )	2-18662689-6
find the country of the rowing with the highest rank .	SELECT t1.country from rowing as t1 ORDER BY t1.rank DESC LIMIT 1	2-18662689-6
what is the count of rowing with more than 10 athlete ?	SELECT COUNT ( * ) from rowing as t1 GROUP BY t1.athlete HAVING COUNT ( * ) > 10 	2-18662689-6
which notes has both rowing with less than 10 rank and rowing with more than 7 rank ?	SELECT t1.notes from rowing as t1 WHERE t1.rank < 10 INTERSECT SELECT t1.notes from rowing as t1 WHERE t1.rank > 7	2-18662689-6
