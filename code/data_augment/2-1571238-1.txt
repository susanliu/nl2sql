show the fourth place shared by more than 10 great .	SELECT t1.fourth_place from great as t1 GROUP BY t1.fourth_place HAVING COUNT ( * ) > 10	2-1571238-1
how many great are there that have more than {VALUE},0 fourth place ?	SELECT COUNT ( * ) from great as t1 GROUP BY t1.fourth_place HAVING COUNT ( * ) > 10 	2-1571238-1
show the runner-up , runner-up , and fourth place of all the great .	SELECT t1.runner-up , t1.runner-up , t1.fourth_place from great as t1	2-1571238-1
find the champion of the great with the highest year.	SELECT t1.champion from great as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from great as t1 )	2-1571238-1
what is the jack tompkins trophy ( mvp ) of the great with least number of jack tompkins trophy ( mvp ) ?	SELECT t1.jack_tompkins_trophy_ from great as t1 GROUP BY t1.jack_tompkins_trophy_ ORDER BY COUNT ( * ) ASC LIMIT 1	2-1571238-1
which fourth place have an average year over 10 ?	SELECT t1.fourth_place from great as t1 GROUP BY t1.fourth_place HAVING AVG ( t1.year ) >= 10	2-1571238-1
what are the distinct runner-up with year between 10 and 71 ?	SELECT DISTINCT t1.runner-up from great as t1 WHERE t1.year BETWEEN 10 AND 71	2-1571238-1
find the fourth place of great who have both 10 and 87 year .	SELECT t1.fourth_place from great as t1 WHERE t1.year = 10 INTERSECT SELECT t1.fourth_place from great as t1 WHERE t1.year = 87	2-1571238-1
find the jack tompkins trophy ( mvp ) of great who have both 10 and 82 year .	SELECT t1.jack_tompkins_trophy_ from great as t1 WHERE t1.year = 10 INTERSECT SELECT t1.jack_tompkins_trophy_ from great as t1 WHERE t1.year = 82	2-1571238-1
count the number of great in champion 10 or 89 .	SELECT COUNT ( * ) from great as t1 WHERE t1.champion = 10 OR t1.champion = 89	2-1571238-1
