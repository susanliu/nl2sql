find the developer ( s ) and franchise of the 1990s with at least 10 release date .	SELECT t1.developer_ , t1.franchise from 1990s as t1 GROUP BY t1.release_date HAVING COUNT ( * ) >= 10	2-18590048-1
Show everything on 1990s	SELECT * FROM 1990s	2-18590048-1
what are the franchise and title of 1990s with 10 or more release date ?	SELECT t1.franchise , t1.title from 1990s as t1 GROUP BY t1.release_date HAVING COUNT ( * ) >= 10	2-18590048-1
return the smallest rank for every release date .	SELECT MIN ( t1.rank ) , t1.release_date from 1990s as t1 GROUP BY t1.release_date	2-18590048-1
what are the title of all 1990s with release date that is 10 ?	SELECT t1.title from 1990s as t1 GROUP BY t1.release_date HAVING COUNT ( * ) = 10	2-18590048-1
count the number of 1990s that have an title containing 10 .	SELECT COUNT ( * ) from 1990s as t1 WHERE t1.title LIKE 10	2-18590048-1
find the release date of the 1990s with the highest units sold ( in millions ).	SELECT t1.release_date from 1990s as t1 WHERE t1.units_sold_ = ( SELECT MAX ( t1.units_sold_ ) from 1990s as t1 )	2-18590048-1
give the t1.title with the fewest 1990s .	SELECT t1.title from 1990s as t1 GROUP BY t1.title ORDER BY COUNT ( * ) LIMIT 1	2-18590048-1
what is the minimum rank in each platform ?	SELECT MIN ( t1.rank ) , t1.platform from 1990s as t1 GROUP BY t1.platform	2-18590048-1
find the franchise of 1990s which have 10 but no 95 as title .	SELECT t1.franchise from 1990s as t1 WHERE t1.title = 10 EXCEPT SELECT t1.franchise from 1990s as t1 WHERE t1.title = 95	2-18590048-1
