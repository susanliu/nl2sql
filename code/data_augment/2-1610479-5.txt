select the average top-5 of each bill 's tournament .	SELECT AVG ( t1.top-5 ) , t1.tournament from bill as t1 GROUP BY t1.tournament	2-1610479-5
Show everything on bill	SELECT * FROM bill	2-1610479-5
what is the tournament of the bill who has the highest number of bill ?	SELECT t1.tournament from bill as t1 GROUP BY t1.tournament ORDER BY COUNT ( * ) DESC LIMIT 1	2-1610479-5
show tournament and the number of distinct tournament in each tournament .	SELECT t1.tournament , COUNT ( DISTINCT t1.tournament ) from bill as t1 GROUP BY t1.tournament	2-1610479-5
what is the minimum top-10 in each tournament ?	SELECT MIN ( t1.top-10 ) , t1.tournament from bill as t1 GROUP BY t1.tournament	2-1610479-5
which tournament has the least top-25 ?	SELECT t1.tournament from bill as t1 ORDER BY t1.top-25 ASC LIMIT 1	2-1610479-5
find the tournament who has exactly 10 bill .	SELECT t1.tournament from bill as t1 GROUP BY t1.tournament HAVING COUNT ( * ) = 10	2-1610479-5
which tournament have less than 3 in bill ?	SELECT t1.tournament from bill as t1 GROUP BY t1.tournament HAVING COUNT ( * ) < 3	2-1610479-5
what are the tournament of bill with cuts made above the average cuts made across all bill ?	SELECT t1.tournament from bill as t1 WHERE t1.cuts_made > ( SELECT AVG ( t1.cuts_made ) from bill as t1	2-1610479-5
what is the tournament and tournament of every bill that has a top-5 lower than average ?	SELECT t1.tournament , t1.tournament from bill as t1 WHERE t1.top-5 < ( SELECT AVG ( t1.top-5 ) {FROM, 3} )	2-1610479-5
