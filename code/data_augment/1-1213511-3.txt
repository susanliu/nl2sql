find the nhl team who has exactly 10 1971 .	SELECT t1.nhl_team from 1971 as t1 GROUP BY t1.nhl_team HAVING COUNT ( * ) = 10	1-1213511-3
show all player and corresponding number of 1971 sorted by the count .	SELECT t1.player , COUNT ( * ) from 1971 as t1 GROUP BY t1.player ORDER BY COUNT ( * )	1-1213511-3
return the position of the largest pick #.	SELECT t1.position from 1971 as t1 ORDER BY t1.pick_ DESC LIMIT 1	1-1213511-3
what are the player that have greater pick # than any pick # in 1971 ?	SELECT t1.player from 1971 as t1 WHERE t1.pick_ > ( SELECT MIN ( t1.pick_ ) from 1971 as t1 )	1-1213511-3
find the number of 1971 that have more than 10 nhl team .	SELECT COUNT ( * ) from 1971 as t1 GROUP BY t1.nhl_team HAVING COUNT ( * ) > 10 	1-1213511-3
find the position of the 1971 which have college/junior/club team 10 but not 65 .	SELECT t1.position from 1971 as t1 WHERE t1.college/junior/club_team = 10 EXCEPT SELECT t1.position from 1971 as t1 WHERE t1.college/junior/club_team = 65	1-1213511-3
show the nationality and the number of 1971 for each nationality in the ascending order.	SELECT t1.nationality , COUNT ( * ) from 1971 as t1 GROUP BY t1.nationality ORDER BY COUNT ( * )	1-1213511-3
find the number of 1971 that have more than 10 nhl team .	SELECT COUNT ( * ) from 1971 as t1 GROUP BY t1.nhl_team HAVING COUNT ( * ) > 10 	1-1213511-3
find the college/junior/club team of 1971 which have 10 but no 78 as position .	SELECT t1.college/junior/club_team from 1971 as t1 WHERE t1.position = 10 EXCEPT SELECT t1.college/junior/club_team from 1971 as t1 WHERE t1.position = 78	1-1213511-3
what are the college/junior/club team of the 1971 that have exactly 10 1971 ?	SELECT t1.college/junior/club_team from 1971 as t1 GROUP BY t1.college/junior/club_team HAVING COUNT ( * ) = 10	1-1213511-3
