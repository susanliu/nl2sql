what are the {venue of all the 1925 , and the total crowd by each ?	SELECT t1.venue , SUM ( t1.crowd ) from 1925 as t1 GROUP BY t1.venue	2-10746200-5
what is the minimum crowd in each home team ?	SELECT MIN ( t1.crowd ) , t1.home_team from 1925 as t1 GROUP BY t1.home_team	2-10746200-5
find the date and home team of the 1925 whose crowd is lower than the average crowd of all 1925 .	SELECT t1.date , t1.home_team from 1925 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10746200-5
find the date and home team of the 1925 whose crowd is lower than the average crowd of all 1925 .	SELECT t1.date , t1.home_team from 1925 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10746200-5
find the home team of the 1925 with the highest crowd.	SELECT t1.home_team from 1925 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1925 as t1 )	2-10746200-5
give the t1.venue with the fewest 1925 .	SELECT t1.venue from 1925 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) LIMIT 1	2-10746200-5
what is the date of the 1925 with the largest crowd ?	SELECT t1.date from 1925 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1925 as t1 )	2-10746200-5
what are the away team score , home team score , and home team score for each 1925 ?	SELECT t1.away_team_score , t1.home_team_score , t1.home_team_score from 1925 as t1	2-10746200-5
what are the away team score and venue of 1925 with 10 or more venue ?	SELECT t1.away_team_score , t1.venue from 1925 as t1 GROUP BY t1.venue HAVING COUNT ( * ) >= 10	2-10746200-5
return the smallest crowd for every away team score .	SELECT MIN ( t1.crowd ) , t1.away_team_score from 1925 as t1 GROUP BY t1.away_team_score	2-10746200-5
