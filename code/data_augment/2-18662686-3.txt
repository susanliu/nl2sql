how many notes did each rowing do, ordered by number of notes ?	SELECT t1.notes , COUNT ( * ) from rowing as t1 GROUP BY t1.notes ORDER BY COUNT ( * )	2-18662686-3
count the number of rowing that have an time containing 10 .	SELECT COUNT ( * ) from rowing as t1 WHERE t1.time LIKE 10	2-18662686-3
find the time of rowing which have 10 but no 53 as country .	SELECT t1.time from rowing as t1 WHERE t1.country = 10 EXCEPT SELECT t1.time from rowing as t1 WHERE t1.country = 53	2-18662686-3
list all country which have rank higher than the average .	SELECT t1.country from rowing as t1 WHERE t1.rank > ( SELECT AVG ( t1.rank ) from rowing as t1	2-18662686-3
what is the notes and rowers of the rowing with the top 5 smallest rank ?	SELECT t1.notes , t1.rowers from rowing as t1 ORDER BY t1.rank LIMIT 5	2-18662686-3
find the time of the rowing which have time 10 but not 59 .	SELECT t1.time from rowing as t1 WHERE t1.time = 10 EXCEPT SELECT t1.time from rowing as t1 WHERE t1.time = 59	2-18662686-3
what is the maximum and mininum rank {COLUMN} for all rowing ?	SELECT MAX ( t1.rank ) , MIN ( t1.rank ) from rowing as t1	2-18662686-3
how many country did each rowing do, ordered by number of country ?	SELECT t1.country , COUNT ( * ) from rowing as t1 GROUP BY t1.country ORDER BY COUNT ( * )	2-18662686-3
what are the rowers of rowing whose country are not 10 ?	SELECT t1.rowers from rowing as t1 WHERE t1.country ! = 10	2-18662686-3
list notes and notes who have rank greater than 5 or rank shorter than 10 .	SELECT t1.notes , t1.notes from rowing as t1 WHERE t1.rank > 5 OR t1.rank < 10	2-18662686-3
