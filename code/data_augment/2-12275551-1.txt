what are the women 's singles of the new that have exactly 10 new ?	SELECT t1.women_'s_singles from new as t1 GROUP BY t1.women_'s_singles HAVING COUNT ( * ) = 10	2-12275551-1
what is the men 's doubles of new with the maximum year across all new ?	SELECT t1.men_'s_doubles from new as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from new as t1 )	2-12275551-1
what is the maximum and mininum year {COLUMN} for all new ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from new as t1	2-12275551-1
which men 's doubles have greater year than that of any year in new ?	SELECT t1.men_'s_doubles from new as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from new as t1 )	2-12275551-1
what is the mixed doubles and women 's singles of the new with maximum year ?	SELECT t1.mixed_doubles , t1.women_'s_singles from new as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-12275551-1
find the women 's doubles of the new with the largest year .	SELECT t1.women_'s_doubles from new as t1 ORDER BY t1.year DESC LIMIT 1	2-12275551-1
what are the women 's doubles of new whose women 's singles is not 10 ?	SELECT t1.women_'s_doubles from new as t1 WHERE t1.women_'s_singles ! = 10	2-12275551-1
what are the average year of new for different men 's singles ?	SELECT AVG ( t1.year ) , t1.men_'s_singles from new as t1 GROUP BY t1.men_'s_singles	2-12275551-1
what are the men 's doubles with exactly 10 new ?	SELECT t1.men_'s_doubles from new as t1 GROUP BY t1.men_'s_doubles HAVING COUNT ( * ) = 10	2-12275551-1
show the women 's doubles of new whose mixed doubles are not 10.	SELECT t1.women_'s_doubles from new as t1 WHERE t1.mixed_doubles ! = 10	2-12275551-1
