find the sponsor and date introduced of the matthew with at least 10 congress .	SELECT t1.sponsor , t1.date_introduced from matthew as t1 GROUP BY t1.congress HAVING COUNT ( * ) >= 10	2-11045469-1
which date introduced have less than 3 in matthew ?	SELECT t1.date_introduced from matthew as t1 GROUP BY t1.date_introduced HAVING COUNT ( * ) < 3	2-11045469-1
find the distinct date introduced of matthew having # of cosponsors between 10 and 85 .	SELECT DISTINCT t1.date_introduced from matthew as t1 WHERE t1.# BETWEEN 10 AND 85	2-11045469-1
what is the minimum # of cosponsors in each congress ?	SELECT MIN ( t1.# ) , t1.congress from matthew as t1 GROUP BY t1.congress	2-11045469-1
what is the count of matthew with more than 10 sponsor ?	SELECT COUNT ( * ) from matthew as t1 GROUP BY t1.sponsor HAVING COUNT ( * ) > 10 	2-11045469-1
what is the congress of the matthew who has the highest number of matthew ?	SELECT t1.congress from matthew as t1 GROUP BY t1.congress ORDER BY COUNT ( * ) DESC LIMIT 1	2-11045469-1
what are the congress of all matthew with congress that is 10 ?	SELECT t1.congress from matthew as t1 GROUP BY t1.congress HAVING COUNT ( * ) = 10	2-11045469-1
what are the congress of all matthew that have 10 or more matthew ?	SELECT t1.congress from matthew as t1 GROUP BY t1.congress HAVING COUNT ( * ) >= 10	2-11045469-1
what are the average # of cosponsors of matthew for different bill number ?	SELECT AVG ( t1.# ) , t1.bill_number from matthew as t1 GROUP BY t1.bill_number	2-11045469-1
how many distinct congress correspond to each congress ?	SELECT t1.congress , COUNT ( DISTINCT t1.congress ) from matthew as t1 GROUP BY t1.congress	2-11045469-1
