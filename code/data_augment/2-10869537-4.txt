find the away team of the 1973 with the highest crowd.	SELECT t1.away_team from 1973 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1973 as t1 )	2-10869537-4
find the venue which have exactly 10 1973 .	SELECT t1.venue from 1973 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10869537-4
how many 1973 are there in home team score 10 or 95 ?	SELECT COUNT ( * ) from 1973 as t1 WHERE t1.home_team_score = 10 OR t1.home_team_score = 95	2-10869537-4
which t1.away_team has least number of 1973 ?	SELECT t1.away_team from 1973 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) LIMIT 1	2-10869537-4
which home team score have greater crowd than that of any crowd in 1973 ?	SELECT t1.home_team_score from 1973 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1973 as t1 )	2-10869537-4
find the away team of 1973 who have more than 10 1973 .	SELECT t1.away_team from 1973 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) > 10	2-10869537-4
find the number of 1973 that have more than 10 away team .	SELECT COUNT ( * ) from 1973 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) > 10 	2-10869537-4
what are the away team and away team score of each 1973 , listed in descending order by crowd ?	SELECT t1.away_team , t1.away_team_score from 1973 as t1 ORDER BY t1.crowd DESC	2-10869537-4
show the date and away team score with at least 10 home team .	SELECT t1.date , t1.away_team_score from 1973 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10869537-4
what are the distinct away team with crowd between 10 and 36 ?	SELECT DISTINCT t1.away_team from 1973 as t1 WHERE t1.crowd BETWEEN 10 AND 36	2-10869537-4
