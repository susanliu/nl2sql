what are the surface of all john with championship that is 10 ?	SELECT t1.surface from john as t1 GROUP BY t1.championship HAVING COUNT ( * ) = 10	1-23259077-1
what are the maximum and minimum year across all john ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from john as t1	1-23259077-1
which score in the final have greater year than that of any year in john ?	SELECT t1.score_in_the_final from john as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from john as t1 )	1-23259077-1
what are the score in the final of all john with championship that is 10 ?	SELECT t1.score_in_the_final from john as t1 GROUP BY t1.championship HAVING COUNT ( * ) = 10	1-23259077-1
show the outcome with fewer than 3 john .	SELECT t1.outcome from john as t1 GROUP BY t1.outcome HAVING COUNT ( * ) < 3	1-23259077-1
what are the outcome more than 10 john have ?	SELECT t1.outcome from john as t1 GROUP BY t1.outcome HAVING COUNT ( * ) > 10	1-23259077-1
what are the score in the final of john whose opponent in the final are not 10 ?	SELECT t1.score_in_the_final from john as t1 WHERE t1.opponent_in_the_final ! = 10	1-23259077-1
find the opponent in the final of the john that is most frequent across all opponent in the final .	SELECT t1.opponent_in_the_final from john as t1 GROUP BY t1.opponent_in_the_final ORDER BY COUNT ( * ) DESC LIMIT 1	1-23259077-1
find the number of john whose opponent in the final contain the word 10 .	SELECT COUNT ( * ) from john as t1 WHERE t1.opponent_in_the_final LIKE 10	1-23259077-1
which surface has both john with less than 10 year and john with more than 44 year ?	SELECT t1.surface from john as t1 WHERE t1.year < 10 INTERSECT SELECT t1.surface from john as t1 WHERE t1.year > 44	1-23259077-1
