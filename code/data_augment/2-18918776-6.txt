show the peak of list who have at least 10 list .	SELECT t1.peak from list as t1 GROUP BY t1.peak HAVING COUNT ( * ) >= 10	2-18918776-6
find the distinct peak of list having prominence ( m ) between 10 and 72 .	SELECT DISTINCT t1.peak from list as t1 WHERE t1.prominence_ BETWEEN 10 AND 72	2-18918776-6
please show the different country , ordered by the number of list that have each .	SELECT t1.country from list as t1 GROUP BY t1.country ORDER BY COUNT ( * ) ASC LIMIT 1	2-18918776-6
show the peak and the total elevation ( m ) of list .	SELECT t1.peak , SUM ( t1.elevation_ ) from list as t1 GROUP BY t1.peak	2-18918776-6
what are the maximum and minimum elevation ( m ) across all list ?	SELECT MAX ( t1.elevation_ ) , MIN ( t1.elevation_ ) from list as t1	2-18918776-6
please show the country of the list with count more than 10 .	SELECT t1.country from list as t1 GROUP BY t1.country HAVING COUNT ( * ) > 10	2-18918776-6
find the peak of the list with the highest prominence ( m ) .	SELECT t1.peak from list as t1 ORDER BY t1.prominence_ DESC LIMIT 1	2-18918776-6
give the maximum and minimum prominence ( m ) of all list .	SELECT MAX ( t1.prominence_ ) , MIN ( t1.prominence_ ) from list as t1	2-18918776-6
count the number of list in country 10 or 77 .	SELECT COUNT ( * ) from list as t1 WHERE t1.country = 10 OR t1.country = 77	2-18918776-6
what is the minimum elevation ( m ) in each peak ?	SELECT MIN ( t1.elevation_ ) , t1.peak from list as t1 GROUP BY t1.peak	2-18918776-6
