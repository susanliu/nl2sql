give the maximum and minimum year of all lpga .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from lpga as t1	1-229059-2
find the distinct score of all lpga that have a higher year than some lpga with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.score from lpga as t1 WHERE t1.year > ( SELECT MIN ( t1.score ) from lpga as t1 WHERE t1.dates = 10 )	1-229059-2
list champion and score who have winners share ( $ ) greater than 5 or winners share ( $ ) shorter than 10 .	SELECT t1.champion , t1.score from lpga as t1 WHERE t1.winners_share_ > 5 OR t1.winners_share_ < 10	1-229059-2
what are the champion of the lpga that have exactly 10 lpga ?	SELECT t1.champion from lpga as t1 GROUP BY t1.champion HAVING COUNT ( * ) = 10	1-229059-2
find the dates and dates of the lpga with at least 10 dates .	SELECT t1.dates , t1.dates from lpga as t1 GROUP BY t1.dates HAVING COUNT ( * ) >= 10	1-229059-2
find the score of lpga which have both 10 and 8 as score .	SELECT t1.score from lpga as t1 WHERE t1.winners_share_ = 10 INTERSECT SELECT t1.score from lpga as t1 WHERE t1.winners_share_ = 8	1-229059-2
find the dates of lpga which are in 10 country but not in 21 country .	SELECT t1.dates from lpga as t1 WHERE t1.country = 10 EXCEPT SELECT t1.dates from lpga as t1 WHERE t1.country = 21	1-229059-2
what is the margin of victory and country of the lpga with maximum purse ( $ ) ?	SELECT t1.margin_of_victory , t1.country from lpga as t1 WHERE t1.purse_ = ( SELECT MAX ( t1.purse_ ) {FROM, 3} )	1-229059-2
what are the score and score ?	SELECT t1.score , t1.score from lpga as t1	1-229059-2
what are the score with exactly 10 lpga ?	SELECT t1.score from lpga as t1 GROUP BY t1.score HAVING COUNT ( * ) = 10	1-229059-2
