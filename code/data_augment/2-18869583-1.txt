which manufacturer has both list with less than 10 co 2 g/km and list with more than 26 co 2 g/km ?	SELECT t1.manufacturer from list as t1 WHERE t1.co_2_g/km < 10 INTERSECT SELECT t1.manufacturer from list as t1 WHERE t1.co_2_g/km > 26	2-18869583-1
show all information on the list that has the largest number of fuel type.	SELECT * from list as t1 ORDER BY t1.fuel_type DESC LIMIT 1	2-18869583-1
what are the green rating of all list with engine capacity that is 10 ?	SELECT t1.green_rating from list as t1 GROUP BY t1.engine_capacity HAVING COUNT ( * ) = 10	2-18869583-1
what is the model and manufacturer of the list with the top 5 smallest mpg-us combined ?	SELECT t1.model , t1.manufacturer from list as t1 ORDER BY t1.mpg-us_combined LIMIT 5	2-18869583-1
please show the engine capacity of the list with count more than 10 .	SELECT t1.engine_capacity from list as t1 GROUP BY t1.engine_capacity HAVING COUNT ( * ) > 10	2-18869583-1
what is the transmission of list with the maximum mpg-us combined across all list ?	SELECT t1.transmission from list as t1 WHERE t1.mpg-us_combined = ( SELECT MAX ( t1.mpg-us_combined ) from list as t1 )	2-18869583-1
return the manufacturer of list that do not have the manufacturer 10 .	SELECT t1.manufacturer from list as t1 WHERE t1.manufacturer ! = 10	2-18869583-1
return the model of list for which the fuel type is not 10 ?	SELECT t1.model from list as t1 WHERE t1.fuel_type ! = 10	2-18869583-1
what are the transmission of all list that have 10 or more list ?	SELECT t1.transmission from list as t1 GROUP BY t1.transmission HAVING COUNT ( * ) >= 10	2-18869583-1
what are the average l/100km urban ( cold ) of list for different green rating ?	SELECT AVG ( t1.l/100km_urban_ ) , t1.green_rating from list as t1 GROUP BY t1.green_rating	2-18869583-1
