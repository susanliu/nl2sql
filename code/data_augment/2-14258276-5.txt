return the description and edge of royal with the five lowest year of issue .	SELECT t1.description , t1.edge from royal as t1 ORDER BY t1.year_of_issue LIMIT 5	2-14258276-5
show the weight and thickness with at least 10 description .	SELECT t1.weight , t1.thickness from royal as t1 GROUP BY t1.description HAVING COUNT ( * ) >= 10	2-14258276-5
what are the thickness for royal who have more than the average year of issue?	SELECT t1.thickness from royal as t1 WHERE t1.year_of_issue > ( SELECT AVG ( t1.year_of_issue ) from royal as t1	2-14258276-5
how many royal are there that have more than {VALUE},0 description ?	SELECT COUNT ( * ) from royal as t1 GROUP BY t1.description HAVING COUNT ( * ) > 10 	2-14258276-5
find the thickness who has exactly 10 royal .	SELECT t1.thickness from royal as t1 GROUP BY t1.thickness HAVING COUNT ( * ) = 10	2-14258276-5
find the weight and edge of the royal with at least 10 diameter .	SELECT t1.weight , t1.edge from royal as t1 GROUP BY t1.diameter HAVING COUNT ( * ) >= 10	2-14258276-5
find the number of royal that have more than 10 thickness .	SELECT COUNT ( * ) from royal as t1 GROUP BY t1.thickness HAVING COUNT ( * ) > 10 	2-14258276-5
find the description of royal which have both 10 and 88 as description .	SELECT t1.description from royal as t1 WHERE t1.year_of_issue = 10 INTERSECT SELECT t1.description from royal as t1 WHERE t1.year_of_issue = 88	2-14258276-5
which diameter have less than 3 in royal ?	SELECT t1.diameter from royal as t1 GROUP BY t1.diameter HAVING COUNT ( * ) < 3	2-14258276-5
list the edge which average year of issue is above 10 .	SELECT t1.edge from royal as t1 GROUP BY t1.edge HAVING AVG ( t1.year_of_issue ) >= 10	2-14258276-5
