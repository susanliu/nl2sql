return the smallest crowd for every away team score .	SELECT MIN ( t1.crowd ) , t1.away_team_score from 1937 as t1 GROUP BY t1.away_team_score	2-10806194-4
find the home team of 1937 who have both 10 and 1 crowd .	SELECT t1.home_team from 1937 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.home_team from 1937 as t1 WHERE t1.crowd = 1	2-10806194-4
what are the home team score for all 1937 , and what is the total {crowd for each ?	SELECT t1.home_team_score , SUM ( t1.crowd ) from 1937 as t1 GROUP BY t1.home_team_score	2-10806194-4
list the date and venue of all 1937 sorted by crowd in descending order .	SELECT t1.date , t1.venue from 1937 as t1 ORDER BY t1.crowd DESC	2-10806194-4
what is the away team score of 1937 with the maximum crowd across all 1937 ?	SELECT t1.away_team_score from 1937 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1937 as t1 )	2-10806194-4
list the venue which average crowd is above 10 .	SELECT t1.venue from 1937 as t1 GROUP BY t1.venue HAVING AVG ( t1.crowd ) >= 10	2-10806194-4
which home team score is the most frequent home team score?	SELECT t1.home_team_score from 1937 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-10806194-4
find the home team score and venue of the 1937 whose crowd is lower than the average crowd of all 1937 .	SELECT t1.home_team_score , t1.venue from 1937 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10806194-4
show the away team score and away team score with at least 10 home team .	SELECT t1.away_team_score , t1.away_team_score from 1937 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10806194-4
find the date of the 1937 who has the largest number of 1937 .	SELECT t1.date from 1937 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-10806194-4
