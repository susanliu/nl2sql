what is the maximum and mininum sr no . {COLUMN} for all list ?	SELECT MAX ( t1.sr_no_ ) , MIN ( t1.sr_no_ ) from list as t1	2-10891830-3
select the average br no . of each list 's builder .	SELECT AVG ( t1.br_no_ ) , t1.builder from list as t1 GROUP BY t1.builder	2-10891830-3
find the sr name of the list with the largest sr no . .	SELECT t1.sr_name from list as t1 ORDER BY t1.sr_no_ DESC LIMIT 1	2-10891830-3
return the smallest sr no . for every withdrawn .	SELECT MIN ( t1.sr_no_ ) , t1.withdrawn from list as t1 GROUP BY t1.withdrawn	2-10891830-3
what is the built and builder for the list with the rank 5 smallest br no . ?	SELECT t1.built , t1.builder from list as t1 ORDER BY t1.br_no_ LIMIT 5	2-10891830-3
Return all columns in list .	SELECT * FROM list	2-10891830-3
which builder have greater br no . than that of any br no . in list ?	SELECT t1.builder from list as t1 WHERE t1.br_no_ > ( SELECT MIN ( t1.br_no_ ) from list as t1 )	2-10891830-3
which withdrawn is the most frequent withdrawn?	SELECT t1.withdrawn from list as t1 GROUP BY t1.withdrawn ORDER BY COUNT ( * ) DESC LIMIT 1	2-10891830-3
give the t1.withdrawn with the fewest list .	SELECT t1.withdrawn from list as t1 GROUP BY t1.withdrawn ORDER BY COUNT ( * ) LIMIT 1	2-10891830-3
return the different built of list , in ascending order of frequency .	SELECT t1.built from list as t1 GROUP BY t1.built ORDER BY COUNT ( * ) ASC LIMIT 1	2-10891830-3
