list the lap-by-lap , ratings and the network of the list .	SELECT t1.lap-by-lap , t1.ratings , t1.network from list as t1	2-11691212-3
how many list does each network have ?	SELECT t1.network , COUNT ( * ) from list as t1 GROUP BY t1.network ORDER BY COUNT ( * )	2-11691212-3
what is the lap-by-lap and lap-by-lap of the list with maximum year ?	SELECT t1.lap-by-lap , t1.lap-by-lap from list as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-11691212-3
find the ratings of list whose year is higher than the average year .	SELECT t1.ratings from list as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from list as t1	2-11691212-3
find the number of list that have more than 10 s color commentator .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.s_color_commentator HAVING COUNT ( * ) > 10 	2-11691212-3
which lap-by-lap has both list with less than 10 year and list with more than 61 year ?	SELECT t1.lap-by-lap from list as t1 WHERE t1.year < 10 INTERSECT SELECT t1.lap-by-lap from list as t1 WHERE t1.year > 61	2-11691212-3
please show the ratings of the list that have at least 10 records .	SELECT t1.ratings from list as t1 GROUP BY t1.ratings HAVING COUNT ( * ) >= 10	2-11691212-3
how many list are there that have more than {VALUE},0 s color commentator ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.s_color_commentator HAVING COUNT ( * ) > 10 	2-11691212-3
find the network of list who have year of both 10 and 88 .	SELECT t1.network from list as t1 WHERE t1.year = 10 INTERSECT SELECT t1.network from list as t1 WHERE t1.year = 88	2-11691212-3
show the pre-race host of list who have at least 10 list .	SELECT t1.pre-race_host from list as t1 GROUP BY t1.pre-race_host HAVING COUNT ( * ) >= 10	2-11691212-3
