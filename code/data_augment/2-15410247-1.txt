how many list correspond to each municipality? show the result in ascending order.	SELECT t1.municipality , COUNT ( * ) from list as t1 GROUP BY t1.municipality ORDER BY COUNT ( * )	2-15410247-1
Return all columns in list .	SELECT * FROM list	2-15410247-1
what are the {municipality of all the list , and the total population ( 2010 census ) by each ?	SELECT t1.municipality , SUM ( t1.population_ ) from list as t1 GROUP BY t1.municipality	2-15410247-1
return the municipality of the largest rank.	SELECT t1.municipality from list as t1 ORDER BY t1.rank DESC LIMIT 1	2-15410247-1
return the maximum and minimum rank across all list .	SELECT MAX ( t1.rank ) , MIN ( t1.rank ) from list as t1	2-15410247-1
find the municipality of list who have both 10 and 33 population ( 2005 census ) .	SELECT t1.municipality from list as t1 WHERE t1.population_ = 10 INTERSECT SELECT t1.municipality from list as t1 WHERE t1.population_ = 33	2-15410247-1
which municipality have an average population ( 2010 census ) over 10 ?	SELECT t1.municipality from list as t1 GROUP BY t1.municipality HAVING AVG ( t1.population_ ) >= 10	2-15410247-1
find the distinct municipality of all list that have a higher population ( 2005 census ) than some list with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.municipality from list as t1 WHERE t1.population_ > ( SELECT MIN ( t1.municipality ) from list as t1 WHERE t1.municipality = 10 )	2-15410247-1
show the municipality , municipality , and municipality of all the list .	SELECT t1.municipality , t1.municipality , t1.municipality from list as t1	2-15410247-1
show all information on the list that has the largest number of municipality.	SELECT * from list as t1 ORDER BY t1.municipality DESC LIMIT 1	2-15410247-1
