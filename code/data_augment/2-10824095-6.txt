show home team score and home team score of 1982 .	SELECT t1.home_team_score , t1.home_team_score from 1982 as t1	2-10824095-6
which date have greater crowd than that of any crowd in 1982 ?	SELECT t1.date from 1982 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1982 as t1 )	2-10824095-6
select the average crowd of each 1982 's home team score .	SELECT AVG ( t1.crowd ) , t1.home_team_score from 1982 as t1 GROUP BY t1.home_team_score	2-10824095-6
what is the minimum crowd in each home team score ?	SELECT MIN ( t1.crowd ) , t1.home_team_score from 1982 as t1 GROUP BY t1.home_team_score	2-10824095-6
what are total crowd for each home team ?	SELECT t1.home_team , SUM ( t1.crowd ) from 1982 as t1 GROUP BY t1.home_team	2-10824095-6
what are the home team score of 1982 , sorted by their frequency?	SELECT t1.home_team_score from 1982 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-10824095-6
show away team score and home team of 1982 .	SELECT t1.away_team_score , t1.home_team from 1982 as t1	2-10824095-6
find the home team and away team score of the 1982 with at least 10 away team .	SELECT t1.home_team , t1.away_team_score from 1982 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) >= 10	2-10824095-6
what is the date and away team of the 1982 with maximum crowd ?	SELECT t1.date , t1.away_team from 1982 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10824095-6
what are the home team score that have greater crowd than any crowd in 1982 ?	SELECT t1.home_team_score from 1982 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1982 as t1 )	2-10824095-6
