show the ground and the number of 2002 for each ground in the ascending order.	SELECT t1.ground , COUNT ( * ) from 2002 as t1 GROUP BY t1.ground ORDER BY COUNT ( * )	2-16388478-3
how many distinct away team correspond to each home team score ?	SELECT t1.home_team_score , COUNT ( DISTINCT t1.away_team ) from 2002 as t1 GROUP BY t1.home_team_score	2-16388478-3
find the date and away team of the 2002 with at least 10 home team score .	SELECT t1.date , t1.away_team from 2002 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) >= 10	2-16388478-3
how many away team did each 2002 do, ordered by number of away team ?	SELECT t1.away_team , COUNT ( * ) from 2002 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * )	2-16388478-3
find the distinct date of 2002 having crowd between 10 and 35 .	SELECT DISTINCT t1.date from 2002 as t1 WHERE t1.crowd BETWEEN 10 AND 35	2-16388478-3
find the ground which have exactly 10 2002 .	SELECT t1.ground from 2002 as t1 GROUP BY t1.ground HAVING COUNT ( * ) = 10	2-16388478-3
please show the home team score of the 2002 that have at least 10 records .	SELECT t1.home_team_score from 2002 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) >= 10	2-16388478-3
what is the away team of highest crowd ?	SELECT t1.away_team from 2002 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-16388478-3
what is the date and away team score of every 2002 that has a crowd lower than average ?	SELECT t1.date , t1.away_team_score from 2002 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-16388478-3
which date is the most frequent date?	SELECT t1.date from 2002 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-16388478-3
