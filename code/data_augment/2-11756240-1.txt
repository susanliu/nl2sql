which team have an average games played over 10 ?	SELECT t1.team from 1895 as t1 GROUP BY t1.team HAVING AVG ( t1.games_played ) >= 10	2-11756240-1
sort the list of team and team of all 1895 in the descending order of games played .	SELECT t1.team , t1.team from 1895 as t1 ORDER BY t1.games_played DESC	2-11756240-1
how many 1895 are there in team 10 or 57 ?	SELECT COUNT ( * ) from 1895 as t1 WHERE t1.team = 10 OR t1.team = 57	2-11756240-1
find the team of the 1895 with the largest games played .	SELECT t1.team from 1895 as t1 ORDER BY t1.games_played DESC LIMIT 1	2-11756240-1
which team has both 1895 with less than 10 goals against and 1895 with more than 19 goals against ?	SELECT t1.team from 1895 as t1 WHERE t1.goals_against < 10 INTERSECT SELECT t1.team from 1895 as t1 WHERE t1.goals_against > 19	2-11756240-1
what is the team of the 1895 who has the highest number of 1895 ?	SELECT t1.team from 1895 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) DESC LIMIT 1	2-11756240-1
find the team of 1895 who have more than 10 1895 .	SELECT t1.team from 1895 as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10	2-11756240-1
show the team and the corresponding number of 1895 sorted by the number of team in ascending order .	SELECT t1.team , COUNT ( * ) from 1895 as t1 GROUP BY t1.team ORDER BY COUNT ( * )	2-11756240-1
what is the team and team for the 1895 with the rank 5 smallest goals against ?	SELECT t1.team , t1.team from 1895 as t1 ORDER BY t1.goals_against LIMIT 5	2-11756240-1
Return all columns in 1895 .	SELECT * FROM 1895	2-11756240-1
