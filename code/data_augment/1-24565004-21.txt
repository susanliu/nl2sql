give the position that has the most list .	SELECT t1.position from list as t1 GROUP BY t1.position ORDER BY COUNT ( * ) DESC LIMIT 1	1-24565004-21
what is the position of the most common list in all position ?	SELECT t1.position from list as t1 GROUP BY t1.position ORDER BY COUNT ( * ) DESC LIMIT 1	1-24565004-21
which period has most number of list ?	SELECT t1.period from list as t1 GROUP BY t1.period ORDER BY COUNT ( * ) DESC LIMIT 1	1-24565004-21
how many list correspond to each name? show the result in ascending order.	SELECT t1.name , COUNT ( * ) from list as t1 GROUP BY t1.name ORDER BY COUNT ( * )	1-24565004-21
find the name of list who have more than 10 list .	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) > 10	1-24565004-21
return the different name of list , in ascending order of frequency .	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) ASC LIMIT 1	1-24565004-21
which period is the most frequent period?	SELECT t1.period from list as t1 GROUP BY t1.period ORDER BY COUNT ( * ) DESC LIMIT 1	1-24565004-21
show all period and corresponding number of list in the ascending order of the numbers.	SELECT t1.period , COUNT ( * ) from list as t1 GROUP BY t1.period ORDER BY COUNT ( * )	1-24565004-21
show all information on the list that has the largest number of name.	SELECT * from list as t1 ORDER BY t1.name DESC LIMIT 1	1-24565004-21
return the name of the list that has the fewest corresponding name .	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) ASC LIMIT 1	1-24565004-21
