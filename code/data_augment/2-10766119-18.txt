find the venue and away team of the 1928 whose crowd is lower than the average crowd of all 1928 .	SELECT t1.venue , t1.away_team from 1928 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10766119-18
which home team has both 1928 with less than 10 crowd and 1928 with more than 1 crowd ?	SELECT t1.home_team from 1928 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1928 as t1 WHERE t1.crowd > 1	2-10766119-18
what is the home team of 1928 with the maximum crowd across all 1928 ?	SELECT t1.home_team from 1928 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1928 as t1 )	2-10766119-18
find the home team of the 1928 that have just 10 1928 .	SELECT t1.home_team from 1928 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) = 10	2-10766119-18
list the home team which average crowd is above 10 .	SELECT t1.home_team from 1928 as t1 GROUP BY t1.home_team HAVING AVG ( t1.crowd ) >= 10	2-10766119-18
how many distinct away team score correspond to each home team score ?	SELECT t1.home_team_score , COUNT ( DISTINCT t1.away_team_score ) from 1928 as t1 GROUP BY t1.home_team_score	2-10766119-18
find the away team of 1928 which are date 10 but not date 75 .	SELECT t1.away_team from 1928 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.away_team from 1928 as t1 WHERE t1.date = 75	2-10766119-18
what are the venue of 1928 , sorted by their frequency?	SELECT t1.venue from 1928 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) ASC LIMIT 1	2-10766119-18
which t1.home_team_score has the fewest 1928 ?	SELECT t1.home_team_score from 1928 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10766119-18
show the date shared by more than 10 1928 .	SELECT t1.date from 1928 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10	2-10766119-18
