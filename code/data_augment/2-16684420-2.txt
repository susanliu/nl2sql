find the country of the 2009 with the highest stolen ends for.	SELECT t1.country from 2009 as t1 WHERE t1.stolen_ends_for = ( SELECT MAX ( t1.stolen_ends_for ) from 2009 as t1 )	2-16684420-2
what is the blank ends f/a of the 2009 with the smallest ends won ?	SELECT t1.blank_ends_f/a from 2009 as t1 ORDER BY t1.ends_won ASC LIMIT 1	2-16684420-2
what are the skip of all 2009 that have 10 or more 2009 ?	SELECT t1.skip from 2009 as t1 GROUP BY t1.skip HAVING COUNT ( * ) >= 10	2-16684420-2
show the country with fewer than 3 2009 .	SELECT t1.country from 2009 as t1 GROUP BY t1.country HAVING COUNT ( * ) < 3	2-16684420-2
find the distinct country of all 2009 that have a higher ends lost than some 2009 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.country from 2009 as t1 WHERE t1.ends_lost > ( SELECT MIN ( t1.country ) from 2009 as t1 WHERE t1.skip = 10 )	2-16684420-2
find the skip of 2009 which have 10 but no 66 as blank ends f/a .	SELECT t1.skip from 2009 as t1 WHERE t1.blank_ends_f/a = 10 EXCEPT SELECT t1.skip from 2009 as t1 WHERE t1.blank_ends_f/a = 66	2-16684420-2
what are the country and blank ends f/a of all 2009 sorted by decreasing stolen ends against ?	SELECT t1.country , t1.blank_ends_f/a from 2009 as t1 ORDER BY t1.stolen_ends_against DESC	2-16684420-2
what are the country for 2009 that have an stolen ends against greater than the average .	SELECT t1.country from 2009 as t1 WHERE t1.stolen_ends_against > ( SELECT AVG ( t1.stolen_ends_against ) from 2009 as t1	2-16684420-2
find the skip of 2009 who have both 10 and 36 stolen ends for .	SELECT t1.skip from 2009 as t1 WHERE t1.stolen_ends_for = 10 INTERSECT SELECT t1.skip from 2009 as t1 WHERE t1.stolen_ends_for = 36	2-16684420-2
what are the blank ends f/a of all 2009 with blank ends f/a that is 10 ?	SELECT t1.blank_ends_f/a from 2009 as t1 GROUP BY t1.blank_ends_f/a HAVING COUNT ( * ) = 10	2-16684420-2
