Return all columns in 2005 .	SELECT * FROM 2005	2-14898305-1
find the team of 2005 which have 10 but no 11 as difference .	SELECT t1.team from 2005 as t1 WHERE t1.difference = 10 EXCEPT SELECT t1.team from 2005 as t1 WHERE t1.difference = 11	2-14898305-1
what is the count of 2005 with more than 10 team ?	SELECT COUNT ( * ) from 2005 as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10 	2-14898305-1
show the team of 2005 whose team are not 10.	SELECT t1.team from 2005 as t1 WHERE t1.team ! = 10	2-14898305-1
return the smallest points for every team .	SELECT MIN ( t1.points ) , t1.team from 2005 as t1 GROUP BY t1.team	2-14898305-1
find the team of 2005 which are in 10 team but not in 59 team .	SELECT t1.team from 2005 as t1 WHERE t1.team = 10 EXCEPT SELECT t1.team from 2005 as t1 WHERE t1.team = 59	2-14898305-1
what are the average drawn of 2005 for different difference ?	SELECT AVG ( t1.drawn ) , t1.difference from 2005 as t1 GROUP BY t1.difference	2-14898305-1
what are the team for 2005 who have more than the average drawn?	SELECT t1.team from 2005 as t1 WHERE t1.drawn > ( SELECT AVG ( t1.drawn ) from 2005 as t1	2-14898305-1
find the team of the 2005 with the highest drawn .	SELECT t1.team from 2005 as t1 ORDER BY t1.drawn DESC LIMIT 1	2-14898305-1
find the number of 2005 whose difference contain the word 10 .	SELECT COUNT ( * ) from 2005 as t1 WHERE t1.difference LIKE 10	2-14898305-1
