which jockey have greater year than that of any year in colonial ?	SELECT t1.jockey from colonial as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from colonial as t1 )	2-11237859-1
show the winner and the total year of colonial .	SELECT t1.winner , SUM ( t1.year ) from colonial as t1 GROUP BY t1.winner	2-11237859-1
find the trainer of the colonial with the highest year.	SELECT t1.trainer from colonial as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from colonial as t1 )	2-11237859-1
which time has both colonial with less than 10 year and colonial with more than 65 year ?	SELECT t1.time from colonial as t1 WHERE t1.year < 10 INTERSECT SELECT t1.time from colonial as t1 WHERE t1.year > 65	2-11237859-1
what are the winner of all colonial with owner that is 10 ?	SELECT t1.winner from colonial as t1 GROUP BY t1.owner HAVING COUNT ( * ) = 10	2-11237859-1
how many colonial have owner that contain the word 10 ?	SELECT COUNT ( * ) from colonial as t1 WHERE t1.owner LIKE 10	2-11237859-1
what are the winner and owner of all colonial sorted by decreasing year ?	SELECT t1.winner , t1.owner from colonial as t1 ORDER BY t1.year DESC	2-11237859-1
what is the winner of the colonial with the minimum year ?	SELECT t1.winner from colonial as t1 ORDER BY t1.year ASC LIMIT 1	2-11237859-1
what are total year for each distance ( miles ) ?	SELECT t1.distance_ , SUM ( t1.year ) from colonial as t1 GROUP BY t1.distance_	2-11237859-1
what is the distance ( miles ) of highest year ?	SELECT t1.distance_ from colonial as t1 ORDER BY t1.year DESC LIMIT 1	2-11237859-1
