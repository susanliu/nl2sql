what are the winning manufacturer , fastest lap , and fastest lap of each 2000 ?	SELECT t1.winning_manufacturer , t1.fastest_lap , t1.fastest_lap from 2000 as t1	2-12563443-2
what is the winning driver and winning manufacturer of the 2000 with maximum round ?	SELECT t1.winning_driver , t1.winning_manufacturer from 2000 as t1 WHERE t1.round = ( SELECT MAX ( t1.round ) {FROM, 3} )	2-12563443-2
show the winning driver of 2000 whose circuit are not 10.	SELECT t1.winning_driver from 2000 as t1 WHERE t1.circuit ! = 10	2-12563443-2
what are the circuit and winning manufacturer of the {COLUMN} who have round above five or round below ten ?	SELECT t1.circuit , t1.winning_manufacturer from 2000 as t1 WHERE t1.round > 5 OR t1.round < 10	2-12563443-2
which fastest lap is the most frequent fastest lap?	SELECT t1.fastest_lap from 2000 as t1 GROUP BY t1.fastest_lap ORDER BY COUNT ( * ) DESC LIMIT 1	2-12563443-2
find the number of 2000 that have more than 10 winning driver .	SELECT COUNT ( * ) from 2000 as t1 GROUP BY t1.winning_driver HAVING COUNT ( * ) > 10 	2-12563443-2
what are the fastest lap of all 2000 with fastest lap that is 10 ?	SELECT t1.fastest_lap from 2000 as t1 GROUP BY t1.fastest_lap HAVING COUNT ( * ) = 10	2-12563443-2
what are the winning team and circuit of the {COLUMN} who have round above five or round below ten ?	SELECT t1.winning_team , t1.circuit from 2000 as t1 WHERE t1.round > 5 OR t1.round < 10	2-12563443-2
show winning manufacturer and the number of distinct winning manufacturer in each winning manufacturer .	SELECT t1.winning_manufacturer , COUNT ( DISTINCT t1.winning_manufacturer ) from 2000 as t1 GROUP BY t1.winning_manufacturer	2-12563443-2
what is the winning manufacturer and circuit of every 2000 that has a round lower than average ?	SELECT t1.winning_manufacturer , t1.circuit from 2000 as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-12563443-2
