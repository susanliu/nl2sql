show the home team score with fewer than 3 1942 .	SELECT t1.home_team_score from 1942 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) < 3	2-10807990-12
count the number of 1942 that have an home team score containing 10 .	SELECT COUNT ( * ) from 1942 as t1 WHERE t1.home_team_score LIKE 10	2-10807990-12
list the date of the 1942 whose date is not 10 .	SELECT t1.date from 1942 as t1 WHERE t1.date ! = 10	2-10807990-12
show all home team score and corresponding number of 1942 in the ascending order of the numbers.	SELECT t1.home_team_score , COUNT ( * ) from 1942 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * )	2-10807990-12
which venue has most number of 1942 ?	SELECT t1.venue from 1942 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) DESC LIMIT 1	2-10807990-12
find the date of the 1942 who has the largest number of 1942 .	SELECT t1.date from 1942 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-10807990-12
find the number of 1942 that have more than 10 away team score .	SELECT COUNT ( * ) from 1942 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10 	2-10807990-12
what is the away team and date for the 1942 with the rank 5 smallest crowd ?	SELECT t1.away_team , t1.date from 1942 as t1 ORDER BY t1.crowd LIMIT 5	2-10807990-12
show all information on the 1942 that has the largest number of home team.	SELECT * from 1942 as t1 ORDER BY t1.home_team DESC LIMIT 1	2-10807990-12
what is the away team and home team of the 1942 with maximum crowd ?	SELECT t1.away_team , t1.home_team from 1942 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10807990-12
