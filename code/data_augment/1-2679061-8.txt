what is the position of 1983 with the maximum pick # across all 1983 ?	SELECT t1.position from 1983 as t1 WHERE t1.pick_ = ( SELECT MAX ( t1.pick_ ) from 1983 as t1 )	1-2679061-8
what is the nationality of 1983 with the maximum pick # across all 1983 ?	SELECT t1.nationality from 1983 as t1 WHERE t1.pick_ = ( SELECT MAX ( t1.pick_ ) from 1983 as t1 )	1-2679061-8
please show the college/junior/club team of the 1983 that have at least 10 records .	SELECT t1.college/junior/club_team from 1983 as t1 GROUP BY t1.college/junior/club_team HAVING COUNT ( * ) >= 10	1-2679061-8
find the player that have 10 1983 .	SELECT t1.player from 1983 as t1 GROUP BY t1.player HAVING COUNT ( * ) = 10	1-2679061-8
how many distinct position correspond to each position ?	SELECT t1.position , COUNT ( DISTINCT t1.position ) from 1983 as t1 GROUP BY t1.position	1-2679061-8
which player has both 1983 with less than 10 pick # and 1983 with more than 75 pick # ?	SELECT t1.player from 1983 as t1 WHERE t1.pick_ < 10 INTERSECT SELECT t1.player from 1983 as t1 WHERE t1.pick_ > 75	1-2679061-8
show the player of the 1983 that has the greatest number of 1983 .	SELECT t1.player from 1983 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) DESC LIMIT 1	1-2679061-8
what are the distinct COLUMN_NAME,0} of every 1983 that has a greater pick # than some 1983 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.nationality from 1983 as t1 WHERE t1.pick_ > ( SELECT MIN ( t1.nationality ) from 1983 as t1 WHERE t1.college/junior/club_team = 10 )	1-2679061-8
find the player and nhl team of the 1983 whose pick # is lower than the average pick # of all 1983 .	SELECT t1.player , t1.nhl_team from 1983 as t1 WHERE t1.pick_ < ( SELECT AVG ( t1.pick_ ) {FROM, 3} )	1-2679061-8
return the smallest pick # for every player .	SELECT MIN ( t1.pick_ ) , t1.player from 1983 as t1 GROUP BY t1.player	1-2679061-8
