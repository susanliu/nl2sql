list all information about 2007 .	SELECT * FROM 2007	2-14431362-8
what is the first of the 2007 with the minimum uni # ?	SELECT t1.first from 2007 as t1 ORDER BY t1.uni_ ASC LIMIT 1	2-14431362-8
what are total uni # for each surname ?	SELECT t1.surname , SUM ( t1.uni_ ) from 2007 as t1 GROUP BY t1.surname	2-14431362-8
count the number of 2007 in throws 10 or 45 .	SELECT COUNT ( * ) from 2007 as t1 WHERE t1.throws = 10 OR t1.throws = 45	2-14431362-8
what are the surname , first , and throws of each 2007 ?	SELECT t1.surname , t1.first , t1.throws from 2007 as t1	2-14431362-8
what are the bats that have greater uni # than any uni # in 2007 ?	SELECT t1.bats from 2007 as t1 WHERE t1.uni_ > ( SELECT MIN ( t1.uni_ ) from 2007 as t1 )	2-14431362-8
please list the bats and throws of 2007 in descending order of uni # .	SELECT t1.bats , t1.throws from 2007 as t1 ORDER BY t1.uni_ DESC	2-14431362-8
what are the distinct position with uni # between 10 and 90 ?	SELECT DISTINCT t1.position from 2007 as t1 WHERE t1.uni_ BETWEEN 10 AND 90	2-14431362-8
what are the maximum and minimum uni # across all 2007 ?	SELECT MAX ( t1.uni_ ) , MIN ( t1.uni_ ) from 2007 as t1	2-14431362-8
what are the bats of the 2007 with bats other than 10 ?	SELECT t1.bats from 2007 as t1 WHERE t1.d.o.b ! = 10	2-14431362-8
