what are the record and opponent of 1966 with 10 or more attendance ?	SELECT t1.record , t1.opponent from 1966 as t1 GROUP BY t1.attendance HAVING COUNT ( * ) >= 10	2-17507817-3
what are the opponent of the 1966 that have exactly 10 1966 ?	SELECT t1.opponent from 1966 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	2-17507817-3
list the attendance of the 1966 whose result is not 10 .	SELECT t1.attendance from 1966 as t1 WHERE t1.result ! = 10	2-17507817-3
return the smallest week for every result .	SELECT MIN ( t1.week ) , t1.result from 1966 as t1 GROUP BY t1.result	2-17507817-3
return the result of the 1966 with the fewest week .	SELECT t1.result from 1966 as t1 ORDER BY t1.week ASC LIMIT 1	2-17507817-3
find the result of 1966 which have both 10 and 19 as result .	SELECT t1.result from 1966 as t1 WHERE t1.week = 10 INTERSECT SELECT t1.result from 1966 as t1 WHERE t1.week = 19	2-17507817-3
what are the opponent of 1966 whose result are not 10 ?	SELECT t1.opponent from 1966 as t1 WHERE t1.result ! = 10	2-17507817-3
sort the list of date and result of all 1966 in the descending order of week .	SELECT t1.date , t1.result from 1966 as t1 ORDER BY t1.week DESC	2-17507817-3
count the number of 1966 in attendance 10 or 54 .	SELECT COUNT ( * ) from 1966 as t1 WHERE t1.attendance = 10 OR t1.attendance = 54	2-17507817-3
find the result of the 1966 that is most frequent across all result .	SELECT t1.result from 1966 as t1 GROUP BY t1.result ORDER BY COUNT ( * ) DESC LIMIT 1	2-17507817-3
