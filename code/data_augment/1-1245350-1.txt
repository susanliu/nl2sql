find the engine capacity which have exactly 10 none .	SELECT t1.engine_capacity from none as t1 GROUP BY t1.engine_capacity HAVING COUNT ( * ) = 10	1-1245350-1
find the engine capacity of none which have 10 but no 8 as engine capacity .	SELECT t1.engine_capacity from none as t1 WHERE t1.engine_capacity = 10 EXCEPT SELECT t1.engine_capacity from none as t1 WHERE t1.engine_capacity = 8	1-1245350-1
find the max speed of none which have both 10 and 50 as max speed .	SELECT t1.max_speed from none as t1 WHERE t1.units_produced = 10 INTERSECT SELECT t1.max_speed from none as t1 WHERE t1.units_produced = 50	1-1245350-1
find the quattroporte iv of the none with the highest units produced.	SELECT t1.quattroporte_iv from none as t1 WHERE t1.units_produced = ( SELECT MAX ( t1.units_produced ) from none as t1 )	1-1245350-1
find the distinct power of all none that have a higher units produced than some none with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.power from none as t1 WHERE t1.units_produced > ( SELECT MIN ( t1.power ) from none as t1 WHERE t1.quattroporte_iv = 10 )	1-1245350-1
what are the power and max speed of all none sorted by decreasing units produced ?	SELECT t1.power , t1.max_speed from none as t1 ORDER BY t1.units_produced DESC	1-1245350-1
what are the average units produced of none for different max speed ?	SELECT AVG ( t1.units_produced ) , t1.max_speed from none as t1 GROUP BY t1.max_speed	1-1245350-1
Show everything on none	SELECT * FROM none	1-1245350-1
find the production period of the none with the highest units produced.	SELECT t1.production_period from none as t1 WHERE t1.units_produced = ( SELECT MAX ( t1.units_produced ) from none as t1 )	1-1245350-1
show production period for all none whose units produced are greater than the average .	SELECT t1.production_period from none as t1 WHERE t1.units_produced > ( SELECT AVG ( t1.units_produced ) from none as t1	1-1245350-1
