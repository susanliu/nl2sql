Return all columns in list .	SELECT * FROM list	2-12017602-11
what are the gender and name of each list , listed in descending order by decile ?	SELECT t1.gender , t1.name from list as t1 ORDER BY t1.decile DESC	2-12017602-11
give the maximum and minimum decile of all list .	SELECT MAX ( t1.decile ) , MIN ( t1.decile ) from list as t1	2-12017602-11
please show the different authority , ordered by the number of list that have each .	SELECT t1.authority from list as t1 GROUP BY t1.authority ORDER BY COUNT ( * ) ASC LIMIT 1	2-12017602-11
how many list are there in gender 10 or 51 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.gender = 10 OR t1.gender = 51	2-12017602-11
what is the years and years of every list that has a roll lower than average ?	SELECT t1.years , t1.years from list as t1 WHERE t1.roll < ( SELECT AVG ( t1.roll ) {FROM, 3} )	2-12017602-11
find the area of list who have decile of both 10 and 58 .	SELECT t1.area from list as t1 WHERE t1.decile = 10 INTERSECT SELECT t1.area from list as t1 WHERE t1.decile = 58	2-12017602-11
what is the gender and gender of every list that has a roll lower than average ?	SELECT t1.gender , t1.gender from list as t1 WHERE t1.roll < ( SELECT AVG ( t1.roll ) {FROM, 3} )	2-12017602-11
what is the maximum and mininum roll {COLUMN} for all list ?	SELECT MAX ( t1.roll ) , MIN ( t1.roll ) from list as t1	2-12017602-11
find the area of the list with the largest roll .	SELECT t1.area from list as t1 ORDER BY t1.roll DESC LIMIT 1	2-12017602-11
