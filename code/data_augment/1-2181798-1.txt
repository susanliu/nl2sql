what are total poles for each avg . finish ?	SELECT t1.avg_._finish , SUM ( t1.poles ) from hut as t1 GROUP BY t1.avg_._finish	1-2181798-1
show all information on the hut that has the largest number of avg . finish.	SELECT * from hut as t1 ORDER BY t1.avg_._finish DESC LIMIT 1	1-2181798-1
what are the avg . finish for hut who have more than the average wins?	SELECT t1.avg_._finish from hut as t1 WHERE t1.wins > ( SELECT AVG ( t1.wins ) from hut as t1	1-2181798-1
find the winnings which have exactly 10 hut .	SELECT t1.winnings from hut as t1 GROUP BY t1.winnings HAVING COUNT ( * ) = 10	1-2181798-1
find the team ( s ) of hut who have wins of both 10 and 9 .	SELECT t1.team_ from hut as t1 WHERE t1.wins = 10 INTERSECT SELECT t1.team_ from hut as t1 WHERE t1.wins = 9	1-2181798-1
what are the avg . finish of hut with top 5 greater than the average of all hut ?	SELECT t1.avg_._finish from hut as t1 WHERE t1.top_5 > ( SELECT AVG ( t1.top_5 ) from hut as t1	1-2181798-1
what are the position of all hut with avg . start that is 10 ?	SELECT t1.position from hut as t1 GROUP BY t1.avg_._start HAVING COUNT ( * ) = 10	1-2181798-1
list the position and position of all hut sorted by top 5 in descending order .	SELECT t1.position , t1.position from hut as t1 ORDER BY t1.top_5 DESC	1-2181798-1
which avg . finish have greater year than that of any year in hut ?	SELECT t1.avg_._finish from hut as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from hut as t1 )	1-2181798-1
what are the distinct COLUMN_NAME,0} of every hut that has a greater top 5 than some hut with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.avg_._finish from hut as t1 WHERE t1.top_5 > ( SELECT MIN ( t1.avg_._finish ) from hut as t1 WHERE t1.avg_._finish = 10 )	1-2181798-1
