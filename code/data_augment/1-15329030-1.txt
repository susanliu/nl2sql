find the date of counting of the state who has the largest number of state .	SELECT t1.date_of_counting from state as t1 GROUP BY t1.date_of_counting ORDER BY COUNT ( * ) DESC LIMIT 1	1-15329030-1
what is the election winner and date of counting of the state with maximum seats ( acs ) ?	SELECT t1.election_winner , t1.date_of_counting from state as t1 WHERE t1.seats_ = ( SELECT MAX ( t1.seats_ ) {FROM, 3} )	1-15329030-1
what are the state of state , sorted by their frequency?	SELECT t1.state from state as t1 GROUP BY t1.state ORDER BY COUNT ( * ) ASC LIMIT 1	1-15329030-1
what is the incumbent and state of every state that has a seats ( acs ) lower than average ?	SELECT t1.incumbent , t1.state from state as t1 WHERE t1.seats_ < ( SELECT AVG ( t1.seats_ ) {FROM, 3} )	1-15329030-1
find the date of polls of state who have both 10 and 24 seats ( acs ) .	SELECT t1.date_of_polls from state as t1 WHERE t1.seats_ = 10 INTERSECT SELECT t1.date_of_polls from state as t1 WHERE t1.seats_ = 24	1-15329030-1
return the incumbent and election winner of state with the five lowest seats ( acs ) .	SELECT t1.incumbent , t1.election_winner from state as t1 ORDER BY t1.seats_ LIMIT 5	1-15329030-1
which state is the most frequent state?	SELECT t1.state from state as t1 GROUP BY t1.state ORDER BY COUNT ( * ) DESC LIMIT 1	1-15329030-1
what are the date of counting for state who have more than the average seats ( acs )?	SELECT t1.date_of_counting from state as t1 WHERE t1.seats_ > ( SELECT AVG ( t1.seats_ ) from state as t1	1-15329030-1
find all date of polls that have fewer than three in state .	SELECT t1.date_of_polls from state as t1 GROUP BY t1.date_of_polls HAVING COUNT ( * ) < 3	1-15329030-1
give the state that has the most state .	SELECT t1.state from state as t1 GROUP BY t1.state ORDER BY COUNT ( * ) DESC LIMIT 1	1-15329030-1
