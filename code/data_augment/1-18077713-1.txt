list revenue ( us $ million ) and ebit ( us $ m ) who have year to april greater than 5 or year to april shorter than 10 .	SELECT t1.revenue_ , t1.ebit_ from micro as t1 WHERE t1.year_to_april > 5 OR t1.year_to_april < 10	1-18077713-1
show net profit ( us $ m ) and earnings per share ( ¢ ) of micro .	SELECT t1.net_profit_ , t1.earnings_per_share_ from micro as t1	1-18077713-1
what are the distinct ebit ( us $ m ) with year to april between 10 and 11 ?	SELECT DISTINCT t1.ebit_ from micro as t1 WHERE t1.year_to_april BETWEEN 10 AND 11	1-18077713-1
show the earnings per share ( ¢ ) shared by more than 10 micro .	SELECT t1.earnings_per_share_ from micro as t1 GROUP BY t1.earnings_per_share_ HAVING COUNT ( * ) > 10	1-18077713-1
find the revenue ( us $ million ) of micro who have year to april of both 10 and 1 .	SELECT t1.revenue_ from micro as t1 WHERE t1.year_to_april = 10 INTERSECT SELECT t1.revenue_ from micro as t1 WHERE t1.year_to_april = 1	1-18077713-1
what are the revenue ( us $ million ) of all micro with ebit ( us $ m ) that is 10 ?	SELECT t1.revenue_ from micro as t1 GROUP BY t1.ebit_ HAVING COUNT ( * ) = 10	1-18077713-1
what is the revenue ( us $ million ) of highest year to april ?	SELECT t1.revenue_ from micro as t1 ORDER BY t1.year_to_april DESC LIMIT 1	1-18077713-1
show all ebit ( us $ m ) and corresponding number of micro in the ascending order of the numbers.	SELECT t1.ebit_ , COUNT ( * ) from micro as t1 GROUP BY t1.ebit_ ORDER BY COUNT ( * )	1-18077713-1
what is the ebit ( us $ m ) and ebit ( us $ m ) of every micro that has a year to april lower than average ?	SELECT t1.ebit_ , t1.ebit_ from micro as t1 WHERE t1.year_to_april < ( SELECT AVG ( t1.year_to_april ) {FROM, 3} )	1-18077713-1
list earnings per share ( ¢ ) and revenue ( us $ million ) who have year to april greater than 5 or year to april shorter than 10 .	SELECT t1.earnings_per_share_ , t1.revenue_ from micro as t1 WHERE t1.year_to_april > 5 OR t1.year_to_april < 10	1-18077713-1
