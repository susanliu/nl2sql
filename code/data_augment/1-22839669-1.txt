what are the opponent in the final with exactly 10 andre ?	SELECT t1.opponent_in_the_final from andre as t1 GROUP BY t1.opponent_in_the_final HAVING COUNT ( * ) = 10	1-22839669-1
find the distinct championship of andre having year between 10 and 90 .	SELECT DISTINCT t1.championship from andre as t1 WHERE t1.year BETWEEN 10 AND 90	1-22839669-1
return the smallest year for every surface .	SELECT MIN ( t1.year ) , t1.surface from andre as t1 GROUP BY t1.surface	1-22839669-1
return the smallest year for every surface .	SELECT MIN ( t1.year ) , t1.surface from andre as t1 GROUP BY t1.surface	1-22839669-1
find the distinct outcome of all andre that have a higher year than some andre with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.outcome from andre as t1 WHERE t1.year > ( SELECT MIN ( t1.outcome ) from andre as t1 WHERE t1.championship = 10 )	1-22839669-1
find the championship and outcome of the andre whose year is lower than the average year of all andre .	SELECT t1.championship , t1.outcome from andre as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	1-22839669-1
what is the score in the final of the andre with the largest year ?	SELECT t1.score_in_the_final from andre as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from andre as t1 )	1-22839669-1
which championship has both andre with less than 10 year and andre with more than 70 year ?	SELECT t1.championship from andre as t1 WHERE t1.year < 10 INTERSECT SELECT t1.championship from andre as t1 WHERE t1.year > 70	1-22839669-1
show the outcome of the andre that has the greatest number of andre .	SELECT t1.outcome from andre as t1 GROUP BY t1.outcome ORDER BY COUNT ( * ) DESC LIMIT 1	1-22839669-1
find the score in the final and score in the final of the andre whose year is lower than the average year of all andre .	SELECT t1.score_in_the_final , t1.score_in_the_final from andre as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	1-22839669-1
