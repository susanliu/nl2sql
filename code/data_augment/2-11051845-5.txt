what are the declination ( j2000 ) and right ascension ( j2000 ) of list with 10 or more declination ( j2000 ) ?	SELECT t1.declination_ , t1.right_ascension_ from list as t1 GROUP BY t1.declination_ HAVING COUNT ( * ) >= 10	2-11051845-5
list all declination ( j2000 ) which have apparent magnitude higher than the average .	SELECT t1.declination_ from list as t1 WHERE t1.apparent_magnitude > ( SELECT AVG ( t1.apparent_magnitude ) from list as t1	2-11051845-5
find the right ascension ( j2000 ) of list who have more than 10 list .	SELECT t1.right_ascension_ from list as t1 GROUP BY t1.right_ascension_ HAVING COUNT ( * ) > 10	2-11051845-5
what are the distinct object type with apparent magnitude between 10 and 55 ?	SELECT DISTINCT t1.object_type from list as t1 WHERE t1.apparent_magnitude BETWEEN 10 AND 55	2-11051845-5
find the declination ( j2000 ) of the list with the highest ngc number.	SELECT t1.declination_ from list as t1 WHERE t1.ngc_number = ( SELECT MAX ( t1.ngc_number ) from list as t1 )	2-11051845-5
what are the constellation more than 10 list have ?	SELECT t1.constellation from list as t1 GROUP BY t1.constellation HAVING COUNT ( * ) > 10	2-11051845-5
show the right ascension ( j2000 ) of list who have at least 10 list .	SELECT t1.right_ascension_ from list as t1 GROUP BY t1.right_ascension_ HAVING COUNT ( * ) >= 10	2-11051845-5
what are the declination ( j2000 ) and constellation of all list sorted by decreasing apparent magnitude ?	SELECT t1.declination_ , t1.constellation from list as t1 ORDER BY t1.apparent_magnitude DESC	2-11051845-5
what is the right ascension ( j2000 ) of the list with the smallest ngc number ?	SELECT t1.right_ascension_ from list as t1 ORDER BY t1.ngc_number ASC LIMIT 1	2-11051845-5
list the constellation which average apparent magnitude is above 10 .	SELECT t1.constellation from list as t1 GROUP BY t1.constellation HAVING AVG ( t1.apparent_magnitude ) >= 10	2-11051845-5
