return the different team of 1959 , in ascending order of frequency .	SELECT t1.team from 1959 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) ASC LIMIT 1	2-17543955-3
how many 1959 are there in team 10 or 23 ?	SELECT COUNT ( * ) from 1959 as t1 WHERE t1.team = 10 OR t1.team = 23	2-17543955-3
what is the team and team of the 1959 with maximum draw ?	SELECT t1.team , t1.team from 1959 as t1 WHERE t1.draw = ( SELECT MAX ( t1.draw ) {FROM, 3} )	2-17543955-3
what are the team of the 1959 with team other than 10 ?	SELECT t1.team from 1959 as t1 WHERE t1.team ! = 10	2-17543955-3
what are the team more than 10 1959 have ?	SELECT t1.team from 1959 as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10	2-17543955-3
what is the team of the most common 1959 in all team ?	SELECT t1.team from 1959 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) DESC LIMIT 1	2-17543955-3
what are the team and team of 1959 with 10 or more team ?	SELECT t1.team , t1.team from 1959 as t1 GROUP BY t1.team HAVING COUNT ( * ) >= 10	2-17543955-3
how many 1959 are there that have more than {VALUE},0 team ?	SELECT COUNT ( * ) from 1959 as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10 	2-17543955-3
find the distinct COLUMN_NAME,0} of all 1959 that have lost higher than some 1959 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.team from 1959 as t1 WHERE t1.lost > ( SELECT MIN ( t1.team ) from 1959 as t1 WHERE t1.team = 10 )	2-17543955-3
which team have greater match than that of any match in 1959 ?	SELECT t1.team from 1959 as t1 WHERE t1.match > ( SELECT MIN ( t1.match ) from 1959 as t1 )	2-17543955-3
