list the entrant , entrant and the chassis of the ian .	SELECT t1.entrant , t1.entrant , t1.chassis from ian as t1	2-1233850-1
find the distinct engine of ian having points between 10 and 3 .	SELECT DISTINCT t1.engine from ian as t1 WHERE t1.points BETWEEN 10 AND 3	2-1233850-1
show all entrant and corresponding number of ian sorted by the count .	SELECT t1.entrant , COUNT ( * ) from ian as t1 GROUP BY t1.entrant ORDER BY COUNT ( * )	2-1233850-1
what is the maximum and mininum points {COLUMN} for all ian ?	SELECT MAX ( t1.points ) , MIN ( t1.points ) from ian as t1	2-1233850-1
what are the engine of ian with year greater than the average of all ian ?	SELECT t1.engine from ian as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from ian as t1	2-1233850-1
return the entrant and engine of ian with the five lowest year .	SELECT t1.entrant , t1.engine from ian as t1 ORDER BY t1.year LIMIT 5	2-1233850-1
what is the engine of all ian whose year is higher than any ian ?	SELECT t1.engine from ian as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from ian as t1 )	2-1233850-1
what are the engine of all ian with engine that is 10 ?	SELECT t1.engine from ian as t1 GROUP BY t1.engine HAVING COUNT ( * ) = 10	2-1233850-1
Show everything on ian	SELECT * FROM ian	2-1233850-1
what is the entrant of the ian with the largest points ?	SELECT t1.entrant from ian as t1 WHERE t1.points = ( SELECT MAX ( t1.points ) from ian as t1 )	2-1233850-1
