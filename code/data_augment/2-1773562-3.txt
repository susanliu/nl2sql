what are the nominee of all city with category that is 10 ?	SELECT t1.nominee from city as t1 GROUP BY t1.category HAVING COUNT ( * ) = 10	2-1773562-3
show all award and corresponding number of city sorted by the count .	SELECT t1.award , COUNT ( * ) from city as t1 GROUP BY t1.award ORDER BY COUNT ( * )	2-1773562-3
return the category of the largest year.	SELECT t1.category from city as t1 ORDER BY t1.year DESC LIMIT 1	2-1773562-3
find the nominee of the city that is most frequent across all nominee .	SELECT t1.nominee from city as t1 GROUP BY t1.nominee ORDER BY COUNT ( * ) DESC LIMIT 1	2-1773562-3
show category and the number of distinct result in each category .	SELECT t1.category , COUNT ( DISTINCT t1.result ) from city as t1 GROUP BY t1.category	2-1773562-3
find the number of city that have more than 10 nominee .	SELECT COUNT ( * ) from city as t1 GROUP BY t1.nominee HAVING COUNT ( * ) > 10 	2-1773562-3
what are the result , result , and award of each city ?	SELECT t1.result , t1.result , t1.award from city as t1	2-1773562-3
what is all the information on the city with the largest number of result ?	SELECT * from city as t1 ORDER BY t1.result DESC LIMIT 1	2-1773562-3
how many distinct result correspond to each award ?	SELECT t1.award , COUNT ( DISTINCT t1.result ) from city as t1 GROUP BY t1.award	2-1773562-3
find the nominee of city which have both 10 and 85 as nominee .	SELECT t1.nominee from city as t1 WHERE t1.year = 10 INTERSECT SELECT t1.nominee from city as t1 WHERE t1.year = 85	2-1773562-3
