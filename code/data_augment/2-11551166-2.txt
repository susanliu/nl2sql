which city of license have an average erp w over 10 ?	SELECT t1.city_of_license from whrv as t1 GROUP BY t1.city_of_license HAVING AVG ( t1.erp_w ) >= 10	2-11551166-2
which class has both whrv with less than 10 erp w and whrv with more than 81 erp w ?	SELECT t1.class from whrv as t1 WHERE t1.erp_w < 10 INTERSECT SELECT t1.class from whrv as t1 WHERE t1.erp_w > 81	2-11551166-2
return the fcc info of the largest erp w.	SELECT t1.fcc_info from whrv as t1 ORDER BY t1.erp_w DESC LIMIT 1	2-11551166-2
what is the city of license and call sign of the whrv with maximum frequency mhz ?	SELECT t1.city_of_license , t1.call_sign from whrv as t1 WHERE t1.frequency_mhz = ( SELECT MAX ( t1.frequency_mhz ) {FROM, 3} )	2-11551166-2
how many whrv are there that have more than {VALUE},0 city of license ?	SELECT COUNT ( * ) from whrv as t1 GROUP BY t1.city_of_license HAVING COUNT ( * ) > 10 	2-11551166-2
find the fcc info of whrv who have erp w of both 10 and 65 .	SELECT t1.fcc_info from whrv as t1 WHERE t1.erp_w = 10 INTERSECT SELECT t1.fcc_info from whrv as t1 WHERE t1.erp_w = 65	2-11551166-2
find the distinct fcc info of whrv having frequency mhz between 10 and 60 .	SELECT DISTINCT t1.fcc_info from whrv as t1 WHERE t1.frequency_mhz BETWEEN 10 AND 60	2-11551166-2
how many city of license did each whrv do, ordered by number of city of license ?	SELECT t1.city_of_license , COUNT ( * ) from whrv as t1 GROUP BY t1.city_of_license ORDER BY COUNT ( * )	2-11551166-2
which city of license has both whrv with less than 10 frequency mhz and whrv with more than 9 frequency mhz ?	SELECT t1.city_of_license from whrv as t1 WHERE t1.frequency_mhz < 10 INTERSECT SELECT t1.city_of_license from whrv as t1 WHERE t1.frequency_mhz > 9	2-11551166-2
how many whrv are there in fcc info 10 or 92 ?	SELECT COUNT ( * ) from whrv as t1 WHERE t1.fcc_info = 10 OR t1.fcc_info = 92	2-11551166-2
