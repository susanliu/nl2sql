what are the average peak position of i , grouped by weeks ?	SELECT AVG ( t1.peak_position ) , t1.weeks from i as t1 GROUP BY t1.weeks	2-1754908-3
which t1.date has least number of i ?	SELECT t1.date from i as t1 GROUP BY t1.date ORDER BY COUNT ( * ) LIMIT 1	2-1754908-3
show all title and the total peak position for each .	SELECT t1.title , SUM ( t1.peak_position ) from i as t1 GROUP BY t1.title	2-1754908-3
show the date and weeks with at least 10 date .	SELECT t1.date , t1.weeks from i as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-1754908-3
which weeks have greater sales than that of any sales in i ?	SELECT t1.weeks from i as t1 WHERE t1.sales > ( SELECT MIN ( t1.sales ) from i as t1 )	2-1754908-3
list the title which average peak position is above 10 .	SELECT t1.title from i as t1 GROUP BY t1.title HAVING AVG ( t1.peak_position ) >= 10	2-1754908-3
which date has both i with less than 10 sales and i with more than 39 sales ?	SELECT t1.date from i as t1 WHERE t1.sales < 10 INTERSECT SELECT t1.date from i as t1 WHERE t1.sales > 39	2-1754908-3
what are the date of i whose weeks is not 10 ?	SELECT t1.date from i as t1 WHERE t1.weeks ! = 10	2-1754908-3
what are the weeks of all i that have 10 or more i ?	SELECT t1.weeks from i as t1 GROUP BY t1.weeks HAVING COUNT ( * ) >= 10	2-1754908-3
how many i ' weeks have the word 10 in them ?	SELECT COUNT ( * ) from i as t1 WHERE t1.weeks LIKE 10	2-1754908-3
