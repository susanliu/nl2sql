what are the home team score of 1936 with crowd greater than the average of all 1936 ?	SELECT t1.home_team_score from 1936 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1936 as t1	2-10790804-1
what are the date and home team score of 1936 with 10 or more venue ?	SELECT t1.date , t1.home_team_score from 1936 as t1 GROUP BY t1.venue HAVING COUNT ( * ) >= 10	2-10790804-1
how many 1936 are there that have more than {VALUE},0 venue ?	SELECT COUNT ( * ) from 1936 as t1 GROUP BY t1.venue HAVING COUNT ( * ) > 10 	2-10790804-1
find the venue of the 1936 which have venue 10 but not 63 .	SELECT t1.venue from 1936 as t1 WHERE t1.venue = 10 EXCEPT SELECT t1.venue from 1936 as t1 WHERE t1.venue = 63	2-10790804-1
please list the date and home team score of 1936 in descending order of crowd .	SELECT t1.date , t1.home_team_score from 1936 as t1 ORDER BY t1.crowd DESC	2-10790804-1
find the number of 1936 that have more than 10 date .	SELECT COUNT ( * ) from 1936 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10 	2-10790804-1
list the home team which average crowd is above 10 .	SELECT t1.home_team from 1936 as t1 GROUP BY t1.home_team HAVING AVG ( t1.crowd ) >= 10	2-10790804-1
find the away team score of the 1936 that is most frequent across all away team score .	SELECT t1.away_team_score from 1936 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-10790804-1
what is the away team and away team of the 1936 with maximum crowd ?	SELECT t1.away_team , t1.away_team from 1936 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10790804-1
what are the home team of all 1936 that have 10 or more 1936 ?	SELECT t1.home_team from 1936 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10790804-1
