find the team of the 1967 with the highest pick.	SELECT t1.team from 1967 as t1 WHERE t1.pick = ( SELECT MAX ( t1.pick ) from 1967 as t1 )	2-18610536-1
find the hometown/school of the 1967 which have position 10 but not 31 .	SELECT t1.hometown/school from 1967 as t1 WHERE t1.position = 10 EXCEPT SELECT t1.hometown/school from 1967 as t1 WHERE t1.position = 31	2-18610536-1
what are the maximum and minimum pick across all 1967 ?	SELECT MAX ( t1.pick ) , MIN ( t1.pick ) from 1967 as t1	2-18610536-1
find the team of 1967 which are position 10 but not position 82 .	SELECT t1.team from 1967 as t1 WHERE t1.position = 10 EXCEPT SELECT t1.team from 1967 as t1 WHERE t1.position = 82	2-18610536-1
find the team and team of the 1967 whose pick is lower than the average pick of all 1967 .	SELECT t1.team , t1.team from 1967 as t1 WHERE t1.pick < ( SELECT AVG ( t1.pick ) {FROM, 3} )	2-18610536-1
find the number of 1967 that have more than 10 hometown/school .	SELECT COUNT ( * ) from 1967 as t1 GROUP BY t1.hometown/school HAVING COUNT ( * ) > 10 	2-18610536-1
return the different player of 1967 , in ascending order of frequency .	SELECT t1.player from 1967 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-18610536-1
what are the team of the 1967 that have exactly 10 1967 ?	SELECT t1.team from 1967 as t1 GROUP BY t1.team HAVING COUNT ( * ) = 10	2-18610536-1
return the position of the largest pick.	SELECT t1.position from 1967 as t1 ORDER BY t1.pick DESC LIMIT 1	2-18610536-1
show the team , player , and position of all the 1967 .	SELECT t1.team , t1.player , t1.position from 1967 as t1	2-18610536-1
