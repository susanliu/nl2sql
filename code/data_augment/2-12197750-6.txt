what are the average decile of list , grouped by authority ?	SELECT AVG ( t1.decile ) , t1.authority from list as t1 GROUP BY t1.authority	2-12197750-6
Return all columns in list .	SELECT * FROM list	2-12197750-6
what is the minimum decile in each area ?	SELECT MIN ( t1.decile ) , t1.area from list as t1 GROUP BY t1.area	2-12197750-6
which area has both list with less than 10 decile and list with more than 56 decile ?	SELECT t1.area from list as t1 WHERE t1.decile < 10 INTERSECT SELECT t1.area from list as t1 WHERE t1.decile > 56	2-12197750-6
what are the authority and gender of the {COLUMN} who have decile above five or decile below ten ?	SELECT t1.authority , t1.gender from list as t1 WHERE t1.decile > 5 OR t1.decile < 10	2-12197750-6
what is the maximum and mininum roll {COLUMN} for all list ?	SELECT MAX ( t1.roll ) , MIN ( t1.roll ) from list as t1	2-12197750-6
show the area and the number of unique years containing each area .	SELECT t1.area , COUNT ( DISTINCT t1.years ) from list as t1 GROUP BY t1.area	2-12197750-6
what is the name and area of every list that has a roll lower than average ?	SELECT t1.name , t1.area from list as t1 WHERE t1.roll < ( SELECT AVG ( t1.roll ) {FROM, 3} )	2-12197750-6
what are the area , years , and area of each list ?	SELECT t1.area , t1.years , t1.gender from list as t1	2-12197750-6
what is the name of the list with least number of name ?	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) ASC LIMIT 1	2-12197750-6
