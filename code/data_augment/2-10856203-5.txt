what are the distinct tournament with events between 10 and 78 ?	SELECT DISTINCT t1.tournament from nick as t1 WHERE t1.events BETWEEN 10 AND 78	2-10856203-5
find the tournament of nick which have both 10 and 67 as tournament .	SELECT t1.tournament from nick as t1 WHERE t1.top-25 = 10 INTERSECT SELECT t1.tournament from nick as t1 WHERE t1.top-25 = 67	2-10856203-5
what is the tournament of nick with the maximum top-10 across all nick ?	SELECT t1.tournament from nick as t1 WHERE t1.top-10 = ( SELECT MAX ( t1.top-10 ) from nick as t1 )	2-10856203-5
which tournament has both nick with less than 10 events and nick with more than 45 events ?	SELECT t1.tournament from nick as t1 WHERE t1.events < 10 INTERSECT SELECT t1.tournament from nick as t1 WHERE t1.events > 45	2-10856203-5
find the tournament of nick which are tournament 10 but not tournament 66 .	SELECT t1.tournament from nick as t1 WHERE t1.tournament = 10 EXCEPT SELECT t1.tournament from nick as t1 WHERE t1.tournament = 66	2-10856203-5
which tournament has both nick with less than 10 cuts made and nick with more than 29 cuts made ?	SELECT t1.tournament from nick as t1 WHERE t1.cuts_made < 10 INTERSECT SELECT t1.tournament from nick as t1 WHERE t1.cuts_made > 29	2-10856203-5
please show the different tournament , ordered by the number of nick that have each .	SELECT t1.tournament from nick as t1 GROUP BY t1.tournament ORDER BY COUNT ( * ) ASC LIMIT 1	2-10856203-5
find the tournament of nick whose cuts made is more than the average cuts made of nick .	SELECT t1.tournament from nick as t1 WHERE t1.cuts_made > ( SELECT AVG ( t1.cuts_made ) from nick as t1	2-10856203-5
show the tournament of nick who have at least 10 nick .	SELECT t1.tournament from nick as t1 GROUP BY t1.tournament HAVING COUNT ( * ) >= 10	2-10856203-5
what is the minimum cuts made in each tournament ?	SELECT MIN ( t1.cuts_made ) , t1.tournament from nick as t1 GROUP BY t1.tournament	2-10856203-5
