list the position of the sterling whose winnings is not 10 .	SELECT t1.position from sterling as t1 WHERE t1.winnings ! = 10	1-1708014-1
find the distinct team ( s ) of all sterling that have a higher year than some sterling with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.team_ from sterling as t1 WHERE t1.year > ( SELECT MIN ( t1.team_ ) from sterling as t1 WHERE t1.avg_._start = 10 )	1-1708014-1
find the distinct avg . finish of sterling having poles between 10 and 100 .	SELECT DISTINCT t1.avg_._finish from sterling as t1 WHERE t1.poles BETWEEN 10 AND 100	1-1708014-1
show the team ( s ) and the number of unique avg . finish containing each team ( s ) .	SELECT t1.team_ , COUNT ( DISTINCT t1.avg_._finish ) from sterling as t1 GROUP BY t1.team_	1-1708014-1
which position is the most frequent position?	SELECT t1.position from sterling as t1 GROUP BY t1.position ORDER BY COUNT ( * ) DESC LIMIT 1	1-1708014-1
which team ( s ) has both sterling with less than 10 top 5 and sterling with more than 49 top 5 ?	SELECT t1.team_ from sterling as t1 WHERE t1.top_5 < 10 INTERSECT SELECT t1.team_ from sterling as t1 WHERE t1.top_5 > 49	1-1708014-1
return the team ( s ) of the sterling with the fewest wins .	SELECT t1.team_ from sterling as t1 ORDER BY t1.wins ASC LIMIT 1	1-1708014-1
which position have an average year over 10 ?	SELECT t1.position from sterling as t1 GROUP BY t1.position HAVING AVG ( t1.year ) >= 10	1-1708014-1
return the avg . start of the sterling with the fewest wins .	SELECT t1.avg_._start from sterling as t1 ORDER BY t1.wins ASC LIMIT 1	1-1708014-1
what are the position and avg . finish of each sterling , listed in descending order by wins ?	SELECT t1.position , t1.avg_._finish from sterling as t1 ORDER BY t1.wins DESC	1-1708014-1
