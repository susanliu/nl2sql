find the rank of 2002 which have both 10 and 25 as rank .	SELECT t1.rank from 2002 as t1 WHERE t1.silver = 10 INTERSECT SELECT t1.rank from 2002 as t1 WHERE t1.silver = 25	2-18951287-4
find the rank of 2002 which have 10 but no 40 as rank .	SELECT t1.rank from 2002 as t1 WHERE t1.rank = 10 EXCEPT SELECT t1.rank from 2002 as t1 WHERE t1.rank = 40	2-18951287-4
what is the minimum gold in each rank ?	SELECT MIN ( t1.gold ) , t1.rank from 2002 as t1 GROUP BY t1.rank	2-18951287-4
find the rank and rank of the 2002 whose total is lower than the average total of all 2002 .	SELECT t1.rank , t1.rank from 2002 as t1 WHERE t1.total < ( SELECT AVG ( t1.total ) {FROM, 3} )	2-18951287-4
what is the count of 2002 with more than 10 nation ?	SELECT COUNT ( * ) from 2002 as t1 GROUP BY t1.nation HAVING COUNT ( * ) > 10 	2-18951287-4
count the number of 2002 in nation 10 or 20 .	SELECT COUNT ( * ) from 2002 as t1 WHERE t1.nation = 10 OR t1.nation = 20	2-18951287-4
show the nation of the 2002 that has the most 2002 .	SELECT t1.nation from 2002 as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) DESC LIMIT 1	2-18951287-4
what are the distinct COLUMN_NAME,0} of every 2002 that has a greater gold than some 2002 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.rank from 2002 as t1 WHERE t1.gold > ( SELECT MIN ( t1.rank ) from 2002 as t1 WHERE t1.rank = 10 )	2-18951287-4
how many 2002 are there that have more than {VALUE},0 nation ?	SELECT COUNT ( * ) from 2002 as t1 GROUP BY t1.nation HAVING COUNT ( * ) > 10 	2-18951287-4
find the rank and rank of the 2002 whose silver is lower than the average silver of all 2002 .	SELECT t1.rank , t1.rank from 2002 as t1 WHERE t1.silver < ( SELECT AVG ( t1.silver ) {FROM, 3} )	2-18951287-4
