what is the t1.name of swimming that has fewest number of swimming ?	SELECT t1.name from swimming as t1 GROUP BY t1.name ORDER BY COUNT ( * ) LIMIT 1	2-18624773-4
what are the name for all swimming , and what is the total {lane for each ?	SELECT t1.name , SUM ( t1.lane ) from swimming as t1 GROUP BY t1.name	2-18624773-4
return the name of the largest lane.	SELECT t1.name from swimming as t1 ORDER BY t1.lane DESC LIMIT 1	2-18624773-4
please show the different time , ordered by the number of swimming that have each .	SELECT t1.time from swimming as t1 GROUP BY t1.time ORDER BY COUNT ( * ) ASC LIMIT 1	2-18624773-4
Return all columns in swimming .	SELECT * FROM swimming	2-18624773-4
find the nationality of the swimming with the highest lane.	SELECT t1.nationality from swimming as t1 WHERE t1.lane = ( SELECT MAX ( t1.lane ) from swimming as t1 )	2-18624773-4
show time for all swimming whose rank are greater than the average .	SELECT t1.time from swimming as t1 WHERE t1.rank > ( SELECT AVG ( t1.rank ) from swimming as t1	2-18624773-4
find the distinct time of swimming having lane between 10 and 93 .	SELECT DISTINCT t1.time from swimming as t1 WHERE t1.lane BETWEEN 10 AND 93	2-18624773-4
list nationality and time who have lane greater than 5 or lane shorter than 10 .	SELECT t1.nationality , t1.time from swimming as t1 WHERE t1.lane > 5 OR t1.lane < 10	2-18624773-4
return the smallest rank for every time .	SELECT MIN ( t1.rank ) , t1.time from swimming as t1 GROUP BY t1.time	2-18624773-4
