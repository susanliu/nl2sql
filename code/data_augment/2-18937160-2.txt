find the tally of the 1963 with the highest total.	SELECT t1.tally from 1963 as t1 WHERE t1.total = ( SELECT MAX ( t1.total ) from 1963 as t1 )	2-18937160-2
what is the tally and tally of every 1963 that has a total lower than average ?	SELECT t1.tally , t1.tally from 1963 as t1 WHERE t1.total < ( SELECT AVG ( t1.total ) {FROM, 3} )	2-18937160-2
show the tally of 1963 who have at least 10 1963 .	SELECT t1.tally from 1963 as t1 GROUP BY t1.tally HAVING COUNT ( * ) >= 10	2-18937160-2
what are the opposition for all 1963 , and what is the total {total for each ?	SELECT t1.opposition , SUM ( t1.total ) from 1963 as t1 GROUP BY t1.opposition	2-18937160-2
what are the player of 1963 with rank above the average rank across all 1963 ?	SELECT t1.player from 1963 as t1 WHERE t1.rank > ( SELECT AVG ( t1.rank ) from 1963 as t1	2-18937160-2
what are the county of all 1963 with opposition that is 10 ?	SELECT t1.county from 1963 as t1 GROUP BY t1.opposition HAVING COUNT ( * ) = 10	2-18937160-2
what are the county of 1963 , sorted by their frequency?	SELECT t1.county from 1963 as t1 GROUP BY t1.county ORDER BY COUNT ( * ) ASC LIMIT 1	2-18937160-2
what is the opposition of 1963 with the maximum rank across all 1963 ?	SELECT t1.opposition from 1963 as t1 WHERE t1.rank = ( SELECT MAX ( t1.rank ) from 1963 as t1 )	2-18937160-2
what is the minimum rank in each county ?	SELECT MIN ( t1.rank ) , t1.county from 1963 as t1 GROUP BY t1.county	2-18937160-2
which player has both 1963 with less than 10 rank and 1963 with more than 53 rank ?	SELECT t1.player from 1963 as t1 WHERE t1.rank < 10 INTERSECT SELECT t1.player from 1963 as t1 WHERE t1.rank > 53	2-18937160-2
