what are the maximum and minimum crowd across all 1973 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1973 as t1	2-10869537-22
find the date who has exactly 10 1973 .	SELECT t1.date from 1973 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10869537-22
show the venue and the corresponding number of 1973 sorted by the number of venue in ascending order .	SELECT t1.venue , COUNT ( * ) from 1973 as t1 GROUP BY t1.venue ORDER BY COUNT ( * )	2-10869537-22
what is the home team of the 1973 with the smallest crowd ?	SELECT t1.home_team from 1973 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10869537-22
give the t1.home_team_score with the fewest 1973 .	SELECT t1.home_team_score from 1973 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10869537-22
how many 1973 are there that have more than {VALUE},0 home team score ?	SELECT COUNT ( * ) from 1973 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) > 10 	2-10869537-22
what are the home team of 1973 with crowd greater than the average of all 1973 ?	SELECT t1.home_team from 1973 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1973 as t1	2-10869537-22
return the smallest crowd for every date .	SELECT MIN ( t1.crowd ) , t1.date from 1973 as t1 GROUP BY t1.date	2-10869537-22
what is the away team and venue of the 1973 with maximum crowd ?	SELECT t1.away_team , t1.venue from 1973 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10869537-22
show the date of 1973 who have at least 10 1973 .	SELECT t1.date from 1973 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-10869537-22
