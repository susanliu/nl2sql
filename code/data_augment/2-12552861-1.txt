show the men 's singles and their total year of canadian .	SELECT t1.men_'s_singles , SUM ( t1.year ) from canadian as t1 GROUP BY t1.men_'s_singles	2-12552861-1
show the men 's singles and the total year of canadian .	SELECT t1.men_'s_singles , SUM ( t1.year ) from canadian as t1 GROUP BY t1.men_'s_singles	2-12552861-1
what are the women 's doubles and mixed doubles ?	SELECT t1.women_'s_doubles , t1.mixed_doubles from canadian as t1	2-12552861-1
return the maximum and minimum year across all canadian .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from canadian as t1	2-12552861-1
what are the men 's doubles and men 's singles of the {COLUMN} who have year above five or year below ten ?	SELECT t1.men_'s_doubles , t1.men_'s_singles from canadian as t1 WHERE t1.year > 5 OR t1.year < 10	2-12552861-1
show mixed doubles and the number of distinct women 's doubles in each mixed doubles .	SELECT t1.mixed_doubles , COUNT ( DISTINCT t1.women_'s_doubles ) from canadian as t1 GROUP BY t1.mixed_doubles	2-12552861-1
find the distinct women 's singles of canadian having year between 10 and 70 .	SELECT DISTINCT t1.women_'s_singles from canadian as t1 WHERE t1.year BETWEEN 10 AND 70	2-12552861-1
return the different mixed doubles of canadian , in ascending order of frequency .	SELECT t1.mixed_doubles from canadian as t1 GROUP BY t1.mixed_doubles ORDER BY COUNT ( * ) ASC LIMIT 1	2-12552861-1
how many canadian are there that have more than {VALUE},0 women 's doubles ?	SELECT COUNT ( * ) from canadian as t1 GROUP BY t1.women_'s_doubles HAVING COUNT ( * ) > 10 	2-12552861-1
which women 's singles has both canadian with less than 10 year and canadian with more than 72 year ?	SELECT t1.women_'s_singles from canadian as t1 WHERE t1.year < 10 INTERSECT SELECT t1.women_'s_singles from canadian as t1 WHERE t1.year > 72	2-12552861-1
