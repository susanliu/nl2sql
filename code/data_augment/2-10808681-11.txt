what are the date and away team score of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.date , t1.away_team_score from 1967 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10808681-11
return the away team of the 1967 that has the fewest corresponding away team .	SELECT t1.away_team from 1967 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10808681-11
return the date of 1967 that do not have the venue 10 .	SELECT t1.date from 1967 as t1 WHERE t1.venue ! = 10	2-10808681-11
return the home team score of the 1967 that has the fewest corresponding home team score .	SELECT t1.home_team_score from 1967 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-10808681-11
Return all columns in 1967 .	SELECT * FROM 1967	2-10808681-11
list date and date who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.date , t1.date from 1967 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10808681-11
what are the venue and date of 1967 with 10 or more date ?	SELECT t1.venue , t1.date from 1967 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-10808681-11
which t1.home_team_score has the smallest amount of 1967?	SELECT t1.home_team_score from 1967 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10808681-11
what is the home team of all 1967 whose crowd is higher than any 1967 ?	SELECT t1.home_team from 1967 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1967 as t1 )	2-10808681-11
what is the maximum and mininum crowd {COLUMN} for all 1967 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1967 as t1	2-10808681-11
