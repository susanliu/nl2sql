return the smallest crowd for every away team .	SELECT MIN ( t1.crowd ) , t1.away_team from 1946 as t1 GROUP BY t1.away_team	2-10809368-11
list venue and away team score who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.venue , t1.away_team_score from 1946 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10809368-11
find the away team score of 1946 who have more than 10 1946 .	SELECT t1.away_team_score from 1946 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10	2-10809368-11
what is the home team score and date of the 1946 with maximum crowd ?	SELECT t1.home_team_score , t1.date from 1946 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809368-11
list home team score and home team who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.home_team_score , t1.home_team from 1946 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10809368-11
find all away team score that have fewer than three in 1946 .	SELECT t1.away_team_score from 1946 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) < 3	2-10809368-11
show the home team score and the corresponding number of 1946 sorted by the number of home team score in ascending order .	SELECT t1.home_team_score , COUNT ( * ) from 1946 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * )	2-10809368-11
what is the home team score of the 1946 with the smallest crowd ?	SELECT t1.home_team_score from 1946 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10809368-11
what are the maximum and minimum crowd across all 1946 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1946 as t1	2-10809368-11
list all home team score which have crowd higher than the average .	SELECT t1.home_team_score from 1946 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1946 as t1	2-10809368-11
