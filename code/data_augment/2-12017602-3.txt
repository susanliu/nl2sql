which name is the most frequent name?	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) DESC LIMIT 1	2-12017602-3
find the name of list whose roll is more than the average roll of list .	SELECT t1.name from list as t1 WHERE t1.roll > ( SELECT AVG ( t1.roll ) from list as t1	2-12017602-3
what is the name of the most common list in all name ?	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) DESC LIMIT 1	2-12017602-3
return the name of the list with the fewest roll .	SELECT t1.name from list as t1 ORDER BY t1.roll ASC LIMIT 1	2-12017602-3
what is all the information on the list with the largest number of years ?	SELECT * from list as t1 ORDER BY t1.years DESC LIMIT 1	2-12017602-3
sort the list of name and name of all list in the descending order of roll .	SELECT t1.name , t1.name from list as t1 ORDER BY t1.roll DESC	2-12017602-3
what are the name of all list with area that is 10 ?	SELECT t1.name from list as t1 GROUP BY t1.area HAVING COUNT ( * ) = 10	2-12017602-3
find the authority of the list that is most frequent across all authority .	SELECT t1.authority from list as t1 GROUP BY t1.authority ORDER BY COUNT ( * ) DESC LIMIT 1	2-12017602-3
find the area of the list with the largest roll .	SELECT t1.area from list as t1 ORDER BY t1.roll DESC LIMIT 1	2-12017602-3
find the distinct decile of list having roll between 10 and 17 .	SELECT DISTINCT t1.decile from list as t1 WHERE t1.roll BETWEEN 10 AND 17	2-12017602-3
