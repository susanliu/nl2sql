list all information about 1969 .	SELECT * FROM 1969	2-10809157-3
what are the home team score of all 1969 with home team score that is 10 ?	SELECT t1.home_team_score from 1969 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) = 10	2-10809157-3
what are the maximum and minimum crowd across all 1969 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1969 as t1	2-10809157-3
what are all the venue and away team?	SELECT t1.venue , t1.away_team from 1969 as t1	2-10809157-3
which venue is the most frequent venue?	SELECT t1.venue from 1969 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) DESC LIMIT 1	2-10809157-3
what are the away team score , date , and away team of each 1969 ?	SELECT t1.away_team_score , t1.date , t1.away_team from 1969 as t1	2-10809157-3
what are the distinct away team score with crowd between 10 and 92 ?	SELECT DISTINCT t1.away_team_score from 1969 as t1 WHERE t1.crowd BETWEEN 10 AND 92	2-10809157-3
which t1.venue has least number of 1969 ?	SELECT t1.venue from 1969 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) LIMIT 1	2-10809157-3
show the home team and the number of unique away team score containing each home team .	SELECT t1.home_team , COUNT ( DISTINCT t1.away_team_score ) from 1969 as t1 GROUP BY t1.home_team	2-10809157-3
what are the away team and away team score of each 1969 , listed in descending order by crowd ?	SELECT t1.away_team , t1.away_team_score from 1969 as t1 ORDER BY t1.crowd DESC	2-10809157-3
