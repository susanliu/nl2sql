find the distinct venue of 1954 having crowd between 10 and 22 .	SELECT DISTINCT t1.venue from 1954 as t1 WHERE t1.crowd BETWEEN 10 AND 22	2-10773616-14
show the away team of 1954 whose away team are not 10.	SELECT t1.away_team from 1954 as t1 WHERE t1.away_team ! = 10	2-10773616-14
which away team is the most frequent away team?	SELECT t1.away_team from 1954 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10773616-14
which away team has the least crowd ?	SELECT t1.away_team from 1954 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10773616-14
find the home team score of 1954 whose crowd is higher than the average crowd .	SELECT t1.home_team_score from 1954 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1954 as t1	2-10773616-14
what are the home team of 1954 with crowd greater than the average of all 1954 ?	SELECT t1.home_team from 1954 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1954 as t1	2-10773616-14
show the away team score of 1954 whose away team score are not 10.	SELECT t1.away_team_score from 1954 as t1 WHERE t1.away_team_score ! = 10	2-10773616-14
Show everything on 1954	SELECT * FROM 1954	2-10773616-14
what are the distinct home team score with crowd between 10 and 38 ?	SELECT DISTINCT t1.home_team_score from 1954 as t1 WHERE t1.crowd BETWEEN 10 AND 38	2-10773616-14
return the away team score of 1954 that do not have the home team 10 .	SELECT t1.away_team_score from 1954 as t1 WHERE t1.home_team ! = 10	2-10773616-14
