what are the opponent with exactly 10 1982 ?	SELECT t1.opponent from 1982 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	2-13131902-7
what is the score and loss of the 1982 with maximum attendance ?	SELECT t1.score , t1.loss from 1982 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) {FROM, 3} )	2-13131902-7
find all date that have fewer than three in 1982 .	SELECT t1.date from 1982 as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	2-13131902-7
how many 1982 are there that have more than {VALUE},0 date ?	SELECT COUNT ( * ) from 1982 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10 	2-13131902-7
what is all the information on the 1982 with the largest number of loss ?	SELECT * from 1982 as t1 ORDER BY t1.loss DESC LIMIT 1	2-13131902-7
return the different opponent of 1982 , in ascending order of frequency .	SELECT t1.opponent from 1982 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-13131902-7
what are the loss of all 1982 that have 10 or more 1982 ?	SELECT t1.loss from 1982 as t1 GROUP BY t1.loss HAVING COUNT ( * ) >= 10	2-13131902-7
show the opponent of 1982 who have at least 10 1982 .	SELECT t1.opponent from 1982 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-13131902-7
find the site of the 1982 with the highest attendance.	SELECT t1.site from 1982 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) from 1982 as t1 )	2-13131902-7
find the opponent that have 10 1982 .	SELECT t1.opponent from 1982 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	2-13131902-7
