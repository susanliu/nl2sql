return the smallest decile for every authority .	SELECT MIN ( t1.decile ) , t1.authority from list as t1 GROUP BY t1.authority	2-12197750-5
what are the distinct years with decile between 10 and 36 ?	SELECT DISTINCT t1.years from list as t1 WHERE t1.decile BETWEEN 10 AND 36	2-12197750-5
how many list are there that have more than {VALUE},0 years ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.years HAVING COUNT ( * ) > 10 	2-12197750-5
find the gender and name of the list with at least 10 authority .	SELECT t1.gender , t1.name from list as t1 GROUP BY t1.authority HAVING COUNT ( * ) >= 10	2-12197750-5
what are the name of list , sorted by their frequency?	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) ASC LIMIT 1	2-12197750-5
what is the maximum and mininum decile {COLUMN} for all list ?	SELECT MAX ( t1.decile ) , MIN ( t1.decile ) from list as t1	2-12197750-5
find the years of list which have both 10 and 68 as years .	SELECT t1.years from list as t1 WHERE t1.decile = 10 INTERSECT SELECT t1.years from list as t1 WHERE t1.decile = 68	2-12197750-5
display the years , area , and years for each list .	SELECT t1.years , t1.area , t1.years from list as t1	2-12197750-5
list the name which average decile is above 10 .	SELECT t1.name from list as t1 GROUP BY t1.name HAVING AVG ( t1.decile ) >= 10	2-12197750-5
list roll and roll who have decile greater than 5 or decile shorter than 10 .	SELECT t1.roll , t1.roll from list as t1 WHERE t1.decile > 5 OR t1.decile < 10	2-12197750-5
