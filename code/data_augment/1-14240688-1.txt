show the open cup of the portland that has the greatest number of portland .	SELECT t1.open_cup from portland as t1 GROUP BY t1.open_cup ORDER BY COUNT ( * ) DESC LIMIT 1	1-14240688-1
what are the average division of portland , grouped by regular season ?	SELECT AVG ( t1.division ) , t1.regular_season from portland as t1 GROUP BY t1.regular_season	1-14240688-1
show the league and the total division of portland .	SELECT t1.league , SUM ( t1.division ) from portland as t1 GROUP BY t1.league	1-14240688-1
give the maximum and minimum year of all portland .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from portland as t1	1-14240688-1
what is all the information on the portland with the largest number of league ?	SELECT * from portland as t1 ORDER BY t1.league DESC LIMIT 1	1-14240688-1
what is the regular season of the portland with least number of regular season ?	SELECT t1.regular_season from portland as t1 GROUP BY t1.regular_season ORDER BY COUNT ( * ) ASC LIMIT 1	1-14240688-1
return the maximum and minimum year across all portland .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from portland as t1	1-14240688-1
which open cup has most number of portland ?	SELECT t1.open_cup from portland as t1 GROUP BY t1.open_cup ORDER BY COUNT ( * ) DESC LIMIT 1	1-14240688-1
what are the playoffs that have greater year than any year in portland ?	SELECT t1.playoffs from portland as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from portland as t1 )	1-14240688-1
list open cup and regular season who have division greater than 5 or avg . attendance shorter than 10 .	SELECT t1.open_cup , t1.regular_season from portland as t1 WHERE t1.division > 5 OR t1.avg_._attendance < 10	1-14240688-1
