list all information about competitive .	SELECT * FROM competitive	2-1070309-2
find all total assets that have fewer than three in competitive .	SELECT t1.total_assets from competitive as t1 GROUP BY t1.total_assets HAVING COUNT ( * ) < 3	2-1070309-2
what is the total assets of competitive with the maximum pages across all competitive ?	SELECT t1.total_assets from competitive as t1 WHERE t1.pages = ( SELECT MAX ( t1.pages ) from competitive as t1 )	2-1070309-2
what is the total assets of competitive with the maximum form across all competitive ?	SELECT t1.total_assets from competitive as t1 WHERE t1.form = ( SELECT MAX ( t1.form ) from competitive as t1 )	2-1070309-2
find the organization name of the competitive with the highest form .	SELECT t1.organization_name from competitive as t1 ORDER BY t1.form DESC LIMIT 1	2-1070309-2
find the distinct organization name of competitive having year between 10 and 1 .	SELECT DISTINCT t1.organization_name from competitive as t1 WHERE t1.year BETWEEN 10 AND 1	2-1070309-2
list the total assets which average pages is above 10 .	SELECT t1.total_assets from competitive as t1 GROUP BY t1.total_assets HAVING AVG ( t1.pages ) >= 10	2-1070309-2
find the state of competitive which are state 10 but not state 24 .	SELECT t1.state from competitive as t1 WHERE t1.state = 10 EXCEPT SELECT t1.state from competitive as t1 WHERE t1.state = 24	2-1070309-2
what are the state for all competitive , and what is the total {year for each ?	SELECT t1.state , SUM ( t1.year ) from competitive as t1 GROUP BY t1.state	2-1070309-2
Return all columns in competitive .	SELECT * FROM competitive	2-1070309-2
