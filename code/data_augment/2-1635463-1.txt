list the championship and championship of all mickey sorted by year in descending order .	SELECT t1.championship , t1.championship from mickey as t1 ORDER BY t1.year DESC	2-1635463-1
which runner ( s ) - up have an average year over 10 ?	SELECT t1.runner_ from mickey as t1 GROUP BY t1.runner_ HAVING AVG ( t1.year ) >= 10	2-1635463-1
find the winning score of mickey whose year is higher than the average year .	SELECT t1.winning_score from mickey as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from mickey as t1	2-1635463-1
what are the runner ( s ) - up and margin of mickey with 10 or more margin ?	SELECT t1.runner_ , t1.margin from mickey as t1 GROUP BY t1.margin HAVING COUNT ( * ) >= 10	2-1635463-1
return the runner ( s ) - up of the mickey with the fewest year .	SELECT t1.runner_ from mickey as t1 ORDER BY t1.year ASC LIMIT 1	2-1635463-1
find the winning score of mickey which have both 10 and 100 as winning score .	SELECT t1.winning_score from mickey as t1 WHERE t1.year = 10 INTERSECT SELECT t1.winning_score from mickey as t1 WHERE t1.year = 100	2-1635463-1
what are the maximum and minimum year across all mickey ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from mickey as t1	2-1635463-1
show all championship and corresponding number of mickey sorted by the count .	SELECT t1.championship , COUNT ( * ) from mickey as t1 GROUP BY t1.championship ORDER BY COUNT ( * )	2-1635463-1
show the margin of mickey who have at least 10 mickey .	SELECT t1.margin from mickey as t1 GROUP BY t1.margin HAVING COUNT ( * ) >= 10	2-1635463-1
what are the winning score and winning score of the {COLUMN} who have year above five or year below ten ?	SELECT t1.winning_score , t1.winning_score from mickey as t1 WHERE t1.year > 5 OR t1.year < 10	2-1635463-1
