what are the average laps of pat , grouped by rank ?	SELECT AVG ( t1.laps ) , t1.rank from pat as t1 GROUP BY t1.rank	2-1252142-1
find the distinct finish of all pat that have a higher laps than some pat with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.finish from pat as t1 WHERE t1.laps > ( SELECT MIN ( t1.finish ) from pat as t1 WHERE t1.start = 10 )	2-1252142-1
which year have greater laps than that of any laps in pat ?	SELECT t1.year from pat as t1 WHERE t1.laps > ( SELECT MIN ( t1.laps ) from pat as t1 )	2-1252142-1
show the year with fewer than 3 pat .	SELECT t1.year from pat as t1 GROUP BY t1.year HAVING COUNT ( * ) < 3	2-1252142-1
which finish has both pat with less than 10 laps and pat with more than 56 laps ?	SELECT t1.finish from pat as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.finish from pat as t1 WHERE t1.laps > 56	2-1252142-1
how many distinct qual correspond to each start ?	SELECT t1.start , COUNT ( DISTINCT t1.qual ) from pat as t1 GROUP BY t1.start	2-1252142-1
what is the minimum laps in each finish ?	SELECT MIN ( t1.laps ) , t1.finish from pat as t1 GROUP BY t1.finish	2-1252142-1
find the year of pat which are qual 10 but not qual 86 .	SELECT t1.year from pat as t1 WHERE t1.qual = 10 EXCEPT SELECT t1.year from pat as t1 WHERE t1.qual = 86	2-1252142-1
which finish has both pat with less than 10 laps and pat with more than 30 laps ?	SELECT t1.finish from pat as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.finish from pat as t1 WHERE t1.laps > 30	2-1252142-1
which finish has the least laps ?	SELECT t1.finish from pat as t1 ORDER BY t1.laps ASC LIMIT 1	2-1252142-1
