what is the count of list with more than 10 height feet / m ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.height_feet_/_m HAVING COUNT ( * ) > 10 	2-12815540-4
what are the maximum and minimum floors across all list ?	SELECT MAX ( t1.floors ) , MIN ( t1.floors ) from list as t1	2-12815540-4
which name has most number of list ?	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) DESC LIMIT 1	2-12815540-4
what is the street address and street address of the list with the top 5 smallest floors ?	SELECT t1.street_address , t1.street_address from list as t1 ORDER BY t1.floors LIMIT 5	2-12815540-4
what are the street address and street address of the {COLUMN} who have floors above five or floors below ten ?	SELECT t1.street_address , t1.street_address from list as t1 WHERE t1.floors > 5 OR t1.floors < 10	2-12815540-4
find the years as tallest of list whose floors is higher than the average floors .	SELECT t1.years_as_tallest from list as t1 WHERE t1.floors > ( SELECT AVG ( t1.floors ) from list as t1	2-12815540-4
return the maximum and minimum floors across all list .	SELECT MAX ( t1.floors ) , MIN ( t1.floors ) from list as t1	2-12815540-4
return the smallest floors for every height feet / m .	SELECT MIN ( t1.floors ) , t1.height_feet_/_m from list as t1 GROUP BY t1.height_feet_/_m	2-12815540-4
what is the street address of the list with the minimum floors ?	SELECT t1.street_address from list as t1 ORDER BY t1.floors ASC LIMIT 1	2-12815540-4
how many list are there for each name ? list the smallest count first .	SELECT t1.name , COUNT ( * ) from list as t1 GROUP BY t1.name ORDER BY COUNT ( * )	2-12815540-4
