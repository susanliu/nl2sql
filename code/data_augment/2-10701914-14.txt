how many 1951 are there that have more than {VALUE},0 home team score ?	SELECT COUNT ( * ) from 1951 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) > 10 	2-10701914-14
Return all columns in 1951 .	SELECT * FROM 1951	2-10701914-14
find the away team score and home team of the 1951 with at least 10 home team .	SELECT t1.away_team_score , t1.home_team from 1951 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10701914-14
how many 1951 have date that contains 10 ?	SELECT COUNT ( * ) from 1951 as t1 WHERE t1.date LIKE 10	2-10701914-14
find the venue and venue of the 1951 with at least 10 away team score .	SELECT t1.venue , t1.venue from 1951 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) >= 10	2-10701914-14
what are the home team score for 1951 who have more than the average crowd?	SELECT t1.home_team_score from 1951 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1951 as t1	2-10701914-14
what are the average crowd of 1951 , grouped by home team ?	SELECT AVG ( t1.crowd ) , t1.home_team from 1951 as t1 GROUP BY t1.home_team	2-10701914-14
what is the date of the 1951 who has the highest number of 1951 ?	SELECT t1.date from 1951 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-10701914-14
find the distinct home team of 1951 having crowd between 10 and 96 .	SELECT DISTINCT t1.home_team from 1951 as t1 WHERE t1.crowd BETWEEN 10 AND 96	2-10701914-14
list away team score of 1951 that have the number of 1951 greater than 10 .	SELECT t1.away_team_score from 1951 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10	2-10701914-14
