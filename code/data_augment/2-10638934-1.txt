what are the years of list with apps greater than the average of all list ?	SELECT t1.years from list as t1 WHERE t1.apps > ( SELECT AVG ( t1.apps ) from list as t1	2-10638934-1
find the distinct nationality of all list that have a higher apps than some list with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.nationality from list as t1 WHERE t1.apps > ( SELECT MIN ( t1.nationality ) from list as t1 WHERE t1.years = 10 )	2-10638934-1
which years has both list with less than 10 apps and list with more than 83 apps ?	SELECT t1.years from list as t1 WHERE t1.apps < 10 INTERSECT SELECT t1.years from list as t1 WHERE t1.apps > 83	2-10638934-1
find the years of the list with the highest apps.	SELECT t1.years from list as t1 WHERE t1.apps = ( SELECT MAX ( t1.apps ) from list as t1 )	2-10638934-1
count the number of list in position 10 or 16 .	SELECT COUNT ( * ) from list as t1 WHERE t1.position = 10 OR t1.position = 16	2-10638934-1
which years have less than 3 in list ?	SELECT t1.years from list as t1 GROUP BY t1.years HAVING COUNT ( * ) < 3	2-10638934-1
which years has the least apps ?	SELECT t1.years from list as t1 ORDER BY t1.apps ASC LIMIT 1	2-10638934-1
how many list are there that have more than {VALUE},0 position ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.position HAVING COUNT ( * ) > 10 	2-10638934-1
find the distinct nationality of list having goals between 10 and 14 .	SELECT DISTINCT t1.nationality from list as t1 WHERE t1.goals BETWEEN 10 AND 14	2-10638934-1
show the years shared by more than 10 list .	SELECT t1.years from list as t1 GROUP BY t1.years HAVING COUNT ( * ) > 10	2-10638934-1
