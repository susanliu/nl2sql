show the record shared by more than 10 2006 .	SELECT t1.record from 2006 as t1 GROUP BY t1.record HAVING COUNT ( * ) > 10	2-18894744-7
what are the high rebounds of 2006 with game above the average game across all 2006 ?	SELECT t1.high_rebounds from 2006 as t1 WHERE t1.game > ( SELECT AVG ( t1.game ) from 2006 as t1	2-18894744-7
find the high assists of the 2006 with the highest game.	SELECT t1.high_assists from 2006 as t1 WHERE t1.game = ( SELECT MAX ( t1.game ) from 2006 as t1 )	2-18894744-7
how many 2006 are there in high points 10 or 93 ?	SELECT COUNT ( * ) from 2006 as t1 WHERE t1.high_points = 10 OR t1.high_points = 93	2-18894744-7
what are the record of all 2006 with opponent that is 10 ?	SELECT t1.record from 2006 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	2-18894744-7
find the opponent of 2006 who have both 10 and 52 game .	SELECT t1.opponent from 2006 as t1 WHERE t1.game = 10 INTERSECT SELECT t1.opponent from 2006 as t1 WHERE t1.game = 52	2-18894744-7
what are the date of 2006 , sorted by their frequency?	SELECT t1.date from 2006 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-18894744-7
please show the date of the 2006 that have at least 10 records .	SELECT t1.date from 2006 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-18894744-7
what are the high points of 2006 whose date are not 10 ?	SELECT t1.high_points from 2006 as t1 WHERE t1.date ! = 10	2-18894744-7
what are the high assists with exactly 10 2006 ?	SELECT t1.high_assists from 2006 as t1 GROUP BY t1.high_assists HAVING COUNT ( * ) = 10	2-18894744-7
