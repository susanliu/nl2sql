list golden rivers and golden rivers who have wins greater than 5 or against shorter than 10 .	SELECT t1.golden_rivers , t1.golden_rivers from golden as t1 WHERE t1.wins > 5 OR t1.against < 10	2-18036506-15
list the golden rivers and golden rivers of all golden sorted by draws in descending order .	SELECT t1.golden_rivers , t1.golden_rivers from golden as t1 ORDER BY t1.draws DESC	2-18036506-15
what are the maximum and minimum wins across all golden ?	SELECT MAX ( t1.wins ) , MIN ( t1.wins ) from golden as t1	2-18036506-15
what is the golden rivers and golden rivers of the golden with maximum byes ?	SELECT t1.golden_rivers , t1.golden_rivers from golden as t1 WHERE t1.byes = ( SELECT MAX ( t1.byes ) {FROM, 3} )	2-18036506-15
return the golden rivers and golden rivers of golden with the five lowest draws .	SELECT t1.golden_rivers , t1.golden_rivers from golden as t1 ORDER BY t1.draws LIMIT 5	2-18036506-15
find the golden rivers and golden rivers of the golden whose against is lower than the average against of all golden .	SELECT t1.golden_rivers , t1.golden_rivers from golden as t1 WHERE t1.against < ( SELECT AVG ( t1.against ) {FROM, 3} )	2-18036506-15
what are total draws for each golden rivers ?	SELECT t1.golden_rivers , SUM ( t1.draws ) from golden as t1 GROUP BY t1.golden_rivers	2-18036506-15
please show the different golden rivers , ordered by the number of golden that have each .	SELECT t1.golden_rivers from golden as t1 GROUP BY t1.golden_rivers ORDER BY COUNT ( * ) ASC LIMIT 1	2-18036506-15
how many golden are there that have more than {VALUE},0 golden rivers ?	SELECT COUNT ( * ) from golden as t1 GROUP BY t1.golden_rivers HAVING COUNT ( * ) > 10 	2-18036506-15
what are the maximum and minimum draws across all golden ?	SELECT MAX ( t1.draws ) , MIN ( t1.draws ) from golden as t1	2-18036506-15
