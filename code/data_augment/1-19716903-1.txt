which elevation is the most frequent elevation?	SELECT t1.elevation from list as t1 GROUP BY t1.elevation ORDER BY COUNT ( * ) DESC LIMIT 1	1-19716903-1
show the mountain range of list whose elevation are not 10.	SELECT t1.mountain_range from list as t1 WHERE t1.elevation ! = 10	1-19716903-1
which mountain peak is the most frequent mountain peak?	SELECT t1.mountain_peak from list as t1 GROUP BY t1.mountain_peak ORDER BY COUNT ( * ) DESC LIMIT 1	1-19716903-1
what are the prominence of list whose mountain peak are not 10 ?	SELECT t1.prominence from list as t1 WHERE t1.mountain_peak ! = 10	1-19716903-1
return the mountain range of the list that has the fewest corresponding mountain range .	SELECT t1.mountain_range from list as t1 GROUP BY t1.mountain_range ORDER BY COUNT ( * ) ASC LIMIT 1	1-19716903-1
what are the distinct COLUMN_NAME,0} of every list that has a greater rank than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.mountain_peak from list as t1 WHERE t1.rank > ( SELECT MIN ( t1.mountain_peak ) from list as t1 WHERE t1.prominence = 10 )	1-19716903-1
what is the count of list with more than 10 mountain peak ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.mountain_peak HAVING COUNT ( * ) > 10 	1-19716903-1
find the prominence of list which are prominence 10 but not prominence 2 .	SELECT t1.prominence from list as t1 WHERE t1.prominence = 10 EXCEPT SELECT t1.prominence from list as t1 WHERE t1.prominence = 2	1-19716903-1
what are the mountain peak and isolation of list with 10 or more elevation ?	SELECT t1.mountain_peak , t1.isolation from list as t1 GROUP BY t1.elevation HAVING COUNT ( * ) >= 10	1-19716903-1
give the maximum and minimum rank of all list .	SELECT MAX ( t1.rank ) , MIN ( t1.rank ) from list as t1	1-19716903-1
