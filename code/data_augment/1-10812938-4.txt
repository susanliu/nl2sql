find the distinct position of 2006 having pick # between 10 and 84 .	SELECT DISTINCT t1.position from 2006 as t1 WHERE t1.pick_ BETWEEN 10 AND 84	1-10812938-4
which player have greater pick # than that of any pick # in 2006 ?	SELECT t1.player from 2006 as t1 WHERE t1.pick_ > ( SELECT MIN ( t1.pick_ ) from 2006 as t1 )	1-10812938-4
which player has the least pick # ?	SELECT t1.player from 2006 as t1 ORDER BY t1.pick_ ASC LIMIT 1	1-10812938-4
return the different position of 2006 , in ascending order of frequency .	SELECT t1.position from 2006 as t1 GROUP BY t1.position ORDER BY COUNT ( * ) ASC LIMIT 1	1-10812938-4
show the player and the total pick # of 2006 .	SELECT t1.player , SUM ( t1.pick_ ) from 2006 as t1 GROUP BY t1.player	1-10812938-4
find the number of 2006 that have more than 10 player .	SELECT COUNT ( * ) from 2006 as t1 GROUP BY t1.player HAVING COUNT ( * ) > 10 	1-10812938-4
list college and cfl team who have pick # greater than 5 or pick # shorter than 10 .	SELECT t1.college , t1.cfl_team from 2006 as t1 WHERE t1.pick_ > 5 OR t1.pick_ < 10	1-10812938-4
please list the player and cfl team of 2006 in descending order of pick # .	SELECT t1.player , t1.cfl_team from 2006 as t1 ORDER BY t1.pick_ DESC	1-10812938-4
show all cfl team and the total pick # for each .	SELECT t1.cfl_team , SUM ( t1.pick_ ) from 2006 as t1 GROUP BY t1.cfl_team	1-10812938-4
what are the distinct COLUMN_NAME,0} of every 2006 that has a greater pick # than some 2006 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.player from 2006 as t1 WHERE t1.pick_ > ( SELECT MIN ( t1.player ) from 2006 as t1 WHERE t1.player = 10 )	1-10812938-4
