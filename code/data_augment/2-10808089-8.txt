what are the date of all 1966 that have 10 or more 1966 ?	SELECT t1.date from 1966 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-10808089-8
how many distinct home team correspond to each away team ?	SELECT t1.away_team , COUNT ( DISTINCT t1.home_team ) from 1966 as t1 GROUP BY t1.away_team	2-10808089-8
what are the date of the 1966 with date other than 10 ?	SELECT t1.date from 1966 as t1 WHERE t1.date ! = 10	2-10808089-8
Return all columns in 1966 .	SELECT * FROM 1966	2-10808089-8
show the venue shared by more than 10 1966 .	SELECT t1.venue from 1966 as t1 GROUP BY t1.venue HAVING COUNT ( * ) > 10	2-10808089-8
what are the home team score and venue of 1966 with 10 or more home team score ?	SELECT t1.home_team_score , t1.venue from 1966 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) >= 10	2-10808089-8
what are the home team score and date of each 1966 , listed in descending order by crowd ?	SELECT t1.home_team_score , t1.date from 1966 as t1 ORDER BY t1.crowd DESC	2-10808089-8
what are the date that have greater crowd than any crowd in 1966 ?	SELECT t1.date from 1966 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1966 as t1 )	2-10808089-8
please show the home team of the 1966 that have at least 10 records .	SELECT t1.home_team from 1966 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10808089-8
what are the venue and away team of all 1966 sorted by decreasing crowd ?	SELECT t1.venue , t1.away_team from 1966 as t1 ORDER BY t1.crowd DESC	2-10808089-8
