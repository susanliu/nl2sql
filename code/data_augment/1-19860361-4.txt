please show the different hokey wolf , ordered by the number of list that have each .	SELECT t1.hokey_wolf from list as t1 GROUP BY t1.hokey_wolf ORDER BY COUNT ( * ) ASC LIMIT 1	1-19860361-4
find the number of list that have more than 10 air date .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.air_date HAVING COUNT ( * ) > 10 	1-19860361-4
show the credited animators with fewer than 3 list .	SELECT t1.credited_animators from list as t1 GROUP BY t1.credited_animators HAVING COUNT ( * ) < 3	1-19860361-4
how many list are there in pixie and dixie 10 or 38 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.pixie_and_dixie = 10 OR t1.pixie_and_dixie = 38	1-19860361-4
which t1.air_date has least number of list ?	SELECT t1.air_date from list as t1 GROUP BY t1.air_date ORDER BY COUNT ( * ) LIMIT 1	1-19860361-4
what are the hokey wolf that have greater ep than any ep in list ?	SELECT t1.hokey_wolf from list as t1 WHERE t1.ep > ( SELECT MIN ( t1.ep ) from list as t1 )	1-19860361-4
what is the air date and huckleberry hound for the list with the rank 5 smallest ep ?	SELECT t1.air_date , t1.huckleberry_hound from list as t1 ORDER BY t1.ep LIMIT 5	1-19860361-4
how many distinct hokey wolf correspond to each hokey wolf ?	SELECT t1.hokey_wolf , COUNT ( DISTINCT t1.hokey_wolf ) from list as t1 GROUP BY t1.hokey_wolf	1-19860361-4
what are the hokey wolf and pixie and dixie of all list sorted by decreasing ep ?	SELECT t1.hokey_wolf , t1.pixie_and_dixie from list as t1 ORDER BY t1.ep DESC	1-19860361-4
what is all the information on the list with the largest number of credited animators ?	SELECT * from list as t1 ORDER BY t1.credited_animators DESC LIMIT 1	1-19860361-4
