what is all the information on the 1986 with the largest number of location ?	SELECT * from 1986 as t1 ORDER BY t1.location DESC LIMIT 1	1-11622771-1
how many 1986 are there in winner 10 or 16 ?	SELECT COUNT ( * ) from 1986 as t1 WHERE t1.winner = 10 OR t1.winner = 16	1-11622771-1
what are the location , tournament , and winner of each 1986 ?	SELECT t1.location , t1.tournament , t1.winner from 1986 as t1	1-11622771-1
what are the average 1st prize ( $ ) of 1986 , grouped by score ?	SELECT AVG ( t1.1st_prize_ ) , t1.score from 1986 as t1 GROUP BY t1.score	1-11622771-1
list the location which average 1st prize ( $ ) is above 10 .	SELECT t1.location from 1986 as t1 GROUP BY t1.location HAVING AVG ( t1.1st_prize_ ) >= 10	1-11622771-1
what are the average 1st prize ( $ ) of 1986 for different winner ?	SELECT AVG ( t1.1st_prize_ ) , t1.winner from 1986 as t1 GROUP BY t1.winner	1-11622771-1
what are the distinct COLUMN_NAME,0} of 1986 with 1st prize ( $ ) higher than any 1986 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.tournament from 1986 as t1 WHERE t1.1st_prize_ > ( SELECT MIN ( t1.tournament ) from 1986 as t1 WHERE t1.tournament = 10 )	1-11622771-1
count the number of 1986 in date 10 or 73 .	SELECT COUNT ( * ) from 1986 as t1 WHERE t1.date = 10 OR t1.date = 73	1-11622771-1
show date and tournament of 1986 .	SELECT t1.date , t1.tournament from 1986 as t1	1-11622771-1
what is the score of the 1986 with the minimum 1st prize ( $ ) ?	SELECT t1.score from 1986 as t1 ORDER BY t1.1st_prize_ ASC LIMIT 1	1-11622771-1
