list all information about 1981 .	SELECT * FROM 1981	2-10823950-19
which home team has both 1981 with less than 10 crowd and 1981 with more than 12 crowd ?	SELECT t1.home_team from 1981 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1981 as t1 WHERE t1.crowd > 12	2-10823950-19
show all information on the 1981 that has the largest number of date.	SELECT * from 1981 as t1 ORDER BY t1.date DESC LIMIT 1	2-10823950-19
show the date of 1981 whose away team are not 10.	SELECT t1.date from 1981 as t1 WHERE t1.away_team ! = 10	2-10823950-19
how many different date correspond to each away team ?	SELECT t1.away_team , COUNT ( DISTINCT t1.date ) from 1981 as t1 GROUP BY t1.away_team	2-10823950-19
find the date of 1981 which have both 10 and 51 as date .	SELECT t1.date from 1981 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.date from 1981 as t1 WHERE t1.crowd = 51	2-10823950-19
what is the home team of the 1981 who has the highest number of 1981 ?	SELECT t1.home_team from 1981 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10823950-19
what is the away team score and home team for the 1981 with the rank 5 smallest crowd ?	SELECT t1.away_team_score , t1.home_team from 1981 as t1 ORDER BY t1.crowd LIMIT 5	2-10823950-19
show the home team and away team with at least 10 date .	SELECT t1.home_team , t1.away_team from 1981 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-10823950-19
what is the venue and date of the 1981 with the top 5 smallest crowd ?	SELECT t1.venue , t1.date from 1981 as t1 ORDER BY t1.crowd LIMIT 5	2-10823950-19
