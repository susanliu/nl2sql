list the director of the none whose u.s. viewers ( million ) is not 10 .	SELECT t1.director from none as t1 WHERE t1.u.s._viewers_ ! = 10	1-2602958-5
what are the distinct writer ( s ) with no . between 10 and 19 ?	SELECT DISTINCT t1.writer_ from none as t1 WHERE t1.no_ BETWEEN 10 AND 19	1-2602958-5
show the director and director with at least 10 original air date .	SELECT t1.director , t1.director from none as t1 GROUP BY t1.original_air_date HAVING COUNT ( * ) >= 10	1-2602958-5
find the original air date and title of the none whose no . is lower than the average no . of all none .	SELECT t1.original_air_date , t1.title from none as t1 WHERE t1.no_ < ( SELECT AVG ( t1.no_ ) {FROM, 3} )	1-2602958-5
what are the writer ( s ) and original air date of all none sorted by decreasing prod . code ?	SELECT t1.writer_ , t1.original_air_date from none as t1 ORDER BY t1.prod_._code DESC	1-2602958-5
what is the title of the none with the smallest prod . code ?	SELECT t1.title from none as t1 ORDER BY t1.prod_._code ASC LIMIT 1	1-2602958-5
what is the original air date and writer ( s ) of the none with maximum prod . code ?	SELECT t1.original_air_date , t1.writer_ from none as t1 WHERE t1.prod_._code = ( SELECT MAX ( t1.prod_._code ) {FROM, 3} )	1-2602958-5
find the original air date and director of the none whose # is lower than the average # of all none .	SELECT t1.original_air_date , t1.director from none as t1 WHERE t1.# < ( SELECT AVG ( t1.# ) {FROM, 3} )	1-2602958-5
list director and title who have no . greater than 5 or prod . code shorter than 10 .	SELECT t1.director , t1.title from none as t1 WHERE t1.no_ > 5 OR t1.prod_._code < 10	1-2602958-5
what is all the information on the none with the largest number of u.s. viewers ( million ) ?	SELECT * from none as t1 ORDER BY t1.u.s._viewers_ DESC LIMIT 1	1-2602958-5
