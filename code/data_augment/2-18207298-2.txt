what is the result of the 1978 with the minimum attendance ?	SELECT t1.result from 1978 as t1 ORDER BY t1.attendance ASC LIMIT 1	2-18207298-2
show the opponent of 1978 whose result are not 10.	SELECT t1.opponent from 1978 as t1 WHERE t1.result ! = 10	2-18207298-2
what is the opponent and date of the 1978 with maximum attendance ?	SELECT t1.opponent , t1.date from 1978 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) {FROM, 3} )	2-18207298-2
return the different date of 1978 , in ascending order of frequency .	SELECT t1.date from 1978 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-18207298-2
what are the result and opponent of the {COLUMN} who have attendance above five or week below ten ?	SELECT t1.result , t1.opponent from 1978 as t1 WHERE t1.attendance > 5 OR t1.week < 10	2-18207298-2
how many different date correspond to each opponent ?	SELECT t1.opponent , COUNT ( DISTINCT t1.date ) from 1978 as t1 GROUP BY t1.opponent	2-18207298-2
what are all the date and opponent?	SELECT t1.date , t1.opponent from 1978 as t1	2-18207298-2
how many 1978 are there that have more than {VALUE},0 opponent ?	SELECT COUNT ( * ) from 1978 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	2-18207298-2
show the result and the number of unique result containing each result .	SELECT t1.result , COUNT ( DISTINCT t1.result ) from 1978 as t1 GROUP BY t1.result	2-18207298-2
what are the date of 1978 with week above the average week across all 1978 ?	SELECT t1.date from 1978 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 1978 as t1	2-18207298-2
