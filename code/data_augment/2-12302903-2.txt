show opponent and method of kazunari .	SELECT t1.opponent , t1.method from kazunari as t1	2-12302903-2
find the time of the kazunari with the largest round .	SELECT t1.time from kazunari as t1 ORDER BY t1.round DESC LIMIT 1	2-12302903-2
find the record of kazunari which have both 10 and 34 as record .	SELECT t1.record from kazunari as t1 WHERE t1.round = 10 INTERSECT SELECT t1.record from kazunari as t1 WHERE t1.round = 34	2-12302903-2
what is the res . of kazunari with the maximum round across all kazunari ?	SELECT t1.res_ from kazunari as t1 WHERE t1.round = ( SELECT MAX ( t1.round ) from kazunari as t1 )	2-12302903-2
list all information about kazunari .	SELECT * FROM kazunari	2-12302903-2
what are the event of all kazunari that have 10 or more kazunari ?	SELECT t1.event from kazunari as t1 GROUP BY t1.event HAVING COUNT ( * ) >= 10	2-12302903-2
return each method with the number of kazunari in ascending order of the number of method .	SELECT t1.method , COUNT ( * ) from kazunari as t1 GROUP BY t1.method ORDER BY COUNT ( * )	2-12302903-2
which opponent have an average round over 10 ?	SELECT t1.opponent from kazunari as t1 GROUP BY t1.opponent HAVING AVG ( t1.round ) >= 10	2-12302903-2
which opponent have greater round than that of any round in kazunari ?	SELECT t1.opponent from kazunari as t1 WHERE t1.round > ( SELECT MIN ( t1.round ) from kazunari as t1 )	2-12302903-2
find the event of the kazunari that is most frequent across all event .	SELECT t1.event from kazunari as t1 GROUP BY t1.event ORDER BY COUNT ( * ) DESC LIMIT 1	2-12302903-2
