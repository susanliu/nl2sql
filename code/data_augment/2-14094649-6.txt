which third quarter have less than 3 in list ?	SELECT t1.third_quarter from list as t1 GROUP BY t1.third_quarter HAVING COUNT ( * ) < 3	2-14094649-6
find the third quarter of the list with the largest rank .	SELECT t1.third_quarter from list as t1 ORDER BY t1.rank DESC LIMIT 1	2-14094649-6
what are the third quarter of all list with fourth quarter that is 10 ?	SELECT t1.third_quarter from list as t1 GROUP BY t1.fourth_quarter HAVING COUNT ( * ) = 10	2-14094649-6
find the fourth quarter of the list who has the largest number of list .	SELECT t1.fourth_quarter from list as t1 GROUP BY t1.fourth_quarter ORDER BY COUNT ( * ) DESC LIMIT 1	2-14094649-6
return the fourth quarter of the list that has the fewest corresponding fourth quarter .	SELECT t1.fourth_quarter from list as t1 GROUP BY t1.fourth_quarter ORDER BY COUNT ( * ) ASC LIMIT 1	2-14094649-6
what are the distinct COLUMN_NAME,0} of list with rank higher than any list from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.fourth_quarter from list as t1 WHERE t1.rank > ( SELECT MIN ( t1.fourth_quarter ) from list as t1 WHERE t1.third_quarter = 10 )	2-14094649-6
what is the minimum rank in each first quarter ?	SELECT MIN ( t1.rank ) , t1.first_quarter from list as t1 GROUP BY t1.first_quarter	2-14094649-6
what are the distinct fourth quarter with rank between 10 and 87 ?	SELECT DISTINCT t1.fourth_quarter from list as t1 WHERE t1.rank BETWEEN 10 AND 87	2-14094649-6
please show the different first quarter , ordered by the number of list that have each .	SELECT t1.first_quarter from list as t1 GROUP BY t1.first_quarter ORDER BY COUNT ( * ) ASC LIMIT 1	2-14094649-6
find the fourth quarter of the list which have third quarter 10 but not 9 .	SELECT t1.fourth_quarter from list as t1 WHERE t1.third_quarter = 10 EXCEPT SELECT t1.fourth_quarter from list as t1 WHERE t1.third_quarter = 9	2-14094649-6
