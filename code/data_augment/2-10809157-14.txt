what is the home team score and away team score of the 1969 with maximum crowd ?	SELECT t1.home_team_score , t1.away_team_score from 1969 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809157-14
what are the date and home team score of 1969 with 10 or more away team ?	SELECT t1.date , t1.home_team_score from 1969 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) >= 10	2-10809157-14
what are the away team of the 1969 with away team other than 10 ?	SELECT t1.away_team from 1969 as t1 WHERE t1.venue ! = 10	2-10809157-14
which away team has both 1969 with less than 10 crowd and 1969 with more than 97 crowd ?	SELECT t1.away_team from 1969 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.away_team from 1969 as t1 WHERE t1.crowd > 97	2-10809157-14
how many 1969 are there that have more than {VALUE},0 date ?	SELECT COUNT ( * ) from 1969 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10 	2-10809157-14
which date has both 1969 with less than 10 crowd and 1969 with more than 58 crowd ?	SELECT t1.date from 1969 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.date from 1969 as t1 WHERE t1.crowd > 58	2-10809157-14
what are the away team score of the 1969 that have exactly 10 1969 ?	SELECT t1.away_team_score from 1969 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) = 10	2-10809157-14
show the away team with fewer than 3 1969 .	SELECT t1.away_team from 1969 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) < 3	2-10809157-14
find the distinct home team of 1969 having crowd between 10 and 56 .	SELECT DISTINCT t1.home_team from 1969 as t1 WHERE t1.crowd BETWEEN 10 AND 56	2-10809157-14
what is the venue and date for the 1969 with the rank 5 smallest crowd ?	SELECT t1.venue , t1.date from 1969 as t1 ORDER BY t1.crowd LIMIT 5	2-10809157-14
