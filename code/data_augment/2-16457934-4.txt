return the oblast \ age of tfr that do not have the oblast \ age 10 .	SELECT t1.oblast_\_age from tfr as t1 WHERE t1.oblast_\_age ! = 10	2-16457934-4
find the distinct oblast \ age of tfr having 55 to 59 between 10 and 74 .	SELECT DISTINCT t1.oblast_\_age from tfr as t1 WHERE t1.55_to_59 BETWEEN 10 AND 74	2-16457934-4
what are the oblast \ age and oblast \ age of tfr with 10 or more oblast \ age ?	SELECT t1.oblast_\_age , t1.oblast_\_age from tfr as t1 GROUP BY t1.oblast_\_age HAVING COUNT ( * ) >= 10	2-16457934-4
find the distinct oblast \ age of tfr having 15 to 17 between 10 and 40 .	SELECT DISTINCT t1.oblast_\_age from tfr as t1 WHERE t1.15_to_17 BETWEEN 10 AND 40	2-16457934-4
show oblast \ age and the number of distinct oblast \ age in each oblast \ age .	SELECT t1.oblast_\_age , COUNT ( DISTINCT t1.oblast_\_age ) from tfr as t1 GROUP BY t1.oblast_\_age	2-16457934-4
find the oblast \ age of the tfr which have oblast \ age 10 but not 71 .	SELECT t1.oblast_\_age from tfr as t1 WHERE t1.oblast_\_age = 10 EXCEPT SELECT t1.oblast_\_age from tfr as t1 WHERE t1.oblast_\_age = 71	2-16457934-4
what are the distinct oblast \ age with 60 to 64 between 10 and 11 ?	SELECT DISTINCT t1.oblast_\_age from tfr as t1 WHERE t1.60_to_64 BETWEEN 10 AND 11	2-16457934-4
what are the average 65 to 69 of tfr , grouped by oblast \ age ?	SELECT AVG ( t1.65_to_69 ) , t1.oblast_\_age from tfr as t1 GROUP BY t1.oblast_\_age	2-16457934-4
find the oblast \ age for all tfr who have more than the average 30 to 34 .	SELECT t1.oblast_\_age from tfr as t1 WHERE t1.30_to_34 > ( SELECT AVG ( t1.30_to_34 ) from tfr as t1	2-16457934-4
what are the oblast \ age of all tfr that have 10 or more tfr ?	SELECT t1.oblast_\_age from tfr as t1 GROUP BY t1.oblast_\_age HAVING COUNT ( * ) >= 10	2-16457934-4
