find the player of the 1967 with the largest money ( $ ) .	SELECT t1.player from 1967 as t1 ORDER BY t1.money_ DESC LIMIT 1	2-17277114-3
find the score and score of the 1967 with at least 10 to par .	SELECT t1.score , t1.score from 1967 as t1 GROUP BY t1.to_par HAVING COUNT ( * ) >= 10	2-17277114-3
which country have less than 3 in 1967 ?	SELECT t1.country from 1967 as t1 GROUP BY t1.country HAVING COUNT ( * ) < 3	2-17277114-3
show all information on the 1967 that has the largest number of player.	SELECT * from 1967 as t1 ORDER BY t1.player DESC LIMIT 1	2-17277114-3
what are the to par and country of the {COLUMN} who have money ( $ ) above five or money ( $ ) below ten ?	SELECT t1.to_par , t1.country from 1967 as t1 WHERE t1.money_ > 5 OR t1.money_ < 10	2-17277114-3
list player and player who have money ( $ ) greater than 5 or money ( $ ) shorter than 10 .	SELECT t1.player , t1.player from 1967 as t1 WHERE t1.money_ > 5 OR t1.money_ < 10	2-17277114-3
what is the t1.player of 1967 that has fewest number of 1967 ?	SELECT t1.player from 1967 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) LIMIT 1	2-17277114-3
please show the score of the 1967 that have at least 10 records .	SELECT t1.score from 1967 as t1 GROUP BY t1.score HAVING COUNT ( * ) >= 10	2-17277114-3
return the score of the 1967 that has the fewest corresponding score .	SELECT t1.score from 1967 as t1 GROUP BY t1.score ORDER BY COUNT ( * ) ASC LIMIT 1	2-17277114-3
what are the average money ( $ ) of 1967 for different score ?	SELECT AVG ( t1.money_ ) , t1.score from 1967 as t1 GROUP BY t1.score	2-17277114-3
