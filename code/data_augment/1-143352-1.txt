what are the distinct agr power station with accounting closure date between 10 and 28 ?	SELECT DISTINCT t1.agr_power_station from advanced as t1 WHERE t1.accounting_closure_date BETWEEN 10 AND 28	1-143352-1
list the agr power station which average commercial operation is above 10 .	SELECT t1.agr_power_station from advanced as t1 GROUP BY t1.agr_power_station HAVING AVG ( t1.commercial_operation ) >= 10	1-143352-1
find the agr power station of advanced which have both 10 and 23 as agr power station .	SELECT t1.agr_power_station from advanced as t1 WHERE t1.net_mwe = 10 INTERSECT SELECT t1.agr_power_station from advanced as t1 WHERE t1.net_mwe = 23	1-143352-1
which agr power station have an average accounting closure date over 10 ?	SELECT t1.agr_power_station from advanced as t1 GROUP BY t1.agr_power_station HAVING AVG ( t1.accounting_closure_date ) >= 10	1-143352-1
what are the average accounting closure date of advanced , grouped by agr power station ?	SELECT AVG ( t1.accounting_closure_date ) , t1.agr_power_station from advanced as t1 GROUP BY t1.agr_power_station	1-143352-1
find the number of advanced that have more than 10 agr power station .	SELECT COUNT ( * ) from advanced as t1 GROUP BY t1.agr_power_station HAVING COUNT ( * ) > 10 	1-143352-1
what are the maximum and minimum construction started across all advanced ?	SELECT MAX ( t1.construction_started ) , MIN ( t1.construction_started ) from advanced as t1	1-143352-1
how many advanced are there that have more than {VALUE},0 agr power station ?	SELECT COUNT ( * ) from advanced as t1 GROUP BY t1.agr_power_station HAVING COUNT ( * ) > 10 	1-143352-1
what is the count of advanced with more than 10 agr power station ?	SELECT COUNT ( * ) from advanced as t1 GROUP BY t1.agr_power_station HAVING COUNT ( * ) > 10 	1-143352-1
what are the agr power station and agr power station of each advanced ?	SELECT t1.agr_power_station , t1.agr_power_station from advanced as t1	1-143352-1
