find the nation and nation of the volleyball whose bronze is lower than the average bronze of all volleyball .	SELECT t1.nation , t1.nation from volleyball as t1 WHERE t1.bronze < ( SELECT AVG ( t1.bronze ) {FROM, 3} )	2-1613392-2
what are the average gold of volleyball for different nation ?	SELECT AVG ( t1.gold ) , t1.nation from volleyball as t1 GROUP BY t1.nation	2-1613392-2
show all information on the volleyball that has the largest number of nation.	SELECT * from volleyball as t1 ORDER BY t1.nation DESC LIMIT 1	2-1613392-2
return the nation of the volleyball that has the fewest corresponding nation .	SELECT t1.nation from volleyball as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) ASC LIMIT 1	2-1613392-2
select the average rank of each volleyball 's nation .	SELECT AVG ( t1.rank ) , t1.nation from volleyball as t1 GROUP BY t1.nation	2-1613392-2
what are the nation and nation of the {COLUMN} who have gold above five or gold below ten ?	SELECT t1.nation , t1.nation from volleyball as t1 WHERE t1.gold > 5 OR t1.gold < 10	2-1613392-2
return the different nation of volleyball , in ascending order of frequency .	SELECT t1.nation from volleyball as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) ASC LIMIT 1	2-1613392-2
list the nation , nation and the nation of the volleyball .	SELECT t1.nation , t1.nation , t1.nation from volleyball as t1	2-1613392-2
how many volleyball are there in nation 10 or 57 ?	SELECT COUNT ( * ) from volleyball as t1 WHERE t1.nation = 10 OR t1.nation = 57	2-1613392-2
find the nation and nation of the volleyball whose rank is lower than the average rank of all volleyball .	SELECT t1.nation , t1.nation from volleyball as t1 WHERE t1.rank < ( SELECT AVG ( t1.rank ) {FROM, 3} )	2-1613392-2
