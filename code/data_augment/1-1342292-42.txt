what are the party of united , sorted by their frequency?	SELECT t1.party from united as t1 GROUP BY t1.party ORDER BY COUNT ( * ) ASC LIMIT 1	1-1342292-42
what is the t1.district of united that has fewest number of united ?	SELECT t1.district from united as t1 GROUP BY t1.district ORDER BY COUNT ( * ) LIMIT 1	1-1342292-42
list party of united that have the number of united greater than 10 .	SELECT t1.party from united as t1 GROUP BY t1.party HAVING COUNT ( * ) > 10	1-1342292-42
find the incumbent of united who have first elected of both 10 and 50 .	SELECT t1.incumbent from united as t1 WHERE t1.first_elected = 10 INTERSECT SELECT t1.incumbent from united as t1 WHERE t1.first_elected = 50	1-1342292-42
find the candidates of the united with the largest first elected .	SELECT t1.candidates from united as t1 ORDER BY t1.first_elected DESC LIMIT 1	1-1342292-42
what are the incumbent and candidates of united with 10 or more candidates ?	SELECT t1.incumbent , t1.candidates from united as t1 GROUP BY t1.candidates HAVING COUNT ( * ) >= 10	1-1342292-42
show all district and corresponding number of united sorted by the count .	SELECT t1.district , COUNT ( * ) from united as t1 GROUP BY t1.district ORDER BY COUNT ( * )	1-1342292-42
find the district of united who have first elected of both 10 and 82 .	SELECT t1.district from united as t1 WHERE t1.first_elected = 10 INTERSECT SELECT t1.district from united as t1 WHERE t1.first_elected = 82	1-1342292-42
which candidates has both united with less than 10 first elected and united with more than 23 first elected ?	SELECT t1.candidates from united as t1 WHERE t1.first_elected < 10 INTERSECT SELECT t1.candidates from united as t1 WHERE t1.first_elected > 23	1-1342292-42
return the incumbent of the united with the fewest first elected .	SELECT t1.incumbent from united as t1 ORDER BY t1.first_elected ASC LIMIT 1	1-1342292-42
