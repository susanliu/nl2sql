what is the home team of the 1943 with the minimum crowd ?	SELECT t1.home_team from 1943 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10808346-8
which home team have less than 3 in 1943 ?	SELECT t1.home_team from 1943 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) < 3	2-10808346-8
return the away team score of the largest crowd.	SELECT t1.away_team_score from 1943 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10808346-8
what are the maximum and minimum crowd across all 1943 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1943 as t1	2-10808346-8
what is the away team and venue of every 1943 that has a crowd lower than average ?	SELECT t1.away_team , t1.venue from 1943 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10808346-8
count the number of 1943 in date 10 or 35 .	SELECT COUNT ( * ) from 1943 as t1 WHERE t1.date = 10 OR t1.date = 35	2-10808346-8
list the home team of {1943 which has number of 1943 greater than 10 .	SELECT t1.home_team from 1943 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) > 10	2-10808346-8
what is the maximum and mininum crowd {COLUMN} for all 1943 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1943 as t1	2-10808346-8
which home team has both 1943 with less than 10 crowd and 1943 with more than 44 crowd ?	SELECT t1.home_team from 1943 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1943 as t1 WHERE t1.crowd > 44	2-10808346-8
what are the home team of all 1943 with date that is 10 ?	SELECT t1.home_team from 1943 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10808346-8
