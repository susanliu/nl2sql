return the date of the 1999 with the fewest race .	SELECT t1.date from 1999 as t1 ORDER BY t1.race ASC LIMIT 1	2-16640814-2
what are the city / state , team , and city / state of each 1999 ?	SELECT t1.city_/_state , t1.team , t1.winner from 1999 as t1	2-16640814-2
find the distinct COLUMN_NAME,0} of all 1999 that have race higher than some 1999 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.city_/_state from 1999 as t1 WHERE t1.race > ( SELECT MIN ( t1.city_/_state ) from 1999 as t1 WHERE t1.date = 10 )	2-16640814-2
what is the winner of all 1999 whose race is higher than any 1999 ?	SELECT t1.winner from 1999 as t1 WHERE t1.race > ( SELECT MIN ( t1.race ) from 1999 as t1 )	2-16640814-2
find the team who has exactly 10 1999 .	SELECT t1.team from 1999 as t1 GROUP BY t1.team HAVING COUNT ( * ) = 10	2-16640814-2
return the smallest race for every race title .	SELECT MIN ( t1.race ) , t1.race_title from 1999 as t1 GROUP BY t1.race_title	2-16640814-2
which date has both 1999 with less than 10 race and 1999 with more than 37 race ?	SELECT t1.date from 1999 as t1 WHERE t1.race < 10 INTERSECT SELECT t1.date from 1999 as t1 WHERE t1.race > 37	2-16640814-2
what are the circuit that have greater race than any race in 1999 ?	SELECT t1.circuit from 1999 as t1 WHERE t1.race > ( SELECT MIN ( t1.race ) from 1999 as t1 )	2-16640814-2
what is the circuit of 1999 with the maximum race across all 1999 ?	SELECT t1.circuit from 1999 as t1 WHERE t1.race = ( SELECT MAX ( t1.race ) from 1999 as t1 )	2-16640814-2
what are the circuit , race title , and city / state for each 1999 ?	SELECT t1.circuit , t1.race_title , t1.city_/_state from 1999 as t1	2-16640814-2
