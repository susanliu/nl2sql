find the operator of port who have both 10 and 38 quay cranes .	SELECT t1.operator from port as t1 WHERE t1.quay_cranes = 10 INTERSECT SELECT t1.operator from port as t1 WHERE t1.quay_cranes = 38	2-1230098-1
what is the operator and terminal of every port that has a berths lower than average ?	SELECT t1.operator , t1.terminal from port as t1 WHERE t1.berths < ( SELECT AVG ( t1.berths ) {FROM, 3} )	2-1230098-1
what is the operator and depth ( m ) of the port with maximum berths ?	SELECT t1.operator , t1.depth_ from port as t1 WHERE t1.berths = ( SELECT MAX ( t1.berths ) {FROM, 3} )	2-1230098-1
what is the operator of the port who has the highest number of port ?	SELECT t1.operator from port as t1 GROUP BY t1.operator ORDER BY COUNT ( * ) DESC LIMIT 1	2-1230098-1
return the different terminal of port , in ascending order of frequency .	SELECT t1.terminal from port as t1 GROUP BY t1.terminal ORDER BY COUNT ( * ) ASC LIMIT 1	2-1230098-1
what are the distinct COLUMN_NAME,0} of port with quay cranes higher than any port from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.depth_ from port as t1 WHERE t1.quay_cranes > ( SELECT MIN ( t1.depth_ ) from port as t1 WHERE t1.terminal = 10 )	2-1230098-1
how many port have operator that contains 10 ?	SELECT COUNT ( * ) from port as t1 WHERE t1.operator LIKE 10	2-1230098-1
find the depth ( m ) of port who have more than 10 port .	SELECT t1.depth_ from port as t1 GROUP BY t1.depth_ HAVING COUNT ( * ) > 10	2-1230098-1
what are the terminal of the port that have exactly 10 port ?	SELECT t1.terminal from port as t1 GROUP BY t1.terminal HAVING COUNT ( * ) = 10	2-1230098-1
find the distinct depth ( m ) of port having quay cranes between 10 and 67 .	SELECT DISTINCT t1.depth_ from port as t1 WHERE t1.quay_cranes BETWEEN 10 AND 67	2-1230098-1
