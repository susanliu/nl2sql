which official name have greater area km 2 than that of any area km 2 in albert ?	SELECT t1.official_name from albert as t1 WHERE t1.area_km_2 > ( SELECT MIN ( t1.area_km_2 ) from albert as t1 )	2-170958-2
find the number of albert that have more than 10 official name .	SELECT COUNT ( * ) from albert as t1 GROUP BY t1.official_name HAVING COUNT ( * ) > 10 	2-170958-2
what are the average area km 2 of albert , grouped by official name ?	SELECT AVG ( t1.area_km_2 ) , t1.official_name from albert as t1 GROUP BY t1.official_name	2-170958-2
what is the maximum and mininum area km 2 {COLUMN} for all albert ?	SELECT MAX ( t1.area_km_2 ) , MIN ( t1.area_km_2 ) from albert as t1	2-170958-2
find the distinct census ranking of all albert that have a higher area km 2 than some albert with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.census_ranking from albert as t1 WHERE t1.area_km_2 > ( SELECT MIN ( t1.census_ranking ) from albert as t1 WHERE t1.status = 10 )	2-170958-2
what is the status and status of every albert that has a area km 2 lower than average ?	SELECT t1.status , t1.status from albert as t1 WHERE t1.area_km_2 < ( SELECT AVG ( t1.area_km_2 ) {FROM, 3} )	2-170958-2
select the average population of each albert 's census ranking .	SELECT AVG ( t1.population ) , t1.census_ranking from albert as t1 GROUP BY t1.census_ranking	2-170958-2
return the census ranking and census ranking of albert with the five lowest area km 2 .	SELECT t1.census_ranking , t1.census_ranking from albert as t1 ORDER BY t1.area_km_2 LIMIT 5	2-170958-2
find the census ranking of albert who have population of both 10 and 96 .	SELECT t1.census_ranking from albert as t1 WHERE t1.population = 10 INTERSECT SELECT t1.census_ranking from albert as t1 WHERE t1.population = 96	2-170958-2
which census ranking has most number of albert ?	SELECT t1.census_ranking from albert as t1 GROUP BY t1.census_ranking ORDER BY COUNT ( * ) DESC LIMIT 1	2-170958-2
