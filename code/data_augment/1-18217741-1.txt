give the original air date that has the most none .	SELECT t1.original_air_date from none as t1 GROUP BY t1.original_air_date ORDER BY COUNT ( * ) DESC LIMIT 1	1-18217741-1
return each u.s. viewers ( millions ) with the number of none in ascending order of the number of u.s. viewers ( millions ) .	SELECT t1.u.s._viewers_ , COUNT ( * ) from none as t1 GROUP BY t1.u.s._viewers_ ORDER BY COUNT ( * )	1-18217741-1
how many distinct u.s. viewers ( millions ) correspond to each written by ?	SELECT t1.written_by , COUNT ( DISTINCT t1.u.s._viewers_ ) from none as t1 GROUP BY t1.written_by	1-18217741-1
count the number of none in u.s. viewers ( millions ) 10 or 50 .	SELECT COUNT ( * ) from none as t1 WHERE t1.u.s._viewers_ = 10 OR t1.u.s._viewers_ = 50	1-18217741-1
what is the directed by and title of the none with maximum series # ?	SELECT t1.directed_by , t1.title from none as t1 WHERE t1.series_ = ( SELECT MAX ( t1.series_ ) {FROM, 3} )	1-18217741-1
how many none ' directed by have the word 10 in them ?	SELECT COUNT ( * ) from none as t1 WHERE t1.directed_by LIKE 10	1-18217741-1
list title of none that have the number of none greater than 10 .	SELECT t1.title from none as t1 GROUP BY t1.title HAVING COUNT ( * ) > 10	1-18217741-1
show title and the number of distinct u.s. viewers ( millions ) in each title .	SELECT t1.title , COUNT ( DISTINCT t1.u.s._viewers_ ) from none as t1 GROUP BY t1.title	1-18217741-1
what are the original air date , directed by , and original air date of each none ?	SELECT t1.original_air_date , t1.directed_by , t1.title from none as t1	1-18217741-1
which u.s. viewers ( millions ) has both none with less than 10 season # and none with more than 12 season # ?	SELECT t1.u.s._viewers_ from none as t1 WHERE t1.season_ < 10 INTERSECT SELECT t1.u.s._viewers_ from none as t1 WHERE t1.season_ > 12	1-18217741-1
