what is the record and date for the 1984 with the rank 5 smallest attendance ?	SELECT t1.record , t1.date from 1984 as t1 ORDER BY t1.attendance LIMIT 5	2-12207528-7
what is the opponent of the 1984 with the largest attendance ?	SELECT t1.opponent from 1984 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) from 1984 as t1 )	2-12207528-7
show all record and the total attendance for each .	SELECT t1.record , SUM ( t1.attendance ) from 1984 as t1 GROUP BY t1.record	2-12207528-7
show the date with fewer than 3 1984 .	SELECT t1.date from 1984 as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	2-12207528-7
how many 1984 are there in record 10 or 33 ?	SELECT COUNT ( * ) from 1984 as t1 WHERE t1.record = 10 OR t1.record = 33	2-12207528-7
return the maximum and minimum attendance across all 1984 .	SELECT MAX ( t1.attendance ) , MIN ( t1.attendance ) from 1984 as t1	2-12207528-7
return the smallest attendance for every record .	SELECT MIN ( t1.attendance ) , t1.record from 1984 as t1 GROUP BY t1.record	2-12207528-7
what is the maximum and mininum attendance {COLUMN} for all 1984 ?	SELECT MAX ( t1.attendance ) , MIN ( t1.attendance ) from 1984 as t1	2-12207528-7
Show everything on 1984	SELECT * FROM 1984	2-12207528-7
find the date of the 1984 which have opponent 10 but not 57 .	SELECT t1.date from 1984 as t1 WHERE t1.opponent = 10 EXCEPT SELECT t1.date from 1984 as t1 WHERE t1.opponent = 57	2-12207528-7
