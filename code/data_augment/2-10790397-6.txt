return the smallest crowd for every home team score .	SELECT MIN ( t1.crowd ) , t1.home_team_score from 1933 as t1 GROUP BY t1.home_team_score	2-10790397-6
find the venue of the 1933 with the highest crowd .	SELECT t1.venue from 1933 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10790397-6
what is the home team and away team score of the 1933 with maximum crowd ?	SELECT t1.home_team , t1.away_team_score from 1933 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10790397-6
find the distinct date of 1933 having crowd between 10 and 63 .	SELECT DISTINCT t1.date from 1933 as t1 WHERE t1.crowd BETWEEN 10 AND 63	2-10790397-6
show venue and the number of distinct venue in each venue .	SELECT t1.venue , COUNT ( DISTINCT t1.venue ) from 1933 as t1 GROUP BY t1.venue	2-10790397-6
what are the date of all 1933 with home team that is 10 ?	SELECT t1.date from 1933 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) = 10	2-10790397-6
which t1.away_team_score has the smallest amount of 1933?	SELECT t1.away_team_score from 1933 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10790397-6
what are the home team score , date , and home team score of each 1933 ?	SELECT t1.home_team_score , t1.date , t1.date from 1933 as t1	2-10790397-6
find the distinct COLUMN_NAME,0} of all 1933 that have crowd higher than some 1933 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.away_team from 1933 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.away_team ) from 1933 as t1 WHERE t1.home_team = 10 )	2-10790397-6
find the away team score of 1933 which are in 10 home team but not in 40 home team .	SELECT t1.away_team_score from 1933 as t1 WHERE t1.home_team = 10 EXCEPT SELECT t1.away_team_score from 1933 as t1 WHERE t1.home_team = 40	2-10790397-6
