which t1.country has the smallest amount of athletics?	SELECT t1.country from athletics as t1 GROUP BY t1.country ORDER BY COUNT ( * ) LIMIT 1	2-18569021-5
what is the minimum rank in each country ?	SELECT MIN ( t1.rank ) , t1.country from athletics as t1 GROUP BY t1.country	2-18569021-5
what is the country and athlete of the athletics with maximum lane ?	SELECT t1.country , t1.athlete from athletics as t1 WHERE t1.lane = ( SELECT MAX ( t1.lane ) {FROM, 3} )	2-18569021-5
which t1.athlete has the fewest athletics ?	SELECT t1.athlete from athletics as t1 GROUP BY t1.athlete ORDER BY COUNT ( * ) LIMIT 1	2-18569021-5
what is the minimum lane in each country ?	SELECT MIN ( t1.lane ) , t1.country from athletics as t1 GROUP BY t1.country	2-18569021-5
find the athlete of athletics who have more than 10 athletics .	SELECT t1.athlete from athletics as t1 GROUP BY t1.athlete HAVING COUNT ( * ) > 10	2-18569021-5
which country has most number of athletics ?	SELECT t1.country from athletics as t1 GROUP BY t1.country ORDER BY COUNT ( * ) DESC LIMIT 1	2-18569021-5
what is the maximum and mininum time {COLUMN} for all athletics ?	SELECT MAX ( t1.time ) , MIN ( t1.time ) from athletics as t1	2-18569021-5
how many athletics are there for each athlete ? list the smallest count first .	SELECT t1.athlete , COUNT ( * ) from athletics as t1 GROUP BY t1.athlete ORDER BY COUNT ( * )	2-18569021-5
return the smallest rank for every athlete .	SELECT MIN ( t1.rank ) , t1.athlete from athletics as t1 GROUP BY t1.athlete	2-18569021-5
