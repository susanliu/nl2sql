find the country and player of the 1992 with at least 10 finish .	SELECT t1.country , t1.player from 1992 as t1 GROUP BY t1.finish HAVING COUNT ( * ) >= 10	2-17162255-2
show all information on the 1992 that has the largest number of year ( s ) won.	SELECT * from 1992 as t1 ORDER BY t1.year_ DESC LIMIT 1	2-17162255-2
count the number of 1992 in year ( s ) won 10 or 65 .	SELECT COUNT ( * ) from 1992 as t1 WHERE t1.year_ = 10 OR t1.year_ = 65	2-17162255-2
what is the player and country of the 1992 with maximum total ?	SELECT t1.player , t1.country from 1992 as t1 WHERE t1.total = ( SELECT MAX ( t1.total ) {FROM, 3} )	2-17162255-2
count the number of 1992 in country 10 or 1 .	SELECT COUNT ( * ) from 1992 as t1 WHERE t1.country = 10 OR t1.country = 1	2-17162255-2
which player has the least total ?	SELECT t1.player from 1992 as t1 ORDER BY t1.total ASC LIMIT 1	2-17162255-2
find the finish and finish of the 1992 with at least 10 player .	SELECT t1.finish , t1.finish from 1992 as t1 GROUP BY t1.player HAVING COUNT ( * ) >= 10	2-17162255-2
show the country and the corresponding number of 1992 sorted by the number of country in ascending order .	SELECT t1.country , COUNT ( * ) from 1992 as t1 GROUP BY t1.country ORDER BY COUNT ( * )	2-17162255-2
how many 1992 are there for each finish ? list the smallest count first .	SELECT t1.finish , COUNT ( * ) from 1992 as t1 GROUP BY t1.finish ORDER BY COUNT ( * )	2-17162255-2
what are the country and player of 1992 with 10 or more player ?	SELECT t1.country , t1.player from 1992 as t1 GROUP BY t1.player HAVING COUNT ( * ) >= 10	2-17162255-2
