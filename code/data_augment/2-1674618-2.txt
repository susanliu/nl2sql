find the club of the mls with the highest games.	SELECT t1.club from mls as t1 WHERE t1.games = ( SELECT MAX ( t1.games ) from mls as t1 )	2-1674618-2
show the player of mls who have at least 10 mls .	SELECT t1.player from mls as t1 GROUP BY t1.player HAVING COUNT ( * ) >= 10	2-1674618-2
list the club of {mls which has number of mls greater than 10 .	SELECT t1.club from mls as t1 GROUP BY t1.club HAVING COUNT ( * ) > 10	2-1674618-2
list all information about mls .	SELECT * FROM mls	2-1674618-2
what are the player and club of each mls , listed in descending order by season ?	SELECT t1.player , t1.club from mls as t1 ORDER BY t1.season DESC	2-1674618-2
Return all columns in mls .	SELECT * FROM mls	2-1674618-2
what are the player and club of the {COLUMN} who have season above five or games below ten ?	SELECT t1.player , t1.club from mls as t1 WHERE t1.season > 5 OR t1.games < 10	2-1674618-2
give the maximum and minimum goals of all mls .	SELECT MAX ( t1.goals ) , MIN ( t1.goals ) from mls as t1	2-1674618-2
what is the minimum season in each player ?	SELECT MIN ( t1.season ) , t1.player from mls as t1 GROUP BY t1.player	2-1674618-2
show the player shared by more than 10 mls .	SELECT t1.player from mls as t1 GROUP BY t1.player HAVING COUNT ( * ) > 10	2-1674618-2
