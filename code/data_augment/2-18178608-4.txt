list all information about list .	SELECT * FROM list	2-18178608-4
what is the count of list with more than 10 status ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.status HAVING COUNT ( * ) > 10 	2-18178608-4
what are the distinct COLUMN_NAME,0} of every list that has a greater against than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.venue from list as t1 WHERE t1.against > ( SELECT MIN ( t1.venue ) from list as t1 WHERE t1.status = 10 )	2-18178608-4
show all venue and corresponding number of list in the ascending order of the numbers.	SELECT t1.venue , COUNT ( * ) from list as t1 GROUP BY t1.venue ORDER BY COUNT ( * )	2-18178608-4
what are the opposing teams and status of each list , listed in descending order by against ?	SELECT t1.opposing_teams , t1.status from list as t1 ORDER BY t1.against DESC	2-18178608-4
show the status shared by more than 10 list .	SELECT t1.status from list as t1 GROUP BY t1.status HAVING COUNT ( * ) > 10	2-18178608-4
how many list are there in venue 10 or 86 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.venue = 10 OR t1.venue = 86	2-18178608-4
show all opposing teams and corresponding number of list sorted by the count .	SELECT t1.opposing_teams , COUNT ( * ) from list as t1 GROUP BY t1.opposing_teams ORDER BY COUNT ( * )	2-18178608-4
find the opposing teams of the list with the highest against.	SELECT t1.opposing_teams from list as t1 WHERE t1.against = ( SELECT MAX ( t1.against ) from list as t1 )	2-18178608-4
find the opposing teams of the list with the largest against .	SELECT t1.opposing_teams from list as t1 ORDER BY t1.against DESC LIMIT 1	2-18178608-4
