return the player of the 2004 that has the fewest corresponding player .	SELECT t1.player from 2004 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-11598240-3
what is the minimum pick in each school ?	SELECT MIN ( t1.pick ) , t1.school from 2004 as t1 GROUP BY t1.school	2-11598240-3
show the team , team , and position of all the 2004 .	SELECT t1.team , t1.team , t1.position from 2004 as t1	2-11598240-3
find the school of 2004 who have both 10 and 58 pick .	SELECT t1.school from 2004 as t1 WHERE t1.pick = 10 INTERSECT SELECT t1.school from 2004 as t1 WHERE t1.pick = 58	2-11598240-3
find the distinct player of 2004 having pick between 10 and 101 .	SELECT DISTINCT t1.player from 2004 as t1 WHERE t1.pick BETWEEN 10 AND 101	2-11598240-3
show the position with fewer than 3 2004 .	SELECT t1.position from 2004 as t1 GROUP BY t1.position HAVING COUNT ( * ) < 3	2-11598240-3
show player for all 2004 whose pick are greater than the average .	SELECT t1.player from 2004 as t1 WHERE t1.pick > ( SELECT AVG ( t1.pick ) from 2004 as t1	2-11598240-3
how many 2004 correspond to each team? show the result in ascending order.	SELECT t1.team , COUNT ( * ) from 2004 as t1 GROUP BY t1.team ORDER BY COUNT ( * )	2-11598240-3
sort the list of school and school of all 2004 in the descending order of pick .	SELECT t1.school , t1.school from 2004 as t1 ORDER BY t1.pick DESC	2-11598240-3
what are the distinct school with pick between 10 and 81 ?	SELECT DISTINCT t1.school from 2004 as t1 WHERE t1.pick BETWEEN 10 AND 81	2-11598240-3
