what are the company for all fortune , and what is the total {rank for each ?	SELECT t1.company , SUM ( t1.rank ) from fortune as t1 GROUP BY t1.company	2-17581425-1
which country have greater rank than that of any rank in fortune ?	SELECT t1.country from fortune as t1 WHERE t1.rank > ( SELECT MIN ( t1.rank ) from fortune as t1 )	2-17581425-1
list all information about fortune .	SELECT * FROM fortune	2-17581425-1
return the country and country of fortune with the five lowest rank .	SELECT t1.country , t1.country from fortune as t1 ORDER BY t1.rank LIMIT 5	2-17581425-1
show the industry and the number of unique revenue in usd containing each industry .	SELECT t1.industry , COUNT ( DISTINCT t1.revenue_in_usd ) from fortune as t1 GROUP BY t1.industry	2-17581425-1
what are the company with exactly 10 fortune ?	SELECT t1.company from fortune as t1 GROUP BY t1.company HAVING COUNT ( * ) = 10	2-17581425-1
find the industry of the fortune with the largest rank .	SELECT t1.industry from fortune as t1 ORDER BY t1.rank DESC LIMIT 1	2-17581425-1
show all industry and corresponding number of fortune sorted by the count .	SELECT t1.industry , COUNT ( * ) from fortune as t1 GROUP BY t1.industry ORDER BY COUNT ( * )	2-17581425-1
what are the industry of all fortune with country that is 10 ?	SELECT t1.industry from fortune as t1 GROUP BY t1.country HAVING COUNT ( * ) = 10	2-17581425-1
what is the company of the fortune with the minimum rank ?	SELECT t1.company from fortune as t1 ORDER BY t1.rank ASC LIMIT 1	2-17581425-1
