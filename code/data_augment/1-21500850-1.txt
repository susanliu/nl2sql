find all original game that have fewer than three in guitar .	SELECT t1.original_game from guitar as t1 GROUP BY t1.original_game HAVING COUNT ( * ) < 3	1-21500850-1
return the genre of the guitar with the fewest year .	SELECT t1.genre from guitar as t1 ORDER BY t1.year ASC LIMIT 1	1-21500850-1
what is the band tier/venue of the guitar with the smallest year ?	SELECT t1.band_tier/venue from guitar as t1 ORDER BY t1.year ASC LIMIT 1	1-21500850-1
Return all columns in guitar .	SELECT * FROM guitar	1-21500850-1
how many guitar does each artist have ?	SELECT t1.artist , COUNT ( * ) from guitar as t1 GROUP BY t1.artist ORDER BY COUNT ( * )	1-21500850-1
what are the band tier/venue , genre , and exportable to gh5/bh of each guitar ?	SELECT t1.band_tier/venue , t1.genre , t1.exportable_to_gh5/bh from guitar as t1	1-21500850-1
what are the song title and original game ?	SELECT t1.song_title , t1.original_game from guitar as t1	1-21500850-1
count the number of guitar that have an genre containing 10 .	SELECT COUNT ( * ) from guitar as t1 WHERE t1.genre LIKE 10	1-21500850-1
please show the original game of the guitar that have at least 10 records .	SELECT t1.original_game from guitar as t1 GROUP BY t1.original_game HAVING COUNT ( * ) >= 10	1-21500850-1
what is the genre of the guitar with the minimum year ?	SELECT t1.genre from guitar as t1 ORDER BY t1.year ASC LIMIT 1	1-21500850-1
