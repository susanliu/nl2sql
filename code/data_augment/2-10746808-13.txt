what is the maximum and mininum crowd {COLUMN} for all 1926 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1926 as t1	2-10746808-13
which away team score have an average crowd over 10 ?	SELECT t1.away_team_score from 1926 as t1 GROUP BY t1.away_team_score HAVING AVG ( t1.crowd ) >= 10	2-10746808-13
Show everything on 1926	SELECT * FROM 1926	2-10746808-13
find the away team and venue of the 1926 with at least 10 home team .	SELECT t1.away_team , t1.venue from 1926 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10746808-13
find the venue of 1926 which have both 10 and 43 as venue .	SELECT t1.venue from 1926 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.venue from 1926 as t1 WHERE t1.crowd = 43	2-10746808-13
return the home team score of 1926 for which the away team score is not 10 ?	SELECT t1.home_team_score from 1926 as t1 WHERE t1.away_team_score ! = 10	2-10746808-13
return the home team and home team of 1926 with the five lowest crowd .	SELECT t1.home_team , t1.home_team from 1926 as t1 ORDER BY t1.crowd LIMIT 5	2-10746808-13
list venue of 1926 that have the number of 1926 greater than 10 .	SELECT t1.venue from 1926 as t1 GROUP BY t1.venue HAVING COUNT ( * ) > 10	2-10746808-13
what are the distinct COLUMN_NAME,0} of every 1926 that has a greater crowd than some 1926 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.away_team_score from 1926 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.away_team_score ) from 1926 as t1 WHERE t1.home_team_score = 10 )	2-10746808-13
how many away team did each 1926 do, ordered by number of away team ?	SELECT t1.away_team , COUNT ( * ) from 1926 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * )	2-10746808-13
