list the entrant which average year is above 10 .	SELECT t1.entrant from cosworth as t1 GROUP BY t1.entrant HAVING AVG ( t1.year ) >= 10	2-164104-4
find the chassis of cosworth which have both 10 and 67 as chassis .	SELECT t1.chassis from cosworth as t1 WHERE t1.year = 10 INTERSECT SELECT t1.chassis from cosworth as t1 WHERE t1.year = 67	2-164104-4
how many cosworth are there that have more than {VALUE},0 entrant ?	SELECT COUNT ( * ) from cosworth as t1 GROUP BY t1.entrant HAVING COUNT ( * ) > 10 	2-164104-4
Return all columns in cosworth .	SELECT * FROM cosworth	2-164104-4
list entrant and points who have year greater than 5 or year shorter than 10 .	SELECT t1.entrant , t1.points from cosworth as t1 WHERE t1.year > 5 OR t1.year < 10	2-164104-4
please list the entrant and points of cosworth in descending order of year .	SELECT t1.entrant , t1.points from cosworth as t1 ORDER BY t1.year DESC	2-164104-4
what is the maximum and mininum year {COLUMN} for all cosworth ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from cosworth as t1	2-164104-4
which points has most number of cosworth ?	SELECT t1.points from cosworth as t1 GROUP BY t1.points ORDER BY COUNT ( * ) DESC LIMIT 1	2-164104-4
Show everything on cosworth	SELECT * FROM cosworth	2-164104-4
how many cosworth are there in chassis 10 or 70 ?	SELECT COUNT ( * ) from cosworth as t1 WHERE t1.chassis = 10 OR t1.chassis = 70	2-164104-4
