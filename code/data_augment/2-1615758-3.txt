what is the rank of highest points ?	SELECT t1.rank from tony as t1 ORDER BY t1.points DESC LIMIT 1	2-1615758-3
find the number of tony that have more than 10 team .	SELECT COUNT ( * ) from tony as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10 	2-1615758-3
give the t1.team with the fewest tony .	SELECT t1.team from tony as t1 GROUP BY t1.team ORDER BY COUNT ( * ) LIMIT 1	2-1615758-3
list engine and engine who have year greater than 5 or year shorter than 10 .	SELECT t1.engine , t1.engine from tony as t1 WHERE t1.year > 5 OR t1.year < 10	2-1615758-3
sort the list of rank and engine of all tony in the descending order of points .	SELECT t1.rank , t1.engine from tony as t1 ORDER BY t1.points DESC	2-1615758-3
which engine have an average year over 10 ?	SELECT t1.engine from tony as t1 GROUP BY t1.engine HAVING AVG ( t1.year ) >= 10	2-1615758-3
what is the team and chassis of the tony with maximum year ?	SELECT t1.team , t1.chassis from tony as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-1615758-3
how many tony correspond to each engine? show the result in ascending order.	SELECT t1.engine , COUNT ( * ) from tony as t1 GROUP BY t1.engine ORDER BY COUNT ( * )	2-1615758-3
which engine has most number of tony ?	SELECT t1.engine from tony as t1 GROUP BY t1.engine ORDER BY COUNT ( * ) DESC LIMIT 1	2-1615758-3
what are the maximum and minimum points across all tony ?	SELECT MAX ( t1.points ) , MIN ( t1.points ) from tony as t1	2-1615758-3
