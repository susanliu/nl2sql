which composer has both malta with less than 10 draw and malta with more than 64 draw ?	SELECT t1.composer from malta as t1 WHERE t1.draw < 10 INTERSECT SELECT t1.composer from malta as t1 WHERE t1.draw > 64	2-15050746-1
what are the distinct artist with draw between 10 and 72 ?	SELECT DISTINCT t1.artist from malta as t1 WHERE t1.draw BETWEEN 10 AND 72	2-15050746-1
show the lyricist shared by more than 10 malta .	SELECT t1.lyricist from malta as t1 GROUP BY t1.lyricist HAVING COUNT ( * ) > 10	2-15050746-1
what are the maximum and minimum points across all malta ?	SELECT MAX ( t1.points ) , MIN ( t1.points ) from malta as t1	2-15050746-1
which artist have an average points over 10 ?	SELECT t1.artist from malta as t1 GROUP BY t1.artist HAVING AVG ( t1.points ) >= 10	2-15050746-1
show the artist of malta who have at least 10 malta .	SELECT t1.artist from malta as t1 GROUP BY t1.artist HAVING COUNT ( * ) >= 10	2-15050746-1
how many malta have composer that contain the word 10 ?	SELECT COUNT ( * ) from malta as t1 WHERE t1.composer LIKE 10	2-15050746-1
list the artist which average draw is above 10 .	SELECT t1.artist from malta as t1 GROUP BY t1.artist HAVING AVG ( t1.draw ) >= 10	2-15050746-1
what are the lyricist and song of each malta ?	SELECT t1.lyricist , t1.song from malta as t1	2-15050746-1
which song have an average points over 10 ?	SELECT t1.song from malta as t1 GROUP BY t1.song HAVING AVG ( t1.points ) >= 10	2-15050746-1
