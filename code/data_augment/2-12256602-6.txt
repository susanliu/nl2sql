how many list ' years have the word 10 in them ?	SELECT COUNT ( * ) from list as t1 WHERE t1.years LIKE 10	2-12256602-6
please list the years and authority of list in descending order of roll .	SELECT t1.years , t1.authority from list as t1 ORDER BY t1.roll DESC	2-12256602-6
please show the different years , ordered by the number of list that have each .	SELECT t1.years from list as t1 GROUP BY t1.years ORDER BY COUNT ( * ) ASC LIMIT 1	2-12256602-6
find the authority and gender of the list whose roll is lower than the average roll of all list .	SELECT t1.authority , t1.gender from list as t1 WHERE t1.roll < ( SELECT AVG ( t1.roll ) {FROM, 3} )	2-12256602-6
please show the authority of the list with count more than 10 .	SELECT t1.authority from list as t1 GROUP BY t1.authority HAVING COUNT ( * ) > 10	2-12256602-6
which t1.gender has the fewest list ?	SELECT t1.gender from list as t1 GROUP BY t1.gender ORDER BY COUNT ( * ) LIMIT 1	2-12256602-6
which gender has both list with less than 10 roll and list with more than 71 roll ?	SELECT t1.gender from list as t1 WHERE t1.roll < 10 INTERSECT SELECT t1.gender from list as t1 WHERE t1.roll > 71	2-12256602-6
what are the distinct name with roll between 10 and 14 ?	SELECT DISTINCT t1.name from list as t1 WHERE t1.roll BETWEEN 10 AND 14	2-12256602-6
what are the area of all list that have 10 or more list ?	SELECT t1.area from list as t1 GROUP BY t1.area HAVING COUNT ( * ) >= 10	2-12256602-6
find the number of list that have more than 10 area .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.area HAVING COUNT ( * ) > 10 	2-12256602-6
