list all information about 1970 .	SELECT * FROM 1970	2-1122483-1
show the driver , driver , and constructor of all the 1970 .	SELECT t1.driver , t1.driver , t1.constructor from 1970 as t1	2-1122483-1
find the driver of the 1970 with the highest grid .	SELECT t1.driver from 1970 as t1 ORDER BY t1.grid DESC LIMIT 1	2-1122483-1
what are the time/retired and constructor of each 1970 , listed in descending order by grid ?	SELECT t1.time/retired , t1.constructor from 1970 as t1 ORDER BY t1.grid DESC	2-1122483-1
show the driver of 1970 who have at least 10 1970 .	SELECT t1.driver from 1970 as t1 GROUP BY t1.driver HAVING COUNT ( * ) >= 10	2-1122483-1
which time/retired has the least grid ?	SELECT t1.time/retired from 1970 as t1 ORDER BY t1.grid ASC LIMIT 1	2-1122483-1
how many distinct constructor correspond to each constructor ?	SELECT t1.constructor , COUNT ( DISTINCT t1.constructor ) from 1970 as t1 GROUP BY t1.constructor	2-1122483-1
what is all the information on the 1970 with the largest number of driver ?	SELECT * from 1970 as t1 ORDER BY t1.driver DESC LIMIT 1	2-1122483-1
show the time/retired with fewer than 3 1970 .	SELECT t1.time/retired from 1970 as t1 GROUP BY t1.time/retired HAVING COUNT ( * ) < 3	2-1122483-1
which time/retired have less than 3 in 1970 ?	SELECT t1.time/retired from 1970 as t1 GROUP BY t1.time/retired HAVING COUNT ( * ) < 3	2-1122483-1
