show the date and away team score with at least 10 date .	SELECT t1.date , t1.away_team_score from 1967 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-10808681-18
show the date and the number of unique home team score containing each date .	SELECT t1.date , COUNT ( DISTINCT t1.home_team_score ) from 1967 as t1 GROUP BY t1.date	2-10808681-18
which away team score has the least crowd ?	SELECT t1.away_team_score from 1967 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10808681-18
give the maximum and minimum crowd of all 1967 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1967 as t1	2-10808681-18
what are the venue and date ?	SELECT t1.venue , t1.date from 1967 as t1	2-10808681-18
what are the venue and home team score of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.venue , t1.home_team_score from 1967 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10808681-18
count the number of 1967 that have an home team score containing 10 .	SELECT COUNT ( * ) from 1967 as t1 WHERE t1.home_team_score LIKE 10	2-10808681-18
count the number of 1967 in date 10 or 89 .	SELECT COUNT ( * ) from 1967 as t1 WHERE t1.date = 10 OR t1.date = 89	2-10808681-18
list date and home team who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.date , t1.home_team from 1967 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10808681-18
what is the t1.away_team_score of 1967 that has fewest number of 1967 ?	SELECT t1.away_team_score from 1967 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10808681-18
