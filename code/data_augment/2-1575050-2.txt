what are the average silver of archery , grouped by rank ?	SELECT AVG ( t1.silver ) , t1.rank from archery as t1 GROUP BY t1.rank	2-1575050-2
find the nation of archery who have more than 10 archery .	SELECT t1.nation from archery as t1 GROUP BY t1.nation HAVING COUNT ( * ) > 10	2-1575050-2
find the rank of archery whose bronze is higher than the average bronze .	SELECT t1.rank from archery as t1 WHERE t1.bronze > ( SELECT AVG ( t1.bronze ) from archery as t1	2-1575050-2
what is the nation of the archery with least number of nation ?	SELECT t1.nation from archery as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) ASC LIMIT 1	2-1575050-2
what are the rank of archery whose rank are not 10 ?	SELECT t1.rank from archery as t1 WHERE t1.rank ! = 10	2-1575050-2
which nation has both archery with less than 10 gold and archery with more than 97 gold ?	SELECT t1.nation from archery as t1 WHERE t1.gold < 10 INTERSECT SELECT t1.nation from archery as t1 WHERE t1.gold > 97	2-1575050-2
count the number of archery in rank 10 or 95 .	SELECT COUNT ( * ) from archery as t1 WHERE t1.rank = 10 OR t1.rank = 95	2-1575050-2
what are the average silver of archery , grouped by nation ?	SELECT AVG ( t1.silver ) , t1.nation from archery as t1 GROUP BY t1.nation	2-1575050-2
what are the nation , nation , and nation for each archery ?	SELECT t1.nation , t1.nation , t1.nation from archery as t1	2-1575050-2
what are the nation for archery that have an gold greater than the average .	SELECT t1.nation from archery as t1 WHERE t1.gold > ( SELECT AVG ( t1.gold ) from archery as t1	2-1575050-2
