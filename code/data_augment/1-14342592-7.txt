give the starter that has the most 1904 .	SELECT t1.starter from 1904 as t1 GROUP BY t1.starter ORDER BY COUNT ( * ) DESC LIMIT 1	1-14342592-7
how many player did each 1904 do, ordered by number of player ?	SELECT t1.player , COUNT ( * ) from 1904 as t1 GROUP BY t1.player ORDER BY COUNT ( * )	1-14342592-7
what are the maximum and minimum field goals across all 1904 ?	SELECT MAX ( t1.field_goals ) , MIN ( t1.field_goals ) from 1904 as t1	1-14342592-7
return each player with the number of 1904 in ascending order of the number of player .	SELECT t1.player , COUNT ( * ) from 1904 as t1 GROUP BY t1.player ORDER BY COUNT ( * )	1-14342592-7
find the distinct player of 1904 having extra points between 10 and 82 .	SELECT DISTINCT t1.player from 1904 as t1 WHERE t1.extra_points BETWEEN 10 AND 82	1-14342592-7
find the distinct player of 1904 having field goals between 10 and 85 .	SELECT DISTINCT t1.player from 1904 as t1 WHERE t1.field_goals BETWEEN 10 AND 85	1-14342592-7
show the player of the 1904 that has the most 1904 .	SELECT t1.player from 1904 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) DESC LIMIT 1	1-14342592-7
what is the player of the most common 1904 in all player ?	SELECT t1.player from 1904 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) DESC LIMIT 1	1-14342592-7
select the average points of each 1904 's position .	SELECT AVG ( t1.points ) , t1.position from 1904 as t1 GROUP BY t1.position	1-14342592-7
which position have an average extra points over 10 ?	SELECT t1.position from 1904 as t1 GROUP BY t1.position HAVING AVG ( t1.extra_points ) >= 10	1-14342592-7
