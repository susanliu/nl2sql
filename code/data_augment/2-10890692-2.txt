what is the chin-up ( reps ) and shuttle run ( sec ) of every ippt that has a points lower than average ?	SELECT t1.chin-up_ , t1.shuttle_run_ from ippt as t1 WHERE t1.points < ( SELECT AVG ( t1.points ) {FROM, 3} )	2-10890692-2
show the grade and the number of unique sit-up ( reps ) containing each grade .	SELECT t1.grade , COUNT ( DISTINCT t1.sit-up_ ) from ippt as t1 GROUP BY t1.grade	2-10890692-2
what are the grade of the ippt with grade other than 10 ?	SELECT t1.grade from ippt as t1 WHERE t1.standing_broad_jump_ ! = 10	2-10890692-2
how many different chin-up ( reps ) correspond to each sit-up ( reps ) ?	SELECT t1.sit-up_ , COUNT ( DISTINCT t1.chin-up_ ) from ippt as t1 GROUP BY t1.sit-up_	2-10890692-2
list the sit-up ( reps ) and grade of all ippt sorted by points in descending order .	SELECT t1.sit-up_ , t1.grade from ippt as t1 ORDER BY t1.points DESC	2-10890692-2
what are the standing broad jump ( cm ) of the ippt that have exactly 10 ippt ?	SELECT t1.standing_broad_jump_ from ippt as t1 GROUP BY t1.standing_broad_jump_ HAVING COUNT ( * ) = 10	2-10890692-2
how many ippt have standing broad jump ( cm ) that contains 10 ?	SELECT COUNT ( * ) from ippt as t1 WHERE t1.standing_broad_jump_ LIKE 10	2-10890692-2
which grade has both ippt with less than 10 points and ippt with more than 97 points ?	SELECT t1.grade from ippt as t1 WHERE t1.points < 10 INTERSECT SELECT t1.grade from ippt as t1 WHERE t1.points > 97	2-10890692-2
what are the grade for all ippt , and what is the total {points for each ?	SELECT t1.grade , SUM ( t1.points ) from ippt as t1 GROUP BY t1.grade	2-10890692-2
find the 2.4 km run ( min : sec ) of ippt which are in 10 2.4 km run ( min : sec ) but not in 80 2.4 km run ( min : sec ) .	SELECT t1.2.4_km_run_ from ippt as t1 WHERE t1.2.4_km_run_ = 10 EXCEPT SELECT t1.2.4_km_run_ from ippt as t1 WHERE t1.2.4_km_run_ = 80	2-10890692-2
