find the language ( s ) of man who have year of both 10 and 61 .	SELECT t1.language_ from man as t1 WHERE t1.year = 10 INTERSECT SELECT t1.language_ from man as t1 WHERE t1.year = 61	2-1526491-1
find the country of the man with the highest year.	SELECT t1.country from man as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from man as t1 )	2-1526491-1
show all name and the total year for each .	SELECT t1.name , SUM ( t1.year ) from man as t1 GROUP BY t1.name	2-1526491-1
show all information on the man that has the largest number of name.	SELECT * from man as t1 ORDER BY t1.name DESC LIMIT 1	2-1526491-1
find the number of man that have more than 10 name .	SELECT COUNT ( * ) from man as t1 GROUP BY t1.name HAVING COUNT ( * ) > 10 	2-1526491-1
what are the distinct COLUMN_NAME,0} of man with year higher than any man from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.language_ from man as t1 WHERE t1.year > ( SELECT MIN ( t1.language_ ) from man as t1 WHERE t1.country = 10 )	2-1526491-1
what are the language ( s ) more than 10 man have ?	SELECT t1.language_ from man as t1 GROUP BY t1.language_ HAVING COUNT ( * ) > 10	2-1526491-1
select the average year of each man 's name .	SELECT AVG ( t1.year ) , t1.name from man as t1 GROUP BY t1.name	2-1526491-1
return the smallest year for every country .	SELECT MIN ( t1.year ) , t1.country from man as t1 GROUP BY t1.country	2-1526491-1
find the country of man which are name 10 but not name 97 .	SELECT t1.country from man as t1 WHERE t1.name = 10 EXCEPT SELECT t1.country from man as t1 WHERE t1.name = 97	2-1526491-1
