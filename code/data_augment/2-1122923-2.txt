find the driver of 1985 who have both 10 and 38 grid .	SELECT t1.driver from 1985 as t1 WHERE t1.grid = 10 INTERSECT SELECT t1.driver from 1985 as t1 WHERE t1.grid = 38	2-1122923-2
what is the t1.time/retired of 1985 that has fewest number of 1985 ?	SELECT t1.time/retired from 1985 as t1 GROUP BY t1.time/retired ORDER BY COUNT ( * ) LIMIT 1	2-1122923-2
find the driver for all 1985 who have more than the average laps .	SELECT t1.driver from 1985 as t1 WHERE t1.laps > ( SELECT AVG ( t1.laps ) from 1985 as t1	2-1122923-2
return the maximum and minimum grid across all 1985 .	SELECT MAX ( t1.grid ) , MIN ( t1.grid ) from 1985 as t1	2-1122923-2
find the number of 1985 that have more than 10 time/retired .	SELECT COUNT ( * ) from 1985 as t1 GROUP BY t1.time/retired HAVING COUNT ( * ) > 10 	2-1122923-2
what is the time/retired and driver of the 1985 with maximum grid ?	SELECT t1.time/retired , t1.driver from 1985 as t1 WHERE t1.grid = ( SELECT MAX ( t1.grid ) {FROM, 3} )	2-1122923-2
what are the time/retired of all 1985 with time/retired that is 10 ?	SELECT t1.time/retired from 1985 as t1 GROUP BY t1.time/retired HAVING COUNT ( * ) = 10	2-1122923-2
show the time/retired and the corresponding number of 1985 sorted by the number of time/retired in ascending order .	SELECT t1.time/retired , COUNT ( * ) from 1985 as t1 GROUP BY t1.time/retired ORDER BY COUNT ( * )	2-1122923-2
what is the constructor and time/retired of every 1985 that has a grid lower than average ?	SELECT t1.constructor , t1.time/retired from 1985 as t1 WHERE t1.grid < ( SELECT AVG ( t1.grid ) {FROM, 3} )	2-1122923-2
show the constructor with fewer than 3 1985 .	SELECT t1.constructor from 1985 as t1 GROUP BY t1.constructor HAVING COUNT ( * ) < 3	2-1122923-2
