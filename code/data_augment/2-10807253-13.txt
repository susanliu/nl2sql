which home team score has both 1940 with less than 10 crowd and 1940 with more than 61 crowd ?	SELECT t1.home_team_score from 1940 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team_score from 1940 as t1 WHERE t1.crowd > 61	2-10807253-13
which away team is the most frequent away team?	SELECT t1.away_team from 1940 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10807253-13
show the venue with fewer than 3 1940 .	SELECT t1.venue from 1940 as t1 GROUP BY t1.venue HAVING COUNT ( * ) < 3	2-10807253-13
what are the home team for all 1940 , and what is the total {crowd for each ?	SELECT t1.home_team , SUM ( t1.crowd ) from 1940 as t1 GROUP BY t1.home_team	2-10807253-13
Return all columns in 1940 .	SELECT * FROM 1940	2-10807253-13
what are the venue of 1940 , sorted by their frequency?	SELECT t1.venue from 1940 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) ASC LIMIT 1	2-10807253-13
which t1.away_team_score has least number of 1940 ?	SELECT t1.away_team_score from 1940 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10807253-13
find the distinct home team of all 1940 that have a higher crowd than some 1940 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.home_team from 1940 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.home_team ) from 1940 as t1 WHERE t1.away_team_score = 10 )	2-10807253-13
what are the distinct away team score with crowd between 10 and 79 ?	SELECT DISTINCT t1.away_team_score from 1940 as t1 WHERE t1.crowd BETWEEN 10 AND 79	2-10807253-13
return the home team of the largest crowd.	SELECT t1.home_team from 1940 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10807253-13
