return the official name of the largest population.	SELECT t1.official_name from york as t1 ORDER BY t1.population DESC LIMIT 1	2-176533-2
show the census ranking of the york that has the most york .	SELECT t1.census_ranking from york as t1 GROUP BY t1.census_ranking ORDER BY COUNT ( * ) DESC LIMIT 1	2-176533-2
please list the official name and official name of york in descending order of population .	SELECT t1.official_name , t1.official_name from york as t1 ORDER BY t1.population DESC	2-176533-2
what are the average area km 2 of york , grouped by official name ?	SELECT AVG ( t1.area_km_2 ) , t1.official_name from york as t1 GROUP BY t1.official_name	2-176533-2
which status have an average area km 2 over 10 ?	SELECT t1.status from york as t1 GROUP BY t1.status HAVING AVG ( t1.area_km_2 ) >= 10	2-176533-2
count the number of york in status 10 or 99 .	SELECT COUNT ( * ) from york as t1 WHERE t1.status = 10 OR t1.status = 99	2-176533-2
find the official name of york which are census ranking 10 but not census ranking 65 .	SELECT t1.official_name from york as t1 WHERE t1.census_ranking = 10 EXCEPT SELECT t1.official_name from york as t1 WHERE t1.census_ranking = 65	2-176533-2
what is the official name of the york with the smallest area km 2 ?	SELECT t1.official_name from york as t1 ORDER BY t1.area_km_2 ASC LIMIT 1	2-176533-2
how many york have official name that contain the word 10 ?	SELECT COUNT ( * ) from york as t1 WHERE t1.official_name LIKE 10	2-176533-2
which status have less than 3 in york ?	SELECT t1.status from york as t1 GROUP BY t1.status HAVING COUNT ( * ) < 3	2-176533-2
