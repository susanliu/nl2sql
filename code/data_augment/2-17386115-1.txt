list player and position who have round greater than 5 or round shorter than 10 .	SELECT t1.player , t1.position from 1995 as t1 WHERE t1.round > 5 OR t1.round < 10	2-17386115-1
find the school/club team of 1995 which have both 10 and 59 as school/club team .	SELECT t1.school/club_team from 1995 as t1 WHERE t1.round = 10 INTERSECT SELECT t1.school/club_team from 1995 as t1 WHERE t1.round = 59	2-17386115-1
what are the player of 1995 whose position are not 10 ?	SELECT t1.player from 1995 as t1 WHERE t1.position ! = 10	2-17386115-1
give the player that has the most 1995 .	SELECT t1.player from 1995 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) DESC LIMIT 1	2-17386115-1
what are the position of the 1995 that have exactly 10 1995 ?	SELECT t1.position from 1995 as t1 GROUP BY t1.position HAVING COUNT ( * ) = 10	2-17386115-1
show all information on the 1995 that has the largest number of player.	SELECT * from 1995 as t1 ORDER BY t1.player DESC LIMIT 1	2-17386115-1
return the maximum and minimum pick across all 1995 .	SELECT MAX ( t1.pick ) , MIN ( t1.pick ) from 1995 as t1	2-17386115-1
how many distinct player correspond to each player ?	SELECT t1.player , COUNT ( DISTINCT t1.player ) from 1995 as t1 GROUP BY t1.player	2-17386115-1
return the smallest round for every school/club team .	SELECT MIN ( t1.round ) , t1.school/club_team from 1995 as t1 GROUP BY t1.school/club_team	2-17386115-1
how many 1995 are there that have more than {VALUE},0 player ?	SELECT COUNT ( * ) from 1995 as t1 GROUP BY t1.player HAVING COUNT ( * ) > 10 	2-17386115-1
