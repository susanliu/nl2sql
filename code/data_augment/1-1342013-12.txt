list the candidates which average first elected is above 10 .	SELECT t1.candidates from united as t1 GROUP BY t1.candidates HAVING AVG ( t1.first_elected ) >= 10	1-1342013-12
find the number of united that have more than 10 candidates .	SELECT COUNT ( * ) from united as t1 GROUP BY t1.candidates HAVING COUNT ( * ) > 10 	1-1342013-12
please list the candidates and incumbent of united in descending order of first elected .	SELECT t1.candidates , t1.incumbent from united as t1 ORDER BY t1.first_elected DESC	1-1342013-12
what is the result of the united with the largest first elected ?	SELECT t1.result from united as t1 WHERE t1.first_elected = ( SELECT MAX ( t1.first_elected ) from united as t1 )	1-1342013-12
which result have less than 3 in united ?	SELECT t1.result from united as t1 GROUP BY t1.result HAVING COUNT ( * ) < 3	1-1342013-12
what is the result of the united with the minimum first elected ?	SELECT t1.result from united as t1 ORDER BY t1.first_elected ASC LIMIT 1	1-1342013-12
return the smallest first elected for every district .	SELECT MIN ( t1.first_elected ) , t1.district from united as t1 GROUP BY t1.district	1-1342013-12
list the result of {united which has number of united greater than 10 .	SELECT t1.result from united as t1 GROUP BY t1.result HAVING COUNT ( * ) > 10	1-1342013-12
find the district of united who have both 10 and 10 first elected .	SELECT t1.district from united as t1 WHERE t1.first_elected = 10 INTERSECT SELECT t1.district from united as t1 WHERE t1.first_elected = 10	1-1342013-12
Return all columns in united .	SELECT * FROM united	1-1342013-12
