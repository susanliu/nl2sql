find the away team score of the 1950 that is most frequent across all away team score .	SELECT t1.away_team_score from 1950 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-10701045-9
what are the date of all 1950 with date that is 10 ?	SELECT t1.date from 1950 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10701045-9
which venue has both 1950 with less than 10 crowd and 1950 with more than 67 crowd ?	SELECT t1.venue from 1950 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.venue from 1950 as t1 WHERE t1.crowd > 67	2-10701045-9
please list the date and away team of 1950 in descending order of crowd .	SELECT t1.date , t1.away_team from 1950 as t1 ORDER BY t1.crowd DESC	2-10701045-9
what are the maximum and minimum crowd across all 1950 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1950 as t1	2-10701045-9
what are the away team score and away team score of each 1950 ?	SELECT t1.away_team_score , t1.away_team_score from 1950 as t1	2-10701045-9
show the away team score with fewer than 3 1950 .	SELECT t1.away_team_score from 1950 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) < 3	2-10701045-9
find the away team of the 1950 with the largest crowd .	SELECT t1.away_team from 1950 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10701045-9
what is the away team of the 1950 with the smallest crowd ?	SELECT t1.away_team from 1950 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10701045-9
what are the home team score of all 1950 that have 10 or more 1950 ?	SELECT t1.home_team_score from 1950 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) >= 10	2-10701045-9
