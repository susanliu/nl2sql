find the title of the none with the largest no . in season .	SELECT t1.title from none as t1 ORDER BY t1.no_._in_season DESC LIMIT 1	1-13336122-5
what is the directed by of all none whose no . in series is higher than any none ?	SELECT t1.directed_by from none as t1 WHERE t1.no_._in_series > ( SELECT MIN ( t1.no_._in_series ) from none as t1 )	1-13336122-5
what are the original air date , title , and title for each none ?	SELECT t1.original_air_date , t1.title , t1.title from none as t1	1-13336122-5
which title have less than 3 in none ?	SELECT t1.title from none as t1 GROUP BY t1.title HAVING COUNT ( * ) < 3	1-13336122-5
which directed by has both none with less than 10 no . in series and none with more than 3 no . in series ?	SELECT t1.directed_by from none as t1 WHERE t1.no_._in_series < 10 INTERSECT SELECT t1.directed_by from none as t1 WHERE t1.no_._in_series > 3	1-13336122-5
what is the written by and u.s. viewers ( million ) of the none with maximum no . in series ?	SELECT t1.written_by , t1.u.s._viewers_ from none as t1 WHERE t1.no_._in_series = ( SELECT MAX ( t1.no_._in_series ) {FROM, 3} )	1-13336122-5
return the different written by of none , in ascending order of frequency .	SELECT t1.written_by from none as t1 GROUP BY t1.written_by ORDER BY COUNT ( * ) ASC LIMIT 1	1-13336122-5
what is the u.s. viewers ( million ) of the none with the smallest no . in series ?	SELECT t1.u.s._viewers_ from none as t1 ORDER BY t1.no_._in_series ASC LIMIT 1	1-13336122-5
find the written by of the none with the largest no . in series .	SELECT t1.written_by from none as t1 ORDER BY t1.no_._in_series DESC LIMIT 1	1-13336122-5
what are the written by that have greater no . in series than any no . in series in none ?	SELECT t1.written_by from none as t1 WHERE t1.no_._in_series > ( SELECT MIN ( t1.no_._in_series ) from none as t1 )	1-13336122-5
