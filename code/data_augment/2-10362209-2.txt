what are the result of all 1997 with date that is 10 ?	SELECT t1.result from 1997 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10362209-2
list the attendance , opponent and the opponent of the 1997 .	SELECT t1.attendance , t1.opponent , t1.opponent from 1997 as t1	2-10362209-2
what are total week for each result ?	SELECT t1.result , SUM ( t1.week ) from 1997 as t1 GROUP BY t1.result	2-10362209-2
show the result and attendance with at least 10 opponent .	SELECT t1.result , t1.attendance from 1997 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-10362209-2
return the different attendance of 1997 , in ascending order of frequency .	SELECT t1.attendance from 1997 as t1 GROUP BY t1.attendance ORDER BY COUNT ( * ) ASC LIMIT 1	2-10362209-2
show all information on the 1997 that has the largest number of attendance.	SELECT * from 1997 as t1 ORDER BY t1.attendance DESC LIMIT 1	2-10362209-2
find the attendance of 1997 which are result 10 but not result 26 .	SELECT t1.attendance from 1997 as t1 WHERE t1.result = 10 EXCEPT SELECT t1.attendance from 1997 as t1 WHERE t1.result = 26	2-10362209-2
which opponent has both 1997 with less than 10 week and 1997 with more than 91 week ?	SELECT t1.opponent from 1997 as t1 WHERE t1.week < 10 INTERSECT SELECT t1.opponent from 1997 as t1 WHERE t1.week > 91	2-10362209-2
list the result which average week is above 10 .	SELECT t1.result from 1997 as t1 GROUP BY t1.result HAVING AVG ( t1.week ) >= 10	2-10362209-2
what is the opponent and opponent of every 1997 that has a week lower than average ?	SELECT t1.opponent , t1.opponent from 1997 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-10362209-2
