what are total wins for each position ?	SELECT t1.position , SUM ( t1.wins ) from randy as t1 GROUP BY t1.position	1-2182562-1
find the number of randy that have more than 10 avg . finish .	SELECT COUNT ( * ) from randy as t1 GROUP BY t1.avg_._finish HAVING COUNT ( * ) > 10 	1-2182562-1
sort the list of position and team ( s ) of all randy in the descending order of top 10 .	SELECT t1.position , t1.team_ from randy as t1 ORDER BY t1.top_10 DESC	1-2182562-1
what are all the position and winnings?	SELECT t1.position , t1.winnings from randy as t1	1-2182562-1
show all information on the randy that has the largest number of avg . start.	SELECT * from randy as t1 ORDER BY t1.avg_._start DESC LIMIT 1	1-2182562-1
what are the avg . start of all randy with team ( s ) that is 10 ?	SELECT t1.avg_._start from randy as t1 GROUP BY t1.team_ HAVING COUNT ( * ) = 10	1-2182562-1
what is the avg . finish of randy with the maximum poles across all randy ?	SELECT t1.avg_._finish from randy as t1 WHERE t1.poles = ( SELECT MAX ( t1.poles ) from randy as t1 )	1-2182562-1
show the avg . start shared by more than 10 randy .	SELECT t1.avg_._start from randy as t1 GROUP BY t1.avg_._start HAVING COUNT ( * ) > 10	1-2182562-1
what is the avg . finish and team ( s ) of the randy with maximum poles ?	SELECT t1.avg_._finish , t1.team_ from randy as t1 WHERE t1.poles = ( SELECT MAX ( t1.poles ) {FROM, 3} )	1-2182562-1
what is all the information on the randy with the largest number of position ?	SELECT * from randy as t1 ORDER BY t1.position DESC LIMIT 1	1-2182562-1
