show the start of len who have at least 10 len .	SELECT t1.start from len as t1 GROUP BY t1.start HAVING COUNT ( * ) >= 10	2-1252129-1
find the qual of len which are in 10 rank but not in 38 rank .	SELECT t1.qual from len as t1 WHERE t1.rank = 10 EXCEPT SELECT t1.qual from len as t1 WHERE t1.rank = 38	2-1252129-1
list all information about len .	SELECT * FROM len	2-1252129-1
how many len are there in year 10 or 77 ?	SELECT COUNT ( * ) from len as t1 WHERE t1.year = 10 OR t1.year = 77	2-1252129-1
find the qual of len whose laps is more than the average laps of len .	SELECT t1.qual from len as t1 WHERE t1.laps > ( SELECT AVG ( t1.laps ) from len as t1	2-1252129-1
what are the rank for len that have an laps greater than the average .	SELECT t1.rank from len as t1 WHERE t1.laps > ( SELECT AVG ( t1.laps ) from len as t1	2-1252129-1
what are the {qual of all the len , and the total laps by each ?	SELECT t1.qual , SUM ( t1.laps ) from len as t1 GROUP BY t1.qual	2-1252129-1
what is the minimum laps in each year ?	SELECT MIN ( t1.laps ) , t1.year from len as t1 GROUP BY t1.year	2-1252129-1
what are the distinct qual with laps between 10 and 70 ?	SELECT DISTINCT t1.qual from len as t1 WHERE t1.laps BETWEEN 10 AND 70	2-1252129-1
sort the list of finish and rank of all len in the descending order of laps .	SELECT t1.finish , t1.rank from len as t1 ORDER BY t1.laps DESC	2-1252129-1
