count the number of football in club 10 or 20 .	SELECT COUNT ( * ) from football as t1 WHERE t1.club = 10 OR t1.club = 20	2-17937080-8
what is the club and season of the football with maximum goals ?	SELECT t1.club , t1.season from football as t1 WHERE t1.goals = ( SELECT MAX ( t1.goals ) {FROM, 3} )	2-17937080-8
find the season of football which have both 10 and 42 as season .	SELECT t1.season from football as t1 WHERE t1.rank = 10 INTERSECT SELECT t1.season from football as t1 WHERE t1.rank = 42	2-17937080-8
return the maximum and minimum goals across all football .	SELECT MAX ( t1.goals ) , MIN ( t1.goals ) from football as t1	2-17937080-8
what are the season of football , sorted by their frequency?	SELECT t1.season from football as t1 GROUP BY t1.season ORDER BY COUNT ( * ) ASC LIMIT 1	2-17937080-8
what are the season , name , and season for each football ?	SELECT t1.season , t1.name , t1.season from football as t1	2-17937080-8
find the club of football who have goals of both 10 and 24 .	SELECT t1.club from football as t1 WHERE t1.goals = 10 INTERSECT SELECT t1.club from football as t1 WHERE t1.goals = 24	2-17937080-8
find the number of football that have more than 10 season .	SELECT COUNT ( * ) from football as t1 GROUP BY t1.season HAVING COUNT ( * ) > 10 	2-17937080-8
what are the name of all football with club that is 10 ?	SELECT t1.name from football as t1 GROUP BY t1.club HAVING COUNT ( * ) = 10	2-17937080-8
which name has both football with less than 10 rank and football with more than 27 rank ?	SELECT t1.name from football as t1 WHERE t1.rank < 10 INTERSECT SELECT t1.name from football as t1 WHERE t1.rank > 27	2-17937080-8
