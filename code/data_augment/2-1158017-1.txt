what are the chassis and engine of derek with 10 or more entrant ?	SELECT t1.chassis , t1.engine from derek as t1 GROUP BY t1.entrant HAVING COUNT ( * ) >= 10	2-1158017-1
what are the maximum and minimum points across all derek ?	SELECT MAX ( t1.points ) , MIN ( t1.points ) from derek as t1	2-1158017-1
what is the entrant of all derek whose points is higher than any derek ?	SELECT t1.entrant from derek as t1 WHERE t1.points > ( SELECT MIN ( t1.points ) from derek as t1 )	2-1158017-1
show the chassis shared by more than 10 derek .	SELECT t1.chassis from derek as t1 GROUP BY t1.chassis HAVING COUNT ( * ) > 10	2-1158017-1
list the chassis and entrant of all derek sorted by points in descending order .	SELECT t1.chassis , t1.entrant from derek as t1 ORDER BY t1.points DESC	2-1158017-1
what are the distinct chassis with year between 10 and 72 ?	SELECT DISTINCT t1.chassis from derek as t1 WHERE t1.year BETWEEN 10 AND 72	2-1158017-1
what is the count of derek with more than 10 engine ?	SELECT COUNT ( * ) from derek as t1 GROUP BY t1.engine HAVING COUNT ( * ) > 10 	2-1158017-1
how many derek are there in chassis 10 or 89 ?	SELECT COUNT ( * ) from derek as t1 WHERE t1.chassis = 10 OR t1.chassis = 89	2-1158017-1
list all entrant which have points higher than the average .	SELECT t1.entrant from derek as t1 WHERE t1.points > ( SELECT AVG ( t1.points ) from derek as t1	2-1158017-1
what are the chassis of all derek that have 10 or more derek ?	SELECT t1.chassis from derek as t1 GROUP BY t1.chassis HAVING COUNT ( * ) >= 10	2-1158017-1
