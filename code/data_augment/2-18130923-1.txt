what is the country of the 1991 with the largest total ?	SELECT t1.country from 1991 as t1 WHERE t1.total = ( SELECT MAX ( t1.total ) from 1991 as t1 )	2-18130923-1
please show the country of the 1991 that have at least 10 records .	SELECT t1.country from 1991 as t1 GROUP BY t1.country HAVING COUNT ( * ) >= 10	2-18130923-1
give the to par that has the most 1991 .	SELECT t1.to_par from 1991 as t1 GROUP BY t1.to_par ORDER BY COUNT ( * ) DESC LIMIT 1	2-18130923-1
return the player of the 1991 that has the fewest corresponding player .	SELECT t1.player from 1991 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-18130923-1
what is the year ( s ) won and to par for the 1991 with the rank 5 smallest total ?	SELECT t1.year_ , t1.to_par from 1991 as t1 ORDER BY t1.total LIMIT 5	2-18130923-1
find the year ( s ) won of 1991 which have both 10 and 74 as year ( s ) won .	SELECT t1.year_ from 1991 as t1 WHERE t1.total = 10 INTERSECT SELECT t1.year_ from 1991 as t1 WHERE t1.total = 74	2-18130923-1
how many 1991 correspond to each finish? show the result in ascending order.	SELECT t1.finish , COUNT ( * ) from 1991 as t1 GROUP BY t1.finish ORDER BY COUNT ( * )	2-18130923-1
find the to par of the 1991 with the highest total .	SELECT t1.to_par from 1991 as t1 ORDER BY t1.total DESC LIMIT 1	2-18130923-1
list the to par which average total is above 10 .	SELECT t1.to_par from 1991 as t1 GROUP BY t1.to_par HAVING AVG ( t1.total ) >= 10	2-18130923-1
give the to par that has the most 1991 .	SELECT t1.to_par from 1991 as t1 GROUP BY t1.to_par ORDER BY COUNT ( * ) DESC LIMIT 1	2-18130923-1
