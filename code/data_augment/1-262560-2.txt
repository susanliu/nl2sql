find the number of mid-states that have more than 10 type .	SELECT COUNT ( * ) from mid-states as t1 GROUP BY t1.type HAVING COUNT ( * ) > 10 	1-262560-2
how many mid-states are there in institution 10 or 19 ?	SELECT COUNT ( * ) from mid-states as t1 WHERE t1.institution = 10 OR t1.institution = 19	1-262560-2
what are the primary conference when joining the msfa that have greater enrollment than any enrollment in mid-states ?	SELECT t1.primary_conference_when_joining_the_msfa from mid-states as t1 WHERE t1.enrollment > ( SELECT MIN ( t1.enrollment ) from mid-states as t1 )	1-262560-2
return the maximum and minimum enrollment across all mid-states .	SELECT MAX ( t1.enrollment ) , MIN ( t1.enrollment ) from mid-states as t1	1-262560-2
find the type of the mid-states with the highest enrollment .	SELECT t1.type from mid-states as t1 ORDER BY t1.enrollment DESC LIMIT 1	1-262560-2
which joined is the most frequent joined?	SELECT t1.joined from mid-states as t1 GROUP BY t1.joined ORDER BY COUNT ( * ) DESC LIMIT 1	1-262560-2
find the institution of mid-states who have founded of both 10 and 1 .	SELECT t1.institution from mid-states as t1 WHERE t1.founded = 10 INTERSECT SELECT t1.institution from mid-states as t1 WHERE t1.founded = 1	1-262560-2
what is the left and nickname of every mid-states that has a founded lower than average ?	SELECT t1.left , t1.nickname from mid-states as t1 WHERE t1.founded < ( SELECT AVG ( t1.founded ) {FROM, 3} )	1-262560-2
what is the minimum enrollment in each type ?	SELECT MIN ( t1.enrollment ) , t1.type from mid-states as t1 GROUP BY t1.type	1-262560-2
give the maximum and minimum founded of all mid-states .	SELECT MAX ( t1.founded ) , MIN ( t1.founded ) from mid-states as t1	1-262560-2
