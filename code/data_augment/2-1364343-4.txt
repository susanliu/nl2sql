return the % of global catholic pop . of the catholic with the fewest catholic .	SELECT t1.% from catholic as t1 ORDER BY t1.catholic ASC LIMIT 1	2-1364343-4
find the number of catholic whose region contain the word 10 .	SELECT COUNT ( * ) from catholic as t1 WHERE t1.region LIKE 10	2-1364343-4
find the region of catholic which are in 10 region but not in 88 region .	SELECT t1.region from catholic as t1 WHERE t1.region = 10 EXCEPT SELECT t1.region from catholic as t1 WHERE t1.region = 88	2-1364343-4
what is the % of global catholic pop . and % of global catholic pop . for the catholic with the rank 5 smallest catholic ?	SELECT t1.% , t1.% from catholic as t1 ORDER BY t1.catholic LIMIT 5	2-1364343-4
show the % of global catholic pop . and the total catholic of catholic .	SELECT t1.% , SUM ( t1.catholic ) from catholic as t1 GROUP BY t1.%	2-1364343-4
show all information on the catholic that has the largest number of % catholic.	SELECT * from catholic as t1 ORDER BY t1.% DESC LIMIT 1	2-1364343-4
please show the different region , ordered by the number of catholic that have each .	SELECT t1.region from catholic as t1 GROUP BY t1.region ORDER BY COUNT ( * ) ASC LIMIT 1	2-1364343-4
what is the minimum catholic in each region ?	SELECT MIN ( t1.catholic ) , t1.region from catholic as t1 GROUP BY t1.region	2-1364343-4
find the distinct % catholic of all catholic that have a higher catholic than some catholic with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.% from catholic as t1 WHERE t1.catholic > ( SELECT MIN ( t1.% ) from catholic as t1 WHERE t1.% = 10 )	2-1364343-4
what are the {region of all the catholic , and the total total population by each ?	SELECT t1.region , SUM ( t1.total_population ) from catholic as t1 GROUP BY t1.region	2-1364343-4
