select the average enrollment of each indiana 's school .	SELECT AVG ( t1.enrollment ) , t1.school from indiana as t1 GROUP BY t1.school	2-13986492-6
which ihsaa class have an average enrollment over 10 ?	SELECT t1.ihsaa_class from indiana as t1 GROUP BY t1.ihsaa_class HAVING AVG ( t1.enrollment ) >= 10	2-13986492-6
how many indiana are there in mascot 10 or 60 ?	SELECT COUNT ( * ) from indiana as t1 WHERE t1.mascot = 10 OR t1.mascot = 60	2-13986492-6
what are the distinct county with enrollment between 10 and 5 ?	SELECT DISTINCT t1.county from indiana as t1 WHERE t1.enrollment BETWEEN 10 AND 5	2-13986492-6
what is the county of indiana with the maximum enrollment across all indiana ?	SELECT t1.county from indiana as t1 WHERE t1.enrollment = ( SELECT MAX ( t1.enrollment ) from indiana as t1 )	2-13986492-6
what is the ihsaa class of the indiana with least number of ihsaa class ?	SELECT t1.ihsaa_class from indiana as t1 GROUP BY t1.ihsaa_class ORDER BY COUNT ( * ) ASC LIMIT 1	2-13986492-6
what is the mascot of the indiana with the largest enrollment ?	SELECT t1.mascot from indiana as t1 WHERE t1.enrollment = ( SELECT MAX ( t1.enrollment ) from indiana as t1 )	2-13986492-6
return the smallest enrollment for every county .	SELECT MIN ( t1.enrollment ) , t1.county from indiana as t1 GROUP BY t1.county	2-13986492-6
show the school and the total enrollment of indiana .	SELECT t1.school , SUM ( t1.enrollment ) from indiana as t1 GROUP BY t1.school	2-13986492-6
return the mascot of the largest enrollment.	SELECT t1.mascot from indiana as t1 ORDER BY t1.enrollment DESC LIMIT 1	2-13986492-6
