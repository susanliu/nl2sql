what are the grades and grades ?	SELECT t1.grades , t1.grades from rocky as t1	2-129118-2
return the school of the rocky with the fewest enrollment .	SELECT t1.school from rocky as t1 ORDER BY t1.enrollment ASC LIMIT 1	2-129118-2
please show the affiliation of the rocky that have at least 10 records .	SELECT t1.affiliation from rocky as t1 GROUP BY t1.affiliation HAVING COUNT ( * ) >= 10	2-129118-2
which grades has most number of rocky ?	SELECT t1.grades from rocky as t1 GROUP BY t1.grades ORDER BY COUNT ( * ) DESC LIMIT 1	2-129118-2
which t1.grades has the fewest rocky ?	SELECT t1.grades from rocky as t1 GROUP BY t1.grades ORDER BY COUNT ( * ) LIMIT 1	2-129118-2
what are the student body and affiliation of the {COLUMN} who have enrollment above five or enrollment below ten ?	SELECT t1.student_body , t1.affiliation from rocky as t1 WHERE t1.enrollment > 5 OR t1.enrollment < 10	2-129118-2
return the grades of rocky that do not have the school 10 .	SELECT t1.grades from rocky as t1 WHERE t1.school ! = 10	2-129118-2
return the different affiliation of rocky , in ascending order of frequency .	SELECT t1.affiliation from rocky as t1 GROUP BY t1.affiliation ORDER BY COUNT ( * ) ASC LIMIT 1	2-129118-2
what are the school , grades , and affiliation of each rocky ?	SELECT t1.school , t1.grades , t1.affiliation from rocky as t1	2-129118-2
what is the minimum enrollment in each grades ?	SELECT MIN ( t1.enrollment ) , t1.grades from rocky as t1 GROUP BY t1.grades	2-129118-2
