find the event and res . of the rob whose round is lower than the average round of all rob .	SELECT t1.event , t1.res_ from rob as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-12614827-2
show the event and the number of unique opponent containing each event .	SELECT t1.event , COUNT ( DISTINCT t1.opponent ) from rob as t1 GROUP BY t1.event	2-12614827-2
what is the opponent of the rob with the minimum round ?	SELECT t1.opponent from rob as t1 ORDER BY t1.round ASC LIMIT 1	2-12614827-2
show all event and corresponding number of rob sorted by the count .	SELECT t1.event , COUNT ( * ) from rob as t1 GROUP BY t1.event ORDER BY COUNT ( * )	2-12614827-2
what is the opponent and opponent for the rob with the rank 5 smallest round ?	SELECT t1.opponent , t1.opponent from rob as t1 ORDER BY t1.round LIMIT 5	2-12614827-2
find the location that have 10 rob .	SELECT t1.location from rob as t1 GROUP BY t1.location HAVING COUNT ( * ) = 10	2-12614827-2
what are the distinct opponent with round between 10 and 77 ?	SELECT DISTINCT t1.opponent from rob as t1 WHERE t1.round BETWEEN 10 AND 77	2-12614827-2
find the method of the rob which have res . 10 but not 97 .	SELECT t1.method from rob as t1 WHERE t1.res_ = 10 EXCEPT SELECT t1.method from rob as t1 WHERE t1.res_ = 97	2-12614827-2
which method has both rob with less than 10 round and rob with more than 45 round ?	SELECT t1.method from rob as t1 WHERE t1.round < 10 INTERSECT SELECT t1.method from rob as t1 WHERE t1.round > 45	2-12614827-2
what are the average round of rob , grouped by res . ?	SELECT AVG ( t1.round ) , t1.res_ from rob as t1 GROUP BY t1.res_	2-12614827-2
