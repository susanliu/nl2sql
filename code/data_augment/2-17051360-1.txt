show the season of the michael that has the greatest number of michael .	SELECT t1.season from michael as t1 GROUP BY t1.season ORDER BY COUNT ( * ) DESC LIMIT 1	2-17051360-1
list the final placing and season of all michael sorted by poles in descending order .	SELECT t1.final_placing , t1.season from michael as t1 ORDER BY t1.poles DESC	2-17051360-1
show points for all michael whose poles are greater than the average .	SELECT t1.points from michael as t1 WHERE t1.poles > ( SELECT AVG ( t1.poles ) from michael as t1	2-17051360-1
what are the series of michael whose season is not 10 ?	SELECT t1.series from michael as t1 WHERE t1.season ! = 10	2-17051360-1
which final placing has both michael with less than 10 races and michael with more than 61 races ?	SELECT t1.final_placing from michael as t1 WHERE t1.races < 10 INTERSECT SELECT t1.final_placing from michael as t1 WHERE t1.races > 61	2-17051360-1
which points have an average wins over 10 ?	SELECT t1.points from michael as t1 GROUP BY t1.points HAVING AVG ( t1.wins ) >= 10	2-17051360-1
find the number of michael that have more than 10 season .	SELECT COUNT ( * ) from michael as t1 GROUP BY t1.season HAVING COUNT ( * ) > 10 	2-17051360-1
find the distinct COLUMN_NAME,0} of all michael that have wins higher than some michael from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.points from michael as t1 WHERE t1.wins > ( SELECT MIN ( t1.points ) from michael as t1 WHERE t1.points = 10 )	2-17051360-1
show all information on the michael that has the largest number of final placing.	SELECT * from michael as t1 ORDER BY t1.final_placing DESC LIMIT 1	2-17051360-1
show the points and series with at least 10 points .	SELECT t1.points , t1.series from michael as t1 GROUP BY t1.points HAVING COUNT ( * ) >= 10	2-17051360-1
