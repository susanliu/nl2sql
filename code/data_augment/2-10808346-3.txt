what is the away team score and home team of the 1943 with the top 5 smallest crowd ?	SELECT t1.away_team_score , t1.home_team from 1943 as t1 ORDER BY t1.crowd LIMIT 5	2-10808346-3
return the home team and away team of 1943 with the five lowest crowd .	SELECT t1.home_team , t1.away_team from 1943 as t1 ORDER BY t1.crowd LIMIT 5	2-10808346-3
what is the venue and home team score of the 1943 with maximum crowd ?	SELECT t1.venue , t1.home_team_score from 1943 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10808346-3
which t1.home_team_score has the fewest 1943 ?	SELECT t1.home_team_score from 1943 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10808346-3
return the home team score of the largest crowd.	SELECT t1.home_team_score from 1943 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10808346-3
what is the count of 1943 with more than 10 date ?	SELECT COUNT ( * ) from 1943 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10 	2-10808346-3
what is the home team score and date of the 1943 with the top 5 smallest crowd ?	SELECT t1.home_team_score , t1.date from 1943 as t1 ORDER BY t1.crowd LIMIT 5	2-10808346-3
what is the away team score of the most common 1943 in all away team score ?	SELECT t1.away_team_score from 1943 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-10808346-3
what is the home team of all 1943 whose crowd is higher than any 1943 ?	SELECT t1.home_team from 1943 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1943 as t1 )	2-10808346-3
show the home team and the corresponding number of 1943 sorted by the number of home team in ascending order .	SELECT t1.home_team , COUNT ( * ) from 1943 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * )	2-10808346-3
