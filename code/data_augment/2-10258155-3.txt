show the nation and nation with at least 10 nation .	SELECT t1.nation , t1.nation from athletics as t1 GROUP BY t1.nation HAVING COUNT ( * ) >= 10	2-10258155-3
find the nation for all athletics who have more than the average bronze .	SELECT t1.nation from athletics as t1 WHERE t1.bronze > ( SELECT AVG ( t1.bronze ) from athletics as t1	2-10258155-3
select the average silver of each athletics 's nation .	SELECT AVG ( t1.silver ) , t1.nation from athletics as t1 GROUP BY t1.nation	2-10258155-3
find the nation and nation of the athletics with at least 10 nation .	SELECT t1.nation , t1.nation from athletics as t1 GROUP BY t1.nation HAVING COUNT ( * ) >= 10	2-10258155-3
what are the nation of athletics with gold greater than the average of all athletics ?	SELECT t1.nation from athletics as t1 WHERE t1.gold > ( SELECT AVG ( t1.gold ) from athletics as t1	2-10258155-3
which nation have an average bronze over 10 ?	SELECT t1.nation from athletics as t1 GROUP BY t1.nation HAVING AVG ( t1.bronze ) >= 10	2-10258155-3
what is the nation of highest total ?	SELECT t1.nation from athletics as t1 ORDER BY t1.total DESC LIMIT 1	2-10258155-3
how many athletics are there that have more than {VALUE},0 nation ?	SELECT COUNT ( * ) from athletics as t1 GROUP BY t1.nation HAVING COUNT ( * ) > 10 	2-10258155-3
what are the nation and nation of the {COLUMN} who have silver above five or gold below ten ?	SELECT t1.nation , t1.nation from athletics as t1 WHERE t1.silver > 5 OR t1.gold < 10	2-10258155-3
what is the nation of athletics with the maximum total across all athletics ?	SELECT t1.nation from athletics as t1 WHERE t1.total = ( SELECT MAX ( t1.total ) from athletics as t1 )	2-10258155-3
