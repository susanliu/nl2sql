which height m ( ft ) has the least frequency mhz ?	SELECT t1.height_m_ from mars as t1 ORDER BY t1.frequency_mhz ASC LIMIT 1	2-12454334-2
what is the city of license of mars with the maximum frequency mhz across all mars ?	SELECT t1.city_of_license from mars as t1 WHERE t1.frequency_mhz = ( SELECT MAX ( t1.frequency_mhz ) from mars as t1 )	2-12454334-2
give the t1.class with the fewest mars .	SELECT t1.class from mars as t1 GROUP BY t1.class ORDER BY COUNT ( * ) LIMIT 1	2-12454334-2
what are the call sign of all mars with fcc info that is 10 ?	SELECT t1.call_sign from mars as t1 GROUP BY t1.fcc_info HAVING COUNT ( * ) = 10	2-12454334-2
what are the call sign of all mars that have 10 or more mars ?	SELECT t1.call_sign from mars as t1 GROUP BY t1.call_sign HAVING COUNT ( * ) >= 10	2-12454334-2
what is the maximum and mininum frequency mhz {COLUMN} for all mars ?	SELECT MAX ( t1.frequency_mhz ) , MIN ( t1.frequency_mhz ) from mars as t1	2-12454334-2
what is all the information on the mars with the largest number of call sign ?	SELECT * from mars as t1 ORDER BY t1.call_sign DESC LIMIT 1	2-12454334-2
what are the height m ( ft ) of mars , sorted by their frequency?	SELECT t1.height_m_ from mars as t1 GROUP BY t1.height_m_ ORDER BY COUNT ( * ) ASC LIMIT 1	2-12454334-2
return the fcc info of the mars that has the fewest corresponding fcc info .	SELECT t1.fcc_info from mars as t1 GROUP BY t1.fcc_info ORDER BY COUNT ( * ) ASC LIMIT 1	2-12454334-2
list the fcc info which average frequency mhz is above 10 .	SELECT t1.fcc_info from mars as t1 GROUP BY t1.fcc_info HAVING AVG ( t1.frequency_mhz ) >= 10	2-12454334-2
