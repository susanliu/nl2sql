show all information on the list that has the largest number of 2010 population density.	SELECT * from list as t1 ORDER BY t1.2010_population_density DESC LIMIT 1	2-1649321-4
list the location and municipio of all list sorted by 2012 rank in descending order .	SELECT t1.location , t1.municipio from list as t1 ORDER BY t1.2012_rank DESC	2-1649321-4
what are the location and 2010 population density of the {COLUMN} who have ansi above five or 2012 rank below ten ?	SELECT t1.location , t1.2010_population_density from list as t1 WHERE t1.ansi > 5 OR t1.2012_rank < 10	2-1649321-4
which 2010 land area has both list with less than 10 2012 rank and list with more than 43 2012 rank ?	SELECT t1.2010_land_area from list as t1 WHERE t1.2012_rank < 10 INTERSECT SELECT t1.2010_land_area from list as t1 WHERE t1.2012_rank > 43	2-1649321-4
list the 2010 population density of the list whose location is not 10 .	SELECT t1.2010_population_density from list as t1 WHERE t1.location ! = 10	2-1649321-4
list the location which average 2012 rank is above 10 .	SELECT t1.location from list as t1 GROUP BY t1.location HAVING AVG ( t1.2012_rank ) >= 10	2-1649321-4
show the 2010 population density of the list that has the most list .	SELECT t1.2010_population_density from list as t1 GROUP BY t1.2010_population_density ORDER BY COUNT ( * ) DESC LIMIT 1	2-1649321-4
find the distinct 2010 population density of list having ansi between 10 and 78 .	SELECT DISTINCT t1.2010_population_density from list as t1 WHERE t1.ansi BETWEEN 10 AND 78	2-1649321-4
find the location of the list that is most frequent across all location .	SELECT t1.location from list as t1 GROUP BY t1.location ORDER BY COUNT ( * ) DESC LIMIT 1	2-1649321-4
which location has both list with less than 10 ansi and list with more than 74 ansi ?	SELECT t1.location from list as t1 WHERE t1.ansi < 10 INTERSECT SELECT t1.location from list as t1 WHERE t1.ansi > 74	2-1649321-4
