what is the up/down of all 2001 whose lowest is higher than any 2001 ?	SELECT t1.up/down from 2001 as t1 WHERE t1.lowest > ( SELECT MIN ( t1.lowest ) from 2001 as t1 )	2-1161065-27
what are the team for all 2001 , and what is the total {average for each ?	SELECT t1.team , SUM ( t1.average ) from 2001 as t1 GROUP BY t1.team	2-1161065-27
what are the maximum and minimum lowest across all 2001 ?	SELECT MAX ( t1.lowest ) , MIN ( t1.lowest ) from 2001 as t1	2-1161065-27
find the team of 2001 who have both 10 and 56 average .	SELECT t1.team from 2001 as t1 WHERE t1.average = 10 INTERSECT SELECT t1.team from 2001 as t1 WHERE t1.average = 56	2-1161065-27
return the team of the 2001 with the fewest total .	SELECT t1.team from 2001 as t1 ORDER BY t1.total ASC LIMIT 1	2-1161065-27
what is the up/down of the 2001 with the largest lowest ?	SELECT t1.up/down from 2001 as t1 WHERE t1.lowest = ( SELECT MAX ( t1.lowest ) from 2001 as t1 )	2-1161065-27
list team and up/down who have last year greater than 5 or last year shorter than 10 .	SELECT t1.team , t1.up/down from 2001 as t1 WHERE t1.last_year > 5 OR t1.last_year < 10	2-1161065-27
show all up/down and corresponding number of 2001 in the ascending order of the numbers.	SELECT t1.up/down , COUNT ( * ) from 2001 as t1 GROUP BY t1.up/down ORDER BY COUNT ( * )	2-1161065-27
what are the up/down and up/down of 2001 with 10 or more up/down ?	SELECT t1.up/down , t1.up/down from 2001 as t1 GROUP BY t1.up/down HAVING COUNT ( * ) >= 10	2-1161065-27
return the maximum and minimum average across all 2001 .	SELECT MAX ( t1.average ) , MIN ( t1.average ) from 2001 as t1	2-1161065-27
