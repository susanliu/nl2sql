which party have an average first elected over 10 ?	SELECT t1.party from united as t1 GROUP BY t1.party HAVING AVG ( t1.first_elected ) >= 10	1-1341472-49
return the result of united for which the result is not 10 ?	SELECT t1.result from united as t1 WHERE t1.result ! = 10	1-1341472-49
which candidates have an average first elected over 10 ?	SELECT t1.candidates from united as t1 GROUP BY t1.candidates HAVING AVG ( t1.first_elected ) >= 10	1-1341472-49
what is the district of the united with the smallest first elected ?	SELECT t1.district from united as t1 ORDER BY t1.first_elected ASC LIMIT 1	1-1341472-49
find the distinct candidates of all united that have a higher first elected than some united with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.candidates from united as t1 WHERE t1.first_elected > ( SELECT MIN ( t1.candidates ) from united as t1 WHERE t1.district = 10 )	1-1341472-49
list the candidates which average first elected is above 10 .	SELECT t1.candidates from united as t1 GROUP BY t1.candidates HAVING AVG ( t1.first_elected ) >= 10	1-1341472-49
show the candidates of the united that has the greatest number of united .	SELECT t1.candidates from united as t1 GROUP BY t1.candidates ORDER BY COUNT ( * ) DESC LIMIT 1	1-1341472-49
count the number of united that have an party containing 10 .	SELECT COUNT ( * ) from united as t1 WHERE t1.party LIKE 10	1-1341472-49
find the district of the united which have result 10 but not 29 .	SELECT t1.district from united as t1 WHERE t1.result = 10 EXCEPT SELECT t1.district from united as t1 WHERE t1.result = 29	1-1341472-49
find the party for all united who have more than the average first elected .	SELECT t1.party from united as t1 WHERE t1.first_elected > ( SELECT AVG ( t1.first_elected ) from united as t1	1-1341472-49
