what is the result of 2000 with the maximum week across all 2000 ?	SELECT t1.result from 2000 as t1 WHERE t1.week = ( SELECT MAX ( t1.week ) from 2000 as t1 )	2-16025613-2
how many 2000 are there in result 10 or 21 ?	SELECT COUNT ( * ) from 2000 as t1 WHERE t1.result = 10 OR t1.result = 21	2-16025613-2
which result has both 2000 with less than 10 week and 2000 with more than 47 week ?	SELECT t1.result from 2000 as t1 WHERE t1.week < 10 INTERSECT SELECT t1.result from 2000 as t1 WHERE t1.week > 47	2-16025613-2
list the opponent and opponent of all 2000 sorted by week in descending order .	SELECT t1.opponent , t1.opponent from 2000 as t1 ORDER BY t1.week DESC	2-16025613-2
return the result and opponent of 2000 with the five lowest week .	SELECT t1.result , t1.opponent from 2000 as t1 ORDER BY t1.week LIMIT 5	2-16025613-2
what is the tv time and date of the 2000 with maximum week ?	SELECT t1.tv_time , t1.date from 2000 as t1 WHERE t1.week = ( SELECT MAX ( t1.week ) {FROM, 3} )	2-16025613-2
find the distinct result of all 2000 that have a higher week than some 2000 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.result from 2000 as t1 WHERE t1.week > ( SELECT MIN ( t1.result ) from 2000 as t1 WHERE t1.date = 10 )	2-16025613-2
what are the tv time for 2000 who have more than the average week?	SELECT t1.tv_time from 2000 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 2000 as t1	2-16025613-2
return the smallest week for every result .	SELECT MIN ( t1.week ) , t1.result from 2000 as t1 GROUP BY t1.result	2-16025613-2
what is the result of the most common 2000 in all result ?	SELECT t1.result from 2000 as t1 GROUP BY t1.result ORDER BY COUNT ( * ) DESC LIMIT 1	2-16025613-2
