return each vice-chancellor with the number of 1994 in ascending order of the number of vice-chancellor .	SELECT t1.vice-chancellor , COUNT ( * ) from 1994 as t1 GROUP BY t1.vice-chancellor ORDER BY COUNT ( * )	2-142950-1
list the institution , vice-chancellor and the institution of the 1994 .	SELECT t1.institution , t1.vice-chancellor , t1.institution from 1994 as t1	2-142950-1
what is the count of 1994 with more than 10 vice-chancellor ?	SELECT COUNT ( * ) from 1994 as t1 GROUP BY t1.vice-chancellor HAVING COUNT ( * ) > 10 	2-142950-1
list all information about 1994 .	SELECT * FROM 1994	2-142950-1
return each institution with the number of 1994 in ascending order of the number of institution .	SELECT t1.institution , COUNT ( * ) from 1994 as t1 GROUP BY t1.institution ORDER BY COUNT ( * )	2-142950-1
what are the distinct location with research funding ( £ ,000 ) between 10 and 72 ?	SELECT DISTINCT t1.location from 1994 as t1 WHERE t1.research_funding_ BETWEEN 10 AND 72	2-142950-1
find the vice-chancellor of the 1994 with the highest research funding ( £ ,000 ) .	SELECT t1.vice-chancellor from 1994 as t1 ORDER BY t1.research_funding_ DESC LIMIT 1	2-142950-1
find the vice-chancellor of 1994 who have total number of students of both 10 and 36 .	SELECT t1.vice-chancellor from 1994 as t1 WHERE t1.total_number_of_students = 10 INTERSECT SELECT t1.vice-chancellor from 1994 as t1 WHERE t1.total_number_of_students = 36	2-142950-1
what is the vice-chancellor of the 1994 with the largest total number of students ?	SELECT t1.vice-chancellor from 1994 as t1 WHERE t1.total_number_of_students = ( SELECT MAX ( t1.total_number_of_students ) from 1994 as t1 )	2-142950-1
what is the institution and vice-chancellor of the 1994 with maximum established ?	SELECT t1.institution , t1.vice-chancellor from 1994 as t1 WHERE t1.established = ( SELECT MAX ( t1.established ) {FROM, 3} )	2-142950-1
