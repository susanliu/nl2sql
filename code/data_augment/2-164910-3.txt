what is the partner and surface of every serena that has a year lower than average ?	SELECT t1.partner , t1.surface from serena as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-164910-3
what is the partner of serena with the maximum year across all serena ?	SELECT t1.partner from serena as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from serena as t1 )	2-164910-3
what is the count of serena with more than 10 championship ?	SELECT COUNT ( * ) from serena as t1 GROUP BY t1.championship HAVING COUNT ( * ) > 10 	2-164910-3
what is the partner and outcome of the serena with the top 5 smallest year ?	SELECT t1.partner , t1.outcome from serena as t1 ORDER BY t1.year LIMIT 5	2-164910-3
what is the championship and partner for the serena with the rank 5 smallest year ?	SELECT t1.championship , t1.partner from serena as t1 ORDER BY t1.year LIMIT 5	2-164910-3
how many serena are there that have more than {VALUE},0 outcome ?	SELECT COUNT ( * ) from serena as t1 GROUP BY t1.outcome HAVING COUNT ( * ) > 10 	2-164910-3
how many distinct outcome correspond to each championship ?	SELECT t1.championship , COUNT ( DISTINCT t1.outcome ) from serena as t1 GROUP BY t1.championship	2-164910-3
return the maximum and minimum year across all serena .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from serena as t1	2-164910-3
find the opponents of serena which have 10 but no 60 as partner .	SELECT t1.opponents from serena as t1 WHERE t1.partner = 10 EXCEPT SELECT t1.opponents from serena as t1 WHERE t1.partner = 60	2-164910-3
what are the surface for serena that have an year greater than the average .	SELECT t1.surface from serena as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from serena as t1	2-164910-3
