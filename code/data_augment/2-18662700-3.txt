show the time , country , and notes of all the rowing .	SELECT t1.time , t1.country , t1.notes from rowing as t1	2-18662700-3
find the notes of rowing who have rank of both 10 and 90 .	SELECT t1.notes from rowing as t1 WHERE t1.rank = 10 INTERSECT SELECT t1.notes from rowing as t1 WHERE t1.rank = 90	2-18662700-3
show time and the number of distinct notes in each time .	SELECT t1.time , COUNT ( DISTINCT t1.notes ) from rowing as t1 GROUP BY t1.time	2-18662700-3
which notes have less than 3 in rowing ?	SELECT t1.notes from rowing as t1 GROUP BY t1.notes HAVING COUNT ( * ) < 3	2-18662700-3
give the maximum and minimum rank of all rowing .	SELECT MAX ( t1.rank ) , MIN ( t1.rank ) from rowing as t1	2-18662700-3
how many rowing have rowers that contain the word 10 ?	SELECT COUNT ( * ) from rowing as t1 WHERE t1.rowers LIKE 10	2-18662700-3
find the notes of the rowing who has the largest number of rowing .	SELECT t1.notes from rowing as t1 GROUP BY t1.notes ORDER BY COUNT ( * ) DESC LIMIT 1	2-18662700-3
find the distinct rowers of rowing having rank between 10 and 3 .	SELECT DISTINCT t1.rowers from rowing as t1 WHERE t1.rank BETWEEN 10 AND 3	2-18662700-3
find the rowers of the rowing that have just 10 rowing .	SELECT t1.rowers from rowing as t1 GROUP BY t1.rowers HAVING COUNT ( * ) = 10	2-18662700-3
how many rowing are there for each rowers ? list the smallest count first .	SELECT t1.rowers , COUNT ( * ) from rowing as t1 GROUP BY t1.rowers ORDER BY COUNT ( * )	2-18662700-3
