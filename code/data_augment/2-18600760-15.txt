show the township and the number of unique county containing each township .	SELECT t1.township , COUNT ( DISTINCT t1.county ) from list as t1 GROUP BY t1.township	2-18600760-15
what are the county that have greater longitude than any longitude in list ?	SELECT t1.county from list as t1 WHERE t1.longitude > ( SELECT MIN ( t1.longitude ) from list as t1 )	2-18600760-15
what are the distinct township with geo id between 10 and 31 ?	SELECT DISTINCT t1.township from list as t1 WHERE t1.geo_id BETWEEN 10 AND 31	2-18600760-15
what is the county and township of every list that has a ansi code lower than average ?	SELECT t1.county , t1.township from list as t1 WHERE t1.ansi_code < ( SELECT AVG ( t1.ansi_code ) {FROM, 3} )	2-18600760-15
what are the county that have greater geo id than any geo id in list ?	SELECT t1.county from list as t1 WHERE t1.geo_id > ( SELECT MIN ( t1.geo_id ) from list as t1 )	2-18600760-15
list all township which have water ( sqmi ) higher than the average .	SELECT t1.township from list as t1 WHERE t1.water_ > ( SELECT AVG ( t1.water_ ) from list as t1	2-18600760-15
find the county and township of the list whose pop . ( 2010 ) is lower than the average pop . ( 2010 ) of all list .	SELECT t1.county , t1.township from list as t1 WHERE t1.pop_._ < ( SELECT AVG ( t1.pop_._ ) {FROM, 3} )	2-18600760-15
what are the county of all list with county that is 10 ?	SELECT t1.county from list as t1 GROUP BY t1.county HAVING COUNT ( * ) = 10	2-18600760-15
what are all the county and township?	SELECT t1.county , t1.township from list as t1	2-18600760-15
return the maximum and minimum water ( sqmi ) across all list .	SELECT MAX ( t1.water_ ) , MIN ( t1.water_ ) from list as t1	2-18600760-15
