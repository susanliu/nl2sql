list all information about none .	SELECT * FROM none	1-2562572-46
how many none are there that have more than {VALUE},0 largest ethnic group ( 2002 ) ?	SELECT COUNT ( * ) from none as t1 GROUP BY t1.largest_ethnic_group_ HAVING COUNT ( * ) > 10 	1-2562572-46
which type has both none with less than 10 population ( 2011 ) and none with more than 74 population ( 2011 ) ?	SELECT t1.type from none as t1 WHERE t1.population_ < 10 INTERSECT SELECT t1.type from none as t1 WHERE t1.population_ > 74	1-2562572-46
please show the cyrillic name other names of the none that have at least 10 records .	SELECT t1.cyrillic_name_other_names from none as t1 GROUP BY t1.cyrillic_name_other_names HAVING COUNT ( * ) >= 10	1-2562572-46
what are the distinct dominant religion ( 2002 ) with population ( 2011 ) between 10 and 92 ?	SELECT DISTINCT t1.dominant_religion_ from none as t1 WHERE t1.population_ BETWEEN 10 AND 92	1-2562572-46
select the average population ( 2011 ) of each none 's cyrillic name other names .	SELECT AVG ( t1.population_ ) , t1.cyrillic_name_other_names from none as t1 GROUP BY t1.cyrillic_name_other_names	1-2562572-46
what is the type of the none with the smallest population ( 2011 ) ?	SELECT t1.type from none as t1 ORDER BY t1.population_ ASC LIMIT 1	1-2562572-46
what are the settlement with exactly 10 none ?	SELECT t1.settlement from none as t1 GROUP BY t1.settlement HAVING COUNT ( * ) = 10	1-2562572-46
what are the largest ethnic group ( 2002 ) and largest ethnic group ( 2002 ) of the {COLUMN} who have population ( 2011 ) above five or population ( 2011 ) below ten ?	SELECT t1.largest_ethnic_group_ , t1.largest_ethnic_group_ from none as t1 WHERE t1.population_ > 5 OR t1.population_ < 10	1-2562572-46
return the different cyrillic name other names of none , in ascending order of frequency .	SELECT t1.cyrillic_name_other_names from none as t1 GROUP BY t1.cyrillic_name_other_names ORDER BY COUNT ( * ) ASC LIMIT 1	1-2562572-46
