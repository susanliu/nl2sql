which district has the least s barangay ?	SELECT t1.district from manila as t1 ORDER BY t1.s_barangay ASC LIMIT 1	2-184334-2
show district and the number of distinct district in each district .	SELECT t1.district , COUNT ( DISTINCT t1.district ) from manila as t1 GROUP BY t1.district	2-184334-2
count the number of manila in district 10 or 63 .	SELECT COUNT ( * ) from manila as t1 WHERE t1.district = 10 OR t1.district = 63	2-184334-2
list the district which average pop . density ( per km2 ) is above 10 .	SELECT t1.district from manila as t1 GROUP BY t1.district HAVING AVG ( t1.pop_._density_ ) >= 10	2-184334-2
find the distinct district of manila having area ( has . ) between 10 and 53 .	SELECT DISTINCT t1.district from manila as t1 WHERE t1.area_ BETWEEN 10 AND 53	2-184334-2
find the district of the manila with the largest population ( 2010 census ) .	SELECT t1.district from manila as t1 ORDER BY t1.population_ DESC LIMIT 1	2-184334-2
find the district of manila which have 10 but no 49 as district .	SELECT t1.district from manila as t1 WHERE t1.district = 10 EXCEPT SELECT t1.district from manila as t1 WHERE t1.district = 49	2-184334-2
what is the district of the manila with least number of district ?	SELECT t1.district from manila as t1 GROUP BY t1.district ORDER BY COUNT ( * ) ASC LIMIT 1	2-184334-2
find the distinct district of manila having population ( 2010 census ) between 10 and 8 .	SELECT DISTINCT t1.district from manila as t1 WHERE t1.population_ BETWEEN 10 AND 8	2-184334-2
list the district which average s barangay is above 10 .	SELECT t1.district from manila as t1 GROUP BY t1.district HAVING AVG ( t1.s_barangay ) >= 10	2-184334-2
