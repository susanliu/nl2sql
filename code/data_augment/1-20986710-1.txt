what is the minimum series year in each score in final ?	SELECT MIN ( t1.series_year ) , t1.score_in_final from toyota as t1 GROUP BY t1.score_in_final	1-20986710-1
what are the surface and champion ?	SELECT t1.surface , t1.champion from toyota as t1	1-20986710-1
what is the runner-up and series-ending tournament of the toyota with the top 5 smallest series year ?	SELECT t1.runner-up , t1.series-ending_tournament from toyota as t1 ORDER BY t1.series_year LIMIT 5	1-20986710-1
list champion of toyota that have the number of toyota greater than 10 .	SELECT t1.champion from toyota as t1 GROUP BY t1.champion HAVING COUNT ( * ) > 10	1-20986710-1
what is the champion of the toyota with the largest series year ?	SELECT t1.champion from toyota as t1 WHERE t1.series_year = ( SELECT MAX ( t1.series_year ) from toyota as t1 )	1-20986710-1
please show the location of the toyota that have at least 10 records .	SELECT t1.location from toyota as t1 GROUP BY t1.location HAVING COUNT ( * ) >= 10	1-20986710-1
return the smallest series year for every champion .	SELECT MIN ( t1.series_year ) , t1.champion from toyota as t1 GROUP BY t1.champion	1-20986710-1
what are the champion and score in final of each toyota , listed in descending order by series year ?	SELECT t1.champion , t1.score_in_final from toyota as t1 ORDER BY t1.series_year DESC	1-20986710-1
how many toyota correspond to each location? show the result in ascending order.	SELECT t1.location , COUNT ( * ) from toyota as t1 GROUP BY t1.location ORDER BY COUNT ( * )	1-20986710-1
what are the score in final more than 10 toyota have ?	SELECT t1.score_in_final from toyota as t1 GROUP BY t1.score_in_final HAVING COUNT ( * ) > 10	1-20986710-1
