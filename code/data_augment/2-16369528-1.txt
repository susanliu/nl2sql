return the best teenage/young adult and best teenage/young adult of ned with the five lowest year .	SELECT t1.best_teenage/young_adult , t1.best_teenage/young_adult from ned as t1 ORDER BY t1.year LIMIT 5	2-16369528-1
list best non-fiction and lifetime achievement who have year greater than 5 or year shorter than 10 .	SELECT t1.best_non-fiction , t1.lifetime_achievement from ned as t1 WHERE t1.year > 5 OR t1.year < 10	2-16369528-1
show all information on the ned that has the largest number of best non-fiction.	SELECT * from ned as t1 ORDER BY t1.best_non-fiction DESC LIMIT 1	2-16369528-1
what is the best non-fiction of the ned with the smallest year ?	SELECT t1.best_non-fiction from ned as t1 ORDER BY t1.year ASC LIMIT 1	2-16369528-1
what is the best non-fiction of the ned with the largest year ?	SELECT t1.best_non-fiction from ned as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from ned as t1 )	2-16369528-1
what are the lifetime achievement more than 10 ned have ?	SELECT t1.lifetime_achievement from ned as t1 GROUP BY t1.lifetime_achievement HAVING COUNT ( * ) > 10	2-16369528-1
find the distinct COLUMN_NAME,0} of all ned that have year higher than some ned from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.reader_'s_vote from ned as t1 WHERE t1.year > ( SELECT MIN ( t1.reader_'s_vote ) from ned as t1 WHERE t1.lifetime_achievement = 10 )	2-16369528-1
show all lifetime achievement and corresponding number of ned in the ascending order of the numbers.	SELECT t1.lifetime_achievement , COUNT ( * ) from ned as t1 GROUP BY t1.lifetime_achievement ORDER BY COUNT ( * )	2-16369528-1
give the best non-fiction that has the most ned .	SELECT t1.best_non-fiction from ned as t1 GROUP BY t1.best_non-fiction ORDER BY COUNT ( * ) DESC LIMIT 1	2-16369528-1
what are the best teenage/young adult of all ned with reader 's vote that is 10 ?	SELECT t1.best_teenage/young_adult from ned as t1 GROUP BY t1.reader_'s_vote HAVING COUNT ( * ) = 10	2-16369528-1
