what is the player of highest pick # ?	SELECT t1.player from list as t1 ORDER BY t1.pick_ DESC LIMIT 1	2-11636955-38
what are the team ( league ) of list , sorted by their frequency?	SELECT t1.team_ from list as t1 GROUP BY t1.team_ ORDER BY COUNT ( * ) ASC LIMIT 1	2-11636955-38
what are total reg gp for each player ?	SELECT t1.player , SUM ( t1.reg_gp ) from list as t1 GROUP BY t1.player	2-11636955-38
count the number of list in team ( league ) 10 or 65 .	SELECT COUNT ( * ) from list as t1 WHERE t1.team_ = 10 OR t1.team_ = 65	2-11636955-38
what is the team ( league ) of the list with least number of team ( league ) ?	SELECT t1.team_ from list as t1 GROUP BY t1.team_ ORDER BY COUNT ( * ) ASC LIMIT 1	2-11636955-38
return the player and team ( league ) of list with the five lowest rd # .	SELECT t1.player , t1.team_ from list as t1 ORDER BY t1.rd_ LIMIT 5	2-11636955-38
find the team ( league ) of list which have both 10 and 25 as team ( league ) .	SELECT t1.team_ from list as t1 WHERE t1.pl_gp = 10 INTERSECT SELECT t1.team_ from list as t1 WHERE t1.pl_gp = 25	2-11636955-38
list player and team ( league ) who have pick # greater than 5 or reg gp shorter than 10 .	SELECT t1.player , t1.team_ from list as t1 WHERE t1.pick_ > 5 OR t1.reg_gp < 10	2-11636955-38
how many different team ( league ) correspond to each player ?	SELECT t1.player , COUNT ( DISTINCT t1.team_ ) from list as t1 GROUP BY t1.player	2-11636955-38
return each player with the number of list in ascending order of the number of player .	SELECT t1.player , COUNT ( * ) from list as t1 GROUP BY t1.player ORDER BY COUNT ( * )	2-11636955-38
