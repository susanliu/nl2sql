show all information on the miss that has the largest number of 1st runner up.	SELECT * from miss as t1 ORDER BY t1.1st_runner_up DESC LIMIT 1	2-18150755-1
find the winner of the miss who has the largest number of miss .	SELECT t1.winner from miss as t1 GROUP BY t1.winner ORDER BY COUNT ( * ) DESC LIMIT 1	2-18150755-1
show all information on the miss that has the largest number of 2nd runner up.	SELECT * from miss as t1 ORDER BY t1.2nd_runner_up DESC LIMIT 1	2-18150755-1
find all 2nd runner up that have fewer than three in miss .	SELECT t1.2nd_runner_up from miss as t1 GROUP BY t1.2nd_runner_up HAVING COUNT ( * ) < 3	2-18150755-1
what are the {winner of all the miss , and the total year by each ?	SELECT t1.winner , SUM ( t1.year ) from miss as t1 GROUP BY t1.winner	2-18150755-1
what are the host city of miss whose 2nd runner up is not 10 ?	SELECT t1.host_city from miss as t1 WHERE t1.2nd_runner_up ! = 10	2-18150755-1
what is the winner of the miss who has the highest number of miss ?	SELECT t1.winner from miss as t1 GROUP BY t1.winner ORDER BY COUNT ( * ) DESC LIMIT 1	2-18150755-1
what are the winner for all miss , and what is the total {year for each ?	SELECT t1.winner , SUM ( t1.year ) from miss as t1 GROUP BY t1.winner	2-18150755-1
find the distinct COLUMN_NAME,0} of all miss that have year higher than some miss from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.winner from miss as t1 WHERE t1.year > ( SELECT MIN ( t1.winner ) from miss as t1 WHERE t1.winner = 10 )	2-18150755-1
what are the 2nd runner up and host city ?	SELECT t1.2nd_runner_up , t1.host_city from miss as t1	2-18150755-1
