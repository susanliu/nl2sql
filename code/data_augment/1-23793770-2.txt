what is the minimum share in each rating ?	SELECT MIN ( t1.share ) , t1.rating from brothers as t1 GROUP BY t1.rating	1-23793770-2
list all information about brothers .	SELECT * FROM brothers	1-23793770-2
return the smallest share for every episode number production number .	SELECT MIN ( t1.share ) , t1.episode_number_production_number from brothers as t1 GROUP BY t1.episode_number_production_number	1-23793770-2
return the title of brothers that do not have the total viewers ( in millions ) 10 .	SELECT t1.title from brothers as t1 WHERE t1.total_viewers_ ! = 10	1-23793770-2
find the title and episode number production number of the brothers whose share is lower than the average share of all brothers .	SELECT t1.title , t1.episode_number_production_number from brothers as t1 WHERE t1.share < ( SELECT AVG ( t1.share ) {FROM, 3} )	1-23793770-2
find the distinct total viewers ( in millions ) of all brothers that have a higher share than some brothers with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.total_viewers_ from brothers as t1 WHERE t1.share > ( SELECT MIN ( t1.total_viewers_ ) from brothers as t1 WHERE t1.total_viewers_ = 10 )	1-23793770-2
find the original airing of brothers who have more than 10 brothers .	SELECT t1.original_airing from brothers as t1 GROUP BY t1.original_airing HAVING COUNT ( * ) > 10	1-23793770-2
find the number of brothers that have more than 10 total viewers ( in millions ) .	SELECT COUNT ( * ) from brothers as t1 GROUP BY t1.total_viewers_ HAVING COUNT ( * ) > 10 	1-23793770-2
what are the {rating of all the brothers , and the total share by each ?	SELECT t1.rating , SUM ( t1.share ) from brothers as t1 GROUP BY t1.rating	1-23793770-2
show all rating/share ( 18 – 49 ) and corresponding number of brothers in the ascending order of the numbers.	SELECT t1.rating/share_ , COUNT ( * ) from brothers as t1 GROUP BY t1.rating/share_ ORDER BY COUNT ( * )	1-23793770-2
