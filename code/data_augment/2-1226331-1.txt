find the chassis of piercarlo which have 10 but no 73 as engine .	SELECT t1.chassis from piercarlo as t1 WHERE t1.engine = 10 EXCEPT SELECT t1.chassis from piercarlo as t1 WHERE t1.engine = 73	2-1226331-1
list the chassis which average pts . is above 10 .	SELECT t1.chassis from piercarlo as t1 GROUP BY t1.chassis HAVING AVG ( t1.pts_ ) >= 10	2-1226331-1
find all entrant that have fewer than three in piercarlo .	SELECT t1.entrant from piercarlo as t1 GROUP BY t1.entrant HAVING COUNT ( * ) < 3	2-1226331-1
which t1.entrant has the fewest piercarlo ?	SELECT t1.entrant from piercarlo as t1 GROUP BY t1.entrant ORDER BY COUNT ( * ) LIMIT 1	2-1226331-1
which engine have an average year over 10 ?	SELECT t1.engine from piercarlo as t1 GROUP BY t1.engine HAVING AVG ( t1.year ) >= 10	2-1226331-1
show the entrant and the corresponding number of piercarlo sorted by the number of entrant in ascending order .	SELECT t1.entrant , COUNT ( * ) from piercarlo as t1 GROUP BY t1.entrant ORDER BY COUNT ( * )	2-1226331-1
select the average year of each piercarlo 's chassis .	SELECT AVG ( t1.year ) , t1.chassis from piercarlo as t1 GROUP BY t1.chassis	2-1226331-1
which engine has most number of piercarlo ?	SELECT t1.engine from piercarlo as t1 GROUP BY t1.engine ORDER BY COUNT ( * ) DESC LIMIT 1	2-1226331-1
give the chassis that has the most piercarlo .	SELECT t1.chassis from piercarlo as t1 GROUP BY t1.chassis ORDER BY COUNT ( * ) DESC LIMIT 1	2-1226331-1
return the entrant of the piercarlo with the fewest pts . .	SELECT t1.entrant from piercarlo as t1 ORDER BY t1.pts_ ASC LIMIT 1	2-1226331-1
