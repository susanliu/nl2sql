please list the races and points of robert in descending order of season .	SELECT t1.races , t1.points from robert as t1 ORDER BY t1.season DESC	2-1841727-1
what are the {races of all the robert , and the total season by each ?	SELECT t1.races , SUM ( t1.season ) from robert as t1 GROUP BY t1.races	2-1841727-1
find the races of robert which are f/laps 10 but not f/laps 89 .	SELECT t1.races from robert as t1 WHERE t1.f/laps = 10 EXCEPT SELECT t1.races from robert as t1 WHERE t1.f/laps = 89	2-1841727-1
find the podiums that have 10 robert .	SELECT t1.podiums from robert as t1 GROUP BY t1.podiums HAVING COUNT ( * ) = 10	2-1841727-1
what are the poles of robert , sorted by their frequency?	SELECT t1.poles from robert as t1 GROUP BY t1.poles ORDER BY COUNT ( * ) ASC LIMIT 1	2-1841727-1
return the series of the robert that has the fewest corresponding series .	SELECT t1.series from robert as t1 GROUP BY t1.series ORDER BY COUNT ( * ) ASC LIMIT 1	2-1841727-1
list the points which average season is above 10 .	SELECT t1.points from robert as t1 GROUP BY t1.points HAVING AVG ( t1.season ) >= 10	2-1841727-1
what is the races of the robert with the smallest season ?	SELECT t1.races from robert as t1 ORDER BY t1.season ASC LIMIT 1	2-1841727-1
find the position of the robert that is most frequent across all position .	SELECT t1.position from robert as t1 GROUP BY t1.position ORDER BY COUNT ( * ) DESC LIMIT 1	2-1841727-1
find the position of robert who have both 10 and 35 season .	SELECT t1.position from robert as t1 WHERE t1.season = 10 INTERSECT SELECT t1.position from robert as t1 WHERE t1.season = 35	2-1841727-1
