what is the location of the 1998 with least number of location ?	SELECT t1.location from 1998 as t1 GROUP BY t1.location ORDER BY COUNT ( * ) ASC LIMIT 1	1-11621799-1
show the date of the 1998 that has the greatest number of 1998 .	SELECT t1.date from 1998 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	1-11621799-1
what are total 1st prize ( $ ) for each tournament ?	SELECT t1.tournament , SUM ( t1.1st_prize_ ) from 1998 as t1 GROUP BY t1.tournament	1-11621799-1
please show the different tournament , ordered by the number of 1998 that have each .	SELECT t1.tournament from 1998 as t1 GROUP BY t1.tournament ORDER BY COUNT ( * ) ASC LIMIT 1	1-11621799-1
list date of 1998 that have the number of 1998 greater than 10 .	SELECT t1.date from 1998 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10	1-11621799-1
what are the date for 1998 who have more than the average 1st prize ( $ )?	SELECT t1.date from 1998 as t1 WHERE t1.1st_prize_ > ( SELECT AVG ( t1.1st_prize_ ) from 1998 as t1	1-11621799-1
find the winner of the 1998 that is most frequent across all winner .	SELECT t1.winner from 1998 as t1 GROUP BY t1.winner ORDER BY COUNT ( * ) DESC LIMIT 1	1-11621799-1
find the number of 1998 whose location contain the word 10 .	SELECT COUNT ( * ) from 1998 as t1 WHERE t1.location LIKE 10	1-11621799-1
find the score of 1998 who have both 10 and 88 purse ( $ ) .	SELECT t1.score from 1998 as t1 WHERE t1.purse_ = 10 INTERSECT SELECT t1.score from 1998 as t1 WHERE t1.purse_ = 88	1-11621799-1
what are the date and score of the {COLUMN} who have purse ( $ ) above five or purse ( $ ) below ten ?	SELECT t1.date , t1.score from 1998 as t1 WHERE t1.purse_ > 5 OR t1.purse_ < 10	1-11621799-1
