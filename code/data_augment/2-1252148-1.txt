which finish have less than 3 in sam ?	SELECT t1.finish from sam as t1 GROUP BY t1.finish HAVING COUNT ( * ) < 3	2-1252148-1
which finish has both sam with less than 10 laps and sam with more than 48 laps ?	SELECT t1.finish from sam as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.finish from sam as t1 WHERE t1.laps > 48	2-1252148-1
how many different start correspond to each start ?	SELECT t1.start , COUNT ( DISTINCT t1.start ) from sam as t1 GROUP BY t1.start	2-1252148-1
how many sam does each start have ?	SELECT t1.start , COUNT ( * ) from sam as t1 GROUP BY t1.start ORDER BY COUNT ( * )	2-1252148-1
find the start and finish of the sam whose laps is lower than the average laps of all sam .	SELECT t1.start , t1.finish from sam as t1 WHERE t1.laps < ( SELECT AVG ( t1.laps ) {FROM, 3} )	2-1252148-1
how many sam are there in rank 10 or 20 ?	SELECT COUNT ( * ) from sam as t1 WHERE t1.rank = 10 OR t1.rank = 20	2-1252148-1
find the qual of the sam with the highest laps.	SELECT t1.qual from sam as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) from sam as t1 )	2-1252148-1
show the rank and the total laps of sam .	SELECT t1.rank , SUM ( t1.laps ) from sam as t1 GROUP BY t1.rank	2-1252148-1
which qual have an average laps over 10 ?	SELECT t1.qual from sam as t1 GROUP BY t1.qual HAVING AVG ( t1.laps ) >= 10	2-1252148-1
return the maximum and minimum laps across all sam .	SELECT MAX ( t1.laps ) , MIN ( t1.laps ) from sam as t1	2-1252148-1
