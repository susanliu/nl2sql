list the position which average height ( m ) is above 10 .	SELECT t1.position from immortals as t1 GROUP BY t1.position HAVING AVG ( t1.height_ ) >= 10	2-14844461-2
find the years of immortals which are in 10 position but not in 21 position .	SELECT t1.years from immortals as t1 WHERE t1.position = 10 EXCEPT SELECT t1.years from immortals as t1 WHERE t1.position = 21	2-14844461-2
find the years which have exactly 10 immortals .	SELECT t1.years from immortals as t1 GROUP BY t1.years HAVING COUNT ( * ) = 10	2-14844461-2
return the player of immortals for which the position is not 10 ?	SELECT t1.player from immortals as t1 WHERE t1.position ! = 10	2-14844461-2
which years have an average height ( m ) over 10 ?	SELECT t1.years from immortals as t1 GROUP BY t1.years HAVING AVG ( t1.height_ ) >= 10	2-14844461-2
which years have greater weight ( kg ) than that of any weight ( kg ) in immortals ?	SELECT t1.years from immortals as t1 WHERE t1.weight_ > ( SELECT MIN ( t1.weight_ ) from immortals as t1 )	2-14844461-2
what is the position and player of the immortals with maximum weight ( kg ) ?	SELECT t1.position , t1.player from immortals as t1 WHERE t1.weight_ = ( SELECT MAX ( t1.weight_ ) {FROM, 3} )	2-14844461-2
Return all columns in immortals .	SELECT * FROM immortals	2-14844461-2
what are the distinct position with weight ( kg ) between 10 and 84 ?	SELECT DISTINCT t1.position from immortals as t1 WHERE t1.weight_ BETWEEN 10 AND 84	2-14844461-2
list player and position who have height ( m ) greater than 5 or height ( m ) shorter than 10 .	SELECT t1.player , t1.position from immortals as t1 WHERE t1.height_ > 5 OR t1.height_ < 10	2-14844461-2
