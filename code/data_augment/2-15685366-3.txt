give the finish that has the most list .	SELECT t1.finish from list as t1 GROUP BY t1.finish ORDER BY COUNT ( * ) DESC LIMIT 1	2-15685366-3
find the conference of list which are in 10 finish but not in 90 finish .	SELECT t1.conference from list as t1 WHERE t1.finish = 10 EXCEPT SELECT t1.conference from list as t1 WHERE t1.finish = 90	2-15685366-3
find the season who has exactly 10 list .	SELECT t1.season from list as t1 GROUP BY t1.season HAVING COUNT ( * ) = 10	2-15685366-3
what is the conference of the list with the smallest losses ?	SELECT t1.conference from list as t1 ORDER BY t1.losses ASC LIMIT 1	2-15685366-3
what is the minimum losses in each finish ?	SELECT MIN ( t1.losses ) , t1.finish from list as t1 GROUP BY t1.finish	2-15685366-3
show the season , finish , and division of all the list .	SELECT t1.season , t1.finish , t1.division from list as t1	2-15685366-3
what are the division of the list with division other than 10 ?	SELECT t1.division from list as t1 WHERE t1.finish ! = 10	2-15685366-3
what is the season of the list with the largest win % ?	SELECT t1.season from list as t1 WHERE t1.win_ = ( SELECT MAX ( t1.win_ ) from list as t1 )	2-15685366-3
what is the division and season for the list with the rank 5 smallest wins ?	SELECT t1.division , t1.season from list as t1 ORDER BY t1.wins LIMIT 5	2-15685366-3
list all information about list .	SELECT * FROM list	2-15685366-3
