what are the average elevation ( m ) of list for different country ?	SELECT AVG ( t1.elevation_ ) , t1.country from list as t1 GROUP BY t1.country	2-18947170-11
return the country and peak of list with the five lowest col ( m ) .	SELECT t1.country , t1.peak from list as t1 ORDER BY t1.col_ LIMIT 5	2-18947170-11
which peak have less than 3 in list ?	SELECT t1.peak from list as t1 GROUP BY t1.peak HAVING COUNT ( * ) < 3	2-18947170-11
show the peak and the number of unique country containing each peak .	SELECT t1.peak , COUNT ( DISTINCT t1.country ) from list as t1 GROUP BY t1.peak	2-18947170-11
what are the country for list that have an col ( m ) greater than the average .	SELECT t1.country from list as t1 WHERE t1.col_ > ( SELECT AVG ( t1.col_ ) from list as t1	2-18947170-11
return the peak and peak of list with the five lowest prominence ( m ) .	SELECT t1.peak , t1.peak from list as t1 ORDER BY t1.prominence_ LIMIT 5	2-18947170-11
list all information about list .	SELECT * FROM list	2-18947170-11
give the t1.country with the fewest list .	SELECT t1.country from list as t1 GROUP BY t1.country ORDER BY COUNT ( * ) LIMIT 1	2-18947170-11
find the distinct country of list having elevation ( m ) between 10 and 24 .	SELECT DISTINCT t1.country from list as t1 WHERE t1.elevation_ BETWEEN 10 AND 24	2-18947170-11
what are the maximum and minimum col ( m ) across all list ?	SELECT MAX ( t1.col_ ) , MIN ( t1.col_ ) from list as t1	2-18947170-11
