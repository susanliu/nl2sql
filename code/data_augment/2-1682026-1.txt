what are the company and industry of the {COLUMN} who have market value ( billion $ ) above five or sales ( billion $ ) below ten ?	SELECT t1.company , t1.industry from forbes as t1 WHERE t1.market_value_ > 5 OR t1.sales_ < 10	2-1682026-1
list all information about forbes .	SELECT * FROM forbes	2-1682026-1
what are the company of the forbes that have exactly 10 forbes ?	SELECT t1.company from forbes as t1 GROUP BY t1.company HAVING COUNT ( * ) = 10	2-1682026-1
find the rank of the forbes which have rank 10 but not 67 .	SELECT t1.rank from forbes as t1 WHERE t1.rank = 10 EXCEPT SELECT t1.rank from forbes as t1 WHERE t1.rank = 67	2-1682026-1
select the average assets ( billion $ ) of each forbes 's rank .	SELECT AVG ( t1.assets_ ) , t1.rank from forbes as t1 GROUP BY t1.rank	2-1682026-1
find the distinct rank of forbes having assets ( billion $ ) between 10 and 10 .	SELECT DISTINCT t1.rank from forbes as t1 WHERE t1.assets_ BETWEEN 10 AND 10	2-1682026-1
give the t1.headquarters with the fewest forbes .	SELECT t1.headquarters from forbes as t1 GROUP BY t1.headquarters ORDER BY COUNT ( * ) LIMIT 1	2-1682026-1
what is the company of the forbes with least number of company ?	SELECT t1.company from forbes as t1 GROUP BY t1.company ORDER BY COUNT ( * ) ASC LIMIT 1	2-1682026-1
return the industry of forbes for which the headquarters is not 10 ?	SELECT t1.industry from forbes as t1 WHERE t1.headquarters ! = 10	2-1682026-1
what are the company of all forbes that have 10 or more forbes ?	SELECT t1.company from forbes as t1 GROUP BY t1.company HAVING COUNT ( * ) >= 10	2-1682026-1
