show the constructor of 1966 who have at least 10 1966 .	SELECT t1.constructor from 1966 as t1 GROUP BY t1.constructor HAVING COUNT ( * ) >= 10	2-1122360-1
find the constructor of the 1966 with the highest grid.	SELECT t1.constructor from 1966 as t1 WHERE t1.grid = ( SELECT MAX ( t1.grid ) from 1966 as t1 )	2-1122360-1
find the driver of 1966 who have both 10 and 54 grid .	SELECT t1.driver from 1966 as t1 WHERE t1.grid = 10 INTERSECT SELECT t1.driver from 1966 as t1 WHERE t1.grid = 54	2-1122360-1
find the time/retired of 1966 who have laps of both 10 and 13 .	SELECT t1.time/retired from 1966 as t1 WHERE t1.laps = 10 INTERSECT SELECT t1.time/retired from 1966 as t1 WHERE t1.laps = 13	2-1122360-1
find the time/retired of the 1966 who has the largest number of 1966 .	SELECT t1.time/retired from 1966 as t1 GROUP BY t1.time/retired ORDER BY COUNT ( * ) DESC LIMIT 1	2-1122360-1
what is the constructor of the 1966 who has the highest number of 1966 ?	SELECT t1.constructor from 1966 as t1 GROUP BY t1.constructor ORDER BY COUNT ( * ) DESC LIMIT 1	2-1122360-1
what are the driver of all 1966 that have 10 or more 1966 ?	SELECT t1.driver from 1966 as t1 GROUP BY t1.driver HAVING COUNT ( * ) >= 10	2-1122360-1
find the time/retired of 1966 who have grid of both 10 and 13 .	SELECT t1.time/retired from 1966 as t1 WHERE t1.grid = 10 INTERSECT SELECT t1.time/retired from 1966 as t1 WHERE t1.grid = 13	2-1122360-1
what are the driver of 1966 whose driver is not 10 ?	SELECT t1.driver from 1966 as t1 WHERE t1.driver ! = 10	2-1122360-1
return the different time/retired of 1966 , in ascending order of frequency .	SELECT t1.time/retired from 1966 as t1 GROUP BY t1.time/retired ORDER BY COUNT ( * ) ASC LIMIT 1	2-1122360-1
