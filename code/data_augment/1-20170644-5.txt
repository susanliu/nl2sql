what are the college and player ?	SELECT t1.college , t1.player from 2009 as t1	1-20170644-5
show cfl team and the number of distinct cfl team in each cfl team .	SELECT t1.cfl_team , COUNT ( DISTINCT t1.cfl_team ) from 2009 as t1 GROUP BY t1.cfl_team	1-20170644-5
what are the average pick # of 2009 , grouped by college ?	SELECT AVG ( t1.pick_ ) , t1.college from 2009 as t1 GROUP BY t1.college	1-20170644-5
what are the player and cfl team of each 2009 ?	SELECT t1.player , t1.cfl_team from 2009 as t1	1-20170644-5
list cfl team of 2009 that have the number of 2009 greater than 10 .	SELECT t1.cfl_team from 2009 as t1 GROUP BY t1.cfl_team HAVING COUNT ( * ) > 10	1-20170644-5
return the player of the 2009 that has the fewest corresponding player .	SELECT t1.player from 2009 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	1-20170644-5
how many 2009 are there in position 10 or 19 ?	SELECT COUNT ( * ) from 2009 as t1 WHERE t1.position = 10 OR t1.position = 19	1-20170644-5
list the position which average pick # is above 10 .	SELECT t1.position from 2009 as t1 GROUP BY t1.position HAVING AVG ( t1.pick_ ) >= 10	1-20170644-5
what are the cfl team , college , and cfl team for each 2009 ?	SELECT t1.cfl_team , t1.college , t1.cfl_team from 2009 as t1	1-20170644-5
which position have greater pick # than that of any pick # in 2009 ?	SELECT t1.position from 2009 as t1 WHERE t1.pick_ > ( SELECT MIN ( t1.pick_ ) from 2009 as t1 )	1-20170644-5
