how many capital are there that have more than {VALUE},0 institution ?	SELECT COUNT ( * ) from capital as t1 GROUP BY t1.institution HAVING COUNT ( * ) > 10 	1-1973648-1
what are the joined and institution of the {COLUMN} who have founded above five or founded below ten ?	SELECT t1.joined , t1.institution from capital as t1 WHERE t1.founded > 5 OR t1.founded < 10	1-1973648-1
please list the location and institution of capital in descending order of enrollment .	SELECT t1.location , t1.institution from capital as t1 ORDER BY t1.enrollment DESC	1-1973648-1
show the nickname , location , and type of all the capital .	SELECT t1.nickname , t1.location , t1.type from capital as t1	1-1973648-1
find the institution of capital which have both 10 and 31 as institution .	SELECT t1.institution from capital as t1 WHERE t1.enrollment = 10 INTERSECT SELECT t1.institution from capital as t1 WHERE t1.enrollment = 31	1-1973648-1
what are the distinct institution with founded between 10 and 1 ?	SELECT DISTINCT t1.institution from capital as t1 WHERE t1.founded BETWEEN 10 AND 1	1-1973648-1
what is the joined of the capital with least number of joined ?	SELECT t1.joined from capital as t1 GROUP BY t1.joined ORDER BY COUNT ( * ) ASC LIMIT 1	1-1973648-1
which location has both capital with less than 10 founded and capital with more than 81 founded ?	SELECT t1.location from capital as t1 WHERE t1.founded < 10 INTERSECT SELECT t1.location from capital as t1 WHERE t1.founded > 81	1-1973648-1
list location of capital that have the number of capital greater than 10 .	SELECT t1.location from capital as t1 GROUP BY t1.location HAVING COUNT ( * ) > 10	1-1973648-1
return the type of the capital with the fewest enrollment .	SELECT t1.type from capital as t1 ORDER BY t1.enrollment ASC LIMIT 1	1-1973648-1
