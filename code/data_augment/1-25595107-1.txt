show the sail number , yacht type , and yacht of all the 2004 .	SELECT t1.sail_number , t1.yacht_type , t1.yacht from 2004 as t1	1-25595107-1
find the yacht of the 2004 with the highest position.	SELECT t1.yacht from 2004 as t1 WHERE t1.position = ( SELECT MAX ( t1.position ) from 2004 as t1 )	1-25595107-1
find the yacht type and elapsed time d : hh : mm : ss of the 2004 whose position is lower than the average position of all 2004 .	SELECT t1.yacht_type , t1.elapsed_time_d_ from 2004 as t1 WHERE t1.position < ( SELECT AVG ( t1.position ) {FROM, 3} )	1-25595107-1
what is the skipper and loa ( metres ) of the 2004 with maximum position ?	SELECT t1.skipper , t1.loa_ from 2004 as t1 WHERE t1.position = ( SELECT MAX ( t1.position ) {FROM, 3} )	1-25595107-1
count the number of 2004 in sail number 10 or 15 .	SELECT COUNT ( * ) from 2004 as t1 WHERE t1.sail_number = 10 OR t1.sail_number = 15	1-25595107-1
how many yacht type did each 2004 do, ordered by number of yacht type ?	SELECT t1.yacht_type , COUNT ( * ) from 2004 as t1 GROUP BY t1.yacht_type ORDER BY COUNT ( * )	1-25595107-1
what are the loa ( metres ) of all 2004 with elapsed time d : hh : mm : ss that is 10 ?	SELECT t1.loa_ from 2004 as t1 GROUP BY t1.elapsed_time_d_ HAVING COUNT ( * ) = 10	1-25595107-1
what is the yacht of the 2004 with least number of yacht ?	SELECT t1.yacht from 2004 as t1 GROUP BY t1.yacht ORDER BY COUNT ( * ) ASC LIMIT 1	1-25595107-1
what is the yacht of all 2004 whose position is higher than any 2004 ?	SELECT t1.yacht from 2004 as t1 WHERE t1.position > ( SELECT MIN ( t1.position ) from 2004 as t1 )	1-25595107-1
what are the maximum and minimum position across all 2004 ?	SELECT MAX ( t1.position ) , MIN ( t1.position ) from 2004 as t1	1-25595107-1
