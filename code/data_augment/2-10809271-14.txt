show the home team score with fewer than 3 1945 .	SELECT t1.home_team_score from 1945 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) < 3	2-10809271-14
find the home team of 1945 whose crowd is more than the average crowd of 1945 .	SELECT t1.home_team from 1945 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1945 as t1	2-10809271-14
find the venue of 1945 which have both 10 and 98 as venue .	SELECT t1.venue from 1945 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.venue from 1945 as t1 WHERE t1.crowd = 98	2-10809271-14
which home team is the most frequent home team?	SELECT t1.home_team from 1945 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10809271-14
what are the date of 1945 whose away team score are not 10 ?	SELECT t1.date from 1945 as t1 WHERE t1.away_team_score ! = 10	2-10809271-14
what are the maximum and minimum crowd across all 1945 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1945 as t1	2-10809271-14
what is the venue and venue of the 1945 with maximum crowd ?	SELECT t1.venue , t1.venue from 1945 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809271-14
which away team has both 1945 with less than 10 crowd and 1945 with more than 24 crowd ?	SELECT t1.away_team from 1945 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.away_team from 1945 as t1 WHERE t1.crowd > 24	2-10809271-14
which date have greater crowd than that of any crowd in 1945 ?	SELECT t1.date from 1945 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1945 as t1 )	2-10809271-14
how many 1945 are there in home team score 10 or 93 ?	SELECT COUNT ( * ) from 1945 as t1 WHERE t1.home_team_score = 10 OR t1.home_team_score = 93	2-10809271-14
