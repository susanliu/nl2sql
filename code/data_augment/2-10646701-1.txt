what are the maximum and minimum week across all 1967 ?	SELECT MAX ( t1.week ) , MIN ( t1.week ) from 1967 as t1	2-10646701-1
count the number of 1967 in opponent 10 or 62 .	SELECT COUNT ( * ) from 1967 as t1 WHERE t1.opponent = 10 OR t1.opponent = 62	2-10646701-1
select the average week of each 1967 's date .	SELECT AVG ( t1.week ) , t1.date from 1967 as t1 GROUP BY t1.date	2-10646701-1
what are the date of 1967 , sorted by their frequency?	SELECT t1.date from 1967 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-10646701-1
find the date that have 10 1967 .	SELECT t1.date from 1967 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10646701-1
what are the result of all 1967 with date that is 10 ?	SELECT t1.result from 1967 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10646701-1
return the smallest week for every opponent .	SELECT MIN ( t1.week ) , t1.opponent from 1967 as t1 GROUP BY t1.opponent	2-10646701-1
sort the list of result and opponent of all 1967 in the descending order of week .	SELECT t1.result , t1.opponent from 1967 as t1 ORDER BY t1.week DESC	2-10646701-1
find the attendance of the 1967 which have date 10 but not 29 .	SELECT t1.attendance from 1967 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.attendance from 1967 as t1 WHERE t1.date = 29	2-10646701-1
what are the date of 1967 whose date are not 10 ?	SELECT t1.date from 1967 as t1 WHERE t1.date ! = 10	2-10646701-1
