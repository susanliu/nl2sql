what is the 54 holes and championship of every james that has a year lower than average ?	SELECT t1.54_holes , t1.championship from james as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-1516353-1
find the margin of the james with the highest year .	SELECT t1.margin from james as t1 ORDER BY t1.year DESC LIMIT 1	2-1516353-1
select the average year of each james 's winning score .	SELECT AVG ( t1.year ) , t1.winning_score from james as t1 GROUP BY t1.winning_score	2-1516353-1
which 54 holes has most number of james ?	SELECT t1.54_holes from james as t1 GROUP BY t1.54_holes ORDER BY COUNT ( * ) DESC LIMIT 1	2-1516353-1
return the smallest year for every runner ( s ) - up .	SELECT MIN ( t1.year ) , t1.runner_ from james as t1 GROUP BY t1.runner_	2-1516353-1
what are the margin and championship of all james sorted by decreasing year ?	SELECT t1.margin , t1.championship from james as t1 ORDER BY t1.year DESC	2-1516353-1
show runner ( s ) - up and 54 holes of james .	SELECT t1.runner_ , t1.54_holes from james as t1	2-1516353-1
show the championship and runner ( s ) - up with at least 10 championship .	SELECT t1.championship , t1.runner_ from james as t1 GROUP BY t1.championship HAVING COUNT ( * ) >= 10	2-1516353-1
how many james ' 54 holes have the word 10 in them ?	SELECT COUNT ( * ) from james as t1 WHERE t1.54_holes LIKE 10	2-1516353-1
what is the winning score of highest year ?	SELECT t1.winning_score from james as t1 ORDER BY t1.year DESC LIMIT 1	2-1516353-1
