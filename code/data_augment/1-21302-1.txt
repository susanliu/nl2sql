count the number of nauru in district 10 or 29 .	SELECT COUNT ( * ) from nauru as t1 WHERE t1.district = 10 OR t1.district = 29	1-21302-1
how many different district correspond to each density persons / ha ?	SELECT t1.density_persons_/_ha , COUNT ( DISTINCT t1.district ) from nauru as t1 GROUP BY t1.density_persons_/_ha	1-21302-1
show the density persons / ha of the nauru that has the most nauru .	SELECT t1.density_persons_/_ha from nauru as t1 GROUP BY t1.density_persons_/_ha ORDER BY COUNT ( * ) DESC LIMIT 1	1-21302-1
show all former name and corresponding number of nauru in the ascending order of the numbers.	SELECT t1.former_name , COUNT ( * ) from nauru as t1 GROUP BY t1.former_name ORDER BY COUNT ( * )	1-21302-1
how many nauru are there that have more than {VALUE},0 density persons / ha ?	SELECT COUNT ( * ) from nauru as t1 GROUP BY t1.density_persons_/_ha HAVING COUNT ( * ) > 10 	1-21302-1
find the distinct former name of nauru having population ( 2005 ) between 10 and 57 .	SELECT DISTINCT t1.former_name from nauru as t1 WHERE t1.population_ BETWEEN 10 AND 57	1-21302-1
what are the density persons / ha and density persons / ha ?	SELECT t1.density_persons_/_ha , t1.density_persons_/_ha from nauru as t1	1-21302-1
what is the density persons / ha of the nauru with the minimum population ( 2005 ) ?	SELECT t1.density_persons_/_ha from nauru as t1 ORDER BY t1.population_ ASC LIMIT 1	1-21302-1
what is the density persons / ha and former name of the nauru with maximum area ( ha ) ?	SELECT t1.density_persons_/_ha , t1.former_name from nauru as t1 WHERE t1.area_ = ( SELECT MAX ( t1.area_ ) {FROM, 3} )	1-21302-1
what are the former name of nauru with nr . greater than the average of all nauru ?	SELECT t1.former_name from nauru as t1 WHERE t1.nr_ > ( SELECT AVG ( t1.nr_ ) from nauru as t1	1-21302-1
