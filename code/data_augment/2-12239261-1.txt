show the host with fewer than 3 list .	SELECT t1.host from list as t1 GROUP BY t1.host HAVING COUNT ( * ) < 3	2-12239261-1
what are all the season and series?	SELECT t1.season , t1.series from list as t1	2-12239261-1
return the series result of the list that has the fewest corresponding series result .	SELECT t1.series_result from list as t1 GROUP BY t1.series_result ORDER BY COUNT ( * ) ASC LIMIT 1	2-12239261-1
find the series of the list which have season 10 but not 61 .	SELECT t1.series from list as t1 WHERE t1.season = 10 EXCEPT SELECT t1.series from list as t1 WHERE t1.season = 61	2-12239261-1
which season has both list with less than 10 england and list with more than 8 england ?	SELECT t1.season from list as t1 WHERE t1.england < 10 INTERSECT SELECT t1.season from list as t1 WHERE t1.england > 8	2-12239261-1
find the holder at the end of the series of the list with the highest tests .	SELECT t1.holder_at_the_end_of_the_series from list as t1 ORDER BY t1.tests DESC LIMIT 1	2-12239261-1
return the series of list for which the holder at the end of the series is not 10 ?	SELECT t1.series from list as t1 WHERE t1.holder_at_the_end_of_the_series ! = 10	2-12239261-1
list series and host who have tests greater than 5 or west indies shorter than 10 .	SELECT t1.series , t1.host from list as t1 WHERE t1.tests > 5 OR t1.west_indies < 10	2-12239261-1
which season have an average england over 10 ?	SELECT t1.season from list as t1 GROUP BY t1.season HAVING AVG ( t1.england ) >= 10	2-12239261-1
please show the different host , ordered by the number of list that have each .	SELECT t1.host from list as t1 GROUP BY t1.host ORDER BY COUNT ( * ) ASC LIMIT 1	2-12239261-1
