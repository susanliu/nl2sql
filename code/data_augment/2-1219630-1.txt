which engine have an average points over 10 ?	SELECT t1.engine from brett as t1 GROUP BY t1.engine HAVING AVG ( t1.points ) >= 10	2-1219630-1
what is the maximum and mininum year {COLUMN} for all brett ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from brett as t1	2-1219630-1
what are the team of all brett with engine that is 10 ?	SELECT t1.team from brett as t1 GROUP BY t1.engine HAVING COUNT ( * ) = 10	2-1219630-1
return the team of the largest year.	SELECT t1.team from brett as t1 ORDER BY t1.year DESC LIMIT 1	2-1219630-1
show all team and the total year for each .	SELECT t1.team , SUM ( t1.year ) from brett as t1 GROUP BY t1.team	2-1219630-1
which chassis has most number of brett ?	SELECT t1.chassis from brett as t1 GROUP BY t1.chassis ORDER BY COUNT ( * ) DESC LIMIT 1	2-1219630-1
what are the {engine of all the brett , and the total points by each ?	SELECT t1.engine , SUM ( t1.points ) from brett as t1 GROUP BY t1.engine	2-1219630-1
what is the minimum points in each team ?	SELECT MIN ( t1.points ) , t1.team from brett as t1 GROUP BY t1.team	2-1219630-1
what are the average year of brett , grouped by engine ?	SELECT AVG ( t1.year ) , t1.engine from brett as t1 GROUP BY t1.engine	2-1219630-1
list the chassis which average year is above 10 .	SELECT t1.chassis from brett as t1 GROUP BY t1.chassis HAVING AVG ( t1.year ) >= 10	2-1219630-1
