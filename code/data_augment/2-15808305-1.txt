return the smallest season for every head coach .	SELECT MIN ( t1.season ) , t1.head_coach from macarthur as t1 GROUP BY t1.head_coach	2-15808305-1
what are the head coach with exactly 10 macarthur ?	SELECT t1.head_coach from macarthur as t1 GROUP BY t1.head_coach HAVING COUNT ( * ) = 10	2-15808305-1
what are the average season of macarthur , grouped by team ?	SELECT AVG ( t1.season ) , t1.team from macarthur as t1 GROUP BY t1.team	2-15808305-1
list the head coach and team of all macarthur sorted by season in descending order .	SELECT t1.head_coach , t1.team from macarthur as t1 ORDER BY t1.season DESC	2-15808305-1
please show the different conference , ordered by the number of macarthur that have each .	SELECT t1.conference from macarthur as t1 GROUP BY t1.conference ORDER BY COUNT ( * ) ASC LIMIT 1	2-15808305-1
what are the head coach of all macarthur with head coach that is 10 ?	SELECT t1.head_coach from macarthur as t1 GROUP BY t1.head_coach HAVING COUNT ( * ) = 10	2-15808305-1
what is the minimum season in each head coach ?	SELECT MIN ( t1.season ) , t1.head_coach from macarthur as t1 GROUP BY t1.head_coach	2-15808305-1
what are the distinct team with season between 10 and 8 ?	SELECT DISTINCT t1.team from macarthur as t1 WHERE t1.season BETWEEN 10 AND 8	2-15808305-1
return the smallest season for every conference .	SELECT MIN ( t1.season ) , t1.conference from macarthur as t1 GROUP BY t1.conference	2-15808305-1
how many macarthur does each head coach have ?	SELECT t1.head_coach , COUNT ( * ) from macarthur as t1 GROUP BY t1.head_coach ORDER BY COUNT ( * )	2-15808305-1
