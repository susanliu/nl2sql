how many distinct runner ( s ) - up correspond to each winning score ?	SELECT t1.winning_score , COUNT ( DISTINCT t1.runner_ ) from inbee as t1 GROUP BY t1.winning_score	2-18198579-2
how many inbee are there in winning score 10 or 46 ?	SELECT COUNT ( * ) from inbee as t1 WHERE t1.winning_score = 10 OR t1.winning_score = 46	2-18198579-2
find the winning score and date of the inbee whose winner 's share ( $ ) is lower than the average winner 's share ( $ ) of all inbee .	SELECT t1.winning_score , t1.date from inbee as t1 WHERE t1.winner_'s_share_ < ( SELECT AVG ( t1.winner_'s_share_ ) {FROM, 3} )	2-18198579-2
count the number of inbee in to par 10 or 86 .	SELECT COUNT ( * ) from inbee as t1 WHERE t1.to_par = 10 OR t1.to_par = 86	2-18198579-2
return the different margin of victory of inbee , in ascending order of frequency .	SELECT t1.margin_of_victory from inbee as t1 GROUP BY t1.margin_of_victory ORDER BY COUNT ( * ) ASC LIMIT 1	2-18198579-2
which to par has both inbee with less than 10 winner 's share ( $ ) and inbee with more than 57 winner 's share ( $ ) ?	SELECT t1.to_par from inbee as t1 WHERE t1.winner_'s_share_ < 10 INTERSECT SELECT t1.to_par from inbee as t1 WHERE t1.winner_'s_share_ > 57	2-18198579-2
show all information on the inbee that has the largest number of date.	SELECT * from inbee as t1 ORDER BY t1.date DESC LIMIT 1	2-18198579-2
show the winning score and runner ( s ) - up with at least 10 runner ( s ) - up .	SELECT t1.winning_score , t1.runner_ from inbee as t1 GROUP BY t1.runner_ HAVING COUNT ( * ) >= 10	2-18198579-2
please show the runner ( s ) - up of the inbee that have at least 10 records .	SELECT t1.runner_ from inbee as t1 GROUP BY t1.runner_ HAVING COUNT ( * ) >= 10	2-18198579-2
which date have greater winner 's share ( $ ) than that of any winner 's share ( $ ) in inbee ?	SELECT t1.date from inbee as t1 WHERE t1.winner_'s_share_ > ( SELECT MIN ( t1.winner_'s_share_ ) from inbee as t1 )	2-18198579-2
