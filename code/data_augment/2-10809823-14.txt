show the venue with fewer than 3 1980 .	SELECT t1.venue from 1980 as t1 GROUP BY t1.venue HAVING COUNT ( * ) < 3	2-10809823-14
what is the minimum crowd in each date ?	SELECT MIN ( t1.crowd ) , t1.date from 1980 as t1 GROUP BY t1.date	2-10809823-14
what is the away team score and away team score of the 1980 with maximum crowd ?	SELECT t1.away_team_score , t1.away_team_score from 1980 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809823-14
how many different away team correspond to each home team score ?	SELECT t1.home_team_score , COUNT ( DISTINCT t1.away_team ) from 1980 as t1 GROUP BY t1.home_team_score	2-10809823-14
show the venue and the number of unique home team containing each venue .	SELECT t1.venue , COUNT ( DISTINCT t1.home_team ) from 1980 as t1 GROUP BY t1.venue	2-10809823-14
what is all the information on the 1980 with the largest number of venue ?	SELECT * from 1980 as t1 ORDER BY t1.venue DESC LIMIT 1	2-10809823-14
which away team score have an average crowd over 10 ?	SELECT t1.away_team_score from 1980 as t1 GROUP BY t1.away_team_score HAVING AVG ( t1.crowd ) >= 10	2-10809823-14
what is the away team score and away team of every 1980 that has a crowd lower than average ?	SELECT t1.away_team_score , t1.away_team from 1980 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10809823-14
what is the minimum crowd in each home team score ?	SELECT MIN ( t1.crowd ) , t1.home_team_score from 1980 as t1 GROUP BY t1.home_team_score	2-10809823-14
what are the venue of 1980 with crowd greater than the average of all 1980 ?	SELECT t1.venue from 1980 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1980 as t1	2-10809823-14
