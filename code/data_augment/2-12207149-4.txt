sort the list of record and record of all 1990 in the descending order of attendance .	SELECT t1.record , t1.record from 1990 as t1 ORDER BY t1.attendance DESC	2-12207149-4
show the score shared by more than 10 1990 .	SELECT t1.score from 1990 as t1 GROUP BY t1.score HAVING COUNT ( * ) > 10	2-12207149-4
what are the date of all 1990 that have 10 or more 1990 ?	SELECT t1.date from 1990 as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	2-12207149-4
what are the date and score of each 1990 ?	SELECT t1.date , t1.score from 1990 as t1	2-12207149-4
return the opponent of the 1990 that has the fewest corresponding opponent .	SELECT t1.opponent from 1990 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-12207149-4
please show the loss of the 1990 that have at least 10 records .	SELECT t1.loss from 1990 as t1 GROUP BY t1.loss HAVING COUNT ( * ) >= 10	2-12207149-4
find the distinct loss of all 1990 that have a higher attendance than some 1990 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.loss from 1990 as t1 WHERE t1.attendance > ( SELECT MIN ( t1.loss ) from 1990 as t1 WHERE t1.opponent = 10 )	2-12207149-4
which record have an average attendance over 10 ?	SELECT t1.record from 1990 as t1 GROUP BY t1.record HAVING AVG ( t1.attendance ) >= 10	2-12207149-4
return the date of the 1990 that has the fewest corresponding date .	SELECT t1.date from 1990 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-12207149-4
what are the score , date , and opponent of each 1990 ?	SELECT t1.score , t1.date , t1.opponent from 1990 as t1	2-12207149-4
