what are the away team score of 1940 whose venue is not 10 ?	SELECT t1.away_team_score from 1940 as t1 WHERE t1.venue ! = 10	2-10807253-12
what are the home team score of all 1940 that have 10 or more 1940 ?	SELECT t1.home_team_score from 1940 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) >= 10	2-10807253-12
find the away team score and home team of the 1940 whose crowd is lower than the average crowd of all 1940 .	SELECT t1.away_team_score , t1.home_team from 1940 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10807253-12
what are the home team that have greater crowd than any crowd in 1940 ?	SELECT t1.home_team from 1940 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1940 as t1 )	2-10807253-12
which t1.home_team has the smallest amount of 1940?	SELECT t1.home_team from 1940 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) LIMIT 1	2-10807253-12
what are the away team and venue of each 1940 ?	SELECT t1.away_team , t1.venue from 1940 as t1	2-10807253-12
what is the minimum crowd in each home team ?	SELECT MIN ( t1.crowd ) , t1.home_team from 1940 as t1 GROUP BY t1.home_team	2-10807253-12
what are the away team and date of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.away_team , t1.date from 1940 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10807253-12
what are the distinct date with crowd between 10 and 45 ?	SELECT DISTINCT t1.date from 1940 as t1 WHERE t1.crowd BETWEEN 10 AND 45	2-10807253-12
what are the date of all 1940 with venue that is 10 ?	SELECT t1.date from 1940 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10807253-12
