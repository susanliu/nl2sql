sort the list of original air date and directed by of all none in the descending order of no . in series .	SELECT t1.original_air_date , t1.directed_by from none as t1 ORDER BY t1.no_._in_series DESC	1-18481791-2
return the title of the none that has the fewest corresponding title .	SELECT t1.title from none as t1 GROUP BY t1.title ORDER BY COUNT ( * ) ASC LIMIT 1	1-18481791-2
what is the directed by and original air date of the none with the top 5 smallest no . in series ?	SELECT t1.directed_by , t1.original_air_date from none as t1 ORDER BY t1.no_._in_series LIMIT 5	1-18481791-2
what are the written by of all none that have 10 or more none ?	SELECT t1.written_by from none as t1 GROUP BY t1.written_by HAVING COUNT ( * ) >= 10	1-18481791-2
how many none ' directed by have the word 10 in them ?	SELECT COUNT ( * ) from none as t1 WHERE t1.directed_by LIKE 10	1-18481791-2
what is all the information on the none with the largest number of original air date ?	SELECT * from none as t1 ORDER BY t1.original_air_date DESC LIMIT 1	1-18481791-2
return the smallest no . in series for every title .	SELECT MIN ( t1.no_._in_series ) , t1.title from none as t1 GROUP BY t1.title	1-18481791-2
what is the u.s. viewers ( in millions ) of the none with the minimum no . in series ?	SELECT t1.u.s._viewers_ from none as t1 ORDER BY t1.no_._in_series ASC LIMIT 1	1-18481791-2
count the number of none in original air date 10 or 55 .	SELECT COUNT ( * ) from none as t1 WHERE t1.original_air_date = 10 OR t1.original_air_date = 55	1-18481791-2
show the written by and original air date with at least 10 u.s. viewers ( in millions ) .	SELECT t1.written_by , t1.original_air_date from none as t1 GROUP BY t1.u.s._viewers_ HAVING COUNT ( * ) >= 10	1-18481791-2
