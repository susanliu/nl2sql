which chassis have less than 3 in bob ?	SELECT t1.chassis from bob as t1 GROUP BY t1.chassis HAVING COUNT ( * ) < 3	2-1251953-3
return the maximum and minimum points across all bob .	SELECT MAX ( t1.points ) , MIN ( t1.points ) from bob as t1	2-1251953-3
what are the engine and entrant of each bob ?	SELECT t1.engine , t1.entrant from bob as t1	2-1251953-3
which entrant have greater points than that of any points in bob ?	SELECT t1.entrant from bob as t1 WHERE t1.points > ( SELECT MIN ( t1.points ) from bob as t1 )	2-1251953-3
find the entrant of bob which are entrant 10 but not entrant 94 .	SELECT t1.entrant from bob as t1 WHERE t1.entrant = 10 EXCEPT SELECT t1.entrant from bob as t1 WHERE t1.entrant = 94	2-1251953-3
list entrant and engine who have year greater than 5 or year shorter than 10 .	SELECT t1.entrant , t1.engine from bob as t1 WHERE t1.year > 5 OR t1.year < 10	2-1251953-3
what is the chassis of the bob with the minimum points ?	SELECT t1.chassis from bob as t1 ORDER BY t1.points ASC LIMIT 1	2-1251953-3
what are the chassis and entrant of each bob ?	SELECT t1.chassis , t1.entrant from bob as t1	2-1251953-3
what are the chassis of all bob with engine that is 10 ?	SELECT t1.chassis from bob as t1 GROUP BY t1.engine HAVING COUNT ( * ) = 10	2-1251953-3
what are the entrant and chassis ?	SELECT t1.entrant , t1.chassis from bob as t1	2-1251953-3
