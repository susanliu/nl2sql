find the manufacturer of the 2007 which have manufacturer 10 but not 99 .	SELECT t1.manufacturer from 2007 as t1 WHERE t1.manufacturer = 10 EXCEPT SELECT t1.manufacturer from 2007 as t1 WHERE t1.manufacturer = 99	2-13719531-2
how many 2007 have manufacturer that contains 10 ?	SELECT COUNT ( * ) from 2007 as t1 WHERE t1.manufacturer LIKE 10	2-13719531-2
which time/retired has both 2007 with less than 10 laps and 2007 with more than 56 laps ?	SELECT t1.time/retired from 2007 as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.time/retired from 2007 as t1 WHERE t1.laps > 56	2-13719531-2
which manufacturer have less than 3 in 2007 ?	SELECT t1.manufacturer from 2007 as t1 GROUP BY t1.manufacturer HAVING COUNT ( * ) < 3	2-13719531-2
what are the manufacturer for 2007 that have an grid greater than the average .	SELECT t1.manufacturer from 2007 as t1 WHERE t1.grid > ( SELECT AVG ( t1.grid ) from 2007 as t1	2-13719531-2
what are the maximum and minimum laps across all 2007 ?	SELECT MAX ( t1.laps ) , MIN ( t1.laps ) from 2007 as t1	2-13719531-2
what are the time/retired and time/retired of 2007 with 10 or more rider ?	SELECT t1.time/retired , t1.time/retired from 2007 as t1 GROUP BY t1.rider HAVING COUNT ( * ) >= 10	2-13719531-2
what is the manufacturer and rider of the 2007 with maximum laps ?	SELECT t1.manufacturer , t1.rider from 2007 as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) {FROM, 3} )	2-13719531-2
what is the manufacturer of the 2007 who has the highest number of 2007 ?	SELECT t1.manufacturer from 2007 as t1 GROUP BY t1.manufacturer ORDER BY COUNT ( * ) DESC LIMIT 1	2-13719531-2
what are the distinct COLUMN_NAME,0} of every 2007 that has a greater laps than some 2007 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.time/retired from 2007 as t1 WHERE t1.laps > ( SELECT MIN ( t1.time/retired ) from 2007 as t1 WHERE t1.manufacturer = 10 )	2-13719531-2
