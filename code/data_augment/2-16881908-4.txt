what is the player and place of the 2008 with maximum score ?	SELECT t1.player , t1.place from 2008 as t1 WHERE t1.score = ( SELECT MAX ( t1.score ) {FROM, 3} )	2-16881908-4
select the average score of each 2008 's country .	SELECT AVG ( t1.score ) , t1.country from 2008 as t1 GROUP BY t1.country	2-16881908-4
give the t1.country with the fewest 2008 .	SELECT t1.country from 2008 as t1 GROUP BY t1.country ORDER BY COUNT ( * ) LIMIT 1	2-16881908-4
find the player of the 2008 that have just 10 2008 .	SELECT t1.player from 2008 as t1 GROUP BY t1.player HAVING COUNT ( * ) = 10	2-16881908-4
find the distinct country of all 2008 that have a higher score than some 2008 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.country from 2008 as t1 WHERE t1.score > ( SELECT MIN ( t1.country ) from 2008 as t1 WHERE t1.place = 10 )	2-16881908-4
return the different place of 2008 , in ascending order of frequency .	SELECT t1.place from 2008 as t1 GROUP BY t1.place ORDER BY COUNT ( * ) ASC LIMIT 1	2-16881908-4
how many 2008 correspond to each to par? show the result in ascending order.	SELECT t1.to_par , COUNT ( * ) from 2008 as t1 GROUP BY t1.to_par ORDER BY COUNT ( * )	2-16881908-4
show the to par of 2008 who have at least 10 2008 .	SELECT t1.to_par from 2008 as t1 GROUP BY t1.to_par HAVING COUNT ( * ) >= 10	2-16881908-4
list all place which have score higher than the average .	SELECT t1.place from 2008 as t1 WHERE t1.score > ( SELECT AVG ( t1.score ) from 2008 as t1	2-16881908-4
which place has most number of 2008 ?	SELECT t1.place from 2008 as t1 GROUP BY t1.place ORDER BY COUNT ( * ) DESC LIMIT 1	2-16881908-4
