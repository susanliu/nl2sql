give the rank that has the most 1994 .	SELECT t1.rank from 1994 as t1 GROUP BY t1.rank ORDER BY COUNT ( * ) DESC LIMIT 1	2-15558966-3
what is the nation and rank of the 1994 with maximum silver ?	SELECT t1.nation , t1.rank from 1994 as t1 WHERE t1.silver = ( SELECT MAX ( t1.silver ) {FROM, 3} )	2-15558966-3
what are the maximum and minimum silver across all 1994 ?	SELECT MAX ( t1.silver ) , MIN ( t1.silver ) from 1994 as t1	2-15558966-3
find the number of 1994 that have more than 10 rank .	SELECT COUNT ( * ) from 1994 as t1 GROUP BY t1.rank HAVING COUNT ( * ) > 10 	2-15558966-3
give the maximum and minimum bronze of all 1994 .	SELECT MAX ( t1.bronze ) , MIN ( t1.bronze ) from 1994 as t1	2-15558966-3
which nation has both 1994 with less than 10 silver and 1994 with more than 24 silver ?	SELECT t1.nation from 1994 as t1 WHERE t1.silver < 10 INTERSECT SELECT t1.nation from 1994 as t1 WHERE t1.silver > 24	2-15558966-3
find the distinct nation of 1994 having total between 10 and 35 .	SELECT DISTINCT t1.nation from 1994 as t1 WHERE t1.total BETWEEN 10 AND 35	2-15558966-3
which rank has both 1994 with less than 10 total and 1994 with more than 81 total ?	SELECT t1.rank from 1994 as t1 WHERE t1.total < 10 INTERSECT SELECT t1.rank from 1994 as t1 WHERE t1.total > 81	2-15558966-3
find the distinct rank of all 1994 that have a higher bronze than some 1994 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.rank from 1994 as t1 WHERE t1.bronze > ( SELECT MIN ( t1.rank ) from 1994 as t1 WHERE t1.rank = 10 )	2-15558966-3
what is the nation and nation of the 1994 with maximum gold ?	SELECT t1.nation , t1.nation from 1994 as t1 WHERE t1.gold = ( SELECT MAX ( t1.gold ) {FROM, 3} )	2-15558966-3
