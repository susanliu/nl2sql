find the district of list who have constituency no . of both 10 and 63 .	SELECT t1.district from list as t1 WHERE t1.constituency_no_ = 10 INTERSECT SELECT t1.district from list as t1 WHERE t1.constituency_no_ = 63	1-29785324-5
show the vidhan sabha constituency , reservation for sc/st , and lok sabha of all the list .	SELECT t1.vidhan_sabha_constituency , t1.reservation_for_sc/st , t1.lok_sabha from list as t1	1-29785324-5
show the lok sabha of list who have at least 10 list .	SELECT t1.lok_sabha from list as t1 GROUP BY t1.lok_sabha HAVING COUNT ( * ) >= 10	1-29785324-5
how many list are there in reservation for sc/st 10 or 42 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.reservation_for_sc/st = 10 OR t1.reservation_for_sc/st = 42	1-29785324-5
list the vidhan sabha constituency , district and the reservation for sc/st of the list .	SELECT t1.vidhan_sabha_constituency , t1.district , t1.reservation_for_sc/st from list as t1	1-29785324-5
which lok sabha have greater constituency no . than that of any constituency no . in list ?	SELECT t1.lok_sabha from list as t1 WHERE t1.constituency_no_ > ( SELECT MIN ( t1.constituency_no_ ) from list as t1 )	1-29785324-5
show the vidhan sabha constituency and their total constituency no . of list .	SELECT t1.vidhan_sabha_constituency , SUM ( t1.constituency_no_ ) from list as t1 GROUP BY t1.vidhan_sabha_constituency	1-29785324-5
what are the reservation for sc/st more than 10 list have ?	SELECT t1.reservation_for_sc/st from list as t1 GROUP BY t1.reservation_for_sc/st HAVING COUNT ( * ) > 10	1-29785324-5
what are the reservation for sc/st of list whose district are not 10 ?	SELECT t1.reservation_for_sc/st from list as t1 WHERE t1.district ! = 10	1-29785324-5
find the district of the list that is most frequent across all district .	SELECT t1.district from list as t1 GROUP BY t1.district ORDER BY COUNT ( * ) DESC LIMIT 1	1-29785324-5
