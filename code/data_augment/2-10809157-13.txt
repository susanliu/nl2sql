return the home team and away team score of 1969 with the five lowest crowd .	SELECT t1.home_team , t1.away_team_score from 1969 as t1 ORDER BY t1.crowd LIMIT 5	2-10809157-13
find the date of 1969 which are in 10 date but not in 90 date .	SELECT t1.date from 1969 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.date from 1969 as t1 WHERE t1.date = 90	2-10809157-13
count the number of 1969 in home team score 10 or 94 .	SELECT COUNT ( * ) from 1969 as t1 WHERE t1.home_team_score = 10 OR t1.home_team_score = 94	2-10809157-13
return the maximum and minimum crowd across all 1969 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1969 as t1	2-10809157-13
how many 1969 have venue that contain the word 10 ?	SELECT COUNT ( * ) from 1969 as t1 WHERE t1.venue LIKE 10	2-10809157-13
show the away team of 1969 whose home team score are not 10.	SELECT t1.away_team from 1969 as t1 WHERE t1.home_team_score ! = 10	2-10809157-13
find the date of 1969 which have 10 but no 25 as venue .	SELECT t1.date from 1969 as t1 WHERE t1.venue = 10 EXCEPT SELECT t1.date from 1969 as t1 WHERE t1.venue = 25	2-10809157-13
which away team has most number of 1969 ?	SELECT t1.away_team from 1969 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10809157-13
give the maximum and minimum crowd of all 1969 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1969 as t1	2-10809157-13
return the home team score and home team score of 1969 with the five lowest crowd .	SELECT t1.home_team_score , t1.home_team_score from 1969 as t1 ORDER BY t1.crowd LIMIT 5	2-10809157-13
