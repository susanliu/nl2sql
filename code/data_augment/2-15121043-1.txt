what are the average numeric code of gost , grouped by country name ?	SELECT AVG ( t1.numeric_code ) , t1.country_name from gost as t1 GROUP BY t1.country_name	2-15121043-1
what are the country name of all gost with latin 3-letter code that is 10 ?	SELECT t1.country_name from gost as t1 GROUP BY t1.latin_3-letter_code HAVING COUNT ( * ) = 10	2-15121043-1
what are the russian name for gost who have more than the average numeric code?	SELECT t1.russian_name from gost as t1 WHERE t1.numeric_code > ( SELECT AVG ( t1.numeric_code ) from gost as t1	2-15121043-1
show all information on the gost that has the largest number of latin 3-letter code.	SELECT * from gost as t1 ORDER BY t1.latin_3-letter_code DESC LIMIT 1	2-15121043-1
what are all the russian name and country name?	SELECT t1.russian_name , t1.country_name from gost as t1	2-15121043-1
what is the t1.cyrillic_code of gost that has fewest number of gost ?	SELECT t1.cyrillic_code from gost as t1 GROUP BY t1.cyrillic_code ORDER BY COUNT ( * ) LIMIT 1	2-15121043-1
what is the russian name of the gost who has the highest number of gost ?	SELECT t1.russian_name from gost as t1 GROUP BY t1.russian_name ORDER BY COUNT ( * ) DESC LIMIT 1	2-15121043-1
what is the maximum and mininum numeric code {COLUMN} for all gost ?	SELECT MAX ( t1.numeric_code ) , MIN ( t1.numeric_code ) from gost as t1	2-15121043-1
return the smallest numeric code for every russian name .	SELECT MIN ( t1.numeric_code ) , t1.russian_name from gost as t1 GROUP BY t1.russian_name	2-15121043-1
how many gost are there in latin 3-letter code 10 or 97 ?	SELECT COUNT ( * ) from gost as t1 WHERE t1.latin_3-letter_code = 10 OR t1.latin_3-letter_code = 97	2-15121043-1
