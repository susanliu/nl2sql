what are the pos . and team of each john ?	SELECT t1.pos_ , t1.team from john as t1	2-1886746-3
which class has the least laps ?	SELECT t1.class from john as t1 ORDER BY t1.laps ASC LIMIT 1	2-1886746-3
what is the co-drivers of the john who has the highest number of john ?	SELECT t1.co-drivers from john as t1 GROUP BY t1.co-drivers ORDER BY COUNT ( * ) DESC LIMIT 1	2-1886746-3
what are the tyres of all john that have 10 or more john ?	SELECT t1.tyres from john as t1 GROUP BY t1.tyres HAVING COUNT ( * ) >= 10	2-1886746-3
find the team of john who have both 10 and 7 laps .	SELECT t1.team from john as t1 WHERE t1.laps = 10 INTERSECT SELECT t1.team from john as t1 WHERE t1.laps = 7	2-1886746-3
find the distinct team of john having laps between 10 and 24 .	SELECT DISTINCT t1.team from john as t1 WHERE t1.laps BETWEEN 10 AND 24	2-1886746-3
what are the co-drivers of the john with co-drivers other than 10 ?	SELECT t1.co-drivers from john as t1 WHERE t1.pos_ ! = 10	2-1886746-3
find the co-drivers of the john with the highest laps.	SELECT t1.co-drivers from john as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) from john as t1 )	2-1886746-3
what is the team of all john whose laps is higher than any john ?	SELECT t1.team from john as t1 WHERE t1.laps > ( SELECT MIN ( t1.laps ) from john as t1 )	2-1886746-3
what is the class of highest laps ?	SELECT t1.class from john as t1 ORDER BY t1.laps DESC LIMIT 1	2-1886746-3
