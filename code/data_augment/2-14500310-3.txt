find the town and town of the list with at least 10 country .	SELECT t1.town , t1.town from list as t1 GROUP BY t1.country HAVING COUNT ( * ) >= 10	2-14500310-3
what are the planned pinnacle height of all list with planned pinnacle height that is 10 ?	SELECT t1.planned_pinnacle_height from list as t1 GROUP BY t1.planned_pinnacle_height HAVING COUNT ( * ) = 10	2-14500310-3
which t1.structural_type has least number of list ?	SELECT t1.structural_type from list as t1 GROUP BY t1.structural_type ORDER BY COUNT ( * ) LIMIT 1	2-14500310-3
find the planned pinnacle height of list which have 10 but no 100 as country .	SELECT t1.planned_pinnacle_height from list as t1 WHERE t1.country = 10 EXCEPT SELECT t1.planned_pinnacle_height from list as t1 WHERE t1.country = 100	2-14500310-3
what are the structural type and town of each list ?	SELECT t1.structural_type , t1.town from list as t1	2-14500310-3
find the number of list that have more than 10 town .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.town HAVING COUNT ( * ) > 10 	2-14500310-3
show the structural type and structural type with at least 10 country .	SELECT t1.structural_type , t1.structural_type from list as t1 GROUP BY t1.country HAVING COUNT ( * ) >= 10	2-14500310-3
find the structural type and town of the list whose expected year of completion is lower than the average expected year of completion of all list .	SELECT t1.structural_type , t1.town from list as t1 WHERE t1.expected_year_of_completion < ( SELECT AVG ( t1.expected_year_of_completion ) {FROM, 3} )	2-14500310-3
what is the country of the list with the largest expected year of completion ?	SELECT t1.country from list as t1 WHERE t1.expected_year_of_completion = ( SELECT MAX ( t1.expected_year_of_completion ) from list as t1 )	2-14500310-3
which country is the most frequent country?	SELECT t1.country from list as t1 GROUP BY t1.country ORDER BY COUNT ( * ) DESC LIMIT 1	2-14500310-3
