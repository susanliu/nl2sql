return the maximum and minimum week across all 2002 .	SELECT MAX ( t1.week ) , MIN ( t1.week ) from 2002 as t1	2-15331575-1
what are the attendance that have greater week than any week in 2002 ?	SELECT t1.attendance from 2002 as t1 WHERE t1.week > ( SELECT MIN ( t1.week ) from 2002 as t1 )	2-15331575-1
find the attendance of the 2002 which have opponent 10 but not 43 .	SELECT t1.attendance from 2002 as t1 WHERE t1.opponent = 10 EXCEPT SELECT t1.attendance from 2002 as t1 WHERE t1.opponent = 43	2-15331575-1
how many distinct attendance correspond to each attendance ?	SELECT t1.attendance , COUNT ( DISTINCT t1.attendance ) from 2002 as t1 GROUP BY t1.attendance	2-15331575-1
find the date of 2002 which have 10 but no 33 as date .	SELECT t1.date from 2002 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.date from 2002 as t1 WHERE t1.date = 33	2-15331575-1
find the opponent of the 2002 with the largest week .	SELECT t1.opponent from 2002 as t1 ORDER BY t1.week DESC LIMIT 1	2-15331575-1
return the attendance of 2002 that do not have the attendance 10 .	SELECT t1.attendance from 2002 as t1 WHERE t1.attendance ! = 10	2-15331575-1
which opponent is the most frequent opponent?	SELECT t1.opponent from 2002 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-15331575-1
find the attendance and opponent of the 2002 whose week is lower than the average week of all 2002 .	SELECT t1.attendance , t1.opponent from 2002 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-15331575-1
find the attendance of the 2002 with the highest week.	SELECT t1.attendance from 2002 as t1 WHERE t1.week = ( SELECT MAX ( t1.week ) from 2002 as t1 )	2-15331575-1
