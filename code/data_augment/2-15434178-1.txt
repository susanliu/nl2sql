find the result and result of the 1960 whose week is lower than the average week of all 1960 .	SELECT t1.result , t1.result from 1960 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-15434178-1
what are the average week of 1960 , grouped by opponent ?	SELECT AVG ( t1.week ) , t1.opponent from 1960 as t1 GROUP BY t1.opponent	2-15434178-1
count the number of 1960 in opponent 10 or 12 .	SELECT COUNT ( * ) from 1960 as t1 WHERE t1.opponent = 10 OR t1.opponent = 12	2-15434178-1
count the number of 1960 that have an date containing 10 .	SELECT COUNT ( * ) from 1960 as t1 WHERE t1.date LIKE 10	2-15434178-1
show date for all 1960 whose attendance are greater than the average .	SELECT t1.date from 1960 as t1 WHERE t1.attendance > ( SELECT AVG ( t1.attendance ) from 1960 as t1	2-15434178-1
show the date and result with at least 10 result .	SELECT t1.date , t1.result from 1960 as t1 GROUP BY t1.result HAVING COUNT ( * ) >= 10	2-15434178-1
give the opponent that has the most 1960 .	SELECT t1.opponent from 1960 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-15434178-1
display the result , opponent , and opponent for each 1960 .	SELECT t1.result , t1.opponent , t1.opponent from 1960 as t1	2-15434178-1
what are all the opponent and date?	SELECT t1.opponent , t1.date from 1960 as t1	2-15434178-1
what is the opponent of 1960 with the maximum attendance across all 1960 ?	SELECT t1.opponent from 1960 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) from 1960 as t1 )	2-15434178-1
