give the maximum and minimum to par of all 1956 .	SELECT MAX ( t1.to_par ) , MIN ( t1.to_par ) from 1956 as t1	2-17290135-3
what are the score that have greater to par than any to par in 1956 ?	SELECT t1.score from 1956 as t1 WHERE t1.to_par > ( SELECT MIN ( t1.to_par ) from 1956 as t1 )	2-17290135-3
what are the average to par of 1956 for different player ?	SELECT AVG ( t1.to_par ) , t1.player from 1956 as t1 GROUP BY t1.player	2-17290135-3
find the player of the 1956 that have just 10 1956 .	SELECT t1.player from 1956 as t1 GROUP BY t1.player HAVING COUNT ( * ) = 10	2-17290135-3
how many 1956 are there in score 10 or 12 ?	SELECT COUNT ( * ) from 1956 as t1 WHERE t1.score = 10 OR t1.score = 12	2-17290135-3
which score has both 1956 with less than 10 to par and 1956 with more than 30 to par ?	SELECT t1.score from 1956 as t1 WHERE t1.to_par < 10 INTERSECT SELECT t1.score from 1956 as t1 WHERE t1.to_par > 30	2-17290135-3
what is the score and place of the 1956 with maximum to par ?	SELECT t1.score , t1.place from 1956 as t1 WHERE t1.to_par = ( SELECT MAX ( t1.to_par ) {FROM, 3} )	2-17290135-3
what are the distinct COLUMN_NAME,0} of 1956 with to par higher than any 1956 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.country from 1956 as t1 WHERE t1.to_par > ( SELECT MIN ( t1.country ) from 1956 as t1 WHERE t1.place = 10 )	2-17290135-3
how many 1956 are there that have more than {VALUE},0 country ?	SELECT COUNT ( * ) from 1956 as t1 GROUP BY t1.country HAVING COUNT ( * ) > 10 	2-17290135-3
show the place and the number of unique player containing each place .	SELECT t1.place , COUNT ( DISTINCT t1.player ) from 1956 as t1 GROUP BY t1.place	2-17290135-3
