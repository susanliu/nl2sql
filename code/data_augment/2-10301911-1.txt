what are the team that have greater rank than any rank in 1970 ?	SELECT t1.team from 1970 as t1 WHERE t1.rank > ( SELECT MIN ( t1.rank ) from 1970 as t1 )	2-10301911-1
find the distinct rider of all 1970 that have a higher rank than some 1970 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.rider from 1970 as t1 WHERE t1.rank > ( SELECT MIN ( t1.rider ) from 1970 as t1 WHERE t1.time = 10 )	2-10301911-1
show all information on the 1970 that has the largest number of speed.	SELECT * from 1970 as t1 ORDER BY t1.speed DESC LIMIT 1	2-10301911-1
show all rider and corresponding number of 1970 sorted by the count .	SELECT t1.rider , COUNT ( * ) from 1970 as t1 GROUP BY t1.rider ORDER BY COUNT ( * )	2-10301911-1
what is the count of 1970 with more than 10 speed ?	SELECT COUNT ( * ) from 1970 as t1 GROUP BY t1.speed HAVING COUNT ( * ) > 10 	2-10301911-1
find the rider for all 1970 who have more than the average rank .	SELECT t1.rider from 1970 as t1 WHERE t1.rank > ( SELECT AVG ( t1.rank ) from 1970 as t1	2-10301911-1
find the time of 1970 who have rank of both 10 and 76 .	SELECT t1.time from 1970 as t1 WHERE t1.rank = 10 INTERSECT SELECT t1.time from 1970 as t1 WHERE t1.rank = 76	2-10301911-1
sort the list of team and rider of all 1970 in the descending order of rank .	SELECT t1.team , t1.rider from 1970 as t1 ORDER BY t1.rank DESC	2-10301911-1
count the number of 1970 that have an team containing 10 .	SELECT COUNT ( * ) from 1970 as t1 WHERE t1.team LIKE 10	2-10301911-1
find the time of the 1970 with the largest rank .	SELECT t1.time from 1970 as t1 ORDER BY t1.rank DESC LIMIT 1	2-10301911-1
