what is the t1.nominated_work of colbie that has fewest number of colbie ?	SELECT t1.nominated_work from colbie as t1 GROUP BY t1.nominated_work ORDER BY COUNT ( * ) LIMIT 1	2-12416709-3
what is the maximum and mininum year {COLUMN} for all colbie ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from colbie as t1	2-12416709-3
how many colbie are there that have more than {VALUE},0 category ?	SELECT COUNT ( * ) from colbie as t1 GROUP BY t1.category HAVING COUNT ( * ) > 10 	2-12416709-3
return the different category of colbie , in ascending order of frequency .	SELECT t1.category from colbie as t1 GROUP BY t1.category ORDER BY COUNT ( * ) ASC LIMIT 1	2-12416709-3
return the smallest year for every result .	SELECT MIN ( t1.year ) , t1.result from colbie as t1 GROUP BY t1.result	2-12416709-3
what is the nominated work of colbie with the maximum year across all colbie ?	SELECT t1.nominated_work from colbie as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from colbie as t1 )	2-12416709-3
what are the result more than 10 colbie have ?	SELECT t1.result from colbie as t1 GROUP BY t1.result HAVING COUNT ( * ) > 10	2-12416709-3
show the result and award with at least 10 result .	SELECT t1.result , t1.award from colbie as t1 GROUP BY t1.result HAVING COUNT ( * ) >= 10	2-12416709-3
what are the average year of colbie , grouped by category ?	SELECT AVG ( t1.year ) , t1.category from colbie as t1 GROUP BY t1.category	2-12416709-3
what are all the award and result?	SELECT t1.award , t1.result from colbie as t1	2-12416709-3
