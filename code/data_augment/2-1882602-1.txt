find the programming of the wviz with the highest channel.	SELECT t1.programming from wviz as t1 WHERE t1.channel = ( SELECT MAX ( t1.channel ) from wviz as t1 )	2-1882602-1
which t1.programming has the fewest wviz ?	SELECT t1.programming from wviz as t1 GROUP BY t1.programming ORDER BY COUNT ( * ) LIMIT 1	2-1882602-1
return the smallest channel for every programming .	SELECT MIN ( t1.channel ) , t1.programming from wviz as t1 GROUP BY t1.programming	2-1882602-1
what is the psip short name and video for the wviz with the rank 5 smallest channel ?	SELECT t1.psip_short_name , t1.video from wviz as t1 ORDER BY t1.channel LIMIT 5	2-1882602-1
what is the programming of the wviz with the largest channel ?	SELECT t1.programming from wviz as t1 WHERE t1.channel = ( SELECT MAX ( t1.channel ) from wviz as t1 )	2-1882602-1
what are the average channel of wviz , grouped by psip short name ?	SELECT AVG ( t1.channel ) , t1.psip_short_name from wviz as t1 GROUP BY t1.psip_short_name	2-1882602-1
select the average channel of each wviz 's programming .	SELECT AVG ( t1.channel ) , t1.programming from wviz as t1 GROUP BY t1.programming	2-1882602-1
which aspect has most number of wviz ?	SELECT t1.aspect from wviz as t1 GROUP BY t1.aspect ORDER BY COUNT ( * ) DESC LIMIT 1	2-1882602-1
what is the video of the wviz with the smallest channel ?	SELECT t1.video from wviz as t1 ORDER BY t1.channel ASC LIMIT 1	2-1882602-1
give the maximum and minimum channel of all wviz .	SELECT MAX ( t1.channel ) , MIN ( t1.channel ) from wviz as t1	2-1882602-1
