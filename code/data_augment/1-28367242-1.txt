list area ( km ² ) and area ( km ² ) who have population ( 2011 ) greater than 5 or population ( 2006 ) shorter than 10 .	SELECT t1.area_ , t1.area_ from list as t1 WHERE t1.population_ > 5 OR t1.population_ < 10	1-28367242-1
what are the change ( % ) of all list with regional district that is 10 ?	SELECT t1.change_ from list as t1 GROUP BY t1.regional_district HAVING COUNT ( * ) = 10	1-28367242-1
what are the change ( % ) of all list with regional district that is 10 ?	SELECT t1.change_ from list as t1 GROUP BY t1.regional_district HAVING COUNT ( * ) = 10	1-28367242-1
what is the regional district and name of the list with the top 5 smallest population ( 2011 ) ?	SELECT t1.regional_district , t1.name from list as t1 ORDER BY t1.population_ LIMIT 5	1-28367242-1
which change ( % ) has both list with less than 10 population ( 2011 ) and list with more than 69 population ( 2011 ) ?	SELECT t1.change_ from list as t1 WHERE t1.population_ < 10 INTERSECT SELECT t1.change_ from list as t1 WHERE t1.population_ > 69	1-28367242-1
show all name and corresponding number of list in the ascending order of the numbers.	SELECT t1.name , COUNT ( * ) from list as t1 GROUP BY t1.name ORDER BY COUNT ( * )	1-28367242-1
how many list ' area ( km ² ) have the word 10 in them ?	SELECT COUNT ( * ) from list as t1 WHERE t1.area_ LIKE 10	1-28367242-1
return the change ( % ) of list for which the regional district is not 10 ?	SELECT t1.change_ from list as t1 WHERE t1.regional_district ! = 10	1-28367242-1
what is the t1.area_ of list that has fewest number of list ?	SELECT t1.area_ from list as t1 GROUP BY t1.area_ ORDER BY COUNT ( * ) LIMIT 1	1-28367242-1
what is the corporate name of highest population ( 2011 ) ?	SELECT t1.corporate_name from list as t1 ORDER BY t1.population_ DESC LIMIT 1	1-28367242-1
