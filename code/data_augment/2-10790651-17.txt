how many 1935 have venue that contain the word 10 ?	SELECT COUNT ( * ) from 1935 as t1 WHERE t1.venue LIKE 10	2-10790651-17
how many 1935 correspond to each home team score? show the result in ascending order.	SELECT t1.home_team_score , COUNT ( * ) from 1935 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * )	2-10790651-17
which away team have an average crowd over 10 ?	SELECT t1.away_team from 1935 as t1 GROUP BY t1.away_team HAVING AVG ( t1.crowd ) >= 10	2-10790651-17
what is the away team of the 1935 with the largest crowd ?	SELECT t1.away_team from 1935 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1935 as t1 )	2-10790651-17
how many distinct away team correspond to each date ?	SELECT t1.date , COUNT ( DISTINCT t1.away_team ) from 1935 as t1 GROUP BY t1.date	2-10790651-17
find the venue and home team of the 1935 with at least 10 home team .	SELECT t1.venue , t1.home_team from 1935 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10790651-17
list home team of 1935 that have the number of 1935 greater than 10 .	SELECT t1.home_team from 1935 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) > 10	2-10790651-17
find the date of the 1935 that is most frequent across all date .	SELECT t1.date from 1935 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-10790651-17
return the venue of the 1935 that has the fewest corresponding venue .	SELECT t1.venue from 1935 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) ASC LIMIT 1	2-10790651-17
give the t1.home_team_score with the fewest 1935 .	SELECT t1.home_team_score from 1935 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10790651-17
