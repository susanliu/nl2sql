what is the time of swimming with the maximum heat across all swimming ?	SELECT t1.time from swimming as t1 WHERE t1.heat = ( SELECT MAX ( t1.heat ) from swimming as t1 )	2-18624740-3
what are the name and nationality ?	SELECT t1.name , t1.nationality from swimming as t1	2-18624740-3
return each time with the number of swimming in ascending order of the number of time .	SELECT t1.time , COUNT ( * ) from swimming as t1 GROUP BY t1.time ORDER BY COUNT ( * )	2-18624740-3
find the nationality of the swimming with the highest heat .	SELECT t1.nationality from swimming as t1 ORDER BY t1.heat DESC LIMIT 1	2-18624740-3
show all name and corresponding number of swimming sorted by the count .	SELECT t1.name , COUNT ( * ) from swimming as t1 GROUP BY t1.name ORDER BY COUNT ( * )	2-18624740-3
find the time of the swimming with the highest lane.	SELECT t1.time from swimming as t1 WHERE t1.lane = ( SELECT MAX ( t1.lane ) from swimming as t1 )	2-18624740-3
what are the nationality and name of the {COLUMN} who have heat above five or lane below ten ?	SELECT t1.nationality , t1.name from swimming as t1 WHERE t1.heat > 5 OR t1.lane < 10	2-18624740-3
what are the time that have greater heat than any heat in swimming ?	SELECT t1.time from swimming as t1 WHERE t1.heat > ( SELECT MIN ( t1.heat ) from swimming as t1 )	2-18624740-3
list the nationality of {swimming which has number of swimming greater than 10 .	SELECT t1.nationality from swimming as t1 GROUP BY t1.nationality HAVING COUNT ( * ) > 10	2-18624740-3
what is the maximum and mininum lane {COLUMN} for all swimming ?	SELECT MAX ( t1.lane ) , MIN ( t1.lane ) from swimming as t1	2-18624740-3
