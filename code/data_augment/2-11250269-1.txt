what are the player of 1991 , sorted by their frequency?	SELECT t1.player from 1991 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-11250269-1
count the number of 1991 that have an position containing 10 .	SELECT COUNT ( * ) from 1991 as t1 WHERE t1.position LIKE 10	2-11250269-1
display the player , position , and position for each 1991 .	SELECT t1.player , t1.position , t1.position from 1991 as t1	2-11250269-1
find the distinct school/club team of 1991 having round between 10 and 47 .	SELECT DISTINCT t1.school/club_team from 1991 as t1 WHERE t1.round BETWEEN 10 AND 47	2-11250269-1
find the position and player of the 1991 whose pick is lower than the average pick of all 1991 .	SELECT t1.position , t1.player from 1991 as t1 WHERE t1.pick < ( SELECT AVG ( t1.pick ) {FROM, 3} )	2-11250269-1
which school/club team have less than 3 in 1991 ?	SELECT t1.school/club_team from 1991 as t1 GROUP BY t1.school/club_team HAVING COUNT ( * ) < 3	2-11250269-1
what is the position and school/club team of the 1991 with maximum pick ?	SELECT t1.position , t1.school/club_team from 1991 as t1 WHERE t1.pick = ( SELECT MAX ( t1.pick ) {FROM, 3} )	2-11250269-1
find the player and school/club team of the 1991 with at least 10 player .	SELECT t1.player , t1.school/club_team from 1991 as t1 GROUP BY t1.player HAVING COUNT ( * ) >= 10	2-11250269-1
what are the maximum and minimum pick across all 1991 ?	SELECT MAX ( t1.pick ) , MIN ( t1.pick ) from 1991 as t1	2-11250269-1
what are the position , player , and position of each 1991 ?	SELECT t1.position , t1.player , t1.player from 1991 as t1	2-11250269-1
