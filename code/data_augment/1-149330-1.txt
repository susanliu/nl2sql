find the distinct % of votes of bharatiya having change in seat between 10 and 9 .	SELECT DISTINCT t1.% from bharatiya as t1 WHERE t1.change_in_seat BETWEEN 10 AND 9	1-149330-1
return the general election of the bharatiya that has the fewest corresponding general election .	SELECT t1.general_election from bharatiya as t1 GROUP BY t1.general_election ORDER BY COUNT ( * ) ASC LIMIT 1	1-149330-1
find the year of bharatiya who have both 10 and 77 seats won .	SELECT t1.year from bharatiya as t1 WHERE t1.seats_won = 10 INTERSECT SELECT t1.year from bharatiya as t1 WHERE t1.seats_won = 77	1-149330-1
what are the general election and year of all bharatiya sorted by decreasing seats won ?	SELECT t1.general_election , t1.year from bharatiya as t1 ORDER BY t1.seats_won DESC	1-149330-1
show % of votes and the number of distinct year in each % of votes .	SELECT t1.% , COUNT ( DISTINCT t1.year ) from bharatiya as t1 GROUP BY t1.%	1-149330-1
find the distinct COLUMN_NAME,0} of all bharatiya that have change in seat higher than some bharatiya from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.year from bharatiya as t1 WHERE t1.change_in_seat > ( SELECT MIN ( t1.year ) from bharatiya as t1 WHERE t1.votes_swing = 10 )	1-149330-1
find the votes swing of the bharatiya with the highest seats won .	SELECT t1.votes_swing from bharatiya as t1 ORDER BY t1.seats_won DESC LIMIT 1	1-149330-1
find the votes swing who has exactly 10 bharatiya .	SELECT t1.votes_swing from bharatiya as t1 GROUP BY t1.votes_swing HAVING COUNT ( * ) = 10	1-149330-1
what are the year of bharatiya with seats won above the average seats won across all bharatiya ?	SELECT t1.year from bharatiya as t1 WHERE t1.seats_won > ( SELECT AVG ( t1.seats_won ) from bharatiya as t1	1-149330-1
how many bharatiya have year that contains 10 ?	SELECT COUNT ( * ) from bharatiya as t1 WHERE t1.year LIKE 10	1-149330-1
