return the callsign and city of license of list with the five lowest frequency .	SELECT t1.callsign , t1.city_of_license from list as t1 ORDER BY t1.frequency LIMIT 5	2-17982829-17
return the smallest frequency for every callsign .	SELECT MIN ( t1.frequency ) , t1.callsign from list as t1 GROUP BY t1.callsign	2-17982829-17
what is the brand and city of license of every list that has a frequency lower than average ?	SELECT t1.brand , t1.city_of_license from list as t1 WHERE t1.frequency < ( SELECT AVG ( t1.frequency ) {FROM, 3} )	2-17982829-17
return the different brand of list , in ascending order of frequency .	SELECT t1.brand from list as t1 GROUP BY t1.brand ORDER BY COUNT ( * ) ASC LIMIT 1	2-17982829-17
find the type of the list with the highest frequency.	SELECT t1.type from list as t1 WHERE t1.frequency = ( SELECT MAX ( t1.frequency ) from list as t1 )	2-17982829-17
what are the brand with exactly 10 list ?	SELECT t1.brand from list as t1 GROUP BY t1.brand HAVING COUNT ( * ) = 10	2-17982829-17
what are the brand and brand of the {COLUMN} who have frequency above five or frequency below ten ?	SELECT t1.brand , t1.brand from list as t1 WHERE t1.frequency > 5 OR t1.frequency < 10	2-17982829-17
list the callsign , callsign and the brand of the list .	SELECT t1.callsign , t1.callsign , t1.brand from list as t1	2-17982829-17
list the brand of {list which has number of list greater than 10 .	SELECT t1.brand from list as t1 GROUP BY t1.brand HAVING COUNT ( * ) > 10	2-17982829-17
what is the type of the list with least number of type ?	SELECT t1.type from list as t1 GROUP BY t1.type ORDER BY COUNT ( * ) ASC LIMIT 1	2-17982829-17
