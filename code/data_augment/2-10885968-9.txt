what is the home team of highest crowd ?	SELECT t1.home_team from 1976 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10885968-9
which home team has both 1976 with less than 10 crowd and 1976 with more than 15 crowd ?	SELECT t1.home_team from 1976 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1976 as t1 WHERE t1.crowd > 15	2-10885968-9
give the maximum and minimum crowd of all 1976 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1976 as t1	2-10885968-9
what is the venue and away team of every 1976 that has a crowd lower than average ?	SELECT t1.venue , t1.away_team from 1976 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10885968-9
which date is the most frequent date?	SELECT t1.date from 1976 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-10885968-9
what are the maximum and minimum crowd across all 1976 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1976 as t1	2-10885968-9
show the home team of the 1976 that has the most 1976 .	SELECT t1.home_team from 1976 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10885968-9
find the number of 1976 that have more than 10 home team score .	SELECT COUNT ( * ) from 1976 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) > 10 	2-10885968-9
Return all columns in 1976 .	SELECT * FROM 1976	2-10885968-9
find the date and away team score of the 1976 whose crowd is lower than the average crowd of all 1976 .	SELECT t1.date , t1.away_team_score from 1976 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10885968-9
