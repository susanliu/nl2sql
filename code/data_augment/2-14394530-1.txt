list the result , competition and the date of the ricardo .	SELECT t1.result , t1.competition , t1.date from ricardo as t1	2-14394530-1
sort the list of date and result of all ricardo in the descending order of goal .	SELECT t1.date , t1.result from ricardo as t1 ORDER BY t1.goal DESC	2-14394530-1
show all information on the ricardo that has the largest number of venue.	SELECT * from ricardo as t1 ORDER BY t1.venue DESC LIMIT 1	2-14394530-1
how many different result correspond to each result ?	SELECT t1.result , COUNT ( DISTINCT t1.result ) from ricardo as t1 GROUP BY t1.result	2-14394530-1
find the distinct COLUMN_NAME,0} of all ricardo that have goal higher than some ricardo from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.competition from ricardo as t1 WHERE t1.goal > ( SELECT MIN ( t1.competition ) from ricardo as t1 WHERE t1.venue = 10 )	2-14394530-1
find the score of ricardo which are date 10 but not date 52 .	SELECT t1.score from ricardo as t1 WHERE t1.date = 10 EXCEPT SELECT t1.score from ricardo as t1 WHERE t1.date = 52	2-14394530-1
show the result shared by more than 10 ricardo .	SELECT t1.result from ricardo as t1 GROUP BY t1.result HAVING COUNT ( * ) > 10	2-14394530-1
what are the result and score of the {COLUMN} who have goal above five or goal below ten ?	SELECT t1.result , t1.score from ricardo as t1 WHERE t1.goal > 5 OR t1.goal < 10	2-14394530-1
what is the venue of the ricardo with least number of venue ?	SELECT t1.venue from ricardo as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) ASC LIMIT 1	2-14394530-1
Return all columns in ricardo .	SELECT * FROM ricardo	2-14394530-1
