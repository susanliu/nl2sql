list opponent and result who have attendance greater than 5 or week shorter than 10 .	SELECT t1.opponent , t1.result from 1981 as t1 WHERE t1.attendance > 5 OR t1.week < 10	2-18847715-2
how many 1981 are there that have more than {VALUE},0 result ?	SELECT COUNT ( * ) from 1981 as t1 GROUP BY t1.result HAVING COUNT ( * ) > 10 	2-18847715-2
find the result of the 1981 which have opponent 10 but not 11 .	SELECT t1.result from 1981 as t1 WHERE t1.opponent = 10 EXCEPT SELECT t1.result from 1981 as t1 WHERE t1.opponent = 11	2-18847715-2
what is the minimum attendance in each opponent ?	SELECT MIN ( t1.attendance ) , t1.opponent from 1981 as t1 GROUP BY t1.opponent	2-18847715-2
what is the opponent of the 1981 with the smallest week ?	SELECT t1.opponent from 1981 as t1 ORDER BY t1.week ASC LIMIT 1	2-18847715-2
what are the date and result of all 1981 sorted by decreasing week ?	SELECT t1.date , t1.result from 1981 as t1 ORDER BY t1.week DESC	2-18847715-2
what is the opponent and date of the 1981 with maximum attendance ?	SELECT t1.opponent , t1.date from 1981 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) {FROM, 3} )	2-18847715-2
show the result and opponent with at least 10 opponent .	SELECT t1.result , t1.opponent from 1981 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-18847715-2
list the result which average attendance is above 10 .	SELECT t1.result from 1981 as t1 GROUP BY t1.result HAVING AVG ( t1.attendance ) >= 10	2-18847715-2
list the result , opponent and the result of the 1981 .	SELECT t1.result , t1.opponent , t1.result from 1981 as t1	2-18847715-2
