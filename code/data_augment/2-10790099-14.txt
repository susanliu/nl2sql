please show the home team of the 1932 that have at least 10 records .	SELECT t1.home_team from 1932 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10790099-14
what are the average crowd of 1932 for different venue ?	SELECT AVG ( t1.crowd ) , t1.venue from 1932 as t1 GROUP BY t1.venue	2-10790099-14
what are the away team score of all 1932 with away team that is 10 ?	SELECT t1.away_team_score from 1932 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) = 10	2-10790099-14
give the t1.home_team with the fewest 1932 .	SELECT t1.home_team from 1932 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) LIMIT 1	2-10790099-14
list all information about 1932 .	SELECT * FROM 1932	2-10790099-14
find the date of the 1932 which have away team score 10 but not 92 .	SELECT t1.date from 1932 as t1 WHERE t1.away_team_score = 10 EXCEPT SELECT t1.date from 1932 as t1 WHERE t1.away_team_score = 92	2-10790099-14
find the distinct away team score of all 1932 that have a higher crowd than some 1932 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.away_team_score from 1932 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.away_team_score ) from 1932 as t1 WHERE t1.date = 10 )	2-10790099-14
what is the date and home team of every 1932 that has a crowd lower than average ?	SELECT t1.date , t1.home_team from 1932 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10790099-14
find the venue of the 1932 that have just 10 1932 .	SELECT t1.venue from 1932 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10790099-14
what are the maximum and minimum crowd across all 1932 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1932 as t1	2-10790099-14
