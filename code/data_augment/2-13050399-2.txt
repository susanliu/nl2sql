find the location of the list which have location 10 but not 5 .	SELECT t1.location from list as t1 WHERE t1.location = 10 EXCEPT SELECT t1.location from list as t1 WHERE t1.location = 5	2-13050399-2
what is the rebuilt and subsuming parish of every list that has a date of demolition lower than average ?	SELECT t1.rebuilt , t1.subsuming_parish from list as t1 WHERE t1.date_of_demolition < ( SELECT AVG ( t1.date_of_demolition ) {FROM, 3} )	2-13050399-2
show subsuming parish and church name of list .	SELECT t1.subsuming_parish , t1.church_name from list as t1	2-13050399-2
what are the rebuilt of all list with location that is 10 ?	SELECT t1.rebuilt from list as t1 GROUP BY t1.location HAVING COUNT ( * ) = 10	2-13050399-2
find the rebuilt of list whose date of demolition is higher than the average date of demolition .	SELECT t1.rebuilt from list as t1 WHERE t1.date_of_demolition > ( SELECT AVG ( t1.date_of_demolition ) from list as t1	2-13050399-2
find the location of list who have both 10 and 44 date of demolition .	SELECT t1.location from list as t1 WHERE t1.date_of_demolition = 10 INTERSECT SELECT t1.location from list as t1 WHERE t1.date_of_demolition = 44	2-13050399-2
show the subsuming parish with fewer than 3 list .	SELECT t1.subsuming_parish from list as t1 GROUP BY t1.subsuming_parish HAVING COUNT ( * ) < 3	2-13050399-2
what are the church name of list whose rebuilt is not 10 ?	SELECT t1.church_name from list as t1 WHERE t1.rebuilt ! = 10	2-13050399-2
what is the maximum and mininum date of demolition {COLUMN} for all list ?	SELECT MAX ( t1.date_of_demolition ) , MIN ( t1.date_of_demolition ) from list as t1	2-13050399-2
show the rebuilt and the number of unique rebuilt containing each rebuilt .	SELECT t1.rebuilt , COUNT ( DISTINCT t1.rebuilt ) from list as t1 GROUP BY t1.rebuilt	2-13050399-2
