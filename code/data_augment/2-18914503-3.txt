Return all columns in list .	SELECT * FROM list	2-18914503-3
what are the year ( s ) of manufacture , year ( s ) of manufacture , and railway number ( s ) of each list ?	SELECT t1.year_ , t1.year_ , t1.railway_number_ from list as t1	2-18914503-3
which t1.type has least number of list ?	SELECT t1.type from list as t1 GROUP BY t1.type ORDER BY COUNT ( * ) LIMIT 1	2-18914503-3
what is all the information on the list with the largest number of railway number ( s ) ?	SELECT * from list as t1 ORDER BY t1.railway_number_ DESC LIMIT 1	2-18914503-3
what are the distinct railway number ( s ) with quantity between 10 and 1 ?	SELECT DISTINCT t1.railway_number_ from list as t1 WHERE t1.quantity BETWEEN 10 AND 1	2-18914503-3
what is the type and class ( old ) to 1868 of the list with maximum quantity ?	SELECT t1.type , t1.class_ from list as t1 WHERE t1.quantity = ( SELECT MAX ( t1.quantity ) {FROM, 3} )	2-18914503-3
what are the average quantity of list for different class ( old ) to 1868 ?	SELECT AVG ( t1.quantity ) , t1.class_ from list as t1 GROUP BY t1.class_	2-18914503-3
give the class ( old ) to 1868 that has the most list .	SELECT t1.class_ from list as t1 GROUP BY t1.class_ ORDER BY COUNT ( * ) DESC LIMIT 1	2-18914503-3
which class ( old ) to 1868 has both list with less than 10 quantity and list with more than 40 quantity ?	SELECT t1.class_ from list as t1 WHERE t1.quantity < 10 INTERSECT SELECT t1.class_ from list as t1 WHERE t1.quantity > 40	2-18914503-3
how many list ' year ( s ) of manufacture have the word 10 in them ?	SELECT COUNT ( * ) from list as t1 WHERE t1.year_ LIKE 10	2-18914503-3
