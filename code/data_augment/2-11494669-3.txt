find the date of the 2005 that is most frequent across all date .	SELECT t1.date from 2005 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-11494669-3
give the t1.loss with the fewest 2005 .	SELECT t1.loss from 2005 as t1 GROUP BY t1.loss ORDER BY COUNT ( * ) LIMIT 1	2-11494669-3
give the opponent that has the most 2005 .	SELECT t1.opponent from 2005 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-11494669-3
which date has the least attendance ?	SELECT t1.date from 2005 as t1 ORDER BY t1.attendance ASC LIMIT 1	2-11494669-3
what is the score of the 2005 with the minimum attendance ?	SELECT t1.score from 2005 as t1 ORDER BY t1.attendance ASC LIMIT 1	2-11494669-3
what is the loss and score of the 2005 with maximum attendance ?	SELECT t1.loss , t1.score from 2005 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) {FROM, 3} )	2-11494669-3
what is the loss and boxscore for the 2005 with the rank 5 smallest attendance ?	SELECT t1.loss , t1.boxscore from 2005 as t1 ORDER BY t1.attendance LIMIT 5	2-11494669-3
return the date of the 2005 that has the fewest corresponding date .	SELECT t1.date from 2005 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-11494669-3
find the record of the 2005 who has the largest number of 2005 .	SELECT t1.record from 2005 as t1 GROUP BY t1.record ORDER BY COUNT ( * ) DESC LIMIT 1	2-11494669-3
please list the opponent and score of 2005 in descending order of attendance .	SELECT t1.opponent , t1.score from 2005 as t1 ORDER BY t1.attendance DESC	2-11494669-3
