find the goal differential of the niger with the highest losses .	SELECT t1.goal_differential from niger as t1 ORDER BY t1.losses DESC LIMIT 1	2-1238866-1
return the different goal differential of niger , in ascending order of frequency .	SELECT t1.goal_differential from niger as t1 GROUP BY t1.goal_differential ORDER BY COUNT ( * ) ASC LIMIT 1	2-1238866-1
what are the goal differential of the niger with goal differential other than 10 ?	SELECT t1.goal_differential from niger as t1 WHERE t1.goal_differential ! = 10	2-1238866-1
show all goal differential and corresponding number of niger sorted by the count .	SELECT t1.goal_differential , COUNT ( * ) from niger as t1 GROUP BY t1.goal_differential ORDER BY COUNT ( * )	2-1238866-1
what are the goal differential and goal differential of each niger ?	SELECT t1.goal_differential , t1.goal_differential from niger as t1	2-1238866-1
what are the goal differential of all niger with goal differential that is 10 ?	SELECT t1.goal_differential from niger as t1 GROUP BY t1.goal_differential HAVING COUNT ( * ) = 10	2-1238866-1
what is the goal differential and goal differential of the niger with maximum losses ?	SELECT t1.goal_differential , t1.goal_differential from niger as t1 WHERE t1.losses = ( SELECT MAX ( t1.losses ) {FROM, 3} )	2-1238866-1
what is the goal differential of the niger with the minimum games ?	SELECT t1.goal_differential from niger as t1 ORDER BY t1.games ASC LIMIT 1	2-1238866-1
what are the goal differential and goal differential of the {COLUMN} who have wins above five or draws below ten ?	SELECT t1.goal_differential , t1.goal_differential from niger as t1 WHERE t1.wins > 5 OR t1.draws < 10	2-1238866-1
what are the goal differential of niger whose goal differential are not 10 ?	SELECT t1.goal_differential from niger as t1 WHERE t1.goal_differential ! = 10	2-1238866-1
