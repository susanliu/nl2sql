find the number of list whose country contain the word 10 .	SELECT COUNT ( * ) from list as t1 WHERE t1.country LIKE 10	2-19001175-1
how many list have points that contains 10 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.points LIKE 10	2-19001175-1
which race entries ( starts ) has most number of list ?	SELECT t1.race_entries_ from list as t1 GROUP BY t1.race_entries_ ORDER BY COUNT ( * ) DESC LIMIT 1	2-19001175-1
show the seasons with fewer than 3 list .	SELECT t1.seasons from list as t1 GROUP BY t1.seasons HAVING COUNT ( * ) < 3	2-19001175-1
what is the seasons of list with the maximum podiums across all list ?	SELECT t1.seasons from list as t1 WHERE t1.podiums = ( SELECT MAX ( t1.podiums ) from list as t1 )	2-19001175-1
how many list have championship titles that contain the word 10 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.championship_titles LIKE 10	2-19001175-1
find the country of list who have both 10 and 84 podiums .	SELECT t1.country from list as t1 WHERE t1.podiums = 10 INTERSECT SELECT t1.country from list as t1 WHERE t1.podiums = 84	2-19001175-1
what are the average podiums of list for different country ?	SELECT AVG ( t1.podiums ) , t1.country from list as t1 GROUP BY t1.country	2-19001175-1
what are the championship titles of all list with race entries ( starts ) that is 10 ?	SELECT t1.championship_titles from list as t1 GROUP BY t1.race_entries_ HAVING COUNT ( * ) = 10	2-19001175-1
find the seasons of the list with the highest fastest laps.	SELECT t1.seasons from list as t1 WHERE t1.fastest_laps = ( SELECT MAX ( t1.fastest_laps ) from list as t1 )	2-19001175-1
