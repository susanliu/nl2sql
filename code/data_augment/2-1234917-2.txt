how many jean-marc correspond to each class pos .? show the result in ascending order.	SELECT t1.class_pos_ , COUNT ( * ) from jean-marc as t1 GROUP BY t1.class_pos_ ORDER BY COUNT ( * )	2-1234917-2
what are the maximum and minimum year across all jean-marc ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from jean-marc as t1	2-1234917-2
show all information on the jean-marc that has the largest number of co-drivers.	SELECT * from jean-marc as t1 ORDER BY t1.co-drivers DESC LIMIT 1	2-1234917-2
what is the count of jean-marc with more than 10 class ?	SELECT COUNT ( * ) from jean-marc as t1 GROUP BY t1.class HAVING COUNT ( * ) > 10 	2-1234917-2
what are the maximum and minimum laps across all jean-marc ?	SELECT MAX ( t1.laps ) , MIN ( t1.laps ) from jean-marc as t1	2-1234917-2
how many jean-marc are there in class pos . 10 or 34 ?	SELECT COUNT ( * ) from jean-marc as t1 WHERE t1.class_pos_ = 10 OR t1.class_pos_ = 34	2-1234917-2
count the number of jean-marc that have an team containing 10 .	SELECT COUNT ( * ) from jean-marc as t1 WHERE t1.team LIKE 10	2-1234917-2
what are total year for each team ?	SELECT t1.team , SUM ( t1.year ) from jean-marc as t1 GROUP BY t1.team	2-1234917-2
find the team of the jean-marc that have just 10 jean-marc .	SELECT t1.team from jean-marc as t1 GROUP BY t1.team HAVING COUNT ( * ) = 10	2-1234917-2
what is the minimum year in each class pos . ?	SELECT MIN ( t1.year ) , t1.class_pos_ from jean-marc as t1 GROUP BY t1.class_pos_	2-1234917-2
