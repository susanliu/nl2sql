find the airdate of happy which are rating/share ( 18 – 49 ) 10 but not rating/share ( 18 – 49 ) 39 .	SELECT t1.airdate from happy as t1 WHERE t1.rating/share_ = 10 EXCEPT SELECT t1.airdate from happy as t1 WHERE t1.rating/share_ = 39	1-22822468-2
what is the minimum order in each rating ?	SELECT MIN ( t1.order ) , t1.rating from happy as t1 GROUP BY t1.rating	1-22822468-2
find the rating/share ( 18 – 49 ) and episode of the happy with at least 10 rating/share ( 18 – 49 ) .	SELECT t1.rating/share_ , t1.episode from happy as t1 GROUP BY t1.rating/share_ HAVING COUNT ( * ) >= 10	1-22822468-2
which t1.airdate has the fewest happy ?	SELECT t1.airdate from happy as t1 GROUP BY t1.airdate ORDER BY COUNT ( * ) LIMIT 1	1-22822468-2
what are the rating and rating/share ( 18 – 49 ) of all happy sorted by decreasing rank ( night ) ?	SELECT t1.rating , t1.rating/share_ from happy as t1 ORDER BY t1.rank_ DESC	1-22822468-2
list the episode and rating of all happy sorted by rank ( night ) in descending order .	SELECT t1.episode , t1.rating from happy as t1 ORDER BY t1.rank_ DESC	1-22822468-2
what are the viewers ( millions ) of all happy that have 10 or more happy ?	SELECT t1.viewers_ from happy as t1 GROUP BY t1.viewers_ HAVING COUNT ( * ) >= 10	1-22822468-2
please list the episode and episode of happy in descending order of share .	SELECT t1.episode , t1.episode from happy as t1 ORDER BY t1.share DESC	1-22822468-2
which airdate have an average rank ( night ) over 10 ?	SELECT t1.airdate from happy as t1 GROUP BY t1.airdate HAVING AVG ( t1.rank_ ) >= 10	1-22822468-2
which t1.episode has the smallest amount of happy?	SELECT t1.episode from happy as t1 GROUP BY t1.episode ORDER BY COUNT ( * ) LIMIT 1	1-22822468-2
