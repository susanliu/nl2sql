what are the away team score and away team score of each 1957 , listed in descending order by crowd ?	SELECT t1.away_team_score , t1.away_team_score from 1957 as t1 ORDER BY t1.crowd DESC	2-10774891-3
find the home team score of 1957 which have both 10 and 92 as home team score .	SELECT t1.home_team_score from 1957 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.home_team_score from 1957 as t1 WHERE t1.crowd = 92	2-10774891-3
what is the maximum and mininum crowd {COLUMN} for all 1957 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1957 as t1	2-10774891-3
which venue have an average crowd over 10 ?	SELECT t1.venue from 1957 as t1 GROUP BY t1.venue HAVING AVG ( t1.crowd ) >= 10	2-10774891-3
sort the list of away team score and venue of all 1957 in the descending order of crowd .	SELECT t1.away_team_score , t1.venue from 1957 as t1 ORDER BY t1.crowd DESC	2-10774891-3
how many 1957 does each home team score have ?	SELECT t1.home_team_score , COUNT ( * ) from 1957 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * )	2-10774891-3
which t1.away_team has the fewest 1957 ?	SELECT t1.away_team from 1957 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) LIMIT 1	2-10774891-3
find the home team score who has exactly 10 1957 .	SELECT t1.home_team_score from 1957 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) = 10	2-10774891-3
what are the away team score of the 1957 with away team score other than 10 ?	SELECT t1.away_team_score from 1957 as t1 WHERE t1.home_team_score ! = 10	2-10774891-3
what is all the information on the 1957 with the largest number of home team ?	SELECT * from 1957 as t1 ORDER BY t1.home_team DESC LIMIT 1	2-10774891-3
