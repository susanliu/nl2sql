what is the t1.home_team_score of 1938 that has fewest number of 1938 ?	SELECT t1.home_team_score from 1938 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10806592-1
show all information on the 1938 that has the largest number of away team.	SELECT * from 1938 as t1 ORDER BY t1.away_team DESC LIMIT 1	2-10806592-1
how many home team did each 1938 do, ordered by number of home team ?	SELECT t1.home_team , COUNT ( * ) from 1938 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * )	2-10806592-1
return the maximum and minimum crowd across all 1938 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1938 as t1	2-10806592-1
find the venue of 1938 who have crowd of both 10 and 58 .	SELECT t1.venue from 1938 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.venue from 1938 as t1 WHERE t1.crowd = 58	2-10806592-1
show the venue of 1938 who have at least 10 1938 .	SELECT t1.venue from 1938 as t1 GROUP BY t1.venue HAVING COUNT ( * ) >= 10	2-10806592-1
list the venue , date and the away team of the 1938 .	SELECT t1.venue , t1.date , t1.away_team from 1938 as t1	2-10806592-1
what is the away team of the 1938 with the smallest crowd ?	SELECT t1.away_team from 1938 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10806592-1
what are the distinct COLUMN_NAME,0} of every 1938 that has a greater crowd than some 1938 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.venue from 1938 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.venue ) from 1938 as t1 WHERE t1.date = 10 )	2-10806592-1
what are the home team of 1938 , sorted by their frequency?	SELECT t1.home_team from 1938 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10806592-1
