please show the result of the zorro that have at least 10 records .	SELECT t1.result from zorro as t1 GROUP BY t1.result HAVING COUNT ( * ) >= 10	2-17143308-1
what is the maximum and mininum year {COLUMN} for all zorro ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from zorro as t1	2-17143308-1
what are the result that have greater year than any year in zorro ?	SELECT t1.result from zorro as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from zorro as t1 )	2-17143308-1
how many zorro does each award have ?	SELECT t1.award , COUNT ( * ) from zorro as t1 GROUP BY t1.award ORDER BY COUNT ( * )	2-17143308-1
show the nominee of zorro who have at least 10 zorro .	SELECT t1.nominee from zorro as t1 GROUP BY t1.nominee HAVING COUNT ( * ) >= 10	2-17143308-1
what are the distinct COLUMN_NAME,0} of every zorro that has a greater year than some zorro with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.award from zorro as t1 WHERE t1.year > ( SELECT MIN ( t1.award ) from zorro as t1 WHERE t1.result = 10 )	2-17143308-1
what is the category and award of the zorro with maximum year ?	SELECT t1.category , t1.award from zorro as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-17143308-1
which category has both zorro with less than 10 year and zorro with more than 92 year ?	SELECT t1.category from zorro as t1 WHERE t1.year < 10 INTERSECT SELECT t1.category from zorro as t1 WHERE t1.year > 92	2-17143308-1
what is the award and award of every zorro that has a year lower than average ?	SELECT t1.award , t1.award from zorro as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-17143308-1
how many nominee did each zorro do, ordered by number of nominee ?	SELECT t1.nominee , COUNT ( * ) from zorro as t1 GROUP BY t1.nominee ORDER BY COUNT ( * )	2-17143308-1
