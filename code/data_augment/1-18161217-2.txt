what are the launch date and launch date ?	SELECT t1.launch_date , t1.launch_date from none as t1	1-18161217-2
show all information on the none that has the largest number of cospar id.	SELECT * from none as t1 ORDER BY t1.cospar_id DESC LIMIT 1	1-18161217-2
find the distinct satellite of all none that have a higher product number than some none with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.satellite from none as t1 WHERE t1.product_number > ( SELECT MIN ( t1.satellite ) from none as t1 WHERE t1.estimated_end_date_ = 10 )	1-18161217-2
count the number of none in cospar id 10 or 90 .	SELECT COUNT ( * ) from none as t1 WHERE t1.cospar_id = 10 OR t1.cospar_id = 90	1-18161217-2
show all launch date and corresponding number of none sorted by the count .	SELECT t1.launch_date , COUNT ( * ) from none as t1 GROUP BY t1.launch_date ORDER BY COUNT ( * )	1-18161217-2
what is the count of none with more than 10 estimated end date [ clarification needed ] ?	SELECT COUNT ( * ) from none as t1 GROUP BY t1.estimated_end_date_ HAVING COUNT ( * ) > 10 	1-18161217-2
what are the launch date with exactly 10 none ?	SELECT t1.launch_date from none as t1 GROUP BY t1.launch_date HAVING COUNT ( * ) = 10	1-18161217-2
show the cospar id and the number of none for each cospar id in the ascending order.	SELECT t1.cospar_id , COUNT ( * ) from none as t1 GROUP BY t1.cospar_id ORDER BY COUNT ( * )	1-18161217-2
how many none are there in cospar id 10 or 90 ?	SELECT COUNT ( * ) from none as t1 WHERE t1.cospar_id = 10 OR t1.cospar_id = 90	1-18161217-2
list all satellite which have satcat no . higher than the average .	SELECT t1.satellite from none as t1 WHERE t1.satcat_no_ > ( SELECT AVG ( t1.satcat_no_ ) from none as t1	1-18161217-2
