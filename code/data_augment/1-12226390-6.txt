find the directed by of the none with the largest production code .	SELECT t1.directed_by from none as t1 ORDER BY t1.production_code DESC LIMIT 1	1-12226390-6
what are the directed by of all none with written by that is 10 ?	SELECT t1.directed_by from none as t1 GROUP BY t1.written_by HAVING COUNT ( * ) = 10	1-12226390-6
how many none are there that have more than {VALUE},0 directed by ?	SELECT COUNT ( * ) from none as t1 GROUP BY t1.directed_by HAVING COUNT ( * ) > 10 	1-12226390-6
how many different directed by correspond to each directed by ?	SELECT t1.directed_by , COUNT ( DISTINCT t1.directed_by ) from none as t1 GROUP BY t1.directed_by	1-12226390-6
show the original air date and written by with at least 10 directed by .	SELECT t1.original_air_date , t1.written_by from none as t1 GROUP BY t1.directed_by HAVING COUNT ( * ) >= 10	1-12226390-6
return the different directed by of none , in ascending order of frequency .	SELECT t1.directed_by from none as t1 GROUP BY t1.directed_by ORDER BY COUNT ( * ) ASC LIMIT 1	1-12226390-6
return the smallest no . in series for every directed by .	SELECT MIN ( t1.no_._in_series ) , t1.directed_by from none as t1 GROUP BY t1.directed_by	1-12226390-6
show the written by and the corresponding number of none sorted by the number of written by in ascending order .	SELECT t1.written_by , COUNT ( * ) from none as t1 GROUP BY t1.written_by ORDER BY COUNT ( * )	1-12226390-6
show the directed by with fewer than 3 none .	SELECT t1.directed_by from none as t1 GROUP BY t1.directed_by HAVING COUNT ( * ) < 3	1-12226390-6
list the written by of {none which has number of none greater than 10 .	SELECT t1.written_by from none as t1 GROUP BY t1.written_by HAVING COUNT ( * ) > 10	1-12226390-6
