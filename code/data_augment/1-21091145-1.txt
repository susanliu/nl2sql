return the date and opponent of 1912 with the five lowest opponents .	SELECT t1.date , t1.opponent from 1912 as t1 ORDER BY t1.opponents LIMIT 5	1-21091145-1
what are the opponent of all 1912 with result that is 10 ?	SELECT t1.opponent from 1912 as t1 GROUP BY t1.result HAVING COUNT ( * ) = 10	1-21091145-1
what is the opponent of all 1912 whose game is higher than any 1912 ?	SELECT t1.opponent from 1912 as t1 WHERE t1.game > ( SELECT MIN ( t1.game ) from 1912 as t1 )	1-21091145-1
how many different opponent correspond to each result ?	SELECT t1.result , COUNT ( DISTINCT t1.opponent ) from 1912 as t1 GROUP BY t1.result	1-21091145-1
give the t1.result with the fewest 1912 .	SELECT t1.result from 1912 as t1 GROUP BY t1.result ORDER BY COUNT ( * ) LIMIT 1	1-21091145-1
what are the result of 1912 whose record is not 10 ?	SELECT t1.result from 1912 as t1 WHERE t1.record ! = 10	1-21091145-1
how many 1912 correspond to each record? show the result in ascending order.	SELECT t1.record , COUNT ( * ) from 1912 as t1 GROUP BY t1.record ORDER BY COUNT ( * )	1-21091145-1
please show the record of the 1912 that have at least 10 records .	SELECT t1.record from 1912 as t1 GROUP BY t1.record HAVING COUNT ( * ) >= 10	1-21091145-1
which t1.date has the fewest 1912 ?	SELECT t1.date from 1912 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) LIMIT 1	1-21091145-1
which result have an average game over 10 ?	SELECT t1.result from 1912 as t1 GROUP BY t1.result HAVING AVG ( t1.game ) >= 10	1-21091145-1
