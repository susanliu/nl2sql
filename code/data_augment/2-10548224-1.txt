which winning team have greater year than that of any year in best ?	SELECT t1.winning_team from best as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from best as t1 )	2-10548224-1
which losing team has both best with less than 10 year and best with more than 72 year ?	SELECT t1.losing_team from best as t1 WHERE t1.year < 10 INTERSECT SELECT t1.losing_team from best as t1 WHERE t1.year > 72	2-10548224-1
return the winning team of the best that has the fewest corresponding winning team .	SELECT t1.winning_team from best as t1 GROUP BY t1.winning_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10548224-1
what is all the information on the best with the largest number of losing team ?	SELECT * from best as t1 ORDER BY t1.losing_team DESC LIMIT 1	2-10548224-1
give the maximum and minimum year of all best .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from best as t1	2-10548224-1
please show the sport of the best that have at least 10 records .	SELECT t1.sport from best as t1 GROUP BY t1.sport HAVING COUNT ( * ) >= 10	2-10548224-1
how many different final score correspond to each final score ?	SELECT t1.final_score , COUNT ( DISTINCT t1.final_score ) from best as t1 GROUP BY t1.final_score	2-10548224-1
what is the maximum and mininum year {COLUMN} for all best ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from best as t1	2-10548224-1
what is the count of best with more than 10 losing team ?	SELECT COUNT ( * ) from best as t1 GROUP BY t1.losing_team HAVING COUNT ( * ) > 10 	2-10548224-1
which final score has both best with less than 10 year and best with more than 44 year ?	SELECT t1.final_score from best as t1 WHERE t1.year < 10 INTERSECT SELECT t1.final_score from best as t1 WHERE t1.year > 44	2-10548224-1
