Show everything on list	SELECT * FROM list	2-10966872-5
give the t1.record_set with the fewest list .	SELECT t1.record_set from list as t1 GROUP BY t1.record_set ORDER BY COUNT ( * ) LIMIT 1	2-10966872-5
what are the maximum and minimum year across all list ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from list as t1	2-10966872-5
how many list are there that have more than {VALUE},0 notes ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.notes HAVING COUNT ( * ) > 10 	2-10966872-5
what is the notes and superlative for the list with the rank 5 smallest year ?	SELECT t1.notes , t1.superlative from list as t1 ORDER BY t1.year LIMIT 5	2-10966872-5
what are the distinct COLUMN_NAME,0} of list with year higher than any list from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.actress from list as t1 WHERE t1.year > ( SELECT MIN ( t1.actress ) from list as t1 WHERE t1.actress = 10 )	2-10966872-5
how many list correspond to each actress? show the result in ascending order.	SELECT t1.actress , COUNT ( * ) from list as t1 GROUP BY t1.actress ORDER BY COUNT ( * )	2-10966872-5
find the distinct actress of list having year between 10 and 9 .	SELECT DISTINCT t1.actress from list as t1 WHERE t1.year BETWEEN 10 AND 9	2-10966872-5
find the distinct superlative of list having year between 10 and 33 .	SELECT DISTINCT t1.superlative from list as t1 WHERE t1.year BETWEEN 10 AND 33	2-10966872-5
find the actress and record set of the list whose year is lower than the average year of all list .	SELECT t1.actress , t1.record_set from list as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-10966872-5
