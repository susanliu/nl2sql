show the birthplace of list whose club/team are not 10.	SELECT t1.birthplace from list as t1 WHERE t1.club/team ! = 10	2-15715109-8
how many list are there in name 10 or 4 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.name = 10 OR t1.name = 4	2-15715109-8
find the club/team of list who have more than 10 list .	SELECT t1.club/team from list as t1 GROUP BY t1.club/team HAVING COUNT ( * ) > 10	2-15715109-8
what are the club/team and position of each list ?	SELECT t1.club/team , t1.position from list as t1	2-15715109-8
return the different birthplace of list , in ascending order of frequency .	SELECT t1.birthplace from list as t1 GROUP BY t1.birthplace ORDER BY COUNT ( * ) ASC LIMIT 1	2-15715109-8
what are the distinct birthdate with jersey # between 10 and 96 ?	SELECT DISTINCT t1.birthdate from list as t1 WHERE t1.jersey_ BETWEEN 10 AND 96	2-15715109-8
find the position of the list that have just 10 list .	SELECT t1.position from list as t1 GROUP BY t1.position HAVING COUNT ( * ) = 10	2-15715109-8
list the club/team which average jersey # is above 10 .	SELECT t1.club/team from list as t1 GROUP BY t1.club/team HAVING AVG ( t1.jersey_ ) >= 10	2-15715109-8
what are the birthdate of all list with birthdate that is 10 ?	SELECT t1.birthdate from list as t1 GROUP BY t1.birthdate HAVING COUNT ( * ) = 10	2-15715109-8
what are the club/team of the list that have exactly 10 list ?	SELECT t1.club/team from list as t1 GROUP BY t1.club/team HAVING COUNT ( * ) = 10	2-15715109-8
