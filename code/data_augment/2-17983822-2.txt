what is the minimum week in each opponent ?	SELECT MIN ( t1.week ) , t1.opponent from 2003 as t1 GROUP BY t1.opponent	2-17983822-2
what is the minimum week in each attendance ?	SELECT MIN ( t1.week ) , t1.attendance from 2003 as t1 GROUP BY t1.attendance	2-17983822-2
please list the result and date of 2003 in descending order of week .	SELECT t1.result , t1.date from 2003 as t1 ORDER BY t1.week DESC	2-17983822-2
what is all the information on the 2003 with the largest number of result ?	SELECT * from 2003 as t1 ORDER BY t1.result DESC LIMIT 1	2-17983822-2
list the date which average week is above 10 .	SELECT t1.date from 2003 as t1 GROUP BY t1.date HAVING AVG ( t1.week ) >= 10	2-17983822-2
which opponent is the most frequent opponent?	SELECT t1.opponent from 2003 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-17983822-2
how many 2003 does each kickoff time have ?	SELECT t1.kickoff_time , COUNT ( * ) from 2003 as t1 GROUP BY t1.kickoff_time ORDER BY COUNT ( * )	2-17983822-2
how many 2003 are there that have more than {VALUE},0 attendance ?	SELECT COUNT ( * ) from 2003 as t1 GROUP BY t1.attendance HAVING COUNT ( * ) > 10 	2-17983822-2
what are the average week of 2003 for different opponent ?	SELECT AVG ( t1.week ) , t1.opponent from 2003 as t1 GROUP BY t1.opponent	2-17983822-2
what is the kickoff time of the 2003 with the minimum week ?	SELECT t1.kickoff_time from 2003 as t1 ORDER BY t1.week ASC LIMIT 1	2-17983822-2
