find the distinct COLUMN_NAME,0} of all weightlifting that have total ( kg ) higher than some weightlifting from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.name from weightlifting as t1 WHERE t1.total_ > ( SELECT MIN ( t1.name ) from weightlifting as t1 WHERE t1.name = 10 )	2-11279593-1
what is all the information on the weightlifting with the largest number of name ?	SELECT * from weightlifting as t1 ORDER BY t1.name DESC LIMIT 1	2-11279593-1
which t1.name has the fewest weightlifting ?	SELECT t1.name from weightlifting as t1 GROUP BY t1.name ORDER BY COUNT ( * ) LIMIT 1	2-11279593-1
return the name of the weightlifting with the fewest total ( kg ) .	SELECT t1.name from weightlifting as t1 ORDER BY t1.total_ ASC LIMIT 1	2-11279593-1
how many weightlifting have name that contains 10 ?	SELECT COUNT ( * ) from weightlifting as t1 WHERE t1.name LIKE 10	2-11279593-1
find the name of the weightlifting with the highest total ( kg ).	SELECT t1.name from weightlifting as t1 WHERE t1.total_ = ( SELECT MAX ( t1.total_ ) from weightlifting as t1 )	2-11279593-1
what are the name of all weightlifting with name that is 10 ?	SELECT t1.name from weightlifting as t1 GROUP BY t1.name HAVING COUNT ( * ) = 10	2-11279593-1
find the name of weightlifting which are name 10 but not name 18 .	SELECT t1.name from weightlifting as t1 WHERE t1.name = 10 EXCEPT SELECT t1.name from weightlifting as t1 WHERE t1.name = 18	2-11279593-1
what is the name of all weightlifting whose clean & jerk is higher than any weightlifting ?	SELECT t1.name from weightlifting as t1 WHERE t1.clean_ > ( SELECT MIN ( t1.clean_ ) from weightlifting as t1 )	2-11279593-1
show the name with fewer than 3 weightlifting .	SELECT t1.name from weightlifting as t1 GROUP BY t1.name HAVING COUNT ( * ) < 3	2-11279593-1
