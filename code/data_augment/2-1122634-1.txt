what are the constructor and time/retired of 1975 with 10 or more driver ?	SELECT t1.constructor , t1.time/retired from 1975 as t1 GROUP BY t1.driver HAVING COUNT ( * ) >= 10	2-1122634-1
what are the driver of all 1975 that have 10 or more 1975 ?	SELECT t1.driver from 1975 as t1 GROUP BY t1.driver HAVING COUNT ( * ) >= 10	2-1122634-1
show driver and the number of distinct time/retired in each driver .	SELECT t1.driver , COUNT ( DISTINCT t1.time/retired ) from 1975 as t1 GROUP BY t1.driver	2-1122634-1
return each time/retired with the number of 1975 in ascending order of the number of time/retired .	SELECT t1.time/retired , COUNT ( * ) from 1975 as t1 GROUP BY t1.time/retired ORDER BY COUNT ( * )	2-1122634-1
what are the average laps of 1975 for different driver ?	SELECT AVG ( t1.laps ) , t1.driver from 1975 as t1 GROUP BY t1.driver	2-1122634-1
which constructor have an average laps over 10 ?	SELECT t1.constructor from 1975 as t1 GROUP BY t1.constructor HAVING AVG ( t1.laps ) >= 10	2-1122634-1
what are the maximum and minimum grid across all 1975 ?	SELECT MAX ( t1.grid ) , MIN ( t1.grid ) from 1975 as t1	2-1122634-1
what are the driver and driver of each 1975 , listed in descending order by grid ?	SELECT t1.driver , t1.driver from 1975 as t1 ORDER BY t1.grid DESC	2-1122634-1
find all time/retired that have fewer than three in 1975 .	SELECT t1.time/retired from 1975 as t1 GROUP BY t1.time/retired HAVING COUNT ( * ) < 3	2-1122634-1
find the number of 1975 whose constructor contain the word 10 .	SELECT COUNT ( * ) from 1975 as t1 WHERE t1.constructor LIKE 10	2-1122634-1
