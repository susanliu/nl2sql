what is all the information on the international with the largest number of world ranking ( 1 ) ?	SELECT * from international as t1 ORDER BY t1.world_ranking_ DESC LIMIT 1	2-12000368-1
what is the author / editor / source of the international with the smallest countries sampled ?	SELECT t1.author_/_editor_/_source from international as t1 ORDER BY t1.countries_sampled ASC LIMIT 1	2-12000368-1
what is the author / editor / source of the international with the minimum countries sampled ?	SELECT t1.author_/_editor_/_source from international as t1 ORDER BY t1.countries_sampled ASC LIMIT 1	2-12000368-1
which ranking l.a. ( 2 ) have greater countries sampled than that of any countries sampled in international ?	SELECT t1.ranking_l.a._ from international as t1 WHERE t1.countries_sampled > ( SELECT MIN ( t1.countries_sampled ) from international as t1 )	2-12000368-1
how many international are there that have more than {VALUE},0 author / editor / source ?	SELECT COUNT ( * ) from international as t1 GROUP BY t1.author_/_editor_/_source HAVING COUNT ( * ) > 10 	2-12000368-1
Show everything on international	SELECT * FROM international	2-12000368-1
find all index ( year ) that have fewer than three in international .	SELECT t1.index_ from international as t1 GROUP BY t1.index_ HAVING COUNT ( * ) < 3	2-12000368-1
find the index ( year ) which have exactly 10 international .	SELECT t1.index_ from international as t1 GROUP BY t1.index_ HAVING COUNT ( * ) = 10	2-12000368-1
what is the index ( year ) of all international whose countries sampled is higher than any international ?	SELECT t1.index_ from international as t1 WHERE t1.countries_sampled > ( SELECT MIN ( t1.countries_sampled ) from international as t1 )	2-12000368-1
return the index ( year ) and ranking l.a. ( 2 ) of international with the five lowest countries sampled .	SELECT t1.index_ , t1.ranking_l.a._ from international as t1 ORDER BY t1.countries_sampled LIMIT 5	2-12000368-1
