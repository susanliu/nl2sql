what is the minimum 2007 headcount in each college ?	SELECT MIN ( t1.2007_headcount ) , t1.college from list as t1 GROUP BY t1.college	1-22308881-2
show all information on the list that has the largest number of college.	SELECT * from list as t1 ORDER BY t1.college DESC LIMIT 1	1-22308881-2
what is the in-county tuition per credit hour ( fall 2009 ) of the list with the largest county population 18 yrs + ?	SELECT t1.in-county_tuition_per_credit_hour_ from list as t1 WHERE t1.county_population_18_yrs_+ = ( SELECT MAX ( t1.county_population_18_yrs_+ ) from list as t1 )	1-22308881-2
show college and college of list .	SELECT t1.college , t1.college from list as t1	1-22308881-2
what is the college of the list with least number of college ?	SELECT t1.college from list as t1 GROUP BY t1.college ORDER BY COUNT ( * ) ASC LIMIT 1	1-22308881-2
what are the distinct COLUMN_NAME,0} of every list that has a greater 2007-2008 credit hours than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.in-county_tuition_per_credit_hour_ from list as t1 WHERE t1.2007-2008_credit_hours > ( SELECT MIN ( t1.in-county_tuition_per_credit_hour_ ) from list as t1 WHERE t1.in-county_tuition_per_credit_hour_ = 10 )	1-22308881-2
what is the college of the list with the largest county population 18 yrs + ?	SELECT t1.college from list as t1 WHERE t1.county_population_18_yrs_+ = ( SELECT MAX ( t1.county_population_18_yrs_+ ) from list as t1 )	1-22308881-2
Show everything on list	SELECT * FROM list	1-22308881-2
what is the college of the list with the smallest 2007 headcount ?	SELECT t1.college from list as t1 ORDER BY t1.2007_headcount ASC LIMIT 1	1-22308881-2
display the college , in-county tuition per credit hour ( fall 2009 ) , and college for each list .	SELECT t1.college , t1.in-county_tuition_per_credit_hour_ , t1.college from list as t1	1-22308881-2
