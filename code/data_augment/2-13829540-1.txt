what are the bill number ( s ) of all tax that have 10 or more tax ?	SELECT t1.bill_number_ from tax as t1 GROUP BY t1.bill_number_ HAVING COUNT ( * ) >= 10	2-13829540-1
please show the date introduced of the tax with count more than 10 .	SELECT t1.date_introduced from tax as t1 GROUP BY t1.date_introduced HAVING COUNT ( * ) > 10	2-13829540-1
what is the congress of tax with the maximum # of cosponsors across all tax ?	SELECT t1.congress from tax as t1 WHERE t1.# = ( SELECT MAX ( t1.# ) from tax as t1 )	2-13829540-1
what is the maximum and mininum # of cosponsors {COLUMN} for all tax ?	SELECT MAX ( t1.# ) , MIN ( t1.# ) from tax as t1	2-13829540-1
show the date introduced , sponsor ( s ) , and date introduced of all the tax .	SELECT t1.date_introduced , t1.sponsor_ , t1.date_introduced from tax as t1	2-13829540-1
what are the date introduced and latest status of each tax , listed in descending order by # of cosponsors ?	SELECT t1.date_introduced , t1.latest_status from tax as t1 ORDER BY t1.# DESC	2-13829540-1
what are the sponsor ( s ) and bill number ( s ) of tax with 10 or more congress ?	SELECT t1.sponsor_ , t1.bill_number_ from tax as t1 GROUP BY t1.congress HAVING COUNT ( * ) >= 10	2-13829540-1
what is the bill number ( s ) and date introduced of the tax with maximum # of cosponsors ?	SELECT t1.bill_number_ , t1.date_introduced from tax as t1 WHERE t1.# = ( SELECT MAX ( t1.# ) {FROM, 3} )	2-13829540-1
return the smallest # of cosponsors for every bill number ( s ) .	SELECT MIN ( t1.# ) , t1.bill_number_ from tax as t1 GROUP BY t1.bill_number_	2-13829540-1
show the bill number ( s ) and bill number ( s ) with at least 10 sponsor ( s ) .	SELECT t1.bill_number_ , t1.bill_number_ from tax as t1 GROUP BY t1.sponsor_ HAVING COUNT ( * ) >= 10	2-13829540-1
