what is the height ft ( m ) of highest year ?	SELECT t1.height_ft_ from list as t1 ORDER BY t1.year DESC LIMIT 1	2-12587248-1
what is the height ft ( m ) of list with the maximum year across all list ?	SELECT t1.height_ft_ from list as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from list as t1 )	2-12587248-1
find the name and rank of the list with at least 10 address .	SELECT t1.name , t1.rank from list as t1 GROUP BY t1.address HAVING COUNT ( * ) >= 10	2-12587248-1
find the height ft ( m ) which have exactly 10 list .	SELECT t1.height_ft_ from list as t1 GROUP BY t1.height_ft_ HAVING COUNT ( * ) = 10	2-12587248-1
show the name of list who have at least 10 list .	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) >= 10	2-12587248-1
which t1.rank has the smallest amount of list?	SELECT t1.rank from list as t1 GROUP BY t1.rank ORDER BY COUNT ( * ) LIMIT 1	2-12587248-1
what is the height ft ( m ) and address for the list with the rank 5 smallest year ?	SELECT t1.height_ft_ , t1.address from list as t1 ORDER BY t1.year LIMIT 5	2-12587248-1
find the number of list whose rank contain the word 10 .	SELECT COUNT ( * ) from list as t1 WHERE t1.rank LIKE 10	2-12587248-1
find the height ft ( m ) of list which have 10 but no 7 as address .	SELECT t1.height_ft_ from list as t1 WHERE t1.address = 10 EXCEPT SELECT t1.height_ft_ from list as t1 WHERE t1.address = 7	2-12587248-1
what is the rank and height ft ( m ) for the list with the rank 5 smallest year ?	SELECT t1.rank , t1.height_ft_ from list as t1 ORDER BY t1.year LIMIT 5	2-12587248-1
