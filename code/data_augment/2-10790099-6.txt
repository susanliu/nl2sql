count the number of 1932 that have an away team containing 10 .	SELECT COUNT ( * ) from 1932 as t1 WHERE t1.away_team LIKE 10	2-10790099-6
what is the home team of the 1932 with the largest crowd ?	SELECT t1.home_team from 1932 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1932 as t1 )	2-10790099-6
how many different away team correspond to each home team ?	SELECT t1.home_team , COUNT ( DISTINCT t1.away_team ) from 1932 as t1 GROUP BY t1.home_team	2-10790099-6
find the away team score of the 1932 with the highest crowd.	SELECT t1.away_team_score from 1932 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1932 as t1 )	2-10790099-6
find the number of 1932 that have more than 10 away team score .	SELECT COUNT ( * ) from 1932 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) > 10 	2-10790099-6
what are the venue of all 1932 that have 10 or more 1932 ?	SELECT t1.venue from 1932 as t1 GROUP BY t1.venue HAVING COUNT ( * ) >= 10	2-10790099-6
what is the home team and away team for the 1932 with the rank 5 smallest crowd ?	SELECT t1.home_team , t1.away_team from 1932 as t1 ORDER BY t1.crowd LIMIT 5	2-10790099-6
what is the maximum and mininum crowd {COLUMN} for all 1932 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1932 as t1	2-10790099-6
show the away team and the number of 1932 for each away team in the ascending order.	SELECT t1.away_team , COUNT ( * ) from 1932 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * )	2-10790099-6
show the home team score and the total crowd of 1932 .	SELECT t1.home_team_score , SUM ( t1.crowd ) from 1932 as t1 GROUP BY t1.home_team_score	2-10790099-6
