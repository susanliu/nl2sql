which mascot have an average enrollment 08-09 over 10 ?	SELECT t1.mascot from allen as t1 GROUP BY t1.mascot HAVING AVG ( t1.enrollment_08-09 ) >= 10	2-18765101-1
show the previous conference of allen who have at least 10 allen .	SELECT t1.previous_conference from allen as t1 GROUP BY t1.previous_conference HAVING COUNT ( * ) >= 10	2-18765101-1
what are the previous conference of all allen that have 10 or more allen ?	SELECT t1.previous_conference from allen as t1 GROUP BY t1.previous_conference HAVING COUNT ( * ) >= 10	2-18765101-1
list the previous conference , mascot and the county of the allen .	SELECT t1.previous_conference , t1.mascot , t1.county from allen as t1	2-18765101-1
find the ihsaa class / football class and location of the allen whose year joined is lower than the average year joined of all allen .	SELECT t1.ihsaa_class_/_football_class , t1.location from allen as t1 WHERE t1.year_joined < ( SELECT AVG ( t1.year_joined ) {FROM, 3} )	2-18765101-1
which mascot have greater year joined than that of any year joined in allen ?	SELECT t1.mascot from allen as t1 WHERE t1.year_joined > ( SELECT MIN ( t1.year_joined ) from allen as t1 )	2-18765101-1
which previous conference has most number of allen ?	SELECT t1.previous_conference from allen as t1 GROUP BY t1.previous_conference ORDER BY COUNT ( * ) DESC LIMIT 1	2-18765101-1
what is the previous conference and previous conference of the allen with maximum enrollment 08-09 ?	SELECT t1.previous_conference , t1.previous_conference from allen as t1 WHERE t1.enrollment_08-09 = ( SELECT MAX ( t1.enrollment_08-09 ) {FROM, 3} )	2-18765101-1
how many distinct mascot correspond to each mascot ?	SELECT t1.mascot , COUNT ( DISTINCT t1.mascot ) from allen as t1 GROUP BY t1.mascot	2-18765101-1
return the smallest enrollment 08-09 for every ihsaa class / football class .	SELECT MIN ( t1.enrollment_08-09 ) , t1.ihsaa_class_/_football_class from allen as t1 GROUP BY t1.ihsaa_class_/_football_class	2-18765101-1
