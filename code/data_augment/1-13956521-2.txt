what is the count of list with more than 10 director ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.director HAVING COUNT ( * ) > 10 	1-13956521-2
what are the prod . code , prod . code , and original airdate of each list ?	SELECT t1.prod_._code , t1.prod_._code , t1.original_airdate from list as t1	1-13956521-2
what are the distinct writer ( s ) with # between 10 and 4 ?	SELECT DISTINCT t1.writer_ from list as t1 WHERE t1.# BETWEEN 10 AND 4	1-13956521-2
what is the episode title and writer ( s ) of every list that has a # lower than average ?	SELECT t1.episode_title , t1.writer_ from list as t1 WHERE t1.# < ( SELECT AVG ( t1.# ) {FROM, 3} )	1-13956521-2
which writer ( s ) have greater # than that of any # in list ?	SELECT t1.writer_ from list as t1 WHERE t1.# > ( SELECT MIN ( t1.# ) from list as t1 )	1-13956521-2
what is the minimum # in each episode title ?	SELECT MIN ( t1.# ) , t1.episode_title from list as t1 GROUP BY t1.episode_title	1-13956521-2
show the episode title with fewer than 3 list .	SELECT t1.episode_title from list as t1 GROUP BY t1.episode_title HAVING COUNT ( * ) < 3	1-13956521-2
what is the director and original airdate of the list with maximum # ?	SELECT t1.director , t1.original_airdate from list as t1 WHERE t1.# = ( SELECT MAX ( t1.# ) {FROM, 3} )	1-13956521-2
which writer ( s ) has both list with less than 10 # and list with more than 90 # ?	SELECT t1.writer_ from list as t1 WHERE t1.# < 10 INTERSECT SELECT t1.writer_ from list as t1 WHERE t1.# > 90	1-13956521-2
count the number of list that have an writer ( s ) containing 10 .	SELECT COUNT ( * ) from list as t1 WHERE t1.writer_ LIKE 10	1-13956521-2
