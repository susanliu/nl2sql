list the 100m split of the swimming whose 100m split is not 10 .	SELECT t1.100m_split from swimming as t1 WHERE t1.100m_split ! = 10	2-10331421-1
what are the name of swimming whose nationality are not 10 ?	SELECT t1.name from swimming as t1 WHERE t1.nationality ! = 10	2-10331421-1
what are the 150m split and nationality of each swimming , listed in descending order by 50m split ?	SELECT t1.150m_split , t1.nationality from swimming as t1 ORDER BY t1.50m_split DESC	2-10331421-1
what are total place for each name ?	SELECT t1.name , SUM ( t1.place ) from swimming as t1 GROUP BY t1.name	2-10331421-1
show the nationality of the swimming that has the most swimming .	SELECT t1.nationality from swimming as t1 GROUP BY t1.nationality ORDER BY COUNT ( * ) DESC LIMIT 1	2-10331421-1
which time have greater 50m split than that of any 50m split in swimming ?	SELECT t1.time from swimming as t1 WHERE t1.50m_split > ( SELECT MIN ( t1.50m_split ) from swimming as t1 )	2-10331421-1
how many swimming are there in 100m split 10 or 22 ?	SELECT COUNT ( * ) from swimming as t1 WHERE t1.100m_split = 10 OR t1.100m_split = 22	2-10331421-1
find the 150m split of swimming who have both 10 and 53 50m split .	SELECT t1.150m_split from swimming as t1 WHERE t1.50m_split = 10 INTERSECT SELECT t1.150m_split from swimming as t1 WHERE t1.50m_split = 53	2-10331421-1
how many different 150m split correspond to each 100m split ?	SELECT t1.100m_split , COUNT ( DISTINCT t1.150m_split ) from swimming as t1 GROUP BY t1.100m_split	2-10331421-1
what is the nationality and 100m split of every swimming that has a 50m split lower than average ?	SELECT t1.nationality , t1.100m_split from swimming as t1 WHERE t1.50m_split < ( SELECT AVG ( t1.50m_split ) {FROM, 3} )	2-10331421-1
