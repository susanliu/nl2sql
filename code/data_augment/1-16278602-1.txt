which pop . density ( per km ² ) have less than 3 in none ?	SELECT t1.pop_._density_ from none as t1 GROUP BY t1.pop_._density_ HAVING COUNT ( * ) < 3	1-16278602-1
list the english name which average area ( km ² ) is above 10 .	SELECT t1.english_name from none as t1 GROUP BY t1.english_name HAVING AVG ( t1.area_ ) >= 10	1-16278602-1
show the seat of administration and the number of none for each seat of administration in the ascending order.	SELECT t1.seat_of_administration , COUNT ( * ) from none as t1 GROUP BY t1.seat_of_administration ORDER BY COUNT ( * )	1-16278602-1
what are the danish name and english name of the {COLUMN} who have area ( km ² ) above five or area ( km ² ) below ten ?	SELECT t1.danish_name , t1.english_name from none as t1 WHERE t1.area_ > 5 OR t1.area_ < 10	1-16278602-1
what are total population ( january 1 , 2008 ) for each largest city ?	SELECT t1.largest_city , SUM ( t1.population_ ) from none as t1 GROUP BY t1.largest_city	1-16278602-1
what is the english name and largest city of the none with maximum population ( january 1 , 2008 ) ?	SELECT t1.english_name , t1.largest_city from none as t1 WHERE t1.population_ = ( SELECT MAX ( t1.population_ ) {FROM, 3} )	1-16278602-1
return the pop . density ( per km ² ) and largest city of none with the five lowest area ( km ² ) .	SELECT t1.pop_._density_ , t1.largest_city from none as t1 ORDER BY t1.area_ LIMIT 5	1-16278602-1
list the seat of administration which average area ( km ² ) is above 10 .	SELECT t1.seat_of_administration from none as t1 GROUP BY t1.seat_of_administration HAVING AVG ( t1.area_ ) >= 10	1-16278602-1
what is all the information on the none with the largest number of english name ?	SELECT * from none as t1 ORDER BY t1.english_name DESC LIMIT 1	1-16278602-1
how many none have danish name that contains 10 ?	SELECT COUNT ( * ) from none as t1 WHERE t1.danish_name LIKE 10	1-16278602-1
