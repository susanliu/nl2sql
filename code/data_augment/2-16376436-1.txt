show the player with fewer than 3 1990 .	SELECT t1.player from 1990 as t1 GROUP BY t1.player HAVING COUNT ( * ) < 3	2-16376436-1
find the distinct position of 1990 having pick # between 10 and 64 .	SELECT DISTINCT t1.position from 1990 as t1 WHERE t1.pick_ BETWEEN 10 AND 64	2-16376436-1
how many 1990 have position that contains 10 ?	SELECT COUNT ( * ) from 1990 as t1 WHERE t1.position LIKE 10	2-16376436-1
what are the nfl team and player of each 1990 ?	SELECT t1.nfl_team , t1.player from 1990 as t1	2-16376436-1
what is the position and college of the 1990 with the top 5 smallest pick # ?	SELECT t1.position , t1.college from 1990 as t1 ORDER BY t1.pick_ LIMIT 5	2-16376436-1
what are the average pick # of 1990 for different position ?	SELECT AVG ( t1.pick_ ) , t1.position from 1990 as t1 GROUP BY t1.position	2-16376436-1
what are the player with exactly 10 1990 ?	SELECT t1.player from 1990 as t1 GROUP BY t1.player HAVING COUNT ( * ) = 10	2-16376436-1
how many different position correspond to each nfl team ?	SELECT t1.nfl_team , COUNT ( DISTINCT t1.position ) from 1990 as t1 GROUP BY t1.nfl_team	2-16376436-1
find the player and position of the 1990 whose pick # is lower than the average pick # of all 1990 .	SELECT t1.player , t1.position from 1990 as t1 WHERE t1.pick_ < ( SELECT AVG ( t1.pick_ ) {FROM, 3} )	2-16376436-1
find the player who has exactly 10 1990 .	SELECT t1.player from 1990 as t1 GROUP BY t1.player HAVING COUNT ( * ) = 10	2-16376436-1
