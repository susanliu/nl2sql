what is the tournament and tournament of the ian with maximum wins ?	SELECT t1.tournament , t1.tournament from ian as t1 WHERE t1.wins = ( SELECT MAX ( t1.wins ) {FROM, 3} )	2-1034991-8
list the tournament which average top-25 is above 10 .	SELECT t1.tournament from ian as t1 GROUP BY t1.tournament HAVING AVG ( t1.top-25 ) >= 10	2-1034991-8
return the tournament of the ian that has the fewest corresponding tournament .	SELECT t1.tournament from ian as t1 GROUP BY t1.tournament ORDER BY COUNT ( * ) ASC LIMIT 1	2-1034991-8
what is the tournament and tournament of the ian with the top 5 smallest cuts made ?	SELECT t1.tournament , t1.tournament from ian as t1 ORDER BY t1.cuts_made LIMIT 5	2-1034991-8
find the tournament of the ian that is most frequent across all tournament .	SELECT t1.tournament from ian as t1 GROUP BY t1.tournament ORDER BY COUNT ( * ) DESC LIMIT 1	2-1034991-8
find all tournament that have fewer than three in ian .	SELECT t1.tournament from ian as t1 GROUP BY t1.tournament HAVING COUNT ( * ) < 3	2-1034991-8
what are the distinct tournament with cuts made between 10 and 26 ?	SELECT DISTINCT t1.tournament from ian as t1 WHERE t1.cuts_made BETWEEN 10 AND 26	2-1034991-8
return the tournament of ian for which the tournament is not 10 ?	SELECT t1.tournament from ian as t1 WHERE t1.tournament ! = 10	2-1034991-8
find the tournament of the ian with the largest events .	SELECT t1.tournament from ian as t1 ORDER BY t1.events DESC LIMIT 1	2-1034991-8
find the tournament of ian which are tournament 10 but not tournament 18 .	SELECT t1.tournament from ian as t1 WHERE t1.tournament = 10 EXCEPT SELECT t1.tournament from ian as t1 WHERE t1.tournament = 18	2-1034991-8
