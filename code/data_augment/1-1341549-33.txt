find all result that have fewer than three in none .	SELECT t1.result from none as t1 GROUP BY t1.result HAVING COUNT ( * ) < 3	1-1341549-33
return the smallest first elected for every candidates .	SELECT MIN ( t1.first_elected ) , t1.candidates from none as t1 GROUP BY t1.candidates	1-1341549-33
Return all columns in none .	SELECT * FROM none	1-1341549-33
find the incumbent and district of the none with at least 10 incumbent .	SELECT t1.incumbent , t1.district from none as t1 GROUP BY t1.incumbent HAVING COUNT ( * ) >= 10	1-1341549-33
find the incumbent for all none who have more than the average first elected .	SELECT t1.incumbent from none as t1 WHERE t1.first_elected > ( SELECT AVG ( t1.first_elected ) from none as t1	1-1341549-33
what is the district and incumbent for the none with the rank 5 smallest first elected ?	SELECT t1.district , t1.incumbent from none as t1 ORDER BY t1.first_elected LIMIT 5	1-1341549-33
find the distinct incumbent of all none that have a higher first elected than some none with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.incumbent from none as t1 WHERE t1.first_elected > ( SELECT MIN ( t1.incumbent ) from none as t1 WHERE t1.district = 10 )	1-1341549-33
what are the result of all none with candidates that is 10 ?	SELECT t1.result from none as t1 GROUP BY t1.candidates HAVING COUNT ( * ) = 10	1-1341549-33
what is the district of the none who has the highest number of none ?	SELECT t1.district from none as t1 GROUP BY t1.district ORDER BY COUNT ( * ) DESC LIMIT 1	1-1341549-33
which district has both none with less than 10 first elected and none with more than 53 first elected ?	SELECT t1.district from none as t1 WHERE t1.first_elected < 10 INTERSECT SELECT t1.district from none as t1 WHERE t1.first_elected > 53	1-1341549-33
