Return all columns in 1953 .	SELECT * FROM 1953	2-14656212-2
what are the result of all 1953 that have 10 or more 1953 ?	SELECT t1.result from 1953 as t1 GROUP BY t1.result HAVING COUNT ( * ) >= 10	2-14656212-2
what are the venue of all 1953 with date that is 10 ?	SELECT t1.venue from 1953 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-14656212-2
which result have less than 3 in 1953 ?	SELECT t1.result from 1953 as t1 GROUP BY t1.result HAVING COUNT ( * ) < 3	2-14656212-2
what is the date of the 1953 with least number of date ?	SELECT t1.date from 1953 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-14656212-2
what are the opponent of 1953 , sorted by their frequency?	SELECT t1.opponent from 1953 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-14656212-2
how many 1953 does each opponent have ?	SELECT t1.opponent , COUNT ( * ) from 1953 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * )	2-14656212-2
find the venue of the 1953 with the largest week .	SELECT t1.venue from 1953 as t1 ORDER BY t1.week DESC LIMIT 1	2-14656212-2
show the date shared by more than 10 1953 .	SELECT t1.date from 1953 as t1 GROUP BY t1.date HAVING COUNT ( * ) > 10	2-14656212-2
what are the average attendance of 1953 for different date ?	SELECT AVG ( t1.attendance ) , t1.date from 1953 as t1 GROUP BY t1.date	2-14656212-2
