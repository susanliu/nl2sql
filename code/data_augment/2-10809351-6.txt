show all information on the 1949 that has the largest number of home team score.	SELECT * from 1949 as t1 ORDER BY t1.home_team_score DESC LIMIT 1	2-10809351-6
show home team and the number of distinct away team score in each home team .	SELECT t1.home_team , COUNT ( DISTINCT t1.away_team_score ) from 1949 as t1 GROUP BY t1.home_team	2-10809351-6
list away team and home team score who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.away_team , t1.home_team_score from 1949 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10809351-6
which home team has the least crowd ?	SELECT t1.home_team from 1949 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10809351-6
what are the venue , away team , and home team score of each 1949 ?	SELECT t1.venue , t1.away_team , t1.home_team_score from 1949 as t1	2-10809351-6
return the home team score of the largest crowd.	SELECT t1.home_team_score from 1949 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10809351-6
which away team have less than 3 in 1949 ?	SELECT t1.away_team from 1949 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) < 3	2-10809351-6
what are the away team score , date , and home team score of each 1949 ?	SELECT t1.away_team_score , t1.date , t1.home_team_score from 1949 as t1	2-10809351-6
find the number of 1949 that have more than 10 away team .	SELECT COUNT ( * ) from 1949 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) > 10 	2-10809351-6
what are the venue and away team ?	SELECT t1.venue , t1.away_team from 1949 as t1	2-10809351-6
