how many different position correspond to each player ?	SELECT t1.player , COUNT ( DISTINCT t1.position ) from 1967 as t1 GROUP BY t1.player	2-18908350-1
find the position that have 10 1967 .	SELECT t1.position from 1967 as t1 GROUP BY t1.position HAVING COUNT ( * ) = 10	2-18908350-1
list the school which average round is above 10 .	SELECT t1.school from 1967 as t1 GROUP BY t1.school HAVING AVG ( t1.round ) >= 10	2-18908350-1
find the player of 1967 who have both 10 and 96 round .	SELECT t1.player from 1967 as t1 WHERE t1.round = 10 INTERSECT SELECT t1.player from 1967 as t1 WHERE t1.round = 96	2-18908350-1
return the smallest round for every player .	SELECT MIN ( t1.round ) , t1.player from 1967 as t1 GROUP BY t1.player	2-18908350-1
return the position of the 1967 that has the fewest corresponding position .	SELECT t1.position from 1967 as t1 GROUP BY t1.position ORDER BY COUNT ( * ) ASC LIMIT 1	2-18908350-1
find the player and player of the 1967 whose pick is lower than the average pick of all 1967 .	SELECT t1.player , t1.player from 1967 as t1 WHERE t1.pick < ( SELECT AVG ( t1.pick ) {FROM, 3} )	2-18908350-1
which position has both 1967 with less than 10 round and 1967 with more than 48 round ?	SELECT t1.position from 1967 as t1 WHERE t1.round < 10 INTERSECT SELECT t1.position from 1967 as t1 WHERE t1.round > 48	2-18908350-1
show the school shared by more than 10 1967 .	SELECT t1.school from 1967 as t1 GROUP BY t1.school HAVING COUNT ( * ) > 10	2-18908350-1
find the school for all 1967 who have more than the average round .	SELECT t1.school from 1967 as t1 WHERE t1.round > ( SELECT AVG ( t1.round ) from 1967 as t1	2-18908350-1
