which % of popular vote have greater # of total votes than that of any # of total votes in progressive ?	SELECT t1.% from progressive as t1 WHERE t1.# > ( SELECT MIN ( t1.# ) from progressive as t1 )	2-123462-2
how many progressive are there that have more than {VALUE},0 % of popular vote ?	SELECT COUNT ( * ) from progressive as t1 GROUP BY t1.% HAVING COUNT ( * ) > 10 	2-123462-2
what is the % of popular vote and % of popular vote of every progressive that has a # of seats won lower than average ?	SELECT t1.% , t1.% from progressive as t1 WHERE t1.# < ( SELECT AVG ( t1.# ) {FROM, 3} )	2-123462-2
find the % of popular vote of progressive who have both 10 and 68 # of total votes .	SELECT t1.% from progressive as t1 WHERE t1.# = 10 INTERSECT SELECT t1.% from progressive as t1 WHERE t1.# = 68	2-123462-2
find the % of popular vote of progressive which are % of popular vote 10 but not % of popular vote 37 .	SELECT t1.% from progressive as t1 WHERE t1.% = 10 EXCEPT SELECT t1.% from progressive as t1 WHERE t1.% = 37	2-123462-2
how many progressive correspond to each % of popular vote? show the result in ascending order.	SELECT t1.% , COUNT ( * ) from progressive as t1 GROUP BY t1.% ORDER BY COUNT ( * )	2-123462-2
Return all columns in progressive .	SELECT * FROM progressive	2-123462-2
what is the count of progressive with more than 10 % of popular vote ?	SELECT COUNT ( * ) from progressive as t1 GROUP BY t1.% HAVING COUNT ( * ) > 10 	2-123462-2
what are the average # of candidates nominated of progressive , grouped by % of popular vote ?	SELECT AVG ( t1.# ) , t1.% from progressive as t1 GROUP BY t1.%	2-123462-2
what is the % of popular vote of progressive with the maximum # of candidates nominated across all progressive ?	SELECT t1.% from progressive as t1 WHERE t1.# = ( SELECT MAX ( t1.# ) from progressive as t1 )	2-123462-2
