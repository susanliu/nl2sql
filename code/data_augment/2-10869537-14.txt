find the date of 1973 who have crowd of both 10 and 9 .	SELECT t1.date from 1973 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.date from 1973 as t1 WHERE t1.crowd = 9	2-10869537-14
what is the home team score and away team score of the 1973 with maximum crowd ?	SELECT t1.home_team_score , t1.away_team_score from 1973 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10869537-14
what are the away team score of 1973 , sorted by their frequency?	SELECT t1.away_team_score from 1973 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-10869537-14
what is the date and away team score of the 1973 with the top 5 smallest crowd ?	SELECT t1.date , t1.away_team_score from 1973 as t1 ORDER BY t1.crowd LIMIT 5	2-10869537-14
what are the {home team score of all the 1973 , and the total crowd by each ?	SELECT t1.home_team_score , SUM ( t1.crowd ) from 1973 as t1 GROUP BY t1.home_team_score	2-10869537-14
find the distinct away team score of 1973 having crowd between 10 and 36 .	SELECT DISTINCT t1.away_team_score from 1973 as t1 WHERE t1.crowd BETWEEN 10 AND 36	2-10869537-14
show the home team score and the total crowd of 1973 .	SELECT t1.home_team_score , SUM ( t1.crowd ) from 1973 as t1 GROUP BY t1.home_team_score	2-10869537-14
select the average crowd of each 1973 's away team score .	SELECT AVG ( t1.crowd ) , t1.away_team_score from 1973 as t1 GROUP BY t1.away_team_score	2-10869537-14
what are the average crowd of 1973 for different date ?	SELECT AVG ( t1.crowd ) , t1.date from 1973 as t1 GROUP BY t1.date	2-10869537-14
what are the maximum and minimum crowd across all 1973 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1973 as t1	2-10869537-14
