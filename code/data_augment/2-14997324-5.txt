show the team and the number of unique team containing each team .	SELECT t1.team , COUNT ( DISTINCT t1.team ) from 2002 as t1 GROUP BY t1.team	2-14997324-5
find the team of 2002 which are in 10 team but not in 17 team .	SELECT t1.team from 2002 as t1 WHERE t1.team = 10 EXCEPT SELECT t1.team from 2002 as t1 WHERE t1.team = 17	2-14997324-5
list all information about 2002 .	SELECT * FROM 2002	2-14997324-5
what are the team of all 2002 with team that is 10 ?	SELECT t1.team from 2002 as t1 GROUP BY t1.team HAVING COUNT ( * ) = 10	2-14997324-5
what is the team and team of the 2002 with maximum losses ?	SELECT t1.team , t1.team from 2002 as t1 WHERE t1.losses = ( SELECT MAX ( t1.losses ) {FROM, 3} )	2-14997324-5
give the team that has the most 2002 .	SELECT t1.team from 2002 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) DESC LIMIT 1	2-14997324-5
how many different team correspond to each team ?	SELECT t1.team , COUNT ( DISTINCT t1.team ) from 2002 as t1 GROUP BY t1.team	2-14997324-5
how many 2002 are there in team 10 or 4 ?	SELECT COUNT ( * ) from 2002 as t1 WHERE t1.team = 10 OR t1.team = 4	2-14997324-5
find the team which have exactly 10 2002 .	SELECT t1.team from 2002 as t1 GROUP BY t1.team HAVING COUNT ( * ) = 10	2-14997324-5
which team has both 2002 with less than 10 losses and 2002 with more than 99 losses ?	SELECT t1.team from 2002 as t1 WHERE t1.losses < 10 INTERSECT SELECT t1.team from 2002 as t1 WHERE t1.losses > 99	2-14997324-5
