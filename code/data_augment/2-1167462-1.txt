please show the description of the midland with count more than 10 .	SELECT t1.description from midland as t1 GROUP BY t1.description HAVING COUNT ( * ) > 10	2-1167462-1
show the description and the corresponding number of midland sorted by the number of description in ascending order .	SELECT t1.description , COUNT ( * ) from midland as t1 GROUP BY t1.description ORDER BY COUNT ( * )	2-1167462-1
find the number & name and owner ( s ) of the midland with at least 10 number & name .	SELECT t1.number_ , t1.owner_ from midland as t1 GROUP BY t1.number_ HAVING COUNT ( * ) >= 10	2-1167462-1
what is the description and description of every midland that has a date lower than average ?	SELECT t1.description , t1.description from midland as t1 WHERE t1.date < ( SELECT AVG ( t1.date ) {FROM, 3} )	2-1167462-1
show number & name and number & name of midland .	SELECT t1.number_ , t1.number_ from midland as t1	2-1167462-1
find the livery of the midland with the largest date .	SELECT t1.livery from midland as t1 ORDER BY t1.date DESC LIMIT 1	2-1167462-1
what are the maximum and minimum date across all midland ?	SELECT MAX ( t1.date ) , MIN ( t1.date ) from midland as t1	2-1167462-1
find the owner ( s ) of midland which are in 10 description but not in 57 description .	SELECT t1.owner_ from midland as t1 WHERE t1.description = 10 EXCEPT SELECT t1.owner_ from midland as t1 WHERE t1.description = 57	2-1167462-1
please show the different owner ( s ) , ordered by the number of midland that have each .	SELECT t1.owner_ from midland as t1 GROUP BY t1.owner_ ORDER BY COUNT ( * ) ASC LIMIT 1	2-1167462-1
what is the minimum date in each livery ?	SELECT MIN ( t1.date ) , t1.livery from midland as t1 GROUP BY t1.livery	2-1167462-1
