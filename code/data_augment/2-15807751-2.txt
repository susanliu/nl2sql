what are the nation of 1985 , sorted by their frequency?	SELECT t1.nation from 1985 as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) ASC LIMIT 1	2-15807751-2
what is the nation of the 1985 with least number of nation ?	SELECT t1.nation from 1985 as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) ASC LIMIT 1	2-15807751-2
find the nation which have exactly 10 1985 .	SELECT t1.nation from 1985 as t1 GROUP BY t1.nation HAVING COUNT ( * ) = 10	2-15807751-2
what are the nation of 1985 whose nation is not 10 ?	SELECT t1.nation from 1985 as t1 WHERE t1.nation ! = 10	2-15807751-2
return the smallest rank for every nation .	SELECT MIN ( t1.rank ) , t1.nation from 1985 as t1 GROUP BY t1.nation	2-15807751-2
give the maximum and minimum rank of all 1985 .	SELECT MAX ( t1.rank ) , MIN ( t1.rank ) from 1985 as t1	2-15807751-2
show the nation of the 1985 that has the most 1985 .	SELECT t1.nation from 1985 as t1 GROUP BY t1.nation ORDER BY COUNT ( * ) DESC LIMIT 1	2-15807751-2
list all nation which have rank higher than the average .	SELECT t1.nation from 1985 as t1 WHERE t1.rank > ( SELECT AVG ( t1.rank ) from 1985 as t1	2-15807751-2
how many 1985 ' nation have the word 10 in them ?	SELECT COUNT ( * ) from 1985 as t1 WHERE t1.nation LIKE 10	2-15807751-2
count the number of 1985 in nation 10 or 72 .	SELECT COUNT ( * ) from 1985 as t1 WHERE t1.nation = 10 OR t1.nation = 72	2-15807751-2
