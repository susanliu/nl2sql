show the distance ( metres ) , athlete , and distance ( metres ) of all the athletics .	SELECT t1.distance_ , t1.athlete , t1.distance_ from athletics as t1	1-26454128-7
how many athletics are there for each country ? list the smallest count first .	SELECT t1.country , COUNT ( * ) from athletics as t1 GROUP BY t1.country ORDER BY COUNT ( * )	1-26454128-7
what are the points and athlete of the {COLUMN} who have adjusted points above five or adjusted points below ten ?	SELECT t1.points , t1.athlete from athletics as t1 WHERE t1.adjusted_points > 5 OR t1.adjusted_points < 10	1-26454128-7
which athlete has most number of athletics ?	SELECT t1.athlete from athletics as t1 GROUP BY t1.athlete ORDER BY COUNT ( * ) DESC LIMIT 1	1-26454128-7
what are the points and country of athletics with 10 or more distance ( metres ) ?	SELECT t1.points , t1.country from athletics as t1 GROUP BY t1.distance_ HAVING COUNT ( * ) >= 10	1-26454128-7
what are the average adjusted points of athletics , grouped by points ?	SELECT AVG ( t1.adjusted_points ) , t1.points from athletics as t1 GROUP BY t1.points	1-26454128-7
how many athletics have distance ( metres ) that contains 10 ?	SELECT COUNT ( * ) from athletics as t1 WHERE t1.distance_ LIKE 10	1-26454128-7
please show the country of the athletics with count more than 10 .	SELECT t1.country from athletics as t1 GROUP BY t1.country HAVING COUNT ( * ) > 10	1-26454128-7
show the distance ( metres ) and distance ( metres ) with at least 10 athlete .	SELECT t1.distance_ , t1.distance_ from athletics as t1 GROUP BY t1.athlete HAVING COUNT ( * ) >= 10	1-26454128-7
find the points of athletics which have 10 but no 70 as country .	SELECT t1.points from athletics as t1 WHERE t1.country = 10 EXCEPT SELECT t1.points from athletics as t1 WHERE t1.country = 70	1-26454128-7
