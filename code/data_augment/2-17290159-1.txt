find the player of the 1954 with the highest to par.	SELECT t1.player from 1954 as t1 WHERE t1.to_par = ( SELECT MAX ( t1.to_par ) from 1954 as t1 )	2-17290159-1
what are the {score of all the 1954 , and the total to par by each ?	SELECT t1.score , SUM ( t1.to_par ) from 1954 as t1 GROUP BY t1.score	2-17290159-1
what is the place of the 1954 with the minimum money ( $ ) ?	SELECT t1.place from 1954 as t1 ORDER BY t1.money_ ASC LIMIT 1	2-17290159-1
list score of 1954 that have the number of 1954 greater than 10 .	SELECT t1.score from 1954 as t1 GROUP BY t1.score HAVING COUNT ( * ) > 10	2-17290159-1
which place has both 1954 with less than 10 to par and 1954 with more than 66 to par ?	SELECT t1.place from 1954 as t1 WHERE t1.to_par < 10 INTERSECT SELECT t1.place from 1954 as t1 WHERE t1.to_par > 66	2-17290159-1
what are the place for 1954 who have more than the average money ( $ )?	SELECT t1.place from 1954 as t1 WHERE t1.money_ > ( SELECT AVG ( t1.money_ ) from 1954 as t1	2-17290159-1
give the maximum and minimum to par of all 1954 .	SELECT MAX ( t1.to_par ) , MIN ( t1.to_par ) from 1954 as t1	2-17290159-1
find the number of 1954 that have more than 10 score .	SELECT COUNT ( * ) from 1954 as t1 GROUP BY t1.score HAVING COUNT ( * ) > 10 	2-17290159-1
what are the player and country of all 1954 sorted by decreasing money ( $ ) ?	SELECT t1.player , t1.country from 1954 as t1 ORDER BY t1.money_ DESC	2-17290159-1
what are the country of all 1954 that have 10 or more 1954 ?	SELECT t1.country from 1954 as t1 GROUP BY t1.country HAVING COUNT ( * ) >= 10	2-17290159-1
