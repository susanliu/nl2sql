find the event of katsuya which are location 10 but not location 49 .	SELECT t1.event from katsuya as t1 WHERE t1.location = 10 EXCEPT SELECT t1.event from katsuya as t1 WHERE t1.location = 49	2-12758642-2
how many katsuya are there for each time ? list the smallest count first .	SELECT t1.time , COUNT ( * ) from katsuya as t1 GROUP BY t1.time ORDER BY COUNT ( * )	2-12758642-2
return the smallest round for every method .	SELECT MIN ( t1.round ) , t1.method from katsuya as t1 GROUP BY t1.method	2-12758642-2
select the average round of each katsuya 's time .	SELECT AVG ( t1.round ) , t1.time from katsuya as t1 GROUP BY t1.time	2-12758642-2
what is all the information on the katsuya with the largest number of method ?	SELECT * from katsuya as t1 ORDER BY t1.method DESC LIMIT 1	2-12758642-2
what are the opponent of all katsuya that have 10 or more katsuya ?	SELECT t1.opponent from katsuya as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-12758642-2
what is all the information on the katsuya with the largest number of time ?	SELECT * from katsuya as t1 ORDER BY t1.time DESC LIMIT 1	2-12758642-2
what is the event and location of the katsuya with the top 5 smallest round ?	SELECT t1.event , t1.location from katsuya as t1 ORDER BY t1.round LIMIT 5	2-12758642-2
find the method and time of the katsuya whose round is lower than the average round of all katsuya .	SELECT t1.method , t1.time from katsuya as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-12758642-2
find the opponent of katsuya which are event 10 but not event 75 .	SELECT t1.opponent from katsuya as t1 WHERE t1.event = 10 EXCEPT SELECT t1.opponent from katsuya as t1 WHERE t1.event = 75	2-12758642-2
