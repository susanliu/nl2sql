what are the distinct COLUMN_NAME,0} of 2000 with grid higher than any 2000 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.driver from 2000 as t1 WHERE t1.grid > ( SELECT MIN ( t1.driver ) from 2000 as t1 WHERE t1.time/retired = 10 )	2-1123395-2
count the number of 2000 that have an driver containing 10 .	SELECT COUNT ( * ) from 2000 as t1 WHERE t1.driver LIKE 10	2-1123395-2
show the driver and the number of unique driver containing each driver .	SELECT t1.driver , COUNT ( DISTINCT t1.driver ) from 2000 as t1 GROUP BY t1.driver	2-1123395-2
what are the average grid of 2000 , grouped by time/retired ?	SELECT AVG ( t1.grid ) , t1.time/retired from 2000 as t1 GROUP BY t1.time/retired	2-1123395-2
what are the driver and constructor of each 2000 ?	SELECT t1.driver , t1.constructor from 2000 as t1	2-1123395-2
what is the driver and constructor for the 2000 with the rank 5 smallest laps ?	SELECT t1.driver , t1.constructor from 2000 as t1 ORDER BY t1.laps LIMIT 5	2-1123395-2
find the constructor of 2000 who have more than 10 2000 .	SELECT t1.constructor from 2000 as t1 GROUP BY t1.constructor HAVING COUNT ( * ) > 10	2-1123395-2
please show the driver of the 2000 that have at least 10 records .	SELECT t1.driver from 2000 as t1 GROUP BY t1.driver HAVING COUNT ( * ) >= 10	2-1123395-2
what is the driver and driver for the 2000 with the rank 5 smallest grid ?	SELECT t1.driver , t1.driver from 2000 as t1 ORDER BY t1.grid LIMIT 5	2-1123395-2
what are the distinct time/retired with grid between 10 and 72 ?	SELECT DISTINCT t1.time/retired from 2000 as t1 WHERE t1.grid BETWEEN 10 AND 72	2-1123395-2
