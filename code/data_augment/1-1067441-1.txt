what are the density and province of the {COLUMN} who have population ( 2004 estimate ) above five or population ( 2004 estimate ) below ten ?	SELECT t1.density , t1.province from ranked as t1 WHERE t1.population_ > 5 OR t1.population_ < 10	1-1067441-1
what is the density of the ranked with the minimum population ( 2004 estimate ) ?	SELECT t1.density from ranked as t1 ORDER BY t1.population_ ASC LIMIT 1	1-1067441-1
find the distinct density of ranked having population ( 2004 estimate ) between 10 and 90 .	SELECT DISTINCT t1.density from ranked as t1 WHERE t1.population_ BETWEEN 10 AND 90	1-1067441-1
what is the density of all ranked whose gdp ( 2003 , pps in mil . € ) is higher than any ranked ?	SELECT t1.density from ranked as t1 WHERE t1.gdp_ > ( SELECT MIN ( t1.gdp_ ) from ranked as t1 )	1-1067441-1
find the density of ranked who have both 10 and 44 population ( 2004 estimate ) .	SELECT t1.density from ranked as t1 WHERE t1.population_ = 10 INTERSECT SELECT t1.density from ranked as t1 WHERE t1.population_ = 44	1-1067441-1
find the density of ranked which have both 10 and 36 as density .	SELECT t1.density from ranked as t1 WHERE t1.area_ = 10 INTERSECT SELECT t1.density from ranked as t1 WHERE t1.area_ = 36	1-1067441-1
how many ranked does each province have ?	SELECT t1.province , COUNT ( * ) from ranked as t1 GROUP BY t1.province ORDER BY COUNT ( * )	1-1067441-1
list the density which average gdp ( 2003 , pps in mil . € ) is above 10 .	SELECT t1.density from ranked as t1 GROUP BY t1.density HAVING AVG ( t1.gdp_ ) >= 10	1-1067441-1
give the maximum and minimum area ( km ² ) of all ranked .	SELECT MAX ( t1.area_ ) , MIN ( t1.area_ ) from ranked as t1	1-1067441-1
what are the density of ranked , sorted by their frequency?	SELECT t1.density from ranked as t1 GROUP BY t1.density ORDER BY COUNT ( * ) ASC LIMIT 1	1-1067441-1
