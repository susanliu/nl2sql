what are the class that have greater year than any year in teo ?	SELECT t1.class from teo as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from teo as t1 )	2-1218368-3
display the tyres , class , and pos . for each teo .	SELECT t1.tyres , t1.class , t1.pos_ from teo as t1	2-1218368-3
what is the class of the teo with the largest laps ?	SELECT t1.class from teo as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) from teo as t1 )	2-1218368-3
what is the count of teo with more than 10 pos . ?	SELECT COUNT ( * ) from teo as t1 GROUP BY t1.pos_ HAVING COUNT ( * ) > 10 	2-1218368-3
which co-drivers have an average laps over 10 ?	SELECT t1.co-drivers from teo as t1 GROUP BY t1.co-drivers HAVING AVG ( t1.laps ) >= 10	2-1218368-3
what are the class pos . and team of the {COLUMN} who have laps above five or laps below ten ?	SELECT t1.class_pos_ , t1.team from teo as t1 WHERE t1.laps > 5 OR t1.laps < 10	2-1218368-3
what is the team and tyres for the teo with the rank 5 smallest year ?	SELECT t1.team , t1.tyres from teo as t1 ORDER BY t1.year LIMIT 5	2-1218368-3
what is the minimum year in each class ?	SELECT MIN ( t1.year ) , t1.class from teo as t1 GROUP BY t1.class	2-1218368-3
what is the pos . and class for the teo with the rank 5 smallest laps ?	SELECT t1.pos_ , t1.class from teo as t1 ORDER BY t1.laps LIMIT 5	2-1218368-3
what is the maximum and mininum laps {COLUMN} for all teo ?	SELECT MAX ( t1.laps ) , MIN ( t1.laps ) from teo as t1	2-1218368-3
