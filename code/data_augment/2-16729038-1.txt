what are the opponent of all 2000 with date that is 10 ?	SELECT t1.opponent from 2000 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-16729038-1
how many distinct result correspond to each opponent ?	SELECT t1.opponent , COUNT ( DISTINCT t1.result ) from 2000 as t1 GROUP BY t1.opponent	2-16729038-1
find the distinct result of 2000 having week between 10 and 24 .	SELECT DISTINCT t1.result from 2000 as t1 WHERE t1.week BETWEEN 10 AND 24	2-16729038-1
what are the opponent for all 2000 , and what is the total {week for each ?	SELECT t1.opponent , SUM ( t1.week ) from 2000 as t1 GROUP BY t1.opponent	2-16729038-1
what are the attendance and date of the {COLUMN} who have week above five or week below ten ?	SELECT t1.attendance , t1.date from 2000 as t1 WHERE t1.week > 5 OR t1.week < 10	2-16729038-1
find the result of the 2000 with the highest week .	SELECT t1.result from 2000 as t1 ORDER BY t1.week DESC LIMIT 1	2-16729038-1
show the date with fewer than 3 2000 .	SELECT t1.date from 2000 as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	2-16729038-1
what are the result and attendance of 2000 with 10 or more attendance ?	SELECT t1.result , t1.attendance from 2000 as t1 GROUP BY t1.attendance HAVING COUNT ( * ) >= 10	2-16729038-1
what are the distinct COLUMN_NAME,0} of every 2000 that has a greater week than some 2000 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.attendance from 2000 as t1 WHERE t1.week > ( SELECT MIN ( t1.attendance ) from 2000 as t1 WHERE t1.date = 10 )	2-16729038-1
give the t1.attendance with the fewest 2000 .	SELECT t1.attendance from 2000 as t1 GROUP BY t1.attendance ORDER BY COUNT ( * ) LIMIT 1	2-16729038-1
