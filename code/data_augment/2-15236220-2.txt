find the title of the judy with the highest year.	SELECT t1.title from judy as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from judy as t1 )	2-15236220-2
find the distinct role of judy having year between 10 and 96 .	SELECT DISTINCT t1.role from judy as t1 WHERE t1.year BETWEEN 10 AND 96	2-15236220-2
what is the director and producer for the judy with the rank 5 smallest year ?	SELECT t1.director , t1.producer from judy as t1 ORDER BY t1.year LIMIT 5	2-15236220-2
what is the producer of the judy with the minimum year ?	SELECT t1.producer from judy as t1 ORDER BY t1.year ASC LIMIT 1	2-15236220-2
which producer has both judy with less than 10 year and judy with more than 66 year ?	SELECT t1.producer from judy as t1 WHERE t1.year < 10 INTERSECT SELECT t1.producer from judy as t1 WHERE t1.year > 66	2-15236220-2
return the smallest year for every producer .	SELECT MIN ( t1.year ) , t1.producer from judy as t1 GROUP BY t1.producer	2-15236220-2
find the role of the judy with the highest year .	SELECT t1.role from judy as t1 ORDER BY t1.year DESC LIMIT 1	2-15236220-2
what are the role , title , and role of each judy ?	SELECT t1.role , t1.title , t1.title from judy as t1	2-15236220-2
how many judy have producer that contain the word 10 ?	SELECT COUNT ( * ) from judy as t1 WHERE t1.producer LIKE 10	2-15236220-2
what are the average year of judy , grouped by director ?	SELECT AVG ( t1.year ) , t1.director from judy as t1 GROUP BY t1.director	2-15236220-2
