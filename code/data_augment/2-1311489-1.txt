find the score of elvir which have both 10 and 59 as score .	SELECT t1.score from elvir as t1 WHERE t1.goal = 10 INTERSECT SELECT t1.score from elvir as t1 WHERE t1.goal = 59	2-1311489-1
what are the competition of all elvir that have 10 or more elvir ?	SELECT t1.competition from elvir as t1 GROUP BY t1.competition HAVING COUNT ( * ) >= 10	2-1311489-1
list all information about elvir .	SELECT * FROM elvir	2-1311489-1
display the venue , competition , and score for each elvir .	SELECT t1.venue , t1.competition , t1.score from elvir as t1	2-1311489-1
how many distinct date correspond to each result ?	SELECT t1.result , COUNT ( DISTINCT t1.date ) from elvir as t1 GROUP BY t1.result	2-1311489-1
which result have an average goal over 10 ?	SELECT t1.result from elvir as t1 GROUP BY t1.result HAVING AVG ( t1.goal ) >= 10	2-1311489-1
find all competition that have fewer than three in elvir .	SELECT t1.competition from elvir as t1 GROUP BY t1.competition HAVING COUNT ( * ) < 3	2-1311489-1
find the result and date of the elvir whose goal is lower than the average goal of all elvir .	SELECT t1.result , t1.date from elvir as t1 WHERE t1.goal < ( SELECT AVG ( t1.goal ) {FROM, 3} )	2-1311489-1
find the result and competition of the elvir whose goal is lower than the average goal of all elvir .	SELECT t1.result , t1.competition from elvir as t1 WHERE t1.goal < ( SELECT AVG ( t1.goal ) {FROM, 3} )	2-1311489-1
return the venue of the elvir with the fewest goal .	SELECT t1.venue from elvir as t1 ORDER BY t1.goal ASC LIMIT 1	2-1311489-1
