what is the count of 1974 with more than 10 away team ?	SELECT COUNT ( * ) from 1974 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) > 10 	2-10869646-7
what is the venue of all 1974 whose crowd is higher than any 1974 ?	SELECT t1.venue from 1974 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1974 as t1 )	2-10869646-7
sort the list of away team score and away team score of all 1974 in the descending order of crowd .	SELECT t1.away_team_score , t1.away_team_score from 1974 as t1 ORDER BY t1.crowd DESC	2-10869646-7
return the maximum and minimum crowd across all 1974 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1974 as t1	2-10869646-7
which home team score have less than 3 in 1974 ?	SELECT t1.home_team_score from 1974 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) < 3	2-10869646-7
how many 1974 are there for each venue ? list the smallest count first .	SELECT t1.venue , COUNT ( * ) from 1974 as t1 GROUP BY t1.venue ORDER BY COUNT ( * )	2-10869646-7
what are the venue with exactly 10 1974 ?	SELECT t1.venue from 1974 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10869646-7
find the distinct venue of all 1974 that have a higher crowd than some 1974 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.venue from 1974 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.venue ) from 1974 as t1 WHERE t1.home_team = 10 )	2-10869646-7
what are the home team of 1974 , sorted by their frequency?	SELECT t1.home_team from 1974 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10869646-7
what are the date for all 1974 , and what is the total {crowd for each ?	SELECT t1.date , SUM ( t1.crowd ) from 1974 as t1 GROUP BY t1.date	2-10869646-7
