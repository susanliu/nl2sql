what are the {chassis of all the ray , and the total points by each ?	SELECT t1.chassis , SUM ( t1.points ) from ray as t1 GROUP BY t1.chassis	2-1252146-3
which chassis has both ray with less than 10 points and ray with more than 2 points ?	SELECT t1.chassis from ray as t1 WHERE t1.points < 10 INTERSECT SELECT t1.chassis from ray as t1 WHERE t1.points > 2	2-1252146-3
which chassis has most number of ray ?	SELECT t1.chassis from ray as t1 GROUP BY t1.chassis ORDER BY COUNT ( * ) DESC LIMIT 1	2-1252146-3
find all entrant that have fewer than three in ray .	SELECT t1.entrant from ray as t1 GROUP BY t1.entrant HAVING COUNT ( * ) < 3	2-1252146-3
find the distinct chassis of all ray that have a higher year than some ray with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.chassis from ray as t1 WHERE t1.year > ( SELECT MIN ( t1.chassis ) from ray as t1 WHERE t1.chassis = 10 )	2-1252146-3
what is the entrant and chassis of every ray that has a year lower than average ?	SELECT t1.entrant , t1.chassis from ray as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-1252146-3
count the number of ray that have an engine containing 10 .	SELECT COUNT ( * ) from ray as t1 WHERE t1.engine LIKE 10	2-1252146-3
find the entrant of ray whose year is more than the average year of ray .	SELECT t1.entrant from ray as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from ray as t1	2-1252146-3
how many ray have entrant that contain the word 10 ?	SELECT COUNT ( * ) from ray as t1 WHERE t1.entrant LIKE 10	2-1252146-3
find the engine and chassis of the ray with at least 10 chassis .	SELECT t1.engine , t1.chassis from ray as t1 GROUP BY t1.chassis HAVING COUNT ( * ) >= 10	2-1252146-3
