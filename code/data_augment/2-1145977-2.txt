show the season premiere and the total viewers ( in millions ) of extreme .	SELECT t1.season_premiere , SUM ( t1.viewers_ ) from extreme as t1 GROUP BY t1.season_premiere	2-1145977-2
what is the t1.season_premiere of extreme that has fewest number of extreme ?	SELECT t1.season_premiere from extreme as t1 GROUP BY t1.season_premiere ORDER BY COUNT ( * ) LIMIT 1	2-1145977-2
show the tv season with fewer than 3 extreme .	SELECT t1.tv_season from extreme as t1 GROUP BY t1.tv_season HAVING COUNT ( * ) < 3	2-1145977-2
what are the maximum and minimum viewers ( in millions ) across all extreme ?	SELECT MAX ( t1.viewers_ ) , MIN ( t1.viewers_ ) from extreme as t1	2-1145977-2
what are the season of all extreme with ranking that is 10 ?	SELECT t1.season from extreme as t1 GROUP BY t1.ranking HAVING COUNT ( * ) = 10	2-1145977-2
how many extreme does each season premiere have ?	SELECT t1.season_premiere , COUNT ( * ) from extreme as t1 GROUP BY t1.season_premiere ORDER BY COUNT ( * )	2-1145977-2
what are the season finale of extreme whose season premiere are not 10 ?	SELECT t1.season_finale from extreme as t1 WHERE t1.season_premiere ! = 10	2-1145977-2
find the distinct COLUMN_NAME,0} of all extreme that have viewers ( in millions ) higher than some extreme from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.season_finale from extreme as t1 WHERE t1.viewers_ > ( SELECT MIN ( t1.season_finale ) from extreme as t1 WHERE t1.season = 10 )	2-1145977-2
return each season with the number of extreme in ascending order of the number of season .	SELECT t1.season , COUNT ( * ) from extreme as t1 GROUP BY t1.season ORDER BY COUNT ( * )	2-1145977-2
what are the season finale and season ?	SELECT t1.season_finale , t1.season from extreme as t1	2-1145977-2
