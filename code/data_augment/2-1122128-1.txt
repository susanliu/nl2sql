find the time/retired of 1955 whose laps is more than the average laps of 1955 .	SELECT t1.time/retired from 1955 as t1 WHERE t1.laps > ( SELECT AVG ( t1.laps ) from 1955 as t1	2-1122128-1
which constructor has both 1955 with less than 10 laps and 1955 with more than 36 laps ?	SELECT t1.constructor from 1955 as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.constructor from 1955 as t1 WHERE t1.laps > 36	2-1122128-1
what are the driver , constructor , and constructor of each 1955 ?	SELECT t1.driver , t1.constructor , t1.constructor from 1955 as t1	2-1122128-1
which driver have less than 3 in 1955 ?	SELECT t1.driver from 1955 as t1 GROUP BY t1.driver HAVING COUNT ( * ) < 3	2-1122128-1
find the constructor and driver of the 1955 whose grid is lower than the average grid of all 1955 .	SELECT t1.constructor , t1.driver from 1955 as t1 WHERE t1.grid < ( SELECT AVG ( t1.grid ) {FROM, 3} )	2-1122128-1
show the driver of 1955 who have at least 10 1955 .	SELECT t1.driver from 1955 as t1 GROUP BY t1.driver HAVING COUNT ( * ) >= 10	2-1122128-1
what is the constructor of highest laps ?	SELECT t1.constructor from 1955 as t1 ORDER BY t1.laps DESC LIMIT 1	2-1122128-1
return the driver of the 1955 with the fewest laps .	SELECT t1.driver from 1955 as t1 ORDER BY t1.laps ASC LIMIT 1	2-1122128-1
what is the minimum laps in each constructor ?	SELECT MIN ( t1.laps ) , t1.constructor from 1955 as t1 GROUP BY t1.constructor	2-1122128-1
what is the constructor and driver of every 1955 that has a grid lower than average ?	SELECT t1.constructor , t1.driver from 1955 as t1 WHERE t1.grid < ( SELECT AVG ( t1.grid ) {FROM, 3} )	2-1122128-1
