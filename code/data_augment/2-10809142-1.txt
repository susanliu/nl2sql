what is the minimum crowd in each home team score ?	SELECT MIN ( t1.crowd ) , t1.home_team_score from 1944 as t1 GROUP BY t1.home_team_score	2-10809142-1
what is the home team of the 1944 with least number of home team ?	SELECT t1.home_team from 1944 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10809142-1
list venue and venue who have crowd greater than 5 or crowd shorter than 10 .	SELECT t1.venue , t1.venue from 1944 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10809142-1
show the date and the total crowd of 1944 .	SELECT t1.date , SUM ( t1.crowd ) from 1944 as t1 GROUP BY t1.date	2-10809142-1
what are the distinct home team score with crowd between 10 and 8 ?	SELECT DISTINCT t1.home_team_score from 1944 as t1 WHERE t1.crowd BETWEEN 10 AND 8	2-10809142-1
return the home team score of the largest crowd.	SELECT t1.home_team_score from 1944 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10809142-1
show all away team and the total crowd for each .	SELECT t1.away_team , SUM ( t1.crowd ) from 1944 as t1 GROUP BY t1.away_team	2-10809142-1
which away team has the least crowd ?	SELECT t1.away_team from 1944 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10809142-1
what is the away team score and date of the 1944 with maximum crowd ?	SELECT t1.away_team_score , t1.date from 1944 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809142-1
what is the home team score of the 1944 with the largest crowd ?	SELECT t1.home_team_score from 1944 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1944 as t1 )	2-10809142-1
