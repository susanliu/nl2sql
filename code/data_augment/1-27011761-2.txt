show the arrival of the kanpur that has the most kanpur .	SELECT t1.arrival from kanpur as t1 GROUP BY t1.arrival ORDER BY COUNT ( * ) DESC LIMIT 1	1-27011761-2
what are the days and platform no . of all kanpur sorted by decreasing train no . ?	SELECT t1.days , t1.platform_no_ from kanpur as t1 ORDER BY t1.train_no_ DESC	1-27011761-2
show the departure and days with at least 10 train name .	SELECT t1.departure , t1.days from kanpur as t1 GROUP BY t1.train_name HAVING COUNT ( * ) >= 10	1-27011761-2
show departure for all kanpur whose train no . are greater than the average .	SELECT t1.departure from kanpur as t1 WHERE t1.train_no_ > ( SELECT AVG ( t1.train_no_ ) from kanpur as t1	1-27011761-2
show the platform no . of the kanpur that has the most kanpur .	SELECT t1.platform_no_ from kanpur as t1 GROUP BY t1.platform_no_ ORDER BY COUNT ( * ) DESC LIMIT 1	1-27011761-2
what are the train name and days of the {COLUMN} who have train no . above five or train no . below ten ?	SELECT t1.train_name , t1.days from kanpur as t1 WHERE t1.train_no_ > 5 OR t1.train_no_ < 10	1-27011761-2
what are the departure of kanpur with train no . greater than the average of all kanpur ?	SELECT t1.departure from kanpur as t1 WHERE t1.train_no_ > ( SELECT AVG ( t1.train_no_ ) from kanpur as t1	1-27011761-2
what is all the information on the kanpur with the largest number of departure ?	SELECT * from kanpur as t1 ORDER BY t1.departure DESC LIMIT 1	1-27011761-2
what are the train name that have greater train no . than any train no . in kanpur ?	SELECT t1.train_name from kanpur as t1 WHERE t1.train_no_ > ( SELECT MIN ( t1.train_no_ ) from kanpur as t1 )	1-27011761-2
what are the train name of all kanpur with arrival that is 10 ?	SELECT t1.train_name from kanpur as t1 GROUP BY t1.arrival HAVING COUNT ( * ) = 10	1-27011761-2
