find the home team of the 1964 which have venue 10 but not 94 .	SELECT t1.home_team from 1964 as t1 WHERE t1.venue = 10 EXCEPT SELECT t1.home_team from 1964 as t1 WHERE t1.venue = 94	2-10784349-10
what are the {away team of all the 1964 , and the total crowd by each ?	SELECT t1.away_team , SUM ( t1.crowd ) from 1964 as t1 GROUP BY t1.away_team	2-10784349-10
what is the home team and away team of the 1964 with maximum crowd ?	SELECT t1.home_team , t1.away_team from 1964 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10784349-10
what is the home team of the 1964 who has the highest number of 1964 ?	SELECT t1.home_team from 1964 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10784349-10
find the venue of 1964 who have more than 10 1964 .	SELECT t1.venue from 1964 as t1 GROUP BY t1.venue HAVING COUNT ( * ) > 10	2-10784349-10
what are the venue and home team score ?	SELECT t1.venue , t1.home_team_score from 1964 as t1	2-10784349-10
list all information about 1964 .	SELECT * FROM 1964	2-10784349-10
find all home team score that have fewer than three in 1964 .	SELECT t1.home_team_score from 1964 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) < 3	2-10784349-10
return each home team score with the number of 1964 in ascending order of the number of home team score .	SELECT t1.home_team_score , COUNT ( * ) from 1964 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * )	2-10784349-10
what is the date and venue of the 1964 with the top 5 smallest crowd ?	SELECT t1.date , t1.venue from 1964 as t1 ORDER BY t1.crowd LIMIT 5	2-10784349-10
