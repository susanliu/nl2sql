what are the average byes of mininera , grouped by mininera dfl ?	SELECT AVG ( t1.byes ) , t1.mininera_dfl from mininera as t1 GROUP BY t1.mininera_dfl	2-17732555-3
return the mininera dfl of the mininera with the fewest byes .	SELECT t1.mininera_dfl from mininera as t1 ORDER BY t1.byes ASC LIMIT 1	2-17732555-3
find the mininera dfl of mininera who have both 10 and 80 draws .	SELECT t1.mininera_dfl from mininera as t1 WHERE t1.draws = 10 INTERSECT SELECT t1.mininera_dfl from mininera as t1 WHERE t1.draws = 80	2-17732555-3
what are the mininera dfl of all mininera with mininera dfl that is 10 ?	SELECT t1.mininera_dfl from mininera as t1 GROUP BY t1.mininera_dfl HAVING COUNT ( * ) = 10	2-17732555-3
find the mininera dfl who has exactly 10 mininera .	SELECT t1.mininera_dfl from mininera as t1 GROUP BY t1.mininera_dfl HAVING COUNT ( * ) = 10	2-17732555-3
what is all the information on the mininera with the largest number of mininera dfl ?	SELECT * from mininera as t1 ORDER BY t1.mininera_dfl DESC LIMIT 1	2-17732555-3
find the distinct mininera dfl of mininera having against between 10 and 33 .	SELECT DISTINCT t1.mininera_dfl from mininera as t1 WHERE t1.against BETWEEN 10 AND 33	2-17732555-3
what are the average losses of mininera for different mininera dfl ?	SELECT AVG ( t1.losses ) , t1.mininera_dfl from mininera as t1 GROUP BY t1.mininera_dfl	2-17732555-3
which mininera dfl have greater wins than that of any wins in mininera ?	SELECT t1.mininera_dfl from mininera as t1 WHERE t1.wins > ( SELECT MIN ( t1.wins ) from mininera as t1 )	2-17732555-3
what are the mininera dfl of mininera whose mininera dfl are not 10 ?	SELECT t1.mininera_dfl from mininera as t1 WHERE t1.mininera_dfl ! = 10	2-17732555-3
