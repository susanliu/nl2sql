find the time/retired of the 1985 which have driver 10 but not 89 .	SELECT t1.time/retired from 1985 as t1 WHERE t1.driver = 10 EXCEPT SELECT t1.time/retired from 1985 as t1 WHERE t1.driver = 89	2-1122942-2
find the constructor of 1985 who have laps of both 10 and 62 .	SELECT t1.constructor from 1985 as t1 WHERE t1.laps = 10 INTERSECT SELECT t1.constructor from 1985 as t1 WHERE t1.laps = 62	2-1122942-2
return the maximum and minimum laps across all 1985 .	SELECT MAX ( t1.laps ) , MIN ( t1.laps ) from 1985 as t1	2-1122942-2
what is the constructor of the 1985 with the largest grid ?	SELECT t1.constructor from 1985 as t1 WHERE t1.grid = ( SELECT MAX ( t1.grid ) from 1985 as t1 )	2-1122942-2
find the distinct COLUMN_NAME,0} of all 1985 that have grid higher than some 1985 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.driver from 1985 as t1 WHERE t1.grid > ( SELECT MIN ( t1.driver ) from 1985 as t1 WHERE t1.driver = 10 )	2-1122942-2
which time/retired have an average grid over 10 ?	SELECT t1.time/retired from 1985 as t1 GROUP BY t1.time/retired HAVING AVG ( t1.grid ) >= 10	2-1122942-2
what are total laps for each constructor ?	SELECT t1.constructor , SUM ( t1.laps ) from 1985 as t1 GROUP BY t1.constructor	2-1122942-2
which t1.time/retired has least number of 1985 ?	SELECT t1.time/retired from 1985 as t1 GROUP BY t1.time/retired ORDER BY COUNT ( * ) LIMIT 1	2-1122942-2
return the driver of the largest laps.	SELECT t1.driver from 1985 as t1 ORDER BY t1.laps DESC LIMIT 1	2-1122942-2
show the constructor , driver , and time/retired of all the 1985 .	SELECT t1.constructor , t1.driver , t1.time/retired from 1985 as t1	2-1122942-2
