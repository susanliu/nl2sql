what is the constructor and time/retired of every 1969 that has a laps lower than average ?	SELECT t1.constructor , t1.time/retired from 1969 as t1 WHERE t1.laps < ( SELECT AVG ( t1.laps ) {FROM, 3} )	2-1122414-1
which driver has most number of 1969 ?	SELECT t1.driver from 1969 as t1 GROUP BY t1.driver ORDER BY COUNT ( * ) DESC LIMIT 1	2-1122414-1
show all information on the 1969 that has the largest number of constructor.	SELECT * from 1969 as t1 ORDER BY t1.constructor DESC LIMIT 1	2-1122414-1
find the distinct constructor of 1969 having grid between 10 and 53 .	SELECT DISTINCT t1.constructor from 1969 as t1 WHERE t1.grid BETWEEN 10 AND 53	2-1122414-1
show constructor and the number of distinct time/retired in each constructor .	SELECT t1.constructor , COUNT ( DISTINCT t1.time/retired ) from 1969 as t1 GROUP BY t1.constructor	2-1122414-1
what is the driver of the 1969 who has the highest number of 1969 ?	SELECT t1.driver from 1969 as t1 GROUP BY t1.driver ORDER BY COUNT ( * ) DESC LIMIT 1	2-1122414-1
return the smallest laps for every driver .	SELECT MIN ( t1.laps ) , t1.driver from 1969 as t1 GROUP BY t1.driver	2-1122414-1
which driver has both 1969 with less than 10 laps and 1969 with more than 62 laps ?	SELECT t1.driver from 1969 as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.driver from 1969 as t1 WHERE t1.laps > 62	2-1122414-1
what is the minimum laps in each constructor ?	SELECT MIN ( t1.laps ) , t1.constructor from 1969 as t1 GROUP BY t1.constructor	2-1122414-1
count the number of 1969 that have an time/retired containing 10 .	SELECT COUNT ( * ) from 1969 as t1 WHERE t1.time/retired LIKE 10	2-1122414-1
