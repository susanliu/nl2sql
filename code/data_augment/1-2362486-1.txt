what is the surface of the ken with least number of surface ?	SELECT t1.surface from ken as t1 GROUP BY t1.surface ORDER BY COUNT ( * ) ASC LIMIT 1	1-2362486-1
return the partner and outcome of ken with the five lowest year .	SELECT t1.partner , t1.outcome from ken as t1 ORDER BY t1.year LIMIT 5	1-2362486-1
return the championship of the largest year.	SELECT t1.championship from ken as t1 ORDER BY t1.year DESC LIMIT 1	1-2362486-1
how many ken does each outcome have ?	SELECT t1.outcome , COUNT ( * ) from ken as t1 GROUP BY t1.outcome ORDER BY COUNT ( * )	1-2362486-1
what are the championship and partner of the {COLUMN} who have year above five or year below ten ?	SELECT t1.championship , t1.partner from ken as t1 WHERE t1.year > 5 OR t1.year < 10	1-2362486-1
what are the outcome and championship of each ken ?	SELECT t1.outcome , t1.championship from ken as t1	1-2362486-1
what are the championship of ken , sorted by their frequency?	SELECT t1.championship from ken as t1 GROUP BY t1.championship ORDER BY COUNT ( * ) ASC LIMIT 1	1-2362486-1
what are the distinct COLUMN_NAME,0} of ken with year higher than any ken from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.score_in_the_final from ken as t1 WHERE t1.year > ( SELECT MIN ( t1.score_in_the_final ) from ken as t1 WHERE t1.surface = 10 )	1-2362486-1
find the score in the final of ken who have year of both 10 and 66 .	SELECT t1.score_in_the_final from ken as t1 WHERE t1.year = 10 INTERSECT SELECT t1.score_in_the_final from ken as t1 WHERE t1.year = 66	1-2362486-1
find the surface of ken who have year of both 10 and 50 .	SELECT t1.surface from ken as t1 WHERE t1.year = 10 INTERSECT SELECT t1.surface from ken as t1 WHERE t1.year = 50	1-2362486-1
