find the pilot of the northrop with the highest mach.	SELECT t1.pilot from northrop as t1 WHERE t1.mach = ( SELECT MAX ( t1.mach ) from northrop as t1 )	2-1284217-1
list all information about northrop .	SELECT * FROM northrop	2-1284217-1
how many northrop are there that have more than {VALUE},0 pilot ?	SELECT COUNT ( * ) from northrop as t1 GROUP BY t1.pilot HAVING COUNT ( * ) > 10 	2-1284217-1
show the vehicle flight # and the number of unique date containing each vehicle flight # .	SELECT t1.vehicle_flight_ , COUNT ( DISTINCT t1.date ) from northrop as t1 GROUP BY t1.vehicle_flight_	2-1284217-1
show the pilot of northrop whose vehicle flight # are not 10.	SELECT t1.pilot from northrop as t1 WHERE t1.vehicle_flight_ ! = 10	2-1284217-1
what is the vehicle flight # and duration of every northrop that has a mach lower than average ?	SELECT t1.vehicle_flight_ , t1.duration from northrop as t1 WHERE t1.mach < ( SELECT AVG ( t1.mach ) {FROM, 3} )	2-1284217-1
find the vehicle flight # of the northrop with the highest mach .	SELECT t1.vehicle_flight_ from northrop as t1 ORDER BY t1.mach DESC LIMIT 1	2-1284217-1
what is the count of northrop with more than 10 vehicle flight # ?	SELECT COUNT ( * ) from northrop as t1 GROUP BY t1.vehicle_flight_ HAVING COUNT ( * ) > 10 	2-1284217-1
show the date with fewer than 3 northrop .	SELECT t1.date from northrop as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	2-1284217-1
what is the date of the northrop with the largest mach ?	SELECT t1.date from northrop as t1 WHERE t1.mach = ( SELECT MAX ( t1.mach ) from northrop as t1 )	2-1284217-1
