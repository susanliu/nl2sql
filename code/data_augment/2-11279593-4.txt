list the clean & jerk which average snatch is above 10 .	SELECT t1.clean_ from weightlifting as t1 GROUP BY t1.clean_ HAVING AVG ( t1.snatch ) >= 10	2-11279593-4
return the clean & jerk of weightlifting that do not have the total ( kg ) 10 .	SELECT t1.clean_ from weightlifting as t1 WHERE t1.total_ ! = 10	2-11279593-4
return the name of weightlifting that do not have the name 10 .	SELECT t1.name from weightlifting as t1 WHERE t1.name ! = 10	2-11279593-4
which total ( kg ) have less than 3 in weightlifting ?	SELECT t1.total_ from weightlifting as t1 GROUP BY t1.total_ HAVING COUNT ( * ) < 3	2-11279593-4
find the total ( kg ) that have 10 weightlifting .	SELECT t1.total_ from weightlifting as t1 GROUP BY t1.total_ HAVING COUNT ( * ) = 10	2-11279593-4
find the clean & jerk of the weightlifting that is most frequent across all clean & jerk .	SELECT t1.clean_ from weightlifting as t1 GROUP BY t1.clean_ ORDER BY COUNT ( * ) DESC LIMIT 1	2-11279593-4
find the name which have exactly 10 weightlifting .	SELECT t1.name from weightlifting as t1 GROUP BY t1.name HAVING COUNT ( * ) = 10	2-11279593-4
Return all columns in weightlifting .	SELECT * FROM weightlifting	2-11279593-4
list the name which average bodyweight is above 10 .	SELECT t1.name from weightlifting as t1 GROUP BY t1.name HAVING AVG ( t1.bodyweight ) >= 10	2-11279593-4
how many weightlifting are there in total ( kg ) 10 or 20 ?	SELECT COUNT ( * ) from weightlifting as t1 WHERE t1.total_ = 10 OR t1.total_ = 20	2-11279593-4
