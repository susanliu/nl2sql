what are the official name and official name of all victoria sorted by decreasing population ?	SELECT t1.official_name , t1.official_name from victoria as t1 ORDER BY t1.population DESC	2-176524-1
select the average area km 2 of each victoria 's census ranking .	SELECT AVG ( t1.area_km_2 ) , t1.census_ranking from victoria as t1 GROUP BY t1.census_ranking	2-176524-1
which census ranking has both victoria with less than 10 area km 2 and victoria with more than 88 area km 2 ?	SELECT t1.census_ranking from victoria as t1 WHERE t1.area_km_2 < 10 INTERSECT SELECT t1.census_ranking from victoria as t1 WHERE t1.area_km_2 > 88	2-176524-1
find the official name which have exactly 10 victoria .	SELECT t1.official_name from victoria as t1 GROUP BY t1.official_name HAVING COUNT ( * ) = 10	2-176524-1
count the number of victoria in census ranking 10 or 7 .	SELECT COUNT ( * ) from victoria as t1 WHERE t1.census_ranking = 10 OR t1.census_ranking = 7	2-176524-1
list the census ranking and status of all victoria sorted by area km 2 in descending order .	SELECT t1.census_ranking , t1.status from victoria as t1 ORDER BY t1.area_km_2 DESC	2-176524-1
which official name has most number of victoria ?	SELECT t1.official_name from victoria as t1 GROUP BY t1.official_name ORDER BY COUNT ( * ) DESC LIMIT 1	2-176524-1
what are the census ranking of victoria , sorted by their frequency?	SELECT t1.census_ranking from victoria as t1 GROUP BY t1.census_ranking ORDER BY COUNT ( * ) ASC LIMIT 1	2-176524-1
list the census ranking which average population is above 10 .	SELECT t1.census_ranking from victoria as t1 GROUP BY t1.census_ranking HAVING AVG ( t1.population ) >= 10	2-176524-1
what are the status , status , and status of each victoria ?	SELECT t1.status , t1.status , t1.census_ranking from victoria as t1	2-176524-1
