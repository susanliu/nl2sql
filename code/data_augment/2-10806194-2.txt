what are the away team score that have greater crowd than any crowd in 1937 ?	SELECT t1.away_team_score from 1937 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1937 as t1 )	2-10806194-2
what are the away team and date of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.away_team , t1.date from 1937 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10806194-2
find the away team of 1937 which are away team 10 but not away team 23 .	SELECT t1.away_team from 1937 as t1 WHERE t1.away_team = 10 EXCEPT SELECT t1.away_team from 1937 as t1 WHERE t1.away_team = 23	2-10806194-2
what are the distinct home team score with crowd between 10 and 61 ?	SELECT DISTINCT t1.home_team_score from 1937 as t1 WHERE t1.crowd BETWEEN 10 AND 61	2-10806194-2
show the away team and the corresponding number of 1937 sorted by the number of away team in ascending order .	SELECT t1.away_team , COUNT ( * ) from 1937 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * )	2-10806194-2
show the home team score of 1937 whose away team are not 10.	SELECT t1.home_team_score from 1937 as t1 WHERE t1.away_team ! = 10	2-10806194-2
find the date of 1937 which are in 10 home team but not in 5 home team .	SELECT t1.date from 1937 as t1 WHERE t1.home_team = 10 EXCEPT SELECT t1.date from 1937 as t1 WHERE t1.home_team = 5	2-10806194-2
show away team score and date of 1937 .	SELECT t1.away_team_score , t1.date from 1937 as t1	2-10806194-2
what are the away team score of 1937 with crowd greater than the average of all 1937 ?	SELECT t1.away_team_score from 1937 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1937 as t1	2-10806194-2
how many 1937 ' away team score have the word 10 in them ?	SELECT COUNT ( * ) from 1937 as t1 WHERE t1.away_team_score LIKE 10	2-10806194-2
