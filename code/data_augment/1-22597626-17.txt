how many john correspond to each score in the final? show the result in ascending order.	SELECT t1.score_in_the_final , COUNT ( * ) from john as t1 GROUP BY t1.score_in_the_final ORDER BY COUNT ( * )	1-22597626-17
return the maximum and minimum year across all john .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from john as t1	1-22597626-17
what is the partner and championship of every john that has a year lower than average ?	SELECT t1.partner , t1.championship from john as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	1-22597626-17
return the championship of john that do not have the opponents in the final 10 .	SELECT t1.championship from john as t1 WHERE t1.opponents_in_the_final ! = 10	1-22597626-17
return the opponents in the final of the largest year.	SELECT t1.opponents_in_the_final from john as t1 ORDER BY t1.year DESC LIMIT 1	1-22597626-17
what are the average year of john , grouped by outcome ?	SELECT AVG ( t1.year ) , t1.outcome from john as t1 GROUP BY t1.outcome	1-22597626-17
please show the opponents in the final of the john that have at least 10 records .	SELECT t1.opponents_in_the_final from john as t1 GROUP BY t1.opponents_in_the_final HAVING COUNT ( * ) >= 10	1-22597626-17
list all opponents in the final which have year higher than the average .	SELECT t1.opponents_in_the_final from john as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from john as t1	1-22597626-17
list the score in the final which average year is above 10 .	SELECT t1.score_in_the_final from john as t1 GROUP BY t1.score_in_the_final HAVING AVG ( t1.year ) >= 10	1-22597626-17
what are the partner of john , sorted by their frequency?	SELECT t1.partner from john as t1 GROUP BY t1.partner ORDER BY COUNT ( * ) ASC LIMIT 1	1-22597626-17
