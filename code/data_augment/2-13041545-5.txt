which record have an average attendance over 10 ?	SELECT t1.record from 2006 as t1 GROUP BY t1.record HAVING AVG ( t1.attendance ) >= 10	2-13041545-5
show the opponent and loss with at least 10 opponent .	SELECT t1.opponent , t1.loss from 2006 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-13041545-5
what is the opponent of 2006 with the maximum attendance across all 2006 ?	SELECT t1.opponent from 2006 as t1 WHERE t1.attendance = ( SELECT MAX ( t1.attendance ) from 2006 as t1 )	2-13041545-5
what are the record of 2006 whose score is not 10 ?	SELECT t1.record from 2006 as t1 WHERE t1.score ! = 10	2-13041545-5
show the loss with fewer than 3 2006 .	SELECT t1.loss from 2006 as t1 GROUP BY t1.loss HAVING COUNT ( * ) < 3	2-13041545-5
return the opponent and score of 2006 with the five lowest attendance .	SELECT t1.opponent , t1.score from 2006 as t1 ORDER BY t1.attendance LIMIT 5	2-13041545-5
return each date with the number of 2006 in ascending order of the number of date .	SELECT t1.date , COUNT ( * ) from 2006 as t1 GROUP BY t1.date ORDER BY COUNT ( * )	2-13041545-5
find the score of 2006 which have 10 but no 21 as date .	SELECT t1.score from 2006 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.score from 2006 as t1 WHERE t1.date = 21	2-13041545-5
return the loss of the 2006 that has the fewest corresponding loss .	SELECT t1.loss from 2006 as t1 GROUP BY t1.loss ORDER BY COUNT ( * ) ASC LIMIT 1	2-13041545-5
Return all columns in 2006 .	SELECT * FROM 2006	2-13041545-5
