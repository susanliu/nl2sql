what are the date of all 1967 with opponent that is 10 ?	SELECT t1.date from 1967 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	1-21197135-1
what are the result and date of each 1967 , listed in descending order by game ?	SELECT t1.result , t1.date from 1967 as t1 ORDER BY t1.game DESC	1-21197135-1
what are the opponent of all 1967 that have 10 or more 1967 ?	SELECT t1.opponent from 1967 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	1-21197135-1
what are total cowboys points for each result ?	SELECT t1.result , SUM ( t1.cowboys_points ) from 1967 as t1 GROUP BY t1.result	1-21197135-1
how many 1967 have record that contain the word 10 ?	SELECT COUNT ( * ) from 1967 as t1 WHERE t1.record LIKE 10	1-21197135-1
how many 1967 are there in opponent 10 or 50 ?	SELECT COUNT ( * ) from 1967 as t1 WHERE t1.opponent = 10 OR t1.opponent = 50	1-21197135-1
find the record of 1967 which are in 10 date but not in 62 date .	SELECT t1.record from 1967 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.record from 1967 as t1 WHERE t1.date = 62	1-21197135-1
which opponent has the least game ?	SELECT t1.opponent from 1967 as t1 ORDER BY t1.game ASC LIMIT 1	1-21197135-1
show all information on the 1967 that has the largest number of opponent.	SELECT * from 1967 as t1 ORDER BY t1.opponent DESC LIMIT 1	1-21197135-1
find the opponent of the 1967 which have date 10 but not 12 .	SELECT t1.opponent from 1967 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.opponent from 1967 as t1 WHERE t1.date = 12	1-21197135-1
