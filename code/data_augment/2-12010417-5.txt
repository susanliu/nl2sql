which name have greater rank than that of any rank in 1969 ?	SELECT t1.name from 1969 as t1 WHERE t1.rank > ( SELECT MIN ( t1.rank ) from 1969 as t1 )	2-12010417-5
find the name of the 1969 with the highest rank.	SELECT t1.name from 1969 as t1 WHERE t1.rank = ( SELECT MAX ( t1.rank ) from 1969 as t1 )	2-12010417-5
what are the {nation of all the 1969 , and the total placings by each ?	SELECT t1.nation , SUM ( t1.placings ) from 1969 as t1 GROUP BY t1.nation	2-12010417-5
which name have greater placings than that of any placings in 1969 ?	SELECT t1.name from 1969 as t1 WHERE t1.placings > ( SELECT MIN ( t1.placings ) from 1969 as t1 )	2-12010417-5
find the nation of the 1969 with the largest rank .	SELECT t1.nation from 1969 as t1 ORDER BY t1.rank DESC LIMIT 1	2-12010417-5
which name is the most frequent name?	SELECT t1.name from 1969 as t1 GROUP BY t1.name ORDER BY COUNT ( * ) DESC LIMIT 1	2-12010417-5
which name has both 1969 with less than 10 rank and 1969 with more than 80 rank ?	SELECT t1.name from 1969 as t1 WHERE t1.rank < 10 INTERSECT SELECT t1.name from 1969 as t1 WHERE t1.rank > 80	2-12010417-5
what is the name of 1969 with the maximum placings across all 1969 ?	SELECT t1.name from 1969 as t1 WHERE t1.placings = ( SELECT MAX ( t1.placings ) from 1969 as t1 )	2-12010417-5
what are the distinct nation with rank between 10 and 39 ?	SELECT DISTINCT t1.nation from 1969 as t1 WHERE t1.rank BETWEEN 10 AND 39	2-12010417-5
what is the nation and nation of the 1969 with the top 5 smallest placings ?	SELECT t1.nation , t1.nation from 1969 as t1 ORDER BY t1.placings LIMIT 5	2-12010417-5
