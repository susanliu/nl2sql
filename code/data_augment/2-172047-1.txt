show the size of network number bit field of classful who have at least 10 classful .	SELECT t1.size_of_network_number_bit_field from classful as t1 GROUP BY t1.size_of_network_number_bit_field HAVING COUNT ( * ) >= 10	2-172047-1
which start address has the least leading bits ?	SELECT t1.start_address from classful as t1 ORDER BY t1.leading_bits ASC LIMIT 1	2-172047-1
what are the size of rest bit field of classful with leading bits above the average leading bits across all classful ?	SELECT t1.size_of_rest_bit_field from classful as t1 WHERE t1.leading_bits > ( SELECT AVG ( t1.leading_bits ) from classful as t1	2-172047-1
show class for all classful whose leading bits are greater than the average .	SELECT t1.class from classful as t1 WHERE t1.leading_bits > ( SELECT AVG ( t1.leading_bits ) from classful as t1	2-172047-1
list the size of rest bit field which average leading bits is above 10 .	SELECT t1.size_of_rest_bit_field from classful as t1 GROUP BY t1.size_of_rest_bit_field HAVING AVG ( t1.leading_bits ) >= 10	2-172047-1
what is the start address and addresses per network for the classful with the rank 5 smallest leading bits ?	SELECT t1.start_address , t1.addresses_per_network from classful as t1 ORDER BY t1.leading_bits LIMIT 5	2-172047-1
find the number of classful whose size of rest bit field contain the word 10 .	SELECT COUNT ( * ) from classful as t1 WHERE t1.size_of_rest_bit_field LIKE 10	2-172047-1
show the addresses per network of classful who have at least 10 classful .	SELECT t1.addresses_per_network from classful as t1 GROUP BY t1.addresses_per_network HAVING COUNT ( * ) >= 10	2-172047-1
what are the start address of classful whose addresses per network are not 10 ?	SELECT t1.start_address from classful as t1 WHERE t1.addresses_per_network ! = 10	2-172047-1
find the addresses per network of the classful with the highest leading bits .	SELECT t1.addresses_per_network from classful as t1 ORDER BY t1.leading_bits DESC LIMIT 1	2-172047-1
