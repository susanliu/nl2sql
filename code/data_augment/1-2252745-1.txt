return the iso 3166-2 of regions for which the name ( english ) is not 10 ?	SELECT t1.iso_3166-2 from regions as t1 WHERE t1.name_ ! = 10	1-2252745-1
which iso 3166-2 has most number of regions ?	SELECT t1.iso_3166-2 from regions as t1 GROUP BY t1.iso_3166-2 ORDER BY COUNT ( * ) DESC LIMIT 1	1-2252745-1
give the maximum and minimum # of all regions .	SELECT MAX ( t1.# ) , MIN ( t1.# ) from regions as t1	1-2252745-1
find the name ( english ) of the regions with the highest population 2008-07-01 .	SELECT t1.name_ from regions as t1 ORDER BY t1.population_2008-07-01 DESC LIMIT 1	1-2252745-1
how many regions are there in administrative centre 10 or 17 ?	SELECT COUNT ( * ) from regions as t1 WHERE t1.administrative_centre = 10 OR t1.administrative_centre = 17	1-2252745-1
what is the minimum # in each name ?	SELECT MIN ( t1.# ) , t1.name from regions as t1 GROUP BY t1.name	1-2252745-1
which name has the least area ( km ² ) ?	SELECT t1.name from regions as t1 ORDER BY t1.area_ ASC LIMIT 1	1-2252745-1
show the iso 3166-2 of regions who have at least 10 regions .	SELECT t1.iso_3166-2 from regions as t1 GROUP BY t1.iso_3166-2 HAVING COUNT ( * ) >= 10	1-2252745-1
which administrative centre is the most frequent administrative centre?	SELECT t1.administrative_centre from regions as t1 GROUP BY t1.administrative_centre ORDER BY COUNT ( * ) DESC LIMIT 1	1-2252745-1
what are the administrative centre of regions whose name ( english ) are not 10 ?	SELECT t1.administrative_centre from regions as t1 WHERE t1.name_ ! = 10	1-2252745-1
