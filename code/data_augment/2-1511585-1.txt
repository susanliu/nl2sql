what are the average founded of eastern for different nickname ?	SELECT AVG ( t1.founded ) , t1.nickname from eastern as t1 GROUP BY t1.nickname	2-1511585-1
what is the primary conference and affiliation for the eastern with the rank 5 smallest founded ?	SELECT t1.primary_conference , t1.affiliation from eastern as t1 ORDER BY t1.founded LIMIT 5	2-1511585-1
what is the location of the eastern with the minimum founded ?	SELECT t1.location from eastern as t1 ORDER BY t1.founded ASC LIMIT 1	2-1511585-1
find the number of eastern that have more than 10 primary conference .	SELECT COUNT ( * ) from eastern as t1 GROUP BY t1.primary_conference HAVING COUNT ( * ) > 10 	2-1511585-1
what are the school that have greater enrollment than any enrollment in eastern ?	SELECT t1.school from eastern as t1 WHERE t1.enrollment > ( SELECT MIN ( t1.enrollment ) from eastern as t1 )	2-1511585-1
find all affiliation that have fewer than three in eastern .	SELECT t1.affiliation from eastern as t1 GROUP BY t1.affiliation HAVING COUNT ( * ) < 3	2-1511585-1
what are the distinct nickname with founded between 10 and 64 ?	SELECT DISTINCT t1.nickname from eastern as t1 WHERE t1.founded BETWEEN 10 AND 64	2-1511585-1
return the school of the eastern with the fewest founded .	SELECT t1.school from eastern as t1 ORDER BY t1.founded ASC LIMIT 1	2-1511585-1
what is the primary conference of the eastern with the minimum enrollment ?	SELECT t1.primary_conference from eastern as t1 ORDER BY t1.enrollment ASC LIMIT 1	2-1511585-1
what are total founded for each nickname ?	SELECT t1.nickname , SUM ( t1.founded ) from eastern as t1 GROUP BY t1.nickname	2-1511585-1
