what is the home team and venue of every 1948 that has a crowd lower than average ?	SELECT t1.home_team , t1.venue from 1948 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10809529-9
find the away team of 1948 which have both 10 and 89 as away team .	SELECT t1.away_team from 1948 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.away_team from 1948 as t1 WHERE t1.crowd = 89	2-10809529-9
find the date of the 1948 that is most frequent across all date .	SELECT t1.date from 1948 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-10809529-9
what are the {away team of all the 1948 , and the total crowd by each ?	SELECT t1.away_team , SUM ( t1.crowd ) from 1948 as t1 GROUP BY t1.away_team	2-10809529-9
sort the list of home team and date of all 1948 in the descending order of crowd .	SELECT t1.home_team , t1.date from 1948 as t1 ORDER BY t1.crowd DESC	2-10809529-9
what are the {away team score of all the 1948 , and the total crowd by each ?	SELECT t1.away_team_score , SUM ( t1.crowd ) from 1948 as t1 GROUP BY t1.away_team_score	2-10809529-9
what are the date , date , and venue of each 1948 ?	SELECT t1.date , t1.date , t1.venue from 1948 as t1	2-10809529-9
what is the date and away team of the 1948 with maximum crowd ?	SELECT t1.date , t1.away_team from 1948 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809529-9
find the away team score of 1948 whose crowd is more than the average crowd of 1948 .	SELECT t1.away_team_score from 1948 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1948 as t1	2-10809529-9
how many 1948 are there in venue 10 or 76 ?	SELECT COUNT ( * ) from 1948 as t1 WHERE t1.venue = 10 OR t1.venue = 76	2-10809529-9
