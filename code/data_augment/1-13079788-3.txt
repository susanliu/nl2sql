show the gtc winner , date , and length of all the 2007 .	SELECT t1.gtc_winner , t1.date , t1.length from 2007 as t1	1-13079788-3
what are the date of the 2007 with date other than 10 ?	SELECT t1.date from 2007 as t1 WHERE t1.pole_position ! = 10	1-13079788-3
what is the pole position of highest round ?	SELECT t1.pole_position from 2007 as t1 ORDER BY t1.round DESC LIMIT 1	1-13079788-3
which length has most number of 2007 ?	SELECT t1.length from 2007 as t1 GROUP BY t1.length ORDER BY COUNT ( * ) DESC LIMIT 1	1-13079788-3
what are the date and gtc winner of the {COLUMN} who have round above five or round below ten ?	SELECT t1.date , t1.gtc_winner from 2007 as t1 WHERE t1.round > 5 OR t1.round < 10	1-13079788-3
find the date and pole position of the 2007 with at least 10 length .	SELECT t1.date , t1.pole_position from 2007 as t1 GROUP BY t1.length HAVING COUNT ( * ) >= 10	1-13079788-3
how many 2007 are there that have more than {VALUE},0 length ?	SELECT COUNT ( * ) from 2007 as t1 GROUP BY t1.length HAVING COUNT ( * ) > 10 	1-13079788-3
how many 2007 ' gt3 winner have the word 10 in them ?	SELECT COUNT ( * ) from 2007 as t1 WHERE t1.gt3_winner LIKE 10	1-13079788-3
which length has the least round ?	SELECT t1.length from 2007 as t1 ORDER BY t1.round ASC LIMIT 1	1-13079788-3
find all date that have fewer than three in 2007 .	SELECT t1.date from 2007 as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	1-13079788-3
