what are the builder of the south that have exactly 10 south ?	SELECT t1.builder from south as t1 GROUP BY t1.builder HAVING COUNT ( * ) = 10	1-29753553-1
what are the builder that have greater year than any year in south ?	SELECT t1.builder from south as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from south as t1 )	1-29753553-1
return the maximum and minimum sar no . across all south .	SELECT MAX ( t1.sar_no_ ) , MIN ( t1.sar_no_ ) from south as t1	1-29753553-1
find the driver diameter of south whose sar no . is more than the average sar no . of south .	SELECT t1.driver_diameter from south as t1 WHERE t1.sar_no_ > ( SELECT AVG ( t1.sar_no_ ) from south as t1	1-29753553-1
find the firebox and firebox of the south with at least 10 driver diameter .	SELECT t1.firebox , t1.firebox from south as t1 GROUP BY t1.driver_diameter HAVING COUNT ( * ) >= 10	1-29753553-1
which driver diameter have greater works no . than that of any works no . in south ?	SELECT t1.driver_diameter from south as t1 WHERE t1.works_no_ > ( SELECT MIN ( t1.works_no_ ) from south as t1 )	1-29753553-1
what is the builder and builder of the south with maximum year ?	SELECT t1.builder , t1.builder from south as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	1-29753553-1
find the firebox of the south which have builder 10 but not 79 .	SELECT t1.firebox from south as t1 WHERE t1.builder = 10 EXCEPT SELECT t1.firebox from south as t1 WHERE t1.builder = 79	1-29753553-1
find the builder and builder of the south whose sar no . is lower than the average sar no . of all south .	SELECT t1.builder , t1.builder from south as t1 WHERE t1.sar_no_ < ( SELECT AVG ( t1.sar_no_ ) {FROM, 3} )	1-29753553-1
show the builder and driver diameter with at least 10 firebox .	SELECT t1.builder , t1.driver_diameter from south as t1 GROUP BY t1.firebox HAVING COUNT ( * ) >= 10	1-29753553-1
