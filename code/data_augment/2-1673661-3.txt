how many maxie does each world rank have ?	SELECT t1.world_rank , COUNT ( * ) from maxie as t1 GROUP BY t1.world_rank ORDER BY COUNT ( * )	2-1673661-3
show all information on the maxie that has the largest number of location.	SELECT * from maxie as t1 ORDER BY t1.location DESC LIMIT 1	2-1673661-3
which location has the least year ?	SELECT t1.location from maxie as t1 ORDER BY t1.year ASC LIMIT 1	2-1673661-3
find the location and world rank of the maxie whose result is lower than the average result of all maxie .	SELECT t1.location , t1.world_rank from maxie as t1 WHERE t1.result < ( SELECT AVG ( t1.result ) {FROM, 3} )	2-1673661-3
what are the world rank that have greater year than any year in maxie ?	SELECT t1.world_rank from maxie as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from maxie as t1 )	2-1673661-3
which date has the least result ?	SELECT t1.date from maxie as t1 ORDER BY t1.result ASC LIMIT 1	2-1673661-3
what is the location and date of the maxie with maximum result ?	SELECT t1.location , t1.date from maxie as t1 WHERE t1.result = ( SELECT MAX ( t1.result ) {FROM, 3} )	2-1673661-3
what is the world rank of maxie with the maximum year across all maxie ?	SELECT t1.world_rank from maxie as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from maxie as t1 )	2-1673661-3
find the world rank of maxie whose year is more than the average year of maxie .	SELECT t1.world_rank from maxie as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from maxie as t1	2-1673661-3
which date has both maxie with less than 10 result and maxie with more than 23 result ?	SELECT t1.date from maxie as t1 WHERE t1.result < 10 INTERSECT SELECT t1.date from maxie as t1 WHERE t1.result > 23	2-1673661-3
