show away team score and the number of distinct away team score in each away team score .	SELECT t1.away_team_score , COUNT ( DISTINCT t1.away_team_score ) from 1931 as t1 GROUP BY t1.away_team_score	2-10789881-18
list the venue which average crowd is above 10 .	SELECT t1.venue from 1931 as t1 GROUP BY t1.venue HAVING AVG ( t1.crowd ) >= 10	2-10789881-18
show all date and the total crowd for each .	SELECT t1.date , SUM ( t1.crowd ) from 1931 as t1 GROUP BY t1.date	2-10789881-18
what is the home team score and date of every 1931 that has a crowd lower than average ?	SELECT t1.home_team_score , t1.date from 1931 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10789881-18
what is the home team score and home team of the 1931 with maximum crowd ?	SELECT t1.home_team_score , t1.home_team from 1931 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10789881-18
what is the maximum and mininum crowd {COLUMN} for all 1931 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1931 as t1	2-10789881-18
Show everything on 1931	SELECT * FROM 1931	2-10789881-18
how many distinct date correspond to each home team ?	SELECT t1.home_team , COUNT ( DISTINCT t1.date ) from 1931 as t1 GROUP BY t1.home_team	2-10789881-18
which t1.away_team has the smallest amount of 1931?	SELECT t1.away_team from 1931 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) LIMIT 1	2-10789881-18
what are the home team score and home team of each 1931 , listed in descending order by crowd ?	SELECT t1.home_team_score , t1.home_team from 1931 as t1 ORDER BY t1.crowd DESC	2-10789881-18
