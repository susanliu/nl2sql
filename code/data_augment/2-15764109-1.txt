what is the minimum points in each percentage ?	SELECT MIN ( t1.points ) , t1.percentage from 2007 as t1 GROUP BY t1.percentage	2-15764109-1
return the club and club of 2007 with the five lowest points .	SELECT t1.club , t1.club from 2007 as t1 ORDER BY t1.points LIMIT 5	2-15764109-1
what is the percentage and percentage of the 2007 with maximum points against ?	SELECT t1.percentage , t1.percentage from 2007 as t1 WHERE t1.points_against = ( SELECT MAX ( t1.points_against ) {FROM, 3} )	2-15764109-1
what are the percentage that have greater points against than any points against in 2007 ?	SELECT t1.percentage from 2007 as t1 WHERE t1.points_against > ( SELECT MIN ( t1.points_against ) from 2007 as t1 )	2-15764109-1
show club and percentage of 2007 .	SELECT t1.club , t1.percentage from 2007 as t1	2-15764109-1
return the maximum and minimum points for across all 2007 .	SELECT MAX ( t1.points_for ) , MIN ( t1.points_for ) from 2007 as t1	2-15764109-1
which club have an average wins over 10 ?	SELECT t1.club from 2007 as t1 GROUP BY t1.club HAVING AVG ( t1.wins ) >= 10	2-15764109-1
what are the percentage of 2007 , sorted by their frequency?	SELECT t1.percentage from 2007 as t1 GROUP BY t1.percentage ORDER BY COUNT ( * ) ASC LIMIT 1	2-15764109-1
what is the count of 2007 with more than 10 club ?	SELECT COUNT ( * ) from 2007 as t1 GROUP BY t1.club HAVING COUNT ( * ) > 10 	2-15764109-1
what are the percentage that have greater wins than any wins in 2007 ?	SELECT t1.percentage from 2007 as t1 WHERE t1.wins > ( SELECT MIN ( t1.wins ) from 2007 as t1 )	2-15764109-1
