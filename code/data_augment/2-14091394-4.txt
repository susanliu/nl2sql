what is the speed and pilot of every fai that has a position lower than average ?	SELECT t1.speed , t1.pilot from fai as t1 WHERE t1.position < ( SELECT AVG ( t1.position ) {FROM, 3} )	2-14091394-4
what are the glider with exactly 10 fai ?	SELECT t1.glider from fai as t1 GROUP BY t1.glider HAVING COUNT ( * ) = 10	2-14091394-4
what is the glider and distance for the fai with the rank 5 smallest position ?	SELECT t1.glider , t1.distance from fai as t1 ORDER BY t1.position LIMIT 5	2-14091394-4
show all pilot and corresponding number of fai in the ascending order of the numbers.	SELECT t1.pilot , COUNT ( * ) from fai as t1 GROUP BY t1.pilot ORDER BY COUNT ( * )	2-14091394-4
what are the speed for fai who have more than the average position?	SELECT t1.speed from fai as t1 WHERE t1.position > ( SELECT AVG ( t1.position ) from fai as t1	2-14091394-4
what is the distance of the fai with the minimum position ?	SELECT t1.distance from fai as t1 ORDER BY t1.position ASC LIMIT 1	2-14091394-4
which glider has both fai with less than 10 position and fai with more than 101 position ?	SELECT t1.glider from fai as t1 WHERE t1.position < 10 INTERSECT SELECT t1.glider from fai as t1 WHERE t1.position > 101	2-14091394-4
what are the average position of fai , grouped by speed ?	SELECT AVG ( t1.position ) , t1.speed from fai as t1 GROUP BY t1.speed	2-14091394-4
what is the distance and pilot of the fai with the top 5 smallest position ?	SELECT t1.distance , t1.pilot from fai as t1 ORDER BY t1.position LIMIT 5	2-14091394-4
how many different speed correspond to each glider ?	SELECT t1.glider , COUNT ( DISTINCT t1.speed ) from fai as t1 GROUP BY t1.glider	2-14091394-4
