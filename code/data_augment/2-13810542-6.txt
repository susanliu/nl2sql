how many 1912 are there that have more than {VALUE},0 opposing pitcher ?	SELECT COUNT ( * ) from 1912 as t1 GROUP BY t1.opposing_pitcher HAVING COUNT ( * ) > 10 	2-13810542-6
find the distinct location of 1912 having game between 10 and 66 .	SELECT DISTINCT t1.location from 1912 as t1 WHERE t1.game BETWEEN 10 AND 66	2-13810542-6
which location is the most frequent location?	SELECT t1.location from 1912 as t1 GROUP BY t1.location ORDER BY COUNT ( * ) DESC LIMIT 1	2-13810542-6
what is the date of the 1912 with the largest triple ?	SELECT t1.date from 1912 as t1 WHERE t1.triple = ( SELECT MAX ( t1.triple ) from 1912 as t1 )	2-13810542-6
show the location of the 1912 that has the most 1912 .	SELECT t1.location from 1912 as t1 GROUP BY t1.location ORDER BY COUNT ( * ) DESC LIMIT 1	2-13810542-6
which location has both 1912 with less than 10 game and 1912 with more than 79 game ?	SELECT t1.location from 1912 as t1 WHERE t1.game < 10 INTERSECT SELECT t1.location from 1912 as t1 WHERE t1.game > 79	2-13810542-6
show the inning shared by more than 10 1912 .	SELECT t1.inning from 1912 as t1 GROUP BY t1.inning HAVING COUNT ( * ) > 10	2-13810542-6
show all information on the 1912 that has the largest number of location.	SELECT * from 1912 as t1 ORDER BY t1.location DESC LIMIT 1	2-13810542-6
return the date and inning of 1912 with the five lowest game .	SELECT t1.date , t1.inning from 1912 as t1 ORDER BY t1.game LIMIT 5	2-13810542-6
what are the maximum and minimum triple across all 1912 ?	SELECT MAX ( t1.triple ) , MIN ( t1.triple ) from 1912 as t1	2-13810542-6
