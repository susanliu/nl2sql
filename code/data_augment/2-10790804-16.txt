what is the minimum crowd in each away team ?	SELECT MIN ( t1.crowd ) , t1.away_team from 1936 as t1 GROUP BY t1.away_team	2-10790804-16
find the venue of 1936 who have more than 10 1936 .	SELECT t1.venue from 1936 as t1 GROUP BY t1.venue HAVING COUNT ( * ) > 10	2-10790804-16
what are the away team score of 1936 with crowd above the average crowd across all 1936 ?	SELECT t1.away_team_score from 1936 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1936 as t1	2-10790804-16
find all away team that have fewer than three in 1936 .	SELECT t1.away_team from 1936 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) < 3	2-10790804-16
what is the home team of all 1936 whose crowd is higher than any 1936 ?	SELECT t1.home_team from 1936 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1936 as t1 )	2-10790804-16
list the date which average crowd is above 10 .	SELECT t1.date from 1936 as t1 GROUP BY t1.date HAVING AVG ( t1.crowd ) >= 10	2-10790804-16
what are the home team and date ?	SELECT t1.home_team , t1.date from 1936 as t1	2-10790804-16
what is the away team of 1936 with the maximum crowd across all 1936 ?	SELECT t1.away_team from 1936 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1936 as t1 )	2-10790804-16
which home team score have greater crowd than that of any crowd in 1936 ?	SELECT t1.home_team_score from 1936 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.crowd ) from 1936 as t1 )	2-10790804-16
find the away team of 1936 which are away team score 10 but not away team score 97 .	SELECT t1.away_team from 1936 as t1 WHERE t1.away_team_score = 10 EXCEPT SELECT t1.away_team from 1936 as t1 WHERE t1.away_team_score = 97	2-10790804-16
