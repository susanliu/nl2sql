find the away team score and away team score of the 1975 whose crowd is lower than the average crowd of all 1975 .	SELECT t1.away_team_score , t1.away_team_score from 1975 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10883333-12
return the venue of the 1975 that has the fewest corresponding venue .	SELECT t1.venue from 1975 as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) ASC LIMIT 1	2-10883333-12
which t1.away_team_score has least number of 1975 ?	SELECT t1.away_team_score from 1975 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) LIMIT 1	2-10883333-12
what are the away team score with exactly 10 1975 ?	SELECT t1.away_team_score from 1975 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) = 10	2-10883333-12
what is the away team and venue for the 1975 with the rank 5 smallest crowd ?	SELECT t1.away_team , t1.venue from 1975 as t1 ORDER BY t1.crowd LIMIT 5	2-10883333-12
return the date of the 1975 that has the fewest corresponding date .	SELECT t1.date from 1975 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-10883333-12
return the home team of the 1975 with the fewest crowd .	SELECT t1.home_team from 1975 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10883333-12
find the venue of 1975 which have 10 but no 65 as away team score .	SELECT t1.venue from 1975 as t1 WHERE t1.away_team_score = 10 EXCEPT SELECT t1.venue from 1975 as t1 WHERE t1.away_team_score = 65	2-10883333-12
return the different home team of 1975 , in ascending order of frequency .	SELECT t1.home_team from 1975 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-10883333-12
find the date and away team of the 1975 whose crowd is lower than the average crowd of all 1975 .	SELECT t1.date , t1.away_team from 1975 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10883333-12
