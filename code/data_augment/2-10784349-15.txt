find the venue of the 1964 with the highest crowd.	SELECT t1.venue from 1964 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1964 as t1 )	2-10784349-15
find the away team score of 1964 which have 10 but no 80 as date .	SELECT t1.away_team_score from 1964 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.away_team_score from 1964 as t1 WHERE t1.date = 80	2-10784349-15
what is all the information on the 1964 with the largest number of away team ?	SELECT * from 1964 as t1 ORDER BY t1.away_team DESC LIMIT 1	2-10784349-15
show the away team of the 1964 that has the most 1964 .	SELECT t1.away_team from 1964 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) DESC LIMIT 1	2-10784349-15
what are the away team of the 1964 that have exactly 10 1964 ?	SELECT t1.away_team from 1964 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) = 10	2-10784349-15
find the venue of 1964 who have more than 10 1964 .	SELECT t1.venue from 1964 as t1 GROUP BY t1.venue HAVING COUNT ( * ) > 10	2-10784349-15
list the home team which average crowd is above 10 .	SELECT t1.home_team from 1964 as t1 GROUP BY t1.home_team HAVING AVG ( t1.crowd ) >= 10	2-10784349-15
find the venue who has exactly 10 1964 .	SELECT t1.venue from 1964 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10784349-15
what are the distinct home team score with crowd between 10 and 100 ?	SELECT DISTINCT t1.home_team_score from 1964 as t1 WHERE t1.crowd BETWEEN 10 AND 100	2-10784349-15
return the venue of the largest crowd.	SELECT t1.venue from 1964 as t1 ORDER BY t1.crowd DESC LIMIT 1	2-10784349-15
