return the directed by of the list that has the fewest corresponding directed by .	SELECT t1.directed_by from list as t1 GROUP BY t1.directed_by ORDER BY COUNT ( * ) ASC LIMIT 1	1-19517621-4
find the original airdate of the list that have just 10 list .	SELECT t1.original_airdate from list as t1 GROUP BY t1.original_airdate HAVING COUNT ( * ) = 10	1-19517621-4
find the directed by for all list who have more than the average episode # .	SELECT t1.directed_by from list as t1 WHERE t1.episode_ > ( SELECT AVG ( t1.episode_ ) from list as t1	1-19517621-4
please show the different original airdate , ordered by the number of list that have each .	SELECT t1.original_airdate from list as t1 GROUP BY t1.original_airdate ORDER BY COUNT ( * ) ASC LIMIT 1	1-19517621-4
show the original airdate and written by with at least 10 written by .	SELECT t1.original_airdate , t1.written_by from list as t1 GROUP BY t1.written_by HAVING COUNT ( * ) >= 10	1-19517621-4
find the title of list which are in 10 title but not in 3 title .	SELECT t1.title from list as t1 WHERE t1.title = 10 EXCEPT SELECT t1.title from list as t1 WHERE t1.title = 3	1-19517621-4
what is the title and written by of the list with maximum viewers ?	SELECT t1.title , t1.written_by from list as t1 WHERE t1.viewers = ( SELECT MAX ( t1.viewers ) {FROM, 3} )	1-19517621-4
how many list are there in directed by 10 or 47 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.directed_by = 10 OR t1.directed_by = 47	1-19517621-4
find the distinct directed by of list having series # between 10 and 43 .	SELECT DISTINCT t1.directed_by from list as t1 WHERE t1.series_ BETWEEN 10 AND 43	1-19517621-4
give the maximum and minimum series # of all list .	SELECT MAX ( t1.series_ ) , MIN ( t1.series_ ) from list as t1	1-19517621-4
