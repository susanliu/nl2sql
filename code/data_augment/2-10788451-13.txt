return the away team score of the 1965 that has the fewest corresponding away team score .	SELECT t1.away_team_score from 1965 as t1 GROUP BY t1.away_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-10788451-13
Show everything on 1965	SELECT * FROM 1965	2-10788451-13
how many venue did each 1965 do, ordered by number of venue ?	SELECT t1.venue , COUNT ( * ) from 1965 as t1 GROUP BY t1.venue ORDER BY COUNT ( * )	2-10788451-13
what are the home team of 1965 whose venue are not 10 ?	SELECT t1.home_team from 1965 as t1 WHERE t1.venue ! = 10	2-10788451-13
what are the home team score of 1965 whose home team score is not 10 ?	SELECT t1.home_team_score from 1965 as t1 WHERE t1.home_team_score ! = 10	2-10788451-13
return the away team of 1965 that do not have the away team score 10 .	SELECT t1.away_team from 1965 as t1 WHERE t1.away_team_score ! = 10	2-10788451-13
find the away team score who has exactly 10 1965 .	SELECT t1.away_team_score from 1965 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) = 10	2-10788451-13
give the t1.home_team with the fewest 1965 .	SELECT t1.home_team from 1965 as t1 GROUP BY t1.home_team ORDER BY COUNT ( * ) LIMIT 1	2-10788451-13
show the home team of 1965 who have at least 10 1965 .	SELECT t1.home_team from 1965 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) >= 10	2-10788451-13
what are the average crowd of 1965 for different home team score ?	SELECT AVG ( t1.crowd ) , t1.home_team_score from 1965 as t1 GROUP BY t1.home_team_score	2-10788451-13
