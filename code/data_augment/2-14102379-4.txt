find the opponent and stadium of the 1961 whose week is lower than the average week of all 1961 .	SELECT t1.opponent , t1.stadium from 1961 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-14102379-4
give the stadium that has the most 1961 .	SELECT t1.stadium from 1961 as t1 GROUP BY t1.stadium ORDER BY COUNT ( * ) DESC LIMIT 1	2-14102379-4
find the number of 1961 whose stadium contain the word 10 .	SELECT COUNT ( * ) from 1961 as t1 WHERE t1.stadium LIKE 10	2-14102379-4
find the record of the 1961 with the largest week .	SELECT t1.record from 1961 as t1 ORDER BY t1.week DESC LIMIT 1	2-14102379-4
how many different date correspond to each result ?	SELECT t1.result , COUNT ( DISTINCT t1.date ) from 1961 as t1 GROUP BY t1.result	2-14102379-4
how many 1961 are there that have more than {VALUE},0 opponent ?	SELECT COUNT ( * ) from 1961 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	2-14102379-4
what are the record of 1961 whose result is not 10 ?	SELECT t1.record from 1961 as t1 WHERE t1.result ! = 10	2-14102379-4
what are the distinct COLUMN_NAME,0} of 1961 with attendance higher than any 1961 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.opponent from 1961 as t1 WHERE t1.attendance > ( SELECT MIN ( t1.opponent ) from 1961 as t1 WHERE t1.record = 10 )	2-14102379-4
find the distinct record of 1961 having attendance between 10 and 101 .	SELECT DISTINCT t1.record from 1961 as t1 WHERE t1.attendance BETWEEN 10 AND 101	2-14102379-4
what are the stadium of 1961 whose opponent is not 10 ?	SELECT t1.stadium from 1961 as t1 WHERE t1.opponent ! = 10	2-14102379-4
