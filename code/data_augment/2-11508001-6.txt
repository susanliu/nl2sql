which opponent has the least attendance ?	SELECT t1.opponent from 1998 as t1 ORDER BY t1.attendance ASC LIMIT 1	2-11508001-6
find the distinct loss of all 1998 that have a higher attendance than some 1998 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.loss from 1998 as t1 WHERE t1.attendance > ( SELECT MIN ( t1.loss ) from 1998 as t1 WHERE t1.record = 10 )	2-11508001-6
find the distinct date of all 1998 that have a higher attendance than some 1998 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.date from 1998 as t1 WHERE t1.attendance > ( SELECT MIN ( t1.date ) from 1998 as t1 WHERE t1.score = 10 )	2-11508001-6
what are the loss of all 1998 with date that is 10 ?	SELECT t1.loss from 1998 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-11508001-6
give the t1.loss with the fewest 1998 .	SELECT t1.loss from 1998 as t1 GROUP BY t1.loss ORDER BY COUNT ( * ) LIMIT 1	2-11508001-6
what are the score and opponent of all 1998 sorted by decreasing attendance ?	SELECT t1.score , t1.opponent from 1998 as t1 ORDER BY t1.attendance DESC	2-11508001-6
what are the opponent and record of all 1998 sorted by decreasing attendance ?	SELECT t1.opponent , t1.record from 1998 as t1 ORDER BY t1.attendance DESC	2-11508001-6
what is the t1.score of 1998 that has fewest number of 1998 ?	SELECT t1.score from 1998 as t1 GROUP BY t1.score ORDER BY COUNT ( * ) LIMIT 1	2-11508001-6
which score have an average attendance over 10 ?	SELECT t1.score from 1998 as t1 GROUP BY t1.score HAVING AVG ( t1.attendance ) >= 10	2-11508001-6
find the opponent of 1998 which have both 10 and 21 as opponent .	SELECT t1.opponent from 1998 as t1 WHERE t1.attendance = 10 INTERSECT SELECT t1.opponent from 1998 as t1 WHERE t1.attendance = 21	2-11508001-6
