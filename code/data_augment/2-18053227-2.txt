return the method of the largest round.	SELECT t1.method from cale as t1 ORDER BY t1.round DESC LIMIT 1	2-18053227-2
find the distinct location of cale having round between 10 and 68 .	SELECT DISTINCT t1.location from cale as t1 WHERE t1.round BETWEEN 10 AND 68	2-18053227-2
which time have greater round than that of any round in cale ?	SELECT t1.time from cale as t1 WHERE t1.round > ( SELECT MIN ( t1.round ) from cale as t1 )	2-18053227-2
find the event of the cale who has the largest number of cale .	SELECT t1.event from cale as t1 GROUP BY t1.event ORDER BY COUNT ( * ) DESC LIMIT 1	2-18053227-2
find the event of cale which have 10 but no 14 as event .	SELECT t1.event from cale as t1 WHERE t1.event = 10 EXCEPT SELECT t1.event from cale as t1 WHERE t1.event = 14	2-18053227-2
list the time which average round is above 10 .	SELECT t1.time from cale as t1 GROUP BY t1.time HAVING AVG ( t1.round ) >= 10	2-18053227-2
what are the opponent of the cale that have exactly 10 cale ?	SELECT t1.opponent from cale as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	2-18053227-2
show the location and the corresponding number of cale sorted by the number of location in ascending order .	SELECT t1.location , COUNT ( * ) from cale as t1 GROUP BY t1.location ORDER BY COUNT ( * )	2-18053227-2
select the average round of each cale 's event .	SELECT AVG ( t1.round ) , t1.event from cale as t1 GROUP BY t1.event	2-18053227-2
what is the maximum and mininum round {COLUMN} for all cale ?	SELECT MAX ( t1.round ) , MIN ( t1.round ) from cale as t1	2-18053227-2
