what are the percentage and candidate of all idaho sorted by decreasing vote ?	SELECT t1.percentage , t1.candidate from idaho as t1 ORDER BY t1.vote DESC	2-17867115-1
which seat has both idaho with less than 10 district and idaho with more than 23 district ?	SELECT t1.seat from idaho as t1 WHERE t1.district < 10 INTERSECT SELECT t1.seat from idaho as t1 WHERE t1.district > 23	2-17867115-1
return the smallest district for every candidate .	SELECT MIN ( t1.district ) , t1.candidate from idaho as t1 GROUP BY t1.candidate	2-17867115-1
list the party which average vote is above 10 .	SELECT t1.party from idaho as t1 GROUP BY t1.party HAVING AVG ( t1.vote ) >= 10	2-17867115-1
find the seat of idaho who have district of both 10 and 59 .	SELECT t1.seat from idaho as t1 WHERE t1.district = 10 INTERSECT SELECT t1.seat from idaho as t1 WHERE t1.district = 59	2-17867115-1
display the candidate , candidate , and party for each idaho .	SELECT t1.candidate , t1.candidate , t1.party from idaho as t1	2-17867115-1
what is the candidate and seat of the idaho with the top 5 smallest vote ?	SELECT t1.candidate , t1.seat from idaho as t1 ORDER BY t1.vote LIMIT 5	2-17867115-1
count the number of idaho in candidate 10 or 60 .	SELECT COUNT ( * ) from idaho as t1 WHERE t1.candidate = 10 OR t1.candidate = 60	2-17867115-1
what are the distinct party with vote between 10 and 33 ?	SELECT DISTINCT t1.party from idaho as t1 WHERE t1.vote BETWEEN 10 AND 33	2-17867115-1
find all party that have fewer than three in idaho .	SELECT t1.party from idaho as t1 GROUP BY t1.party HAVING COUNT ( * ) < 3	2-17867115-1
