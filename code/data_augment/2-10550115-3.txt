what are the nation for 1997 that have an total greater than the average .	SELECT t1.nation from 1997 as t1 WHERE t1.total > ( SELECT AVG ( t1.total ) from 1997 as t1	2-10550115-3
how many 1997 are there that have more than {VALUE},0 rank ?	SELECT COUNT ( * ) from 1997 as t1 GROUP BY t1.rank HAVING COUNT ( * ) > 10 	2-10550115-3
what is the nation of 1997 with the maximum total across all 1997 ?	SELECT t1.nation from 1997 as t1 WHERE t1.total = ( SELECT MAX ( t1.total ) from 1997 as t1 )	2-10550115-3
show the nation of 1997 who have at least 10 1997 .	SELECT t1.nation from 1997 as t1 GROUP BY t1.nation HAVING COUNT ( * ) >= 10	2-10550115-3
what is the maximum and mininum total {COLUMN} for all 1997 ?	SELECT MAX ( t1.total ) , MIN ( t1.total ) from 1997 as t1	2-10550115-3
how many different rank correspond to each nation ?	SELECT t1.nation , COUNT ( DISTINCT t1.rank ) from 1997 as t1 GROUP BY t1.nation	2-10550115-3
find the distinct nation of all 1997 that have a higher total than some 1997 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.nation from 1997 as t1 WHERE t1.total > ( SELECT MIN ( t1.nation ) from 1997 as t1 WHERE t1.nation = 10 )	2-10550115-3
find the nation of the 1997 that have just 10 1997 .	SELECT t1.nation from 1997 as t1 GROUP BY t1.nation HAVING COUNT ( * ) = 10	2-10550115-3
show the rank with fewer than 3 1997 .	SELECT t1.rank from 1997 as t1 GROUP BY t1.rank HAVING COUNT ( * ) < 3	2-10550115-3
show the rank of the 1997 that has the most 1997 .	SELECT t1.rank from 1997 as t1 GROUP BY t1.rank ORDER BY COUNT ( * ) DESC LIMIT 1	2-10550115-3
