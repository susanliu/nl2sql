return the maximum and minimum year across all hans .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from hans as t1	2-1233829-1
which class have an average laps over 10 ?	SELECT t1.class from hans as t1 GROUP BY t1.class HAVING AVG ( t1.laps ) >= 10	2-1233829-1
which pos . have greater laps than that of any laps in hans ?	SELECT t1.pos_ from hans as t1 WHERE t1.laps > ( SELECT MIN ( t1.laps ) from hans as t1 )	2-1233829-1
how many different pos . correspond to each pos . ?	SELECT t1.pos_ , COUNT ( DISTINCT t1.pos_ ) from hans as t1 GROUP BY t1.pos_	2-1233829-1
what is all the information on the hans with the largest number of class ?	SELECT * from hans as t1 ORDER BY t1.class DESC LIMIT 1	2-1233829-1
which team has both hans with less than 10 year and hans with more than 18 year ?	SELECT t1.team from hans as t1 WHERE t1.year < 10 INTERSECT SELECT t1.team from hans as t1 WHERE t1.year > 18	2-1233829-1
what is the class and team of the hans with the top 5 smallest year ?	SELECT t1.class , t1.team from hans as t1 ORDER BY t1.year LIMIT 5	2-1233829-1
what are the class and team of hans with 10 or more team ?	SELECT t1.class , t1.team from hans as t1 GROUP BY t1.team HAVING COUNT ( * ) >= 10	2-1233829-1
show team for all hans whose laps are greater than the average .	SELECT t1.team from hans as t1 WHERE t1.laps > ( SELECT AVG ( t1.laps ) from hans as t1	2-1233829-1
find the pos . of hans which are pos . 10 but not pos . 31 .	SELECT t1.pos_ from hans as t1 WHERE t1.pos_ = 10 EXCEPT SELECT t1.pos_ from hans as t1 WHERE t1.pos_ = 31	2-1233829-1
