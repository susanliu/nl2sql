how many distinct plural correspond to each plural ?	SELECT t1.plural , COUNT ( DISTINCT t1.plural ) from bengali as t1 GROUP BY t1.plural	2-1625862-3
what is the minimum subject in each plural ?	SELECT MIN ( t1.subject ) , t1.plural from bengali as t1 GROUP BY t1.plural	2-1625862-3
please show the different honor , ordered by the number of bengali that have each .	SELECT t1.honor from bengali as t1 GROUP BY t1.honor ORDER BY COUNT ( * ) ASC LIMIT 1	2-1625862-3
find the plural of bengali who have both 10 and 91 subject .	SELECT t1.plural from bengali as t1 WHERE t1.subject = 10 INTERSECT SELECT t1.plural from bengali as t1 WHERE t1.subject = 91	2-1625862-3
what are the distinct COLUMN_NAME,0} of bengali with subject higher than any bengali from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.plural from bengali as t1 WHERE t1.subject > ( SELECT MIN ( t1.plural ) from bengali as t1 WHERE t1.singular = 10 )	2-1625862-3
return the honor of the largest subject.	SELECT t1.honor from bengali as t1 ORDER BY t1.subject DESC LIMIT 1	2-1625862-3
find the plural of bengali which are in 10 honor but not in 48 honor .	SELECT t1.plural from bengali as t1 WHERE t1.honor = 10 EXCEPT SELECT t1.plural from bengali as t1 WHERE t1.honor = 48	2-1625862-3
find the plural who has exactly 10 bengali .	SELECT t1.plural from bengali as t1 GROUP BY t1.plural HAVING COUNT ( * ) = 10	2-1625862-3
please show the honor of the bengali that have at least 10 records .	SELECT t1.honor from bengali as t1 GROUP BY t1.honor HAVING COUNT ( * ) >= 10	2-1625862-3
find the distinct honor of bengali having subject between 10 and 23 .	SELECT DISTINCT t1.honor from bengali as t1 WHERE t1.subject BETWEEN 10 AND 23	2-1625862-3
