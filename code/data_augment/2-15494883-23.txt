find the location of list whose total passengers is more than the average total passengers of list .	SELECT t1.location from list as t1 WHERE t1.total_passengers > ( SELECT AVG ( t1.total_passengers ) from list as t1	2-15494883-23
what are the distinct COLUMN_NAME,0} of list with total passengers higher than any list from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.location from list as t1 WHERE t1.total_passengers > ( SELECT MIN ( t1.location ) from list as t1 WHERE t1.annual_change = 10 )	2-15494883-23
what are the distinct COLUMN_NAME,0} of every list that has a greater total passengers than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.annual_change from list as t1 WHERE t1.total_passengers > ( SELECT MIN ( t1.annual_change ) from list as t1 WHERE t1.annual_change = 10 )	2-15494883-23
what is the annual change of all list whose rank is higher than any list ?	SELECT t1.annual_change from list as t1 WHERE t1.rank > ( SELECT MIN ( t1.rank ) from list as t1 )	2-15494883-23
show all information on the list that has the largest number of capacity in use.	SELECT * from list as t1 ORDER BY t1.capacity_in_use DESC LIMIT 1	2-15494883-23
what are all the capacity in use and annual change?	SELECT t1.capacity_in_use , t1.annual_change from list as t1	2-15494883-23
what is the location of the list with the smallest total passengers ?	SELECT t1.location from list as t1 ORDER BY t1.total_passengers ASC LIMIT 1	2-15494883-23
which location has most number of list ?	SELECT t1.location from list as t1 GROUP BY t1.location ORDER BY COUNT ( * ) DESC LIMIT 1	2-15494883-23
what is the capacity in use and capacity in use of the list with maximum rank ?	SELECT t1.capacity_in_use , t1.capacity_in_use from list as t1 WHERE t1.rank = ( SELECT MAX ( t1.rank ) {FROM, 3} )	2-15494883-23
how many list are there that have more than {VALUE},0 capacity in use ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.capacity_in_use HAVING COUNT ( * ) > 10 	2-15494883-23
