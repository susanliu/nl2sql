return the maximum and minimum crowd across all 1946 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1946 as t1	2-10809368-12
give the t1.date with the fewest 1946 .	SELECT t1.date from 1946 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) LIMIT 1	2-10809368-12
find the venue of 1946 who have both 10 and 51 crowd .	SELECT t1.venue from 1946 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.venue from 1946 as t1 WHERE t1.crowd = 51	2-10809368-12
what are the home team score of 1946 whose away team score is not 10 ?	SELECT t1.home_team_score from 1946 as t1 WHERE t1.away_team_score ! = 10	2-10809368-12
what is the minimum crowd in each away team score ?	SELECT MIN ( t1.crowd ) , t1.away_team_score from 1946 as t1 GROUP BY t1.away_team_score	2-10809368-12
find the away team of 1946 which are venue 10 but not venue 101 .	SELECT t1.away_team from 1946 as t1 WHERE t1.venue = 10 EXCEPT SELECT t1.away_team from 1946 as t1 WHERE t1.venue = 101	2-10809368-12
find the home team score of 1946 who have both 10 and 7 crowd .	SELECT t1.home_team_score from 1946 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.home_team_score from 1946 as t1 WHERE t1.crowd = 7	2-10809368-12
which date has both 1946 with less than 10 crowd and 1946 with more than 31 crowd ?	SELECT t1.date from 1946 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.date from 1946 as t1 WHERE t1.crowd > 31	2-10809368-12
what is the away team score and home team score of the 1946 with maximum crowd ?	SELECT t1.away_team_score , t1.home_team_score from 1946 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10809368-12
find the home team that have 10 1946 .	SELECT t1.home_team from 1946 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) = 10	2-10809368-12
