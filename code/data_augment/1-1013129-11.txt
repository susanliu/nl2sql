select the average pick of each 1994 's college/junior/club team .	SELECT AVG ( t1.pick ) , t1.college/junior/club_team from 1994 as t1 GROUP BY t1.college/junior/club_team	1-1013129-11
what are the average pick of 1994 , grouped by nationality ?	SELECT AVG ( t1.pick ) , t1.nationality from 1994 as t1 GROUP BY t1.nationality	1-1013129-11
show all information on the 1994 that has the largest number of college/junior/club team.	SELECT * from 1994 as t1 ORDER BY t1.college/junior/club_team DESC LIMIT 1	1-1013129-11
which player has both 1994 with less than 10 pick and 1994 with more than 67 pick ?	SELECT t1.player from 1994 as t1 WHERE t1.pick < 10 INTERSECT SELECT t1.player from 1994 as t1 WHERE t1.pick > 67	1-1013129-11
how many 1994 are there in player 10 or 56 ?	SELECT COUNT ( * ) from 1994 as t1 WHERE t1.player = 10 OR t1.player = 56	1-1013129-11
which nhl team has both 1994 with less than 10 pick and 1994 with more than 42 pick ?	SELECT t1.nhl_team from 1994 as t1 WHERE t1.pick < 10 INTERSECT SELECT t1.nhl_team from 1994 as t1 WHERE t1.pick > 42	1-1013129-11
find the nationality of the 1994 who has the largest number of 1994 .	SELECT t1.nationality from 1994 as t1 GROUP BY t1.nationality ORDER BY COUNT ( * ) DESC LIMIT 1	1-1013129-11
list all position which have pick higher than the average .	SELECT t1.position from 1994 as t1 WHERE t1.pick > ( SELECT AVG ( t1.pick ) from 1994 as t1	1-1013129-11
Show everything on 1994	SELECT * FROM 1994	1-1013129-11
what is the nationality of all 1994 whose pick is higher than any 1994 ?	SELECT t1.nationality from 1994 as t1 WHERE t1.pick > ( SELECT MIN ( t1.pick ) from 1994 as t1 )	1-1013129-11
