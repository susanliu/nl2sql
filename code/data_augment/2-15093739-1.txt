list all information about 1968 .	SELECT * FROM 1968	2-15093739-1
what are the average attendance of 1968 , grouped by opponent ?	SELECT AVG ( t1.attendance ) , t1.opponent from 1968 as t1 GROUP BY t1.opponent	2-15093739-1
find the result and result of the 1968 whose week is lower than the average week of all 1968 .	SELECT t1.result , t1.result from 1968 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-15093739-1
which date has the least week ?	SELECT t1.date from 1968 as t1 ORDER BY t1.week ASC LIMIT 1	2-15093739-1
find the date and result of the 1968 whose attendance is lower than the average attendance of all 1968 .	SELECT t1.date , t1.result from 1968 as t1 WHERE t1.attendance < ( SELECT AVG ( t1.attendance ) {FROM, 3} )	2-15093739-1
what are the result of 1968 with week greater than the average of all 1968 ?	SELECT t1.result from 1968 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 1968 as t1	2-15093739-1
show the date of the 1968 that has the greatest number of 1968 .	SELECT t1.date from 1968 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	2-15093739-1
give the opponent that has the most 1968 .	SELECT t1.opponent from 1968 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-15093739-1
how many 1968 are there in opponent 10 or 47 ?	SELECT COUNT ( * ) from 1968 as t1 WHERE t1.opponent = 10 OR t1.opponent = 47	2-15093739-1
which opponent is the most frequent opponent?	SELECT t1.opponent from 1968 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-15093739-1
