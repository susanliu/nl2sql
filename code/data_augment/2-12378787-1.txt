which t1.previous_conference has the fewest hoosier ?	SELECT t1.previous_conference from hoosier as t1 GROUP BY t1.previous_conference ORDER BY COUNT ( * ) LIMIT 1	2-12378787-1
return each previous conference with the number of hoosier in ascending order of the number of previous conference .	SELECT t1.previous_conference , COUNT ( * ) from hoosier as t1 GROUP BY t1.previous_conference ORDER BY COUNT ( * )	2-12378787-1
find the previous conference and mascot of the hoosier with at least 10 mascot .	SELECT t1.previous_conference , t1.mascot from hoosier as t1 GROUP BY t1.mascot HAVING COUNT ( * ) >= 10	2-12378787-1
what is the mascot of the hoosier with the minimum enrollment 08-09 ?	SELECT t1.mascot from hoosier as t1 ORDER BY t1.enrollment_08-09 ASC LIMIT 1	2-12378787-1
sort the list of mascot and location of all hoosier in the descending order of year joined .	SELECT t1.mascot , t1.location from hoosier as t1 ORDER BY t1.year_joined DESC	2-12378787-1
what is the school of the hoosier with the largest year joined ?	SELECT t1.school from hoosier as t1 WHERE t1.year_joined = ( SELECT MAX ( t1.year_joined ) from hoosier as t1 )	2-12378787-1
what are the average year joined of hoosier for different # / county ?	SELECT AVG ( t1.year_joined ) , t1.# from hoosier as t1 GROUP BY t1.#	2-12378787-1
return the smallest year joined for every previous conference .	SELECT MIN ( t1.year_joined ) , t1.previous_conference from hoosier as t1 GROUP BY t1.previous_conference	2-12378787-1
show the ihsaa class of hoosier whose previous conference are not 10.	SELECT t1.ihsaa_class from hoosier as t1 WHERE t1.previous_conference ! = 10	2-12378787-1
count the number of hoosier in school 10 or 13 .	SELECT COUNT ( * ) from hoosier as t1 WHERE t1.school = 10 OR t1.school = 13	2-12378787-1
