what is the headquarters and industry of the list with maximum revenue ( millions . $ ) ?	SELECT t1.headquarters , t1.industry from list as t1 WHERE t1.revenue_ = ( SELECT MAX ( t1.revenue_ ) {FROM, 3} )	2-16256777-1
find the name and industry of the list whose rank fortune 500 is lower than the average rank fortune 500 of all list .	SELECT t1.name , t1.industry from list as t1 WHERE t1.rank_fortune_500 < ( SELECT AVG ( t1.rank_fortune_500 ) {FROM, 3} )	2-16256777-1
what are the distinct headquarters with rank between 10 and 98 ?	SELECT DISTINCT t1.headquarters from list as t1 WHERE t1.rank BETWEEN 10 AND 98	2-16256777-1
return the different industry of list , in ascending order of frequency .	SELECT t1.industry from list as t1 GROUP BY t1.industry ORDER BY COUNT ( * ) ASC LIMIT 1	2-16256777-1
what are the distinct COLUMN_NAME,0} of every list that has a greater revenue ( millions . $ ) than some list with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.headquarters from list as t1 WHERE t1.revenue_ > ( SELECT MIN ( t1.headquarters ) from list as t1 WHERE t1.name = 10 )	2-16256777-1
list name of list that have the number of list greater than 10 .	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) > 10	2-16256777-1
give the name that has the most list .	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) DESC LIMIT 1	2-16256777-1
what is the headquarters of highest rank ?	SELECT t1.headquarters from list as t1 ORDER BY t1.rank DESC LIMIT 1	2-16256777-1
what are the industry and industry of each list ?	SELECT t1.industry , t1.industry from list as t1	2-16256777-1
what is the name of the list with the largest employees ?	SELECT t1.name from list as t1 WHERE t1.employees = ( SELECT MAX ( t1.employees ) from list as t1 )	2-16256777-1
