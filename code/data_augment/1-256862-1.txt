find the number of volvo that have more than 10 finish .	SELECT COUNT ( * ) from volvo as t1 GROUP BY t1.finish HAVING COUNT ( * ) > 10 	1-256862-1
return the start of the volvo that has the fewest corresponding start .	SELECT t1.start from volvo as t1 GROUP BY t1.start ORDER BY COUNT ( * ) ASC LIMIT 1	1-256862-1
please show the edition of the volvo that have at least 10 records .	SELECT t1.edition from volvo as t1 GROUP BY t1.edition HAVING COUNT ( * ) >= 10	1-256862-1
how many different in-port races correspond to each edition ?	SELECT t1.edition , COUNT ( DISTINCT t1.in-port_races ) from volvo as t1 GROUP BY t1.edition	1-256862-1
find the start of volvo which are winning skipper 10 but not winning skipper 1 .	SELECT t1.start from volvo as t1 WHERE t1.winning_skipper = 10 EXCEPT SELECT t1.start from volvo as t1 WHERE t1.winning_skipper = 1	1-256862-1
list all information about volvo .	SELECT * FROM volvo	1-256862-1
what are the entries that have greater legs than any legs in volvo ?	SELECT t1.entries from volvo as t1 WHERE t1.legs > ( SELECT MIN ( t1.legs ) from volvo as t1 )	1-256862-1
what are the {in-port races of all the volvo , and the total legs by each ?	SELECT t1.in-port_races , SUM ( t1.legs ) from volvo as t1 GROUP BY t1.in-port_races	1-256862-1
how many volvo are there in entries 10 or 79 ?	SELECT COUNT ( * ) from volvo as t1 WHERE t1.entries = 10 OR t1.entries = 79	1-256862-1
return the smallest legs for every winning skipper .	SELECT MIN ( t1.legs ) , t1.winning_skipper from volvo as t1 GROUP BY t1.winning_skipper	1-256862-1
