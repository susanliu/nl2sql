please show the different opponent , ordered by the number of 1981 that have each .	SELECT t1.opponent from 1981 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-10361889-2
what are the date and result of all 1981 sorted by decreasing week ?	SELECT t1.date , t1.result from 1981 as t1 ORDER BY t1.week DESC	2-10361889-2
what is the opponent of the 1981 who has the highest number of 1981 ?	SELECT t1.opponent from 1981 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-10361889-2
what is the result and opponent of the 1981 with maximum week ?	SELECT t1.result , t1.opponent from 1981 as t1 WHERE t1.week = ( SELECT MAX ( t1.week ) {FROM, 3} )	2-10361889-2
find the opponent of 1981 who have both 10 and 30 week .	SELECT t1.opponent from 1981 as t1 WHERE t1.week = 10 INTERSECT SELECT t1.opponent from 1981 as t1 WHERE t1.week = 30	2-10361889-2
list the opponent of the 1981 whose opponent is not 10 .	SELECT t1.opponent from 1981 as t1 WHERE t1.opponent ! = 10	2-10361889-2
how many 1981 are there that have more than {VALUE},0 opponent ?	SELECT COUNT ( * ) from 1981 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	2-10361889-2
which t1.date has least number of 1981 ?	SELECT t1.date from 1981 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) LIMIT 1	2-10361889-2
which result have an average week over 10 ?	SELECT t1.result from 1981 as t1 GROUP BY t1.result HAVING AVG ( t1.week ) >= 10	2-10361889-2
what are the date of all 1981 with date that is 10 ?	SELECT t1.date from 1981 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	2-10361889-2
