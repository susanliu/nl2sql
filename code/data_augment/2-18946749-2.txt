what is the maximum and mininum rank {COLUMN} for all list ?	SELECT MAX ( t1.rank ) , MIN ( t1.rank ) from list as t1	2-18946749-2
what is the peak and peak of the list with maximum prominence ( m ) ?	SELECT t1.peak , t1.peak from list as t1 WHERE t1.prominence_ = ( SELECT MAX ( t1.prominence_ ) {FROM, 3} )	2-18946749-2
find the number of list that have more than 10 island .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.island HAVING COUNT ( * ) > 10 	2-18946749-2
show all information on the list that has the largest number of island.	SELECT * from list as t1 ORDER BY t1.island DESC LIMIT 1	2-18946749-2
what are the island of all list with country that is 10 ?	SELECT t1.island from list as t1 GROUP BY t1.country HAVING COUNT ( * ) = 10	2-18946749-2
how many list are there in island 10 or 33 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.island = 10 OR t1.island = 33	2-18946749-2
find the country and country of the list with at least 10 peak .	SELECT t1.country , t1.country from list as t1 GROUP BY t1.peak HAVING COUNT ( * ) >= 10	2-18946749-2
return the smallest elevation ( m ) for every island .	SELECT MIN ( t1.elevation_ ) , t1.island from list as t1 GROUP BY t1.island	2-18946749-2
show all information on the list that has the largest number of country.	SELECT * from list as t1 ORDER BY t1.country DESC LIMIT 1	2-18946749-2
return the island of the list with the fewest rank .	SELECT t1.island from list as t1 ORDER BY t1.rank ASC LIMIT 1	2-18946749-2
