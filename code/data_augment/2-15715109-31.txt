what is the birthdate and name of every list that has a weight ( kg ) lower than average ?	SELECT t1.birthdate , t1.name from list as t1 WHERE t1.weight_ < ( SELECT AVG ( t1.weight_ ) {FROM, 3} )	2-15715109-31
count the number of list in position 10 or 46 .	SELECT COUNT ( * ) from list as t1 WHERE t1.position = 10 OR t1.position = 46	2-15715109-31
which t1.birthdate has the fewest list ?	SELECT t1.birthdate from list as t1 GROUP BY t1.birthdate ORDER BY COUNT ( * ) LIMIT 1	2-15715109-31
find the birthdate of the list with the highest weight ( kg ) .	SELECT t1.birthdate from list as t1 ORDER BY t1.weight_ DESC LIMIT 1	2-15715109-31
what is all the information on the list with the largest number of birthplace ?	SELECT * from list as t1 ORDER BY t1.birthplace DESC LIMIT 1	2-15715109-31
find the distinct name of list having weight ( kg ) between 10 and 66 .	SELECT DISTINCT t1.name from list as t1 WHERE t1.weight_ BETWEEN 10 AND 66	2-15715109-31
which t1.birthdate has the smallest amount of list?	SELECT t1.birthdate from list as t1 GROUP BY t1.birthdate ORDER BY COUNT ( * ) LIMIT 1	2-15715109-31
return the maximum and minimum jersey # across all list .	SELECT MAX ( t1.jersey_ ) , MIN ( t1.jersey_ ) from list as t1	2-15715109-31
show the name of the list that has the most list .	SELECT t1.name from list as t1 GROUP BY t1.name ORDER BY COUNT ( * ) DESC LIMIT 1	2-15715109-31
what are the birthdate and birthdate of the {COLUMN} who have jersey # above five or height ( cm ) below ten ?	SELECT t1.birthdate , t1.birthdate from list as t1 WHERE t1.jersey_ > 5 OR t1.height_ < 10	2-15715109-31
