what are the constellation of teen whose arrival date is not 10 ?	SELECT t1.constellation from teen as t1 WHERE t1.arrival_date ! = 10	2-1820752-1
return the hd designation and spectral type of teen with the five lowest signal power ( kw ) .	SELECT t1.hd_designation , t1.spectral_type from teen as t1 ORDER BY t1.signal_power_ LIMIT 5	2-1820752-1
what are the arrival date and hd designation of all teen sorted by decreasing distance ( ly ) ?	SELECT t1.arrival_date , t1.hd_designation from teen as t1 ORDER BY t1.distance_ DESC	2-1820752-1
show the constellation with fewer than 3 teen .	SELECT t1.constellation from teen as t1 GROUP BY t1.constellation HAVING COUNT ( * ) < 3	2-1820752-1
which spectral type have greater distance ( ly ) than that of any distance ( ly ) in teen ?	SELECT t1.spectral_type from teen as t1 WHERE t1.distance_ > ( SELECT MIN ( t1.distance_ ) from teen as t1 )	2-1820752-1
what are the distinct COLUMN_NAME,0} of every teen that has a greater signal power ( kw ) than some teen with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.hd_designation from teen as t1 WHERE t1.signal_power_ > ( SELECT MIN ( t1.hd_designation ) from teen as t1 WHERE t1.arrival_date = 10 )	2-1820752-1
please show the arrival date of the teen that have at least 10 records .	SELECT t1.arrival_date from teen as t1 GROUP BY t1.arrival_date HAVING COUNT ( * ) >= 10	2-1820752-1
what are the distinct spectral type with signal power ( kw ) between 10 and 44 ?	SELECT DISTINCT t1.spectral_type from teen as t1 WHERE t1.signal_power_ BETWEEN 10 AND 44	2-1820752-1
what is the hd designation of the teen with the minimum signal power ( kw ) ?	SELECT t1.hd_designation from teen as t1 ORDER BY t1.signal_power_ ASC LIMIT 1	2-1820752-1
show the hd designation and their total distance ( ly ) of teen .	SELECT t1.hd_designation , SUM ( t1.distance_ ) from teen as t1 GROUP BY t1.hd_designation	2-1820752-1
