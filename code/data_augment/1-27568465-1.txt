which club have greater points difference than that of any points difference in none ?	SELECT t1.club from none as t1 WHERE t1.points_difference > ( SELECT MIN ( t1.points_difference ) from none as t1 )	1-27568465-1
how many none are there in club 10 or 20 ?	SELECT COUNT ( * ) from none as t1 WHERE t1.club = 10 OR t1.club = 20	1-27568465-1
what is the club of none with the maximum won across all none ?	SELECT t1.club from none as t1 WHERE t1.won = ( SELECT MAX ( t1.won ) from none as t1 )	1-27568465-1
which club have less than 3 in none ?	SELECT t1.club from none as t1 GROUP BY t1.club HAVING COUNT ( * ) < 3	1-27568465-1
give the club that has the most none .	SELECT t1.club from none as t1 GROUP BY t1.club ORDER BY COUNT ( * ) DESC LIMIT 1	1-27568465-1
find the club of the none with the largest drawn .	SELECT t1.club from none as t1 ORDER BY t1.drawn DESC LIMIT 1	1-27568465-1
how many none are there that have more than {VALUE},0 club ?	SELECT COUNT ( * ) from none as t1 GROUP BY t1.club HAVING COUNT ( * ) > 10 	1-27568465-1
return the club of the none that has the fewest corresponding club .	SELECT t1.club from none as t1 GROUP BY t1.club ORDER BY COUNT ( * ) ASC LIMIT 1	1-27568465-1
what is the club of the none with the largest points against ?	SELECT t1.club from none as t1 WHERE t1.points_against = ( SELECT MAX ( t1.points_against ) from none as t1 )	1-27568465-1
show the club and the total points for of none .	SELECT t1.club , SUM ( t1.points_for ) from none as t1 GROUP BY t1.club	1-27568465-1
