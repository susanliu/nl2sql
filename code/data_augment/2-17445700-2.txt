what are the average round of tedd for different event ?	SELECT AVG ( t1.round ) , t1.event from tedd as t1 GROUP BY t1.event	2-17445700-2
what is all the information on the tedd with the largest number of event ?	SELECT * from tedd as t1 ORDER BY t1.event DESC LIMIT 1	2-17445700-2
what are the event and res . ?	SELECT t1.event , t1.res_ from tedd as t1	2-17445700-2
find the time of tedd which are event 10 but not event 76 .	SELECT t1.time from tedd as t1 WHERE t1.event = 10 EXCEPT SELECT t1.time from tedd as t1 WHERE t1.event = 76	2-17445700-2
what are the record of all tedd with method that is 10 ?	SELECT t1.record from tedd as t1 GROUP BY t1.method HAVING COUNT ( * ) = 10	2-17445700-2
what are the opponent and res . of the {COLUMN} who have round above five or round below ten ?	SELECT t1.opponent , t1.res_ from tedd as t1 WHERE t1.round > 5 OR t1.round < 10	2-17445700-2
return the record and time of tedd with the five lowest round .	SELECT t1.record , t1.time from tedd as t1 ORDER BY t1.round LIMIT 5	2-17445700-2
what are the opponent and time of each tedd ?	SELECT t1.opponent , t1.time from tedd as t1	2-17445700-2
what is the minimum round in each method ?	SELECT MIN ( t1.round ) , t1.method from tedd as t1 GROUP BY t1.method	2-17445700-2
find the record of the tedd with the highest round .	SELECT t1.record from tedd as t1 ORDER BY t1.round DESC LIMIT 1	2-17445700-2
