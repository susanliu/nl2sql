count the number of jaguar in event 10 or 96 .	SELECT COUNT ( * ) from jaguar as t1 WHERE t1.event = 10 OR t1.event = 96	2-1498073-4
sort the list of auction house and event of all jaguar in the descending order of gbp price .	SELECT t1.auction_house , t1.event from jaguar as t1 ORDER BY t1.gbp_price DESC	2-1498073-4
what are the event of the jaguar with event other than 10 ?	SELECT t1.event from jaguar as t1 WHERE t1.event ! = 10	2-1498073-4
show all information on the jaguar that has the largest number of auction house.	SELECT * from jaguar as t1 ORDER BY t1.auction_house DESC LIMIT 1	2-1498073-4
find the distinct auction house of jaguar having gbp price between 10 and 89 .	SELECT DISTINCT t1.auction_house from jaguar as t1 WHERE t1.gbp_price BETWEEN 10 AND 89	2-1498073-4
which event is the most frequent event?	SELECT t1.event from jaguar as t1 GROUP BY t1.event ORDER BY COUNT ( * ) DESC LIMIT 1	2-1498073-4
what are the auction house of all jaguar that have 10 or more jaguar ?	SELECT t1.auction_house from jaguar as t1 GROUP BY t1.auction_house HAVING COUNT ( * ) >= 10	2-1498073-4
which t1.event has the fewest jaguar ?	SELECT t1.event from jaguar as t1 GROUP BY t1.event ORDER BY COUNT ( * ) LIMIT 1	2-1498073-4
what are total price for each event ?	SELECT t1.event , SUM ( t1.price ) from jaguar as t1 GROUP BY t1.event	2-1498073-4
find the auction house of the jaguar which have auction house 10 but not 91 .	SELECT t1.auction_house from jaguar as t1 WHERE t1.auction_house = 10 EXCEPT SELECT t1.auction_house from jaguar as t1 WHERE t1.auction_house = 91	2-1498073-4
