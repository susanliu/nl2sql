how many list correspond to each name? show the result in ascending order.	SELECT t1.name , COUNT ( * ) from list as t1 GROUP BY t1.name ORDER BY COUNT ( * )	2-16799784-16
find the number of list whose name contain the word 10 .	SELECT COUNT ( * ) from list as t1 WHERE t1.name LIKE 10	2-16799784-16
which latitude is the most frequent latitude?	SELECT t1.latitude from list as t1 GROUP BY t1.latitude ORDER BY COUNT ( * ) DESC LIMIT 1	2-16799784-16
what is all the information on the list with the largest number of name ?	SELECT * from list as t1 ORDER BY t1.name DESC LIMIT 1	2-16799784-16
find the number of list that have more than 10 longitude .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.longitude HAVING COUNT ( * ) > 10 	2-16799784-16
what are the latitude of list , sorted by their frequency?	SELECT t1.latitude from list as t1 GROUP BY t1.latitude ORDER BY COUNT ( * ) ASC LIMIT 1	2-16799784-16
which latitude have greater year named than that of any year named in list ?	SELECT t1.latitude from list as t1 WHERE t1.year_named > ( SELECT MIN ( t1.year_named ) from list as t1 )	2-16799784-16
what is the longitude of list with the maximum diameter ( km ) across all list ?	SELECT t1.longitude from list as t1 WHERE t1.diameter_ = ( SELECT MAX ( t1.diameter_ ) from list as t1 )	2-16799784-16
find the latitude of the list with the highest diameter ( km ).	SELECT t1.latitude from list as t1 WHERE t1.diameter_ = ( SELECT MAX ( t1.diameter_ ) from list as t1 )	2-16799784-16
show all information on the list that has the largest number of name.	SELECT * from list as t1 ORDER BY t1.name DESC LIMIT 1	2-16799784-16
