show the league , league , and venue of all the erie .	SELECT t1.league , t1.league , t1.venue from erie as t1	2-132482-2
find the distinct venue of all erie that have a higher established than some erie with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.venue from erie as t1 WHERE t1.established > ( SELECT MIN ( t1.venue ) from erie as t1 WHERE t1.club = 10 )	2-132482-2
find the league of erie who have both 10 and 62 championships .	SELECT t1.league from erie as t1 WHERE t1.championships = 10 INTERSECT SELECT t1.league from erie as t1 WHERE t1.championships = 62	2-132482-2
what is the sport of the erie with the minimum established ?	SELECT t1.sport from erie as t1 ORDER BY t1.established ASC LIMIT 1	2-132482-2
what is the club and venue of every erie that has a established lower than average ?	SELECT t1.club , t1.venue from erie as t1 WHERE t1.established < ( SELECT AVG ( t1.established ) {FROM, 3} )	2-132482-2
list the club which average established is above 10 .	SELECT t1.club from erie as t1 GROUP BY t1.club HAVING AVG ( t1.established ) >= 10	2-132482-2
return the sport of the largest established.	SELECT t1.sport from erie as t1 ORDER BY t1.established DESC LIMIT 1	2-132482-2
what is all the information on the erie with the largest number of club ?	SELECT * from erie as t1 ORDER BY t1.club DESC LIMIT 1	2-132482-2
select the average championships of each erie 's sport .	SELECT AVG ( t1.championships ) , t1.sport from erie as t1 GROUP BY t1.sport	2-132482-2
what is the minimum championships in each sport ?	SELECT MIN ( t1.championships ) , t1.sport from erie as t1 GROUP BY t1.sport	2-132482-2
