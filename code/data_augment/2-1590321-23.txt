how many distinct name correspond to each years ?	SELECT t1.years , COUNT ( DISTINCT t1.name ) from list as t1 GROUP BY t1.years	2-1590321-23
what is the count of list with more than 10 name ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.name HAVING COUNT ( * ) > 10 	2-1590321-23
show the name and the corresponding number of list sorted by the number of name in ascending order .	SELECT t1.name , COUNT ( * ) from list as t1 GROUP BY t1.name ORDER BY COUNT ( * )	2-1590321-23
what are the years that have greater rank than any rank in list ?	SELECT t1.years from list as t1 WHERE t1.rank > ( SELECT MIN ( t1.rank ) from list as t1 )	2-1590321-23
show name for all list whose goals are greater than the average .	SELECT t1.name from list as t1 WHERE t1.goals > ( SELECT AVG ( t1.goals ) from list as t1	2-1590321-23
what are the years of all list with years that is 10 ?	SELECT t1.years from list as t1 GROUP BY t1.years HAVING COUNT ( * ) = 10	2-1590321-23
find the years of the list who has the largest number of list .	SELECT t1.years from list as t1 GROUP BY t1.years ORDER BY COUNT ( * ) DESC LIMIT 1	2-1590321-23
find the name of the list with the highest goals .	SELECT t1.name from list as t1 ORDER BY t1.goals DESC LIMIT 1	2-1590321-23
what are the name of the list that have exactly 10 list ?	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) = 10	2-1590321-23
find the years of list which are years 10 but not years 16 .	SELECT t1.years from list as t1 WHERE t1.years = 10 EXCEPT SELECT t1.years from list as t1 WHERE t1.years = 16	2-1590321-23
