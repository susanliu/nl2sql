what are the final episode of list with final episode count greater than the average of all list ?	SELECT t1.final_episode from list as t1 WHERE t1.final_episode_count > ( SELECT AVG ( t1.final_episode_count ) from list as t1	1-11210576-3
list first episode and final episode who have final episode count greater than 5 or final episode count shorter than 10 .	SELECT t1.first_episode , t1.final_episode from list as t1 WHERE t1.final_episode_count > 5 OR t1.final_episode_count < 10	1-11210576-3
please show the different actor , ordered by the number of list that have each .	SELECT t1.actor from list as t1 GROUP BY t1.actor ORDER BY COUNT ( * ) ASC LIMIT 1	1-11210576-3
how many different first episode correspond to each first episode ?	SELECT t1.first_episode , COUNT ( DISTINCT t1.first_episode ) from list as t1 GROUP BY t1.first_episode	1-11210576-3
return the character of the largest final episode count.	SELECT t1.character from list as t1 ORDER BY t1.final_episode_count DESC LIMIT 1	1-11210576-3
count the number of list in position 10 or 63 .	SELECT COUNT ( * ) from list as t1 WHERE t1.position = 10 OR t1.position = 63	1-11210576-3
find all final episode that have fewer than three in list .	SELECT t1.final_episode from list as t1 GROUP BY t1.final_episode HAVING COUNT ( * ) < 3	1-11210576-3
what are the distinct actor with final episode count between 10 and 100 ?	SELECT DISTINCT t1.actor from list as t1 WHERE t1.final_episode_count BETWEEN 10 AND 100	1-11210576-3
what are the first episode more than 10 list have ?	SELECT t1.first_episode from list as t1 GROUP BY t1.first_episode HAVING COUNT ( * ) > 10	1-11210576-3
show the position of list who have at least 10 list .	SELECT t1.position from list as t1 GROUP BY t1.position HAVING COUNT ( * ) >= 10	1-11210576-3
