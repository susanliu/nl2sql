which owner has both media with less than 10 day power ( w ) and media with more than 63 day power ( w ) ?	SELECT t1.owner from media as t1 WHERE t1.day_power_ < 10 INTERSECT SELECT t1.owner from media as t1 WHERE t1.day_power_ > 63	2-10742875-2
which t1.format has the fewest media ?	SELECT t1.format from media as t1 GROUP BY t1.format ORDER BY COUNT ( * ) LIMIT 1	2-10742875-2
which city have an average freq over 10 ?	SELECT t1.city from media as t1 GROUP BY t1.city HAVING AVG ( t1.freq ) >= 10	2-10742875-2
please show the call of the media that have at least 10 records .	SELECT t1.call from media as t1 GROUP BY t1.call HAVING COUNT ( * ) >= 10	2-10742875-2
which call have greater day power ( w ) than that of any day power ( w ) in media ?	SELECT t1.call from media as t1 WHERE t1.day_power_ > ( SELECT MIN ( t1.day_power_ ) from media as t1 )	2-10742875-2
what are the format and owner ?	SELECT t1.format , t1.owner from media as t1	2-10742875-2
show the city and the number of unique call containing each city .	SELECT t1.city , COUNT ( DISTINCT t1.call ) from media as t1 GROUP BY t1.city	2-10742875-2
find the number of media whose format contain the word 10 .	SELECT COUNT ( * ) from media as t1 WHERE t1.format LIKE 10	2-10742875-2
find the format and format of the media whose day power ( w ) is lower than the average day power ( w ) of all media .	SELECT t1.format , t1.format from media as t1 WHERE t1.day_power_ < ( SELECT AVG ( t1.day_power_ ) {FROM, 3} )	2-10742875-2
find the format of media which are in 10 city but not in 38 city .	SELECT t1.format from media as t1 WHERE t1.city = 10 EXCEPT SELECT t1.format from media as t1 WHERE t1.city = 38	2-10742875-2
