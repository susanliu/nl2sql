show the rider of the 2008 that has the most 2008 .	SELECT t1.rider from 2008 as t1 GROUP BY t1.rider ORDER BY COUNT ( * ) DESC LIMIT 1	2-18543211-1
how many 2008 are there that have more than {VALUE},0 bike ?	SELECT COUNT ( * ) from 2008 as t1 GROUP BY t1.bike HAVING COUNT ( * ) > 10 	2-18543211-1
list rider and bike who have laps greater than 5 or grid shorter than 10 .	SELECT t1.rider , t1.bike from 2008 as t1 WHERE t1.laps > 5 OR t1.grid < 10	2-18543211-1
find the time and rider of the 2008 with at least 10 bike .	SELECT t1.time , t1.rider from 2008 as t1 GROUP BY t1.bike HAVING COUNT ( * ) >= 10	2-18543211-1
what are the time that have greater grid than any grid in 2008 ?	SELECT t1.time from 2008 as t1 WHERE t1.grid > ( SELECT MIN ( t1.grid ) from 2008 as t1 )	2-18543211-1
what is the minimum grid in each time ?	SELECT MIN ( t1.grid ) , t1.time from 2008 as t1 GROUP BY t1.time	2-18543211-1
which time has both 2008 with less than 10 grid and 2008 with more than 74 grid ?	SELECT t1.time from 2008 as t1 WHERE t1.grid < 10 INTERSECT SELECT t1.time from 2008 as t1 WHERE t1.grid > 74	2-18543211-1
find the bike of 2008 which are time 10 but not time 28 .	SELECT t1.bike from 2008 as t1 WHERE t1.time = 10 EXCEPT SELECT t1.bike from 2008 as t1 WHERE t1.time = 28	2-18543211-1
which t1.bike has the fewest 2008 ?	SELECT t1.bike from 2008 as t1 GROUP BY t1.bike ORDER BY COUNT ( * ) LIMIT 1	2-18543211-1
how many distinct rider correspond to each time ?	SELECT t1.time , COUNT ( DISTINCT t1.rider ) from 2008 as t1 GROUP BY t1.time	2-18543211-1
