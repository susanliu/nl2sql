how many distinct name origin correspond to each latitude ?	SELECT t1.latitude , COUNT ( DISTINCT t1.name_origin ) from list as t1 GROUP BY t1.latitude	1-16799784-4
find the distinct name of list having year named between 10 and 92 .	SELECT DISTINCT t1.name from list as t1 WHERE t1.year_named BETWEEN 10 AND 92	1-16799784-4
return the longitude of the largest year named.	SELECT t1.longitude from list as t1 ORDER BY t1.year_named DESC LIMIT 1	1-16799784-4
find the name origin of the list with the highest year named.	SELECT t1.name_origin from list as t1 WHERE t1.year_named = ( SELECT MAX ( t1.year_named ) from list as t1 )	1-16799784-4
find the name origin of list which are name 10 but not name 13 .	SELECT t1.name_origin from list as t1 WHERE t1.name = 10 EXCEPT SELECT t1.name_origin from list as t1 WHERE t1.name = 13	1-16799784-4
what is the name and name for the list with the rank 5 smallest year named ?	SELECT t1.name , t1.name from list as t1 ORDER BY t1.year_named LIMIT 5	1-16799784-4
Return all columns in list .	SELECT * FROM list	1-16799784-4
find the name of list whose year named is more than the average year named of list .	SELECT t1.name from list as t1 WHERE t1.year_named > ( SELECT AVG ( t1.year_named ) from list as t1	1-16799784-4
return the name origin of the list with the fewest year named .	SELECT t1.name_origin from list as t1 ORDER BY t1.year_named ASC LIMIT 1	1-16799784-4
please show the longitude of the list that have at least 10 records .	SELECT t1.longitude from list as t1 GROUP BY t1.longitude HAVING COUNT ( * ) >= 10	1-16799784-4
