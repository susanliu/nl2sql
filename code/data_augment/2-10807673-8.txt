what is the away team score of the 1941 with the minimum crowd ?	SELECT t1.away_team_score from 1941 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10807673-8
what are the venue and venue of the {COLUMN} who have crowd above five or crowd below ten ?	SELECT t1.venue , t1.venue from 1941 as t1 WHERE t1.crowd > 5 OR t1.crowd < 10	2-10807673-8
show the away team score and away team with at least 10 venue .	SELECT t1.away_team_score , t1.away_team from 1941 as t1 GROUP BY t1.venue HAVING COUNT ( * ) >= 10	2-10807673-8
please show the away team of the 1941 that have at least 10 records .	SELECT t1.away_team from 1941 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) >= 10	2-10807673-8
return the away team of 1941 for which the away team is not 10 ?	SELECT t1.away_team from 1941 as t1 WHERE t1.away_team ! = 10	2-10807673-8
what are the distinct date with crowd between 10 and 37 ?	SELECT DISTINCT t1.date from 1941 as t1 WHERE t1.crowd BETWEEN 10 AND 37	2-10807673-8
which date have less than 3 in 1941 ?	SELECT t1.date from 1941 as t1 GROUP BY t1.date HAVING COUNT ( * ) < 3	2-10807673-8
what are the average crowd of 1941 for different venue ?	SELECT AVG ( t1.crowd ) , t1.venue from 1941 as t1 GROUP BY t1.venue	2-10807673-8
show the home team and the total crowd of 1941 .	SELECT t1.home_team , SUM ( t1.crowd ) from 1941 as t1 GROUP BY t1.home_team	2-10807673-8
what are the date of all 1941 with away team that is 10 ?	SELECT t1.date from 1941 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) = 10	2-10807673-8
