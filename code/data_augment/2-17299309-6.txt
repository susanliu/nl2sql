what is the date and score for the peruvian with the rank 5 smallest season ?	SELECT t1.date , t1.score from peruvian as t1 ORDER BY t1.season LIMIT 5	2-17299309-6
what is the competition round and date of every peruvian that has a season lower than average ?	SELECT t1.competition_round , t1.date from peruvian as t1 WHERE t1.season < ( SELECT AVG ( t1.season ) {FROM, 3} )	2-17299309-6
show the away team and the corresponding number of peruvian sorted by the number of away team in ascending order .	SELECT t1.away_team , COUNT ( * ) from peruvian as t1 GROUP BY t1.away_team ORDER BY COUNT ( * )	2-17299309-6
please list the date and home team of peruvian in descending order of season .	SELECT t1.date , t1.home_team from peruvian as t1 ORDER BY t1.season DESC	2-17299309-6
what are the distinct COLUMN_NAME,0} of peruvian with season higher than any peruvian from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.venue from peruvian as t1 WHERE t1.season > ( SELECT MIN ( t1.venue ) from peruvian as t1 WHERE t1.score = 10 )	2-17299309-6
find the distinct COLUMN_NAME,0} of all peruvian that have season higher than some peruvian from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.date from peruvian as t1 WHERE t1.season > ( SELECT MIN ( t1.date ) from peruvian as t1 WHERE t1.competition_round = 10 )	2-17299309-6
what are the home team , score , and competition round for each peruvian ?	SELECT t1.home_team , t1.score , t1.competition_round from peruvian as t1	2-17299309-6
what are the competition round of all peruvian with score that is 10 ?	SELECT t1.competition_round from peruvian as t1 GROUP BY t1.score HAVING COUNT ( * ) = 10	2-17299309-6
which home team have an average season over 10 ?	SELECT t1.home_team from peruvian as t1 GROUP BY t1.home_team HAVING AVG ( t1.season ) >= 10	2-17299309-6
sort the list of score and away team of all peruvian in the descending order of season .	SELECT t1.score , t1.away_team from peruvian as t1 ORDER BY t1.season DESC	2-17299309-6
