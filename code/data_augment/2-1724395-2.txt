what are the event of pat , sorted by their frequency?	SELECT t1.event from pat as t1 GROUP BY t1.event ORDER BY COUNT ( * ) ASC LIMIT 1	2-1724395-2
how many pat are there that have more than {VALUE},0 opponent ?	SELECT COUNT ( * ) from pat as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	2-1724395-2
what are the opponent , method , and location of each pat ?	SELECT t1.opponent , t1.method , t1.location from pat as t1	2-1724395-2
show all record and corresponding number of pat sorted by the count .	SELECT t1.record , COUNT ( * ) from pat as t1 GROUP BY t1.record ORDER BY COUNT ( * )	2-1724395-2
find the opponent and record of the pat with at least 10 opponent .	SELECT t1.opponent , t1.record from pat as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-1724395-2
what are the opponent of all pat with location that is 10 ?	SELECT t1.opponent from pat as t1 GROUP BY t1.location HAVING COUNT ( * ) = 10	2-1724395-2
what are the average round of pat for different event ?	SELECT AVG ( t1.round ) , t1.event from pat as t1 GROUP BY t1.event	2-1724395-2
what is all the information on the pat with the largest number of res . ?	SELECT * from pat as t1 ORDER BY t1.res_ DESC LIMIT 1	2-1724395-2
what is the record of the pat with the smallest round ?	SELECT t1.record from pat as t1 ORDER BY t1.round ASC LIMIT 1	2-1724395-2
give the time that has the most pat .	SELECT t1.time from pat as t1 GROUP BY t1.time ORDER BY COUNT ( * ) DESC LIMIT 1	2-1724395-2
