what are the date of all 1936 with home team that is 10 ?	SELECT t1.date from 1936 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) = 10	2-10790804-2
what are the home team of all 1936 with venue that is 10 ?	SELECT t1.home_team from 1936 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10790804-2
show the away team score with fewer than 3 1936 .	SELECT t1.away_team_score from 1936 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) < 3	2-10790804-2
what are the home team score of 1936 whose date are not 10 ?	SELECT t1.home_team_score from 1936 as t1 WHERE t1.date ! = 10	2-10790804-2
please list the venue and venue of 1936 in descending order of crowd .	SELECT t1.venue , t1.venue from 1936 as t1 ORDER BY t1.crowd DESC	2-10790804-2
return the away team of the 1936 with the fewest crowd .	SELECT t1.away_team from 1936 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10790804-2
what are the maximum and minimum crowd across all 1936 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1936 as t1	2-10790804-2
what is the away team score and home team score of the 1936 with maximum crowd ?	SELECT t1.away_team_score , t1.home_team_score from 1936 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10790804-2
what is the away team and venue of the 1936 with maximum crowd ?	SELECT t1.away_team , t1.venue from 1936 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10790804-2
which home team score has both 1936 with less than 10 crowd and 1936 with more than 59 crowd ?	SELECT t1.home_team_score from 1936 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team_score from 1936 as t1 WHERE t1.crowd > 59	2-10790804-2
