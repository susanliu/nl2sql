what is the gross of 1990 with the maximum rank across all 1990 ?	SELECT t1.gross from 1990 as t1 WHERE t1.rank = ( SELECT MAX ( t1.rank ) from 1990 as t1 )	2-171259-1
return the title of the 1990 with the fewest rank .	SELECT t1.title from 1990 as t1 ORDER BY t1.rank ASC LIMIT 1	2-171259-1
what are the studio of all 1990 that have 10 or more 1990 ?	SELECT t1.studio from 1990 as t1 GROUP BY t1.studio HAVING COUNT ( * ) >= 10	2-171259-1
list the title and title of all 1990 sorted by rank in descending order .	SELECT t1.title , t1.title from 1990 as t1 ORDER BY t1.rank DESC	2-171259-1
what are the studio of all 1990 with studio that is 10 ?	SELECT t1.studio from 1990 as t1 GROUP BY t1.studio HAVING COUNT ( * ) = 10	2-171259-1
find the director of the 1990 with the highest rank .	SELECT t1.director from 1990 as t1 ORDER BY t1.rank DESC LIMIT 1	2-171259-1
how many 1990 have director that contains 10 ?	SELECT COUNT ( * ) from 1990 as t1 WHERE t1.director LIKE 10	2-171259-1
what are the title and director of the {COLUMN} who have rank above five or rank below ten ?	SELECT t1.title , t1.director from 1990 as t1 WHERE t1.rank > 5 OR t1.rank < 10	2-171259-1
list studio and director who have rank greater than 5 or rank shorter than 10 .	SELECT t1.studio , t1.director from 1990 as t1 WHERE t1.rank > 5 OR t1.rank < 10	2-171259-1
find the title who has exactly 10 1990 .	SELECT t1.title from 1990 as t1 GROUP BY t1.title HAVING COUNT ( * ) = 10	2-171259-1
