show the winnings with fewer than 3 sterling .	SELECT t1.winnings from sterling as t1 GROUP BY t1.winnings HAVING COUNT ( * ) < 3	2-1708014-2
what is the winnings of the sterling who has the highest number of sterling ?	SELECT t1.winnings from sterling as t1 GROUP BY t1.winnings ORDER BY COUNT ( * ) DESC LIMIT 1	2-1708014-2
what is all the information on the sterling with the largest number of position ?	SELECT * from sterling as t1 ORDER BY t1.position DESC LIMIT 1	2-1708014-2
return the smallest top 10 for every position .	SELECT MIN ( t1.top_10 ) , t1.position from sterling as t1 GROUP BY t1.position	2-1708014-2
what is the count of sterling with more than 10 winnings ?	SELECT COUNT ( * ) from sterling as t1 GROUP BY t1.winnings HAVING COUNT ( * ) > 10 	2-1708014-2
which position has both sterling with less than 10 avg . start and sterling with more than 2 avg . start ?	SELECT t1.position from sterling as t1 WHERE t1.avg_._start < 10 INTERSECT SELECT t1.position from sterling as t1 WHERE t1.avg_._start > 2	2-1708014-2
Show everything on sterling	SELECT * FROM sterling	2-1708014-2
what are the position and position of each sterling , listed in descending order by top 5 ?	SELECT t1.position , t1.position from sterling as t1 ORDER BY t1.top_5 DESC	2-1708014-2
what are the position , winnings , and team ( s ) for each sterling ?	SELECT t1.position , t1.winnings , t1.team_ from sterling as t1	2-1708014-2
return the winnings of the sterling that has the fewest corresponding winnings .	SELECT t1.winnings from sterling as t1 GROUP BY t1.winnings ORDER BY COUNT ( * ) ASC LIMIT 1	2-1708014-2
