find the opponent of the tito with the largest round .	SELECT t1.opponent from tito as t1 ORDER BY t1.round DESC LIMIT 1	2-169662-2
what is the count of tito with more than 10 record ?	SELECT COUNT ( * ) from tito as t1 GROUP BY t1.record HAVING COUNT ( * ) > 10 	2-169662-2
what is all the information on the tito with the largest number of res . ?	SELECT * from tito as t1 ORDER BY t1.res_ DESC LIMIT 1	2-169662-2
what are the distinct COLUMN_NAME,0} of every tito that has a greater round than some tito with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.event from tito as t1 WHERE t1.round > ( SELECT MIN ( t1.event ) from tito as t1 WHERE t1.location = 10 )	2-169662-2
find the event who has exactly 10 tito .	SELECT t1.event from tito as t1 GROUP BY t1.event HAVING COUNT ( * ) = 10	2-169662-2
find the event of tito who have round of both 10 and 8 .	SELECT t1.event from tito as t1 WHERE t1.round = 10 INTERSECT SELECT t1.event from tito as t1 WHERE t1.round = 8	2-169662-2
how many tito are there in res . 10 or 97 ?	SELECT COUNT ( * ) from tito as t1 WHERE t1.res_ = 10 OR t1.res_ = 97	2-169662-2
show the res . and method with at least 10 res . .	SELECT t1.res_ , t1.method from tito as t1 GROUP BY t1.res_ HAVING COUNT ( * ) >= 10	2-169662-2
what are the event and time of all tito sorted by decreasing round ?	SELECT t1.event , t1.time from tito as t1 ORDER BY t1.round DESC	2-169662-2
return the opponent of the tito with the fewest round .	SELECT t1.opponent from tito as t1 ORDER BY t1.round ASC LIMIT 1	2-169662-2
