what are the player of 1991 , sorted by their frequency?	SELECT t1.player from 1991 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-18131508-7
display the score , score , and country for each 1991 .	SELECT t1.score , t1.score , t1.country from 1991 as t1	2-18131508-7
what is the country and score of every 1991 that has a money ( £ ) lower than average ?	SELECT t1.country , t1.score from 1991 as t1 WHERE t1.money_ < ( SELECT AVG ( t1.money_ ) {FROM, 3} )	2-18131508-7
which player have less than 3 in 1991 ?	SELECT t1.player from 1991 as t1 GROUP BY t1.player HAVING COUNT ( * ) < 3	2-18131508-7
what is the score of all 1991 whose money ( £ ) is higher than any 1991 ?	SELECT t1.score from 1991 as t1 WHERE t1.money_ > ( SELECT MIN ( t1.money_ ) from 1991 as t1 )	2-18131508-7
find the score of the 1991 that is most frequent across all score .	SELECT t1.score from 1991 as t1 GROUP BY t1.score ORDER BY COUNT ( * ) DESC LIMIT 1	2-18131508-7
what is the to par and country of the 1991 with the top 5 smallest money ( £ ) ?	SELECT t1.to_par , t1.country from 1991 as t1 ORDER BY t1.money_ LIMIT 5	2-18131508-7
please list the place and player of 1991 in descending order of money ( £ ) .	SELECT t1.place , t1.player from 1991 as t1 ORDER BY t1.money_ DESC	2-18131508-7
what is the place of the 1991 with the smallest money ( £ ) ?	SELECT t1.place from 1991 as t1 ORDER BY t1.money_ ASC LIMIT 1	2-18131508-7
show player and place of 1991 .	SELECT t1.player , t1.place from 1991 as t1	2-18131508-7
