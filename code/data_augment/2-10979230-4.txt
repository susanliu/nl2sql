what are the reference of all yuki with reference that is 10 ?	SELECT t1.reference from yuki as t1 GROUP BY t1.reference HAVING COUNT ( * ) = 10	2-10979230-4
what is the release date and romaji title of the yuki with maximum oricon ?	SELECT t1.release_date , t1.romaji_title from yuki as t1 WHERE t1.oricon = ( SELECT MAX ( t1.oricon ) {FROM, 3} )	2-10979230-4
what are the maximum and minimum oricon across all yuki ?	SELECT MAX ( t1.oricon ) , MIN ( t1.oricon ) from yuki as t1	2-10979230-4
find the distinct release date of yuki having oricon between 10 and 26 .	SELECT DISTINCT t1.release_date from yuki as t1 WHERE t1.oricon BETWEEN 10 AND 26	2-10979230-4
what are the distinct reference with oricon between 10 and 87 ?	SELECT DISTINCT t1.reference from yuki as t1 WHERE t1.oricon BETWEEN 10 AND 87	2-10979230-4
what are the distinct reference with oricon between 10 and 73 ?	SELECT DISTINCT t1.reference from yuki as t1 WHERE t1.oricon BETWEEN 10 AND 73	2-10979230-4
return the japanese title of yuki that do not have the reference 10 .	SELECT t1.japanese_title from yuki as t1 WHERE t1.reference ! = 10	2-10979230-4
find the romaji title and japanese title of the yuki whose oricon is lower than the average oricon of all yuki .	SELECT t1.romaji_title , t1.japanese_title from yuki as t1 WHERE t1.oricon < ( SELECT AVG ( t1.oricon ) {FROM, 3} )	2-10979230-4
what are the distinct release date with oricon between 10 and 93 ?	SELECT DISTINCT t1.release_date from yuki as t1 WHERE t1.oricon BETWEEN 10 AND 93	2-10979230-4
which release date has the least oricon ?	SELECT t1.release_date from yuki as t1 ORDER BY t1.oricon ASC LIMIT 1	2-10979230-4
