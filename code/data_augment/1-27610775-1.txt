find the directed by of the none with the highest prod . code.	SELECT t1.directed_by from none as t1 WHERE t1.prod_._code = ( SELECT MAX ( t1.prod_._code ) from none as t1 )	1-27610775-1
what are the u.s. viewers ( millions ) of none , sorted by their frequency?	SELECT t1.u.s._viewers_ from none as t1 GROUP BY t1.u.s._viewers_ ORDER BY COUNT ( * ) ASC LIMIT 1	1-27610775-1
what is the title of the none with the largest prod . code ?	SELECT t1.title from none as t1 WHERE t1.prod_._code = ( SELECT MAX ( t1.prod_._code ) from none as t1 )	1-27610775-1
what is the t1.original_air_date of none that has fewest number of none ?	SELECT t1.original_air_date from none as t1 GROUP BY t1.original_air_date ORDER BY COUNT ( * ) LIMIT 1	1-27610775-1
what are the original air date more than 10 none have ?	SELECT t1.original_air_date from none as t1 GROUP BY t1.original_air_date HAVING COUNT ( * ) > 10	1-27610775-1
what is all the information on the none with the largest number of directed by ?	SELECT * from none as t1 ORDER BY t1.directed_by DESC LIMIT 1	1-27610775-1
which original air date have less than 3 in none ?	SELECT t1.original_air_date from none as t1 GROUP BY t1.original_air_date HAVING COUNT ( * ) < 3	1-27610775-1
find the directed by of none whose prod . code is more than the average prod . code of none .	SELECT t1.directed_by from none as t1 WHERE t1.prod_._code > ( SELECT AVG ( t1.prod_._code ) from none as t1	1-27610775-1
what are the average prod . code of none , grouped by written by ?	SELECT AVG ( t1.prod_._code ) , t1.written_by from none as t1 GROUP BY t1.written_by	1-27610775-1
find all directed by that have fewer than three in none .	SELECT t1.directed_by from none as t1 GROUP BY t1.directed_by HAVING COUNT ( * ) < 3	1-27610775-1
