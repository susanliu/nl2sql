find the country of the canoeing which have athletes 10 but not 98 .	SELECT t1.country from canoeing as t1 WHERE t1.athletes = 10 EXCEPT SELECT t1.country from canoeing as t1 WHERE t1.athletes = 98	2-18646639-5
how many canoeing are there in athletes 10 or 98 ?	SELECT COUNT ( * ) from canoeing as t1 WHERE t1.athletes = 10 OR t1.athletes = 98	2-18646639-5
return the country of the canoeing that has the fewest corresponding country .	SELECT t1.country from canoeing as t1 GROUP BY t1.country ORDER BY COUNT ( * ) ASC LIMIT 1	2-18646639-5
count the number of canoeing that have an time containing 10 .	SELECT COUNT ( * ) from canoeing as t1 WHERE t1.time LIKE 10	2-18646639-5
show the athletes with fewer than 3 canoeing .	SELECT t1.athletes from canoeing as t1 GROUP BY t1.athletes HAVING COUNT ( * ) < 3	2-18646639-5
what is the minimum rank in each time ?	SELECT MIN ( t1.rank ) , t1.time from canoeing as t1 GROUP BY t1.time	2-18646639-5
how many canoeing have time that contains 10 ?	SELECT COUNT ( * ) from canoeing as t1 WHERE t1.time LIKE 10	2-18646639-5
how many distinct athletes correspond to each country ?	SELECT t1.country , COUNT ( DISTINCT t1.athletes ) from canoeing as t1 GROUP BY t1.country	2-18646639-5
what are the time of the canoeing with time other than 10 ?	SELECT t1.time from canoeing as t1 WHERE t1.athletes ! = 10	2-18646639-5
what are the country of canoeing , sorted by their frequency?	SELECT t1.country from canoeing as t1 GROUP BY t1.country ORDER BY COUNT ( * ) ASC LIMIT 1	2-18646639-5
