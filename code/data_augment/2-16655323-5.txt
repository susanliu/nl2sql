what is the sweet sixteen of the 1994 with the minimum # of bids ?	SELECT t1.sweet_sixteen from 1994 as t1 ORDER BY t1.# ASC LIMIT 1	2-16655323-5
what are the {conference of all the 1994 , and the total # of bids by each ?	SELECT t1.conference , SUM ( t1.# ) from 1994 as t1 GROUP BY t1.conference	2-16655323-5
return the smallest # of bids for every round of 32 .	SELECT MIN ( t1.# ) , t1.round_of_32 from 1994 as t1 GROUP BY t1.round_of_32	2-16655323-5
what are the average # of bids of 1994 for different final four ?	SELECT AVG ( t1.# ) , t1.final_four from 1994 as t1 GROUP BY t1.final_four	2-16655323-5
what is the minimum # of bids in each sweet sixteen ?	SELECT MIN ( t1.# ) , t1.sweet_sixteen from 1994 as t1 GROUP BY t1.sweet_sixteen	2-16655323-5
which record have greater # of bids than that of any # of bids in 1994 ?	SELECT t1.record from 1994 as t1 WHERE t1.# > ( SELECT MIN ( t1.# ) from 1994 as t1 )	2-16655323-5
show the win % and the number of unique record containing each win % .	SELECT t1.win_ , COUNT ( DISTINCT t1.record ) from 1994 as t1 GROUP BY t1.win_	2-16655323-5
sort the list of championship game and sweet sixteen of all 1994 in the descending order of # of bids .	SELECT t1.championship_game , t1.sweet_sixteen from 1994 as t1 ORDER BY t1.# DESC	2-16655323-5
what is the championship game of the 1994 with the smallest # of bids ?	SELECT t1.championship_game from 1994 as t1 ORDER BY t1.# ASC LIMIT 1	2-16655323-5
what are the sweet sixteen of the 1994 that have exactly 10 1994 ?	SELECT t1.sweet_sixteen from 1994 as t1 GROUP BY t1.sweet_sixteen HAVING COUNT ( * ) = 10	2-16655323-5
