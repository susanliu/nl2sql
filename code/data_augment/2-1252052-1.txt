what is the rank and start of the bobby with maximum laps ?	SELECT t1.rank , t1.start from bobby as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) {FROM, 3} )	2-1252052-1
find the finish who has exactly 10 bobby .	SELECT t1.finish from bobby as t1 GROUP BY t1.finish HAVING COUNT ( * ) = 10	2-1252052-1
give the t1.qual with the fewest bobby .	SELECT t1.qual from bobby as t1 GROUP BY t1.qual ORDER BY COUNT ( * ) LIMIT 1	2-1252052-1
list all start which have laps higher than the average .	SELECT t1.start from bobby as t1 WHERE t1.laps > ( SELECT AVG ( t1.laps ) from bobby as t1	2-1252052-1
what are the average laps of bobby , grouped by finish ?	SELECT AVG ( t1.laps ) , t1.finish from bobby as t1 GROUP BY t1.finish	2-1252052-1
find the number of bobby that have more than 10 year .	SELECT COUNT ( * ) from bobby as t1 GROUP BY t1.year HAVING COUNT ( * ) > 10 	2-1252052-1
which finish has both bobby with less than 10 laps and bobby with more than 9 laps ?	SELECT t1.finish from bobby as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.finish from bobby as t1 WHERE t1.laps > 9	2-1252052-1
what are the start and finish of each bobby , listed in descending order by laps ?	SELECT t1.start , t1.finish from bobby as t1 ORDER BY t1.laps DESC	2-1252052-1
show finish and the number of distinct start in each finish .	SELECT t1.finish , COUNT ( DISTINCT t1.start ) from bobby as t1 GROUP BY t1.finish	2-1252052-1
what are the finish of all bobby that have 10 or more bobby ?	SELECT t1.finish from bobby as t1 GROUP BY t1.finish HAVING COUNT ( * ) >= 10	2-1252052-1
