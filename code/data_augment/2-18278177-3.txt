what are the average pick of list , grouped by nationality ?	SELECT AVG ( t1.pick ) , t1.nationality from list as t1 GROUP BY t1.nationality	2-18278177-3
what is the maximum and mininum draft {COLUMN} for all list ?	SELECT MAX ( t1.draft ) , MIN ( t1.draft ) from list as t1	2-18278177-3
what is the player and player of every list that has a draft lower than average ?	SELECT t1.player , t1.player from list as t1 WHERE t1.draft < ( SELECT AVG ( t1.draft ) {FROM, 3} )	2-18278177-3
what are the player , nationality , and player of each list ?	SELECT t1.player , t1.nationality , t1.player from list as t1	2-18278177-3
what is the player of all list whose pick is higher than any list ?	SELECT t1.player from list as t1 WHERE t1.pick > ( SELECT MIN ( t1.pick ) from list as t1 )	2-18278177-3
find the distinct nationality of list having pick between 10 and 45 .	SELECT DISTINCT t1.nationality from list as t1 WHERE t1.pick BETWEEN 10 AND 45	2-18278177-3
find the nationality and nationality of the list whose round is lower than the average round of all list .	SELECT t1.nationality , t1.nationality from list as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-18278177-3
please show the different player , ordered by the number of list that have each .	SELECT t1.player from list as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-18278177-3
what are the distinct nationality with draft between 10 and 69 ?	SELECT DISTINCT t1.nationality from list as t1 WHERE t1.draft BETWEEN 10 AND 69	2-18278177-3
find the nationality and player of the list whose round is lower than the average round of all list .	SELECT t1.nationality , t1.player from list as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-18278177-3
