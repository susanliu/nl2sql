what is the count of list with more than 10 place ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.place HAVING COUNT ( * ) > 10 	2-11802780-3
which place have greater zone than that of any zone in list ?	SELECT t1.place from list as t1 WHERE t1.zone > ( SELECT MIN ( t1.zone ) from list as t1 )	2-11802780-3
show all information on the list that has the largest number of stations.	SELECT * from list as t1 ORDER BY t1.stations DESC LIMIT 1	2-11802780-3
how many distinct place correspond to each managed by ?	SELECT t1.managed_by , COUNT ( DISTINCT t1.place ) from list as t1 GROUP BY t1.managed_by	2-11802780-3
show all information on the list that has the largest number of stations.	SELECT * from list as t1 ORDER BY t1.stations DESC LIMIT 1	2-11802780-3
show the managed by and the corresponding number of list sorted by the number of managed by in ascending order .	SELECT t1.managed_by , COUNT ( * ) from list as t1 GROUP BY t1.managed_by ORDER BY COUNT ( * )	2-11802780-3
Return all columns in list .	SELECT * FROM list	2-11802780-3
which stations has most number of list ?	SELECT t1.stations from list as t1 GROUP BY t1.stations ORDER BY COUNT ( * ) DESC LIMIT 1	2-11802780-3
find all place that have fewer than three in list .	SELECT t1.place from list as t1 GROUP BY t1.place HAVING COUNT ( * ) < 3	2-11802780-3
find the managed by of the list who has the largest number of list .	SELECT t1.managed_by from list as t1 GROUP BY t1.managed_by ORDER BY COUNT ( * ) DESC LIMIT 1	2-11802780-3
