return the maximum and minimum place across all 1970 .	SELECT MAX ( t1.place ) , MIN ( t1.place ) from 1970 as t1	2-10301911-5
find the distinct speed of 1970 having place between 10 and 99 .	SELECT DISTINCT t1.speed from 1970 as t1 WHERE t1.place BETWEEN 10 AND 99	2-10301911-5
show all information on the 1970 that has the largest number of rider.	SELECT * from 1970 as t1 ORDER BY t1.rider DESC LIMIT 1	2-10301911-5
which rider has both 1970 with less than 10 place and 1970 with more than 5 place ?	SELECT t1.rider from 1970 as t1 WHERE t1.place < 10 INTERSECT SELECT t1.rider from 1970 as t1 WHERE t1.place > 5	2-10301911-5
what is the count of 1970 with more than 10 rider ?	SELECT COUNT ( * ) from 1970 as t1 GROUP BY t1.rider HAVING COUNT ( * ) > 10 	2-10301911-5
what are the speed of all 1970 with country that is 10 ?	SELECT t1.speed from 1970 as t1 GROUP BY t1.country HAVING COUNT ( * ) = 10	2-10301911-5
show the machine , speed , and rider of all the 1970 .	SELECT t1.machine , t1.speed , t1.rider from 1970 as t1	2-10301911-5
list time and rider who have points greater than 5 or points shorter than 10 .	SELECT t1.time , t1.rider from 1970 as t1 WHERE t1.points > 5 OR t1.points < 10	2-10301911-5
what is the rider of the 1970 with the minimum points ?	SELECT t1.rider from 1970 as t1 ORDER BY t1.points ASC LIMIT 1	2-10301911-5
show the machine and the number of unique speed containing each machine .	SELECT t1.machine , COUNT ( DISTINCT t1.speed ) from 1970 as t1 GROUP BY t1.machine	2-10301911-5
