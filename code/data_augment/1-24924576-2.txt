what is the opponent and result for the list with the rank 5 smallest defenses ?	SELECT t1.opponent , t1.result from list as t1 ORDER BY t1.defenses LIMIT 5	1-24924576-2
what is the opponent of list with the maximum number across all list ?	SELECT t1.opponent from list as t1 WHERE t1.number = ( SELECT MAX ( t1.number ) from list as t1 )	1-24924576-2
find the titles and name of the list with at least 10 date .	SELECT t1.titles , t1.name from list as t1 GROUP BY t1.date HAVING COUNT ( * ) >= 10	1-24924576-2
count the number of list in titles 10 or 9 .	SELECT COUNT ( * ) from list as t1 WHERE t1.titles = 10 OR t1.titles = 9	1-24924576-2
what is the titles of all list whose defenses is higher than any list ?	SELECT t1.titles from list as t1 WHERE t1.defenses > ( SELECT MIN ( t1.defenses ) from list as t1 )	1-24924576-2
which result have greater number than that of any number in list ?	SELECT t1.result from list as t1 WHERE t1.number > ( SELECT MIN ( t1.number ) from list as t1 )	1-24924576-2
what is the date of the list who has the highest number of list ?	SELECT t1.date from list as t1 GROUP BY t1.date ORDER BY COUNT ( * ) DESC LIMIT 1	1-24924576-2
find the number of list that have more than 10 opponent .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	1-24924576-2
what is the result of highest defenses ?	SELECT t1.result from list as t1 ORDER BY t1.defenses DESC LIMIT 1	1-24924576-2
find the titles of list who have both 10 and 99 number .	SELECT t1.titles from list as t1 WHERE t1.number = 10 INTERSECT SELECT t1.titles from list as t1 WHERE t1.number = 99	1-24924576-2
