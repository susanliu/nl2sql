how many distinct 1st venue correspond to each 4th venue ?	SELECT t1.4th_venue , COUNT ( DISTINCT t1.1st_venue ) from fina as t1 GROUP BY t1.4th_venue	1-22482077-1
which 1st venue has most number of fina ?	SELECT t1.1st_venue from fina as t1 GROUP BY t1.1st_venue ORDER BY COUNT ( * ) DESC LIMIT 1	1-22482077-1
how many fina correspond to each 1st venue? show the result in ascending order.	SELECT t1.1st_venue , COUNT ( * ) from fina as t1 GROUP BY t1.1st_venue ORDER BY COUNT ( * )	1-22482077-1
show all 3rd venue and corresponding number of fina in the ascending order of the numbers.	SELECT t1.3rd_venue , COUNT ( * ) from fina as t1 GROUP BY t1.3rd_venue ORDER BY COUNT ( * )	1-22482077-1
what are the distinct 1st venue with year between 10 and 81 ?	SELECT DISTINCT t1.1st_venue from fina as t1 WHERE t1.year BETWEEN 10 AND 81	1-22482077-1
list 4th venue of fina that have the number of fina greater than 10 .	SELECT t1.4th_venue from fina as t1 GROUP BY t1.4th_venue HAVING COUNT ( * ) > 10	1-22482077-1
show the 3rd venue of the fina that has the greatest number of fina .	SELECT t1.3rd_venue from fina as t1 GROUP BY t1.3rd_venue ORDER BY COUNT ( * ) DESC LIMIT 1	1-22482077-1
what is the minimum year in each 2nd venue ?	SELECT MIN ( t1.year ) , t1.2nd_venue from fina as t1 GROUP BY t1.2nd_venue	1-22482077-1
what are the 3rd venue and 3rd venue of each fina , listed in descending order by year ?	SELECT t1.3rd_venue , t1.3rd_venue from fina as t1 ORDER BY t1.year DESC	1-22482077-1
what are the distinct 1st venue with year between 10 and 82 ?	SELECT DISTINCT t1.1st_venue from fina as t1 WHERE t1.year BETWEEN 10 AND 82	1-22482077-1
