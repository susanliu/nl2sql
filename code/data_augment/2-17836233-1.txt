Show everything on 1995	SELECT * FROM 1995	2-17836233-1
which team have greater pick than that of any pick in 1995 ?	SELECT t1.team from 1995 as t1 WHERE t1.pick > ( SELECT MIN ( t1.pick ) from 1995 as t1 )	2-17836233-1
which team have less than 3 in 1995 ?	SELECT t1.team from 1995 as t1 GROUP BY t1.team HAVING COUNT ( * ) < 3	2-17836233-1
what are the average pick of 1995 , grouped by team ?	SELECT AVG ( t1.pick ) , t1.team from 1995 as t1 GROUP BY t1.team	2-17836233-1
how many 1995 are there that have more than {VALUE},0 hometown/school ?	SELECT COUNT ( * ) from 1995 as t1 GROUP BY t1.hometown/school HAVING COUNT ( * ) > 10 	2-17836233-1
show the team of the 1995 that has the most 1995 .	SELECT t1.team from 1995 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) DESC LIMIT 1	2-17836233-1
what are the hometown/school and position of 1995 with 10 or more team ?	SELECT t1.hometown/school , t1.position from 1995 as t1 GROUP BY t1.team HAVING COUNT ( * ) >= 10	2-17836233-1
return the position of the largest pick.	SELECT t1.position from 1995 as t1 ORDER BY t1.pick DESC LIMIT 1	2-17836233-1
which player have an average pick over 10 ?	SELECT t1.player from 1995 as t1 GROUP BY t1.player HAVING AVG ( t1.pick ) >= 10	2-17836233-1
what is the player and position of the 1995 with maximum pick ?	SELECT t1.player , t1.position from 1995 as t1 WHERE t1.pick = ( SELECT MAX ( t1.pick ) {FROM, 3} )	2-17836233-1
