find the album of the sans with the highest year.	SELECT t1.album from sans as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from sans as t1 )	2-15188879-2
what are the length , length , and album for each sans ?	SELECT t1.length , t1.length , t1.album from sans as t1	2-15188879-2
return the smallest year for every album .	SELECT MIN ( t1.year ) , t1.album from sans as t1 GROUP BY t1.album	2-15188879-2
what are the album of all sans that have 10 or more sans ?	SELECT t1.album from sans as t1 GROUP BY t1.album HAVING COUNT ( * ) >= 10	2-15188879-2
how many sans are there in remixed by 10 or 94 ?	SELECT COUNT ( * ) from sans as t1 WHERE t1.remixed_by = 10 OR t1.remixed_by = 94	2-15188879-2
which t1.length has least number of sans ?	SELECT t1.length from sans as t1 GROUP BY t1.length ORDER BY COUNT ( * ) LIMIT 1	2-15188879-2
what is the maximum and mininum year {COLUMN} for all sans ?	SELECT MAX ( t1.year ) , MIN ( t1.year ) from sans as t1	2-15188879-2
please show the version of the sans that have at least 10 records .	SELECT t1.version from sans as t1 GROUP BY t1.version HAVING COUNT ( * ) >= 10	2-15188879-2
select the average year of each sans 's length .	SELECT AVG ( t1.year ) , t1.length from sans as t1 GROUP BY t1.length	2-15188879-2
which t1.album has least number of sans ?	SELECT t1.album from sans as t1 GROUP BY t1.album ORDER BY COUNT ( * ) LIMIT 1	2-15188879-2
