find the away team and away team score of the 1956 whose crowd is lower than the average crowd of all 1956 .	SELECT t1.away_team , t1.away_team_score from 1956 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10640687-15
find the away team score of 1956 which have 10 but no 94 as home team score .	SELECT t1.away_team_score from 1956 as t1 WHERE t1.home_team_score = 10 EXCEPT SELECT t1.away_team_score from 1956 as t1 WHERE t1.home_team_score = 94	2-10640687-15
what are the away team score for 1956 that have an crowd greater than the average .	SELECT t1.away_team_score from 1956 as t1 WHERE t1.crowd > ( SELECT AVG ( t1.crowd ) from 1956 as t1	2-10640687-15
what are the away team and away team score of each 1956 ?	SELECT t1.away_team , t1.away_team_score from 1956 as t1	2-10640687-15
which home team has both 1956 with less than 10 crowd and 1956 with more than 63 crowd ?	SELECT t1.home_team from 1956 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1956 as t1 WHERE t1.crowd > 63	2-10640687-15
return the smallest crowd for every away team score .	SELECT MIN ( t1.crowd ) , t1.away_team_score from 1956 as t1 GROUP BY t1.away_team_score	2-10640687-15
select the average crowd of each 1956 's home team score .	SELECT AVG ( t1.crowd ) , t1.home_team_score from 1956 as t1 GROUP BY t1.home_team_score	2-10640687-15
what are the maximum and minimum crowd across all 1956 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1956 as t1	2-10640687-15
what are the home team score of 1956 whose away team score are not 10 ?	SELECT t1.home_team_score from 1956 as t1 WHERE t1.away_team_score ! = 10	2-10640687-15
what are the venue of 1956 whose away team score are not 10 ?	SELECT t1.venue from 1956 as t1 WHERE t1.away_team_score ! = 10	2-10640687-15
