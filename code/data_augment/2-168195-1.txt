find the rank of 1996 who have bronze of both 10 and 63 .	SELECT t1.rank from 1996 as t1 WHERE t1.bronze = 10 INTERSECT SELECT t1.rank from 1996 as t1 WHERE t1.bronze = 63	2-168195-1
what is the minimum gold in each nation ?	SELECT MIN ( t1.gold ) , t1.nation from 1996 as t1 GROUP BY t1.nation	2-168195-1
sort the list of rank and nation of all 1996 in the descending order of silver .	SELECT t1.rank , t1.nation from 1996 as t1 ORDER BY t1.silver DESC	2-168195-1
what are the maximum and minimum silver across all 1996 ?	SELECT MAX ( t1.silver ) , MIN ( t1.silver ) from 1996 as t1	2-168195-1
list nation and rank who have silver greater than 5 or silver shorter than 10 .	SELECT t1.nation , t1.rank from 1996 as t1 WHERE t1.silver > 5 OR t1.silver < 10	2-168195-1
how many rank did each 1996 do, ordered by number of rank ?	SELECT t1.rank , COUNT ( * ) from 1996 as t1 GROUP BY t1.rank ORDER BY COUNT ( * )	2-168195-1
return the nation and rank of 1996 with the five lowest silver .	SELECT t1.nation , t1.rank from 1996 as t1 ORDER BY t1.silver LIMIT 5	2-168195-1
which rank has both 1996 with less than 10 gold and 1996 with more than 100 gold ?	SELECT t1.rank from 1996 as t1 WHERE t1.gold < 10 INTERSECT SELECT t1.rank from 1996 as t1 WHERE t1.gold > 100	2-168195-1
what is the nation and nation of every 1996 that has a total lower than average ?	SELECT t1.nation , t1.nation from 1996 as t1 WHERE t1.total < ( SELECT AVG ( t1.total ) {FROM, 3} )	2-168195-1
which nation has both 1996 with less than 10 gold and 1996 with more than 29 gold ?	SELECT t1.nation from 1996 as t1 WHERE t1.gold < 10 INTERSECT SELECT t1.nation from 1996 as t1 WHERE t1.gold > 29	2-168195-1
