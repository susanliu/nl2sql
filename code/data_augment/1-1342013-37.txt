which t1.district has least number of united ?	SELECT t1.district from united as t1 GROUP BY t1.district ORDER BY COUNT ( * ) LIMIT 1	1-1342013-37
what is all the information on the united with the largest number of party ?	SELECT * from united as t1 ORDER BY t1.party DESC LIMIT 1	1-1342013-37
what are the party and candidates of each united ?	SELECT t1.party , t1.candidates from united as t1	1-1342013-37
which candidates has both united with less than 10 first elected and united with more than 95 first elected ?	SELECT t1.candidates from united as t1 WHERE t1.first_elected < 10 INTERSECT SELECT t1.candidates from united as t1 WHERE t1.first_elected > 95	1-1342013-37
which party have greater first elected than that of any first elected in united ?	SELECT t1.party from united as t1 WHERE t1.first_elected > ( SELECT MIN ( t1.first_elected ) from united as t1 )	1-1342013-37
how many united are there that have more than {VALUE},0 party ?	SELECT COUNT ( * ) from united as t1 GROUP BY t1.party HAVING COUNT ( * ) > 10 	1-1342013-37
which candidates have an average first elected over 10 ?	SELECT t1.candidates from united as t1 GROUP BY t1.candidates HAVING AVG ( t1.first_elected ) >= 10	1-1342013-37
what are the district of all united with party that is 10 ?	SELECT t1.district from united as t1 GROUP BY t1.party HAVING COUNT ( * ) = 10	1-1342013-37
return the smallest first elected for every candidates .	SELECT MIN ( t1.first_elected ) , t1.candidates from united as t1 GROUP BY t1.candidates	1-1342013-37
show all information on the united that has the largest number of party.	SELECT * from united as t1 ORDER BY t1.party DESC LIMIT 1	1-1342013-37
