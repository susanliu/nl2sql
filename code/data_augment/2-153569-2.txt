find the distinct composer ( s ) of all christmas that have a higher year than some christmas with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.composer_ from christmas as t1 WHERE t1.year > ( SELECT MIN ( t1.composer_ ) from christmas as t1 WHERE t1.performer_ = 10 )	2-153569-2
list the performer ( s ) which average rank is above 10 .	SELECT t1.performer_ from christmas as t1 GROUP BY t1.performer_ HAVING AVG ( t1.rank ) >= 10	2-153569-2
return the smallest year for every song title .	SELECT MIN ( t1.year ) , t1.song_title from christmas as t1 GROUP BY t1.song_title	2-153569-2
Return all columns in christmas .	SELECT * FROM christmas	2-153569-2
return each song title with the number of christmas in ascending order of the number of song title .	SELECT t1.song_title , COUNT ( * ) from christmas as t1 GROUP BY t1.song_title ORDER BY COUNT ( * )	2-153569-2
what are the performer ( s ) of christmas whose performer ( s ) are not 10 ?	SELECT t1.performer_ from christmas as t1 WHERE t1.performer_ ! = 10	2-153569-2
which song title have less than 3 in christmas ?	SELECT t1.song_title from christmas as t1 GROUP BY t1.song_title HAVING COUNT ( * ) < 3	2-153569-2
what are the composer ( s ) of christmas , sorted by their frequency?	SELECT t1.composer_ from christmas as t1 GROUP BY t1.composer_ ORDER BY COUNT ( * ) ASC LIMIT 1	2-153569-2
list the composer ( s ) , song title and the performer ( s ) of the christmas .	SELECT t1.composer_ , t1.song_title , t1.performer_ from christmas as t1	2-153569-2
what is all the information on the christmas with the largest number of composer ( s ) ?	SELECT * from christmas as t1 ORDER BY t1.composer_ DESC LIMIT 1	2-153569-2
