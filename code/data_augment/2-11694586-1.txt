find the player who has exactly 10 kicks .	SELECT t1.player from kicks as t1 GROUP BY t1.player HAVING COUNT ( * ) = 10	2-11694586-1
what are the player of kicks whose team is not 10 ?	SELECT t1.player from kicks as t1 WHERE t1.team ! = 10	2-11694586-1
give the maximum and minimum year of all kicks .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from kicks as t1	2-11694586-1
which score has both kicks with less than 10 year and kicks with more than 29 year ?	SELECT t1.score from kicks as t1 WHERE t1.year < 10 INTERSECT SELECT t1.score from kicks as t1 WHERE t1.year > 29	2-11694586-1
return the different team of kicks , in ascending order of frequency .	SELECT t1.team from kicks as t1 GROUP BY t1.team ORDER BY COUNT ( * ) ASC LIMIT 1	2-11694586-1
which opponent is the most frequent opponent?	SELECT t1.opponent from kicks as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-11694586-1
please show the opponent of the kicks that have at least 10 records .	SELECT t1.opponent from kicks as t1 GROUP BY t1.opponent HAVING COUNT ( * ) >= 10	2-11694586-1
find the opponent of kicks which are team 10 but not team 32 .	SELECT t1.opponent from kicks as t1 WHERE t1.team = 10 EXCEPT SELECT t1.opponent from kicks as t1 WHERE t1.team = 32	2-11694586-1
find the distinct opponent of all kicks that have a higher year than some kicks with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.opponent from kicks as t1 WHERE t1.year > ( SELECT MIN ( t1.opponent ) from kicks as t1 WHERE t1.opponent = 10 )	2-11694586-1
return the player and score of kicks with the five lowest year .	SELECT t1.player , t1.score from kicks as t1 ORDER BY t1.year LIMIT 5	2-11694586-1
