which t1.population has the smallest amount of list?	SELECT t1.population from list as t1 GROUP BY t1.population ORDER BY COUNT ( * ) LIMIT 1	2-12283002-1
find the island of list which have both 10 and 27 as island .	SELECT t1.island from list as t1 WHERE t1.height_ = 10 INTERSECT SELECT t1.island from list as t1 WHERE t1.height_ = 27	2-12283002-1
give the population that has the most list .	SELECT t1.population from list as t1 GROUP BY t1.population ORDER BY COUNT ( * ) DESC LIMIT 1	2-12283002-1
how many distinct island correspond to each population ?	SELECT t1.population , COUNT ( DISTINCT t1.island ) from list as t1 GROUP BY t1.population	2-12283002-1
what is all the information on the list with the largest number of group ?	SELECT * from list as t1 ORDER BY t1.group DESC LIMIT 1	2-12283002-1
what are the area ( ha ) of list whose island are not 10 ?	SELECT t1.area_ from list as t1 WHERE t1.island ! = 10	2-12283002-1
find the population and island of the list whose height ( m ) is lower than the average height ( m ) of all list .	SELECT t1.population , t1.island from list as t1 WHERE t1.height_ < ( SELECT AVG ( t1.height_ ) {FROM, 3} )	2-12283002-1
show all information on the list that has the largest number of group.	SELECT * from list as t1 ORDER BY t1.group DESC LIMIT 1	2-12283002-1
what are the island of all list that have 10 or more list ?	SELECT t1.island from list as t1 GROUP BY t1.island HAVING COUNT ( * ) >= 10	2-12283002-1
find the distinct population of all list that have a higher height ( m ) than some list with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.population from list as t1 WHERE t1.height_ > ( SELECT MIN ( t1.population ) from list as t1 WHERE t1.island = 10 )	2-12283002-1
