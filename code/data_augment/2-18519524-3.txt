find the distinct u.s. r & b of b.g having year between 10 and 58 .	SELECT DISTINCT t1.u.s._r_ from b.g as t1 WHERE t1.year BETWEEN 10 AND 58	2-18519524-3
find the u.s. r & b of the b.g with the largest year .	SELECT t1.u.s._r_ from b.g as t1 ORDER BY t1.year DESC LIMIT 1	2-18519524-3
what are the u.s. rap that have greater year than any year in b.g ?	SELECT t1.u.s._rap from b.g as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from b.g as t1 )	2-18519524-3
what are the u.s. r & b and u.s. rap of the {COLUMN} who have year above five or year below ten ?	SELECT t1.u.s._r_ , t1.u.s._rap from b.g as t1 WHERE t1.year > 5 OR t1.year < 10	2-18519524-3
what are the u.s. rap that have greater year than any year in b.g ?	SELECT t1.u.s._rap from b.g as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from b.g as t1 )	2-18519524-3
select the average year of each b.g 's album .	SELECT AVG ( t1.year ) , t1.album from b.g as t1 GROUP BY t1.album	2-18519524-3
find the u.s. r & b of b.g which have both 10 and 59 as u.s. r & b .	SELECT t1.u.s._r_ from b.g as t1 WHERE t1.year = 10 INTERSECT SELECT t1.u.s._r_ from b.g as t1 WHERE t1.year = 59	2-18519524-3
what is the count of b.g with more than 10 u.s. rap ?	SELECT COUNT ( * ) from b.g as t1 GROUP BY t1.u.s._rap HAVING COUNT ( * ) > 10 	2-18519524-3
what is the u.s. hot 100 and album of the b.g with maximum year ?	SELECT t1.u.s._hot_100 , t1.album from b.g as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-18519524-3
which album have an average year over 10 ?	SELECT t1.album from b.g as t1 GROUP BY t1.album HAVING AVG ( t1.year ) >= 10	2-18519524-3
