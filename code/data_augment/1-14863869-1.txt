find the record of 1984 which are in 10 result but not in 71 result .	SELECT t1.record from 1984 as t1 WHERE t1.result = 10 EXCEPT SELECT t1.record from 1984 as t1 WHERE t1.result = 71	1-14863869-1
find the date which have exactly 10 1984 .	SELECT t1.date from 1984 as t1 GROUP BY t1.date HAVING COUNT ( * ) = 10	1-14863869-1
what is the date of all 1984 whose attendance is higher than any 1984 ?	SELECT t1.date from 1984 as t1 WHERE t1.attendance > ( SELECT MIN ( t1.attendance ) from 1984 as t1 )	1-14863869-1
return the maximum and minimum week across all 1984 .	SELECT MAX ( t1.week ) , MIN ( t1.week ) from 1984 as t1	1-14863869-1
what are the result , date , and record of each 1984 ?	SELECT t1.result , t1.date , t1.record from 1984 as t1	1-14863869-1
show the result and the total week of 1984 .	SELECT t1.result , SUM ( t1.week ) from 1984 as t1 GROUP BY t1.result	1-14863869-1
what is the t1.record of 1984 that has fewest number of 1984 ?	SELECT t1.record from 1984 as t1 GROUP BY t1.record ORDER BY COUNT ( * ) LIMIT 1	1-14863869-1
what are the maximum and minimum week across all 1984 ?	SELECT MAX ( t1.week ) , MIN ( t1.week ) from 1984 as t1	1-14863869-1
what are the game site and result of the {COLUMN} who have week above five or week below ten ?	SELECT t1.game_site , t1.result from 1984 as t1 WHERE t1.week > 5 OR t1.week < 10	1-14863869-1
how many different result correspond to each date ?	SELECT t1.date , COUNT ( DISTINCT t1.result ) from 1984 as t1 GROUP BY t1.date	1-14863869-1
