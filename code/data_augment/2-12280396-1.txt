what are the municipality for list that have an isolation ( km ) greater than the average .	SELECT t1.municipality from list as t1 WHERE t1.isolation_ > ( SELECT AVG ( t1.isolation_ ) from list as t1	2-12280396-1
what is the county and municipality for the list with the rank 5 smallest elevation ( m ) ?	SELECT t1.county , t1.municipality from list as t1 ORDER BY t1.elevation_ LIMIT 5	2-12280396-1
what are the average isolation ( km ) of list for different peak ?	SELECT AVG ( t1.isolation_ ) , t1.peak from list as t1 GROUP BY t1.peak	2-12280396-1
what is the minimum elevation ( m ) in each county ?	SELECT MIN ( t1.elevation_ ) , t1.county from list as t1 GROUP BY t1.county	2-12280396-1
which municipality has both list with less than 10 elevation ( m ) and list with more than 30 elevation ( m ) ?	SELECT t1.municipality from list as t1 WHERE t1.elevation_ < 10 INTERSECT SELECT t1.municipality from list as t1 WHERE t1.elevation_ > 30	2-12280396-1
find the peak of the list that is most frequent across all peak .	SELECT t1.peak from list as t1 GROUP BY t1.peak ORDER BY COUNT ( * ) DESC LIMIT 1	2-12280396-1
what are the county , municipality , and county of each list ?	SELECT t1.county , t1.municipality , t1.county from list as t1	2-12280396-1
how many list are there in county 10 or 85 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.county = 10 OR t1.county = 85	2-12280396-1
return the peak and municipality of list with the five lowest prominence ( m ) .	SELECT t1.peak , t1.municipality from list as t1 ORDER BY t1.prominence_ LIMIT 5	2-12280396-1
find the municipality that have 10 list .	SELECT t1.municipality from list as t1 GROUP BY t1.municipality HAVING COUNT ( * ) = 10	2-12280396-1
