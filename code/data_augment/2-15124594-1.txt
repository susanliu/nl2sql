return the opponent of the 1949 that has the fewest corresponding opponent .	SELECT t1.opponent from 1949 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-15124594-1
return the result of the largest attendance.	SELECT t1.result from 1949 as t1 ORDER BY t1.attendance DESC LIMIT 1	2-15124594-1
what are the opponent for 1949 that have an week greater than the average .	SELECT t1.opponent from 1949 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 1949 as t1	2-15124594-1
find the opponent of 1949 which are in 10 result but not in 6 result .	SELECT t1.opponent from 1949 as t1 WHERE t1.result = 10 EXCEPT SELECT t1.opponent from 1949 as t1 WHERE t1.result = 6	2-15124594-1
what are the opponent , opponent , and opponent of each 1949 ?	SELECT t1.opponent , t1.opponent , t1.opponent from 1949 as t1	2-15124594-1
return the smallest attendance for every result .	SELECT MIN ( t1.attendance ) , t1.result from 1949 as t1 GROUP BY t1.result	2-15124594-1
return the date of the 1949 with the fewest week .	SELECT t1.date from 1949 as t1 ORDER BY t1.week ASC LIMIT 1	2-15124594-1
return the different opponent of 1949 , in ascending order of frequency .	SELECT t1.opponent from 1949 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-15124594-1
show the opponent of the 1949 that has the most 1949 .	SELECT t1.opponent from 1949 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) DESC LIMIT 1	2-15124594-1
please list the result and result of 1949 in descending order of week .	SELECT t1.result , t1.result from 1949 as t1 ORDER BY t1.week DESC	2-15124594-1
