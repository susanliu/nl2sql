what is the fcc info of the most common kmro in all fcc info ?	SELECT t1.fcc_info from kmro as t1 GROUP BY t1.fcc_info ORDER BY COUNT ( * ) DESC LIMIT 1	2-12760940-1
what is the t1.call_sign of kmro that has fewest number of kmro ?	SELECT t1.call_sign from kmro as t1 GROUP BY t1.call_sign ORDER BY COUNT ( * ) LIMIT 1	2-12760940-1
find the fcc info that have 10 kmro .	SELECT t1.fcc_info from kmro as t1 GROUP BY t1.fcc_info HAVING COUNT ( * ) = 10	2-12760940-1
what are the city of license of all kmro with class that is 10 ?	SELECT t1.city_of_license from kmro as t1 GROUP BY t1.class HAVING COUNT ( * ) = 10	2-12760940-1
what is the count of kmro with more than 10 city of license ?	SELECT COUNT ( * ) from kmro as t1 GROUP BY t1.city_of_license HAVING COUNT ( * ) > 10 	2-12760940-1
which city of license has both kmro with less than 10 erp w and kmro with more than 78 erp w ?	SELECT t1.city_of_license from kmro as t1 WHERE t1.erp_w < 10 INTERSECT SELECT t1.city_of_license from kmro as t1 WHERE t1.erp_w > 78	2-12760940-1
what is the city of license and class of every kmro that has a frequency mhz lower than average ?	SELECT t1.city_of_license , t1.class from kmro as t1 WHERE t1.frequency_mhz < ( SELECT AVG ( t1.frequency_mhz ) {FROM, 3} )	2-12760940-1
count the number of kmro in city of license 10 or 15 .	SELECT COUNT ( * ) from kmro as t1 WHERE t1.city_of_license = 10 OR t1.city_of_license = 15	2-12760940-1
what is the call sign and class of the kmro with maximum frequency mhz ?	SELECT t1.call_sign , t1.class from kmro as t1 WHERE t1.frequency_mhz = ( SELECT MAX ( t1.frequency_mhz ) {FROM, 3} )	2-12760940-1
show all information on the kmro that has the largest number of fcc info.	SELECT * from kmro as t1 ORDER BY t1.fcc_info DESC LIMIT 1	2-12760940-1
