what is the minimum order year in each model ?	SELECT MIN ( t1.order_year ) , t1.model from list as t1 GROUP BY t1.model	1-24193494-3
how many list have fuel or propulsion that contain the word 10 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.fuel_or_propulsion LIKE 10	1-24193494-3
show the division of the list that has the greatest number of list .	SELECT t1.division from list as t1 GROUP BY t1.division ORDER BY COUNT ( * ) DESC LIMIT 1	1-24193494-3
return the manufacturer and division of list with the five lowest order year .	SELECT t1.manufacturer , t1.division from list as t1 ORDER BY t1.order_year LIMIT 5	1-24193494-3
list all information about list .	SELECT * FROM list	1-24193494-3
what is the manufacturer of the list with the largest order year ?	SELECT t1.manufacturer from list as t1 WHERE t1.order_year = ( SELECT MAX ( t1.order_year ) from list as t1 )	1-24193494-3
please show the division of the list with count more than 10 .	SELECT t1.division from list as t1 GROUP BY t1.division HAVING COUNT ( * ) > 10	1-24193494-3
show division and the number of distinct fuel or propulsion in each division .	SELECT t1.division , COUNT ( DISTINCT t1.fuel_or_propulsion ) from list as t1 GROUP BY t1.division	1-24193494-3
give the t1.division with the fewest list .	SELECT t1.division from list as t1 GROUP BY t1.division ORDER BY COUNT ( * ) LIMIT 1	1-24193494-3
give the powertrain ( engine/transmission ) that has the most list .	SELECT t1.powertrain_ from list as t1 GROUP BY t1.powertrain_ ORDER BY COUNT ( * ) DESC LIMIT 1	1-24193494-3
