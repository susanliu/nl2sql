find the away team score and ground of the 1990 whose crowd is lower than the average crowd of all 1990 .	SELECT t1.away_team_score , t1.ground from 1990 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-16387653-1
which t1.away_team has least number of 1990 ?	SELECT t1.away_team from 1990 as t1 GROUP BY t1.away_team ORDER BY COUNT ( * ) LIMIT 1	2-16387653-1
what are the home team , home team , and home team of each 1990 ?	SELECT t1.home_team , t1.home_team , t1.home_team from 1990 as t1	2-16387653-1
find the home team score that have 10 1990 .	SELECT t1.home_team_score from 1990 as t1 GROUP BY t1.home_team_score HAVING COUNT ( * ) = 10	2-16387653-1
return the home team score of the 1990 that has the fewest corresponding home team score .	SELECT t1.home_team_score from 1990 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	2-16387653-1
what are the maximum and minimum crowd across all 1990 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1990 as t1	2-16387653-1
which home team has both 1990 with less than 10 crowd and 1990 with more than 93 crowd ?	SELECT t1.home_team from 1990 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.home_team from 1990 as t1 WHERE t1.crowd > 93	2-16387653-1
what are the ground of the 1990 with ground other than 10 ?	SELECT t1.ground from 1990 as t1 WHERE t1.ground ! = 10	2-16387653-1
what are the home team with exactly 10 1990 ?	SELECT t1.home_team from 1990 as t1 GROUP BY t1.home_team HAVING COUNT ( * ) = 10	2-16387653-1
find the away team of 1990 who have both 10 and 75 crowd .	SELECT t1.away_team from 1990 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.away_team from 1990 as t1 WHERE t1.crowd = 75	2-16387653-1
