show the result of la whose result are not 10.	SELECT t1.result from la as t1 WHERE t1.result ! = 10	2-1195142-2
what are the award of all la with result that is 10 ?	SELECT t1.award from la as t1 GROUP BY t1.result HAVING COUNT ( * ) = 10	2-1195142-2
what is the nominee and nominee of every la that has a year lower than average ?	SELECT t1.nominee , t1.nominee from la as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	2-1195142-2
find the distinct COLUMN_NAME,0} of all la that have year higher than some la from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.nominee from la as t1 WHERE t1.year > ( SELECT MIN ( t1.nominee ) from la as t1 WHERE t1.award = 10 )	2-1195142-2
what are the distinct COLUMN_NAME,0} of la with year higher than any la from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.award from la as t1 WHERE t1.year > ( SELECT MIN ( t1.award ) from la as t1 WHERE t1.category = 10 )	2-1195142-2
sort the list of result and result of all la in the descending order of year .	SELECT t1.result , t1.result from la as t1 ORDER BY t1.year DESC	2-1195142-2
count the number of la that have an result containing 10 .	SELECT COUNT ( * ) from la as t1 WHERE t1.result LIKE 10	2-1195142-2
please show the result of the la that have at least 10 records .	SELECT t1.result from la as t1 GROUP BY t1.result HAVING COUNT ( * ) >= 10	2-1195142-2
what are the award for la who have more than the average year?	SELECT t1.award from la as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from la as t1	2-1195142-2
what is the category and result of the la with maximum year ?	SELECT t1.category , t1.result from la as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-1195142-2
