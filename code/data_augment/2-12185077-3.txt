give the maximum and minimum points of all shinichi .	SELECT MAX ( t1.points ) , MIN ( t1.points ) from shinichi as t1	2-12185077-3
list all information about shinichi .	SELECT * FROM shinichi	2-12185077-3
show the rank and the number of unique team containing each rank .	SELECT t1.rank , COUNT ( DISTINCT t1.team ) from shinichi as t1 GROUP BY t1.rank	2-12185077-3
return the maximum and minimum year across all shinichi .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from shinichi as t1	2-12185077-3
count the number of shinichi in rank 10 or 84 .	SELECT COUNT ( * ) from shinichi as t1 WHERE t1.rank = 10 OR t1.rank = 84	2-12185077-3
show class and the number of distinct machine in each class .	SELECT t1.class , COUNT ( DISTINCT t1.machine ) from shinichi as t1 GROUP BY t1.class	2-12185077-3
list the class which average year is above 10 .	SELECT t1.class from shinichi as t1 GROUP BY t1.class HAVING AVG ( t1.year ) >= 10	2-12185077-3
show all machine and the total year for each .	SELECT t1.machine , SUM ( t1.year ) from shinichi as t1 GROUP BY t1.machine	2-12185077-3
find the machine of the shinichi that is most frequent across all machine .	SELECT t1.machine from shinichi as t1 GROUP BY t1.machine ORDER BY COUNT ( * ) DESC LIMIT 1	2-12185077-3
what are the rank and team of the {COLUMN} who have points above five or year below ten ?	SELECT t1.rank , t1.team from shinichi as t1 WHERE t1.points > 5 OR t1.year < 10	2-12185077-3
