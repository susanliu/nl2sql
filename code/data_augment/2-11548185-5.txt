what are the airdate of all list with airdate that is 10 ?	SELECT t1.airdate from list as t1 GROUP BY t1.airdate HAVING COUNT ( * ) = 10	2-11548185-5
what are the airdate that have greater # in season than any # in season in list ?	SELECT t1.airdate from list as t1 WHERE t1.# > ( SELECT MIN ( t1.# ) from list as t1 )	2-11548185-5
what are the airdate of list with episode # greater than the average of all list ?	SELECT t1.airdate from list as t1 WHERE t1.episode_ > ( SELECT AVG ( t1.episode_ ) from list as t1	2-11548185-5
find the episode name and episode name of the list with at least 10 episode name .	SELECT t1.episode_name , t1.episode_name from list as t1 GROUP BY t1.episode_name HAVING COUNT ( * ) >= 10	2-11548185-5
return the episode name of the list that has the fewest corresponding episode name .	SELECT t1.episode_name from list as t1 GROUP BY t1.episode_name ORDER BY COUNT ( * ) ASC LIMIT 1	2-11548185-5
please show the airdate of the list that have at least 10 records .	SELECT t1.airdate from list as t1 GROUP BY t1.airdate HAVING COUNT ( * ) >= 10	2-11548185-5
what are the location and location of list with 10 or more location ?	SELECT t1.location , t1.location from list as t1 GROUP BY t1.location HAVING COUNT ( * ) >= 10	2-11548185-5
how many distinct airdate correspond to each episode name ?	SELECT t1.episode_name , COUNT ( DISTINCT t1.airdate ) from list as t1 GROUP BY t1.episode_name	2-11548185-5
which t1.location has least number of list ?	SELECT t1.location from list as t1 GROUP BY t1.location ORDER BY COUNT ( * ) LIMIT 1	2-11548185-5
which location have less than 3 in list ?	SELECT t1.location from list as t1 GROUP BY t1.location HAVING COUNT ( * ) < 3	2-11548185-5
