show the attendance of 2004 whose date are not 10.	SELECT t1.attendance from 2004 as t1 WHERE t1.date ! = 10	2-15405000-2
select the average week of each 2004 's opponent .	SELECT AVG ( t1.week ) , t1.opponent from 2004 as t1 GROUP BY t1.opponent	2-15405000-2
what are the attendance and attendance of each 2004 , listed in descending order by week ?	SELECT t1.attendance , t1.attendance from 2004 as t1 ORDER BY t1.week DESC	2-15405000-2
how many different date correspond to each opponent ?	SELECT t1.opponent , COUNT ( DISTINCT t1.date ) from 2004 as t1 GROUP BY t1.opponent	2-15405000-2
what is the opponent of the 2004 with the minimum week ?	SELECT t1.opponent from 2004 as t1 ORDER BY t1.week ASC LIMIT 1	2-15405000-2
what are the result of 2004 with week above the average week across all 2004 ?	SELECT t1.result from 2004 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 2004 as t1	2-15405000-2
which result has both 2004 with less than 10 week and 2004 with more than 29 week ?	SELECT t1.result from 2004 as t1 WHERE t1.week < 10 INTERSECT SELECT t1.result from 2004 as t1 WHERE t1.week > 29	2-15405000-2
return the different opponent of 2004 , in ascending order of frequency .	SELECT t1.opponent from 2004 as t1 GROUP BY t1.opponent ORDER BY COUNT ( * ) ASC LIMIT 1	2-15405000-2
find the distinct opponent of all 2004 that have a higher week than some 2004 with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.opponent from 2004 as t1 WHERE t1.week > ( SELECT MIN ( t1.opponent ) from 2004 as t1 WHERE t1.attendance = 10 )	2-15405000-2
what are the opponent for 2004 that have an week greater than the average .	SELECT t1.opponent from 2004 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 2004 as t1	2-15405000-2
