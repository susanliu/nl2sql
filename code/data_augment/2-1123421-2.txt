what is the time/retired and constructor of the 2001 with maximum laps ?	SELECT t1.time/retired , t1.constructor from 2001 as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) {FROM, 3} )	2-1123421-2
which t1.driver has least number of 2001 ?	SELECT t1.driver from 2001 as t1 GROUP BY t1.driver ORDER BY COUNT ( * ) LIMIT 1	2-1123421-2
what are the constructor of the 2001 with constructor other than 10 ?	SELECT t1.constructor from 2001 as t1 WHERE t1.time/retired ! = 10	2-1123421-2
show the constructor and the number of unique constructor containing each constructor .	SELECT t1.constructor , COUNT ( DISTINCT t1.constructor ) from 2001 as t1 GROUP BY t1.constructor	2-1123421-2
Return all columns in 2001 .	SELECT * FROM 2001	2-1123421-2
show the driver of the 2001 that has the most 2001 .	SELECT t1.driver from 2001 as t1 GROUP BY t1.driver ORDER BY COUNT ( * ) DESC LIMIT 1	2-1123421-2
show all driver and corresponding number of 2001 in the ascending order of the numbers.	SELECT t1.driver , COUNT ( * ) from 2001 as t1 GROUP BY t1.driver ORDER BY COUNT ( * )	2-1123421-2
find the driver of 2001 which have both 10 and 5 as driver .	SELECT t1.driver from 2001 as t1 WHERE t1.grid = 10 INTERSECT SELECT t1.driver from 2001 as t1 WHERE t1.grid = 5	2-1123421-2
what are the driver and time/retired of the {COLUMN} who have grid above five or laps below ten ?	SELECT t1.driver , t1.time/retired from 2001 as t1 WHERE t1.grid > 5 OR t1.laps < 10	2-1123421-2
find the time/retired and time/retired of the 2001 whose laps is lower than the average laps of all 2001 .	SELECT t1.time/retired , t1.time/retired from 2001 as t1 WHERE t1.laps < ( SELECT AVG ( t1.laps ) {FROM, 3} )	2-1123421-2
