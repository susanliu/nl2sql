show the draws p.k. wins / p.k. losses of 1997 who have at least 10 1997 .	SELECT t1.draws_p.k._wins_/_p.k._losses from 1997 as t1 GROUP BY t1.draws_p.k._wins_/_p.k._losses HAVING COUNT ( * ) >= 10	1-18703133-1
what are the team of 1997 , sorted by their frequency?	SELECT t1.team from 1997 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) ASC LIMIT 1	1-18703133-1
what are the draws p.k. wins / p.k. losses and draws p.k. wins / p.k. losses of the {COLUMN} who have conceded above five or points below ten ?	SELECT t1.draws_p.k._wins_/_p.k._losses , t1.draws_p.k._wins_/_p.k._losses from 1997 as t1 WHERE t1.conceded > 5 OR t1.points < 10	1-18703133-1
count the number of 1997 that have an draws p.k. wins / p.k. losses containing 10 .	SELECT COUNT ( * ) from 1997 as t1 WHERE t1.draws_p.k._wins_/_p.k._losses LIKE 10	1-18703133-1
sort the list of draws p.k. wins / p.k. losses and team of all 1997 in the descending order of wins .	SELECT t1.draws_p.k._wins_/_p.k._losses , t1.team from 1997 as t1 ORDER BY t1.wins DESC	1-18703133-1
what is the minimum conceded in each draws p.k. wins / p.k. losses ?	SELECT MIN ( t1.conceded ) , t1.draws_p.k._wins_/_p.k._losses from 1997 as t1 GROUP BY t1.draws_p.k._wins_/_p.k._losses	1-18703133-1
list the draws p.k. wins / p.k. losses which average scored is above 10 .	SELECT t1.draws_p.k._wins_/_p.k._losses from 1997 as t1 GROUP BY t1.draws_p.k._wins_/_p.k._losses HAVING AVG ( t1.scored ) >= 10	1-18703133-1
what are the {team of all the 1997 , and the total conceded by each ?	SELECT t1.team , SUM ( t1.conceded ) from 1997 as t1 GROUP BY t1.team	1-18703133-1
list team and draws p.k. wins / p.k. losses who have points greater than 5 or position shorter than 10 .	SELECT t1.team , t1.draws_p.k._wins_/_p.k._losses from 1997 as t1 WHERE t1.points > 5 OR t1.position < 10	1-18703133-1
return the different team of 1997 , in ascending order of frequency .	SELECT t1.team from 1997 as t1 GROUP BY t1.team ORDER BY COUNT ( * ) ASC LIMIT 1	1-18703133-1
