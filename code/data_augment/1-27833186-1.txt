show the best-conditioned horse and the total year of ride .	SELECT t1.best-conditioned_horse , SUM ( t1.year ) from ride as t1 GROUP BY t1.best-conditioned_horse	1-27833186-1
find the best-conditioned horse of ride who have more than 10 ride .	SELECT t1.best-conditioned_horse from ride as t1 GROUP BY t1.best-conditioned_horse HAVING COUNT ( * ) > 10	1-27833186-1
which location have greater distance ( miles ) than that of any distance ( miles ) in ride ?	SELECT t1.location from ride as t1 WHERE t1.distance_ > ( SELECT MIN ( t1.distance_ ) from ride as t1 )	1-27833186-1
what is the best-conditioned horse of ride with the maximum year across all ride ?	SELECT t1.best-conditioned_horse from ride as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from ride as t1 )	1-27833186-1
what are the horse name , horse name , and best-conditioned horse for each ride ?	SELECT t1.horse_name , t1.horse_name , t1.best-conditioned_horse from ride as t1	1-27833186-1
what are the horse name and rider names of each ride , listed in descending order by distance ( miles ) ?	SELECT t1.horse_name , t1.rider_names from ride as t1 ORDER BY t1.distance_ DESC	1-27833186-1
what is the rider names of the ride with the minimum distance ( miles ) ?	SELECT t1.rider_names from ride as t1 ORDER BY t1.distance_ ASC LIMIT 1	1-27833186-1
find the rider names of ride who have both 10 and 66 year .	SELECT t1.rider_names from ride as t1 WHERE t1.year = 10 INTERSECT SELECT t1.rider_names from ride as t1 WHERE t1.year = 66	1-27833186-1
count the number of ride that have an best-conditioned horse containing 10 .	SELECT COUNT ( * ) from ride as t1 WHERE t1.best-conditioned_horse LIKE 10	1-27833186-1
what are the average distance ( miles ) of ride , grouped by rider names ?	SELECT AVG ( t1.distance_ ) , t1.rider_names from ride as t1 GROUP BY t1.rider_names	1-27833186-1
