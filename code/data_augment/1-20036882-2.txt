show winning pilot and the number of distinct fastest qualifying in each winning pilot .	SELECT t1.winning_pilot , COUNT ( DISTINCT t1.fastest_qualifying ) from 2009 as t1 GROUP BY t1.winning_pilot	1-20036882-2
find the date of the 2009 with the largest round .	SELECT t1.date from 2009 as t1 ORDER BY t1.round DESC LIMIT 1	1-20036882-2
what are the country and country of the {COLUMN} who have round above five or round below ten ?	SELECT t1.country , t1.country from 2009 as t1 WHERE t1.round > 5 OR t1.round < 10	1-20036882-2
how many 2009 correspond to each date? show the result in ascending order.	SELECT t1.date , COUNT ( * ) from 2009 as t1 GROUP BY t1.date ORDER BY COUNT ( * )	1-20036882-2
what are the fastest qualifying of the 2009 with fastest qualifying other than 10 ?	SELECT t1.fastest_qualifying from 2009 as t1 WHERE t1.date ! = 10	1-20036882-2
what are the distinct COLUMN_NAME,0} of 2009 with round higher than any 2009 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.winning_aircraft from 2009 as t1 WHERE t1.round > ( SELECT MIN ( t1.winning_aircraft ) from 2009 as t1 WHERE t1.winning_pilot = 10 )	1-20036882-2
please show the different date , ordered by the number of 2009 that have each .	SELECT t1.date from 2009 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	1-20036882-2
show location and the number of distinct winning pilot in each location .	SELECT t1.location , COUNT ( DISTINCT t1.winning_pilot ) from 2009 as t1 GROUP BY t1.location	1-20036882-2
show the location and the number of unique winning pilot containing each location .	SELECT t1.location , COUNT ( DISTINCT t1.winning_pilot ) from 2009 as t1 GROUP BY t1.location	1-20036882-2
what are the average round of 2009 , grouped by winning pilot ?	SELECT AVG ( t1.round ) , t1.winning_pilot from 2009 as t1 GROUP BY t1.winning_pilot	1-20036882-2
