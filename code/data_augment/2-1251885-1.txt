which start has both al with less than 10 laps and al with more than 53 laps ?	SELECT t1.start from al as t1 WHERE t1.laps < 10 INTERSECT SELECT t1.start from al as t1 WHERE t1.laps > 53	2-1251885-1
which rank has the least laps ?	SELECT t1.rank from al as t1 ORDER BY t1.laps ASC LIMIT 1	2-1251885-1
what is the start of al with the maximum laps across all al ?	SELECT t1.start from al as t1 WHERE t1.laps = ( SELECT MAX ( t1.laps ) from al as t1 )	2-1251885-1
find all finish that have fewer than three in al .	SELECT t1.finish from al as t1 GROUP BY t1.finish HAVING COUNT ( * ) < 3	2-1251885-1
what are the year of al whose qual are not 10 ?	SELECT t1.year from al as t1 WHERE t1.qual ! = 10	2-1251885-1
find the year of al which are in 10 rank but not in 20 rank .	SELECT t1.year from al as t1 WHERE t1.rank = 10 EXCEPT SELECT t1.year from al as t1 WHERE t1.rank = 20	2-1251885-1
return the rank of the largest laps.	SELECT t1.rank from al as t1 ORDER BY t1.laps DESC LIMIT 1	2-1251885-1
which t1.qual has the smallest amount of al?	SELECT t1.qual from al as t1 GROUP BY t1.qual ORDER BY COUNT ( * ) LIMIT 1	2-1251885-1
what is the finish of all al whose laps is higher than any al ?	SELECT t1.finish from al as t1 WHERE t1.laps > ( SELECT MIN ( t1.laps ) from al as t1 )	2-1251885-1
what is the start and year for the al with the rank 5 smallest laps ?	SELECT t1.start , t1.year from al as t1 ORDER BY t1.laps LIMIT 5	2-1251885-1
