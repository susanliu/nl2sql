how many longview are there in type 10 or 8 ?	SELECT COUNT ( * ) from longview as t1 WHERE t1.type = 10 OR t1.type = 8	2-151199-7
what are the licensed location of all longview with call letters that is 10 ?	SELECT t1.licensed_location from longview as t1 GROUP BY t1.call_letters HAVING COUNT ( * ) = 10	2-151199-7
how many longview have format that contains 10 ?	SELECT COUNT ( * ) from longview as t1 WHERE t1.format LIKE 10	2-151199-7
show the format of the longview that has the most longview .	SELECT t1.format from longview as t1 GROUP BY t1.format ORDER BY COUNT ( * ) DESC LIMIT 1	2-151199-7
what are the type for all longview , and what is the total {frequency ( khz ) for each ?	SELECT t1.type , SUM ( t1.frequency_ ) from longview as t1 GROUP BY t1.type	2-151199-7
what are the call letters and call letters of the {COLUMN} who have frequency ( khz ) above five or frequency ( khz ) below ten ?	SELECT t1.call_letters , t1.call_letters from longview as t1 WHERE t1.frequency_ > 5 OR t1.frequency_ < 10	2-151199-7
what is the type of highest frequency ( khz ) ?	SELECT t1.type from longview as t1 ORDER BY t1.frequency_ DESC LIMIT 1	2-151199-7
what are all the call letters and format?	SELECT t1.call_letters , t1.format from longview as t1	2-151199-7
how many longview does each format have ?	SELECT t1.format , COUNT ( * ) from longview as t1 GROUP BY t1.format ORDER BY COUNT ( * )	2-151199-7
show the licensed location of the longview that has the most longview .	SELECT t1.licensed_location from longview as t1 GROUP BY t1.licensed_location ORDER BY COUNT ( * ) DESC LIMIT 1	2-151199-7
