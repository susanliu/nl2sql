find the time of the marcos with the highest round.	SELECT t1.time from marcos as t1 WHERE t1.round = ( SELECT MAX ( t1.round ) from marcos as t1 )	2-14767837-2
find the time and opponent of the marcos whose round is lower than the average round of all marcos .	SELECT t1.time , t1.opponent from marcos as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	2-14767837-2
find the record of the marcos with the largest round .	SELECT t1.record from marcos as t1 ORDER BY t1.round DESC LIMIT 1	2-14767837-2
find the distinct event of marcos having round between 10 and 64 .	SELECT DISTINCT t1.event from marcos as t1 WHERE t1.round BETWEEN 10 AND 64	2-14767837-2
list the method which average round is above 10 .	SELECT t1.method from marcos as t1 GROUP BY t1.method HAVING AVG ( t1.round ) >= 10	2-14767837-2
how many distinct location correspond to each time ?	SELECT t1.time , COUNT ( DISTINCT t1.location ) from marcos as t1 GROUP BY t1.time	2-14767837-2
what are the time of all marcos with opponent that is 10 ?	SELECT t1.time from marcos as t1 GROUP BY t1.opponent HAVING COUNT ( * ) = 10	2-14767837-2
which record have greater round than that of any round in marcos ?	SELECT t1.record from marcos as t1 WHERE t1.round > ( SELECT MIN ( t1.round ) from marcos as t1 )	2-14767837-2
select the average round of each marcos 's opponent .	SELECT AVG ( t1.round ) , t1.opponent from marcos as t1 GROUP BY t1.opponent	2-14767837-2
what are the res . with exactly 10 marcos ?	SELECT t1.res_ from marcos as t1 GROUP BY t1.res_ HAVING COUNT ( * ) = 10	2-14767837-2
