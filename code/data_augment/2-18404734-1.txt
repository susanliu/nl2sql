give the maximum and minimum year of all col .	SELECT MAX ( t1.year ) , MIN ( t1.year ) from col as t1	2-18404734-1
what is the finish and finish of the col with the top 5 smallest year ?	SELECT t1.finish , t1.finish from col as t1 ORDER BY t1.year LIMIT 5	2-18404734-1
what are the start that have greater year than any year in col ?	SELECT t1.start from col as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from col as t1 )	2-18404734-1
Show everything on col	SELECT * FROM col	2-18404734-1
select the average year of each col 's start .	SELECT AVG ( t1.year ) , t1.start from col as t1 GROUP BY t1.start	2-18404734-1
what is the start and leader at the summit of the col with maximum year ?	SELECT t1.start , t1.leader_at_the_summit from col as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) {FROM, 3} )	2-18404734-1
how many col does each start have ?	SELECT t1.start , COUNT ( * ) from col as t1 GROUP BY t1.start ORDER BY COUNT ( * )	2-18404734-1
give the finish that has the most col .	SELECT t1.finish from col as t1 GROUP BY t1.finish ORDER BY COUNT ( * ) DESC LIMIT 1	2-18404734-1
what is the finish of all col whose year is higher than any col ?	SELECT t1.finish from col as t1 WHERE t1.year > ( SELECT MIN ( t1.year ) from col as t1 )	2-18404734-1
what are the average year of col for different leader at the summit ?	SELECT AVG ( t1.year ) , t1.leader_at_the_summit from col as t1 GROUP BY t1.leader_at_the_summit	2-18404734-1
