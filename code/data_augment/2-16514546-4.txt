what is the player of the 1998 with the minimum money ( $ ) ?	SELECT t1.player from 1998 as t1 ORDER BY t1.money_ ASC LIMIT 1	2-16514546-4
give the maximum and minimum money ( $ ) of all 1998 .	SELECT MAX ( t1.money_ ) , MIN ( t1.money_ ) from 1998 as t1	2-16514546-4
show all player and corresponding number of 1998 sorted by the count .	SELECT t1.player , COUNT ( * ) from 1998 as t1 GROUP BY t1.player ORDER BY COUNT ( * )	2-16514546-4
what is the place and score of the 1998 with maximum money ( $ ) ?	SELECT t1.place , t1.score from 1998 as t1 WHERE t1.money_ = ( SELECT MAX ( t1.money_ ) {FROM, 3} )	2-16514546-4
what are the score of the 1998 that have exactly 10 1998 ?	SELECT t1.score from 1998 as t1 GROUP BY t1.score HAVING COUNT ( * ) = 10	2-16514546-4
what are the player of all 1998 with score that is 10 ?	SELECT t1.player from 1998 as t1 GROUP BY t1.score HAVING COUNT ( * ) = 10	2-16514546-4
show the player of the 1998 that has the greatest number of 1998 .	SELECT t1.player from 1998 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) DESC LIMIT 1	2-16514546-4
find the player of 1998 whose money ( $ ) is more than the average money ( $ ) of 1998 .	SELECT t1.player from 1998 as t1 WHERE t1.money_ > ( SELECT AVG ( t1.money_ ) from 1998 as t1	2-16514546-4
show the player with fewer than 3 1998 .	SELECT t1.player from 1998 as t1 GROUP BY t1.player HAVING COUNT ( * ) < 3	2-16514546-4
find the distinct place of 1998 having money ( $ ) between 10 and 62 .	SELECT DISTINCT t1.place from 1998 as t1 WHERE t1.money_ BETWEEN 10 AND 62	2-16514546-4
