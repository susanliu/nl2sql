find the distinct opponent of united having elected between 10 and 19 .	SELECT DISTINCT t1.opponent from united as t1 WHERE t1.elected BETWEEN 10 AND 19	1-1341568-44
show the party with fewer than 3 united .	SELECT t1.party from united as t1 GROUP BY t1.party HAVING COUNT ( * ) < 3	1-1341568-44
show party for all united whose elected are greater than the average .	SELECT t1.party from united as t1 WHERE t1.elected > ( SELECT AVG ( t1.elected ) from united as t1	1-1341568-44
please show the different incumbent , ordered by the number of united that have each .	SELECT t1.incumbent from united as t1 GROUP BY t1.incumbent ORDER BY COUNT ( * ) ASC LIMIT 1	1-1341568-44
what are the status , opponent , and status of each united ?	SELECT t1.status , t1.opponent , t1.status from united as t1	1-1341568-44
what are the party and incumbent of the {COLUMN} who have elected above five or elected below ten ?	SELECT t1.party , t1.incumbent from united as t1 WHERE t1.elected > 5 OR t1.elected < 10	1-1341568-44
list district of united that have the number of united greater than 10 .	SELECT t1.district from united as t1 GROUP BY t1.district HAVING COUNT ( * ) > 10	1-1341568-44
what are the average elected of united , grouped by status ?	SELECT AVG ( t1.elected ) , t1.status from united as t1 GROUP BY t1.status	1-1341568-44
which incumbent has both united with less than 10 elected and united with more than 8 elected ?	SELECT t1.incumbent from united as t1 WHERE t1.elected < 10 INTERSECT SELECT t1.incumbent from united as t1 WHERE t1.elected > 8	1-1341568-44
show all information on the united that has the largest number of district.	SELECT * from united as t1 ORDER BY t1.district DESC LIMIT 1	1-1341568-44
