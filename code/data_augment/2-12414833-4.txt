what are the conference for list who have more than the average national championships?	SELECT t1.conference from list as t1 WHERE t1.national_championships > ( SELECT AVG ( t1.national_championships ) from list as t1	2-12414833-4
what are all the school and city?	SELECT t1.school , t1.city from list as t1	2-12414833-4
list the conference and school of all list sorted by national championships in descending order .	SELECT t1.conference , t1.school from list as t1 ORDER BY t1.national_championships DESC	2-12414833-4
what are the distinct nickname with national championships between 10 and 45 ?	SELECT DISTINCT t1.nickname from list as t1 WHERE t1.national_championships BETWEEN 10 AND 45	2-12414833-4
show the city and the total national championships of list .	SELECT t1.city , SUM ( t1.national_championships ) from list as t1 GROUP BY t1.city	2-12414833-4
show the school shared by more than 10 list .	SELECT t1.school from list as t1 GROUP BY t1.school HAVING COUNT ( * ) > 10	2-12414833-4
what is the maximum and mininum national championships {COLUMN} for all list ?	SELECT MAX ( t1.national_championships ) , MIN ( t1.national_championships ) from list as t1	2-12414833-4
show the conference of list whose city are not 10.	SELECT t1.conference from list as t1 WHERE t1.city ! = 10	2-12414833-4
how many different nickname correspond to each conference ?	SELECT t1.conference , COUNT ( DISTINCT t1.nickname ) from list as t1 GROUP BY t1.conference	2-12414833-4
please show the city of the list with count more than 10 .	SELECT t1.city from list as t1 GROUP BY t1.city HAVING COUNT ( * ) > 10	2-12414833-4
