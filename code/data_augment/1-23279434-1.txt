what is the production code of the none with the smallest series # ?	SELECT t1.production_code from none as t1 ORDER BY t1.series_ ASC LIMIT 1	1-23279434-1
which directed by has both none with less than 10 series # and none with more than 14 series # ?	SELECT t1.directed_by from none as t1 WHERE t1.series_ < 10 INTERSECT SELECT t1.directed_by from none as t1 WHERE t1.series_ > 14	1-23279434-1
what are the title and original air date of the {COLUMN} who have series # above five or series # below ten ?	SELECT t1.title , t1.original_air_date from none as t1 WHERE t1.series_ > 5 OR t1.series_ < 10	1-23279434-1
what is the count of none with more than 10 directed by ?	SELECT COUNT ( * ) from none as t1 GROUP BY t1.directed_by HAVING COUNT ( * ) > 10 	1-23279434-1
what is the directed by of none with the maximum series # across all none ?	SELECT t1.directed_by from none as t1 WHERE t1.series_ = ( SELECT MAX ( t1.series_ ) from none as t1 )	1-23279434-1
find the number of none whose directed by contain the word 10 .	SELECT COUNT ( * ) from none as t1 WHERE t1.directed_by LIKE 10	1-23279434-1
what are the directed by of all none with title that is 10 ?	SELECT t1.directed_by from none as t1 GROUP BY t1.title HAVING COUNT ( * ) = 10	1-23279434-1
find the directed by of the none with the highest series # .	SELECT t1.directed_by from none as t1 ORDER BY t1.series_ DESC LIMIT 1	1-23279434-1
return the production code of none that do not have the title 10 .	SELECT t1.production_code from none as t1 WHERE t1.title ! = 10	1-23279434-1
how many none ' title have the word 10 in them ?	SELECT COUNT ( * ) from none as t1 WHERE t1.title LIKE 10	1-23279434-1
