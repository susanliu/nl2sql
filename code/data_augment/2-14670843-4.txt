list name and where built who have year built greater than 5 or year built shorter than 10 .	SELECT t1.name , t1.where_built from list as t1 WHERE t1.year_built > 5 OR t1.year_built < 10	2-14670843-4
find the name of the list that have just 10 list .	SELECT t1.name from list as t1 GROUP BY t1.name HAVING COUNT ( * ) = 10	2-14670843-4
what are the where built of list with year built above the average year built across all list ?	SELECT t1.where_built from list as t1 WHERE t1.year_built > ( SELECT AVG ( t1.year_built ) from list as t1	2-14670843-4
what are the where built that have greater year built than any year built in list ?	SELECT t1.where_built from list as t1 WHERE t1.year_built > ( SELECT MIN ( t1.year_built ) from list as t1 )	2-14670843-4
which type has the least year built ?	SELECT t1.type from list as t1 ORDER BY t1.year_built ASC LIMIT 1	2-14670843-4
find the distinct name of list having year built between 10 and 34 .	SELECT DISTINCT t1.name from list as t1 WHERE t1.year_built BETWEEN 10 AND 34	2-14670843-4
show all information on the list that has the largest number of initial owners.	SELECT * from list as t1 ORDER BY t1.initial_owners DESC LIMIT 1	2-14670843-4
return the type of the largest year built.	SELECT t1.type from list as t1 ORDER BY t1.year_built DESC LIMIT 1	2-14670843-4
what is the minimum year built in each name ?	SELECT MIN ( t1.year_built ) , t1.name from list as t1 GROUP BY t1.name	2-14670843-4
select the average year built of each list 's where built .	SELECT AVG ( t1.year_built ) , t1.where_built from list as t1 GROUP BY t1.where_built	2-14670843-4
