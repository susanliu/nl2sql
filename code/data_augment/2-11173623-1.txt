find the result and opponent of the 1938 whose week is lower than the average week of all 1938 .	SELECT t1.result , t1.opponent from 1938 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-11173623-1
show all result and corresponding number of 1938 sorted by the count .	SELECT t1.result , COUNT ( * ) from 1938 as t1 GROUP BY t1.result ORDER BY COUNT ( * )	2-11173623-1
what are the result and result of each 1938 , listed in descending order by week ?	SELECT t1.result , t1.result from 1938 as t1 ORDER BY t1.week DESC	2-11173623-1
find the number of 1938 that have more than 10 result .	SELECT COUNT ( * ) from 1938 as t1 GROUP BY t1.result HAVING COUNT ( * ) > 10 	2-11173623-1
which date has both 1938 with less than 10 attendance and 1938 with more than 11 attendance ?	SELECT t1.date from 1938 as t1 WHERE t1.attendance < 10 INTERSECT SELECT t1.date from 1938 as t1 WHERE t1.attendance > 11	2-11173623-1
please show the different date , ordered by the number of 1938 that have each .	SELECT t1.date from 1938 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-11173623-1
please list the date and opponent of 1938 in descending order of week .	SELECT t1.date , t1.opponent from 1938 as t1 ORDER BY t1.week DESC	2-11173623-1
what is the opponent and result of every 1938 that has a week lower than average ?	SELECT t1.opponent , t1.result from 1938 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-11173623-1
what is the count of 1938 with more than 10 opponent ?	SELECT COUNT ( * ) from 1938 as t1 GROUP BY t1.opponent HAVING COUNT ( * ) > 10 	2-11173623-1
find the date of 1938 whose week is higher than the average week .	SELECT t1.date from 1938 as t1 WHERE t1.week > ( SELECT AVG ( t1.week ) from 1938 as t1	2-11173623-1
