find the team of 2002 which are team 10 but not team 65 .	SELECT t1.team from 2002 as t1 WHERE t1.team = 10 EXCEPT SELECT t1.team from 2002 as t1 WHERE t1.team = 65	1-14997324-5
list team and team who have points greater than 5 or points shorter than 10 .	SELECT t1.team , t1.team from 2002 as t1 WHERE t1.points > 5 OR t1.points < 10	1-14997324-5
find the team of 2002 whose points is higher than the average points .	SELECT t1.team from 2002 as t1 WHERE t1.points > ( SELECT AVG ( t1.points ) from 2002 as t1	1-14997324-5
what are the team and team of the {COLUMN} who have wins above five or position below ten ?	SELECT t1.team , t1.team from 2002 as t1 WHERE t1.wins > 5 OR t1.position < 10	1-14997324-5
what is the team and team of the 2002 with maximum position ?	SELECT t1.team , t1.team from 2002 as t1 WHERE t1.position = ( SELECT MAX ( t1.position ) {FROM, 3} )	1-14997324-5
which team has both 2002 with less than 10 position and 2002 with more than 3 position ?	SELECT t1.team from 2002 as t1 WHERE t1.position < 10 INTERSECT SELECT t1.team from 2002 as t1 WHERE t1.position > 3	1-14997324-5
how many 2002 are there that have more than {VALUE},0 team ?	SELECT COUNT ( * ) from 2002 as t1 GROUP BY t1.team HAVING COUNT ( * ) > 10 	1-14997324-5
select the average played of each 2002 's team .	SELECT AVG ( t1.played ) , t1.team from 2002 as t1 GROUP BY t1.team	1-14997324-5
return the smallest draws for every team .	SELECT MIN ( t1.draws ) , t1.team from 2002 as t1 GROUP BY t1.team	1-14997324-5
find the team of the 2002 with the highest scored.	SELECT t1.team from 2002 as t1 WHERE t1.scored = ( SELECT MAX ( t1.scored ) from 2002 as t1 )	1-14997324-5
