show the score and the number of unique to par containing each score .	SELECT t1.score , COUNT ( DISTINCT t1.to_par ) from 1995 as t1 GROUP BY t1.score	2-17162214-5
how many 1995 are there in player 10 or 52 ?	SELECT COUNT ( * ) from 1995 as t1 WHERE t1.player = 10 OR t1.player = 52	2-17162214-5
which score have an average money ( $ ) over 10 ?	SELECT t1.score from 1995 as t1 GROUP BY t1.score HAVING AVG ( t1.money_ ) >= 10	2-17162214-5
show the country and the number of 1995 for each country in the ascending order.	SELECT t1.country , COUNT ( * ) from 1995 as t1 GROUP BY t1.country ORDER BY COUNT ( * )	2-17162214-5
find the to par of 1995 who have both 10 and 93 money ( $ ) .	SELECT t1.to_par from 1995 as t1 WHERE t1.money_ = 10 INTERSECT SELECT t1.to_par from 1995 as t1 WHERE t1.money_ = 93	2-17162214-5
what is the player and score of the 1995 with maximum money ( $ ) ?	SELECT t1.player , t1.score from 1995 as t1 WHERE t1.money_ = ( SELECT MAX ( t1.money_ ) {FROM, 3} )	2-17162214-5
count the number of 1995 in player 10 or 65 .	SELECT COUNT ( * ) from 1995 as t1 WHERE t1.player = 10 OR t1.player = 65	2-17162214-5
find the score for all 1995 who have more than the average money ( $ ) .	SELECT t1.score from 1995 as t1 WHERE t1.money_ > ( SELECT AVG ( t1.money_ ) from 1995 as t1	2-17162214-5
what are the average money ( $ ) of 1995 for different to par ?	SELECT AVG ( t1.money_ ) , t1.to_par from 1995 as t1 GROUP BY t1.to_par	2-17162214-5
what are the score of all 1995 that have 10 or more 1995 ?	SELECT t1.score from 1995 as t1 GROUP BY t1.score HAVING COUNT ( * ) >= 10	2-17162214-5
