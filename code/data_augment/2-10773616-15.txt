what are the distinct COLUMN_NAME,0} of 1954 with crowd higher than any 1954 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.date from 1954 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.date ) from 1954 as t1 WHERE t1.venue = 10 )	2-10773616-15
what are the distinct away team with crowd between 10 and 83 ?	SELECT DISTINCT t1.away_team from 1954 as t1 WHERE t1.crowd BETWEEN 10 AND 83	2-10773616-15
which away team has both 1954 with less than 10 crowd and 1954 with more than 7 crowd ?	SELECT t1.away_team from 1954 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.away_team from 1954 as t1 WHERE t1.crowd > 7	2-10773616-15
find the date of 1954 who have both 10 and 66 crowd .	SELECT t1.date from 1954 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.date from 1954 as t1 WHERE t1.crowd = 66	2-10773616-15
show all away team score and the total crowd for each .	SELECT t1.away_team_score , SUM ( t1.crowd ) from 1954 as t1 GROUP BY t1.away_team_score	2-10773616-15
which home team score has most number of 1954 ?	SELECT t1.home_team_score from 1954 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) DESC LIMIT 1	2-10773616-15
show the date and the number of unique home team score containing each date .	SELECT t1.date , COUNT ( DISTINCT t1.home_team_score ) from 1954 as t1 GROUP BY t1.date	2-10773616-15
what are the average crowd of 1954 , grouped by home team ?	SELECT AVG ( t1.crowd ) , t1.home_team from 1954 as t1 GROUP BY t1.home_team	2-10773616-15
give the maximum and minimum crowd of all 1954 .	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1954 as t1	2-10773616-15
what are the maximum and minimum crowd across all 1954 ?	SELECT MAX ( t1.crowd ) , MIN ( t1.crowd ) from 1954 as t1	2-10773616-15
