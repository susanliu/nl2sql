what are the average ends lost of 2008 , grouped by skip ?	SELECT AVG ( t1.ends_lost ) , t1.skip from 2008 as t1 GROUP BY t1.skip	1-15333005-1
what is the locale and skip of the 2008 with the top 5 smallest l ?	SELECT t1.locale , t1.skip from 2008 as t1 ORDER BY t1.l LIMIT 5	1-15333005-1
find the locale of 2008 which are skip 10 but not skip 80 .	SELECT t1.locale from 2008 as t1 WHERE t1.skip = 10 EXCEPT SELECT t1.locale from 2008 as t1 WHERE t1.skip = 80	1-15333005-1
show the locale , locale , and skip of all the 2008 .	SELECT t1.locale , t1.locale , t1.skip from 2008 as t1	1-15333005-1
list all information about 2008 .	SELECT * FROM 2008	1-15333005-1
find the skip and locale of the 2008 whose w is lower than the average w of all 2008 .	SELECT t1.skip , t1.locale from 2008 as t1 WHERE t1.w < ( SELECT AVG ( t1.w ) {FROM, 3} )	1-15333005-1
which skip have less than 3 in 2008 ?	SELECT t1.skip from 2008 as t1 GROUP BY t1.skip HAVING COUNT ( * ) < 3	1-15333005-1
find the number of 2008 that have more than 10 skip .	SELECT COUNT ( * ) from 2008 as t1 GROUP BY t1.skip HAVING COUNT ( * ) > 10 	1-15333005-1
what is the skip and locale of the 2008 with maximum pf ?	SELECT t1.skip , t1.locale from 2008 as t1 WHERE t1.pf = ( SELECT MAX ( t1.pf ) {FROM, 3} )	1-15333005-1
find the skip of the 2008 which have skip 10 but not 39 .	SELECT t1.skip from 2008 as t1 WHERE t1.skip = 10 EXCEPT SELECT t1.skip from 2008 as t1 WHERE t1.skip = 39	1-15333005-1
