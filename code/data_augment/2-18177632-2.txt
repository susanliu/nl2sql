list the method and location of all danny sorted by round in descending order .	SELECT t1.method , t1.location from danny as t1 ORDER BY t1.round DESC	2-18177632-2
what is the res . of the danny with the largest round ?	SELECT t1.res_ from danny as t1 WHERE t1.round = ( SELECT MAX ( t1.round ) from danny as t1 )	2-18177632-2
show the method and the number of unique time containing each method .	SELECT t1.method , COUNT ( DISTINCT t1.time ) from danny as t1 GROUP BY t1.method	2-18177632-2
what is the location and opponent of the danny with the top 5 smallest round ?	SELECT t1.location , t1.opponent from danny as t1 ORDER BY t1.round LIMIT 5	2-18177632-2
how many danny are there that have more than {VALUE},0 time ?	SELECT COUNT ( * ) from danny as t1 GROUP BY t1.time HAVING COUNT ( * ) > 10 	2-18177632-2
which res . have an average round over 10 ?	SELECT t1.res_ from danny as t1 GROUP BY t1.res_ HAVING AVG ( t1.round ) >= 10	2-18177632-2
find the time of danny which are record 10 but not record 45 .	SELECT t1.time from danny as t1 WHERE t1.record = 10 EXCEPT SELECT t1.time from danny as t1 WHERE t1.record = 45	2-18177632-2
what are the record , time , and method for each danny ?	SELECT t1.record , t1.time , t1.method from danny as t1	2-18177632-2
what is the time and time of the danny with the top 5 smallest round ?	SELECT t1.time , t1.time from danny as t1 ORDER BY t1.round LIMIT 5	2-18177632-2
what are the method of danny whose time is not 10 ?	SELECT t1.method from danny as t1 WHERE t1.time ! = 10	2-18177632-2
