how many distinct date correspond to each record ?	SELECT t1.record , COUNT ( DISTINCT t1.date ) from lillehammer as t1 GROUP BY t1.record	2-15842932-6
how many different sport correspond to each date ?	SELECT t1.date , COUNT ( DISTINCT t1.sport ) from lillehammer as t1 GROUP BY t1.date	2-15842932-6
what are the average time of lillehammer for different nation ?	SELECT AVG ( t1.time ) , t1.nation from lillehammer as t1 GROUP BY t1.nation	2-15842932-6
find the record who has exactly 10 lillehammer .	SELECT t1.record from lillehammer as t1 GROUP BY t1.record HAVING COUNT ( * ) = 10	2-15842932-6
find all sport that have fewer than three in lillehammer .	SELECT t1.sport from lillehammer as t1 GROUP BY t1.sport HAVING COUNT ( * ) < 3	2-15842932-6
which nation have an average time over 10 ?	SELECT t1.nation from lillehammer as t1 GROUP BY t1.nation HAVING AVG ( t1.time ) >= 10	2-15842932-6
what is all the information on the lillehammer with the largest number of athlete ( s ) ?	SELECT * from lillehammer as t1 ORDER BY t1.athlete_ DESC LIMIT 1	2-15842932-6
find the nation of the lillehammer which have nation 10 but not 93 .	SELECT t1.nation from lillehammer as t1 WHERE t1.nation = 10 EXCEPT SELECT t1.nation from lillehammer as t1 WHERE t1.nation = 93	2-15842932-6
count the number of lillehammer in athlete ( s ) 10 or 57 .	SELECT COUNT ( * ) from lillehammer as t1 WHERE t1.athlete_ = 10 OR t1.athlete_ = 57	2-15842932-6
what is the record and athlete ( s ) of every lillehammer that has a time lower than average ?	SELECT t1.record , t1.athlete_ from lillehammer as t1 WHERE t1.time < ( SELECT AVG ( t1.time ) {FROM, 3} )	2-15842932-6
