sort the list of position and position of all 2002 in the descending order of pick # .	SELECT t1.position , t1.position from 2002 as t1 ORDER BY t1.pick_ DESC	2-1026919-3
what is the player and position for the 2002 with the rank 5 smallest pick # ?	SELECT t1.player , t1.position from 2002 as t1 ORDER BY t1.pick_ LIMIT 5	2-1026919-3
what is the affiliation and player of the 2002 with maximum pick # ?	SELECT t1.affiliation , t1.player from 2002 as t1 WHERE t1.pick_ = ( SELECT MAX ( t1.pick_ ) {FROM, 3} )	2-1026919-3
what is the mls team of the 2002 with least number of mls team ?	SELECT t1.mls_team from 2002 as t1 GROUP BY t1.mls_team ORDER BY COUNT ( * ) ASC LIMIT 1	2-1026919-3
show all mls team and the total pick # for each .	SELECT t1.mls_team , SUM ( t1.pick_ ) from 2002 as t1 GROUP BY t1.mls_team	2-1026919-3
what is the mls team and affiliation of the 2002 with the top 5 smallest pick # ?	SELECT t1.mls_team , t1.affiliation from 2002 as t1 ORDER BY t1.pick_ LIMIT 5	2-1026919-3
list the position of {2002 which has number of 2002 greater than 10 .	SELECT t1.position from 2002 as t1 GROUP BY t1.position HAVING COUNT ( * ) > 10	2-1026919-3
how many 2002 have position that contains 10 ?	SELECT COUNT ( * ) from 2002 as t1 WHERE t1.position LIKE 10	2-1026919-3
please list the affiliation and position of 2002 in descending order of pick # .	SELECT t1.affiliation , t1.position from 2002 as t1 ORDER BY t1.pick_ DESC	2-1026919-3
what are the position for all 2002 , and what is the total {pick # for each ?	SELECT t1.position , SUM ( t1.pick_ ) from 2002 as t1 GROUP BY t1.position	2-1026919-3
