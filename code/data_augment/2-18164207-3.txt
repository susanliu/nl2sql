what are the score and country of all 1983 sorted by decreasing money ( $ ) ?	SELECT t1.score , t1.country from 1983 as t1 ORDER BY t1.money_ DESC	2-18164207-3
select the average money ( $ ) of each 1983 's country .	SELECT AVG ( t1.money_ ) , t1.country from 1983 as t1 GROUP BY t1.country	2-18164207-3
what is the player of 1983 with the maximum money ( $ ) across all 1983 ?	SELECT t1.player from 1983 as t1 WHERE t1.money_ = ( SELECT MAX ( t1.money_ ) from 1983 as t1 )	2-18164207-3
what are the score and player ?	SELECT t1.score , t1.player from 1983 as t1	2-18164207-3
how many distinct score correspond to each to par ?	SELECT t1.to_par , COUNT ( DISTINCT t1.score ) from 1983 as t1 GROUP BY t1.to_par	2-18164207-3
what is the country of the most common 1983 in all country ?	SELECT t1.country from 1983 as t1 GROUP BY t1.country ORDER BY COUNT ( * ) DESC LIMIT 1	2-18164207-3
what are the score and place ?	SELECT t1.score , t1.place from 1983 as t1	2-18164207-3
what are the distinct COLUMN_NAME,0} of 1983 with money ( $ ) higher than any 1983 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.player from 1983 as t1 WHERE t1.money_ > ( SELECT MIN ( t1.player ) from 1983 as t1 WHERE t1.country = 10 )	2-18164207-3
list the player of {1983 which has number of 1983 greater than 10 .	SELECT t1.player from 1983 as t1 GROUP BY t1.player HAVING COUNT ( * ) > 10	2-18164207-3
what are the distinct COLUMN_NAME,0} of every 1983 that has a greater money ( $ ) than some 1983 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.to_par from 1983 as t1 WHERE t1.money_ > ( SELECT MIN ( t1.to_par ) from 1983 as t1 WHERE t1.player = 10 )	2-18164207-3
