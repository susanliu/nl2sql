find all cfl team that have fewer than three in 1970 .	SELECT t1.cfl_team from 1970 as t1 GROUP BY t1.cfl_team HAVING COUNT ( * ) < 3	1-26996293-7
what are the maximum and minimum pick # across all 1970 ?	SELECT MAX ( t1.pick_ ) , MIN ( t1.pick_ ) from 1970 as t1	1-26996293-7
what is the college of the 1970 with the smallest pick # ?	SELECT t1.college from 1970 as t1 ORDER BY t1.pick_ ASC LIMIT 1	1-26996293-7
what are the player of 1970 whose position is not 10 ?	SELECT t1.player from 1970 as t1 WHERE t1.position ! = 10	1-26996293-7
how many 1970 are there that have more than {VALUE},0 college ?	SELECT COUNT ( * ) from 1970 as t1 GROUP BY t1.college HAVING COUNT ( * ) > 10 	1-26996293-7
find the distinct COLUMN_NAME,0} of all 1970 that have pick # higher than some 1970 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.player from 1970 as t1 WHERE t1.pick_ > ( SELECT MIN ( t1.player ) from 1970 as t1 WHERE t1.cfl_team = 10 )	1-26996293-7
what is the player of the 1970 with the minimum pick # ?	SELECT t1.player from 1970 as t1 ORDER BY t1.pick_ ASC LIMIT 1	1-26996293-7
what are the cfl team of all 1970 with position that is 10 ?	SELECT t1.cfl_team from 1970 as t1 GROUP BY t1.position HAVING COUNT ( * ) = 10	1-26996293-7
Return all columns in 1970 .	SELECT * FROM 1970	1-26996293-7
list the college which average pick # is above 10 .	SELECT t1.college from 1970 as t1 GROUP BY t1.college HAVING AVG ( t1.pick_ ) >= 10	1-26996293-7
