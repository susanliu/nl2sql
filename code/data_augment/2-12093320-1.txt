find the province of members whose term expires is more than the average term expires of members .	SELECT t1.province from members as t1 WHERE t1.term_expires > ( SELECT AVG ( t1.term_expires ) from members as t1	2-12093320-1
show the name shared by more than 10 members .	SELECT t1.name from members as t1 GROUP BY t1.name HAVING COUNT ( * ) > 10	2-12093320-1
Show everything on members	SELECT * FROM members	2-12093320-1
what is the name of all members whose term expires is higher than any members ?	SELECT t1.name from members as t1 WHERE t1.term_expires > ( SELECT MIN ( t1.term_expires ) from members as t1 )	2-12093320-1
show all information on the members that has the largest number of term of office.	SELECT * from members as t1 ORDER BY t1.term_of_office DESC LIMIT 1	2-12093320-1
which term of office has both members with less than 10 term expires and members with more than 69 term expires ?	SELECT t1.term_of_office from members as t1 WHERE t1.term_expires < 10 INTERSECT SELECT t1.term_of_office from members as t1 WHERE t1.term_expires > 69	2-12093320-1
please show the different province , ordered by the number of members that have each .	SELECT t1.province from members as t1 GROUP BY t1.province ORDER BY COUNT ( * ) ASC LIMIT 1	2-12093320-1
find the name which have exactly 10 members .	SELECT t1.name from members as t1 GROUP BY t1.name HAVING COUNT ( * ) = 10	2-12093320-1
find the distinct party of members having term expires between 10 and 62 .	SELECT DISTINCT t1.party from members as t1 WHERE t1.term_expires BETWEEN 10 AND 62	2-12093320-1
what are the province , party , and name for each members ?	SELECT t1.province , t1.party , t1.name from members as t1	2-12093320-1
