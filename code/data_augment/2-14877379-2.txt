return the date and attendance of 1993 with the five lowest week .	SELECT t1.date , t1.attendance from 1993 as t1 ORDER BY t1.week LIMIT 5	2-14877379-2
Show everything on 1993	SELECT * FROM 1993	2-14877379-2
what is the minimum week in each record ?	SELECT MIN ( t1.week ) , t1.record from 1993 as t1 GROUP BY t1.record	2-14877379-2
list the game site which average week is above 10 .	SELECT t1.game_site from 1993 as t1 GROUP BY t1.game_site HAVING AVG ( t1.week ) >= 10	2-14877379-2
how many 1993 are there in record 10 or 76 ?	SELECT COUNT ( * ) from 1993 as t1 WHERE t1.record = 10 OR t1.record = 76	2-14877379-2
what are the date of all 1993 with record that is 10 ?	SELECT t1.date from 1993 as t1 GROUP BY t1.record HAVING COUNT ( * ) = 10	2-14877379-2
show the game site of the 1993 that has the greatest number of 1993 .	SELECT t1.game_site from 1993 as t1 GROUP BY t1.game_site ORDER BY COUNT ( * ) DESC LIMIT 1	2-14877379-2
how many 1993 are there in date 10 or 19 ?	SELECT COUNT ( * ) from 1993 as t1 WHERE t1.date = 10 OR t1.date = 19	2-14877379-2
find the result which have exactly 10 1993 .	SELECT t1.result from 1993 as t1 GROUP BY t1.result HAVING COUNT ( * ) = 10	2-14877379-2
which result has both 1993 with less than 10 week and 1993 with more than 35 week ?	SELECT t1.result from 1993 as t1 WHERE t1.week < 10 INTERSECT SELECT t1.result from 1993 as t1 WHERE t1.week > 35	2-14877379-2
