find the deaths of the atlantic with the highest year.	SELECT t1.deaths from atlantic as t1 WHERE t1.year = ( SELECT MAX ( t1.year ) from atlantic as t1 )	1-2930244-3
show strongest storm and strongest storm of atlantic .	SELECT t1.strongest_storm , t1.strongest_storm from atlantic as t1	1-2930244-3
find the strongest storm and deaths of the atlantic whose year is lower than the average year of all atlantic .	SELECT t1.strongest_storm , t1.deaths from atlantic as t1 WHERE t1.year < ( SELECT AVG ( t1.year ) {FROM, 3} )	1-2930244-3
what is all the information on the atlantic with the largest number of strongest storm ?	SELECT * from atlantic as t1 ORDER BY t1.strongest_storm DESC LIMIT 1	1-2930244-3
return the strongest storm of the largest number of major hurricanes.	SELECT t1.strongest_storm from atlantic as t1 ORDER BY t1.number_of_major_hurricanes DESC LIMIT 1	1-2930244-3
return the deaths of the atlantic with the fewest number of tropical storms .	SELECT t1.deaths from atlantic as t1 ORDER BY t1.number_of_tropical_storms ASC LIMIT 1	1-2930244-3
what are the deaths and strongest storm of the {COLUMN} who have number of hurricanes above five or number of major hurricanes below ten ?	SELECT t1.deaths , t1.strongest_storm from atlantic as t1 WHERE t1.number_of_hurricanes > 5 OR t1.number_of_major_hurricanes < 10	1-2930244-3
show all information on the atlantic that has the largest number of deaths.	SELECT * from atlantic as t1 ORDER BY t1.deaths DESC LIMIT 1	1-2930244-3
count the number of atlantic in strongest storm 10 or 97 .	SELECT COUNT ( * ) from atlantic as t1 WHERE t1.strongest_storm = 10 OR t1.strongest_storm = 97	1-2930244-3
list the deaths and strongest storm of all atlantic sorted by number of tropical storms in descending order .	SELECT t1.deaths , t1.strongest_storm from atlantic as t1 ORDER BY t1.number_of_tropical_storms DESC	1-2930244-3
