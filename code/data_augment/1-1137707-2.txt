which grand prix has most number of 1990 ?	SELECT t1.grand_prix from 1990 as t1 GROUP BY t1.grand_prix ORDER BY COUNT ( * ) DESC LIMIT 1	1-1137707-2
what are the grand prix , date , and report of each 1990 ?	SELECT t1.grand_prix , t1.date , t1.report from 1990 as t1	1-1137707-2
show the location of 1990 who have at least 10 1990 .	SELECT t1.location from 1990 as t1 GROUP BY t1.location HAVING COUNT ( * ) >= 10	1-1137707-2
what is the winning constructor and report of every 1990 that has a round lower than average ?	SELECT t1.winning_constructor , t1.report from 1990 as t1 WHERE t1.round < ( SELECT AVG ( t1.round ) {FROM, 3} )	1-1137707-2
how many 1990 are there that have more than {VALUE},0 grand prix ?	SELECT COUNT ( * ) from 1990 as t1 GROUP BY t1.grand_prix HAVING COUNT ( * ) > 10 	1-1137707-2
find the location of the 1990 that have just 10 1990 .	SELECT t1.location from 1990 as t1 GROUP BY t1.location HAVING COUNT ( * ) = 10	1-1137707-2
what are the fastest lap of the 1990 that have exactly 10 1990 ?	SELECT t1.fastest_lap from 1990 as t1 GROUP BY t1.fastest_lap HAVING COUNT ( * ) = 10	1-1137707-2
how many distinct grand prix correspond to each location ?	SELECT t1.location , COUNT ( DISTINCT t1.grand_prix ) from 1990 as t1 GROUP BY t1.location	1-1137707-2
Return all columns in 1990 .	SELECT * FROM 1990	1-1137707-2
what is the grand prix and fastest lap for the 1990 with the rank 5 smallest round ?	SELECT t1.grand_prix , t1.fastest_lap from 1990 as t1 ORDER BY t1.round LIMIT 5	1-1137707-2
