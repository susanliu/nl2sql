what is the minimum points in each class ?	SELECT MIN ( t1.points ) , t1.class from jacques as t1 GROUP BY t1.class	2-14890412-3
Return all columns in jacques .	SELECT * FROM jacques	2-14890412-3
return the different rank of jacques , in ascending order of frequency .	SELECT t1.rank from jacques as t1 GROUP BY t1.rank ORDER BY COUNT ( * ) ASC LIMIT 1	2-14890412-3
show all class and corresponding number of jacques in the ascending order of the numbers.	SELECT t1.class , COUNT ( * ) from jacques as t1 GROUP BY t1.class ORDER BY COUNT ( * )	2-14890412-3
show rank for all jacques whose year are greater than the average .	SELECT t1.rank from jacques as t1 WHERE t1.year > ( SELECT AVG ( t1.year ) from jacques as t1	2-14890412-3
return the team and class of jacques with the five lowest year .	SELECT t1.team , t1.class from jacques as t1 ORDER BY t1.year LIMIT 5	2-14890412-3
find the distinct class of jacques having year between 10 and 37 .	SELECT DISTINCT t1.class from jacques as t1 WHERE t1.year BETWEEN 10 AND 37	2-14890412-3
what are the team and rank of the {COLUMN} who have points above five or wins below ten ?	SELECT t1.team , t1.rank from jacques as t1 WHERE t1.points > 5 OR t1.wins < 10	2-14890412-3
which class has both jacques with less than 10 wins and jacques with more than 9 wins ?	SELECT t1.class from jacques as t1 WHERE t1.wins < 10 INTERSECT SELECT t1.class from jacques as t1 WHERE t1.wins > 9	2-14890412-3
please show the different class , ordered by the number of jacques that have each .	SELECT t1.class from jacques as t1 GROUP BY t1.class ORDER BY COUNT ( * ) ASC LIMIT 1	2-14890412-3
