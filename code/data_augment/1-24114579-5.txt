what is the episode # of the none with the smallest trip # ?	SELECT t1.episode_ from none as t1 ORDER BY t1.trip_ ASC LIMIT 1	1-24114579-5
which episode # is the most frequent episode #?	SELECT t1.episode_ from none as t1 GROUP BY t1.episode_ ORDER BY COUNT ( * ) DESC LIMIT 1	1-24114579-5
find the distinct episode # of all none that have a higher trip # than some none with 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.episode_ from none as t1 WHERE t1.trip_ > ( SELECT MIN ( t1.episode_ ) from none as t1 WHERE t1.episode_ = 10 )	1-24114579-5
count the number of none that have an comments containing 10 .	SELECT COUNT ( * ) from none as t1 WHERE t1.comments LIKE 10	1-24114579-5
what is the date aired of the none who has the highest number of none ?	SELECT t1.date_aired from none as t1 GROUP BY t1.date_aired ORDER BY COUNT ( * ) DESC LIMIT 1	1-24114579-5
find the happy sunday episode # of none whose trip # is higher than the average trip # .	SELECT t1.happy_sunday_episode_ from none as t1 WHERE t1.trip_ > ( SELECT AVG ( t1.trip_ ) from none as t1	1-24114579-5
what is the happy sunday episode # of all none whose trip # is higher than any none ?	SELECT t1.happy_sunday_episode_ from none as t1 WHERE t1.trip_ > ( SELECT MIN ( t1.trip_ ) from none as t1 )	1-24114579-5
what is the episode # of the none with the largest trip # ?	SELECT t1.episode_ from none as t1 WHERE t1.trip_ = ( SELECT MAX ( t1.trip_ ) from none as t1 )	1-24114579-5
please list the comments and date aired of none in descending order of trip # .	SELECT t1.comments , t1.date_aired from none as t1 ORDER BY t1.trip_ DESC	1-24114579-5
which episode # is the most frequent episode #?	SELECT t1.episode_ from none as t1 GROUP BY t1.episode_ ORDER BY COUNT ( * ) DESC LIMIT 1	1-24114579-5
