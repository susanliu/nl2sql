what are the distinct COLUMN_NAME,0} of every ludovico that has a greater year than some ludovico with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.engine from ludovico as t1 WHERE t1.year > ( SELECT MIN ( t1.engine ) from ludovico as t1 WHERE t1.engine = 10 )	2-1235774-1
what are total year for each chassis ?	SELECT t1.chassis , SUM ( t1.year ) from ludovico as t1 GROUP BY t1.chassis	2-1235774-1
what are the team and chassis of ludovico with 10 or more engine ?	SELECT t1.team , t1.chassis from ludovico as t1 GROUP BY t1.engine HAVING COUNT ( * ) >= 10	2-1235774-1
what is the engine and team of the ludovico with maximum points ?	SELECT t1.engine , t1.team from ludovico as t1 WHERE t1.points = ( SELECT MAX ( t1.points ) {FROM, 3} )	2-1235774-1
return the chassis and team of ludovico with the five lowest year .	SELECT t1.chassis , t1.team from ludovico as t1 ORDER BY t1.year LIMIT 5	2-1235774-1
which team has the least year ?	SELECT t1.team from ludovico as t1 ORDER BY t1.year ASC LIMIT 1	2-1235774-1
which chassis have an average year over 10 ?	SELECT t1.chassis from ludovico as t1 GROUP BY t1.chassis HAVING AVG ( t1.year ) >= 10	2-1235774-1
find all chassis that have fewer than three in ludovico .	SELECT t1.chassis from ludovico as t1 GROUP BY t1.chassis HAVING COUNT ( * ) < 3	2-1235774-1
which team have greater points than that of any points in ludovico ?	SELECT t1.team from ludovico as t1 WHERE t1.points > ( SELECT MIN ( t1.points ) from ludovico as t1 )	2-1235774-1
sort the list of team and team of all ludovico in the descending order of year .	SELECT t1.team , t1.team from ludovico as t1 ORDER BY t1.year DESC	2-1235774-1
