give the maximum and minimum length , km of all railway .	SELECT MAX ( t1.length_ ) , MIN ( t1.length_ ) from railway as t1	2-16197280-1
what are the distinct COLUMN_NAME,0} of every railway that has a greater length , km than some railway with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.gauge from railway as t1 WHERE t1.length_ > ( SELECT MIN ( t1.gauge ) from railway as t1 WHERE t1.takeover_date = 10 )	2-16197280-1
find the takeover date of the railway who has the largest number of railway .	SELECT t1.takeover_date from railway as t1 GROUP BY t1.takeover_date ORDER BY COUNT ( * ) DESC LIMIT 1	2-16197280-1
which takeover date has both railway with less than 10 length , km and railway with more than 24 length , km ?	SELECT t1.takeover_date from railway as t1 WHERE t1.length_ < 10 INTERSECT SELECT t1.takeover_date from railway as t1 WHERE t1.length_ > 24	2-16197280-1
what are the distinct COLUMN_NAME,0} of every railway that has a greater length , km than some railway with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.fa_division_ from railway as t1 WHERE t1.length_ > ( SELECT MIN ( t1.fa_division_ ) from railway as t1 WHERE t1.concessionaire = 10 )	2-16197280-1
Show everything on railway	SELECT * FROM railway	2-16197280-1
show the gauge , takeover date , and fa division ( s ) of all the railway .	SELECT t1.gauge , t1.takeover_date , t1.fa_division_ from railway as t1	2-16197280-1
list the concessionaire of the railway whose gauge is not 10 .	SELECT t1.concessionaire from railway as t1 WHERE t1.gauge ! = 10	2-16197280-1
which gauge has both railway with less than 10 length , km and railway with more than 52 length , km ?	SELECT t1.gauge from railway as t1 WHERE t1.length_ < 10 INTERSECT SELECT t1.gauge from railway as t1 WHERE t1.length_ > 52	2-16197280-1
what are the fa division ( s ) for all railway , and what is the total {length , km for each ?	SELECT t1.fa_division_ , SUM ( t1.length_ ) from railway as t1 GROUP BY t1.fa_division_	2-16197280-1
