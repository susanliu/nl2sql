which result have an average week over 10 ?	SELECT t1.result from 1990 as t1 GROUP BY t1.result HAVING AVG ( t1.week ) >= 10	2-16678071-2
Show everything on 1990	SELECT * FROM 1990	2-16678071-2
what are the result of the 1990 that have exactly 10 1990 ?	SELECT t1.result from 1990 as t1 GROUP BY t1.result HAVING COUNT ( * ) = 10	2-16678071-2
list result and opponent who have week greater than 5 or week shorter than 10 .	SELECT t1.result , t1.opponent from 1990 as t1 WHERE t1.week > 5 OR t1.week < 10	2-16678071-2
what is the date and attendance of the 1990 with maximum week ?	SELECT t1.date , t1.attendance from 1990 as t1 WHERE t1.week = ( SELECT MAX ( t1.week ) {FROM, 3} )	2-16678071-2
what is the attendance and result of every 1990 that has a week lower than average ?	SELECT t1.attendance , t1.result from 1990 as t1 WHERE t1.week < ( SELECT AVG ( t1.week ) {FROM, 3} )	2-16678071-2
what are the date of 1990 , sorted by their frequency?	SELECT t1.date from 1990 as t1 GROUP BY t1.date ORDER BY COUNT ( * ) ASC LIMIT 1	2-16678071-2
list all information about 1990 .	SELECT * FROM 1990	2-16678071-2
what are the result and date of the {COLUMN} who have week above five or week below ten ?	SELECT t1.result , t1.date from 1990 as t1 WHERE t1.week > 5 OR t1.week < 10	2-16678071-2
which attendance have greater week than that of any week in 1990 ?	SELECT t1.attendance from 1990 as t1 WHERE t1.week > ( SELECT MIN ( t1.week ) from 1990 as t1 )	2-16678071-2
