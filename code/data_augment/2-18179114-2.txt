what is the date of the list with the largest against ?	SELECT t1.date from list as t1 WHERE t1.against = ( SELECT MAX ( t1.against ) from list as t1 )	2-18179114-2
what is the venue of list with the maximum against across all list ?	SELECT t1.venue from list as t1 WHERE t1.against = ( SELECT MAX ( t1.against ) from list as t1 )	2-18179114-2
find the number of list that have more than 10 status .	SELECT COUNT ( * ) from list as t1 GROUP BY t1.status HAVING COUNT ( * ) > 10 	2-18179114-2
return the smallest against for every date .	SELECT MIN ( t1.against ) , t1.date from list as t1 GROUP BY t1.date	2-18179114-2
count the number of list in opposing teams 10 or 70 .	SELECT COUNT ( * ) from list as t1 WHERE t1.opposing_teams = 10 OR t1.opposing_teams = 70	2-18179114-2
what are total against for each venue ?	SELECT t1.venue , SUM ( t1.against ) from list as t1 GROUP BY t1.venue	2-18179114-2
what is the date and status for the list with the rank 5 smallest against ?	SELECT t1.date , t1.status from list as t1 ORDER BY t1.against LIMIT 5	2-18179114-2
find the opposing teams of list who have more than 10 list .	SELECT t1.opposing_teams from list as t1 GROUP BY t1.opposing_teams HAVING COUNT ( * ) > 10	2-18179114-2
show the status and the number of list for each status in the ascending order.	SELECT t1.status , COUNT ( * ) from list as t1 GROUP BY t1.status ORDER BY COUNT ( * )	2-18179114-2
which t1.venue has least number of list ?	SELECT t1.venue from list as t1 GROUP BY t1.venue ORDER BY COUNT ( * ) LIMIT 1	2-18179114-2
