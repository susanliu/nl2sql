list constructor and constructor who have grid greater than 5 or grid shorter than 10 .	SELECT t1.constructor , t1.constructor from 2004 as t1 WHERE t1.grid > 5 OR t1.grid < 10	2-1123631-2
show the time/retired and the number of unique constructor containing each time/retired .	SELECT t1.time/retired , COUNT ( DISTINCT t1.constructor ) from 2004 as t1 GROUP BY t1.time/retired	2-1123631-2
select the average grid of each 2004 's time/retired .	SELECT AVG ( t1.grid ) , t1.time/retired from 2004 as t1 GROUP BY t1.time/retired	2-1123631-2
return the constructor of the 2004 with the fewest grid .	SELECT t1.constructor from 2004 as t1 ORDER BY t1.grid ASC LIMIT 1	2-1123631-2
show the time/retired and the total grid of 2004 .	SELECT t1.time/retired , SUM ( t1.grid ) from 2004 as t1 GROUP BY t1.time/retired	2-1123631-2
what is the constructor of highest laps ?	SELECT t1.constructor from 2004 as t1 ORDER BY t1.laps DESC LIMIT 1	2-1123631-2
find the time/retired of the 2004 which have time/retired 10 but not 81 .	SELECT t1.time/retired from 2004 as t1 WHERE t1.time/retired = 10 EXCEPT SELECT t1.time/retired from 2004 as t1 WHERE t1.time/retired = 81	2-1123631-2
please show the constructor of the 2004 that have at least 10 records .	SELECT t1.constructor from 2004 as t1 GROUP BY t1.constructor HAVING COUNT ( * ) >= 10	2-1123631-2
which driver is the most frequent driver?	SELECT t1.driver from 2004 as t1 GROUP BY t1.driver ORDER BY COUNT ( * ) DESC LIMIT 1	2-1123631-2
what are the driver for 2004 who have more than the average grid?	SELECT t1.driver from 2004 as t1 WHERE t1.grid > ( SELECT AVG ( t1.grid ) from 2004 as t1	2-1123631-2
