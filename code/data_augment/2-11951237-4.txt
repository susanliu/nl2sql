what are the directed by of all list with original air date that is 10 ?	SELECT t1.directed_by from list as t1 GROUP BY t1.original_air_date HAVING COUNT ( * ) = 10	2-11951237-4
how many list ' title have the word 10 in them ?	SELECT COUNT ( * ) from list as t1 WHERE t1.title LIKE 10	2-11951237-4
find the title of list whose series # is more than the average series # of list .	SELECT t1.title from list as t1 WHERE t1.series_ > ( SELECT AVG ( t1.series_ ) from list as t1	2-11951237-4
what is the count of list with more than 10 directed by ?	SELECT COUNT ( * ) from list as t1 GROUP BY t1.directed_by HAVING COUNT ( * ) > 10 	2-11951237-4
return the smallest season # for every production code .	SELECT MIN ( t1.season_ ) , t1.production_code from list as t1 GROUP BY t1.production_code	2-11951237-4
find the original air date of list which have 10 but no 66 as original air date .	SELECT t1.original_air_date from list as t1 WHERE t1.original_air_date = 10 EXCEPT SELECT t1.original_air_date from list as t1 WHERE t1.original_air_date = 66	2-11951237-4
what are the original air date and production code of the {COLUMN} who have season # above five or series # below ten ?	SELECT t1.original_air_date , t1.production_code from list as t1 WHERE t1.season_ > 5 OR t1.series_ < 10	2-11951237-4
what are the title and title of the {COLUMN} who have series # above five or series # below ten ?	SELECT t1.title , t1.title from list as t1 WHERE t1.series_ > 5 OR t1.series_ < 10	2-11951237-4
what is the title of the list who has the highest number of list ?	SELECT t1.title from list as t1 GROUP BY t1.title ORDER BY COUNT ( * ) DESC LIMIT 1	2-11951237-4
find the title of list who have more than 10 list .	SELECT t1.title from list as t1 GROUP BY t1.title HAVING COUNT ( * ) > 10	2-11951237-4
