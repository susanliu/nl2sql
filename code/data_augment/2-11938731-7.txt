find the player and player of the 1987 whose solo is lower than the average solo of all 1987 .	SELECT t1.player , t1.player from 1987 as t1 WHERE t1.solo < ( SELECT AVG ( t1.solo ) {FROM, 3} )	2-11938731-7
how many 1987 are there that have more than {VALUE},0 player ?	SELECT COUNT ( * ) from 1987 as t1 GROUP BY t1.player HAVING COUNT ( * ) > 10 	2-11938731-7
what are the distinct player with assisted between 10 and 44 ?	SELECT DISTINCT t1.player from 1987 as t1 WHERE t1.assisted BETWEEN 10 AND 44	2-11938731-7
what is the player of the 1987 with least number of player ?	SELECT t1.player from 1987 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) ASC LIMIT 1	2-11938731-7
what are the distinct COLUMN_NAME,0} of every 1987 that has a greater assisted than some 1987 with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.player from 1987 as t1 WHERE t1.assisted > ( SELECT MIN ( t1.player ) from 1987 as t1 WHERE t1.player = 10 )	2-11938731-7
show the player of the 1987 that has the most 1987 .	SELECT t1.player from 1987 as t1 GROUP BY t1.player ORDER BY COUNT ( * ) DESC LIMIT 1	2-11938731-7
show player and the number of distinct player in each player .	SELECT t1.player , COUNT ( DISTINCT t1.player ) from 1987 as t1 GROUP BY t1.player	2-11938731-7
list all information about 1987 .	SELECT * FROM 1987	2-11938731-7
what is the player and player of every 1987 that has a td 's lower than average ?	SELECT t1.player , t1.player from 1987 as t1 WHERE t1.td_ < ( SELECT AVG ( t1.td_ ) {FROM, 3} )	2-11938731-7
which player have less than 3 in 1987 ?	SELECT t1.player from 1987 as t1 GROUP BY t1.player HAVING COUNT ( * ) < 3	2-11938731-7
