find the olympics of list whose total ( min . 2 medals ) is more than the average total ( min . 2 medals ) of list .	SELECT t1.olympics from list as t1 WHERE t1.total_ > ( SELECT AVG ( t1.total_ ) from list as t1	1-22355-65
show all nation and corresponding number of list in the ascending order of the numbers.	SELECT t1.nation , COUNT ( * ) from list as t1 GROUP BY t1.nation ORDER BY COUNT ( * )	1-22355-65
find the distinct athlete of list having gold between 10 and 83 .	SELECT DISTINCT t1.athlete from list as t1 WHERE t1.gold BETWEEN 10 AND 83	1-22355-65
list the athlete , nation and the athlete of the list .	SELECT t1.athlete , t1.nation , t1.athlete from list as t1	1-22355-65
what is the athlete of the list who has the highest number of list ?	SELECT t1.athlete from list as t1 GROUP BY t1.athlete ORDER BY COUNT ( * ) DESC LIMIT 1	1-22355-65
how many list are there in athlete 10 or 40 ?	SELECT COUNT ( * ) from list as t1 WHERE t1.athlete = 10 OR t1.athlete = 40	1-22355-65
what are the average gold of list , grouped by athlete ?	SELECT AVG ( t1.gold ) , t1.athlete from list as t1 GROUP BY t1.athlete	1-22355-65
find the athlete of list who have more than 10 list .	SELECT t1.athlete from list as t1 GROUP BY t1.athlete HAVING COUNT ( * ) > 10	1-22355-65
what are the nation and nation of each list , listed in descending order by rank ?	SELECT t1.nation , t1.nation from list as t1 ORDER BY t1.rank DESC	1-22355-65
return the smallest silver for every olympics .	SELECT MIN ( t1.silver ) , t1.olympics from list as t1 GROUP BY t1.olympics	1-22355-65
