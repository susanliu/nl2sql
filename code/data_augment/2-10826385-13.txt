what is the date and away team score of every 1972 that has a crowd lower than average ?	SELECT t1.date , t1.away_team_score from 1972 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	2-10826385-13
what is the home team and venue of the 1972 with maximum crowd ?	SELECT t1.home_team , t1.venue from 1972 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) {FROM, 3} )	2-10826385-13
count the number of 1972 in home team 10 or 46 .	SELECT COUNT ( * ) from 1972 as t1 WHERE t1.home_team = 10 OR t1.home_team = 46	2-10826385-13
find the venue of 1972 which have 10 but no 38 as date .	SELECT t1.venue from 1972 as t1 WHERE t1.date = 10 EXCEPT SELECT t1.venue from 1972 as t1 WHERE t1.date = 38	2-10826385-13
what is the count of 1972 with more than 10 away team ?	SELECT COUNT ( * ) from 1972 as t1 GROUP BY t1.away_team HAVING COUNT ( * ) > 10 	2-10826385-13
which home team score have an average crowd over 10 ?	SELECT t1.home_team_score from 1972 as t1 GROUP BY t1.home_team_score HAVING AVG ( t1.crowd ) >= 10	2-10826385-13
Return all columns in 1972 .	SELECT * FROM 1972	2-10826385-13
what is the away team of the 1972 with the smallest crowd ?	SELECT t1.away_team from 1972 as t1 ORDER BY t1.crowd ASC LIMIT 1	2-10826385-13
find the distinct COLUMN_NAME,0} of all 1972 that have crowd higher than some 1972 from 10 {COLUMN_NAME_2} .	SELECT DISTINCT t1.away_team_score from 1972 as t1 WHERE t1.crowd > ( SELECT MIN ( t1.away_team_score ) from 1972 as t1 WHERE t1.away_team = 10 )	2-10826385-13
what are the away team score and date of all 1972 sorted by decreasing crowd ?	SELECT t1.away_team_score , t1.date from 1972 as t1 ORDER BY t1.crowd DESC	2-10826385-13
