find the place who has exactly 10 1940 .	SELECT t1.place from 1940 as t1 GROUP BY t1.place HAVING COUNT ( * ) = 10	2-12641884-1
return the smallest money ( $ ) for every country .	SELECT MIN ( t1.money_ ) , t1.country from 1940 as t1 GROUP BY t1.country	2-12641884-1
sort the list of to par and player of all 1940 in the descending order of money ( $ ) .	SELECT t1.to_par , t1.player from 1940 as t1 ORDER BY t1.money_ DESC	2-12641884-1
what is the maximum and mininum money ( $ ) {COLUMN} for all 1940 ?	SELECT MAX ( t1.money_ ) , MIN ( t1.money_ ) from 1940 as t1	2-12641884-1
what is the to par of the 1940 with the minimum money ( $ ) ?	SELECT t1.to_par from 1940 as t1 ORDER BY t1.money_ ASC LIMIT 1	2-12641884-1
what are the player and place of the {COLUMN} who have money ( $ ) above five or money ( $ ) below ten ?	SELECT t1.player , t1.place from 1940 as t1 WHERE t1.money_ > 5 OR t1.money_ < 10	2-12641884-1
show the player and the number of unique country containing each player .	SELECT t1.player , COUNT ( DISTINCT t1.country ) from 1940 as t1 GROUP BY t1.player	2-12641884-1
return the smallest money ( $ ) for every place .	SELECT MIN ( t1.money_ ) , t1.place from 1940 as t1 GROUP BY t1.place	2-12641884-1
what are the score and country of all 1940 sorted by decreasing money ( $ ) ?	SELECT t1.score , t1.country from 1940 as t1 ORDER BY t1.money_ DESC	2-12641884-1
what is the place of the 1940 who has the highest number of 1940 ?	SELECT t1.place from 1940 as t1 GROUP BY t1.place ORDER BY COUNT ( * ) DESC LIMIT 1	2-12641884-1
