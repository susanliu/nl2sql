find the home team and away team of the 1959 with at least 10 away team score .	SELECT t1.home_team , t1.away_team from 1959 as t1 GROUP BY t1.away_team_score HAVING COUNT ( * ) >= 10	2-10775038-7
what are the distinct home team with crowd between 10 and 39 ?	SELECT DISTINCT t1.home_team from 1959 as t1 WHERE t1.crowd BETWEEN 10 AND 39	2-10775038-7
how many distinct home team correspond to each venue ?	SELECT t1.venue , COUNT ( DISTINCT t1.home_team ) from 1959 as t1 GROUP BY t1.venue	2-10775038-7
what are the {home team score of all the 1959 , and the total crowd by each ?	SELECT t1.home_team_score , SUM ( t1.crowd ) from 1959 as t1 GROUP BY t1.home_team_score	2-10775038-7
Return all columns in 1959 .	SELECT * FROM 1959	2-10775038-7
what are the away team score , date , and away team for each 1959 ?	SELECT t1.away_team_score , t1.date , t1.away_team from 1959 as t1	2-10775038-7
how many 1959 ' away team score have the word 10 in them ?	SELECT COUNT ( * ) from 1959 as t1 WHERE t1.away_team_score LIKE 10	2-10775038-7
find the venue of 1959 which have both 10 and 72 as venue .	SELECT t1.venue from 1959 as t1 WHERE t1.crowd = 10 INTERSECT SELECT t1.venue from 1959 as t1 WHERE t1.crowd = 72	2-10775038-7
show venue and home team score of 1959 .	SELECT t1.venue , t1.home_team_score from 1959 as t1	2-10775038-7
what are the venue of the 1959 that have exactly 10 1959 ?	SELECT t1.venue from 1959 as t1 GROUP BY t1.venue HAVING COUNT ( * ) = 10	2-10775038-7
