select the average enrollment of each indiana 's ihsaa class .	SELECT AVG ( t1.enrollment ) , t1.ihsaa_class from indiana as t1 GROUP BY t1.ihsaa_class	2-18974097-12
show the # / county with fewer than 3 indiana .	SELECT t1.# from indiana as t1 GROUP BY t1.# HAVING COUNT ( * ) < 3	2-18974097-12
show the ihsaa class and the corresponding number of indiana sorted by the number of ihsaa class in ascending order .	SELECT t1.ihsaa_class , COUNT ( * ) from indiana as t1 GROUP BY t1.ihsaa_class ORDER BY COUNT ( * )	2-18974097-12
find the location that have 10 indiana .	SELECT t1.location from indiana as t1 GROUP BY t1.location HAVING COUNT ( * ) = 10	2-18974097-12
find the location which have exactly 10 indiana .	SELECT t1.location from indiana as t1 GROUP BY t1.location HAVING COUNT ( * ) = 10	2-18974097-12
how many indiana have location that contains 10 ?	SELECT COUNT ( * ) from indiana as t1 WHERE t1.location LIKE 10	2-18974097-12
list ihsaa class and mascot who have enrollment greater than 5 or enrollment shorter than 10 .	SELECT t1.ihsaa_class , t1.mascot from indiana as t1 WHERE t1.enrollment > 5 OR t1.enrollment < 10	2-18974097-12
show ihsaa class and the number of distinct ihsaa football class in each ihsaa class .	SELECT t1.ihsaa_class , COUNT ( DISTINCT t1.ihsaa_football_class ) from indiana as t1 GROUP BY t1.ihsaa_class	2-18974097-12
show the ihsaa football class and the total enrollment of indiana .	SELECT t1.ihsaa_football_class , SUM ( t1.enrollment ) from indiana as t1 GROUP BY t1.ihsaa_football_class	2-18974097-12
which school have an average enrollment over 10 ?	SELECT t1.school from indiana as t1 GROUP BY t1.school HAVING AVG ( t1.enrollment ) >= 10	2-18974097-12
