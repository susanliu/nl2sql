find the state and institution of the midwest whose enrollment is lower than the average enrollment of all midwest .	SELECT t1.state , t1.institution from midwest as t1 WHERE t1.enrollment < ( SELECT AVG ( t1.enrollment ) {FROM, 3} )	1-27961684-1
list team name of midwest that have the number of midwest greater than 10 .	SELECT t1.team_name from midwest as t1 GROUP BY t1.team_name HAVING COUNT ( * ) > 10	1-27961684-1
what are the team name of midwest with enrollment above the average enrollment across all midwest ?	SELECT t1.team_name from midwest as t1 WHERE t1.enrollment > ( SELECT AVG ( t1.enrollment ) from midwest as t1	1-27961684-1
find the city that have 10 midwest .	SELECT t1.city from midwest as t1 GROUP BY t1.city HAVING COUNT ( * ) = 10	1-27961684-1
what is the home conference of the midwest with least number of home conference ?	SELECT t1.home_conference from midwest as t1 GROUP BY t1.home_conference ORDER BY COUNT ( * ) ASC LIMIT 1	1-27961684-1
display the team name , affiliation , and home conference for each midwest .	SELECT t1.team_name , t1.affiliation , t1.home_conference from midwest as t1	1-27961684-1
what is the institution and home conference of every midwest that has a enrollment lower than average ?	SELECT t1.institution , t1.home_conference from midwest as t1 WHERE t1.enrollment < ( SELECT AVG ( t1.enrollment ) {FROM, 3} )	1-27961684-1
return the team name and home conference of midwest with the five lowest enrollment .	SELECT t1.team_name , t1.home_conference from midwest as t1 ORDER BY t1.enrollment LIMIT 5	1-27961684-1
which institution has most number of midwest ?	SELECT t1.institution from midwest as t1 GROUP BY t1.institution ORDER BY COUNT ( * ) DESC LIMIT 1	1-27961684-1
what is the city and home conference for the midwest with the rank 5 smallest enrollment ?	SELECT t1.city , t1.home_conference from midwest as t1 ORDER BY t1.enrollment LIMIT 5	1-27961684-1
