what are the distinct COLUMN_NAME,0} of every formula that has a greater rank than some formula with 10 {COLUMN_NAME_2} ?	SELECT DISTINCT t1.engine from formula as t1 WHERE t1.rank > ( SELECT MIN ( t1.engine ) from formula as t1 WHERE t1.engine = 10 )	2-11034903-1
what are the average rank of formula for different first win ?	SELECT AVG ( t1.rank ) , t1.first_win from formula as t1 GROUP BY t1.first_win	2-11034903-1
return the smallest wins for every engine .	SELECT MIN ( t1.wins ) , t1.engine from formula as t1 GROUP BY t1.engine	2-11034903-1
what is all the information on the formula with the largest number of latest win ?	SELECT * from formula as t1 ORDER BY t1.latest_win DESC LIMIT 1	2-11034903-1
what are the first win and first win ?	SELECT t1.first_win , t1.first_win from formula as t1	2-11034903-1
find the engine of formula who have rank of both 10 and 50 .	SELECT t1.engine from formula as t1 WHERE t1.rank = 10 INTERSECT SELECT t1.engine from formula as t1 WHERE t1.rank = 50	2-11034903-1
return the first win and engine of formula with the five lowest rank .	SELECT t1.first_win , t1.engine from formula as t1 ORDER BY t1.rank LIMIT 5	2-11034903-1
what is the engine and latest win of the formula with maximum wins ?	SELECT t1.engine , t1.latest_win from formula as t1 WHERE t1.wins = ( SELECT MAX ( t1.wins ) {FROM, 3} )	2-11034903-1
what are the latest win of all formula that have 10 or more formula ?	SELECT t1.latest_win from formula as t1 GROUP BY t1.latest_win HAVING COUNT ( * ) >= 10	2-11034903-1
what are the maximum and minimum wins across all formula ?	SELECT MAX ( t1.wins ) , MIN ( t1.wins ) from formula as t1	2-11034903-1
