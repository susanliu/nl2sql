find the home team of 1999 which are in 10 time but not in 78 time .	SELECT t1.home_team from 1999 as t1 WHERE t1.time = 10 EXCEPT SELECT t1.home_team from 1999 as t1 WHERE t1.time = 78	1-16388357-1
which away team has both 1999 with less than 10 crowd and 1999 with more than 4 crowd ?	SELECT t1.away_team from 1999 as t1 WHERE t1.crowd < 10 INTERSECT SELECT t1.away_team from 1999 as t1 WHERE t1.crowd > 4	1-16388357-1
what are the away team score of all 1999 with time that is 10 ?	SELECT t1.away_team_score from 1999 as t1 GROUP BY t1.time HAVING COUNT ( * ) = 10	1-16388357-1
how many distinct time correspond to each home team score ?	SELECT t1.home_team_score , COUNT ( DISTINCT t1.time ) from 1999 as t1 GROUP BY t1.home_team_score	1-16388357-1
find the distinct home team score of 1999 having crowd between 10 and 28 .	SELECT DISTINCT t1.home_team_score from 1999 as t1 WHERE t1.crowd BETWEEN 10 AND 28	1-16388357-1
what is the away team of 1999 with the maximum crowd across all 1999 ?	SELECT t1.away_team from 1999 as t1 WHERE t1.crowd = ( SELECT MAX ( t1.crowd ) from 1999 as t1 )	1-16388357-1
return the home team score of the 1999 that has the fewest corresponding home team score .	SELECT t1.home_team_score from 1999 as t1 GROUP BY t1.home_team_score ORDER BY COUNT ( * ) ASC LIMIT 1	1-16388357-1
what is the away team score and date of every 1999 that has a crowd lower than average ?	SELECT t1.away_team_score , t1.date from 1999 as t1 WHERE t1.crowd < ( SELECT AVG ( t1.crowd ) {FROM, 3} )	1-16388357-1
list all information about 1999 .	SELECT * FROM 1999	1-16388357-1
what are the home team and home team ?	SELECT t1.home_team , t1.home_team from 1999 as t1	1-16388357-1
