#!/bin/bash

export CUDA_VISIBLE_DEVICES=2,3
TEST_DATA="$1/test.json"

# # full + aug
SAVE_PATH="../model_output/generated_data/saved_models/"

#SAVE_PATH=generated_datasets/generated_data/saved_models_hs=full_tbl=std
python -W ignore test.py \
    --test_data_path  ${TEST_DATA} \
    --models          ${SAVE_PATH} \
    --output_path     ${SAVE_PATH}/test_result.txt \
    --history_type    full \
    --test_mode       $2 \
    --constraint_mode $3 \
    --table_type      std  # 2>&1 | tee "${SAVE_PATH}/dev_result.out.txt"
  #  2>&1 | tee "${SAVE_PATH}/dev_result.out.txt"
## - aug
#SAVE_PATH="generated_datasets/generated_data/saved_models_hs=full_tbl=std_2018-12-04-15:03:34/"
#python test.py \
#    --test_data_path  ${TEST_DATA} \
#    --models          ${SAVE_PATH} \
#    --output_path     ${SAVE_PATH}/dev_result.txt \
#    --history_type    full \
#    --table_type      std \
#     > ${SAVE_PATH}/dev_result.out.txt 2>&1 &
#
#
## - aug - table
#SAVE_PATH=generated_datasets/generated_data/saved_models_hs=full_tbl=no
#python test.py \
#    --test_data_path  ${TEST_DATA} \
#    --models          ${SAVE_PATH} \
#    --output_path     ${SAVE_PATH}/dev_result.txt \
#    --history_type    full \
#    --table_type      no \
#     > ${SAVE_PATH}/dev_result.out.txt 2>&1 &
#
#
## - aug - table - history
#SAVE_PATH=generated_datasets/generated_data/saved_models_hs=no_tbl=no
#python test.py \
#    --test_data_path  ${TEST_DATA} \
#    --models          ${SAVE_PATH} \
#    --output_path     ${SAVE_PATH}/dev_result.txt \
#    --history_type    no \
#    --table_type      no \
#     > ${SAVE_PATH}/dev_result.out.txt 2>&1 &
