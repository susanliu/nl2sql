import json,re

jsondata = json.loads(open("../newdata/dev.json").read())

fin = open("../newdata/test_gold.sql")
fin2 = open("../model_output/generated_data/saved_models/test_result.txt")
fout = open("test_goldtest.txt", "w")

for i in range(0, 1344):
	line = fin.readline()
	line2 = fin2.readline()
	questioni = jsondata[i]["question"]
	line2 = line2.replace("select ", "SELECT ")
	line2 = line2.replace(" from ", " FROM ")
	line2 = line2.replace(" order by ", " ORDER BY ")
	line2 = line2.replace(" group by ", " GROUP BY ")
	line2 = line2.replace(" having ", " HAVING ")
	line2 = line2.replace(" where ", " WHERE ")
	line2 = line2.replace(" join ", " JOIN ")
	line2 = line2.replace(" between ", " BETWEEN ")
	line2 = line2.replace(" and ", " AND ")
	line2 = line2.replace(" or ", " OR ")
	line2 = line2.replace(" like ", " LIKE ")
	line2 = line2.replace(" desc ", " DESC ")
	line2 = line2.replace(" asc ", " ASC ")
	line2 = line2.replace(" limit ", " LIMIT ")
	questioni = re.sub("[^\x00-\x7F]+", " ", questioni)
	questioni = re.sub("\s+", " ", questioni)
	fout.write(str(questioni) + "\n" + line + line2 + "\n")

fout.close()
