import json
import torch
import datetime
import argparse
import numpy as np
from utils import *
from word_embedding import WordEmbedding
from models.agg_predictor import AggPredictor
from models.col_predictor import ColPredictor
from models.desasc_limit_predictor import DesAscLimitPredictor
from models.having_predictor import HavingPredictor
from models.keyword_predictor import KeyWordPredictor
from models.multisql_predictor import MultiSqlPredictor
from models.op_predictor import OpPredictor
from models.root_teminal_predictor import RootTeminalPredictor
from models.andor_predictor import AndOrPredictor
#from supermodel import SuperModel

if __name__ == '__main__':
    SQL_TOK = ['<UNK>', '<END>', 'WHERE', 'AND', 'EQL', 'GT', 'LT', '<BEG>']
    parser = argparse.ArgumentParser()
    parser.add_argument('--train_emb', action='store_true',
            help='Train word embedding.')
    parser.add_argument('--toy', action='store_true',
                        help='If set, use small data; used for fast debugging.')
    parser.add_argument('--models', type=str, help='path to saved model')
    parser.add_argument('--test_component',type=str,default='',
                        help='set test components,available:[multi_sql,keyword,col,op,agg,root_tem,des_asc,having,andor]')
    parser.add_argument('--data_root',type=str)
    parser.add_argument('--output_path', type=str)
    parser.add_argument('--history_type', type=str, default='full', choices=['full','part','no'], help='full, part, or no history')
    parser.add_argument('--table_type', type=str, default='std', choices=['std','hier','no'], help='standard, hierarchical, or no table info')
    parser.add_argument("--test_mode", type=str, default="all",  help="")
    parser.add_argument("--constraint_mode", type=str, default="all", help="")
    args = parser.parse_args()
    use_hs = True
    if args.history_type == "no":
        args.history_type = "full"
        use_hs = False

    N_word=300
    B_word=42
    N_h = 300
    N_depth=2
    # if args.part:
    #     part = True
    # else:
    #     part = False
    if args.toy:
        USE_SMALL=True
        GPU=True
        BATCH_SIZE=2 #20
    else:
        USE_SMALL=False
        GPU=True
        BATCH_SIZE = 2
    # TRAIN_ENTRY=(False, True, False)  # (AGG, SEL, COND)
    # TRAIN_AGG, TRAIN_SEL, TRAIN_COND = TRAIN_ENTRY
    learning_rate = 1e-4

    #TODO
    test_data = load_train_dev_dataset(args.test_component, "test", args.history_type, args.data_root)

    word_emb = load_word_emb('../SQLNet/glove/glove.%dB.%dd.txt'%(B_word,N_word), \
            load_used=args.train_emb, use_small=USE_SMALL)
    embed_layer = WordEmbedding(word_emb, N_word, gpu=GPU,
                                SQL_TOK=SQL_TOK, trainable=args.train_emb)

    print "Loading from modules..."
    model = None
    if args.test_component == "multi_sql":
        model = MultiSqlPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/multi_sql_models.dump".format(args.models)))
    elif args.test_component == "keyword":
        model = KeyWordPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/keyword_models.dump".format(args.models)))
    elif args.test_component == "col":
        model = ColPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/col_models.dump".format(args.models)))
    elif args.test_component == "op":
        model = OpPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/op_models.dump".format(args.models)))
    elif args.test_component == "agg":
        model = AggPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/agg_models.dump".format(args.models)))
    elif args.test_component == "root_tem":
        model = RootTeminalPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/root_tem_models.dump".format(args.models)))
    elif args.test_component == "des_asc":
        model = DesAscLimitPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/des_asc_models.dump".format(args.models)))
    elif args.test_component == "having":
        model = HavingPredictor(N_word=N_word,N_h=N_h,N_depth=N_depth,gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/having_models.dump".format(args.models)))
    elif args.test_component == "andor":
        model = AndOrPredictor(N_word=N_word, N_h=N_h, N_depth=N_depth, gpu=GPU, use_hs=use_hs)
        model.load_state_dict(torch.load("{}/andor_models.dump".format(args.models)))

    acc = epoch_acc(model, BATCH_SIZE, args.test_component,embed_layer,test_data,table_type=args.table_type)
    print acc


    #test_exec_acc()
