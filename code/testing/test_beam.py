import numpy as np
import sys,re
import torch
import json

def load_word_emb(file_name, use_small=False):
        print ('Loading word embedding from %s'%file_name)
        ret = {}
        with open(file_name) as inf:
            for idx, line in enumerate(inf):
                if (use_small and idx >= 5000):
                    break
                info = line.strip().split(' ')
                if info[0].lower() not in ret:
                    ret[info[0]] = np.array(map(lambda x:float(x), info[1:]))
        return ret

def get_table_dict(table_data_path):
    data = json.load(open(table_data_path))
    table = dict()
    for item in data:
        table[item["db_id"]] = item
    return table

B_word = 42
N_word = 300

model_path = "../../model_output/generated_data/saved_models/"
test_data_path = "../newdata_augment/test.json"

word_emb = load_word_emb('../../SQLNet/glove/glove.42B.300d.txt',use_small=False)

sys.path.insert(0, "../")

from supermodel import SuperModel

model = SuperModel(word_emb, N_word=300, gpu=True, trainable_emb = False, table_type="std", use_hs=True)

print "Loading from modules..."
model.multi_sql.load_state_dict(torch.load("{}/multi_sql_models.dump".format(model_path)))
model.key_word.load_state_dict(torch.load("{}/keyword_models.dump".format(model_path)))
model.col.load_state_dict(torch.load("{}/col_models.dump".format(model_path)))
model.op.load_state_dict(torch.load("{}/op_models.dump".format(model_path)))
model.agg.load_state_dict(torch.load("{}/agg_models.dump".format(model_path)))
model.root_teminal.load_state_dict(torch.load("{}/root_tem_models.dump".format(model_path)))
model.des_asc.load_state_dict(torch.load("{}/des_asc_models.dump".format(model_path)))
model.having.load_state_dict(torch.load("{}/having_models.dump".format(model_path)))

test_data = json.load(open(test_data_path))

table_dict = get_table_dict("../newdata_augment/tables.json")

correct_count = 0
total_count = 0
correct_count2 = 0
total_count2 = 0

for item in test_data[:]:
	db_id = item["db_id"]
	g_str = item["query"].lower()
	score = model.forward([item["question_toks"]]* 2, [], table_dict[db_id])
	score_data = score[0].data.cpu().numpy()[0]
	score_dict = dict([(x, score_data[x]) for x in range(0, 4)])
	sorted_score_dict = sorted(score_dict.items(), key = lambda x:x[1], reverse=True)
	num_kw = sorted_score_dict[0][0] 
	num_sets = set([sorted_score_dict[0][0]])
	for x in range(1, 4):
		if sorted_score_dict[x][1] >= sorted_score_dict[0][1] - 2.0:
			num_sets.add(sorted_score_dict[x][0])
	gt_num = 0
	kwlist = ["where", "group by", "order by"]
	for i in range(0, 3):
		gt_num += len(re.findall(kwlist[i], g_str))
	total_count += 1
	if num_kw == gt_num:	
		correct_count += 1
	total_count2 += len(num_sets)
	if gt_num in num_sets:
		correct_count2 += 1

print float(correct_count) / total_count, correct_count, total_count
print float(correct_count2) / total_count2, correct_count2, total_count2
